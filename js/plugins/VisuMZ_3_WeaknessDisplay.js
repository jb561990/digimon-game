//=============================================================================
// VisuStella MZ - Weakness Display
// VisuMZ_3_WeaknessDisplay.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_3_WeaknessDisplay = true;

var VisuMZ = VisuMZ || {};
VisuMZ.WeaknessDisplay = VisuMZ.WeaknessDisplay || {};
VisuMZ.WeaknessDisplay.version = 1.00;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 3] [Version 1.00] [WeaknessDisplay]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Weakness_Display_VisuStella_MZ
 * @base VisuMZ_1_BattleCore
 * @base VisuMZ_1_ElementStatusCore
 * @orderAfter VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_ElementStatusCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin creates a display in battle to show enemy elemental weaknesses.
 * These weaknesses will start off hidden and will be slowly revealed whenever
 * the respective enemies receive elemental damage of the correct type. This
 * way, your players no longer have to jot down mental notes on what enemies
 * are weak to which elements, and instead, have access to their pool of
 * discovered knowledge right at the screen.
 *
 * Features include all (but not limited to) the following:
 * 
 * * The Weakness Display is a new UI element added below each enemy.
 * * The display will reveal each of the elemental weaknesses an enemy has.
 * * The elements will be hidden at first but will be slowly revealed as the
 *   player hits the enemies with the correct element type.
 * * Players can use Analyze Weakness effects to reveal weaknesses without
 *   needing to hit them directly.
 * * Adjust the positions, icons, and elements for the Weakness Display to fit
 *   your game.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_1_BattleCore
 * * VisuMZ_1_ElementStatusCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 3 ------
 *
 * This plugin is a Tier 3 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 *
 * VisuMZ_3_BoostAction
 *
 * The VisuStella MZ Boost Action plugin contains a notetag that allows the
 * Weakness Display's Analyze effect to trigger multiple times. This notetag
 * is <Boost Analyze> and its potency is dependent on the Plugin Parameter
 * settings found in the Boost Action plugin.
 *
 * ---
 * 
 * VisuMZ_4_BreakShields
 * 
 * The VisuStella MZ Break Shields plugin has a game mechanic to protect one's
 * elemental weaknesses. The protected elements will show up in the Weakness
 * Display and have a special icon on top of them.
 * 
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === Analyze-Related Notetags ===
 * 
 * ---
 *
 * <Analyze Weakness: x>
 * <Analyze Weaknesses: x>
 *
 * - Used for: Skill, Item Notetags
 * - Reveals 'x' amount of elemental weaknesses the target enemy may have
 *   without needing to hit the enemy with the said elemental weakness first.
 * - Replace 'x' with a number representing the number of weaknesses to reveal
 *   through this action.
 * - This has no effect on actors.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * These are the general settings regarding the Weakness Display plugin. They
 * allow you to adjust which elements appear and which icons to assign to them.
 * They also let you control which icons to use for the various display parts.
 *
 * ---
 *
 * Elements
 * 
 *   Shown Elements:
 *   - This is a list of the Element ID's you wish to display.
 *   - If this is empty, this will show all database elements.
 * 
 *   Element Icons:
 *   - Icon ID's used for each Element ID in order.
 *   - Priority will be given to elements with \I[x] in their names.
 * 
 *     Auto-Assign Icons:
 *     - Automatically assign icons if they do not show up in the
 *       Plugin Parameter settings based on their English names?
 *     - Icons will be automatically assigned based on the default icon sheet
 *       for the following elements if their names are detected:
 *       - Air, Aqua, Axe, Blade, Bow, Claw, Crossbow, Cure, Dagger, Dark,
 *         Earth, Energy, Evil, Fire, Flame, Frost, Glove, Ground, Gun, Heal,
 *         Holy, Ice, Knife, Light, Lightning, Mace, Magic, Mana, Melee,
 *         Polearm, Power, Physical, Sacred, Spear, Staff, Sword, Thunder,
 *         Wand, Water, Whip, Wind
 *       - Unlisted element names will default to icon index 160.
 *
 * ---
 *
 * Icons
 * 
 *   Background Icon:
 *   - Which icon index do you wish to use to assign as the background for the
 *     Weakness Display icons?
 * 
 *   Unknown Icon:
 *   - Which icon index do you wish to use to assign as the unknown marker for
 *     the Weakness Display icons?
 * 
 *   Smooth Icons?:
 *   - Do you wish to smooth out the icons or pixelate them?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Display Settings
 * ============================================================================
 *
 * These are the settings regarding how the Weakness Display UI elements behave
 * in-game. Adjust them to make them fit the visuals for your battle system.
 *
 * ---
 *
 * Display UI
 * 
 *   Icon Scale:
 *   - Scale up or down the display UI.
 *   - Use a number between 0 and 1 for the best results.
 * 
 *   Always Visible?:
 *   - Do you wish to make the Weakness Display always visible?
 * 
 *   Temporary Duration:
 *   - If not always visible, how many frames will the Weakness Display be
 *     temporarily visible?
 *   - 60 frames = 1 second.
 *
 * ---
 *
 * Positioning
 * 
 *   Offset X/Y:
 *   - How much to offset the Weakness Display X/Y position by?
 *   - For X: Negative goes left. Positive goes right.
 *   - For Y: Negative goes up. Positive goes down.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.00 Official Release Date: April 28, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PluginCommandFunctionName
 * @text Category: Function Name
 * @desc Plugin Command Description Text
 *
 * @arg Step1:arraynum
 * @text Step 1: Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @arg option:num
 * @text Option Text
 * @type number
 * @max 1
 * @desc Change the value to this number
 * @default 42069
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemEnableWeaknessDisplayMenu
 * @text System: Enable WeaknessDisplay in Menu?
 * @desc Enables/disables WeaknessDisplay menu inside the main menu.
 *
 * @arg Enable:eval
 * @text Enable/Disable?
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables/disables WeaknessDisplay menu inside the main menu.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemShowWeaknessDisplayMenu
 * @text System: Show WeaknessDisplay in Menu?
 * @desc Shows/hides WeaknessDisplay menu inside the main menu.
 *
 * @arg Show:eval
 * @text Show/Hide?
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Shows/hides WeaknessDisplay menu inside the main menu.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param WeaknessDisplay
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param General:struct
 * @text General Settings
 * @type struct<General>
 * @desc These are the general settings regarding the Weakness Display.
 * @default {"Elements":"","ShownElements:arraynum":"[]","ElementIcons:arraynum":"[]","AutoIcon:eval":"true","Icons":"","BackgroundIcon:num":"16","UnknownIcon:num":"188","SmoothIcons:eval":"true"}
 *
 * @param Display:struct
 * @text Display Settings
 * @type struct<Display>
 * @desc These are the display settings regarding the Weakness Display UI.
 * @default {"DisplayUI":"","IconScale:num":"0.75","AlwaysVisible:eval":"false","TempDuration:num":"90","Positioning":"","DisplayOffsetX:num":"+0","DisplayOffsetY:num":"+0"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * General Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~General:
 *
 * @param Elements
 *
 * @param ShownElements:arraynum
 * @text Shown Elements
 * @parent Elements
 * @type number[]
 * @desc This is a list of the Element ID's you wish to display.
 * If this is empty, this will show all elements.
 * @default []
 *
 * @param ElementIcons:arraynum
 * @text Element Icons
 * @parent Elements
 * @type number[]
 * @desc Icon ID's used for each Element ID in order. Priority will
 * be given to elements with \I[x] in their names.
 * @default []
 *
 * @param AutoIcon:eval
 * @text Auto-Assign Icons
 * @parent ElementIcons:arraynum
 * @type boolean
 * @on Auto-Assign
 * @off Nothing
 * @desc Automatically assign icons if they do not show up in the
 * Plugin Parameter settings based on their English names?
 * @default true
 *
 * @param Icons
 *
 * @param BackgroundIcon:num
 * @text Background Icon
 * @parent Icons
 * @desc Which icon index do you wish to use to assign as the
 * background for the Weakness Display icons?
 * @default 16
 *
 * @param UnknownIcon:num
 * @text Unknown Icon
 * @parent Icons
 * @desc Which icon index do you wish to use to assign as the
 * unknown marker for the Weakness Display icons?
 * @default 188
 *
 * @param SmoothIcons:eval
 * @text Smooth Icons?
 * @parent Icons
 * @type boolean
 * @on Smooth
 * @off Pixelate
 * @desc Do you wish to smooth out the icons or pixelate them?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Display Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Display:
 *
 * @param DisplayUI
 * @text Display UI
 *
 * @param IconScale:num
 * @text Icon Scale
 * @parent DisplayUI
 * @desc Scale up or down the display UI.
 * Use a number between 0 and 1 for the best results.
 * @default 0.75
 *
 * @param AlwaysVisible:eval
 * @text Always Visible?
 * @parent DisplayUI
 * @type boolean
 * @on Always Visible
 * @off Temporarily
 * @desc Do you wish to make the Weakness Display always visible?
 * @default false
 *
 * @param TempDuration:num
 * @text Temporary Duration
 * @parent AlwaysVisible:eval
 * @type number
 * @desc If not always visible, how many frames will the Weakness
 * Display be temporarily visible? 60 frames = 1 second.
 * @default 90
 *
 * @param Positioning
 *
 * @param DisplayOffsetX:num
 * @text Offset X
 * @parent Positioning
 * @desc How much to offset the Weakness Display X position by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param DisplayOffsetY:num
 * @text Offset Y
 * @parent Positioning
 * @desc How much to offset the Weakness Display Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 *
 */
//=============================================================================

const _0x35e7=['update','isAlwaysVisible','elementRate','Game_Action_executeDamage','_baseX','BREAK_SHIELDS_MINIMUM_WEAKNESS_RATE','_weaknessDisplayUnshiftedElementIcons','ElementIcons','item','Game_BattlerBase_refresh','WEAKNESS_DISPLAY_DURATION','setPosition','exit','IconScale','_iconSheet','format','weaknessDisplayShownElements','weaknessDisplayIconsheetBitmap','createWeaknessContainerSprites','ARRAYJSON','trim','TempDuration','floor','JSON','WEAKNESS_DISPLAY_AUTO_ICONS','elements','_elementID','RegExp','isSceneBattle','apply','revealWeaknessDisplay','enemyId','_uiContainer','ShownElements','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','updateLink','WEAKNESS_DISPLAY_SHOWN_ELEMENTS','SCALE_RATE','_childSpriteContainer','status','parameters','356QybLGG','EVAL','subject','AutoIcon','_backgroundSprite','_weaknessDisplayShownElements','keys','Game_Action_applyItemUserEffect','General','_visibleElements','revealNewWeaknesses','iconHeight','WEAKNESS_DISPLAY_SMOOTHING','resize','getProtectedWeaknessElements','BoostAction','iconWidth','initializeRevealedEnemyWeaknesses','ARRAYEVAL','STR','FUNC','refreshAllWeaknessDisplaySprites','Analyze','_revealedEnemyWeaknesses','length','STRUCT','initMembers','scale','900245XLgyEI','_showWeaknessDisplayDuration','UnknownIcon','Game_Action_apply','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','369129zGSVqG','_weaknessDisplayIconSheet','boostAddition','getWeaknessElements','1oXRovc','_iconIndexSprite','setFrame','_linkedSprite','_protectionSprite','note','max','updatePosition','VisuMZ_3_BoostAction','setProtected','getRevealedEnemyWeaknesses','getRevealedElements','isEnemy','_battleField','1hBWAyZ','64072RDAHXu','1834PfGUnz','name','updateAllWeaknessDisplaySprites','AlwaysVisible','return\x200','prototype','updateChildSprites','applyItemUserEffect','ARRAYSTRUCT','WEAKNESS_DISPLAY_UNKNOWN_ICON','Display','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','width','refresh','_customModified','_baseY','applyWeaknessAnalyze','weaknessDisplayTransferBitmap','_battler','addLoadListener','height','setup','OFFSET_Y','isDrain','round','VisuMZ_1_ElementStatusCore','updateOpacity','Spriteset_Battle_update','random','toUpperCase','opacity','createLowerLayer','Settings','addChild','SmoothIcons','blt','executeDamage','push','AnalyzeEffect','isDead','description','864031skqAhn','ConvertParams','VisuMZ_4_BreakShields','bitmap','_protectIcon','visibilityState','match','isAppeared','ARRAYSTR','_scene','WEAKNESS_DISPLAY_ELEMENT_ICONS','originalElementRate','includes','_weaknessContainer','IconSet','contains','23URHMBr','create','Spriteset_Battle_createLowerLayer','getWeaknessDisplayElementIcon','call','updateIconFrame','createChildrenSprite','ARRAYFUNC','_enemyWindow','719051KXwHLM','WeaknessDisplay','createChildSpriteContainer','sort','children','breakShield_ProtectIcon','_needRefreshAllEnemyWeaknessWindows','map','NUM','filter','_iconIndex','OFFSET_X','version','BoostAnalyze','Game_Temp_requestAnimation','1QDxyPu','initialize','_lineHeight','isRecover','addEnemyWeaknessElement','setRevealed','members','parse','bind','35635TYqWgC','requestAnimation','linkSprite'];const _0x2009=function(_0x52b828,_0x26e7b3){_0x52b828=_0x52b828-0xfc;let _0x35e765=_0x35e7[_0x52b828];return _0x35e765;};const _0x18d993=_0x2009;(function(_0x190ce9,_0xecf2c8){const _0xf595c5=_0x2009;while(!![]){try{const _0x3c4273=-parseInt(_0xf595c5(0x11f))+parseInt(_0xf595c5(0x16c))*-parseInt(_0xf595c5(0x18d))+-parseInt(_0xf595c5(0x15c))*-parseInt(_0xf595c5(0x131))+parseInt(_0xf595c5(0x11a))+parseInt(_0xf595c5(0xfe))*-parseInt(_0xf595c5(0x133))+parseInt(_0xf595c5(0x123))*-parseInt(_0xf595c5(0x132))+-parseInt(_0xf595c5(0x184))*-parseInt(_0xf595c5(0x175));if(_0x3c4273===_0xecf2c8)break;else _0x190ce9['push'](_0x190ce9['shift']());}catch(_0x3ef2af){_0x190ce9['push'](_0x190ce9['shift']());}}}(_0x35e7,0x8d051));var label=_0x18d993(0x176),tier=tier||0x0,dependencies=['VisuMZ_1_BattleCore',_0x18d993(0x14c)],pluginData=$plugins[_0x18d993(0x17e)](function(_0xe9eff5){const _0x8c6cb7=_0x18d993;return _0xe9eff5[_0x8c6cb7(0xfc)]&&_0xe9eff5[_0x8c6cb7(0x15b)][_0x8c6cb7(0x168)]('['+label+']');})[0x0];VisuMZ[label]['Settings']=VisuMZ[label][_0x18d993(0x153)]||{},VisuMZ[_0x18d993(0x15d)]=function(_0x11c4f4,_0xa12def){const _0x1931f1=_0x18d993;for(const _0x22f285 in _0xa12def){if(_0x22f285[_0x1931f1(0x162)](/(.*):(.*)/i)){const _0x3f7453=String(RegExp['$1']),_0x533809=String(RegExp['$2'])[_0x1931f1(0x150)]()[_0x1931f1(0x1a4)]();let _0x3afa7b,_0x5a79d9,_0x799a0b;switch(_0x533809){case _0x1931f1(0x17d):_0x3afa7b=_0xa12def[_0x22f285]!==''?Number(_0xa12def[_0x22f285]):0x0;break;case'ARRAYNUM':_0x5a79d9=_0xa12def[_0x22f285]!==''?JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285]):[],_0x3afa7b=_0x5a79d9[_0x1931f1(0x17c)](_0x323a61=>Number(_0x323a61));break;case _0x1931f1(0xff):_0x3afa7b=_0xa12def[_0x22f285]!==''?eval(_0xa12def[_0x22f285]):null;break;case _0x1931f1(0x110):_0x5a79d9=_0xa12def[_0x22f285]!==''?JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285]):[],_0x3afa7b=_0x5a79d9[_0x1931f1(0x17c)](_0x3a800a=>eval(_0x3a800a));break;case _0x1931f1(0x1a7):_0x3afa7b=_0xa12def[_0x22f285]!==''?JSON['parse'](_0xa12def[_0x22f285]):'';break;case _0x1931f1(0x1a3):_0x5a79d9=_0xa12def[_0x22f285]!==''?JSON['parse'](_0xa12def[_0x22f285]):[],_0x3afa7b=_0x5a79d9['map'](_0xa40d8d=>JSON[_0x1931f1(0x18b)](_0xa40d8d));break;case _0x1931f1(0x112):_0x3afa7b=_0xa12def[_0x22f285]!==''?new Function(JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285])):new Function(_0x1931f1(0x137));break;case _0x1931f1(0x173):_0x5a79d9=_0xa12def[_0x22f285]!==''?JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285]):[],_0x3afa7b=_0x5a79d9[_0x1931f1(0x17c)](_0x5e193a=>new Function(JSON['parse'](_0x5e193a)));break;case _0x1931f1(0x111):_0x3afa7b=_0xa12def[_0x22f285]!==''?String(_0xa12def[_0x22f285]):'';break;case _0x1931f1(0x164):_0x5a79d9=_0xa12def[_0x22f285]!==''?JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285]):[],_0x3afa7b=_0x5a79d9[_0x1931f1(0x17c)](_0x36d1d2=>String(_0x36d1d2));break;case _0x1931f1(0x117):_0x799a0b=_0xa12def[_0x22f285]!==''?JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285]):{},_0x3afa7b=VisuMZ[_0x1931f1(0x15d)]({},_0x799a0b);break;case _0x1931f1(0x13b):_0x5a79d9=_0xa12def[_0x22f285]!==''?JSON[_0x1931f1(0x18b)](_0xa12def[_0x22f285]):[],_0x3afa7b=_0x5a79d9[_0x1931f1(0x17c)](_0x51df59=>VisuMZ[_0x1931f1(0x15d)]({},JSON[_0x1931f1(0x18b)](_0x51df59)));break;default:continue;}_0x11c4f4[_0x3f7453]=_0x3afa7b;}}return _0x11c4f4;},(_0x596917=>{const _0x1bd0e3=_0x18d993,_0x222168=_0x596917[_0x1bd0e3(0x134)];for(const _0x294410 of dependencies){if(!Imported[_0x294410]){alert(_0x1bd0e3(0x13e)[_0x1bd0e3(0x19f)](_0x222168,_0x294410)),SceneManager['exit']();break;}}const _0x367d0c=_0x596917['description'];if(_0x367d0c[_0x1bd0e3(0x162)](/\[Version[ ](.*?)\]/i)){const _0x54af5b=Number(RegExp['$1']);_0x54af5b!==VisuMZ[label][_0x1bd0e3(0x181)]&&(alert(_0x1bd0e3(0x11e)[_0x1bd0e3(0x19f)](_0x222168,_0x54af5b)),SceneManager[_0x1bd0e3(0x19c)]());}if(_0x367d0c['match'](/\[Tier[ ](\d+)\]/i)){const _0x20cc39=Number(RegExp['$1']);_0x20cc39<tier?(alert(_0x1bd0e3(0x1b2)[_0x1bd0e3(0x19f)](_0x222168,_0x20cc39,tier)),SceneManager['exit']()):tier=Math[_0x1bd0e3(0x129)](_0x20cc39,tier);}VisuMZ[_0x1bd0e3(0x15d)](VisuMZ[label][_0x1bd0e3(0x153)],_0x596917[_0x1bd0e3(0xfd)]);})(pluginData),VisuMZ[_0x18d993(0x176)][_0x18d993(0x1ab)]={'AnalyzeEffect':/<ANALYZE (?:WEAKNESS|WEAKNESSES): (\d+)>/i},DataManager['WEAKNESS_DISPLAY_SHOWN_ELEMENTS']=VisuMZ[_0x18d993(0x176)]['Settings'][_0x18d993(0x106)][_0x18d993(0x1b1)],DataManager[_0x18d993(0x1a0)]=function(){const _0x1efd83=_0x18d993;if(this['_weaknessDisplayShownElements']!==undefined)return this[_0x1efd83(0x103)];let _0x423d18=DataManager[_0x1efd83(0x1b4)];const _0x17f3cd=_0x423d18[_0x1efd83(0x116)]>0x0?_0x423d18:[...Array($dataSystem['elements']['length']-0x1)[_0x1efd83(0x104)]()][_0x1efd83(0x17c)](_0xc7ea3a=>_0xc7ea3a+0x1);return this[_0x1efd83(0x103)]=_0x17f3cd[_0x1efd83(0x17e)](_0x35d128=>ImageManager[_0x1efd83(0x16f)](_0x35d128)>0x0),this[_0x1efd83(0x103)];},ImageManager[_0x18d993(0x166)]=VisuMZ[_0x18d993(0x176)][_0x18d993(0x153)][_0x18d993(0x106)][_0x18d993(0x197)],ImageManager['WEAKNESS_DISPLAY_AUTO_ICONS']=VisuMZ[_0x18d993(0x176)][_0x18d993(0x153)][_0x18d993(0x106)][_0x18d993(0x101)],ImageManager['WEAKNESS_DISPLAY_BACKGROUND_ICON']=VisuMZ[_0x18d993(0x176)][_0x18d993(0x153)][_0x18d993(0x106)]['BackgroundIcon'],ImageManager[_0x18d993(0x13c)]=VisuMZ[_0x18d993(0x176)]['Settings'][_0x18d993(0x106)][_0x18d993(0x11c)],ImageManager[_0x18d993(0x10a)]=VisuMZ['WeaknessDisplay'][_0x18d993(0x153)][_0x18d993(0x106)][_0x18d993(0x155)],ImageManager[_0x18d993(0x16f)]=function(_0xa83f39){const _0x3d74a5=_0x18d993;!this['_weaknessDisplayUnshiftedElementIcons']&&(this[_0x3d74a5(0x196)]=!![],ImageManager['WEAKNESS_DISPLAY_ELEMENT_ICONS']['unshift'](0x0));const _0x158aa9=$dataSystem[_0x3d74a5(0x1a9)][_0xa83f39];if(_0x158aa9[_0x3d74a5(0x162)](/\\I\[(\d+)\]/i))return Number(RegExp['$1']);else{if(ImageManager['WEAKNESS_DISPLAY_ELEMENT_ICONS'][_0xa83f39])return ImageManager[_0x3d74a5(0x166)][_0xa83f39];else{if(ImageManager[_0x3d74a5(0x1a8)]){if(_0x158aa9[_0x3d74a5(0x162)](/(?:FIRE|FLAME)/i))return 0x40;else{if(_0x158aa9['match'](/(?:ICE|FROST)/i))return 0x41;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:THUNDER|LIGHTNING)/i))return 0x42;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:WATER|AQUA)/i))return 0x43;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:EARTH|GROUND)/i))return 0x44;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:WIND|AIR)/i))return 0x45;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:HOLY|LIGHT|SACRED)/i))return 0x46;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:DARK|EVIL)/i))return 0x47;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:HEAL|CURE)/i))return 0x48;else{if(_0x158aa9['match'](/(?:PHYS|MELEE)/i))return 0x4d;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:ENERGY|POWER)/i))return 0x4e;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:MAGIC|MANA)/i))return 0x4f;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:DAGGER|KNIFE)/i))return 0x60;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:SWORD|BLADE)/i))return 0x61;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:MACE|MORNING)/i))return 0x62;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:AXE)/i))return 0x63;else{if(_0x158aa9['match'](/(?:WHIP)/i))return 0x64;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:WAND|STAFF)/i))return 0x65;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:CROSSBOW)/i))return 0x67;else{if(_0x158aa9['match'](/(?:BOW)/i))return 0x66;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:GUN)/i))return 0x68;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:CLAW)/i))return 0x69;else{if(_0x158aa9[_0x3d74a5(0x162)](/(?:GLOVE)/i))return 0x6a;else return _0x158aa9['match'](/(?:SPEAR|POLE)/i)?0x6b:0xa0;}}}}}}}}}}}}}}}}}}}}}}}else return 0x0;}}},ImageManager[_0x18d993(0x1a1)]=function(){const _0x50ce11=_0x18d993;if(!this[_0x50ce11(0x120)]){this[_0x50ce11(0x120)]=new Bitmap();const _0x5d8c11=ImageManager['loadSystem'](_0x50ce11(0x16a));_0x5d8c11[_0x50ce11(0x146)](this[_0x50ce11(0x144)][_0x50ce11(0x18c)](this,_0x5d8c11));}return this[_0x50ce11(0x120)];},ImageManager[_0x18d993(0x144)]=function(_0x45578a){const _0x2d7a8b=_0x18d993;this[_0x2d7a8b(0x120)]['resize'](_0x45578a['width'],_0x45578a['height']),this[_0x2d7a8b(0x120)]['blt'](_0x45578a,0x0,0x0,_0x45578a[_0x2d7a8b(0x13f)],_0x45578a['height'],0x0,0x0),this[_0x2d7a8b(0x120)]['smooth']=ImageManager['WEAKNESS_DISPLAY_SMOOTHING'],this['_weaknessDisplayIconSheet'][_0x2d7a8b(0x141)]=![];},BattleManager['revealWeakness']=function(_0x1732a2){const _0x508c7c=_0x18d993;var _0x581a16=$gameTroop[_0x508c7c(0x18a)](),_0x329ba9=[];for(var _0x519646=0x0;_0x519646<_0x581a16['length'];_0x519646++){var _0xd0f0fa=_0x581a16[_0x519646];!!_0xd0f0fa&&!_0x329ba9[_0x508c7c(0x16b)](_0xd0f0fa[_0x508c7c(0x1af)]())&&(_0xd0f0fa[_0x508c7c(0x108)](_0x1732a2),_0x329ba9[_0x508c7c(0x158)](_0xd0f0fa[_0x508c7c(0x1af)]()));}},VisuMZ[_0x18d993(0x176)][_0x18d993(0x183)]=Game_Temp[_0x18d993(0x138)][_0x18d993(0x18e)],Game_Temp['prototype'][_0x18d993(0x18e)]=function(_0x1511d0,_0x5e538f,_0x5f6a32=![]){const _0x30858d=_0x18d993;VisuMZ[_0x30858d(0x176)][_0x30858d(0x183)][_0x30858d(0x170)](this,_0x1511d0,_0x5e538f,_0x5f6a32);if(!SceneManager[_0x30858d(0x1ac)]())return;for(const _0xeadc6 of _0x1511d0){if(!_0xeadc6)continue;_0xeadc6[_0x30858d(0x12f)]()&&_0xeadc6[_0x30858d(0x1ae)]();}},VisuMZ[_0x18d993(0x176)]['Game_System_initialize']=Game_System['prototype']['initialize'],Game_System[_0x18d993(0x138)][_0x18d993(0x185)]=function(){const _0x2dee10=_0x18d993;VisuMZ[_0x2dee10(0x176)]['Game_System_initialize'][_0x2dee10(0x170)](this),this[_0x2dee10(0x10f)]();},Game_System[_0x18d993(0x138)][_0x18d993(0x10f)]=function(){const _0x13af8d=_0x18d993;this[_0x13af8d(0x115)]=this[_0x13af8d(0x115)]||{};},Game_System[_0x18d993(0x138)][_0x18d993(0x188)]=function(_0x83e1d2,_0x58f966){const _0x31929c=_0x18d993;this[_0x31929c(0x115)]===undefined&&this[_0x31929c(0x10f)](),this['_revealedEnemyWeaknesses'][_0x83e1d2]=this[_0x31929c(0x115)][_0x83e1d2]||[],!this[_0x31929c(0x115)][_0x83e1d2][_0x31929c(0x16b)](_0x58f966)&&this[_0x31929c(0x115)][_0x83e1d2]['push'](_0x58f966),this['_revealedEnemyWeaknesses'][_0x83e1d2][_0x31929c(0x178)](function(_0x2d8563,_0x230d8a){return _0x2d8563-_0x230d8a;});},Game_System[_0x18d993(0x138)]['getRevealedEnemyWeaknesses']=function(_0x1897e1){const _0x412764=_0x18d993;return this['_revealedEnemyWeaknesses']===undefined&&this[_0x412764(0x10f)](),this[_0x412764(0x115)][_0x1897e1]=this[_0x412764(0x115)][_0x1897e1]||[],this['_revealedEnemyWeaknesses'][_0x1897e1];},VisuMZ[_0x18d993(0x176)]['Game_Action_apply']=Game_Action[_0x18d993(0x138)][_0x18d993(0x1ad)],Game_Action[_0x18d993(0x138)][_0x18d993(0x1ad)]=function(_0x540b4d){const _0x4bac33=_0x18d993;VisuMZ['WeaknessDisplay'][_0x4bac33(0x11d)][_0x4bac33(0x170)](this,_0x540b4d),_0x540b4d[_0x4bac33(0x1ae)]();},VisuMZ[_0x18d993(0x176)][_0x18d993(0x193)]=Game_Action[_0x18d993(0x138)][_0x18d993(0x157)],Game_Action[_0x18d993(0x138)]['executeDamage']=function(_0x21b462,_0x2d9e74){const _0x18c4e6=_0x18d993;VisuMZ[_0x18c4e6(0x176)][_0x18c4e6(0x193)][_0x18c4e6(0x170)](this,_0x21b462,_0x2d9e74),!!_0x21b462&&_0x21b462[_0x18c4e6(0x12f)]()&&(this['isDamage']()||this[_0x18c4e6(0x187)]()||this[_0x18c4e6(0x14a)]())&&this[_0x18c4e6(0x188)](_0x21b462);},Game_Action[_0x18d993(0x138)][_0x18d993(0x188)]=function(_0x484d08){const _0x16950a=_0x18d993;for(const _0x4aa623 of this['elements']()){_0x4aa623>0x0&&$gameSystem[_0x16950a(0x188)](_0x484d08['enemyId'](),_0x4aa623);}},VisuMZ['WeaknessDisplay'][_0x18d993(0x105)]=Game_Action[_0x18d993(0x138)][_0x18d993(0x13a)],Game_Action[_0x18d993(0x138)][_0x18d993(0x13a)]=function(_0x5f5bdc){const _0x493880=_0x18d993;VisuMZ[_0x493880(0x176)][_0x493880(0x105)][_0x493880(0x170)](this,_0x5f5bdc),_0x5f5bdc['isEnemy']()&&this[_0x493880(0x143)](_0x5f5bdc);},Game_Action[_0x18d993(0x138)][_0x18d993(0x143)]=function(_0x5ae9c1){const _0x530f2c=_0x18d993,_0x31cea1=VisuMZ[_0x530f2c(0x176)]['RegExp'];if(this[_0x530f2c(0x198)]()[_0x530f2c(0x128)]['match'](_0x31cea1[_0x530f2c(0x159)])){var _0x3d4476=parseInt(RegExp['$1']);if(Imported[_0x530f2c(0x12b)]&&this['item']()[_0x530f2c(0x128)][_0x530f2c(0x162)](VisuMZ[_0x530f2c(0x10d)][_0x530f2c(0x1ab)][_0x530f2c(0x182)])){var _0x3f7440=this[_0x530f2c(0x100)]()['boostMultiplier'](_0x530f2c(0x114));_0x3d4476=Math[_0x530f2c(0x14b)](_0x3f7440*_0x3d4476),_0x3d4476+=this['subject']()[_0x530f2c(0x121)](_0x530f2c(0x114));}_0x5ae9c1[_0x530f2c(0x108)](_0x3d4476);}},VisuMZ['WeaknessDisplay'][_0x18d993(0x199)]=Game_BattlerBase[_0x18d993(0x138)][_0x18d993(0x140)],Game_BattlerBase['prototype'][_0x18d993(0x140)]=function(){const _0x385b71=_0x18d993;VisuMZ['WeaknessDisplay'][_0x385b71(0x199)][_0x385b71(0x170)](this),this[_0x385b71(0x12f)]()&&($gameTemp[_0x385b71(0x17b)]=!![]);},Game_Battler[_0x18d993(0x138)]['revealWeaknessDisplay']=function(){},Game_Enemy['WEAKNESS_DISPLAY_DURATION']=VisuMZ[_0x18d993(0x176)][_0x18d993(0x153)][_0x18d993(0x13d)][_0x18d993(0x1a5)],Game_Enemy[_0x18d993(0x138)][_0x18d993(0x122)]=function(){const _0x2b6833=_0x18d993;var _0x46d48e=[];for(const _0x56684a of DataManager[_0x2b6833(0x1a0)]()){const _0x34509d=Imported['VisuMZ_4_BreakShields']?this[_0x2b6833(0x167)](_0x56684a):this[_0x2b6833(0x192)](_0x56684a),_0xd264f2=Imported[_0x2b6833(0x15e)]?Game_Action[_0x2b6833(0x195)]:1.05;_0x34509d>=_0xd264f2&&_0x46d48e[_0x2b6833(0x158)](_0x56684a);}return _0x46d48e;},Game_Enemy[_0x18d993(0x138)][_0x18d993(0x12e)]=function(){const _0x353bb8=_0x18d993;return $gameSystem['getRevealedEnemyWeaknesses'](this[_0x353bb8(0x1af)]());},Game_Enemy['prototype']['revealNewWeaknesses']=function(_0x1b6723){const _0x3441b8=_0x18d993;var _0x172ed7=this[_0x3441b8(0x122)](),_0xae17e=$gameSystem[_0x3441b8(0x12d)](this[_0x3441b8(0x1af)]()),_0x441281=[];for(var _0x3621a2=0x0;_0x3621a2<_0x172ed7['length'];_0x3621a2++){var _0x55fa15=_0x172ed7[_0x3621a2];!_0xae17e[_0x3441b8(0x16b)](_0x55fa15)&&_0x441281['push'](_0x55fa15);}while(_0x1b6723>0x0){if(_0x441281[_0x3441b8(0x116)]<=0x0)break;_0x1b6723-=0x1;var _0x3563f6=Math['floor'](Math[_0x3441b8(0x14f)]()*_0x441281['length']),_0x210759=_0x441281[_0x3563f6];$gameSystem[_0x3441b8(0x188)](this['enemyId'](),_0x210759),_0x441281['splice'](_0x3563f6,0x1);}this[_0x3441b8(0x1ae)](),$gameTemp[_0x3441b8(0x17b)]=!![];},Game_Enemy[_0x18d993(0x138)][_0x18d993(0x1ae)]=function(){const _0x331c1e=_0x18d993;this[_0x331c1e(0x11b)]=Game_Enemy[_0x331c1e(0x19a)];};function Sprite_WeaknessContainer(){this['initialize'](...arguments);}Sprite_WeaknessContainer['prototype']=Object[_0x18d993(0x16d)](Sprite['prototype']),Sprite_WeaknessContainer['prototype']['constructor']=Sprite_WeaknessContainer,Sprite_WeaknessContainer[_0x18d993(0x1b5)]=VisuMZ[_0x18d993(0x176)][_0x18d993(0x153)][_0x18d993(0x13d)][_0x18d993(0x19d)],Sprite_WeaknessContainer['ALWAYS_VISIBLE']=VisuMZ['WeaknessDisplay'][_0x18d993(0x153)][_0x18d993(0x13d)][_0x18d993(0x136)],Sprite_WeaknessContainer[_0x18d993(0x180)]=VisuMZ['WeaknessDisplay'][_0x18d993(0x153)][_0x18d993(0x13d)]['DisplayOffsetX'],Sprite_WeaknessContainer['OFFSET_Y']=VisuMZ[_0x18d993(0x176)][_0x18d993(0x153)][_0x18d993(0x13d)]['DisplayOffsetY'],Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x185)]=function(){const _0xc9fa8a=_0x18d993;Sprite[_0xc9fa8a(0x138)]['initialize'][_0xc9fa8a(0x170)](this),this[_0xc9fa8a(0x118)](),this[_0xc9fa8a(0x177)]();},Sprite_WeaknessContainer['prototype']['initMembers']=function(){const _0x2d04be=_0x18d993;this['opacity']=0x0,this[_0x2d04be(0x126)]=null,this[_0x2d04be(0x145)]=null,this[_0x2d04be(0x119)]['x']=this[_0x2d04be(0x119)]['y']=Sprite_WeaknessContainer[_0x2d04be(0x1b5)],this[_0x2d04be(0x107)]=0x0;},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x177)]=function(){const _0xc456c2=_0x18d993;this[_0xc456c2(0x1b6)]=new Sprite(),this[_0xc456c2(0x154)](this[_0xc456c2(0x1b6)]);for(const _0xcf0852 of DataManager['weaknessDisplayShownElements']()){const _0x435a8=new Sprite_WeaknessIcon(_0xcf0852);this[_0xc456c2(0x1b6)][_0xc456c2(0x154)](_0x435a8);}},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x18f)]=function(_0x1221ff){const _0x34f1f5=_0x18d993;this[_0x34f1f5(0x126)]=_0x1221ff;},Sprite_WeaknessContainer[_0x18d993(0x138)]['setup']=function(_0x20c60a){const _0x2b0764=_0x18d993;this[_0x2b0764(0x145)]=_0x20c60a,this['updateChildSprites']();},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x190)]=function(){const _0x32ca7c=_0x18d993;Sprite['prototype'][_0x32ca7c(0x190)][_0x32ca7c(0x170)](this),this['updateLink'](),this[_0x32ca7c(0x12a)](),this[_0x32ca7c(0x14d)]();},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x1b3)]=function(){const _0x3e044d=_0x18d993;if(!this['_linkedSprite'])return;this[_0x3e044d(0x145)]!==this[_0x3e044d(0x126)][_0x3e044d(0x145)]&&this[_0x3e044d(0x148)](this[_0x3e044d(0x126)][_0x3e044d(0x145)]);},Sprite_WeaknessContainer[_0x18d993(0x138)]['updatePosition']=function(){const _0x254ffe=_0x18d993;if(!this[_0x254ffe(0x126)])return;this[_0x254ffe(0x186)]=this[_0x254ffe(0x186)]||Window_Base[_0x254ffe(0x138)]['lineHeight'](),this['x']=this['_linkedSprite'][_0x254ffe(0x194)],this['y']=this['_linkedSprite'][_0x254ffe(0x142)]+this[_0x254ffe(0x186)]*0.5,this['x']+=Sprite_WeaknessContainer[_0x254ffe(0x180)]||0x0,this['y']+=Sprite_WeaknessContainer[_0x254ffe(0x149)]||0x0;const _0x4f549f=this[_0x254ffe(0x107)]*ImageManager[_0x254ffe(0x10e)];this[_0x254ffe(0x1b6)]['x']=Math[_0x254ffe(0x14b)](-_0x4f549f/0x2);},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x139)]=function(){const _0x3372fa=_0x18d993;if(this[_0x3372fa(0x145)]&&this[_0x3372fa(0x1b6)]){const _0x33e976=this['_battler'][_0x3372fa(0x122)](),_0x203e9b=this['_battler']['getRevealedElements'](),_0x61199c=Imported['VisuMZ_4_BreakShields']?this[_0x3372fa(0x145)][_0x3372fa(0x10c)]():[];this[_0x3372fa(0x107)]=0x0;let _0xa69bef=0x0;for(const _0x1c909d of DataManager[_0x3372fa(0x1a0)]()){const _0x158bdd=this[_0x3372fa(0x1b6)][_0x3372fa(0x179)][_0xa69bef];_0xa69bef++,_0x33e976[_0x3372fa(0x168)](_0x1c909d)?(_0x158bdd['setPosition'](this[_0x3372fa(0x107)]),_0x158bdd[_0x3372fa(0x189)](_0x203e9b['includes'](_0x1c909d)),_0x158bdd[_0x3372fa(0x12c)](_0x61199c[_0x3372fa(0x168)](_0x1c909d)),this[_0x3372fa(0x107)]++):_0x158bdd[_0x3372fa(0x19b)](-0x1);}}else this[_0x3372fa(0x151)]=0x0;},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x14d)]=function(){const _0x2f77be=_0x18d993,_0x59bfea=this[_0x2f77be(0x161)]();if(_0x59bfea&&this['opacity']<0xff)this[_0x2f77be(0x151)]+=0x10;else!_0x59bfea&&this[_0x2f77be(0x151)]>0x0&&(this[_0x2f77be(0x151)]-=0x10);},Sprite_WeaknessContainer[_0x18d993(0x138)][_0x18d993(0x161)]=function(){const _0x2ebfb5=_0x18d993;if(!this[_0x2ebfb5(0x145)])return![];else{if(this[_0x2ebfb5(0x145)][_0x2ebfb5(0x15a)]())return![];else{if(!this[_0x2ebfb5(0x145)][_0x2ebfb5(0x163)]())return![];else{if(this[_0x2ebfb5(0x191)]())return!![];else{if(this[_0x2ebfb5(0x145)]['_showWeaknessDisplayDuration'])return this['_battler'][_0x2ebfb5(0x11b)]--,!![];else{if(SceneManager[_0x2ebfb5(0x165)]['_enemyWindow']&&SceneManager[_0x2ebfb5(0x165)][_0x2ebfb5(0x174)]['active']&&SceneManager['_scene'][_0x2ebfb5(0x174)]['_enemies']['includes'](this[_0x2ebfb5(0x145)]))return!![];else{if(this[_0x2ebfb5(0x151)]>0x0)return![];}}}}}}},Sprite_WeaknessContainer['prototype'][_0x18d993(0x191)]=function(){return Sprite_WeaknessContainer['ALWAYS_VISIBLE'];};function Sprite_WeaknessIcon(){const _0x2c3758=_0x18d993;this[_0x2c3758(0x185)](...arguments);}Sprite_WeaknessIcon[_0x18d993(0x138)]=Object['create'](Sprite[_0x18d993(0x138)]),Sprite_WeaknessIcon[_0x18d993(0x138)]['constructor']=Sprite_WeaknessIcon,Sprite_WeaknessIcon['prototype'][_0x18d993(0x185)]=function(_0x63964f){const _0x185f13=_0x18d993;this[_0x185f13(0x1aa)]=_0x63964f,Sprite[_0x185f13(0x138)][_0x185f13(0x185)][_0x185f13(0x170)](this),this['initMembers'](),this[_0x185f13(0x172)]();},Sprite_WeaknessIcon[_0x18d993(0x138)]['initMembers']=function(){const _0x52aed0=_0x18d993;this['_backgroundIndex']=ImageManager['WEAKNESS_DISPLAY_BACKGROUND_ICON'],this['_iconIndex']=ImageManager[_0x52aed0(0x13c)],this[_0x52aed0(0x160)]=0x0;},Sprite_WeaknessIcon[_0x18d993(0x138)][_0x18d993(0x172)]=function(){const _0x4bec27=_0x18d993;this[_0x4bec27(0x19e)]=ImageManager[_0x4bec27(0x1a1)](),this[_0x4bec27(0x102)]=new Sprite(),this['_backgroundSprite'][_0x4bec27(0x15f)]=this['_iconSheet'],this[_0x4bec27(0x154)](this['_backgroundSprite']),this['_iconIndexSprite']=new Sprite(),this[_0x4bec27(0x124)][_0x4bec27(0x15f)]=this[_0x4bec27(0x19e)],this[_0x4bec27(0x154)](this['_iconIndexSprite']),this[_0x4bec27(0x127)]=new Sprite(),this['_protectionSprite'][_0x4bec27(0x15f)]=this[_0x4bec27(0x19e)],this[_0x4bec27(0x154)](this[_0x4bec27(0x127)]);},Sprite_WeaknessIcon['prototype']['transferBitmap']=function(_0x4f0a73){const _0x1824be=_0x18d993;this[_0x1824be(0x19e)][_0x1824be(0x10b)](_0x4f0a73[_0x1824be(0x13f)],_0x4f0a73[_0x1824be(0x147)]),this[_0x1824be(0x19e)][_0x1824be(0x156)](_0x4f0a73,0x0,0x0,_0x4f0a73['width'],_0x4f0a73[_0x1824be(0x147)],0x0,0x0);},Sprite_WeaknessIcon[_0x18d993(0x138)][_0x18d993(0x190)]=function(){const _0x1b649f=_0x18d993;Sprite['prototype'][_0x1b649f(0x190)][_0x1b649f(0x170)](this),this['updateIconFrame'](this[_0x1b649f(0x102)],this['_backgroundIndex']),this[_0x1b649f(0x171)](this[_0x1b649f(0x124)],this[_0x1b649f(0x17f)]),this[_0x1b649f(0x171)](this[_0x1b649f(0x127)],this[_0x1b649f(0x160)]);},Sprite_WeaknessIcon[_0x18d993(0x138)][_0x18d993(0x171)]=function(_0x2f8a4b,_0x3facf1){const _0x37ca91=_0x18d993;if(!_0x2f8a4b)return;const _0x29dcd4=ImageManager[_0x37ca91(0x10e)],_0x34b089=ImageManager[_0x37ca91(0x109)],_0x158d01=_0x3facf1%0x10*_0x29dcd4,_0x4e720d=Math[_0x37ca91(0x1a6)](_0x3facf1/0x10)*_0x34b089;_0x2f8a4b[_0x37ca91(0x125)](_0x158d01,_0x4e720d,_0x29dcd4,_0x34b089);},Sprite_WeaknessIcon[_0x18d993(0x138)][_0x18d993(0x19b)]=function(_0xc3c483){const _0x5b426a=_0x18d993;_0xc3c483<0x0?this['opacity']=0x0:(this[_0x5b426a(0x151)]=0xff,this['x']=ImageManager['iconWidth']*_0xc3c483);},Sprite_WeaknessIcon[_0x18d993(0x138)]['setRevealed']=function(_0x329800){const _0x74657=_0x18d993;_0x329800?this['_iconIndex']=ImageManager[_0x74657(0x16f)](this[_0x74657(0x1aa)]):this[_0x74657(0x17f)]=ImageManager['WEAKNESS_DISPLAY_UNKNOWN_ICON'];},Sprite_WeaknessIcon[_0x18d993(0x138)][_0x18d993(0x12c)]=function(_0x44ebc9){const _0x33ba7f=_0x18d993;_0x44ebc9&&Imported[_0x33ba7f(0x15e)]?this[_0x33ba7f(0x160)]=ImageManager[_0x33ba7f(0x17a)]:this['_protectIcon']=0x0;},VisuMZ['WeaknessDisplay'][_0x18d993(0x16e)]=Spriteset_Battle['prototype']['createLowerLayer'],Spriteset_Battle[_0x18d993(0x138)][_0x18d993(0x152)]=function(){const _0x4ec7c4=_0x18d993;VisuMZ['WeaknessDisplay'][_0x4ec7c4(0x16e)][_0x4ec7c4(0x170)](this),this[_0x4ec7c4(0x1a2)]();},Spriteset_Battle[_0x18d993(0x138)][_0x18d993(0x1a2)]=function(){const _0x310383=_0x18d993;this[_0x310383(0x169)]=new Sprite();this[_0x310383(0x1b0)]?this[_0x310383(0x1b0)][_0x310383(0x154)](this[_0x310383(0x169)]):this[_0x310383(0x130)][_0x310383(0x154)](this[_0x310383(0x169)]);for(const _0x27bb5d of this['_enemySprites']){const _0x4f702c=new Sprite_WeaknessContainer();this[_0x310383(0x169)][_0x310383(0x154)](_0x4f702c),_0x4f702c[_0x310383(0x18f)](_0x27bb5d);}},VisuMZ['WeaknessDisplay']['Spriteset_Battle_update']=Spriteset_Battle[_0x18d993(0x138)][_0x18d993(0x190)],Spriteset_Battle[_0x18d993(0x138)]['update']=function(){const _0x31a13b=_0x18d993;VisuMZ[_0x31a13b(0x176)][_0x31a13b(0x14e)][_0x31a13b(0x170)](this),this[_0x31a13b(0x135)]();},Spriteset_Battle['prototype'][_0x18d993(0x135)]=function(){const _0x32f546=_0x18d993;$gameTemp[_0x32f546(0x17b)]&&($gameTemp['_needRefreshAllEnemyWeaknessWindows']=![],this['refreshAllWeaknessDisplaySprites']());},Spriteset_Battle[_0x18d993(0x138)][_0x18d993(0x113)]=function(){const _0x5c20f7=_0x18d993;for(const _0x32e029 of this[_0x5c20f7(0x169)][_0x5c20f7(0x179)]){if(!_0x32e029)continue;_0x32e029[_0x5c20f7(0x139)]();}};