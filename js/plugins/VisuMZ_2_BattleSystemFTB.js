//=============================================================================
// VisuStella MZ - Battle System - FTB - Free Turn Battle
// VisuMZ_2_BattleSystemFTB.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_BattleSystemFTB = true;

var VisuMZ = VisuMZ || {};
VisuMZ.BattleSystemFTB = VisuMZ.BattleSystemFTB || {};
VisuMZ.BattleSystemFTB.version = 1.02;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.02] [BattleSystemFTB]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Battle_System_-_FTB_VisuStella_MZ
 * @base VisuMZ_0_CoreEngine
 * @base VisuMZ_1_BattleCore
 * @base VisuMZ_1_ItemsEquipsCore
 * @base VisuMZ_1_SkillsStatesCore
 * @orderAfter VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_ItemsEquipsCore
 * @orderAfter VisuMZ_1_SkillsStatesCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * Free Turn Battle (FTB) is a type of battle system made for RPG Maker MZ,
 * where the teams for actors and enemies take turns attacking one another as
 * a whole. During each team's turns, an action count is given to them and they
 * can freely perform actions among their teammates as wanted (or if turned off
 * by the Plugin Parameters, in a cycle). When the action count is depleted or
 * if one team ran out of battler's that can act, the other team begins their
 * turn and so forth.
 * 
 * *NOTE* To use this battle system, you will need the updated version of
 * VisuStella's Core Engine. Go into its Plugin Parameters and change the
 * "Battle System" plugin parameter to "ftb".
 *
 * Features include all (but not limited to) the following:
 * 
 * * Actor and enemy teams take turns attacking each other as a whole.
 * * Action counts are given to each team at the start of each turn to utilize
 *   actions for.
 * * If enabled, actors can be freely switched around to perform actions with.
 * * Alter the mechanics of the Battle System FTB to your liking through the
 *   Plugin Parameters.
 * * An Action Count Display is shown for each side to relay information to the
 *   player about the current state of each turn.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_0_CoreEngine
 * * VisuMZ_1_BattleCore
 * * VisuMZ_1_ItemsEquipsCore
 * * VisuMZ_1_SkillsStatesCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 * 
 * Surprise Attacks and Preemptive Bonuses
 * 
 * Due to the nature of a team-based battle system, surprise attacks and
 * preemptive bonuses no longer prevent the other team from being able to act
 * for a turn as that gives the initiating team too much advantage. Instead,
 * a surprise attack means the enemy team will always start first for each turn
 * while a preemptive bonus means the actor team will always start first for
 * each turn.
 * 
 * ---
 * 
 * Agility and Speed
 * 
 * When there is no surprise attack or preemptive bonus, aka a neutral battle
 * initiative, then the team that goes first is determined by their Agility
 * value at the start of battle (unless determined otherwise through the Plugin
 * Parameters).
 * 
 * However, because of the nature of team-based battle systems, agility and
 * speed have no impact on action speeds as action speeds are now instantly
 * performed.
 * 
 * Agility, however, can influence Action Counts through buffs and debuffs if
 * enabled through the Plugin Parameters. Each stack of Agility buffs will
 * raise the Action Count for a team while each stack of Agility debuffs will
 * decrease them for subsequent turns.
 * 
 * ---
 * 
 * Action Orders
 * 
 * As team-based battle systems always have teams go between each other, the
 * standard action orders seen for turn-based and tick-based battle systems no
 * longer exist. However, in the event the actor team has berserk, confused, or
 * autobattlers, the actions will be performed in the following order:
 * 
 * 1. Berserk, confused, and auto battlers go first.
 * 2. If any actions are left, inputtable actors go next.
 * 3. If any actions are left, but there are no inputtable actors, berserk,
 *    confused, and auto battlers use up the remaining actions.
 * 4. Switch to the next team.
 * 
 * For enemy teams, enemies will always go in order from left-to-right for the
 * front view or right-to-left for sideview. If there are actions left, the
 * enemy team will cycle back to the first acting enemy.
 * 
 * ---
 * 
 * Free Range Switching
 * 
 * If this is enabled (it's an optional feature) and it's the player's turn,
 * the player can freely switch between actors in his/her party by pressing the
 * left/right buttons or the page up/page down buttons. The Actor Command
 * Window will automatically update to the newly selected actor. This gives the
 * player complete control and freedom over the party and the party's actions.
 * 
 * For touch controls, instead of pressing left/right or page up/page down on
 * the keyboard, click on the Battle Status Window for the target actor to be
 * selected to perform an action. The Actor Command Window will automatically
 * update to the newly selected actor.
 * 
 * ---
 *
 * Turn Structure
 * 
 * Each battle turn is dedicated to one team or the other. You need to design
 * your turns with this in mind. When one team finishes its actions, the next
 * turn will have the other team perform theirs.
 * 
 * As a result, both teams will not benefit from their turn end activities such
 * as regeneration at the end of each battle turn. Instead, they will only
 * occur at the end of their own respective turns.
 * 
 * However, for states and buffs, this is slightly different. States and buffs
 * update at the end of the opposing team's turn. This is so that 1 turn states
 * like Guard will last until the opponent's turn is over instead of being over
 * immediately after the player's turn ends (rendering the effect useless).
 * 
 * The state and buff turn updates can be disabled in the Plugin Parameters.
 * However, the durations must be accounted for if disabled (ie. making Guard
 * last two turns instead of 1).
 * 
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === General FTB-Related Notetags ===
 * 
 * These notetags are general purpose notetags that have became available
 * through this plugin.
 * 
 * ---
 * 
 * <FTB Help>
 *  description
 *  description
 * </FTB Help>
 *
 * - Used for: Skill, Item Notetags
 * - If your game happens to support the ability to change battle systems, this
 *   notetag lets you change how the skill/item's help description text will
 *   look under FTB.
 * - This is primarily used if the skill behaves differently in FTB versus any
 *   other battle system.
 * - Replace 'description' with help text that's only displayed if the game's
 *   battle system is set to FTB.
 *
 * ---
 * 
 * === Action Cost-Related Notetags ===
 * 
 * ---
 *
 * <FTB Action Cost: x>
 *
 * - Used for: Skill, Item Notetags
 * - Changes the FTB action cost of this skill/item to 'x'.
 * - Replace 'x' with a number value representing the action cost required to
 *   perform the skill.
 *
 * ---
 *
 * <FTB Hide Action Cost>
 *
 * - Used for: Skill, Item Notetags
 * - Makes the FTB action cost for this skill/item hidden.
 *
 * ---
 * 
 * === Mechanics-Related Notetags ===
 * 
 * ---
 *
 * <FTB Pass Turn>
 *
 * - Used for: Skill, Item Notetags
 * - If a battler uses this skill/item, then even if there are actions left for
 *   the team to perform, that battler would no longer be able to input as they
 *   have already passed their turn.
 * - By default, this applies to "Guard". If you don't want it to apply to the
 *   Guard skill, turn it off in the Plugin Parameters for mechanics.
 *
 * ---
 *
 * <FTB Actions: +x>
 * <FTB Actions: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Battlers associated with these trait objects can increase or decrease the
 *   maximum number of actions performed each turn.
 * - Replace 'x' with a number representing the increase or decrease in action
 *   count per turn.
 * - Depending on the Plugin Parameters, altering the max value can result in
 *   gaining or losing remaining actions for the current turn.
 *
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === System Plugin Commands ===
 * 
 * ---
 *
 * System: FTB Action Count Visibility
 * - Determine the visibility of the FTB Action Count Display.
 *
 *   Visibility:
 *   - Changes the visibility of the FTB Action Count Display.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * Determines the general settings of the FTB Battle System. These settings
 * range from determining how the Action Count resources and costs are
 * displayed to the text that appear during team shifting.
 *
 * ---
 *
 * Action Counts
 * 
 *   Full Name:
 *   - What is the full name of "Action Counts" in your game?
 * 
 *   Abbreviation:
 *   - What is the abbreviation of "Action Counts" in your game?
 * 
 *   Cost Format:
 *   - How are Action Count costs displayed?
 *   - %1 - Cost, %2 - Abbr Text, %3 - Icon
 * 
 * ---
 * 
 * Icons
 * 
 *   Actor Action Icon:
 *   - What icon is used to represent actor actions?
 * 
 *   Enemy Action Icon:
 *   - What icon is used to represent enemy actions?
 * 
 *   Empty Action Icon:
 *   - What icon is used to represent empty actions?
 *
 * ---
 *
 * Team Shift
 * 
 *   Party's Turn:
 *   - Text that appears when it's the party's turn.
 *   - %1 - Party Name
 * 
 *   Enemy's Turn:
 *   - Text that appears when it's the enemy's turn.
 * 
 *   Wait Frames:
 *   - How many frames to wait in between team changes?
 *
 * ---
 *
 * Displayed Costs
 * 
 *   Cost Position Front?:
 *   - Put the action cost at the front of skill/item costs?
 * 
 *   Show Cost: Attack:
 *   - Show the action cost for the Attack command?
 * 
 *   Show Cost: Guard:
 *   - Show the action cost for the Guard command?
 * 
 *   Show Cost: 0 Action:
 *   - Show the action cost when the cost is 0 action?
 * 
 *   Show Cost: 1 Action:
 *   - Show the action cost when the cost is 1 action?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * Determines the mechanics of the FTB Battle System. From here, you can
 * enable or disable core mechanics, determine how to determine turn advantage,
 * and how the various supporting mechanics operate.
 *
 * ---
 *
 * Main Mechanics
 * 
 *   Enable Free Switch?:
 *   - Enable free range switching between actors?
 * 
 *   Maintain Same Actor?:
 *   - Maintain the same actor after an action?
 *   - Or move onto the next available actor?
 * 
 *   Guard > Pass Turn?:
 *   - Does guarding cause a battler to pass turn?
 * 
 *   Gain Differences?:
 *   - If the max Action Count for a team changes, gain the difference in value
 *     if positive?
 * 
 *   Lose Differences?:
 *   - If the max Action Count for a team changes, lose the difference in value
 *     if negative?
 * 
 *   State/Buff Updates:
 *   - If enabled, update state/buff turns only on opponent turns.
 *   - Otherwise, they occur every turn.
 *
 * ---
 *
 * Turn Advantage
 * 
 *   Neutral Advantage:
 *   - For a neutral advantage battle, what determines which team goes first?
 *     - Random - 50% chance on which team goes first
 *     - Player - Player's team always goes first.
 *     - Lowest AGI - Battler with lowest AGI's team goes first
 *     - Average AGI - Team with the highest average AGI goes first
 *     - Highest AGI - Battler with highest AGI's team goes first
 *     - Total AGI - Team with highest total AGI goes first
 *
 * ---
 *
 * Action Generation
 * 
 *   Base:
 *   - What is the starting base number of actions that are generated per
 *     battler each turn?
 * 
 *   AGI Buff Influence?:
 *   - Do AGI buffs give +1 for each stack?
 * 
 *   AGI Debuff Influence?:
 *   - Do AGI debuffs give -1 for each stack?
 * 
 *   Maximum Actions:
 *   - What is the absolute maximum number of actions a team can have
 *     each turn?
 * 
 *   Minimum Actions:
 *   - What is the bare minimum number of actions a team can have each turn?
 * 
 *   Allow Overflow?:
 *   - Allow current actions to overflow?
 *   - Or let them cap at the current team max?
 *
 * ---
 *
 * Default Action Costs
 * 
 *   Skills:
 *   - What is the default action cost for skills?
 * 
 *   Items:
 *   - What is the default action cost for items?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Action Count Display Settings
 * ============================================================================
 *
 * Adjust the settings for the Action Count Display. They appear in the upper
 * or lower corners of the screen for the player party and the enemy troop.
 *
 * ---
 *
 * Display Settings
 * 
 *   Draw Horizontally?:
 *   - Which direction do you want the Action Count Display to go?
 * 
 *   Bottom Position?:
 *   - Place the Action Count Display towards the bottom of the screen?
 * 
 *     Offset Top Log Y?:
 *     - If using the top position, offset the log window's Y position.
 * 
 *     Reposition for Help?:
 *     - If using the top position, reposition the display when the help window
 *       is open?
 *
 * ---
 *
 * Reposition For Help
 * 
 *   Repostion X By:
 *   Repostion Y By:
 *   - Reposition the display's X/Y coordinates by this much when the
 *     Help Window is visible.
 *
 * ---
 *
 * Picture Settings
 * 
 *   Actor Action Picture:
 *   Enemy Action Picture:
 *   Empty Action Picture:
 *   - Optional. Place an image for an actor, enemy, or empty action instead of
 *     an icon?
 *
 * ---
 *
 * Coordinates
 * 
 *   Screen Buffer X:
 *   Screen Buffer Y:
 *   - Buffer from the the edge of the screen's X/Y by this much.
 * 
 *   Actor Offset X:
 *   Actor Offset Y:
 *   Enemy Offset X:
 *   Enemy Offset Y:
 *   - Offset the actor/enemy images' X/Y by this much.
 *
 * ---
 *
 * Draw Settings
 * 
 *   Max Actions Visible:
 *   - How many action slots max should be drawn for each team?
 * 
 *   Image Size:
 *   - What is the size of the icons or pictures for the action slots?
 * 
 *   Gap Distance:
 *   - How wide should the gab between each slot be in pixels?
 * 
 *   Icon Smoothing?:
 *   - Smooth the display for icons?
 *   - Or pixelate them?
 * 
 *   Picture Smoothing?:
 *   - Smooth the display for pictures?
 *   - Or pixelate them?
 *
 * ---
 *
 * Turns Remaining
 * 
 *   Show Number?:
 *   - Show a number to display the actions remaining?
 * 
 *   Font Size:
 *   - What font size should be used for this number?
 * 
 *   Offset X:
 *   Offset Y:
 *   - Offset the remaining actions number X/Y.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.02: April 2, 2021
 * * Bug Fixes!
 * ** Action costs for FTP will now only take effect if inside battle only.
 *    Fix made by Olivia.
 * 
 * Version 1.01: March 19, 2021
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.00 Official Release Date: February 22, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemActionCountVisibility
 * @text System: FTB Action Count Visibility
 * @desc Determine the visibility of the FTB Action Count Display.
 *
 * @arg Visible:eval
 * @text Visibility
 * @type boolean
 * @on Visible
 * @off Hidden
 * @desc Changes the visibility of the FTB Action Count Display.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BattleSystemFTB
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param General:struct
 * @text General Settings
 * @type struct<General>
 * @desc Determines the general settings of the FTB Battle System.
 * @default {"ActionCounts":"","ActionCountFull:str":"Fight Points","ActionCountAbbr:str":"FP","ActionCountCostFmt:str":"\\FS[22]\\C[0]×%1%3\\C[0]","Icons":"","ActorActionsIcon:num":"165","EnemyActionsIcon:num":"162","EmptyActionsIcon:num":"161","TeamShift":"","PartyTeamShiftFmt:str":"%1's Turn!","TroopTeamShiftFmt:str":"Opponent's Turn!","TeamShiftWait:num":"60","DisplayedCosts":"","CostPosition:eval":"false","ShowCostForAttack:eval":"false","ShowCostForGuard:eval":"false","Show_0_Action_Cost:eval":"true","Show_1_Action_Cost:eval":"true"}
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Determines the mechanics of the FTB Battle System.
 * @default {"Main":"","FreeChange:eval":"true","KeepPrevActor:eval":"true","GuardPass:eval":"true","GainDiff:eval":"true","LoseDiff:eval":"false","StateBuffUpdate:eval":"true","TurnAdvantage":"","NeutralAdvantage:str":"average agi","ActionGeneration":"","GenerateBase:num":"1","AgiBuff:eval":"true","AgiDebuff:eval":"false","MaxActions:num":"99","MinActions:num":"1","AllowOverflow:eval":"false","DefaultCost":"","DefaultCostSkill:num":"1","DefaultCostItem:num":"1"}
 *
 * @param ActionCountDisplay:struct
 * @text Action Count Display
 * @type struct<ActionCountDisplay>
 * @desc Adjust the settings for the Action Count Display.
 * @default {"Display":"","DrawHorz:eval":"true","BottomPosition:eval":"true","LogWindowTopOffsetY:num":"40","RepositionTopForHelp:eval":"true","Reposition":"","RepositionTopHelpX:num":"0","RepositionTopHelpY:num":"160","Pictures":"","ActorActionPicture:str":"","EnemyActionPicture:str":"","EmptyActionPicture:str":"","Coordinates":"","ScreenBufferX:num":"16","ScreenBufferY:num":"16","ActorOffsetX:num":"0","ActorOffsetY:num":"0","EnemyOffsetX:num":"0","EnemyOffsetY:num":"0","DrawSettings":"","MaxVisible:num":"10","ImageSize:num":"32","ImageGapDistance:num":"2","IconSmoothing:eval":"false","PictureSmoothing:eval":"true","TurnsRemaining":"","DrawActionsRemaining:eval":"true","ActionsRemainingFontSize:num":"26","ActionsRemainingOffsetX:num":"0","ActionsRemainingOffsetY:num":"0"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * General Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~General:
 *
 * @param ActionCounts
 * @text Action Counts
 *
 * @param ActionCountFull:str
 * @text Full Name
 * @parent ActionCounts
 * @desc What is the full name of "Action Counts" in your game?
 * @default Fight Points
 *
 * @param ActionCountAbbr:str
 * @text Abbreviation
 * @parent ActionCounts
 * @desc What is the abbreviation of "Action Counts" in your game?
 * @default FP
 *
 * @param ActionCountCostFmt:str
 * @text Cost Format
 * @parent ActionCounts
 * @desc How are Action Count costs displayed?
 * %1 - Cost, %2 - Abbr Text, %3 - Icon
 * @default \FS[22]\C[0]×%1%3\C[0]
 *
 * @param Icons
 *
 * @param ActorActionsIcon:num
 * @text Actor Action Icon
 * @parent Icons
 * @desc What icon is used to represent actor actions?
 * @default 165
 *
 * @param EnemyActionsIcon:num
 * @text Enemy Action Icon
 * @parent Icons
 * @desc What icon is used to represent enemy actions?
 * @default 162
 *
 * @param EmptyActionsIcon:num
 * @text Empty Action Icon
 * @parent Icons
 * @desc What icon is used to represent empty actions?
 * @default 161
 *
 * @param TeamShift
 * @text Team Shift
 *
 * @param PartyTeamShiftFmt:str
 * @text Party's Turn
 * @parent TeamShift
 * @desc Text that appears when it's the party's turn.
 * %1 - Party Name
 * @default %1's Turn!
 *
 * @param TroopTeamShiftFmt:str
 * @text Enemy's Turn
 * @parent TeamShift
 * @desc Text that appears when it's the enemy's turn.
 * @default Opponent's Turn!
 *
 * @param TeamShiftWait:num
 * @text Wait Frames
 * @parent TeamShift
 * @type number
 * @desc How many frames to wait in between team changes?
 * @default 60
 *
 * @param DisplayedCosts
 * @text Displayed Costs
 *
 * @param CostPosition:eval
 * @text Cost Position Front?
 * @parent DisplayedCosts
 * @type boolean
 * @on Front
 * @off Back
 * @desc Put the action cost at the front of skill/item costs?
 * @default false
 *
 * @param ShowCostForAttack:eval
 * @text Show Cost: Attack
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the action cost for the Attack command?
 * @default false
 *
 * @param ShowCostForGuard:eval
 * @text Show Cost: Guard
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the action cost for the Guard command?
 * @default false
 *
 * @param Show_0_Action_Cost:eval
 * @text Show Cost: 0 Action
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the action cost when the cost is 0 action?
 * @default true
 *
 * @param Show_1_Action_Cost:eval
 * @text Show Cost: 1 Action
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the action cost when the cost is 1 action?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param Main
 * @text Main Mechanics
 *
 * @param FreeChange:eval
 * @text Enable Free Switch?
 * @parent Main
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enable free range switching between actors?
 * @default true
 *
 * @param KeepPrevActor:eval
 * @text Maintain Same Actor?
 * @parent Main
 * @type boolean
 * @on Maintain
 * @off Next Available
 * @desc Maintain the same actor after an action?
 * Or move onto the next available actor?
 * @default true
 *
 * @param GuardPass:eval
 * @text Guard > Pass Turn?
 * @parent Main
 * @type boolean
 * @on Pass Turn
 * @off Don't Pass
 * @desc Does guarding cause a battler to pass turn?
 * @default true
 *
 * @param GainDiff:eval
 * @text Gain Differences?
 * @parent Main
 * @type boolean
 * @on Gain Differences
 * @off Keep Same
 * @desc If the max Action Count for a team changes,
 * gain the difference in value if positive?
 * @default true
 *
 * @param LoseDiff:eval
 * @text Lose Differences?
 * @parent Main
 * @type boolean
 * @on Lose Differences
 * @off Keep Same
 * @desc If the max Action Count for a team changes,
 * lose the difference in value if negative?
 * @default false
 *
 * @param StateBuffUpdate:eval
 * @text State/Buff Updates
 * @parent Main
 * @type boolean
 * @on Opponent Turns Only
 * @off All Turns
 * @desc If enabled, update state/buff turns only on opponent
 * turns. Otherwise, they occur every turn.
 * @default true
 *
 * @param TurnAdvantage
 * @text Turn Advantage
 *
 * @param NeutralAdvantage:str
 * @text Neutral Advantage
 * @parent TurnAdvantage
 * @type select
 * @option Random - 50% chance on which team goes first
 * @value random
 * @option Player - Player's team always goes first
 * @value player
 * @option Enemy - Enemy's team always goes first
 * @value enemy
 * @option Lowest AGI - Battler with lowest AGI's team goes first
 * @value lowest agi
 * @option Average AGI - Team with the highest average AGI goes first
 * @value average agi
 * @option Highest AGI - Battler with highest AGI's team goes first
 * @value highest agi
 * @option Total AGI - Team with highest total AGI goes first
 * @value total agi
 * @desc For a neutral advantage battle, what determines which team goes first?
 * @default average agi
 *
 * @param ActionGeneration
 * @text Action Generation
 *
 * @param GenerateBase:num
 * @text Base
 * @parent ActionGeneration
 * @type number
 * @desc What is the starting base number of actions that are generated per battler each turn?
 * @default 1
 *
 * @param AgiBuff:eval
 * @text AGI Buff Influence?
 * @parent ActionGeneration
 * @type boolean
 * @on Influence
 * @off No Influence
 * @desc Do AGI buffs give +1 for each stack?
 * @default true
 *
 * @param AgiDebuff:eval
 * @text AGI Debuff Influence?
 * @parent ActionGeneration
 * @type boolean
 * @on Influence
 * @off No Influence
 * @desc Do AGI debuffs give -1 for each stack?
 * @default false
 *
 * @param MaxActions:num
 * @text Maximum Actions
 * @parent ActionGeneration
 * @type number
 * @desc What is the absolute maximum number of actions a team can have each turn?
 * @default 99
 *
 * @param MinActions:num
 * @text Minimum Actions
 * @parent ActionGeneration
 * @type number
 * @desc What is the bare minimum number of actions a team can have each turn?
 * @default 1
 *
 * @param AllowOverflow:eval
 * @text Allow Overflow?
 * @parent ActionGeneration
 * @type boolean
 * @on Allow
 * @off Cap to Max
 * @desc Allow current actions to overflow?
 * Or let them cap at the current team max?
 * @default false
 *
 * @param DefaultCost
 * @text Default Action Costs
 *
 * @param DefaultCostSkill:num
 * @text Skills
 * @parent DefaultCost
 * @type number
 * @desc What is the default action cost for skills?
 * @default 1
 *
 * @param DefaultCostItem:num
 * @text Items
 * @parent DefaultCost
 * @type number
 * @desc What is the default action cost for items?
 * @default 1
 * 
 */
/* ----------------------------------------------------------------------------
 * Action Count Display Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ActionCountDisplay:
 *
 * @param Display
 * @text Display Settings
 *
 * @param DrawHorz:eval
 * @text Draw Horizontally?
 * @parent Display
 * @type boolean
 * @on Horizontal
 * @off Vertical
 * @desc Which direction do you want the Action Count Display to go?
 * @default true
 *
 * @param BottomPosition:eval
 * @text Bottom Position?
 * @parent Display
 * @type boolean
 * @on Bottom
 * @off Top
 * @desc Place the Action Count Display towards the bottom of the screen?
 * @default true
 *
 * @param LogWindowTopOffsetY:num
 * @text Offset Top Log Y?
 * @parent BottomPosition:eval
 * @type number
 * @desc If using the top position, offset the log window's Y position.
 * @default 40
 *
 * @param RepositionTopForHelp:eval
 * @text Reposition for Help?
 * @parent BottomPosition:eval
 * @type boolean
 * @on Reposition
 * @off Stay
 * @desc If using the top position, reposition the display when the help window is open?
 * @default true
 *
 * @param Reposition
 * @text Reposition For Help
 *
 * @param RepositionTopHelpX:num
 * @text Repostion X By
 * @parent Reposition
 * @desc Reposition the display's X coordinates by this much when
 * the Help Window is visible.
 * @default 0
 *
 * @param RepositionTopHelpY:num
 * @text Repostion Y By
 * @parent Reposition
 * @desc Reposition the display's Y coordinates by this much when
 * the Help Window is visible.
 * @default 160
 *
 * @param Pictures
 * @text Picture Settings
 *
 * @param ActorActionPicture:str
 * @text Actor Action Picture
 * @parent Pictures
 * @type file
 * @dir img/pictures/
 * @desc Optional. Place an image for an actor action instead of an icon?
 * @default 
 *
 * @param EnemyActionPicture:str
 * @text Enemy Action Picture
 * @parent Pictures
 * @type file
 * @dir img/pictures/
 * @desc Optional. Place an image for an enemy action instead of an icon?
 * @default 
 *
 * @param EmptyActionPicture:str
 * @text Empty Action Picture
 * @parent Pictures
 * @type file
 * @dir img/pictures/
 * @desc Optional. Place an image for an empty action instead of an icon?
 * @default 
 *
 * @param Coordinates
 *
 * @param ScreenBufferX:num
 * @text Screen Buffer X
 * @parent Coordinates
 * @desc Buffer from the the edge of the screen's X by this much.
 * @default 16
 *
 * @param ScreenBufferY:num
 * @text Screen Buffer Y
 * @parent Coordinates
 * @desc Buffer from the the edge of the screen's Y by this much.
 * @default 16
 *
 * @param ActorOffsetX:num
 * @text Actor Offset X
 * @parent Coordinates
 * @desc Offset the actor images' X by this much.
 * @default 0
 *
 * @param ActorOffsetY:num
 * @text Actor Offset Y
 * @parent Coordinates
 * @desc Offset the actor images' Y by this much.
 * @default 0
 *
 * @param EnemyOffsetX:num
 * @text Enemy Offset X
 * @parent Coordinates
 * @desc Offset the enemy images' X by this much.
 * @default 0
 *
 * @param EnemyOffsetY:num
 * @text Enemy Offset Y
 * @parent Coordinates
 * @desc Offset the enemy images' Y by this much.
 * @default 0
 *
 * @param DrawSettings
 * @text Draw Settings
 *
 * @param MaxVisible:num
 * @text Max Actions Visible
 * @parent DrawSettings
 * @desc How many action slots max should be drawn for each team?
 * @default 10
 *
 * @param ImageSize:num
 * @text Image Size
 * @parent DrawSettings
 * @desc What is the size of the icons or pictures for the action slots?
 * @default 32
 *
 * @param ImageGapDistance:num
 * @text Gap Distance
 * @parent DrawSettings
 * @desc How wide should the gab between each slot be in pixels?
 * @default 2
 *
 * @param IconSmoothing:eval
 * @text Icon Smoothing?
 * @parent DrawSettings
 * @type boolean
 * @on Smooth
 * @off Pixelate
 * @desc Smooth the display for icons?
 * Or pixelate them?
 * @default false
 *
 * @param PictureSmoothing:eval
 * @text Picture Smoothing?
 * @parent DrawSettings
 * @type boolean
 * @on Smooth
 * @off Pixelate
 * @desc Smooth the display for pictures?
 * Or pixelate them?
 * @default true
 *
 * @param TurnsRemaining
 * @text Turns Remaining
 *
 * @param DrawActionsRemaining:eval
 * @text Show Number?
 * @parent TurnsRemaining
 * @type boolean
 * @on Show Number
 * @off Don't Show
 * @desc Show a number to display the actions remaining?
 * @default true
 *
 * @param ActionsRemainingFontSize:num
 * @text Font Size
 * @parent DrawActionsRemaining:eval
 * @desc What font size should be used for this number?
 * @default 26
 *
 * @param ActionsRemainingOffsetX:num
 * @text Offset X
 * @parent DrawActionsRemaining:eval
 * @desc Offset the remaining actions number X.
 * @default 0
 *
 * @param ActionsRemainingOffsetY:num
 * @text Offset Y
 * @parent DrawActionsRemaining:eval
 * @desc Offset the remaining actions number Y.
 * @default 0
 *
 */
//=============================================================================

const _0x31e7=['_FTB_ACTION_AGI_BUFF','center','ItemQuantityFontSize','reduceActionsFTB','Game_BattlerBase_appear','battleSys','fontSize','STR','performTurnEndFTB','keepPrevSubjectFTB','Empty','_FTB_GUARD_PASS','MaxActions','updateStateTurns','onTurnEnd','IconSet','_currentActor','hitIndex','waitCount','endTurn','_FTB_ACTION_BASE','startDamagePopup','removeStatesAuto','ftbActionPointsFull','_context','description','_FTB_KEEP_PREV_ACTOR','makeActionOrders','_FTB_MAX_ACTIONS','setUnit','recalculateActionsFTB','StateBuffUpdate','releaseUnequippableItems','126008ikGWDK','Enemy','isSkill','repositionLogWindowFTB','NUM','BattleManager_startTurn','_doubleTouch','random','canMove','Nothing','meetEndTurnConditionsFTB','RegExp','addChildAt','removeBuff','hide','55973zdeqeU','initMembers','Window_Base_makeAdditionalSkillCostText','height','_FTB_ACTION_OVERFLOW','_FTB_COST_SHOW_0','General','_FTB_FREE_CHANGE','Window_Selectable_cursorPageup','ftbSwitchActorDirection','drawItemNumber','MaxVisible','Window_Selectable_cursorPagedown','match','FreeChange','iconHeight','BattleManager_startBattle','setMaxActionsFTB','ActionCountAbbr','DefaultCostSkill','aliveMembers','13347lTxDof','KeepPrevActor','_inputting','setCurrentActionsFTB','Actor','Game_BattlerBase_hide','Game_Battler_addDebuff','createStartingCoordinates','ftb%1ActionsIcon','appear','clear','TeamShiftWait','_logWindow','changeEquipById','registerCommand','initialize','isBattleSystemFTBActionCountVisible','CostPosition','BattleManager_endAction','ActionsRemainingOffsetY','canDrawActionsRemaining','_FTB_NEUTRAL_TURN_ADVANTAGE','Game_Actor_changeEquip','isDrawItemNumber','ftbCreateTeamSwitchText','note','startInputFTB','_ftbTroopActionCountWindow','battler','ActionsRemainingOffsetX','padding','2433WFzRkI','endTurnFTB','BattleManager_isTpb','Game_Actor_forceChangeEquip','processTouch','randomInt','skillCostSeparator','BattleManager_battleSys','EnemyActionPicture','LogWindowTopOffsetY','EnemyActionsIcon','makeActionOrdersFTB','startTurn','screenX','addText','_scene','_currentActions','Window_Base_drawItemNumber','index','Game_System_initialize','startTurnFTB','constructor','%1ActionPicture','refresh','createContentsArray','loseCurrentActionsFTB','speed','setup','innerHeight','_actorCommandWindow','startActorCommandSelection','AgiDebuff','ftbHighestAgility','floor','Game_BattlerBase_canUse','Settings','Game_Battler_useItem','startActorInput','trim','addState','BattleManager_makeActionOrders','updateVisibility','Game_Actor_changeClass','DefaultCostItem','imageSmoothingEnabled','unshift','finishActorInput','ftbActionPointsAbbr','members','isOpen','initMembersFTB','ARRAYFUNC','ARRAYSTR','inBattle','canActFTB','subject','BattleManager_processTurn','Game_BattlerBase_clearStates','startInput','textSizeEx','cursorPageup','processTurn','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','Game_Action_applyGlobal','drawImage','createActionsFTB','isFTB','Game_Battler_performCollapse','_subject','_forceAction','maxCols','isPassingTurnFTB','ARRAYNUM','selectNextActorFTB','performCollapse','getMaxActionsFTB','friendsUnit','active','currentAction','battleMembers','windowRect','\x5cI[%1]','ScreenBufferY','Game_Actor_releaseUnequippableItems','clearStates','total\x20agi','isPartyCommandWindowDisabled','AllowOverflow','GainDiff','max','Window_Selectable_cursorRight','cursorRight','updateBuffTurns','playCursorSound','_ftbTeamEven','loadPicture','return\x200','_statusWindow','gainCurrentActionsFTB','PictureSmoothing','endActionFTB','Game_Battler_removeBuff','applyGlobal','format','toUpperCase','concat','_partyCommandWindow','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','getActionCostFTB','player','_FTB_MIN_ACTIONS','_storedBitmaps','exit','Scene_Battle_createAllWindows','selectNextCommand','removeState','Scene_Battle_createActorCommandWindow','createAllWindows','agi','payActionCostFTB','getBattleSystem','resetFontSettings','Game_BattlerBase_updateStateTurns','drawText','actors','sort','createActorCommandWindow','_buffs','ftbCostFormat','setText','getCurrentActionsFTB','commandFight','Game_Actor_discardEquip','BattleManager_finishActorInput','EmptyActionsIcon','prototype','battleEnd','Game_Battler_addState','_ftbActionsCur','cursorLeft','_actionBattlers','makeActions','ScreenBufferX','Window_Selectable_processTouch','map','ftbPartyTeamShift','update','_maxActions','_forcedBattlers','setBattleSystemFTBActionCountVisible','_ftbTurnAdvantageUnit','RepositionTopHelpY','push','_bypassStateTurnUpdatesFTB','endAction','ftbFreeRangeSwitch','Visible','attackSkillId','drawTextEx','LoseDiff','Window_Selectable_cursorLeft','ftbEnemyActionsIcon','createActionCountWindowsFTB','ActorActionPicture','drawActionsRemaining','DrawHorz','ARRAYEVAL','EVAL','7822jKNSod','isSideView','ActorOffsetX','drawBigIcon','_ftbCurrentUnit','iconWidth','transform','enemies','item','cursorPagedown','ItemScene','contents','Game_Action_speed','makeAdditionalCostTextFTB','addBuff','HideActionPointCost','filter','round','_FTB_RECALC_SUB_DIFF','409322NQoNoJ','removeActionBattlersFTB','guardSkillId','_FTB_COST_SHOW_GUARD','call','changeClass','stepForward','Window_Help_setItem','isItem','BattleManager_isActiveTpb','STRUCT','_passedTurnFTB','processTurnFTB','ShowCostForAttack','ftbLowestAgility','blt','ImageSize','some','87CzborH','endAllBattlersTurn','create','parameters','addDebuff','isActor','Game_Battler_addBuff','getChildIndex','version','processTouchFTB','getNextSubject','clearPassTurnFTB','JSON','canUse','isTpb','_action','select','Game_Battler_removeState','makeAdditionalSkillCostText','Game_Enemy_transform','canInput','turnCount','bind','_ftbTeamOdd','BattleManager_selectNextActor','_FTB_COST_POSITION','agility','commandCancel','IconSmoothing','min','setBackgroundType','indexOf','textWidth','changeEquip','Game_BattlerBase_updateBuffTurns','_surprise','checkNeedsUpdate','isActiveTpb','initBattleSystemFTB','updatePadding','ARRAYJSON','_FTB_STATE_BUFF_TURN_UPDATES_ONLY_ON_OPPONENT_TURNS','MinActions','Game_Actor_changeEquipById','applyGlobalFTB','innerWidth','ftbEmptyActionsIcon','EmptyActionPicture','ActionCountFull','width','passTurnFTB','close','isSceneBattle','reduce','ConvertParams','enemy','FTB','canActorBeSelectedFTB','_helpWindow','startBattle','_ftbActionCountVisible','1wBLNQz','setItem','createActorCommandWindowFTB','_ftbActionsMax','forceChangeEquip','BottomPosition','_unit','stepBack','highest\x20agi','BattleSystemFTB','_FTB_COST_SHOW_ATTACK','clearBuffs','Mechanics','Current','ActionCountCostFmt','name','NeutralAdvantage','_FTB_ACTION_AGI_DEBUFF','BattleManager_endTurn','ActionPointCost','processSwitchActors','BattleManager_endAllBattlersTurn','length','isTriggered','drawItemNumberFTB','ImageGapDistance','_FTB_COST_SHOW_1','parse','RepositionTopForHelp','Game_Battler_onTurnEnd','ftbActorActionsIcon','clamp','loadSystem','181933jUBDIE','ftbTotalAgility','Scene_Battle_commandFight','startBattleFTB','commandCancelFTB','_ftbPartyActionCountWindow','updatePosition','BattleManager_isTurnBased','addLoadListener','ftbActionCount','Game_Actor_selectNextCommand','lowest\x20agi'];const _0x329e=function(_0x3dd709,_0x40a0fe){_0x3dd709=_0x3dd709-0x1cd;let _0x31e76f=_0x31e7[_0x3dd709];return _0x31e76f;};const _0x2deec5=_0x329e;(function(_0xcbd0c3,_0x307b84){const _0x1f3847=_0x329e;while(!![]){try{const _0x19f100=parseInt(_0x1f3847(0x2a5))*-parseInt(_0x1f3847(0x284))+parseInt(_0x1f3847(0x222))+parseInt(_0x1f3847(0x2f6))+parseInt(_0x1f3847(0x2e1))+parseInt(_0x1f3847(0x2d2))+parseInt(_0x1f3847(0x235))+parseInt(_0x1f3847(0x315))*-parseInt(_0x1f3847(0x247));if(_0x19f100===_0x307b84)break;else _0xcbd0c3['push'](_0xcbd0c3['shift']());}catch(_0x325586){_0xcbd0c3['push'](_0xcbd0c3['shift']());}}}(_0x31e7,0x356f4));var label=_0x2deec5(0x28d),tier=tier||0x0,dependencies=[],pluginData=$plugins[_0x2deec5(0x232)](function(_0x50b056){const _0x389cbd=_0x2deec5;return _0x50b056['status']&&_0x50b056[_0x389cbd(0x2ca)]['includes']('['+label+']');})[0x0];VisuMZ[label]['Settings']=VisuMZ[label]['Settings']||{},VisuMZ[_0x2deec5(0x27d)]=function(_0xe82e7e,_0x16afb5){const _0x4968f5=_0x2deec5;for(const _0x24d3ab in _0x16afb5){if(_0x24d3ab[_0x4968f5(0x2ee)](/(.*):(.*)/i)){const _0x13d921=String(RegExp['$1']),_0x853d04=String(RegExp['$2'])[_0x4968f5(0x1e2)]()[_0x4968f5(0x33b)]();let _0x3c51e9,_0x2469b5,_0x3ea813;switch(_0x853d04){case _0x4968f5(0x2d6):_0x3c51e9=_0x16afb5[_0x24d3ab]!==''?Number(_0x16afb5[_0x24d3ab]):0x0;break;case _0x4968f5(0x35d):_0x2469b5=_0x16afb5[_0x24d3ab]!==''?JSON['parse'](_0x16afb5[_0x24d3ab]):[],_0x3c51e9=_0x2469b5[_0x4968f5(0x20a)](_0x40fea0=>Number(_0x40fea0));break;case _0x4968f5(0x221):_0x3c51e9=_0x16afb5[_0x24d3ab]!==''?eval(_0x16afb5[_0x24d3ab]):null;break;case _0x4968f5(0x220):_0x2469b5=_0x16afb5[_0x24d3ab]!==''?JSON['parse'](_0x16afb5[_0x24d3ab]):[],_0x3c51e9=_0x2469b5['map'](_0x5916b7=>eval(_0x5916b7));break;case _0x4968f5(0x253):_0x3c51e9=_0x16afb5[_0x24d3ab]!==''?JSON[_0x4968f5(0x29f)](_0x16afb5[_0x24d3ab]):'';break;case _0x4968f5(0x26f):_0x2469b5=_0x16afb5[_0x24d3ab]!==''?JSON['parse'](_0x16afb5[_0x24d3ab]):[],_0x3c51e9=_0x2469b5['map'](_0x3f368d=>JSON['parse'](_0x3f368d));break;case'FUNC':_0x3c51e9=_0x16afb5[_0x24d3ab]!==''?new Function(JSON[_0x4968f5(0x29f)](_0x16afb5[_0x24d3ab])):new Function(_0x4968f5(0x1da));break;case _0x4968f5(0x348):_0x2469b5=_0x16afb5[_0x24d3ab]!==''?JSON['parse'](_0x16afb5[_0x24d3ab]):[],_0x3c51e9=_0x2469b5[_0x4968f5(0x20a)](_0x141833=>new Function(JSON['parse'](_0x141833)));break;case _0x4968f5(0x2b8):_0x3c51e9=_0x16afb5[_0x24d3ab]!==''?String(_0x16afb5[_0x24d3ab]):'';break;case _0x4968f5(0x349):_0x2469b5=_0x16afb5[_0x24d3ab]!==''?JSON[_0x4968f5(0x29f)](_0x16afb5[_0x24d3ab]):[],_0x3c51e9=_0x2469b5[_0x4968f5(0x20a)](_0x48cbed=>String(_0x48cbed));break;case _0x4968f5(0x23f):_0x3ea813=_0x16afb5[_0x24d3ab]!==''?JSON[_0x4968f5(0x29f)](_0x16afb5[_0x24d3ab]):{},_0x3c51e9=VisuMZ[_0x4968f5(0x27d)]({},_0x3ea813);break;case'ARRAYSTRUCT':_0x2469b5=_0x16afb5[_0x24d3ab]!==''?JSON[_0x4968f5(0x29f)](_0x16afb5[_0x24d3ab]):[],_0x3c51e9=_0x2469b5['map'](_0x526662=>VisuMZ['ConvertParams']({},JSON[_0x4968f5(0x29f)](_0x526662)));break;default:continue;}_0xe82e7e[_0x13d921]=_0x3c51e9;}}return _0xe82e7e;},(_0x5c5b71=>{const _0x59ddf6=_0x2deec5,_0x54df41=_0x5c5b71[_0x59ddf6(0x293)];for(const _0x533e0f of dependencies){if(!Imported[_0x533e0f]){alert(_0x59ddf6(0x1e5)[_0x59ddf6(0x1e1)](_0x54df41,_0x533e0f)),SceneManager[_0x59ddf6(0x1ea)]();break;}}const _0x1b7678=_0x5c5b71['description'];if(_0x1b7678[_0x59ddf6(0x2ee)](/\[Version[ ](.*?)\]/i)){const _0x56dd25=Number(RegExp['$1']);_0x56dd25!==VisuMZ[label][_0x59ddf6(0x24f)]&&(alert('%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.'[_0x59ddf6(0x1e1)](_0x54df41,_0x56dd25)),SceneManager['exit']());}if(_0x1b7678[_0x59ddf6(0x2ee)](/\[Tier[ ](\d+)\]/i)){const _0x3b4f7c=Number(RegExp['$1']);_0x3b4f7c<tier?(alert(_0x59ddf6(0x353)[_0x59ddf6(0x1e1)](_0x54df41,_0x3b4f7c,tier)),SceneManager['exit']()):tier=Math[_0x59ddf6(0x1d3)](_0x3b4f7c,tier);}VisuMZ[_0x59ddf6(0x27d)](VisuMZ[label][_0x59ddf6(0x338)],_0x5c5b71[_0x59ddf6(0x24a)]);})(pluginData),PluginManager[_0x2deec5(0x304)](pluginData[_0x2deec5(0x293)],'SystemActionCountVisibility',_0x589c4c=>{const _0x5dbb62=_0x2deec5;VisuMZ['ConvertParams'](_0x589c4c,_0x589c4c);const _0xdaca04=_0x589c4c[_0x5dbb62(0x216)];$gameSystem[_0x5dbb62(0x20f)](_0xdaca04);}),VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x2dd)]={'ActionPointCost':/<FTB (?:FP|ACTION) COST:[ ](\d+)>/i,'HideActionPointCost':/<FTB HIDE (?:FP|ACTION) COST>/i,'PassTurn':/<FTB PASS TURN>/i,'ActionPointTraitPlus':/<FTB (?:FP|ACTION|ACTIONS):[ ]([\+\-]\d+)>/i},DataManager[_0x2deec5(0x1e6)]=function(_0x32af67){const _0x3adef2=_0x2deec5;if(!_0x32af67)return 0x0;const _0x2a105a=VisuMZ[_0x3adef2(0x28d)][_0x3adef2(0x338)][_0x3adef2(0x290)],_0x2cba2c=VisuMZ[_0x3adef2(0x28d)][_0x3adef2(0x2dd)],_0x2db64=_0x32af67[_0x3adef2(0x30f)];if(_0x2db64[_0x3adef2(0x2ee)](_0x2cba2c[_0x3adef2(0x297)]))return Number(RegExp['$1']);else{if(DataManager[_0x3adef2(0x2d4)](_0x32af67))return _0x2a105a[_0x3adef2(0x2f4)];else return DataManager[_0x3adef2(0x23d)](_0x32af67)?_0x2a105a[_0x3adef2(0x340)]:0x0;}},ImageManager[_0x2deec5(0x2a2)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x2e7)]['ActorActionsIcon'],ImageManager[_0x2deec5(0x21b)]=VisuMZ['BattleSystemFTB']['Settings'][_0x2deec5(0x2e7)][_0x2deec5(0x31f)],ImageManager[_0x2deec5(0x275)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)]['General'][_0x2deec5(0x200)],TextManager[_0x2deec5(0x2c8)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x2e7)][_0x2deec5(0x277)],TextManager[_0x2deec5(0x344)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)]['General'][_0x2deec5(0x2f3)],TextManager[_0x2deec5(0x1fa)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x2e7)][_0x2deec5(0x292)],TextManager['ftbPartyTeamShift']=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)]['General']['PartyTeamShiftFmt'],TextManager['ftbTroopTeamShift']=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x2e7)]['TroopTeamShiftFmt'],SceneManager[_0x2deec5(0x27b)]=function(){const _0x36253b=_0x2deec5;return this[_0x36253b(0x324)]&&this[_0x36253b(0x324)]['constructor']===Scene_Battle;},BattleManager[_0x2deec5(0x2e8)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x2ef)],BattleManager[_0x2deec5(0x2cb)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)]['Mechanics'][_0x2deec5(0x2f7)],BattleManager[_0x2deec5(0x2bc)]=VisuMZ[_0x2deec5(0x28d)]['Settings'][_0x2deec5(0x290)]['GuardPass'],BattleManager['_FTB_RECALC_ADD_DIFF']=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)]['Mechanics'][_0x2deec5(0x1d2)],BattleManager[_0x2deec5(0x234)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x219)],BattleManager[_0x2deec5(0x30b)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x294)],BattleManager['_FTB_BETWEEN_TEAMS_WAIT']=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x2e7)][_0x2deec5(0x301)],BattleManager[_0x2deec5(0x270)]=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x2d0)],VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x31c)]=BattleManager[_0x2deec5(0x2b6)],BattleManager[_0x2deec5(0x2b6)]=function(){const _0x1bca05=_0x2deec5;if(this['isFTB']())return _0x1bca05(0x27f);return VisuMZ[_0x1bca05(0x28d)][_0x1bca05(0x31c)][_0x1bca05(0x239)](this);},BattleManager['isFTB']=function(){const _0x55b663=_0x2deec5;return $gameSystem[_0x55b663(0x1f2)]()===_0x55b663(0x27f);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x317)]=BattleManager[_0x2deec5(0x255)],BattleManager['isTpb']=function(){const _0x24849d=_0x2deec5;if(this['isFTB']())return![];return VisuMZ[_0x24849d(0x28d)][_0x24849d(0x317)][_0x24849d(0x239)](this);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x23e)]=BattleManager[_0x2deec5(0x26c)],BattleManager[_0x2deec5(0x26c)]=function(){const _0x43a6ca=_0x2deec5;if(this[_0x43a6ca(0x357)]())return![];return VisuMZ[_0x43a6ca(0x28d)][_0x43a6ca(0x23e)][_0x43a6ca(0x239)](this);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x2ac)]=BattleManager['isTurnBased'],BattleManager['isTurnBased']=function(){const _0x4c7b69=_0x2deec5;if(this[_0x4c7b69(0x357)]())return!![];return VisuMZ['BattleSystemFTB'][_0x4c7b69(0x2ac)][_0x4c7b69(0x239)](this);},VisuMZ[_0x2deec5(0x28d)]['BattleManager_startInput']=BattleManager[_0x2deec5(0x34f)],BattleManager[_0x2deec5(0x34f)]=function(){const _0x4e7d40=_0x2deec5;if(this[_0x4e7d40(0x357)]())this[_0x4e7d40(0x26a)]=![];VisuMZ['BattleSystemFTB']['BattleManager_startInput'][_0x4e7d40(0x239)](this);if(this[_0x4e7d40(0x357)]()&&$gameParty[_0x4e7d40(0x25b)]())this[_0x4e7d40(0x310)]();},BattleManager[_0x2deec5(0x310)]=function(){const _0x44484e=_0x2deec5;this[_0x44484e(0x321)]();},VisuMZ[_0x2deec5(0x28d)]['BattleManager_processTurn']=BattleManager[_0x2deec5(0x352)],BattleManager[_0x2deec5(0x352)]=function(){const _0x33e2d8=_0x2deec5;this[_0x33e2d8(0x357)]()?this['processTurnFTB']():VisuMZ[_0x33e2d8(0x28d)][_0x33e2d8(0x34d)][_0x33e2d8(0x239)](this);},BattleManager[_0x2deec5(0x241)]=function(){const _0x17d34a=_0x2deec5,_0x4f2404=this[_0x17d34a(0x359)];if(_0x4f2404&&!_0x4f2404[_0x17d34a(0x361)]()[_0x17d34a(0x34b)]())this[_0x17d34a(0x214)](),this[_0x17d34a(0x359)]=null,this['updateTurn'](![]);else{if(_0x4f2404&&_0x4f2404[_0x17d34a(0x24c)]()&&_0x4f2404[_0x17d34a(0x25b)]()){const _0xc74029=_0x4f2404[_0x17d34a(0x363)]();if(!_0xc74029)VisuMZ[_0x17d34a(0x28d)][_0x17d34a(0x34d)][_0x17d34a(0x239)](this);else _0xc74029[_0x17d34a(0x35a)]?VisuMZ['BattleSystemFTB'][_0x17d34a(0x34d)]['call'](this):(this[_0x17d34a(0x2c1)]=_0x4f2404,this[_0x17d34a(0x33a)]());}else VisuMZ[_0x17d34a(0x28d)]['BattleManager_processTurn']['call'](this);}},VisuMZ[_0x2deec5(0x28d)]['BattleManager_finishActorInput']=BattleManager[_0x2deec5(0x343)],BattleManager[_0x2deec5(0x343)]=function(){const _0x2b5dd2=_0x2deec5;this[_0x2b5dd2(0x357)]()?VisuMZ[_0x2b5dd2(0x28d)][_0x2b5dd2(0x34d)]['call'](this):VisuMZ[_0x2b5dd2(0x28d)][_0x2b5dd2(0x1ff)][_0x2b5dd2(0x239)](this);},VisuMZ[_0x2deec5(0x28d)]['BattleManager_selectNextActor']=BattleManager['selectNextActor'],BattleManager['selectNextActor']=function(){const _0xa66aa4=_0x2deec5;this[_0xa66aa4(0x357)]()?this['selectNextActorFTB']():VisuMZ[_0xa66aa4(0x28d)][_0xa66aa4(0x25f)]['call'](this);},BattleManager[_0x2deec5(0x35e)]=function(){const _0x11fdc9=_0x2deec5;this[_0x11fdc9(0x2c1)]=null,this[_0x11fdc9(0x2f8)]=![];},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x308)]=BattleManager[_0x2deec5(0x214)],BattleManager['endAction']=function(){const _0xed474e=_0x2deec5,_0x21f5b5=this[_0xed474e(0x359)];VisuMZ['BattleSystemFTB'][_0xed474e(0x308)][_0xed474e(0x239)](this),this[_0xed474e(0x1de)](_0x21f5b5);},BattleManager[_0x2deec5(0x1de)]=function(_0x57c2c9){const _0x5729c9=_0x2deec5;if(!this['isFTB']())return;_0x57c2c9&&_0x57c2c9[_0x5729c9(0x207)]();if(this[_0x5729c9(0x20e)]['length']>0x0)this[_0x5729c9(0x359)]&&(!this[_0x5729c9(0x206)]['includes'](this[_0x5729c9(0x359)])&&this[_0x5729c9(0x206)]['unshift'](this[_0x5729c9(0x359)])),this[_0x5729c9(0x359)]=this[_0x5729c9(0x251)]();else this[_0x5729c9(0x2ba)](_0x57c2c9)&&(this['_subject']=_0x57c2c9);},BattleManager[_0x2deec5(0x2ba)]=function(_0x40c787){const _0x16c5a7=_0x2deec5;if(!_0x40c787)return![];if(!_0x40c787[_0x16c5a7(0x24c)]())return![];if(!_0x40c787[_0x16c5a7(0x2da)]())return![];if(!_0x40c787[_0x16c5a7(0x25b)]())return![];if(_0x40c787['isPassingTurnFTB']())return![];return BattleManager[_0x16c5a7(0x2e8)]&&BattleManager['_FTB_KEEP_PREV_ACTOR'];},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x2f1)]=BattleManager[_0x2deec5(0x282)],BattleManager[_0x2deec5(0x282)]=function(){const _0x5d5eef=_0x2deec5;VisuMZ[_0x5d5eef(0x28d)][_0x5d5eef(0x2f1)]['call'](this),this[_0x5d5eef(0x2a8)]();},BattleManager[_0x2deec5(0x2a8)]=function(){const _0x214dff=_0x2deec5;if(!this[_0x214dff(0x357)]())return;if(this['_preemptive'])this[_0x214dff(0x210)]=_0x214dff(0x1f6);else this[_0x214dff(0x26a)]?this[_0x214dff(0x210)]='enemies':this[_0x214dff(0x210)]=BattleManager[_0x214dff(0x30b)];this[_0x214dff(0x210)]=this[_0x214dff(0x210)]||_0x214dff(0x2d9);let _0x58223e=0x0,_0x121c21=0x0;switch(this['_ftbTurnAdvantageUnit']['toLowerCase']()[_0x214dff(0x33b)]()){case'random':let _0xb10431=[_0x214dff(0x1f6),_0x214dff(0x229)];this[_0x214dff(0x210)]=_0xb10431[Math[_0x214dff(0x31a)](_0xb10431[_0x214dff(0x29a)])];break;case _0x214dff(0x1e7):this[_0x214dff(0x210)]=_0x214dff(0x1f6);break;case _0x214dff(0x27e):this[_0x214dff(0x210)]=_0x214dff(0x229);break;case _0x214dff(0x2b0):_0x58223e=$gameParty[_0x214dff(0x243)](),_0x121c21=$gameTroop[_0x214dff(0x243)](),this[_0x214dff(0x210)]=_0x58223e>=_0x121c21?_0x214dff(0x1f6):_0x214dff(0x229);break;case'average\x20agi':_0x58223e=$gameParty[_0x214dff(0x261)](),_0x121c21=$gameTroop[_0x214dff(0x261)](),this['_ftbTurnAdvantageUnit']=_0x58223e>=_0x121c21?_0x214dff(0x1f6):_0x214dff(0x229);break;case _0x214dff(0x28c):_0x58223e=$gameParty['ftbHighestAgility'](),_0x121c21=$gameTroop[_0x214dff(0x335)](),this[_0x214dff(0x210)]=_0x58223e>=_0x121c21?_0x214dff(0x1f6):_0x214dff(0x229);break;case _0x214dff(0x1cf):_0x58223e=$gameParty[_0x214dff(0x2a6)](),_0x121c21=$gameTroop[_0x214dff(0x2a6)](),this['_ftbTurnAdvantageUnit']=_0x58223e>=_0x121c21?'actors':_0x214dff(0x229);break;}this[_0x214dff(0x25e)]=this[_0x214dff(0x210)]==='actors'?$gameParty:$gameTroop,this[_0x214dff(0x1d8)]=this[_0x214dff(0x210)]===_0x214dff(0x1f6)?$gameTroop:$gameParty;},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x33d)]=BattleManager[_0x2deec5(0x2cc)],BattleManager['makeActionOrders']=function(){const _0x435796=_0x2deec5;this[_0x435796(0x357)]()?this[_0x435796(0x320)]():VisuMZ[_0x435796(0x28d)]['BattleManager_makeActionOrders'][_0x435796(0x239)](this);},BattleManager['makeActionOrdersFTB']=function(){const _0xee5386=_0x2deec5;let _0x1e18a2=[],_0x134947=[],_0x38faf8=0x0;const _0x3d1e43=$gameTroop[_0xee5386(0x25c)]();let _0x1baaae=_0x3d1e43%0x2===0x0?this[_0xee5386(0x1d8)]:this[_0xee5386(0x25e)];this[_0xee5386(0x226)]=_0x1baaae;if(_0x1baaae===$gameParty){let _0x17943c=$gameParty[_0xee5386(0x2f5)]()[_0xee5386(0x232)](_0x5d6394=>_0x5d6394[_0xee5386(0x2da)]()&&!_0x5d6394[_0xee5386(0x25b)]()),_0x5f55f9=$gameParty[_0xee5386(0x2f5)]()[_0xee5386(0x232)](_0x1d1b47=>_0x1d1b47[_0xee5386(0x2da)]()&&_0x1d1b47[_0xee5386(0x25b)]());_0x1e18a2=_0x1e18a2[_0xee5386(0x1e3)](_0x17943c),_0x38faf8=Game_Unit[_0xee5386(0x2cd)];while(_0x38faf8--){_0x1e18a2=_0x1e18a2[_0xee5386(0x1e3)](_0x5f55f9);}_0x38faf8=Game_Unit[_0xee5386(0x2cd)]-0x1;while(_0x38faf8--){_0x1e18a2=_0x1e18a2[_0xee5386(0x1e3)](_0x17943c);}}if(_0x1baaae===$gameTroop){let _0x5d5985=$gameTroop['aliveMembers']()[_0xee5386(0x232)](_0xe68c69=>_0xe68c69[_0xee5386(0x2da)]());$gameSystem[_0xee5386(0x223)]()?_0x5d5985[_0xee5386(0x1f7)]((_0x3b2cd6,_0x18aff3)=>_0x18aff3['screenX']()-_0x3b2cd6[_0xee5386(0x322)]()):_0x5d5985['sort']((_0x5d9b13,_0x49c30f)=>_0x5d9b13[_0xee5386(0x322)]()-_0x49c30f[_0xee5386(0x322)]());_0x38faf8=Game_Unit[_0xee5386(0x2cd)];while(_0x38faf8--){_0x134947=_0x134947[_0xee5386(0x1e3)](_0x5d5985);}$gameTroop[_0xee5386(0x207)]();}this[_0xee5386(0x206)]=_0x1e18a2[_0xee5386(0x1e3)](_0x134947);},BattleManager[_0x2deec5(0x236)]=function(){const _0x28141e=_0x2deec5;if(!this['isFTB']())return;this[_0x28141e(0x206)]=this[_0x28141e(0x206)]||[],this[_0x28141e(0x206)]=this[_0x28141e(0x206)][_0x28141e(0x232)](_0x14ac2f=>_0x14ac2f[_0x28141e(0x2da)]()&&!_0x14ac2f[_0x28141e(0x35c)]());},VisuMZ[_0x2deec5(0x28d)]['BattleManager_setup']=BattleManager[_0x2deec5(0x330)],BattleManager[_0x2deec5(0x330)]=function(_0x21a303,_0xf42d77,_0xf34598){const _0x1f46c5=_0x2deec5;VisuMZ['BattleSystemFTB']['BattleManager_setup']['call'](this,_0x21a303,_0xf42d77,_0xf34598),this[_0x1f46c5(0x347)]();},BattleManager[_0x2deec5(0x347)]=function(){const _0x224f8a=_0x2deec5;if(!BattleManager[_0x224f8a(0x357)]())return;this['_ftbCurrentUnit']=undefined,$gameParty['startTurnFTB'](),$gameTroop[_0x224f8a(0x329)]();},VisuMZ['BattleSystemFTB']['BattleManager_startTurn']=BattleManager['startTurn'],BattleManager['startTurn']=function(){const _0xc98bff=_0x2deec5;this[_0xc98bff(0x329)](),VisuMZ[_0xc98bff(0x28d)][_0xc98bff(0x2d7)][_0xc98bff(0x239)](this),this[_0xc98bff(0x30e)]();},BattleManager['startTurnFTB']=function(){const _0x26eb35=_0x2deec5;if(!BattleManager[_0x26eb35(0x357)]())return;$gameParty[_0x26eb35(0x252)](),$gameTroop[_0x26eb35(0x252)]();const _0x118d29=$gameTroop[_0x26eb35(0x25c)]()+0x1;let _0xfb4e99=_0x118d29%0x2===0x0?this['_ftbTeamEven']:this['_ftbTeamOdd'],_0x1a2c2c=_0x118d29%0x2===0x0?this[_0x26eb35(0x25e)]:this[_0x26eb35(0x1d8)];_0x118d29>0x1&&_0x1a2c2c[_0x26eb35(0x2b9)](),_0xfb4e99['updateStateTurnsFTB'](),_0xfb4e99[_0x26eb35(0x329)]();},VisuMZ['BattleSystemFTB']['BattleManager_endTurn']=BattleManager['endTurn'],BattleManager[_0x2deec5(0x2c4)]=function(){const _0x382c50=_0x2deec5;VisuMZ['BattleSystemFTB'][_0x382c50(0x296)][_0x382c50(0x239)](this),this['endTurnFTB']();},BattleManager[_0x2deec5(0x316)]=function(){const _0x459a48=_0x2deec5;if(!BattleManager[_0x459a48(0x357)]())return;},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x299)]=BattleManager['endAllBattlersTurn'],BattleManager[_0x2deec5(0x248)]=function(){const _0x188f57=_0x2deec5;if(this['isFTB']())return;VisuMZ['BattleSystemFTB'][_0x188f57(0x299)]['call'](this);},BattleManager[_0x2deec5(0x30e)]=function(){const _0x3bddae=_0x2deec5;if(!BattleManager['isFTB']())return;let _0x29ac8c='';if(this[_0x3bddae(0x226)]===$gameParty){let _0x3ee6af=$gameParty[_0x3bddae(0x293)]();_0x29ac8c=TextManager[_0x3bddae(0x20b)][_0x3bddae(0x1e1)](_0x3ee6af);}else _0x29ac8c=TextManager['ftbTroopTeamShift'];if(_0x29ac8c!==''){this[_0x3bddae(0x302)]['push'](_0x3bddae(0x323),_0x29ac8c);const _0x1189db=BattleManager['_FTB_BETWEEN_TEAMS_WAIT'];this[_0x3bddae(0x302)][_0x3bddae(0x212)](_0x3bddae(0x2c3),_0x1189db),this['_logWindow'][_0x3bddae(0x212)](_0x3bddae(0x300));}},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x328)]=Game_System[_0x2deec5(0x201)]['initialize'],Game_System[_0x2deec5(0x201)][_0x2deec5(0x305)]=function(){const _0xe71856=_0x2deec5;VisuMZ['BattleSystemFTB'][_0xe71856(0x328)][_0xe71856(0x239)](this),this['initBattleSystemFTB']();},Game_System[_0x2deec5(0x201)][_0x2deec5(0x26d)]=function(){const _0x1e1c84=_0x2deec5;this[_0x1e1c84(0x283)]=!![];},Game_System['prototype']['isBattleSystemFTBActionCountVisible']=function(){const _0x5bd36e=_0x2deec5;if(BattleManager['_phase']===_0x5bd36e(0x202))return![];return this[_0x5bd36e(0x283)]===undefined&&this[_0x5bd36e(0x26d)](),this['_ftbActionCountVisible'];},Game_System['prototype'][_0x2deec5(0x20f)]=function(_0xd38b68){const _0x18d17a=_0x2deec5;this[_0x18d17a(0x283)]===undefined&&this[_0x18d17a(0x26d)](),this[_0x18d17a(0x283)]=_0xd38b68;},VisuMZ['BattleSystemFTB'][_0x2deec5(0x22e)]=Game_Action[_0x2deec5(0x201)][_0x2deec5(0x32f)],Game_Action[_0x2deec5(0x201)][_0x2deec5(0x32f)]=function(){const _0x13ddf9=_0x2deec5;return BattleManager['isFTB']()?0x0:VisuMZ['BattleSystemFTB'][_0x13ddf9(0x22e)][_0x13ddf9(0x239)](this);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x354)]=Game_Action[_0x2deec5(0x201)][_0x2deec5(0x1e0)],Game_Action[_0x2deec5(0x201)][_0x2deec5(0x1e0)]=function(){const _0x3dc7d3=_0x2deec5;VisuMZ[_0x3dc7d3(0x28d)][_0x3dc7d3(0x354)][_0x3dc7d3(0x239)](this),this[_0x3dc7d3(0x273)]();},Game_Action['prototype'][_0x2deec5(0x273)]=function(){const _0x39b30a=_0x2deec5;if(!BattleManager[_0x39b30a(0x357)]())return;if(!this[_0x39b30a(0x34c)]())return;if(!this[_0x39b30a(0x22a)]())return;this[_0x39b30a(0x2d4)]()&&this[_0x39b30a(0x22a)]()['id']===this['subject']()[_0x39b30a(0x237)]()&&(BattleManager[_0x39b30a(0x2bc)]&&this['subject']()[_0x39b30a(0x279)]());const _0x1b5837=VisuMZ[_0x39b30a(0x28d)][_0x39b30a(0x2dd)],_0x76ec37=this[_0x39b30a(0x22a)]()[_0x39b30a(0x30f)];_0x76ec37['match'](_0x1b5837['PassTurn'])&&this[_0x39b30a(0x34c)]()[_0x39b30a(0x279)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x2fb)]=Game_BattlerBase[_0x2deec5(0x201)]['hide'],Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x2e0)]=function(){const _0x5bd931=_0x2deec5;VisuMZ[_0x5bd931(0x28d)][_0x5bd931(0x2fb)][_0x5bd931(0x239)](this),BattleManager['removeActionBattlersFTB'](),this[_0x5bd931(0x361)]()['recalculateActionsFTB']();},VisuMZ['BattleSystemFTB'][_0x2deec5(0x2b5)]=Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x2ff)],Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x2ff)]=function(){const _0x135fb4=_0x2deec5;VisuMZ[_0x135fb4(0x28d)][_0x135fb4(0x2b5)]['call'](this),BattleManager['removeActionBattlersFTB'](),this[_0x135fb4(0x361)]()[_0x135fb4(0x2cf)]();},VisuMZ['BattleSystemFTB']['Game_Battler_performCollapse']=Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x35f)],Game_Battler['prototype'][_0x2deec5(0x35f)]=function(){const _0xc3b24a=_0x2deec5;VisuMZ[_0xc3b24a(0x28d)][_0xc3b24a(0x358)][_0xc3b24a(0x239)](this),BattleManager[_0xc3b24a(0x236)](),this[_0xc3b24a(0x361)]()[_0xc3b24a(0x2cf)]();},Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x279)]=function(){const _0xbc32f=_0x2deec5;this[_0xbc32f(0x240)]=!![],BattleManager['removeActionBattlersFTB']();},Game_BattlerBase['prototype']['isPassingTurnFTB']=function(){return!!this['_passedTurnFTB'];},Game_BattlerBase['_FTB_ACTION_BASE']=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x290)]['GenerateBase'],Game_BattlerBase[_0x2deec5(0x2b1)]=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)][_0x2deec5(0x290)]['AgiBuff'],Game_BattlerBase[_0x2deec5(0x295)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x334)],Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x2ae)]=function(){const _0x484527=_0x2deec5;let _0x4a999b=Game_BattlerBase[_0x484527(0x2c5)];if(this[_0x484527(0x1f9)]===undefined)this[_0x484527(0x28f)]();const _0x33bd0f=this[_0x484527(0x1f9)][0x6]||0x0;if(_0x33bd0f>0x0&&Game_BattlerBase[_0x484527(0x2b1)])_0x4a999b+=_0x33bd0f;else _0x33bd0f<0x0&&Game_BattlerBase[_0x484527(0x295)]&&(_0x4a999b+=_0x33bd0f);const _0x33500c=VisuMZ[_0x484527(0x28d)][_0x484527(0x2dd)],_0x40f230=this['traitObjects']();for(const _0x5c58a9 of _0x40f230){if(!_0x5c58a9)continue;const _0x33580f=_0x5c58a9[_0x484527(0x30f)];_0x33580f[_0x484527(0x2ee)](_0x33500c['ActionPointTraitPlus'])&&(_0x4a999b+=Number(RegExp['$1']));}return Math[_0x484527(0x1d3)](0x0,_0x4a999b);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x34e)]=Game_BattlerBase[_0x2deec5(0x201)]['clearStates'],Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x1ce)]=function(){const _0x1f6ace=_0x2deec5;VisuMZ[_0x1f6ace(0x28d)][_0x1f6ace(0x34e)]['call'](this),this[_0x1f6ace(0x361)]()[_0x1f6ace(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x337)]=Game_BattlerBase['prototype'][_0x2deec5(0x254)],Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x254)]=function(_0x451dee){const _0x5b53f1=_0x2deec5;if(SceneManager['isSceneBattle']()&&BattleManager[_0x5b53f1(0x357)]()){const _0x201e93=DataManager[_0x5b53f1(0x1e6)](_0x451dee);if(_0x201e93>this['friendsUnit']()[_0x5b53f1(0x1fc)]())return![];}return VisuMZ[_0x5b53f1(0x28d)][_0x5b53f1(0x337)][_0x5b53f1(0x239)](this,_0x451dee);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x339)]=Game_Battler['prototype']['useItem'],Game_Battler[_0x2deec5(0x201)]['useItem']=function(_0x536804){const _0x3cb315=_0x2deec5;VisuMZ[_0x3cb315(0x28d)]['Game_Battler_useItem']['call'](this,_0x536804),this[_0x3cb315(0x1f1)](_0x536804);},Game_Battler['prototype']['payActionCostFTB']=function(_0x3b4af6){const _0x5b5dcb=_0x2deec5;if(!_0x3b4af6)return;if(!SceneManager[_0x5b5dcb(0x27b)]())return;if(!BattleManager[_0x5b5dcb(0x357)]())return;const _0x12d427=BattleManager[_0x5b5dcb(0x256)];if(_0x12d427&&_0x12d427[_0x5b5dcb(0x35a)])return;const _0x2f6582=DataManager[_0x5b5dcb(0x1e6)](_0x3b4af6);this[_0x5b5dcb(0x361)]()[_0x5b5dcb(0x2b4)](_0x2f6582);},VisuMZ['BattleSystemFTB'][_0x2deec5(0x2a1)]=Game_Battler['prototype']['onTurnEnd'],Game_Battler['prototype'][_0x2deec5(0x2bf)]=function(){const _0x391092=_0x2deec5;this['_bypassStateTurnUpdatesFTB']=BattleManager[_0x391092(0x357)]()&&BattleManager[_0x391092(0x270)],VisuMZ['BattleSystemFTB']['Game_Battler_onTurnEnd'][_0x391092(0x239)](this),delete this[_0x391092(0x213)];},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x1f4)]=Game_BattlerBase[_0x2deec5(0x201)][_0x2deec5(0x2be)],Game_BattlerBase['prototype'][_0x2deec5(0x2be)]=function(){const _0x59d51a=_0x2deec5;if(this['_bypassStateTurnUpdatesFTB'])return;VisuMZ[_0x59d51a(0x28d)][_0x59d51a(0x1f4)][_0x59d51a(0x239)](this);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x269)]=Game_BattlerBase['prototype']['updateBuffTurns'],Game_BattlerBase[_0x2deec5(0x201)]['updateBuffTurns']=function(){const _0x298bd9=_0x2deec5;if(this[_0x298bd9(0x213)])return;VisuMZ['BattleSystemFTB'][_0x298bd9(0x269)][_0x298bd9(0x239)](this);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x203)]=Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x33c)],Game_Battler[_0x2deec5(0x201)]['addState']=function(_0x8170be){const _0x1cb0bf=_0x2deec5;VisuMZ[_0x1cb0bf(0x28d)][_0x1cb0bf(0x203)][_0x1cb0bf(0x239)](this,_0x8170be),this[_0x1cb0bf(0x361)]()['recalculateActionsFTB']();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x258)]=Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x1ed)],Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x1ed)]=function(_0x441e7c){const _0x7933d1=_0x2deec5;VisuMZ[_0x7933d1(0x28d)][_0x7933d1(0x258)][_0x7933d1(0x239)](this,_0x441e7c),this[_0x7933d1(0x361)]()[_0x7933d1(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x24d)]=Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x230)],Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x230)]=function(_0x3db666,_0x56ea77){const _0x2e435f=_0x2deec5;VisuMZ[_0x2e435f(0x28d)]['Game_Battler_addBuff'][_0x2e435f(0x239)](this,_0x3db666,_0x56ea77),this[_0x2e435f(0x361)]()[_0x2e435f(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x2fc)]=Game_Battler['prototype'][_0x2deec5(0x24b)],Game_Battler[_0x2deec5(0x201)]['addDebuff']=function(_0x859fa2,_0x1009fc){const _0x7bd413=_0x2deec5;VisuMZ[_0x7bd413(0x28d)][_0x7bd413(0x2fc)][_0x7bd413(0x239)](this,_0x859fa2,_0x1009fc),this[_0x7bd413(0x361)]()[_0x7bd413(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x1df)]=Game_Battler[_0x2deec5(0x201)]['removeBuff'],Game_Battler[_0x2deec5(0x201)][_0x2deec5(0x2df)]=function(_0x5d343e){const _0x2f7031=_0x2deec5;VisuMZ[_0x2f7031(0x28d)]['Game_Battler_removeBuff']['call'](this,_0x5d343e),this[_0x2f7031(0x361)]()['recalculateActionsFTB']();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x2af)]=Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x1ec)],Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x1ec)]=function(){const _0x2c04ab=_0x2deec5;if(BattleManager[_0x2c04ab(0x357)]()){if(this['battler']())this['battler']()[_0x2c04ab(0x23b)]();return![];}return VisuMZ[_0x2c04ab(0x28d)][_0x2c04ab(0x2af)][_0x2c04ab(0x239)](this);},VisuMZ['BattleSystemFTB']['Game_Actor_changeEquip']=Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x268)],Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x268)]=function(_0x476ead,_0x5c9bfd){const _0x210790=_0x2deec5;VisuMZ[_0x210790(0x28d)][_0x210790(0x30c)][_0x210790(0x239)](this,_0x476ead,_0x5c9bfd),this[_0x210790(0x361)]()[_0x210790(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x318)]=Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x288)],Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x288)]=function(_0x5a6a2e,_0x14451b){const _0x48fea2=_0x2deec5;VisuMZ[_0x48fea2(0x28d)]['Game_Actor_forceChangeEquip'][_0x48fea2(0x239)](this,_0x5a6a2e,_0x14451b),this[_0x48fea2(0x361)]()[_0x48fea2(0x2cf)]();},VisuMZ['BattleSystemFTB'][_0x2deec5(0x272)]=Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x303)],Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x303)]=function(_0x4833ba,_0x586c51){const _0x23bbdb=_0x2deec5;VisuMZ[_0x23bbdb(0x28d)][_0x23bbdb(0x272)]['call'](this,_0x4833ba,_0x586c51),this[_0x23bbdb(0x361)]()[_0x23bbdb(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x1fe)]=Game_Actor[_0x2deec5(0x201)]['discardEquip'],Game_Actor[_0x2deec5(0x201)]['discardEquip']=function(_0x2006cd){const _0x43823e=_0x2deec5;VisuMZ[_0x43823e(0x28d)][_0x43823e(0x1fe)][_0x43823e(0x239)](this,_0x2006cd),this[_0x43823e(0x361)]()[_0x43823e(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)]['Game_Actor_releaseUnequippableItems']=Game_Actor[_0x2deec5(0x201)]['releaseUnequippableItems'],Game_Actor[_0x2deec5(0x201)][_0x2deec5(0x2d1)]=function(_0x3033da){const _0x154c90=_0x2deec5;VisuMZ[_0x154c90(0x28d)][_0x154c90(0x1cd)][_0x154c90(0x239)](this,_0x3033da),this[_0x154c90(0x361)]()[_0x154c90(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)]['Game_Actor_changeClass']=Game_Actor['prototype']['changeClass'],Game_Actor['prototype'][_0x2deec5(0x23a)]=function(_0x4ef551,_0x8e10fe){const _0x1cb36a=_0x2deec5;VisuMZ[_0x1cb36a(0x28d)][_0x1cb36a(0x33f)][_0x1cb36a(0x239)](this,_0x4ef551,_0x8e10fe),this[_0x1cb36a(0x361)]()[_0x1cb36a(0x2cf)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x25a)]=Game_Enemy['prototype']['transform'],Game_Enemy['prototype'][_0x2deec5(0x228)]=function(_0x54f0e7){const _0x14bcac=_0x2deec5;VisuMZ['BattleSystemFTB']['Game_Enemy_transform']['call'](this,_0x54f0e7),this[_0x14bcac(0x361)]()[_0x14bcac(0x2cf)]();},Game_Unit[_0x2deec5(0x2cd)]=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x2bd)],Game_Unit[_0x2deec5(0x1e8)]=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x271)],Game_Unit[_0x2deec5(0x2e5)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x290)][_0x2deec5(0x1d1)],Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x329)]=function(){const _0x2d4f1c=_0x2deec5;this[_0x2d4f1c(0x356)](),this[_0x2d4f1c(0x2f9)](this[_0x2d4f1c(0x360)]());},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x356)]=function(){const _0x18d3b9=_0x2deec5;this['_inBattle']=!![];let _0x237c98=0x0,_0x36b02c=this['aliveMembers']()[_0x18d3b9(0x232)](_0x7f514b=>_0x7f514b['canMove']());_0x237c98=_0x36b02c[_0x18d3b9(0x27c)]((_0x3dfb32,_0xf29512)=>_0x3dfb32+_0xf29512[_0x18d3b9(0x2ae)](),_0x237c98),_0x237c98=_0x237c98['clamp'](Game_Unit[_0x18d3b9(0x1e8)],Game_Unit[_0x18d3b9(0x2cd)]),this[_0x18d3b9(0x287)]=_0x237c98;},Game_Unit[_0x2deec5(0x201)]['recalculateActionsFTB']=function(){const _0x2d156a=_0x2deec5;if(!BattleManager['isFTB']())return;if(!$gameParty[_0x2d156a(0x34a)]())return;const _0x284a47=this[_0x2d156a(0x360)]();this[_0x2d156a(0x356)]();let _0x2ee318=this[_0x2d156a(0x1fc)]();const _0x58e2f0=this[_0x2d156a(0x360)]()-_0x284a47;if(BattleManager['_FTB_RECALC_ADD_DIFF']&&_0x58e2f0>0x0)_0x2ee318+=_0x58e2f0;if(BattleManager[_0x2d156a(0x234)]&&_0x58e2f0<0x0)_0x2ee318+=_0x58e2f0;_0x2ee318=Math[_0x2d156a(0x264)](_0x2ee318,Game_Unit['_FTB_MAX_ACTIONS']),this[_0x2d156a(0x2f9)](_0x2ee318);},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x1fc)]=function(){const _0x4f69ae=_0x2deec5;return this[_0x4f69ae(0x204)]||0x0;},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x2f9)]=function(_0x45f4dd){const _0x47e9c8=_0x2deec5;this[_0x47e9c8(0x204)]=Math[_0x47e9c8(0x233)](_0x45f4dd)[_0x47e9c8(0x2a3)](0x0,Game_Unit[_0x47e9c8(0x2cd)]),!Game_Unit[_0x47e9c8(0x2e5)]&&(this['_ftbActionsCur']=Math['min'](this[_0x47e9c8(0x204)],this['getMaxActionsFTB']()));},Game_Unit[_0x2deec5(0x201)]['gainCurrentActionsFTB']=function(_0x4d9c8a){const _0x32f6a5=_0x2deec5;this[_0x32f6a5(0x2f9)](this[_0x32f6a5(0x1fc)]()+_0x4d9c8a);},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x32e)]=function(_0x372bd0){const _0x171585=_0x2deec5;this[_0x171585(0x1dc)](-_0x372bd0);},Game_Unit['prototype']['getMaxActionsFTB']=function(){const _0x52c099=_0x2deec5;return this[_0x52c099(0x287)]||0x0;},Game_Unit['prototype'][_0x2deec5(0x2f2)]=function(_0xd21734){const _0x5a2bf=_0x2deec5;this[_0x5a2bf(0x287)]=_0xd21734[_0x5a2bf(0x2a3)](Game_Unit[_0x5a2bf(0x1e8)],Game_Unit[_0x5a2bf(0x2cd)]);},Game_Unit[_0x2deec5(0x201)]['reduceActionsFTB']=function(_0x1a2ba5){this['loseCurrentActionsFTB'](_0x1a2ba5);},Game_Unit['prototype'][_0x2deec5(0x34b)]=function(){const _0x2ba2aa=_0x2deec5;return this[_0x2ba2aa(0x204)]=this[_0x2ba2aa(0x204)]||0x0,this[_0x2ba2aa(0x204)]>0x0;},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x2b9)]=function(){const _0x490e7b=_0x2deec5;for(const _0x233b28 of this[_0x490e7b(0x345)]()){if(!_0x233b28)continue;_0x233b28[_0x490e7b(0x2bf)](),_0x233b28[_0x490e7b(0x2c6)]();}},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x2dc)]=function(){const _0x41d86a=_0x2deec5;if(this['getCurrentActionsFTB']()<=0x0)return!![];if(!this[_0x41d86a(0x2f5)]()[_0x41d86a(0x246)](_0x1ae88b=>_0x1ae88b[_0x41d86a(0x2da)]()))return!![];return![];},Game_Unit[_0x2deec5(0x201)]['updateStateTurnsFTB']=function(){const _0x320e6c=_0x2deec5;for(const _0x19fd49 of this[_0x320e6c(0x345)]()){if(!_0x19fd49)continue;_0x19fd49[_0x320e6c(0x2be)](),_0x19fd49[_0x320e6c(0x2c7)](0x2),_0x19fd49[_0x320e6c(0x1d6)](),_0x19fd49[_0x320e6c(0x2c6)]();}},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x252)]=function(){const _0x22dd34=_0x2deec5;for(const _0x2594dd of this['members']()){if(!_0x2594dd)continue;_0x2594dd[_0x22dd34(0x240)]=![];}},Game_Unit[_0x2deec5(0x201)]['ftbLowestAgility']=function(){const _0x56c566=_0x2deec5,_0x28e9d5=this['members']();return Math[_0x56c566(0x264)](..._0x28e9d5[_0x56c566(0x20a)](_0x298b3a=>_0x298b3a[_0x56c566(0x1f0)]));},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x335)]=function(){const _0x29f1de=_0x2deec5,_0x4a7f86=this[_0x29f1de(0x345)]();return Math['max'](..._0x4a7f86['map'](_0x4228cf=>_0x4228cf[_0x29f1de(0x1f0)]));},Game_Unit[_0x2deec5(0x201)][_0x2deec5(0x2a6)]=function(){const _0x1f0524=_0x2deec5,_0x2ba3a8=this[_0x1f0524(0x345)]();return _0x2ba3a8[_0x1f0524(0x27c)]((_0x3b417b,_0xd55b09)=>_0x3b417b+_0xd55b09[_0x1f0524(0x1f0)],0x0);},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x1ee)]=Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x1f8)],Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x1f8)]=function(){const _0x143618=_0x2deec5;VisuMZ[_0x143618(0x28d)][_0x143618(0x1ee)][_0x143618(0x239)](this),BattleManager[_0x143618(0x357)]()&&this[_0x143618(0x286)]();},Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x286)]=function(){const _0xd023bc=_0x2deec5,_0x3d191e=this['_actorCommandWindow'];this[_0xd023bc(0x1d0)]()&&delete _0x3d191e['_handlers']['cancel'];},VisuMZ[_0x2deec5(0x28d)]['Scene_Battle_commandCancel']=Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x262)],Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x262)]=function(){const _0x53d9a2=_0x2deec5;BattleManager['isFTB']()?this[_0x53d9a2(0x2a9)]():VisuMZ[_0x53d9a2(0x28d)]['Scene_Battle_commandCancel'][_0x53d9a2(0x239)](this);},Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x2a9)]=function(){const _0x2b3c4c=_0x2deec5;this[_0x2b3c4c(0x1e4)][_0x2b3c4c(0x330)](),this[_0x2b3c4c(0x332)][_0x2b3c4c(0x27a)]();},VisuMZ['BattleSystemFTB']['Scene_Battle_commandFight']=Scene_Battle[_0x2deec5(0x201)]['commandFight'],Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x1fd)]=function(){const _0x147910=_0x2deec5;BattleManager[_0x147910(0x357)]()?this[_0x147910(0x333)]():VisuMZ[_0x147910(0x28d)][_0x147910(0x2a7)][_0x147910(0x239)](this);},VisuMZ['BattleSystemFTB'][_0x2deec5(0x1eb)]=Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x1ef)],Scene_Battle[_0x2deec5(0x201)]['createAllWindows']=function(){const _0x3f7a2e=_0x2deec5;VisuMZ[_0x3f7a2e(0x28d)][_0x3f7a2e(0x1eb)]['call'](this),this['createActionCountWindowsFTB']();},Scene_Battle[_0x2deec5(0x201)][_0x2deec5(0x21c)]=function(){const _0x5c395c=_0x2deec5;if(!BattleManager[_0x5c395c(0x357)]())return;const _0x726bb9=this[_0x5c395c(0x24e)](this['_windowLayer']);this['_ftbTroopActionCountWindow']=new Window_FTB_ActionCount(),this[_0x5c395c(0x311)][_0x5c395c(0x2ce)]($gameTroop),this[_0x5c395c(0x2de)](this[_0x5c395c(0x311)],_0x726bb9),this['_ftbPartyActionCountWindow']=new Window_FTB_ActionCount(),this['_ftbPartyActionCountWindow'][_0x5c395c(0x2ce)]($gameParty),this['addChildAt'](this[_0x5c395c(0x2aa)],_0x726bb9),this['repositionLogWindowFTB']();},Scene_Battle['prototype'][_0x2deec5(0x2d5)]=function(){const _0x131a23=_0x2deec5;if(!BattleManager[_0x131a23(0x357)]())return;if(!this[_0x131a23(0x302)])return;const _0x47012e=Window_FTB_ActionCount[_0x131a23(0x338)];if(_0x47012e[_0x131a23(0x289)])return;this[_0x131a23(0x302)]['y']+=_0x47012e[_0x131a23(0x31e)];},Window_Base[_0x2deec5(0x260)]=VisuMZ[_0x2deec5(0x28d)]['Settings'][_0x2deec5(0x2e7)][_0x2deec5(0x307)],Window_Base[_0x2deec5(0x28e)]=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)][_0x2deec5(0x2e7)][_0x2deec5(0x242)],Window_Base[_0x2deec5(0x238)]=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)][_0x2deec5(0x2e7)]['ShowCostForGuard'],Window_Base['_FTB_COST_SHOW_0']=VisuMZ[_0x2deec5(0x28d)]['Settings'][_0x2deec5(0x2e7)]['Show_0_Action_Cost'],Window_Base[_0x2deec5(0x29e)]=VisuMZ['BattleSystemFTB'][_0x2deec5(0x338)]['General']['Show_1_Action_Cost'],VisuMZ['BattleSystemFTB']['Window_Base_makeAdditionalSkillCostText']=Window_Base['prototype'][_0x2deec5(0x259)],Window_Base[_0x2deec5(0x201)][_0x2deec5(0x259)]=function(_0x4fd882,_0x5f6b62,_0x42bb0c){const _0x4ba49a=_0x2deec5;return _0x42bb0c=VisuMZ[_0x4ba49a(0x28d)][_0x4ba49a(0x2e3)][_0x4ba49a(0x239)](this,_0x4fd882,_0x5f6b62,_0x42bb0c),_0x42bb0c=this[_0x4ba49a(0x22f)](_0x4fd882,_0x5f6b62,_0x42bb0c),_0x42bb0c;},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x326)]=Window_Base['prototype']['drawItemNumber'],Window_Base[_0x2deec5(0x201)][_0x2deec5(0x2eb)]=function(_0x1e3392,_0x39bef7,_0x37e3b4,_0x4044f6){const _0x33e686=_0x2deec5;BattleManager[_0x33e686(0x357)]()&&this['constructor']===Window_BattleItem?this['drawItemNumberFTB'](_0x1e3392,_0x39bef7,_0x37e3b4,_0x4044f6):VisuMZ[_0x33e686(0x28d)][_0x33e686(0x326)][_0x33e686(0x239)](this,_0x1e3392,_0x39bef7,_0x37e3b4,_0x4044f6),this['resetFontSettings']();},Window_Base[_0x2deec5(0x201)][_0x2deec5(0x29c)]=function(_0x4b7f65,_0x4880ba,_0x6a6b40,_0x3bfce7){const _0x34d53c=_0x2deec5,_0x2f4738=BattleManager['_actor']||$gameParty[_0x34d53c(0x345)]()[0x0],_0xb6fd70=this[_0x34d53c(0x22f)](_0x2f4738,_0x4b7f65,''),_0x39914a=this[_0x34d53c(0x350)](_0xb6fd70)['width'],_0x17039d=Window_Base[_0x34d53c(0x260)];let _0xd0b3ce=_0x4880ba+_0x3bfce7-_0x39914a;if(_0xb6fd70==='')VisuMZ[_0x34d53c(0x28d)][_0x34d53c(0x326)]['call'](this,_0x4b7f65,_0x4880ba,_0x6a6b40,_0x3bfce7);else{if(this[_0x34d53c(0x30d)](_0x4b7f65)){this[_0x34d53c(0x1f3)]();const _0xb2835d=VisuMZ['ItemsEquipsCore'][_0x34d53c(0x338)][_0x34d53c(0x22c)];this[_0x34d53c(0x22d)][_0x34d53c(0x2b7)]=_0xb2835d[_0x34d53c(0x2b3)];if(_0x17039d){const _0x46ee3c=_0xb2835d['ItemQuantityFmt'],_0x546c34=_0x46ee3c['format']($gameParty['numItems'](_0x4b7f65)),_0xb3eb72=this[_0x34d53c(0x267)](_0x546c34+this[_0x34d53c(0x31b)]());_0xd0b3ce-=_0xb3eb72;}else _0x3bfce7-=this['textWidth'](this[_0x34d53c(0x31b)]())+_0x39914a;VisuMZ[_0x34d53c(0x28d)][_0x34d53c(0x326)][_0x34d53c(0x239)](this,_0x4b7f65,_0x4880ba,_0x6a6b40,_0x3bfce7);}}this[_0x34d53c(0x218)](_0xb6fd70,_0xd0b3ce,_0x6a6b40);},Window_Base['prototype'][_0x2deec5(0x22f)]=function(_0x968d57,_0xc4de1,_0x1f8df2){const _0x333156=_0x2deec5;if(!BattleManager[_0x333156(0x357)]())return _0x1f8df2;if(!_0x968d57)return _0x1f8df2;if(!_0xc4de1)return _0x1f8df2;if(_0xc4de1[_0x333156(0x30f)][_0x333156(0x2ee)](VisuMZ[_0x333156(0x28d)][_0x333156(0x2dd)][_0x333156(0x231)]))return _0x1f8df2;let _0x45577d=DataManager['getActionCostFTB'](_0xc4de1);const _0x1c055b=Window_Base[_0x333156(0x260)],_0x4e28c2=Window_Base[_0x333156(0x28e)],_0x2473a0=Window_Base['_FTB_COST_SHOW_GUARD'],_0x2fb4cb=Window_Base[_0x333156(0x2e6)],_0x175f9d=Window_Base[_0x333156(0x29e)];if(DataManager[_0x333156(0x2d4)](_0xc4de1)&&this['constructor']===Window_ActorCommand){if(!_0x4e28c2&&_0xc4de1['id']===_0x968d57[_0x333156(0x217)]())return _0x1f8df2;if(!_0x2473a0&&_0xc4de1['id']===_0x968d57[_0x333156(0x237)]())return _0x1f8df2;}if(_0x45577d<0x0)return _0x1f8df2;if(!_0x2fb4cb&&_0x45577d===0x0)return _0x1f8df2;if(!_0x175f9d&&_0x45577d===0x1)return _0x1f8df2;const _0xca4886=_0x333156(0x366)[_0x333156(0x1e1)](ImageManager[_0x333156(0x2a2)]),_0x1d4ccd=TextManager[_0x333156(0x344)];let _0x703827=TextManager['ftbCostFormat'][_0x333156(0x1e1)](_0x45577d,_0x1d4ccd,_0xca4886);if(_0x1f8df2==='')_0x1f8df2+=_0x703827;else _0x1c055b?_0x1f8df2=_0x703827+this[_0x333156(0x31b)]()+_0x1f8df2:_0x1f8df2=_0x1f8df2+this[_0x333156(0x31b)]()+_0x703827;return _0x1f8df2;},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x23c)]=Window_Help[_0x2deec5(0x201)][_0x2deec5(0x285)],Window_Help['prototype'][_0x2deec5(0x285)]=function(_0x2cae4b){const _0x49a6a8=_0x2deec5;BattleManager[_0x49a6a8(0x357)]()&&_0x2cae4b&&_0x2cae4b[_0x49a6a8(0x30f)]&&_0x2cae4b[_0x49a6a8(0x30f)]['match'](/<(?:FTB) HELP>\s*([\s\S]*)\s*<\/(?:FTB) HELP>/i)?this[_0x49a6a8(0x1fb)](String(RegExp['$1'])):VisuMZ[_0x49a6a8(0x28d)][_0x49a6a8(0x23c)]['call'](this,_0x2cae4b);},Window_Selectable[_0x2deec5(0x201)][_0x2deec5(0x215)]=function(){const _0x1e8c84=_0x2deec5;return this[_0x1e8c84(0x32a)]===Window_ActorCommand&&BattleManager[_0x1e8c84(0x357)]()&&BattleManager['_FTB_FREE_CHANGE'];},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x1d4)]=Window_Selectable[_0x2deec5(0x201)][_0x2deec5(0x1d5)],Window_Selectable['prototype'][_0x2deec5(0x1d5)]=function(_0x4fae4f){const _0x108bed=_0x2deec5;this[_0x108bed(0x215)]()&&this[_0x108bed(0x35b)]()===0x1?this[_0x108bed(0x2ea)](!![]):VisuMZ[_0x108bed(0x28d)][_0x108bed(0x1d4)][_0x108bed(0x239)](this,_0x4fae4f);},VisuMZ[_0x2deec5(0x28d)]['Window_Selectable_cursorLeft']=Window_Selectable['prototype'][_0x2deec5(0x205)],Window_Selectable['prototype'][_0x2deec5(0x205)]=function(_0x1c921d){const _0xbab6b0=_0x2deec5;this[_0xbab6b0(0x215)]()&&this['maxCols']()===0x1?this[_0xbab6b0(0x2ea)](![]):VisuMZ[_0xbab6b0(0x28d)][_0xbab6b0(0x21a)]['call'](this,_0x1c921d);},VisuMZ['BattleSystemFTB'][_0x2deec5(0x2ed)]=Window_Selectable['prototype'][_0x2deec5(0x22b)],Window_Selectable[_0x2deec5(0x201)][_0x2deec5(0x22b)]=function(){const _0xb9bd5e=_0x2deec5;this[_0xb9bd5e(0x215)]()?this[_0xb9bd5e(0x2ea)](!![]):VisuMZ[_0xb9bd5e(0x28d)][_0xb9bd5e(0x2ed)]['call'](this);},VisuMZ['BattleSystemFTB'][_0x2deec5(0x2e9)]=Window_Selectable[_0x2deec5(0x201)][_0x2deec5(0x351)],Window_Selectable[_0x2deec5(0x201)]['cursorPageup']=function(){const _0x14d33f=_0x2deec5;this['ftbFreeRangeSwitch']()?this[_0x14d33f(0x2ea)](![]):VisuMZ[_0x14d33f(0x28d)]['Window_Selectable_cursorPageup']['call'](this);},Window_ActorCommand[_0x2deec5(0x201)][_0x2deec5(0x2ea)]=function(_0xd1d30){const _0x1cf30f=_0x2deec5,_0x4a5583=BattleManager[_0x1cf30f(0x2c1)];let _0x146280=$gameParty[_0x1cf30f(0x364)]()[_0x1cf30f(0x266)](_0x4a5583);const _0xe58fc9=$gameParty[_0x1cf30f(0x364)]()[_0x1cf30f(0x29a)]-0x1;let _0x35db90=$gameParty['battleMembers']()[_0x146280];for(;;){_0x146280+=_0xd1d30?0x1:-0x1;if(_0x146280<0x0)_0x146280=_0xe58fc9;if(_0x146280>_0xe58fc9)_0x146280=0x0;_0x35db90=$gameParty['battleMembers']()[_0x146280];if(_0x35db90&&_0x35db90[_0x1cf30f(0x25b)]()&&!_0x35db90[_0x1cf30f(0x35c)]())break;if(_0x35db90===_0x4a5583)break;}this[_0x1cf30f(0x298)](_0x4a5583,_0x35db90);},Window_ActorCommand['prototype']['processSwitchActors']=function(_0x5ec560,_0x59d962){const _0x3ee759=_0x2deec5;if(_0x5ec560===_0x59d962)return;if(_0x5ec560[_0x3ee759(0x312)]())_0x5ec560[_0x3ee759(0x312)]()[_0x3ee759(0x28b)]();this[_0x3ee759(0x1d7)](),BattleManager[_0x3ee759(0x359)]=_0x59d962,BattleManager[_0x3ee759(0x2c1)]=_0x59d962,BattleManager[_0x3ee759(0x33a)](),SceneManager[_0x3ee759(0x324)][_0x3ee759(0x333)]();},VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x209)]=Window_Selectable[_0x2deec5(0x201)]['processTouch'],Window_Selectable['prototype'][_0x2deec5(0x319)]=function(){const _0x4697d0=_0x2deec5;BattleManager[_0x4697d0(0x357)]()&&BattleManager[_0x4697d0(0x2e8)]&&this['constructor']===Window_BattleStatus?this[_0x4697d0(0x250)]():VisuMZ[_0x4697d0(0x28d)][_0x4697d0(0x209)][_0x4697d0(0x239)](this);},Window_BattleStatus[_0x2deec5(0x201)][_0x2deec5(0x250)]=function(){const _0xf85ac4=_0x2deec5;this[_0xf85ac4(0x346)]()&&(TouchInput[_0xf85ac4(0x29b)]()&&this['onTouchSelectFTB'](!![]));},Window_BattleStatus[_0x2deec5(0x201)]['onTouchSelectFTB']=function(_0xb39dc9){const _0x487343=_0x2deec5,_0x51de50=SceneManager[_0x487343(0x324)][_0x487343(0x332)];if(!_0x51de50)return;if(!_0x51de50[_0x487343(0x362)])return;this[_0x487343(0x2d8)]=![];const _0x4301d2=this['index'](),_0x170c87=this[_0x487343(0x2c2)]();if(_0x170c87>=0x0){const _0x3ae6a3=$gameParty['battleMembers']()[_0x4301d2],_0x1002a3=$gameParty['battleMembers']()[_0x170c87];this['canActorBeSelectedFTB'](_0x1002a3)&&(_0x170c87===this[_0x487343(0x327)]()&&(this[_0x487343(0x2d8)]=!![]),this[_0x487343(0x257)](_0x170c87),_0x51de50['processSwitchActors'](_0x3ae6a3,_0x1002a3));}},Window_BattleStatus['prototype'][_0x2deec5(0x280)]=function(_0x5865e5){const _0x3c81c1=_0x2deec5;if(!_0x5865e5)return![];if(!_0x5865e5[_0x3c81c1(0x2da)]())return![];if(!_0x5865e5['canInput']())return![];if(_0x5865e5[_0x3c81c1(0x35c)]())return![];return!![];};function Window_FTB_ActionCount(){const _0x4b29c9=_0x2deec5;this[_0x4b29c9(0x305)](...arguments);}Window_FTB_ActionCount[_0x2deec5(0x201)]=Object[_0x2deec5(0x249)](Window_Base['prototype']),Window_FTB_ActionCount[_0x2deec5(0x201)]['constructor']=Window_FTB_ActionCount,Window_FTB_ActionCount['Settings']=VisuMZ[_0x2deec5(0x28d)][_0x2deec5(0x338)]['ActionCountDisplay'],Window_FTB_ActionCount[_0x2deec5(0x201)]['initialize']=function(){const _0x4502e3=_0x2deec5,_0x3df45a=this[_0x4502e3(0x365)]();Window_Base[_0x4502e3(0x201)]['initialize'][_0x4502e3(0x239)](this,_0x3df45a),this[_0x4502e3(0x265)](0x0),this['initMembers'](),this['opacity']=0x0;},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x365)]=function(){const _0x18e724=_0x2deec5;return new Rectangle(0x0,0x0,Graphics['width'],Graphics[_0x18e724(0x2e4)]);},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x2e2)]=function(){const _0x318854=_0x2deec5;this[_0x318854(0x28a)]=null,this[_0x318854(0x325)]=0x0,this[_0x318854(0x20d)]=0x0;const _0x2534ef=Window_FTB_ActionCount['Settings'];this[_0x318854(0x1e9)]={'ActorPicture':_0x2534ef['ActorActionPicture']?ImageManager[_0x318854(0x1d9)](_0x2534ef[_0x318854(0x21d)]):'','EnemyPicture':_0x2534ef['EnemyActionPicture']?ImageManager[_0x318854(0x1d9)](_0x2534ef[_0x318854(0x31d)]):'','EmptyPicture':_0x2534ef[_0x318854(0x276)]?ImageManager[_0x318854(0x1d9)](_0x2534ef[_0x318854(0x276)]):''};},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x26e)]=function(){const _0x811d3f=_0x2deec5;this[_0x811d3f(0x314)]=0x0;},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x2ce)]=function(_0x1083ec){const _0x15e119=_0x2deec5;this[_0x15e119(0x28a)]=_0x1083ec,this[_0x15e119(0x20c)]();},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x20c)]=function(){const _0x20c16d=_0x2deec5;Window_Base[_0x20c16d(0x201)][_0x20c16d(0x20c)]['call'](this),this[_0x20c16d(0x26b)](),this['updatePosition'](),this[_0x20c16d(0x33e)]();},Window_FTB_ActionCount['prototype'][_0x2deec5(0x26b)]=function(){const _0xed125f=_0x2deec5;if(!this['_unit'])return;(this['_currentActions']!==this[_0xed125f(0x28a)][_0xed125f(0x1fc)]()||this[_0xed125f(0x20d)]!==this['_unit']['getMaxActionsFTB']())&&(this[_0xed125f(0x325)]=this[_0xed125f(0x28a)][_0xed125f(0x1fc)](),this[_0xed125f(0x20d)]=this[_0xed125f(0x28a)][_0xed125f(0x360)](),this[_0xed125f(0x32c)]());},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x33e)]=function(){const _0x1c3e40=_0x2deec5;this['visible']=$gameSystem[_0x1c3e40(0x306)]();},Window_FTB_ActionCount[_0x2deec5(0x201)]['refresh']=function(){const _0x28573d=_0x2deec5;this[_0x28573d(0x22d)][_0x28573d(0x300)]();if(!this[_0x28573d(0x28a)])return;const _0x3c51b4=Window_FTB_ActionCount[_0x28573d(0x338)];if(!_0x3c51b4)return;const _0x33d417=this['createStartingCoordinates'](),_0x216133=this[_0x28573d(0x32d)](),_0x2000f2=_0x3c51b4[_0x28573d(0x245)]+_0x3c51b4[_0x28573d(0x29d)],_0x171c96=_0x3c51b4[_0x28573d(0x21f)];let _0x34e2f8=_0x33d417['x'],_0x186865=_0x33d417['y'];while(_0x216133[_0x28573d(0x29a)]>0x0){const _0xbcc108=_0x216133['shift']();this[_0x28573d(0x355)](_0xbcc108,_0x34e2f8,_0x186865,_0x216133[_0x28573d(0x29a)]),_0x171c96?_0x34e2f8+=_0x2000f2:_0x186865+=_0x2000f2;}},Window_FTB_ActionCount['prototype'][_0x2deec5(0x2fd)]=function(){const _0x450275=_0x2deec5,_0x288366=Window_FTB_ActionCount[_0x450275(0x338)],_0x1a273a=this['_unit']===$gameParty,_0x50dbbd=_0x288366['ImageSize'],_0x1c5448=_0x50dbbd*(_0x288366[_0x450275(0x2ec)]-0x1)+_0x288366['ImageGapDistance']*(_0x288366[_0x450275(0x2ec)]-0x2),_0x5e7ae7=_0x288366['DrawHorz'],_0x47c4cc=SceneManager[_0x450275(0x324)][_0x450275(0x1db)]['height'];let _0x2b3ce8=0x0,_0x4220fc=0x0;const _0x56ec6f=_0x288366[_0x450275(0x289)];if(_0x56ec6f){_0x4220fc=this[_0x450275(0x331)]-_0x47c4cc-_0x288366[_0x450275(0x367)]-_0x50dbbd,_0x2b3ce8=_0x1a273a?this[_0x450275(0x274)]-_0x288366['ScreenBufferX']-_0x50dbbd:_0x288366[_0x450275(0x208)];if(_0x5e7ae7&&_0x1a273a)_0x2b3ce8-=_0x1c5448;else!_0x5e7ae7&&(_0x4220fc-=_0x1c5448);}else _0x4220fc=_0x288366[_0x450275(0x367)],_0x2b3ce8=_0x1a273a?this[_0x450275(0x274)]-_0x288366[_0x450275(0x208)]-_0x50dbbd:_0x288366[_0x450275(0x208)],_0x5e7ae7&&_0x1a273a&&(_0x2b3ce8-=_0x1c5448);return _0x2b3ce8+=_0x1a273a?_0x288366['ActorOffsetX']:_0x288366['EnemyOffsetX'],_0x4220fc+=_0x1a273a?_0x288366[_0x450275(0x224)]:_0x288366['EnemyOffsetY'],new Point(Math['round'](_0x2b3ce8),Math[_0x450275(0x233)](_0x4220fc));},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x32d)]=function(){const _0xf0152d=_0x2deec5,_0x3a1c22=Window_FTB_ActionCount['Settings'];let _0x592fcf=!![];if(_0x3a1c22[_0xf0152d(0x21f)]){if(this[_0xf0152d(0x28a)]===$gameParty)_0x592fcf=!_0x592fcf;}else _0x592fcf=!_0x3a1c22[_0xf0152d(0x289)];let _0x5c4660=this['_unit'][_0xf0152d(0x1fc)](),_0x20dca7=Math[_0xf0152d(0x1d3)](0x0,this['_unit'][_0xf0152d(0x360)]()-_0x5c4660);const _0x3825df=[];while(_0x5c4660--){const _0x177a90=_0xf0152d(0x291);_0x3825df['push'](_0x177a90);}while(_0x20dca7--){const _0x357041=_0xf0152d(0x2bb);_0x592fcf?_0x3825df[_0xf0152d(0x212)](_0x357041):_0x3825df[_0xf0152d(0x342)](_0x357041);}while(_0x3825df[_0xf0152d(0x29a)]<0xa){const _0x57e8dd=_0xf0152d(0x2db);_0x592fcf?_0x3825df['push'](_0x57e8dd):_0x3825df[_0xf0152d(0x342)](_0x57e8dd);}return _0x3825df;},Window_FTB_ActionCount['prototype']['drawImage']=function(_0x1ab606,_0x18d154,_0x3cf191,_0x6730c0){const _0x4b1af3=_0x2deec5;if(_0x1ab606==='Nothing')return;if(_0x1ab606==='Current')_0x1ab606=this[_0x4b1af3(0x28a)]===$gameParty?_0x4b1af3(0x2fa):_0x4b1af3(0x2d3);const _0x1c14fa=Window_FTB_ActionCount[_0x4b1af3(0x338)];if(_0x1c14fa['%1ActionPicture'[_0x4b1af3(0x1e1)](_0x1ab606)]){const _0x4ad923=_0x1c14fa[_0x4b1af3(0x32b)[_0x4b1af3(0x1e1)](_0x1ab606)],_0x4aa06b=ImageManager[_0x4b1af3(0x1d9)](_0x4ad923);_0x4aa06b[_0x4b1af3(0x2ad)](this['drawPicture'][_0x4b1af3(0x25d)](this,_0x4aa06b,_0x18d154,_0x3cf191,_0x6730c0));}else{const _0x2a5bf7=ImageManager[_0x4b1af3(0x2fe)[_0x4b1af3(0x1e1)](_0x1ab606)];this[_0x4b1af3(0x225)](_0x2a5bf7,_0x18d154,_0x3cf191),this[_0x4b1af3(0x30a)](_0x6730c0)&&this['drawActionsRemaining'](_0x18d154,_0x3cf191);}},Window_FTB_ActionCount[_0x2deec5(0x201)]['drawPicture']=function(_0x3ae965,_0x268cfd,_0x764949,_0x48dadb){const _0x436677=_0x2deec5;if(!_0x3ae965)return;const _0xdec84b=Window_FTB_ActionCount[_0x436677(0x338)],_0xc3cd80=_0xdec84b[_0x436677(0x245)],_0x568ebb=_0xc3cd80/_0x3ae965['width'],_0x19e54c=_0xc3cd80/_0x3ae965[_0x436677(0x2e4)],_0x3010d1=Math[_0x436677(0x264)](_0x568ebb,_0x19e54c,0x1),_0x5add32=_0x3ae965[_0x436677(0x2e4)],_0x5d3a58=_0x3ae965['height'],_0x4310cf=Math[_0x436677(0x233)](_0x5add32*_0x3010d1),_0x546292=Math[_0x436677(0x233)](_0x5d3a58*_0x3010d1),_0x5e6e12=Math[_0x436677(0x233)](_0x268cfd+(_0xc3cd80-_0x4310cf)/0x2),_0x1e599d=Math[_0x436677(0x233)](_0x764949+(_0xc3cd80-_0x546292)/0x2);this[_0x436677(0x22d)]['_context'][_0x436677(0x341)]=_0xdec84b[_0x436677(0x1dd)],this[_0x436677(0x22d)][_0x436677(0x244)](_0x3ae965,0x0,0x0,_0x5add32,_0x5d3a58,_0x5e6e12,_0x1e599d,_0x4310cf,_0x546292),this[_0x436677(0x22d)][_0x436677(0x2c9)][_0x436677(0x341)]=!![],this[_0x436677(0x30a)](_0x48dadb)&&this['drawActionsRemaining'](_0x268cfd,_0x764949);},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x225)]=function(_0x349f55,_0x5241a7,_0x51ed52){const _0x39e479=_0x2deec5,_0x2b1506=Window_FTB_ActionCount[_0x39e479(0x338)];let _0x52cea8=_0x2b1506['ImageSize'];const _0x46cb38=ImageManager[_0x39e479(0x2a4)](_0x39e479(0x2c0)),_0x15f499=ImageManager[_0x39e479(0x227)],_0x496949=ImageManager[_0x39e479(0x2f0)],_0x20363c=_0x349f55%0x10*_0x15f499,_0x55e72d=Math[_0x39e479(0x336)](_0x349f55/0x10)*_0x496949;this[_0x39e479(0x22d)][_0x39e479(0x2c9)]['imageSmoothingEnabled']=_0x2b1506[_0x39e479(0x263)],this[_0x39e479(0x22d)][_0x39e479(0x244)](_0x46cb38,_0x20363c,_0x55e72d,_0x15f499,_0x496949,_0x5241a7,_0x51ed52,_0x52cea8,_0x52cea8),this[_0x39e479(0x22d)][_0x39e479(0x2c9)][_0x39e479(0x341)]=!![];},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x2ab)]=function(){const _0x413dff=_0x2deec5,_0xb5dea1=Window_FTB_ActionCount['Settings'];if(_0xb5dea1['BottomPosition'])return;if(!_0xb5dea1[_0x413dff(0x2a0)])return;const _0x205f3c=SceneManager[_0x413dff(0x324)][_0x413dff(0x281)];if(!_0x205f3c)return;_0x205f3c['visible']?(this['x']=_0xb5dea1['RepositionTopHelpX']||0x0,this['y']=_0xb5dea1[_0x413dff(0x211)]||0x0):(this['x']=0x0,this['y']=0x0);},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x30a)]=function(_0x2af647){const _0xe09474=_0x2deec5,_0x1841ab=Window_FTB_ActionCount['Settings'];if(!_0x1841ab['DrawActionsRemaining'])return![];const _0xc62a06=_0x1841ab[_0xe09474(0x289)],_0xd79d5a=_0x1841ab[_0xe09474(0x21f)],_0x42ae38=this[_0xe09474(0x28a)]===$gameParty;if(_0xd79d5a)return _0x42ae38?_0x2af647===0x0:_0x2af647===_0x1841ab['MaxVisible']-0x1;else return _0xc62a06?_0x2af647===0x0:_0x2af647===_0x1841ab['MaxVisible']-0x1;},Window_FTB_ActionCount[_0x2deec5(0x201)][_0x2deec5(0x21e)]=function(_0x549b1a,_0x39f688){const _0x35a4cb=_0x2deec5;this[_0x35a4cb(0x1f3)]();const _0x448184=Window_FTB_ActionCount[_0x35a4cb(0x338)],_0x179a47=new Rectangle(_0x549b1a,_0x39f688,_0x448184[_0x35a4cb(0x245)],_0x448184[_0x35a4cb(0x245)]);_0x179a47['x']+=_0x448184[_0x35a4cb(0x313)],_0x179a47['y']+=_0x448184[_0x35a4cb(0x309)];const _0x148ada=this[_0x35a4cb(0x28a)][_0x35a4cb(0x1fc)]();this[_0x35a4cb(0x22d)][_0x35a4cb(0x2b7)]=_0x448184['ActionsRemainingFontSize'],this['contents'][_0x35a4cb(0x1f5)](_0x148ada,_0x179a47['x'],_0x179a47['y'],_0x179a47[_0x35a4cb(0x278)],_0x179a47[_0x35a4cb(0x2e4)],_0x35a4cb(0x2b2)),this[_0x35a4cb(0x1f3)]();};