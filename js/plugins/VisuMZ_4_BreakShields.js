//=============================================================================
// VisuStella MZ - Break Shields
// VisuMZ_4_BreakShields.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_4_BreakShields = true;

var VisuMZ = VisuMZ || {};
VisuMZ.BreakShields = VisuMZ.BreakShields || {};
VisuMZ.BreakShields.version = 1.00;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 4] [Version 1.00] [BreakShields]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Break_Shields_VisuStella_MZ
 * @orderAfter VisuMZ_0_CoreEngine
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin introduces a new mechanic called Break Shields. Actors and/or
 * enemies can have them. Whenever a battler is struck with an elemental
 * weakness, their Break Shield is reduced by 1 (unless modified by a notetag).
 * Once the battler's Break Shield reaches a score of 0, a state is then
 * applied to the battler (usually a stun state). Once the Break state wears
 * off, the battler will regain their Break Shields again. This can be used to
 * create complex battle depth for your game.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Control how Break Shields are calculated alongside how many hits are
 *   required for each actor and/or enemy to enter the Break Stun state.
 * * Display the Break Shields on the screen and relay the information to your
 *   players through icons.
 * * Play animations when hitting a weakness and reducing Break Shields.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 4 ------
 *
 * This plugin is a Tier 4 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 *
 * VisuMZ_0_CoreEngine
 *
 * Two of the animation Plugin Parameters require the Core Engine to play them.
 * This is due to how the Core Engine allows playing animations without halting
 * the battle system to allow for a seamless flow despite relaying the Break
 * Shield reduction visual feedback.
 *
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins. Here is a list
 * of the ones this plugin is not compatible with.
 *
 * ---
 *
 * VisuMZ_2_BattleSystemSTB
 * 
 * The Break Shields plugin can be used together with Battle System - STB.
 * However, it cannot be used together with the STB Exploit system. This is
 * because both Break Shields and the Exploit system function under similar
 * mechanics and will conflict. However, if STB's Exploit system is turned off,
 * then you can use all of the Break Shield plugin's features fully.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === Break Shield Calculation-Related Notetags ===
 * 
 * ---
 *
 * <Break Shields: x>
 *
 * - Used for: Actor, Class, Enemy Notetags
 * - Declares the base amount of Break Shields this battler will have.
 * - This will ignore the default setting from the Plugin Parameters.
 * - Replace 'x' with a number representing the base amount of Break Shields to
 *   give this battler.
 * - If both the Actor and Class database object has this notetag, priority
 *   will be given to the Class before the Actor.
 *
 * ---
 *
 * <Break Shields: +x>
 * <Break Shields: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Allows trait objects to alter the amount of Break Shields battlers have
 *   whenever their Break Shields are reset.
 * - Replace 'x' with a number representing the Break Shields to increase or
 *   decrease the amount by.
 * - Total Break Shields cannot go under 1 and cannot go whatever the maximum
 *   is declared inside the Plugin Parameters.
 *
 * ---
 * 
 * === Break Shield Alteration-Related Notetags ===
 * 
 * ---
 *
 * <Break Reduce: x>
 *
 * - Used for: Skill, Item Notetags
 * - Reduces the target's Break Shield by x if this action hits a weakness.
 * - This will ignore the default setting from the Plugin Parameters.
 * - Replace 'x' with a number to determine how many Break Shields to reduce.
 * - If Break Shields reach 0, the target will enter a Stun state.
 *
 * ---
 *
 * <Change Break Shield: x>
 *
 * - Used for: Skill, Item Notetags
 * - This will change the target battler's Break Shield value to x if the
 *   battler isn't currently stunned.
 * - No effect if you don't use this notetag.
 * - Replace 'x' with a number value to change the target battler's Break
 *   Shield value to.
 *
 * ---
 *
 * <Increase Break Shield: +x>
 * <Decrease Break Shield: -x>
 *
 * - Used for: Skill, Item Notetags
 * - This will either increase the target battler's break shield by x or
 *   decrease the target battler's break shield by x.
 * - Happens after the Change Break Shield notetag.
 * - No effect if you don't use this notetag.
 * - Replace 'x' with a number value representing the amount to alter the
 *   target's Break Shields by.
 *
 * ---
 * 
 * === Element-Related Notetags ===
 * 
 * ---
 *
 * <Protect Element: id>
 * <Protect Elements: id, id, id>
 * 
 * <Protect Element: name>
 * <Protect Elements: name, name, name>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Specified element(s) will be guarded and Break Shields cannot be reduced
 *   when struck with that element (as long as the requirement is above 100%).
 * - The element rate for those will cap at 100%, preventing extra damage from
 *   being dealt despite having weaknesses, although custom JS effects will
 *   bypass this.
 * - Replace 'id' with a number value representing the ID(s) of the element(s).
 * - Replace 'name' with the name(s) of the element(s).
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * Customize the mechanical settings for Break Shields.
 *
 * ---
 *
 * Break Shields
 * 
 *   Affect: Actors?:
 *   - Do Break Shields affect actors?
 * 
 *   Affect: Enemies?:
 *   - Do Break Shields affect actors?
 * 
 *   Base Shield Value:
 *   - The starting amount of shields a battler has.
 *   - Can be altered through notetags.
 * 
 *   Maximum Shields:
 *   - The maximum amount of shields a battler can have.
 *   - This is a hard cap.
 * 
 *   Stun State ID:
 *   - This is the state to be applied when all Break Shields are reduced to 0.
 *
 * ---
 *
 * Animation
 * 
 *   Reduce Animation ID:
 *   - Play this animation when Break Shields are reduced.
 *   - Requires VisuMZ_0_CoreEngine.
 * 
 *   Stun Animation ID:
 *   - Play this animation when Break Stun is achieved.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 * ---
 *
 * Weaknesses
 * 
 *   Minimum Rate:
 *   - What is the minimum element rate for an attack to be considered striking
 *     a weakness?
 * 
 *   Default Reduction:
 *   - Default reduction amount for Break Shields when striking an elemental
 *     weakness.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: UI Settings
 * ============================================================================
 *
 * Customize the UI settings for Break Shields.
 *
 * ---
 *
 * Icons
 * 
 *   Break Shield Icon:
 *   - Icon used to represent Break Shields.
 * 
 *   Stun State Icon:
 *   - Icon used to represent Break Stun if the Break Stun state does NOT have
 *     an icon assigned to it.
 * 
 *     Show Turns?:
 *     - Show how many turns are remaining with the Break Stun?
 * 
 *   Protect Icon:
 *   - Icon used to represent Protected Elements.
 *   - Used for other plugins.
 * 
 *   Font Size:
 *   - What is the font size used to display the turns and Break Shields
 *     remaining?
 *
 * ---
 *
 * Battlers > Actors/Enemies
 * 
 *   Show Battler Icon?:
 *   - Show Break Shield icons on the SV_Actor/enemy battlers?
 * 
 *   Position:
 *   - Where on the battler would you like to place the icon?
 * 
 *   Offset X:
 *   - How much to offset the icon X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the icon Y position by?
 *   - Negative goes up. Positive goes down.
 * 
 *   Name: Attach Shields (Enemies Only)
 *   - Attach the Break Shield icon to the enemy name?
 *   - Overrides direct attachment.
 *   - Requires VisuMZ_1_BattleCore!
 * 
 *     Attach: Offset X:
 *     - How much to offset the attached icon's X position by?
 *     - Negative goes left. Positive goes right.
 * 
 *     Attach: Offset Y:
 *     - How much to offset the attached icon's Y position by?
 *     - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Battle Status
 * 
 *   Show Break Shields?:
 *   - Show Break Shield icons in the Battle Status?
 * 
 *   Auto-Position?:
 *   - Automatically position the Break Shield icon?
 *   - If not, it'll position it to the upper left.
 * 
 *   Offset X:
 *   - How much to offset the icon X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the icon Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Menu Status
 * 
 *   Show Break Shields?:
 *   - Show Break Shield icons in the menu scenes?
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.00 Official Release Date: April 30, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BreakShields
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Customize the mechanical settings for Break Shields.
 * @default {"BreakShields":"","AffectActors:eval":"true","AffectEnemies:eval":"true","Base:num":"1","Max:num":"99","StunState:num":"13","Animation":"","ReduceAniID:num":"2","StunAniID:num":"15","Weaknesses":"","MinRate:num":"1.05","Reduction:num":"1"}
 *
 * @param UI:struct
 * @text UI Settings
 * @type struct<UI>
 * @desc Customize the UI settings for Break Shields.
 * @default {"Icons":"","ShieldIcon:num":"81","StunIcon:num":"6","ShowStunTurns:eval":"false","ProtectIcon:num":"128","FontSize:num":"22","Battlers":"","Actors":"","ActorDisplayIcon:eval":"false","ActorDisplayPosition:str":"bottom center","ActorOffsetX:num":"+0","ActorOffsetY:num":"+8","Enemies":"","EnemyDisplayIcon:eval":"true","EnemyDisplayPosition:str":"bottom center","EnemyOffsetX:num":"+0","EnemyOffsetY:num":"+8","NameAttachShieldIcon:eval":"true","AttachShieldOffsetX:num":"+0","AttachShieldOffsetY:num":"+0","BattleStatus":"","BattleStatusDisplayIcons:eval":"true","BattleStatusAutoPosition:eval":"true","BattleStatusOffsetX:num":"+0","BattleStatusOffsetY:num":"+0","MenuStatus":"","MenuStatusBreakShieldIcons:eval":"true"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param BreakShields
 * @text Break Shields
 *
 * @param AffectActors:eval
 * @text Affect: Actors?
 * @parent BreakShields
 * @type boolean
 * @on Yes
 * @off No
 * @desc Do Break Shields affect actors?
 * @default true
 *
 * @param AffectEnemies:eval
 * @text Affect: Enemies?
 * @parent BreakShields
 * @type boolean
 * @on Yes
 * @off No
 * @desc Do Break Shields affect actors?
 * @default true
 *
 * @param Base:num
 * @text Base Shield Value
 * @parent BreakShields
 * @type number
 * @min 1
 * @desc The starting amount of shields a battler has.
 * Can be altered through notetags.
 * @default 1
 *
 * @param Max:num
 * @text Maximum Shields
 * @parent BreakShields
 * @type number
 * @min 1
 * @desc The maximum amount of shields a battler can have.
 * This is a hard cap.
 * @default 99
 *
 * @param StunState:num
 * @text Stun State ID
 * @parent BreakShields
 * @type state
 * @desc This is the state to be applied when all Break Shields
 * are reduced to 0.
 * @default 13
 *
 * @param Animation
 *
 * @param ReduceAniID:num
 * @text Reduce Animation ID
 * @parent Animation
 * @type animation
 * @desc Play this animation when Break Shields are reduced.
 * Requires VisuMZ_0_CoreEngine.
 * @default 2
 *
 * @param StunAniID:num
 * @text Stun Animation ID
 * @parent Animation
 * @type animation
 * @desc Play this animation when Break Stun is achieved.
 * Requires VisuMZ_0_CoreEngine.
 * @default 15
 *
 * @param Weaknesses
 *
 * @param MinRate:num
 * @text Minimum Rate
 * @parent Weaknesses
 * @desc What is the minimum element rate for an attack to be
 * considered striking a weakness?
 * @default 1.05
 *
 * @param Reduction:num
 * @text Default Reduction
 * @parent Weaknesses
 * @type number
 * @min 1
 * @desc Default reduction amount for Break Shields when striking
 * an elemental weakness.
 * @default 1
 *
 */
/* ----------------------------------------------------------------------------
 * UI Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~UI:
 *
 * @param Icons
 *
 * @param ShieldIcon:num
 * @text Break Shield Icon
 * @parent Icons
 * @desc Icon used to represent Break Shields.
 * @default 81
 *
 * @param StunIcon:num
 * @text Stun State Icon
 * @parent Icons
 * @desc Icon used to represent Break Stun if the Break Stun state
 * does NOT have an icon assigned to it.
 * @default 6
 *
 * @param ShowStunTurns:eval
 * @text Show Turns?
 * @parent StunIcon:num
 * @type boolean
 * @on Show Turns
 * @off Hide Turns
 * @desc Show how many turns are remaining with the Break Stun?
 * @default false
 *
 * @param ProtectIcon:num
 * @text Protect Icon
 * @parent Icons
 * @desc Icon used to represent Protected Elements.
 * Used for other plugins.
 * @default 128
 *
 * @param FontSize:num
 * @text Font Size
 * @parent Icons
 * @number
 * @min 1
 * @desc What is the font size used to display the turns and
 * Break Shields remaining?
 * @default 22
 *
 * @param Battlers
 * 
 * @param Actors
 * @parent Battlers
 *
 * @param ActorDisplayIcon:eval
 * @text Show Battler Icon?
 * @parent Actors
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show Break Shield icons on the SV_Actor battlers?
 * @default false
 *
 * @param ActorDisplayPosition:str
 * @text Position
 * @parent Actors
 * @type combo
 * @option top left
 * @option top center
 * @option top right
 * @option middle left
 * @option middle center
 * @option middle right
 * @option bottom left
 * @option bottom center
 * @option bottom right
 * @desc Where on the battler would you like to place the icon?
 * @default bottom center
 *
 * @param ActorOffsetX:num
 * @text Offset X
 * @parent Actors
 * @desc How much to offset the icon X position by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param ActorOffsetY:num
 * @text Offset Y
 * @parent Actors
 * @desc How much to offset the icon Y position by?
 * Negative goes up. Positive goes down.
 * @default +8
 * 
 * @param Enemies
 * @parent Battlers
 *
 * @param EnemyDisplayIcon:eval
 * @text Show Battler Icon?
 * @parent Enemies
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show Break Shield icons on the enemy battlers?
 * @default true
 *
 * @param EnemyDisplayPosition:str
 * @text Position
 * @parent Enemies
 * @type combo
 * @option top left
 * @option top center
 * @option top right
 * @option middle left
 * @option middle center
 * @option middle right
 * @option bottom left
 * @option bottom center
 * @option bottom right
 * @desc Where on the battler would you like to place the icon?
 * @default bottom center
 *
 * @param EnemyOffsetX:num
 * @text Offset X
 * @parent Enemies
 * @desc How much to offset the icon X position by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param EnemyOffsetY:num
 * @text Offset Y
 * @parent Enemies
 * @desc How much to offset the icon Y position by?
 * Negative goes up. Positive goes down.
 * @default +8
 *
 * @param NameAttachShieldIcon:eval
 * @text Name: Attach Shields
 * @parent Enemies
 * @type boolean
 * @on Attach
 * @off Normal Position
 * @desc Attach the Break Shield icon to the enemy name?
 * Overrides direct attachment. Requires VisuMZ_1_BattleCore!
 * @default true
 *
 * @param AttachShieldOffsetX:num
 * @text Attach: Offset X
 * @parent NameAttachShieldIcon:eval
 * @desc How much to offset the attached icon's X position by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param AttachShieldOffsetY:num
 * @text Attach: Offset Y
 * @parent NameAttachShieldIcon:eval
 * @desc How much to offset the attached icon's Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param BattleStatus
 * @text Battle Status
 *
 * @param BattleStatusDisplayIcons:eval
 * @text Show Break Shields?
 * @parent BattleStatus
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show Break Shield icons in the Battle Status?
 * @default true
 *
 * @param BattleStatusAutoPosition:eval
 * @text Auto-Position?
 * @parent BattleStatus
 * @type boolean
 * @on Auto-Position
 * @off Manual Position
 * @desc Automatically position the Break Shield icon?
 * If not, it'll position it to the upper left.
 * @default true
 *
 * @param BattleStatusOffsetX:num
 * @text Offset X
 * @parent BattleStatus
 * @desc How much to offset the icon X position by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param BattleStatusOffsetY:num
 * @text Offset Y
 * @parent BattleStatus
 * @desc How much to offset the icon Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param MenuStatus
 * @text Menu Status
 *
 * @param MenuStatusBreakShieldIcons:eval
 * @text Show Break Shields?
 * @parent MenuStatus
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show Break Shield icons in the menu scenes?
 * @default true
 *
 */
//=============================================================================

const _0x501d=['isSTB','updateFrame','ARRAYSTRUCT','BREAK_SHIELDS_DISPLAY_ICONS','_stateTurns','BREAK_SHIELD_BATTLER_ATTACH_OFFSET_Y','initialize','originalElementRate','addState','drawItemStatusBreakShieldsDefault','isAppeared','executeBreakShieldReduction','BREAK_SHIELDS_ENEMIES','RegExp','getElementIdWithName','BREAK_SHIELD_BATTLER_ATTACH_ICON_NAME','clamp','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','addChild','resetBreakShield','1pFVuYx','updateBreakShieldIconSprite','190524CBKbgR','isAffectedByBreakShield','BREAK_SHIELDS_BASE','43387CCZggk','drawItemStatusBreakBattleCore','Mechanics','BREAK_SHIELDS_DISPLAY_OFFSET_Y','ActorDisplayPosition','min','toUpperCase','BREAK_SHIELDS_STUN_STATE','BattleCore','Game_Battler_removeBattleStates','itemRectWithPadding','itemRect','Sprite_EnemyName_updateAttachedSprites','EnemyDisplayIcon','getProtectedWeaknessElements','32124SapznL','MinRate','fontFace','map','StunAniID','createBreakShieldIconSprite','Game_BattlerBase_elementRate','BREAK_SHIELDS_DISPLAY_OFFSET_X','BREAK_SHIELDS_MENU_ICONS','round','279952GVUZXD','356449TFPOAx','BREAK_SHIELDS_ACTORS','item','currentClass','nameY','createNumberDisplay','version','drawItemStatus','EnemyDisplayPosition','Sprite_Enemy_setBattler','stbCannotBeExploited','contains','createAttachedSprites','ConvertParams','breakShield_StunTurns','floor','NameAttachShieldIcon','_inBattle','Base','FontSize','update','BreakShields','ShieldIcon','ARRAYFUNC','BREAK_SHIELDS_DEFAULT_REDUCTION','iconIndex','updateAutoPosition','ReduceAniID','anchor','BattleStatusDisplayIcons','placeBreakShieldIcon','Game_Actor_refresh','replace','STRUCT','_enemy','ProtectIcon','BREAK_SHIELDS_REDUCE_ANIMATION','drawItemStatusBreakShields','_needRefreshAllEnemyWeaknessWindows','match','test','ActorOffsetX','shouldDisplay','setBreakShield','applyChangeBreakShield','ARRAYJSON','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','drawText','requestFauxAnimation','iconWidth','alterBreakShield','show','_displayValue','VisuMZ_1_BattleCore','topBreakShield','Settings','BREAK_SHIELD_BATTLER_DISPLAY_ICON','_battler','loadSystem','fontSize','isActor','shouldDisplayBreakShields','split','setup','ARRAYNUM','isEnemy','Game_Action_executeDamage','portrait','findTargetSprite','refresh','_autoPositioning','_iconIndex','resetBreakShields','isHpEffect','_elementIDs','max','BattleManager_setup','BaseBreakShields','ProtectedElements','executeDamage','Reduction','BREAK_SHIELD_BATTLER_DISPLAY_POSITION','elementRate','isBreakShieldIconDisplayed','EnemyOffsetX','isBreakStunned','elements','compatibilityVisible','isSTBExploitSystemEnabled','call','BattleStatusOffsetY','currentBreakShield','updateAttachedSprites','Game_Action_applyItemUserEffect','create','bitmap','AlterBreakShield','move','list','BREAK_SHIELDS_DISPLAY_AUTO','Window_BattleStatus_drawItemStatus','AttachShieldOffsetY','createInnerSprite','_numberValue','parse','exit','Max','format','_currentBreakShield','VisuMZ_0_CoreEngine','isSceneBattle','BREAK_SHIELD_BATTLER_DISPLAY_OFFSET_X','Sprite_Enemy_initMembers','isDead','NUM','drawActorIcons','BattleStatusOffsetX','updateIcon','StunIcon','prototype','status','AffectEnemies','3VkKysx','BattleStatusAutoPosition','actor%1-breakShieldIcon','64885fOIpLp','constructor','battleLayoutStyle','enemy','width','breakShield_ShieldIcon','breakShield_ProtectIcon','ActorDisplayIcon','startBreakShieldReduceAnimation','Sprite_EnemyName_createAttachedSprites','applyItemUserEffect','description','_lineHeight','actor','textWidth','ActorOffsetY','traitObjects','calcElementRate','EnemyOffsetY','BREAK_SHIELD_BATTLER_DISPLAY_OFFSET_Y','STR','_scene','AddedBreakShields','FUNC','setFrame','BREAK_SHIELDS_STUN_ANIMATION','BattleLayout','setBattler','BREAK_SHIELDS_MAX','addedBreakShields','loadBitmap','baseBreakShield','ShowStunTurns','isStateAffected','VisuMZ_2_BattleSystemSTB','itemBreakShieldReduction','updateNumber','_numberSprite','startBreakShieldBrokenAnimation','_breakShieldSprite','_resettingBreakShield','_showWeaknessDisplayDuration','_spriteset','BreakReduce','AttachShieldOffsetX','initMembers','iconHeight','border','filter','Window_StatusBase_drawActorIcons','BREAK_SHIELDS_MINIMUM_WEAKNESS_RATE','note','StunState','BREAK_SHIELD_BATTLER_ATTACH_OFFSET_X','applyBreakStun','110899xYlHbD','clear'];const _0xfece=function(_0x5972ea,_0x31a2e3){_0x5972ea=_0x5972ea-0x71;let _0x501ddd=_0x501d[_0x5972ea];return _0x501ddd;};const _0x105f13=_0xfece;(function(_0x2ff794,_0x804133){const _0x3436a2=_0xfece;while(!![]){try{const _0xe10f2=parseInt(_0x3436a2(0xd0))+-parseInt(_0x3436a2(0xe6))*parseInt(_0x3436a2(0x105))+-parseInt(_0x3436a2(0xfa))*-parseInt(_0x3436a2(0x96))+parseInt(_0x3436a2(0xe8))+-parseInt(_0x3436a2(0xeb))+-parseInt(_0x3436a2(0x99))+parseInt(_0x3436a2(0x104));if(_0xe10f2===_0x804133)break;else _0x2ff794['push'](_0x2ff794['shift']());}catch(_0x46935e){_0x2ff794['push'](_0x2ff794['shift']());}}}(_0x501d,0x34022));var label='BreakShields',tier=tier||0x0,dependencies=[],pluginData=$plugins[_0x105f13(0xc9)](function(_0x308be8){const _0x250f81=_0x105f13;return _0x308be8[_0x250f81(0x94)]&&_0x308be8[_0x250f81(0xa4)]['includes']('['+label+']');})[0x0];VisuMZ[label][_0x105f13(0x13c)]=VisuMZ[label][_0x105f13(0x13c)]||{},VisuMZ['ConvertParams']=function(_0x1bb50d,_0x127cae){const _0x3fed07=_0x105f13;for(const _0x5d4c57 in _0x127cae){if(_0x5d4c57['match'](/(.*):(.*)/i)){const _0x5b0511=String(RegExp['$1']),_0x670ebc=String(RegExp['$2'])[_0x3fed07(0xf1)]()['trim']();let _0x5de240,_0x45dcf5,_0x57a3c3;switch(_0x670ebc){case _0x3fed07(0x8e):_0x5de240=_0x127cae[_0x5d4c57]!==''?Number(_0x127cae[_0x5d4c57]):0x0;break;case _0x3fed07(0x145):_0x45dcf5=_0x127cae[_0x5d4c57]!==''?JSON[_0x3fed07(0x84)](_0x127cae[_0x5d4c57]):[],_0x5de240=_0x45dcf5[_0x3fed07(0xfd)](_0x3fd529=>Number(_0x3fd529));break;case'EVAL':_0x5de240=_0x127cae[_0x5d4c57]!==''?eval(_0x127cae[_0x5d4c57]):null;break;case'ARRAYEVAL':_0x45dcf5=_0x127cae[_0x5d4c57]!==''?JSON['parse'](_0x127cae[_0x5d4c57]):[],_0x5de240=_0x45dcf5[_0x3fed07(0xfd)](_0x5f270a=>eval(_0x5f270a));break;case'JSON':_0x5de240=_0x127cae[_0x5d4c57]!==''?JSON[_0x3fed07(0x84)](_0x127cae[_0x5d4c57]):'';break;case _0x3fed07(0x132):_0x45dcf5=_0x127cae[_0x5d4c57]!==''?JSON['parse'](_0x127cae[_0x5d4c57]):[],_0x5de240=_0x45dcf5[_0x3fed07(0xfd)](_0x58a027=>JSON[_0x3fed07(0x84)](_0x58a027));break;case _0x3fed07(0xb0):_0x5de240=_0x127cae[_0x5d4c57]!==''?new Function(JSON['parse'](_0x127cae[_0x5d4c57])):new Function('return\x200');break;case _0x3fed07(0x11c):_0x45dcf5=_0x127cae[_0x5d4c57]!==''?JSON[_0x3fed07(0x84)](_0x127cae[_0x5d4c57]):[],_0x5de240=_0x45dcf5[_0x3fed07(0xfd)](_0x78a9a9=>new Function(JSON['parse'](_0x78a9a9)));break;case _0x3fed07(0xad):_0x5de240=_0x127cae[_0x5d4c57]!==''?String(_0x127cae[_0x5d4c57]):'';break;case'ARRAYSTR':_0x45dcf5=_0x127cae[_0x5d4c57]!==''?JSON[_0x3fed07(0x84)](_0x127cae[_0x5d4c57]):[],_0x5de240=_0x45dcf5[_0x3fed07(0xfd)](_0x52860b=>String(_0x52860b));break;case _0x3fed07(0x126):_0x57a3c3=_0x127cae[_0x5d4c57]!==''?JSON[_0x3fed07(0x84)](_0x127cae[_0x5d4c57]):{},_0x5de240=VisuMZ[_0x3fed07(0x112)]({},_0x57a3c3);break;case _0x3fed07(0xd4):_0x45dcf5=_0x127cae[_0x5d4c57]!==''?JSON['parse'](_0x127cae[_0x5d4c57]):[],_0x5de240=_0x45dcf5['map'](_0x2318ca=>VisuMZ[_0x3fed07(0x112)]({},JSON[_0x3fed07(0x84)](_0x2318ca)));break;default:continue;}_0x1bb50d[_0x5b0511]=_0x5de240;}}return _0x1bb50d;},(_0x5a5e20=>{const _0xbf123e=_0x105f13,_0x5912ca=_0x5a5e20['name'];for(const _0xf43880 of dependencies){if(!Imported[_0xf43880]){alert(_0xbf123e(0x133)[_0xbf123e(0x87)](_0x5912ca,_0xf43880)),SceneManager['exit']();break;}}const _0x233ad9=_0x5a5e20[_0xbf123e(0xa4)];if(_0x233ad9[_0xbf123e(0x12c)](/\[Version[ ](.*?)\]/i)){const _0x1c939f=Number(RegExp['$1']);_0x1c939f!==VisuMZ[label][_0xbf123e(0x10b)]&&(alert(_0xbf123e(0xe3)[_0xbf123e(0x87)](_0x5912ca,_0x1c939f)),SceneManager[_0xbf123e(0x85)]());}if(_0x233ad9[_0xbf123e(0x12c)](/\[Tier[ ](\d+)\]/i)){const _0x3107d8=Number(RegExp['$1']);_0x3107d8<tier?(alert('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0xbf123e(0x87)](_0x5912ca,_0x3107d8,tier)),SceneManager[_0xbf123e(0x85)]()):tier=Math[_0xbf123e(0x150)](_0x3107d8,tier);}VisuMZ['ConvertParams'](VisuMZ[label]['Settings'],_0x5a5e20['parameters']);})(pluginData),VisuMZ['BreakShields'][_0x105f13(0xdf)]={'BreakReduce':/<BREAK (?:REDUCE|REDUCTION):[ ](\d+)>/i,'SetBreakShield':/<(?:SET|CHANGE) BREAK (?:SHIELD|SHIELDS): (\d+)>/i,'AlterBreakShield':/<(?:INCREASE|DECREASE|ALTER) BREAK (?:SHIELD|SHIELDS): ([\+\-]\d+)>/i,'ProtectedElements':/<PROTECT (?:ELEMENT|ELEMENTS):[ ](.*)>/i,'AddedBreakShields':/<BREAK (?:SHIELD|SHIELDS): ([\+\-]\d+)>/i,'BaseBreakShields':/<BREAK (?:SHIELD|SHIELDS): (\d+)>/i},DataManager[_0x105f13(0xe0)]=function(_0x5f5cc2){const _0x252400=_0x105f13;_0x5f5cc2=_0x5f5cc2['toUpperCase']()['trim'](),this[_0x252400(0x14f)]=this['_elementIDs']||{};if(this[_0x252400(0x14f)][_0x5f5cc2])return this[_0x252400(0x14f)][_0x5f5cc2];let _0xe23f8=0x1;for(const _0x206f07 of $dataSystem[_0x252400(0x72)]){if(!_0x206f07)continue;let _0x3c660e=_0x206f07['toUpperCase']();_0x3c660e=_0x3c660e[_0x252400(0x125)](/\x1I\[(\d+)\]/gi,''),_0x3c660e=_0x3c660e[_0x252400(0x125)](/\\I\[(\d+)\]/gi,''),this[_0x252400(0x14f)][_0x3c660e]=_0xe23f8,_0xe23f8++;}return this[_0x252400(0x14f)][_0x5f5cc2]||0x0;},ImageManager[_0x105f13(0x9e)]=VisuMZ['BreakShields'][_0x105f13(0x13c)]['UI'][_0x105f13(0x11b)],ImageManager['breakShield_StunIcon']=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0x92)],ImageManager[_0x105f13(0x113)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xb9)],ImageManager[_0x105f13(0x9f)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0x128)],SceneManager[_0x105f13(0x8a)]=function(){const _0x3d76e3=_0x105f13;return this['_scene']&&this[_0x3d76e3(0xae)][_0x3d76e3(0x9a)]===Scene_Battle;},VisuMZ[_0x105f13(0x11a)][_0x105f13(0x151)]=BattleManager[_0x105f13(0x144)],BattleManager[_0x105f13(0x144)]=function(_0x4e0555,_0x1d0e08,_0x3cc0bf){const _0x1a1cf8=_0x105f13;VisuMZ[_0x1a1cf8(0x11a)][_0x1a1cf8(0x151)][_0x1a1cf8(0x75)](this,_0x4e0555,_0x1d0e08,_0x3cc0bf),$gameParty['resetBreakShields'](),$gameTroop[_0x1a1cf8(0x14d)]();},Game_Action[_0x105f13(0xcb)]=VisuMZ[_0x105f13(0x11a)]['Settings']['Mechanics'][_0x105f13(0xfb)],Game_Action['BREAK_SHIELDS_DEFAULT_REDUCTION']=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)][_0x105f13(0xed)][_0x105f13(0x155)],VisuMZ['BreakShields'][_0x105f13(0x147)]=Game_Action[_0x105f13(0x93)][_0x105f13(0x154)],Game_Action['prototype'][_0x105f13(0x154)]=function(_0x280667,_0x3bc5cc){const _0x404c9a=_0x105f13;VisuMZ[_0x404c9a(0x11a)]['Game_Action_executeDamage'][_0x404c9a(0x75)](this,_0x280667,_0x3bc5cc),!!_0x280667&&_0x3bc5cc>0x0&&_0x280667[_0x404c9a(0xe9)]()&&this[_0x404c9a(0x14e)]()&&this[_0x404c9a(0xdd)](_0x280667,_0x3bc5cc);},Game_Action[_0x105f13(0x93)][_0x105f13(0xdd)]=function(_0x3f5598,_0x6dbedf){const _0x2dfaee=_0x105f13;if(!_0x3f5598[_0x2dfaee(0x71)]()){var _0x1e9710=this[_0x2dfaee(0xaa)](_0x3f5598);if(_0x1e9710>=Game_Action[_0x2dfaee(0xcb)]){var _0x6dbedf=-0x1*this[_0x2dfaee(0xbc)]();_0x3f5598[_0x2dfaee(0xa1)](),_0x3f5598[_0x2dfaee(0x137)](_0x6dbedf);}}},Game_Action[_0x105f13(0x93)]['itemBreakShieldReduction']=function(){const _0x37da63=_0x105f13,_0x49340d=VisuMZ['BreakShields']['RegExp'];return this[_0x37da63(0x107)]()[_0x37da63(0xcc)]['match'](_0x49340d[_0x37da63(0xc4)])?parseInt(RegExp['$1']):Game_Action[_0x37da63(0x11d)];},VisuMZ[_0x105f13(0x11a)][_0x105f13(0x79)]=Game_Action[_0x105f13(0x93)]['applyItemUserEffect'],Game_Action['prototype'][_0x105f13(0xa3)]=function(_0x7e1665){const _0x46b554=_0x105f13;VisuMZ[_0x46b554(0x11a)][_0x46b554(0x79)]['call'](this,_0x7e1665),!!_0x7e1665&&_0x7e1665[_0x46b554(0xe9)]()&&this['applyChangeBreakShield'](_0x7e1665);},Game_Action[_0x105f13(0x93)][_0x105f13(0x131)]=function(_0x5a9420){const _0x434169=_0x105f13;if(!_0x5a9420[_0x434169(0x71)]()){const _0x2fa5cb=VisuMZ[_0x434169(0x11a)][_0x434169(0xdf)];this[_0x434169(0x107)]()['note']['match'](_0x2fa5cb['SetBreakShield'])&&(_0x5a9420[_0x434169(0x130)](parseInt(RegExp['$1'])),$gameTemp[_0x434169(0x12b)]=!![]),this[_0x434169(0x107)]()['note'][_0x434169(0x12c)](_0x2fa5cb[_0x434169(0x7c)])&&(_0x5a9420[_0x434169(0x137)](parseInt(RegExp['$1'])),$gameTemp[_0x434169(0x12b)]=!![]);}},VisuMZ[_0x105f13(0x11a)][_0x105f13(0x100)]=Game_BattlerBase['prototype'][_0x105f13(0x157)],Game_BattlerBase[_0x105f13(0x93)]['elementRate']=function(_0x17e131){const _0x19e5f0=_0x105f13;var _0x22810c=VisuMZ[_0x19e5f0(0x11a)]['Game_BattlerBase_elementRate'][_0x19e5f0(0x75)](this,_0x17e131);return this[_0x19e5f0(0xf9)]()[_0x19e5f0(0x110)](_0x17e131)?Math[_0x19e5f0(0xf0)](0x1,_0x22810c):_0x22810c;},Game_BattlerBase[_0x105f13(0x93)][_0x105f13(0xd9)]=function(_0x53aca1){const _0x113d13=_0x105f13;return VisuMZ[_0x113d13(0x11a)]['Game_BattlerBase_elementRate'][_0x113d13(0x75)](this,_0x53aca1);},Game_Battler[_0x105f13(0xea)]=VisuMZ[_0x105f13(0x11a)]['Settings'][_0x105f13(0xed)][_0x105f13(0x117)],Game_Battler[_0x105f13(0xb5)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['Mechanics'][_0x105f13(0x86)],Game_Battler[_0x105f13(0xf2)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)][_0x105f13(0xed)][_0x105f13(0xcd)],Game_Battler[_0x105f13(0x129)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)][_0x105f13(0xed)][_0x105f13(0x120)],Game_Battler['BREAK_SHIELDS_STUN_ANIMATION']=VisuMZ['BreakShields'][_0x105f13(0x13c)][_0x105f13(0xed)][_0x105f13(0xfe)],Game_Battler[_0x105f13(0x106)]=VisuMZ[_0x105f13(0x11a)]['Settings'][_0x105f13(0xed)]['AffectActors'],Game_Battler['BREAK_SHIELDS_ENEMIES']=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)][_0x105f13(0xed)][_0x105f13(0x95)],VisuMZ[_0x105f13(0x11a)][_0x105f13(0xf4)]=Game_Battler[_0x105f13(0x93)]['removeBattleStates'],Game_Battler[_0x105f13(0x93)]['removeBattleStates']=function(){const _0x318357=_0x105f13;VisuMZ[_0x318357(0x11a)][_0x318357(0xf4)][_0x318357(0x75)](this),this[_0x318357(0xe5)]();},Game_Battler[_0x105f13(0x93)][_0x105f13(0xe9)]=function(){return![];},Game_Battler[_0x105f13(0x93)][_0x105f13(0xe5)]=function(){const _0x1d8b0c=_0x105f13;this[_0x1d8b0c(0xe9)]()&&this[_0x1d8b0c(0x130)](this[_0x1d8b0c(0x13b)]());},Game_Battler['prototype'][_0x105f13(0xb8)]=function(){const _0x360c4a=_0x105f13;return Game_Battler[_0x360c4a(0xea)];},Game_Battler[_0x105f13(0x93)]['topBreakShield']=function(){const _0x145e11=_0x105f13;var _0x55298e=this[_0x145e11(0xb8)]();return _0x55298e=this[_0x145e11(0xb6)](_0x55298e),_0x55298e[_0x145e11(0xe2)](0x1,Game_Battler['BREAK_SHIELDS_MAX']);},Game_Battler[_0x105f13(0x93)][_0x105f13(0xb6)]=function(_0x447249){const _0x4ad1a2=_0x105f13,_0x30f2b7=VisuMZ['BreakShields'][_0x4ad1a2(0xdf)];for(const _0x5638c0 of this['traitObjects']()){if(!_0x5638c0)continue;_0x5638c0['note'][_0x4ad1a2(0x12c)](_0x30f2b7[_0x4ad1a2(0xaf)])&&(_0x447249+=Number(RegExp['$1'])||0x0);}return _0x447249;},Game_Battler['prototype'][_0x105f13(0x77)]=function(){const _0xb873bd=_0x105f13;return this[_0xb873bd(0x88)]===undefined&&this['setBreakShield'](this[_0xb873bd(0x13b)]()),this[_0xb873bd(0x88)];},Game_Battler[_0x105f13(0x93)][_0x105f13(0x130)]=function(_0x44616b){const _0x501c9f=_0x105f13;this[_0x501c9f(0xe9)]()&&(this['_currentBreakShield']=Math['ceil'](_0x44616b),this['_currentBreakShield']=this['_currentBreakShield'][_0x501c9f(0xe2)](0x0,Game_Battler[_0x501c9f(0xb5)]),this['_currentBreakShield']<=0x0&&this[_0x501c9f(0xcf)](),this[_0x501c9f(0x14a)]());},Game_Battler[_0x105f13(0x93)][_0x105f13(0x137)]=function(_0x262017){const _0x45adea=_0x105f13;this[_0x45adea(0x130)](this['currentBreakShield']()+_0x262017);},Game_Battler['prototype'][_0x105f13(0xcf)]=function(){const _0x40fdf1=_0x105f13;this[_0x40fdf1(0x130)](this[_0x40fdf1(0x13b)]());var _0x50d464=Game_Battler[_0x40fdf1(0xf2)];this[_0x40fdf1(0xda)](_0x50d464),this[_0x40fdf1(0xbf)]();},Game_Battler['prototype'][_0x105f13(0x71)]=function(){const _0x5a1288=_0x105f13;return this[_0x5a1288(0xba)](Game_Battler[_0x5a1288(0xf2)]);},Game_Battler[_0x105f13(0x93)][_0x105f13(0xa1)]=function(){const _0x1f979d=_0x105f13;if(Imported[_0x1f979d(0x89)]&&Game_Battler[_0x1f979d(0x129)]){var _0x4c8972=Game_Battler[_0x1f979d(0x129)];$gameTemp['requestFauxAnimation']([this],_0x4c8972,![],![]);}},Game_Battler[_0x105f13(0x93)][_0x105f13(0xbf)]=function(){const _0x19201f=_0x105f13;if(Imported[_0x19201f(0x89)]&&Game_Battler[_0x19201f(0xb2)]){var _0x49310e=Game_Battler[_0x19201f(0xb2)];$gameTemp[_0x19201f(0x135)]([this],_0x49310e,![],![]);}},Game_Battler[_0x105f13(0x93)][_0x105f13(0xf9)]=function(){const _0x4052f2=_0x105f13,_0x5dd5b2=VisuMZ[_0x4052f2(0x11a)][_0x4052f2(0xdf)];let _0x2805b1=[];for(const _0x147ab2 of this[_0x4052f2(0xa9)]()){if(!_0x147ab2)continue;if(_0x147ab2[_0x4052f2(0xcc)][_0x4052f2(0x12c)](_0x5dd5b2[_0x4052f2(0x153)])){const _0x501446=RegExp['$1'][_0x4052f2(0x143)](',')[_0x4052f2(0xfd)](_0x4184e6=>_0x4184e6['trim']());for(const _0x298753 of _0x501446){const _0x28079=/^\d+$/[_0x4052f2(0x12d)](_0x298753);if(_0x28079)_0x2805b1['push'](Number(_0x298753));else{const _0x5a750e=DataManager['getElementIdWithName'](_0x298753);if(_0x5a750e)_0x2805b1['push'](_0x5a750e);}}}}return _0x2805b1['sort'](function(_0x291e9c,_0x18e4dd){return _0x291e9c-_0x18e4dd;}),_0x2805b1;},Game_Actor[_0x105f13(0x93)][_0x105f13(0xe9)]=function(){const _0x426ce0=_0x105f13;if(Imported[_0x426ce0(0xbb)]&&BattleManager[_0x426ce0(0xd2)]()&&BattleManager[_0x426ce0(0x74)]())return this[_0x426ce0(0x10f)]()?!![]:![];return Game_Battler[_0x426ce0(0x106)];},Game_Actor['prototype'][_0x105f13(0xb8)]=function(){const _0x464ae9=_0x105f13,_0x8f6d5e=VisuMZ[_0x464ae9(0x11a)][_0x464ae9(0xdf)];let _0x3842b7=Game_Battler[_0x464ae9(0x93)][_0x464ae9(0xb8)][_0x464ae9(0x75)](this);if(!!this[_0x464ae9(0x108)]()&&this[_0x464ae9(0x108)]()[_0x464ae9(0xcc)][_0x464ae9(0x12c)](_0x8f6d5e[_0x464ae9(0x152)]))_0x3842b7=parseInt(RegExp['$1']);else this[_0x464ae9(0xa6)]()&&this[_0x464ae9(0xa6)]()[_0x464ae9(0xcc)][_0x464ae9(0x12c)](_0x8f6d5e[_0x464ae9(0x152)])&&(_0x3842b7=parseInt(RegExp['$1']));return Math[_0x464ae9(0x150)](0x1,_0x3842b7);},VisuMZ[_0x105f13(0x11a)][_0x105f13(0x124)]=Game_Actor['prototype'][_0x105f13(0x14a)],Game_Actor[_0x105f13(0x93)][_0x105f13(0x14a)]=function(){const _0x5230db=_0x105f13;VisuMZ[_0x5230db(0x11a)]['Game_Actor_refresh'][_0x5230db(0x75)](this),!$gameParty['inBattle']()&&!this['_resettingBreakShield']&&(this[_0x5230db(0xc1)]=!![],this['resetBreakShield'](),this['_resettingBreakShield']=undefined);},Game_Enemy[_0x105f13(0x93)]['isAffectedByBreakShield']=function(){const _0x1b0384=_0x105f13;if(Imported['VisuMZ_2_BattleSystemSTB']&&BattleManager[_0x1b0384(0xd2)]()&&BattleManager[_0x1b0384(0x74)]())return this['stbCannotBeExploited']()?!![]:![];return Game_Battler[_0x1b0384(0xde)];},Game_Enemy[_0x105f13(0x93)][_0x105f13(0xb8)]=function(){const _0x468363=_0x105f13,_0x1ac2bc=VisuMZ[_0x468363(0x11a)]['RegExp'];let _0x1c83d3=Game_Battler[_0x468363(0x93)][_0x468363(0xb8)][_0x468363(0x75)](this);return this[_0x468363(0x9c)]()&&this[_0x468363(0x9c)]()[_0x468363(0xcc)]['match'](_0x1ac2bc[_0x468363(0x152)])&&(_0x1c83d3=parseInt(RegExp['$1'])),Math[_0x468363(0x150)](0x1,_0x1c83d3);},Game_Unit[_0x105f13(0x93)][_0x105f13(0x14d)]=function(){const _0x507df5=_0x105f13;var _0x316b37=this[_0x507df5(0x116)];this['_inBattle']=![];for(const _0x2e9e83 of this['members']()){_0x2e9e83&&_0x2e9e83['resetBreakShield']();}this['_inBattle']=_0x316b37;},Sprite_Battler[_0x105f13(0x93)][_0x105f13(0xff)]=function(){const _0x2a4620=_0x105f13;this[_0x2a4620(0xc0)]=new Sprite_BreakShieldIcon(),this[_0x2a4620(0xe4)](this[_0x2a4620(0xc0)]);},Sprite_Actor[_0x105f13(0x13d)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xa0)],Sprite_Actor[_0x105f13(0x156)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xef)],Sprite_Actor[_0x105f13(0x8b)]=VisuMZ['BreakShields'][_0x105f13(0x13c)]['UI'][_0x105f13(0x12e)],Sprite_Actor[_0x105f13(0xac)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xa8)],VisuMZ[_0x105f13(0x11a)]['Sprite_Actor_initMembers']=Sprite_Actor[_0x105f13(0x93)][_0x105f13(0xc6)],Sprite_Actor[_0x105f13(0x93)][_0x105f13(0xc6)]=function(){const _0x1826e0=_0x105f13;VisuMZ['BreakShields']['Sprite_Actor_initMembers']['call'](this),this[_0x1826e0(0x158)]()&&this[_0x1826e0(0xff)]();},Sprite_Actor[_0x105f13(0x93)][_0x105f13(0x158)]=function(){const _0x471685=_0x105f13;return Sprite_Actor[_0x471685(0x13d)]&&this[_0x471685(0x9a)]===Sprite_Actor;},VisuMZ[_0x105f13(0x11a)]['Sprite_Actor_setBattler']=Sprite_Actor['prototype'][_0x105f13(0xb4)],Sprite_Actor[_0x105f13(0x93)][_0x105f13(0xb4)]=function(_0x44ab95){const _0x55eca7=_0x105f13;VisuMZ['BreakShields']['Sprite_Actor_setBattler'][_0x55eca7(0x75)](this,_0x44ab95),this[_0x55eca7(0xc0)]&&this[_0x55eca7(0xc0)]['setup'](this['_actor'],!![]);},Sprite_Enemy[_0x105f13(0x13d)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xf8)],Sprite_Enemy[_0x105f13(0x156)]=VisuMZ[_0x105f13(0x11a)]['Settings']['UI'][_0x105f13(0x10d)],Sprite_Enemy[_0x105f13(0x8b)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0x159)],Sprite_Enemy[_0x105f13(0xac)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xab)],Sprite_Enemy['BREAK_SHIELD_BATTLER_ATTACH_ICON_NAME']=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0x115)],Sprite_Enemy[_0x105f13(0xce)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0xc5)],Sprite_Enemy[_0x105f13(0xd7)]=VisuMZ[_0x105f13(0x11a)]['Settings']['UI'][_0x105f13(0x81)],VisuMZ[_0x105f13(0x11a)][_0x105f13(0x8c)]=Sprite_Enemy[_0x105f13(0x93)][_0x105f13(0xc6)],Sprite_Enemy[_0x105f13(0x93)][_0x105f13(0xc6)]=function(){const _0x40b5b6=_0x105f13;VisuMZ[_0x40b5b6(0x11a)][_0x40b5b6(0x8c)][_0x40b5b6(0x75)](this),this['isBreakShieldIconDisplayed']()&&this[_0x40b5b6(0xff)]();},Sprite_Enemy[_0x105f13(0x93)][_0x105f13(0x158)]=function(){const _0x3786e5=_0x105f13;return Imported['VisuMZ_1_BattleCore']&&Sprite_Enemy[_0x3786e5(0xe1)]?![]:Sprite_Enemy['BREAK_SHIELD_BATTLER_DISPLAY_ICON'];},VisuMZ['BreakShields'][_0x105f13(0x10e)]=Sprite_Enemy[_0x105f13(0x93)]['setBattler'],Sprite_Enemy[_0x105f13(0x93)][_0x105f13(0xb4)]=function(_0x5eb986){const _0x3ef1c7=_0x105f13;VisuMZ[_0x3ef1c7(0x11a)][_0x3ef1c7(0x10e)][_0x3ef1c7(0x75)](this,_0x5eb986),this[_0x3ef1c7(0xc0)]&&this[_0x3ef1c7(0xc0)]['setup'](this[_0x3ef1c7(0x127)],!![]);};Imported[_0x105f13(0x13a)]&&(VisuMZ['BreakShields']['Sprite_EnemyName_compatibilityVisible']=Sprite_EnemyName[_0x105f13(0x93)][_0x105f13(0x73)],Sprite_EnemyName[_0x105f13(0x93)][_0x105f13(0x73)]=function(){const _0x185a85=_0x105f13;if(Imported['VisuMZ_3_WeaknessDisplay']&&Sprite_Enemy['BREAK_SHIELD_BATTLER_ATTACH_ICON_NAME']){if(this[_0x185a85(0x13e)][_0x185a85(0xc2)]&&this[_0x185a85(0x13e)]['_showWeaknessDisplayDuration']>0x0)return!![];}return VisuMZ[_0x185a85(0x11a)]['Sprite_EnemyName_compatibilityVisible']['call'](this);});;function Sprite_BreakShieldIcon(){this['initialize'](...arguments);}Sprite_BreakShieldIcon[_0x105f13(0x93)]=Object[_0x105f13(0x7a)](Sprite['prototype']),Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0x9a)]=Sprite_BreakShieldIcon,Sprite_BreakShieldIcon['prototype']['initialize']=function(){const _0x413861=_0x105f13;Sprite[_0x413861(0x93)][_0x413861(0xd8)][_0x413861(0x75)](this),this[_0x413861(0xc6)](),this[_0x413861(0xb7)](),this['createNumberDisplay']();},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0xc6)]=function(){const _0x20f3d7=_0x105f13;this[_0x20f3d7(0x13e)]=null,this[_0x20f3d7(0x14b)]=![],this[_0x20f3d7(0x14c)]=0x0,this[_0x20f3d7(0x83)]='',this['_displayValue']='',this[_0x20f3d7(0x121)]['x']=0.5,this['anchor']['y']=0.5;},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0xb7)]=function(){const _0x48519d=_0x105f13;this[_0x48519d(0x7b)]=ImageManager[_0x48519d(0x13f)]('IconSet'),this[_0x48519d(0xb1)](0x0,0x0,0x0,0x0);},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0x10a)]=function(){const _0x58aa9b=_0x105f13;this['_numberSprite']=new Sprite(),this[_0x58aa9b(0xbe)][_0x58aa9b(0x7b)]=new Bitmap(ImageManager[_0x58aa9b(0x136)],ImageManager['iconHeight']),this[_0x58aa9b(0xbe)]['anchor']['x']=0.5,this[_0x58aa9b(0xbe)][_0x58aa9b(0x121)]['y']=0.5,this[_0x58aa9b(0xe4)](this[_0x58aa9b(0xbe)]);},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0x144)]=function(_0x4a931b,_0x2ebfe3){this['_battler']!==_0x4a931b&&(this['_battler']=_0x4a931b),this['_autoPositioning']=_0x2ebfe3;},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0x119)]=function(){const _0x52603b=_0x105f13;Sprite['prototype'][_0x52603b(0x119)]['call'](this),this[_0x52603b(0x12f)]()?(this['opacity']=0xff,this[_0x52603b(0x91)](),this[_0x52603b(0xd3)](),this['updateNumber'](),this[_0x52603b(0x11f)]()):this['opacity']=0x0;},Sprite_BreakShieldIcon[_0x105f13(0x93)]['shouldDisplay']=function(){const _0x5506ff=_0x105f13;return this['_battler']&&this[_0x5506ff(0x13e)][_0x5506ff(0xdc)]()&&this['_battler'][_0x5506ff(0xe9)]();},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0x91)]=function(){const _0x150873=_0x105f13;if(this[_0x150873(0x13e)][_0x150873(0x8d)]()){const _0x49d342=$dataStates[this[_0x150873(0x13e)]['deathStateId']()];_0x49d342&&_0x49d342[_0x150873(0x11e)]>0x0?this['_iconIndex']=_0x49d342[_0x150873(0x11e)]:this[_0x150873(0x14c)]=0x0,this['_numberValue']='';}else{if(this[_0x150873(0x13e)][_0x150873(0x71)]()){const _0x259e69=$dataStates[Game_Battler[_0x150873(0xf2)]];_0x259e69&&_0x259e69[_0x150873(0x11e)]>0x0?this[_0x150873(0x14c)]=_0x259e69['iconIndex']:this[_0x150873(0x14c)]=ImageManager['breakShield_StunIcon'];if(ImageManager['breakShield_StunTurns']){this['_numberValue']=this[_0x150873(0x13e)][_0x150873(0xd6)][_0x259e69['id']]||0x0;if(this[_0x150873(0x83)]<=0x0)this['_numberValue']='';}else this[_0x150873(0x83)]='';}else this[_0x150873(0x14c)]=ImageManager[_0x150873(0x9e)],this['_numberValue']=this['_battler'][_0x150873(0x77)]();}},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0xd3)]=function(){const _0x5c4b26=_0x105f13,_0x1c1b37=ImageManager[_0x5c4b26(0x136)],_0xa77050=ImageManager[_0x5c4b26(0xc7)],_0x1db601=this[_0x5c4b26(0x14c)]%0x10*_0x1c1b37,_0x217832=Math[_0x5c4b26(0x114)](this[_0x5c4b26(0x14c)]/0x10)*_0xa77050;this['setFrame'](_0x1db601,_0x217832,_0x1c1b37,_0xa77050);},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0xbd)]=function(){const _0x2ce92d=_0x105f13;if(this[_0x2ce92d(0x139)]===this[_0x2ce92d(0x83)])return;this['_displayValue']=this['_numberValue'];const _0xccd2ec=this['_numberSprite']['bitmap'];_0xccd2ec[_0x2ce92d(0xfc)]=$gameSystem['numberFontFace'](),_0xccd2ec[_0x2ce92d(0x140)]=VisuMZ['BreakShields'][_0x2ce92d(0x13c)]['UI'][_0x2ce92d(0x118)],_0xccd2ec[_0x2ce92d(0xd1)](),_0xccd2ec[_0x2ce92d(0x134)](this[_0x2ce92d(0x139)],0x0,0x0,_0xccd2ec[_0x2ce92d(0x9d)],_0xccd2ec['height'],'center');},Sprite_BreakShieldIcon[_0x105f13(0x93)][_0x105f13(0x11f)]=function(){const _0x7e99f5=_0x105f13;if(!this[_0x7e99f5(0x14b)])return;if(!SceneManager[_0x7e99f5(0x8a)]())return;if(!SceneManager[_0x7e99f5(0xae)]['_spriteset'])return;const _0x1d811a=SceneManager[_0x7e99f5(0xae)][_0x7e99f5(0xc3)][_0x7e99f5(0x149)](this[_0x7e99f5(0x13e)]);if(!_0x1d811a)return;const _0x25c9a9=this[_0x7e99f5(0x13e)][_0x7e99f5(0x141)]()?Sprite_Actor:Sprite_Enemy,_0x280d37=_0x25c9a9['BREAK_SHIELD_BATTLER_DISPLAY_POSITION'];this['x']=0x0;if(_0x280d37[_0x7e99f5(0x12c)](/left/i))this['x']=Math[_0x7e99f5(0x114)](_0x1d811a[_0x7e99f5(0x9d)]/-0x2);else _0x280d37['match'](/right/i)&&(this['x']=Math['ceil'](_0x1d811a[_0x7e99f5(0x9d)]/0x2));this['x']+=_0x25c9a9[_0x7e99f5(0x8b)],this['y']=0x0;if(_0x280d37['match'](/top/i))this['y']=_0x1d811a['height']*-0x1;else _0x280d37[_0x7e99f5(0x12c)](/middle/i)&&(this['y']=Math[_0x7e99f5(0x103)](_0x1d811a['height']*-0.5));this['y']+=_0x25c9a9[_0x7e99f5(0xac)];};Imported[_0x105f13(0x13a)]&&Sprite_Enemy[_0x105f13(0xe1)]&&(VisuMZ[_0x105f13(0x11a)][_0x105f13(0xa2)]=Sprite_EnemyName[_0x105f13(0x93)][_0x105f13(0x111)],Sprite_EnemyName['prototype']['createAttachedSprites']=function(){const _0x155b61=_0x105f13;VisuMZ[_0x155b61(0x11a)]['Sprite_EnemyName_createAttachedSprites'][_0x155b61(0x75)](this),this[_0x155b61(0xc0)]=new Sprite_BreakShieldIcon(),this[_0x155b61(0xe4)](this['_breakShieldSprite']);},VisuMZ['BreakShields'][_0x105f13(0xf7)]=Sprite_EnemyName['prototype'][_0x105f13(0x78)],Sprite_EnemyName[_0x105f13(0x93)][_0x105f13(0x78)]=function(){const _0x27d4fa=_0x105f13;VisuMZ[_0x27d4fa(0x11a)]['Sprite_EnemyName_updateAttachedSprites'][_0x27d4fa(0x75)](this),this[_0x27d4fa(0xe7)]();},Sprite_EnemyName[_0x105f13(0x93)][_0x105f13(0xe7)]=function(){const _0x455728=_0x105f13;if(!this[_0x455728(0xc0)])return;this[_0x455728(0x13e)]!==this[_0x455728(0xc0)][_0x455728(0x13e)]&&this[_0x455728(0xc0)][_0x455728(0x144)](this[_0x455728(0x13e)],![]);const _0x2e6b1e=this[_0x455728(0xa7)]();this[_0x455728(0xa5)]=this['_lineHeight']||Window_Base['prototype']['lineHeight'](),this['_breakShieldSprite']['x']=Math[_0x455728(0x103)]((_0x2e6b1e+ImageManager['iconWidth'])/-0x2)-0x8,this[_0x455728(0xc0)]['y']=this[_0x455728(0xa5)]/0x2,this['_breakShieldSprite']['x']+=Sprite_Enemy[_0x455728(0xce)]||0x0,this['_breakShieldSprite']['y']+=Sprite_Enemy['BREAK_SHIELD_BATTLER_ATTACH_OFFSET_Y']||0x0;});;Window_StatusBase[_0x105f13(0x102)]=VisuMZ['BreakShields']['Settings']['UI']['MenuStatusBreakShieldIcons'],VisuMZ[_0x105f13(0x11a)][_0x105f13(0xca)]=Window_StatusBase[_0x105f13(0x93)]['drawActorIcons'],Window_StatusBase['prototype'][_0x105f13(0x8f)]=function(_0x1489b5,_0x29e69e,_0x10f288,_0x92cfed){const _0x24116a=_0x105f13;_0x92cfed=_0x92cfed||0x90;if(this[_0x24116a(0x142)](_0x1489b5)){const _0x5d7a53=_0x29e69e+Math[_0x24116a(0x103)](ImageManager[_0x24116a(0x136)]/0x2),_0x5ae011=_0x10f288+Math['round'](ImageManager[_0x24116a(0xc7)]/0x2)+0x2;this['placeBreakShieldIcon'](_0x1489b5,_0x5d7a53,_0x5ae011),_0x29e69e+=ImageManager['iconWidth'],_0x92cfed-=ImageManager[_0x24116a(0x136)];}VisuMZ['BreakShields'][_0x24116a(0xca)]['call'](this,_0x1489b5,_0x29e69e,_0x10f288,_0x92cfed);},Window_StatusBase['prototype'][_0x105f13(0x142)]=function(_0x33de08){const _0x250e0a=_0x105f13;if(!_0x33de08)return![];if(!Window_StatusBase[_0x250e0a(0x102)])return![];if(_0x33de08[_0x250e0a(0x141)]())return Game_Battler[_0x250e0a(0x106)];else return _0x33de08[_0x250e0a(0x146)]()?Game_Battler['BREAK_SHIELDS_ENEMIES']:!![];},Window_StatusBase[_0x105f13(0x93)][_0x105f13(0x123)]=function(_0x4fcf6c,_0x2c1caf,_0x36d06a){const _0x49948f=_0x105f13,_0xb944e3=_0x49948f(0x98)[_0x49948f(0x87)](_0x4fcf6c['actorId']()),_0x86c121=this[_0x49948f(0x82)](_0xb944e3,Sprite_BreakShieldIcon);_0x86c121[_0x49948f(0x144)](_0x4fcf6c,![]),_0x86c121[_0x49948f(0x7d)](_0x2c1caf,_0x36d06a),_0x86c121[_0x49948f(0x138)]();},Window_BattleStatus['BREAK_SHIELDS_DISPLAY_ICONS']=VisuMZ[_0x105f13(0x11a)]['Settings']['UI'][_0x105f13(0x122)],Window_BattleStatus[_0x105f13(0x7f)]=VisuMZ['BreakShields'][_0x105f13(0x13c)]['UI'][_0x105f13(0x97)],Window_BattleStatus[_0x105f13(0x101)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0x90)],Window_BattleStatus[_0x105f13(0xee)]=VisuMZ[_0x105f13(0x11a)][_0x105f13(0x13c)]['UI'][_0x105f13(0x76)],VisuMZ[_0x105f13(0x11a)][_0x105f13(0x80)]=Window_BattleStatus[_0x105f13(0x93)][_0x105f13(0x10c)],Window_BattleStatus['prototype']['drawItemStatus']=function(_0x26dad5){const _0x2b421=_0x105f13;VisuMZ['BreakShields']['Window_BattleStatus_drawItemStatus'][_0x2b421(0x75)](this,_0x26dad5),this[_0x2b421(0x12a)](_0x26dad5);},Window_BattleStatus['prototype'][_0x105f13(0x12a)]=function(_0x891aeb){const _0x1ae2b6=_0x105f13;if(!Window_BattleStatus[_0x1ae2b6(0xd5)])return;if(!Game_Battler[_0x1ae2b6(0x106)])return;const _0x555ce5=this[_0x1ae2b6(0xa6)](_0x891aeb);if(!_0x555ce5[_0x1ae2b6(0xe9)]())return;if(!Window_BattleStatus[_0x1ae2b6(0x7f)])this[_0x1ae2b6(0xdb)](_0x891aeb);else!Imported[_0x1ae2b6(0x13a)]?this[_0x1ae2b6(0xdb)](_0x891aeb):this[_0x1ae2b6(0xec)](_0x891aeb);},Window_BattleStatus[_0x105f13(0x93)][_0x105f13(0xdb)]=function(_0xfe4a13){const _0x982574=_0x105f13,_0x1533c4=this[_0x982574(0xa6)](_0xfe4a13),_0x137c1f=this[_0x982574(0xf5)](_0xfe4a13),_0xf8cad4=Math[_0x982574(0x103)](ImageManager[_0x982574(0x136)]/0x2);let _0x9cd904=_0x137c1f['x']+_0xf8cad4-0x4+Window_BattleStatus['BREAK_SHIELDS_DISPLAY_OFFSET_X'],_0x345a54=_0x137c1f['y']+_0xf8cad4+0x4+Window_BattleStatus[_0x982574(0xee)];this[_0x982574(0x123)](_0x1533c4,_0x9cd904,_0x345a54);},Window_BattleStatus[_0x105f13(0x93)][_0x105f13(0xec)]=function(_0x2d7017){const _0x2bac13=_0x105f13,_0x42fecd=this[_0x2bac13(0xa6)](_0x2d7017),_0x2bd650=this[_0x2bac13(0xf6)](_0x2d7017),_0x51cec4=Math[_0x2bac13(0x103)](_0x2bd650['x']+(_0x2bd650[_0x2bac13(0x9d)]-0x80)/0x2),_0x23ae73=this[_0x2bac13(0x109)](_0x2bd650),_0x3fdb40=Math['round'](ImageManager[_0x2bac13(0x136)]/0x2);let _0x51a7a4=_0x51cec4-_0x3fdb40-0x4,_0x457aed=_0x23ae73+_0x3fdb40;_0x51a7a4-ImageManager[_0x2bac13(0x136)]/0x2<_0x2bd650['x']&&(_0x51a7a4=_0x51cec4+_0x3fdb40-0x4,_0x457aed=_0x23ae73-_0x3fdb40);let _0x5981b8=_0x2bd650['x']+_0x3fdb40+0x4,_0x5432bc=_0x2bd650['y']+_0x3fdb40+0x4;const _0x16512a=this[_0x2bac13(0x9b)]();switch(_0x16512a){case _0x2bac13(0x7e):!VisuMZ[_0x2bac13(0xf3)][_0x2bac13(0x13c)][_0x2bac13(0xb3)]['ShowFacesListStyle']&&(_0x5981b8=_0x2bd650['x']+_0x2bd650[_0x2bac13(0x9d)]-ImageManager[_0x2bac13(0x136)]);break;case'xp':case _0x2bac13(0x148):case'default':case _0x2bac13(0xc8):_0x5981b8=_0x51a7a4,_0x5432bc=_0x457aed+ImageManager[_0x2bac13(0xc7)];break;}_0x5981b8+=Window_BattleStatus['BREAK_SHIELDS_DISPLAY_OFFSET_X'],_0x5432bc+=Window_BattleStatus[_0x2bac13(0xee)],this[_0x2bac13(0x123)](_0x42fecd,_0x5981b8,_0x5432bc);};