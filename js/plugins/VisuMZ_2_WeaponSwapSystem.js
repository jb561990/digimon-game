//=============================================================================
// VisuStella MZ - Weapon Swap System
// VisuMZ_2_WeaponSwapSystem.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_WeaponSwapSystem = true;

var VisuMZ = VisuMZ || {};
VisuMZ.WeaponSwapSystem = VisuMZ.WeaponSwapSystem || {};
VisuMZ.WeaponSwapSystem.version = 1.01;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.01] [WeaponSwapSystem]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Weapon_Swap_System_VisuStella_MZ
 * @base VisuMZ_1_BattleCore
 * @base VisuMZ_1_ItemsEquipsCore
 * @orderAfter VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_ItemsEquipsCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin adds in a Weapon Swap System. Actors can equip a different
 * weapon for each weapon type available for use. These weapons can be swapped
 * to and from during the middle of a battle. Swapping weapons can let the
 * player's team adapt to certain situations better or giving them the ability
 * to hit certain weapon weaknesses in battle.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Actors can equip multiple weapons, one for each weapon type.
 * * These weapons can be switched during the middle of battle.
 * * Choose to display only equippable weapon types in the Equip Menu or all
 *   of the possible weapon types.
 * * Have certain skills switch over to different equipped weapons when
 *   performing them.
 * * Shortcut keys to allow switching between weapon types easily when
 *   selecting commands.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_1_BattleCore
 * * VisuMZ_1_ItemsEquipsCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Dual Wielding
 * 
 * Dual Wielding properties have been disabled to allow for the Weapon Swap
 * System. There are too many conflicts between it and the Weapon Swap System.
 * There is simply no way around it.
 *
 * ---
 * 
 * Required Weapons
 * 
 * RPG Maker MZ's skills allowed for Required Weapons and needed the actor to
 * have any of the said weapon type(s) equipped upon usage. This function has
 * now been changed. Now, as long as the actor has any of the weapon types
 * available and a weapon attached to it, the actor will be able to use the
 * skill without needing to switch to that weapon first.
 * 
 * When using the skill, the actor will switch to the first available weapon
 * type if needed as long as it is a requirement.
 * 
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins or specific
 * features. This section will highlight the main plugins/features that will
 * not be compatible with this plugin or put focus on how the make certain
 * features compatible.
 *
 * ---
 *
 * VisuMZ_1_ItemsEquipsCore
 *
 * The custom equip slots feature from the VisuStella MZ Items and Equips Core
 * allowed you to add in extra weapon slots. This is now curated up to a max
 * of one weapon slot per character. This needs to be done to make the Weapon
 * Swap System viable.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === Skill Usage-Related Notetags ===
 * 
 * ---
 *
 * <Require Any Weapon>
 *
 * - Used for: Skill Notetags
 * - Requires the actor to have any weapon equipped in order to use the skill,
 *   regardless of the weapon's type.
 * - This does not affect enemies.
 *
 * ---
 *
 * <Switch to Weapon Type: id>
 * <Switch to Weapon Type: name>
 *
 * - Used for: Skill Notetags
 * - When using the skill, the actor will switch to the equipped weapon of the
 *   matching type.
 * - Replace 'id' with a number representing the weapon type's ID.
 * - Replace 'name' with the name of the weapon type.
 * - Weapon types are not the same as weapons. Weapon types are found in the
 *   Database > Types tab.
 * - This does not affect enemies.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * There's not too many mechanics that can be modified through the Plugin
 * Parameters, but the setting here will at least let you ease up on testing
 * battles from the database.
 *
 * ---
 *
 * Battle Test
 * 
 *   Equip All Weapons?:
 *   - Do you want to equip one of each weapon type during battle tests for
 *     all actors?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: UI Settings
 * ============================================================================
 *
 * The following Plugin Parameters are dedicated towards modifying the UI
 * elements added through this plugin.
 *
 * ---
 *
 * Attack Command
 * 
 *   Change Attack Icon?:
 *   - Change the Attack command to show the weapon?
 *   - Or have it represent the Attack skill?
 * 
 *   Swap Shortcut?:
 *   - Allow shortcut to switch weapons while selecting the Attack command?
 * 
 *     Show Arrows?:
 *     - Show arrows to the left and right of the Attack command for an easy
 *       reminder of the shortcut?
 *
 * ---
 *
 * Swap Command
 * 
 *   Show Command?:
 *   - Show the Swap weapon command in the Actor Command Window?
 *   - The Swap weapon command will be listed by default after the Attack
 *     command.
 *     - If you do not have the Attack command, it will not be shown unless you
 *       add "Weapon Swap" to the battle command list.
 * 
 * 
 *   Swap Icon:
 *   - What icon do you wish to use to represent the Swap command for the
 *     Actor Command Window?
 * 
 *   Swap Name:
 *   - What text do you want to use to represent the Swap command for the
 *     Actor Command Window?
 * 
 *   Help: Swap:
 *   - Help text for Swap command.
 *
 * ---
 *
 * Equip Scene
 * 
 *   Show Unequippable?:
 *   - Show all weapon types in the equip scene?
 *   - Or only just the equippable ones?
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.01: April 9, 2021
 * * Bug Fixes!
 * ** Shortcut arrow now accounts for changes in the actor command window size
 *    when updated post-initialization. Fix made by Olivia.
 * * Documentation Update!
 * ** Help file updated for new features.
 * ** Documentation updated for the "UI Settings Plugin Parameters":
 * *** The Swap weapon command will be listed by default after the Attack
 *     command.
 * **** If you do not have the Attack command, it will not be shown unless you
 *      add "Weapon Swap" to the battle command list.
 * * New Features!
 * ** New Plugin Parameters added by Olivia!
 * *** Plugin Parameters > UI Settings > Help: Swap
 * **** Help text for Swap command.
 *
 * Version 1.00 Official Release Date: May 3, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param WeaponSwapSystem
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Mechanics settings for the Weapon Swap System.
 * @default {"Testing":"","BattleTestAllWeapons:eval":"true"}
 *
 * @param UI:struct
 * @text UI Settings
 * @type struct<UI>
 * @desc UI settings for the Weapon Swap System.
 * @default {"AttackCommand":"","ChangeAttackIcon:eval":"true","SwapShortcut:eval":"true","ShowShortcutArrows:eval":"true","SwapCommand":"","ShowSwapCommand:eval":"false","SwapCommandIcon:num":"76","SwapCommandName:str":"Swap","EquipScene":"","ShowUnequippable:eval":"false"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param Testing
 * @text Battle Test
 *
 * @param BattleTestAllWeapons:eval
 * @text Equip All Weapons?
 * @parent Testing
 * @type boolean
 * @on All Weapons
 * @off Just Settings
 * @desc Do you want to equip one of each weapon type during
 * battle tests for all actors?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * UI Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~UI:
 *
 * @param AttackCommand
 * @text Attack Command
 *
 * @param ChangeAttackIcon:eval
 * @text Change Attack Icon?
 * @parent AttackCommand
 * @type boolean
 * @on Represent Weapon
 * @off Represent Skill Icon
 * @desc Change the Attack command to show the weapon?
 * Or have it represent the Attack skill?
 * @default true
 *
 * @param SwapShortcut:eval
 * @text Swap Shortcut?
 * @parent AttackCommand
 * @type boolean
 * @on Allow Shortcut
 * @off Don't Use
 * @desc Allow shortcut to switch weapons while selecting
 * the Attack command?
 * @default true
 *
 * @param ShowShortcutArrows:eval
 * @text Show Arrows?
 * @parent SwapShortcut:eval
 * @type boolean
 * @on Show Arrows
 * @off Hide Arrows
 * @desc Show arrows to the left and right of the Attack
 * command for an easy reminder of the shortcut?
 * @default true
 *
 * @param SwapCommand
 * @text Swap Command
 *
 * @param ShowSwapCommand:eval
 * @text Show Command?
 * @parent SwapCommand
 * @type boolean
 * @on Show Command
 * @off Hide Command
 * @desc Show the Swap weapon command in the
 * Actor Command Window?
 * @default true
 *
 * @param SwapCommandIcon:num
 * @text Swap Icon
 * @parent SwapCommand
 * @desc What icon do you wish to use to represent the
 * Swap command for the Actor Command Window?
 * @default 76
 *
 * @param SwapCommandName:str
 * @text Swap Name
 * @parent SwapCommand
 * @desc What text do you want to use to represent the
 * Swap command for the Actor Command Window?
 * @default Swap
 *
 * @param BattleHelpSwap:json
 * @text Help: Swap
 * @parent SwapCommand
 * @type note
 * @desc Help text for Swap command.
 * @default "Switch out the current weapon."
 *
 * @param EquipScene
 * @text Equip Scene
 *
 * @param ShowUnequippable:eval
 * @text Show Unequippable?
 * @parent EquipScene
 * @type boolean
 * @on All Weapons
 * @off Equippable Weapons
 * @desc Show all weapon types in the equip scene?
 * Or only just the equippable ones?
 * @default false
 *
 */
//=============================================================================

const _0x2d42=['Window','initialize','setupBattleTestWeapons','setText','clearEquipments','updateArrows','Window_EquipSlot_itemAt','text','Window_ActorCommand_addAttackCommand','replace','updateHelp','allMembers','switchToWeaponType','WEAPON_SWAP_SHORTCUT_ENABLE','swapWeaponIcon','weaponTypes','filter','parent','requestMotionRefresh','trim','ConvertParams','Game_Action_applyGlobal','setSwapWeapon','canAddSkillCommand','Window_ActorCommand_setup','height','maxItemsWeaponSwap','WEAPON_SWAP_SHORTCUT_ARROWS','width','Game_Actor_clearEquipments','Game_Actor_releaseUnequippableItems','bestEquipWeapon','_equips','initEquips','remove','Window_EquipItem_includes','isEquipWtypeOk','JSON','_swapWeapons','status','289473hotnhN','iconIndex','swapWeaponNext','executeEquipChange','actorSlotName','updateShortcutOpacity','addChild','swapWeaponPrevious','Game_Actor_optimizeEquipments','isEnabled','_helpWindow','ARRAYEVAL','max','createActorCommandWindow','commandStyle','bind','_weaponSwapShortcutSprite_Left','ARRAYJSON','onWeaponSwap','contentsOpacity','Window_ActorCommand_updateHelp','isOptimizeEquipOk','optimizeEquipments','updateWeaponSwapShortcutSprites','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','weapons','WEAPON_SWAP_SHOW_COMMAND','itemRect','findSymbol','Game_Actor_equipSlots','changeWeapon','setupBattleTestMembers','requiredWtypeId2','tradeItemWithParty','weaponSwap','3mCpCdu','EVAL','includes','_weaponSwapShortcutSprite_Right','MISSING\x20WEAPON\x20TYPE:\x20%1','changeEquip','Game_Actor_initEquips','859noPaTP','call','item','anchor','prototype','initWeaponSwapSystem','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','Game_Battler_requestMotionRefresh','exit','Scene_Battle_createActorCommandWindow','map','equipSlots','playOkSound','currentSymbol','getAllEquippedSwapWeapons','\x5cI[%1]%2','_subject','_cache','WEAPON_SWAP_CHANGE_ATTACK_ICON','_currentweapontype','commandWeaponSwap','_slotId','BARE\x20HANDS','constructor','24954syelEm','229203Jyltkv','isWeaponSwapShortcutEnabled','setObject','playEquip','Game_Actor_isDualWield','_itemWindow','swapWeaponCmd','parameters','RegExp','length','_actorCommandWindow','ARRAYNUM','Game_Party_setupBattleTestMembers','_checkingWeaponSwaps','processShiftRemoveShortcut','setup','indexOf','refreshMotion','performAttack','Window_EquipSlot_equipSlotIndex','isWeapon','weaponSwapTypes','attack','splice','SwitchWpnTypeNum','name','round','Window_EquipSlot_maxItems','applyGlobal','itemAtWeaponSwap','maxItems','opacity','isDualWield','visible','checkCacheKey','Window_Base_playOkSound','WEAPON_SWAP_BATTLE_TEST_ALL_WEAPONS','match','Switch\x20out\x20the\x20current\x20weapon.','Window_EquipSlot_isEnabled','parse','applyWeaponSwapAction','293995FLgDrO','push','meetsSkillConditions','note','refresh','openness','_currentWeaponType','isSkillWtypeOk','canWeaponSwap','description','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','_firstOfEachWeaponType','addAttackCommand','Window_EquipItem_initialize','getWtypeIdWithName','createWeaponSwapShortcutSprites','padding','getSwapWeapon','_list','_inBattle','WEAPON_SWAP_SYSTEM_SHOW_UNEQUIPPABLE_SLOTS','cursorRight','isClearEquipOk','optimizeSwappableWeapons','getFirstOfEachWeaponType','Window_EquipItem_setSlotId','Settings','actorSlotNameWeaponSwap','NUM','equipSlotIndex','_actor','203604LStIUi','meetsAnyWeaponEquippedCondition','Window_StatusBase_actorSlotName','wtypeId','addWeaponSwapCommand','_scene','setSlotId','SwapCommandName','alterAttackCommand','itemAt','ARRAYSTRUCT','VisuMZ_1_BattleCore','isSkill','ShowShortcutArrows','format','createWeaponSwapTypes','unshift','addCommand','performWeaponSwap','393MVrOhQ','Sprite_Actor_refreshMotion','Window_ActorCommand','releaseUnequippableItems','calcEquipItemPerformance','battleCommandName','Game_Actor_isClearEquipOk','SwapCommandIcon','_wtypeID','Game_Actor_isOptimizeEquipOk','setFrame','_wtypeIDs','cursorLeft','isEnabledWeaponSwap','SwitchWpnTypeStr','requiredWtypeId1','WeaponSwapSystem','isActor','callUpdateHelp','actor','clearSwappableWeapons','Game_BattlerBase_meetsSkillConditions','1921nElUaL','lineHeight','FUNC','3BdJLNK','toUpperCase','Game_Actor_changeEquip','_statusWindow'];const _0x1e3f=function(_0x1e6faa,_0x130e20){_0x1e6faa=_0x1e6faa-0x6c;let _0x2d4254=_0x2d42[_0x1e6faa];return _0x2d4254;};const _0x3d79da=_0x1e3f;(function(_0x27cd04,_0x396578){const _0x368e63=_0x1e3f;while(!![]){try{const _0xb73c22=-parseInt(_0x368e63(0xab))*-parseInt(_0x368e63(0xca))+parseInt(_0x368e63(0xb2))*-parseInt(_0x368e63(0x127))+-parseInt(_0x368e63(0xf5))+parseInt(_0x368e63(0xcb))+-parseInt(_0x368e63(0x13d))*-parseInt(_0x368e63(0x140))+parseInt(_0x368e63(0x114))+parseInt(_0x368e63(0x88));if(_0xb73c22===_0x396578)break;else _0x27cd04['push'](_0x27cd04['shift']());}catch(_0x5c544b){_0x27cd04['push'](_0x27cd04['shift']());}}}(_0x2d42,0x29d3b));var label=_0x3d79da(0x137),tier=tier||0x0,dependencies=[_0x3d79da(0x11f),'VisuMZ_1_ItemsEquipsCore'],pluginData=$plugins[_0x3d79da(0x70)](function(_0x26058b){const _0x573928=_0x3d79da;return _0x26058b[_0x573928(0x87)]&&_0x26058b['description'][_0x573928(0xad)]('['+label+']');})[0x0];VisuMZ[label][_0x3d79da(0x10f)]=VisuMZ[label]['Settings']||{},VisuMZ[_0x3d79da(0x74)]=function(_0x3dfb49,_0x1f3c2c){const _0x445f44=_0x3d79da;for(const _0x4cbeee in _0x1f3c2c){if(_0x4cbeee[_0x445f44(0xf0)](/(.*):(.*)/i)){const _0x519767=String(RegExp['$1']),_0xe0f6bf=String(RegExp['$2'])[_0x445f44(0x141)]()['trim']();let _0xff7fe8,_0x33ad51,_0x2c06af;switch(_0xe0f6bf){case _0x445f44(0x111):_0xff7fe8=_0x1f3c2c[_0x4cbeee]!==''?Number(_0x1f3c2c[_0x4cbeee]):0x0;break;case _0x445f44(0xd6):_0x33ad51=_0x1f3c2c[_0x4cbeee]!==''?JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee]):[],_0xff7fe8=_0x33ad51['map'](_0x3fe188=>Number(_0x3fe188));break;case _0x445f44(0xac):_0xff7fe8=_0x1f3c2c[_0x4cbeee]!==''?eval(_0x1f3c2c[_0x4cbeee]):null;break;case _0x445f44(0x93):_0x33ad51=_0x1f3c2c[_0x4cbeee]!==''?JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee]):[],_0xff7fe8=_0x33ad51[_0x445f44(0xbc)](_0x5685ff=>eval(_0x5685ff));break;case _0x445f44(0x85):_0xff7fe8=_0x1f3c2c[_0x4cbeee]!==''?JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee]):'';break;case _0x445f44(0x99):_0x33ad51=_0x1f3c2c[_0x4cbeee]!==''?JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee]):[],_0xff7fe8=_0x33ad51[_0x445f44(0xbc)](_0x5ac202=>JSON['parse'](_0x5ac202));break;case _0x445f44(0x13f):_0xff7fe8=_0x1f3c2c[_0x4cbeee]!==''?new Function(JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee])):new Function('return\x200');break;case'ARRAYFUNC':_0x33ad51=_0x1f3c2c[_0x4cbeee]!==''?JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee]):[],_0xff7fe8=_0x33ad51[_0x445f44(0xbc)](_0x51a9a4=>new Function(JSON[_0x445f44(0xf3)](_0x51a9a4)));break;case'STR':_0xff7fe8=_0x1f3c2c[_0x4cbeee]!==''?String(_0x1f3c2c[_0x4cbeee]):'';break;case'ARRAYSTR':_0x33ad51=_0x1f3c2c[_0x4cbeee]!==''?JSON['parse'](_0x1f3c2c[_0x4cbeee]):[],_0xff7fe8=_0x33ad51[_0x445f44(0xbc)](_0x31db8a=>String(_0x31db8a));break;case'STRUCT':_0x2c06af=_0x1f3c2c[_0x4cbeee]!==''?JSON['parse'](_0x1f3c2c[_0x4cbeee]):{},_0xff7fe8=VisuMZ[_0x445f44(0x74)]({},_0x2c06af);break;case _0x445f44(0x11e):_0x33ad51=_0x1f3c2c[_0x4cbeee]!==''?JSON[_0x445f44(0xf3)](_0x1f3c2c[_0x4cbeee]):[],_0xff7fe8=_0x33ad51['map'](_0xdab9b=>VisuMZ[_0x445f44(0x74)]({},JSON[_0x445f44(0xf3)](_0xdab9b)));break;default:continue;}_0x3dfb49[_0x519767]=_0xff7fe8;}}return _0x3dfb49;},(_0x1c2ad7=>{const _0x496538=_0x3d79da,_0x52c9a5=_0x1c2ad7['name'];for(const _0x2f8c69 of dependencies){if(!Imported[_0x2f8c69]){alert(_0x496538(0xb8)[_0x496538(0x122)](_0x52c9a5,_0x2f8c69)),SceneManager[_0x496538(0xba)]();break;}}const _0x5cc3ad=_0x1c2ad7[_0x496538(0xfe)];if(_0x5cc3ad['match'](/\[Version[ ](.*?)\]/i)){const _0x525c7c=Number(RegExp['$1']);_0x525c7c!==VisuMZ[label]['version']&&(alert(_0x496538(0xff)[_0x496538(0x122)](_0x52c9a5,_0x525c7c)),SceneManager['exit']());}if(_0x5cc3ad[_0x496538(0xf0)](/\[Tier[ ](\d+)\]/i)){const _0x5815c1=Number(RegExp['$1']);_0x5815c1<tier?(alert(_0x496538(0xa0)[_0x496538(0x122)](_0x52c9a5,_0x5815c1,tier)),SceneManager['exit']()):tier=Math[_0x496538(0x94)](_0x5815c1,tier);}VisuMZ['ConvertParams'](VisuMZ[label][_0x496538(0x10f)],_0x1c2ad7[_0x496538(0xd2)]);})(pluginData),VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xd3)]={'RequireAnyWpn':/<(?:REQUIRE|REQUIRES) ANY (?:WEAPON|WEAPONS)>/i,'SwitchWpnTypeNum':/<(?:SWITCH|SWITCHES) TO (?:WEAPON|WEAPON TYPE|WTYPE):[ ](\d+)>/i,'SwitchWpnTypeStr':/<(?:SWITCH|SWITCHES) TO (?:WEAPON|WEAPON TYPE|WTYPE):[ ](\d+)>/i},DataManager[_0x3d79da(0x10d)]=function(){const _0x47a91c=_0x3d79da;if(this[_0x47a91c(0x100)])return this[_0x47a91c(0x100)];this[_0x47a91c(0x100)]=[];for(let _0x25dddb=0x1;_0x25dddb<$dataSystem[_0x47a91c(0x6f)]['length'];_0x25dddb++){const _0x469d54=$dataWeapons[_0x47a91c(0x70)](_0xf49d7f=>_0xf49d7f&&_0xf49d7f['wtypeId']===_0x25dddb),_0x1f2d19=_0x469d54[0x0]||null;!_0x1f2d19&&console['log'](_0x47a91c(0xaf)[_0x47a91c(0x122)]($dataSystem[_0x47a91c(0x6f)][_0x25dddb][_0x47a91c(0x14d)](/\\I\[(\d+)\]/gi,''))),this[_0x47a91c(0x100)][_0x47a91c(0xf6)](_0x1f2d19);}return this[_0x47a91c(0x100)][_0x47a91c(0x82)](null)[_0x47a91c(0x82)](undefined),this[_0x47a91c(0x100)];},DataManager[_0x3d79da(0x103)]=function(_0x1a0816){const _0x751926=_0x3d79da;_0x1a0816=_0x1a0816[_0x751926(0x141)]()['trim'](),this[_0x751926(0x132)]=this[_0x751926(0x132)]||{};if(this[_0x751926(0x132)][_0x1a0816])return this['_wtypeIDs'][_0x1a0816];for(let _0x331e92=0x1;_0x331e92<0x64;_0x331e92++){if(!$dataSystem[_0x751926(0x6f)][_0x331e92])continue;let _0x9a7176=$dataSystem[_0x751926(0x6f)][_0x331e92][_0x751926(0x141)]()[_0x751926(0x73)]();_0x9a7176=_0x9a7176[_0x751926(0x14d)](/\x1I\[(\d+)\]/gi,''),_0x9a7176=_0x9a7176[_0x751926(0x14d)](/\\I\[(\d+)\]/gi,''),this[_0x751926(0x132)][_0x9a7176]=_0x331e92;}return this[_0x751926(0x132)][_0x751926(0xc8)]=0x0,this['_wtypeIDs'][_0x1a0816]||0x0;},ImageManager['swapWeaponIcon']=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['UI'][_0x3d79da(0x12e)],TextManager[_0x3d79da(0xd1)]=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['UI'][_0x3d79da(0x11b)],TextManager['swapWeaponHelp']=VisuMZ[_0x3d79da(0x137)]['Settings']['UI']['BattleHelpSwap']??_0x3d79da(0xf1),VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x75)]=Game_Action[_0x3d79da(0xb6)][_0x3d79da(0xe7)],Game_Action['prototype'][_0x3d79da(0xe7)]=function(){const _0x51941a=_0x3d79da;VisuMZ[_0x51941a(0x137)][_0x51941a(0x75)]['call'](this),this[_0x51941a(0xc2)]&&this[_0x51941a(0xc2)][_0x51941a(0x138)]()&&this[_0x51941a(0x120)]()&&this['_subject'][_0x51941a(0xf4)](this[_0x51941a(0xb4)]());},VisuMZ[_0x3d79da(0x137)]['Game_BattlerBase_meetsSkillConditions']=Game_BattlerBase[_0x3d79da(0xb6)][_0x3d79da(0xf7)],Game_BattlerBase[_0x3d79da(0xb6)][_0x3d79da(0xf7)]=function(_0x4f6cec){const _0x2b9d6d=_0x3d79da;return VisuMZ[_0x2b9d6d(0x137)][_0x2b9d6d(0x13c)][_0x2b9d6d(0xb3)](this,_0x4f6cec)&&this['meetsAnyWeaponEquippedCondition'](_0x4f6cec);},Game_BattlerBase[_0x3d79da(0xb6)][_0x3d79da(0x115)]=function(_0x507a4){return!![];},VisuMZ['WeaponSwapSystem'][_0x3d79da(0xb9)]=Game_Battler[_0x3d79da(0xb6)][_0x3d79da(0x72)],Game_Battler[_0x3d79da(0xb6)][_0x3d79da(0x72)]=function(){const _0x36ca6e=_0x3d79da;if(this['battler']()&&this['_swappingWeapon'])return;else VisuMZ[_0x36ca6e(0x137)][_0x36ca6e(0xb9)][_0x36ca6e(0xb3)](this);},Game_Actor[_0x3d79da(0xef)]=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['Mechanics']['BattleTestAllWeapons'],VisuMZ[_0x3d79da(0x137)]['Game_Actor_initEquips']=Game_Actor[_0x3d79da(0xb6)]['initEquips'],Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x81)]=function(_0x5e9fe4){const _0x29b2dd=_0x3d79da;VisuMZ['WeaponSwapSystem'][_0x29b2dd(0xb1)][_0x29b2dd(0xb3)](this,_0x5e9fe4),this[_0x29b2dd(0xb7)]();},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0xb7)]=function(){const _0x352ab5=_0x3d79da;this[_0x352ab5(0x86)]={};for(let _0x2265ad=0x1;_0x2265ad<$dataSystem[_0x352ab5(0x6f)][_0x352ab5(0xd4)];_0x2265ad++){this[_0x352ab5(0x86)][_0x2265ad]=0x0;}this[_0x352ab5(0xfb)]=0x0;for(const _0xb77cbd of this[_0x352ab5(0xa1)]()){if(!_0xb77cbd)continue;const _0x3a5549=_0xb77cbd['wtypeId'];this[_0x352ab5(0x86)][_0x3a5549]=_0xb77cbd['id'],this[_0x352ab5(0xfb)]=this['_currentWeaponType']||_0x3a5549;}},Game_Actor['prototype'][_0x3d79da(0xfd)]=function(){const _0x3945e0=_0x3d79da;return this[_0x3945e0(0xbd)]()[_0x3945e0(0xad)](0x1);},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xcf)]=Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0xeb)],Game_Actor['prototype'][_0x3d79da(0xeb)]=function(){return![];},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xa5)]=Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0xbd)],Game_Actor['prototype']['equipSlots']=function(){const _0x3769de=_0x3d79da;let _0x2e35c3=VisuMZ[_0x3769de(0x137)][_0x3769de(0xa5)][_0x3769de(0xb3)](this);return _0x2e35c3[_0x3769de(0xad)](0x1)&&(_0x2e35c3[_0x3769de(0x82)](0x1),_0x2e35c3[_0x3769de(0x124)](0x1)),_0x2e35c3;},Game_Actor['prototype']['weaponSwapTypes']=function(){const _0x448ea2=_0x3d79da;let _0x58923f=_0x448ea2(0xe0);if(this[_0x448ea2(0xed)](_0x58923f))return this['_cache'][_0x58923f];return this[_0x448ea2(0xc3)][_0x58923f]=this[_0x448ea2(0x123)](),this[_0x448ea2(0xc3)][_0x58923f];},Game_Actor['prototype'][_0x3d79da(0x123)]=function(){const _0x1880b1=_0x3d79da,_0x2f5641=[],_0x1e89b3=$dataSystem[_0x1880b1(0x6f)][_0x1880b1(0xd4)];for(let _0x47e930=0x1;_0x47e930<_0x1e89b3;_0x47e930++){if(this['isEquipWtypeOk'](_0x47e930))_0x2f5641[_0x1880b1(0xf6)](_0x47e930);}return _0x2f5641;},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x106)]=function(_0x33c829){const _0x58f00e=_0x3d79da;return this[_0x58f00e(0x86)]===undefined&&this[_0x58f00e(0xb7)](),this['_swapWeapons'][_0x33c829]=this[_0x58f00e(0x86)][_0x33c829]||0x0,$dataWeapons[this[_0x58f00e(0x86)][_0x33c829]]||null;},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0xc0)]=function(){const _0x2fceb4=_0x3d79da;return this['weaponSwapTypes']()['map'](_0x3ba96c=>this[_0x2fceb4(0x106)](_0x3ba96c))['remove'](null)['remove'](undefined);},Game_Actor[_0x3d79da(0xb6)]['setSwapWeapon']=function(_0x18b499,_0x52c275){const _0x213f3a=_0x3d79da;this['_swapWeapons']===undefined&&this[_0x213f3a(0xb7)](),this[_0x213f3a(0x86)][_0x18b499]=_0x52c275,this[_0x213f3a(0xf9)]();},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x8a)]=function(){const _0x24fd5f=_0x3d79da;this['_swapWeapons']===undefined&&this['initWeaponSwapSystem']();const _0x39b8df=this[_0x24fd5f(0xfb)],_0x258b32=this['weaponSwapTypes']();let _0x303211=_0x258b32[_0x24fd5f(0xdb)](this[_0x24fd5f(0xfb)]);for(;;){_0x303211++;if(_0x303211>=_0x258b32[_0x24fd5f(0xd4)])_0x303211=0x0;if(this['getSwapWeapon'](_0x258b32[_0x303211]))break;}const _0x1496e0=_0x258b32[_0x303211];this[_0x24fd5f(0x6c)](_0x1496e0),_0x1496e0!==_0x39b8df&&this[_0x24fd5f(0x9a)](!![]);},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x8f)]=function(){const _0x1ab8cf=_0x3d79da;this[_0x1ab8cf(0x86)]===undefined&&this['initWeaponSwapSystem']();const _0x3a62ee=this[_0x1ab8cf(0xfb)],_0x1c46af=this[_0x1ab8cf(0xe0)]();let _0x48d183=_0x1c46af[_0x1ab8cf(0xdb)](this[_0x1ab8cf(0xfb)]);for(;;){_0x48d183--;if(_0x48d183<0x0)_0x48d183=_0x1c46af[_0x1ab8cf(0xd4)]-0x1;if(this['getSwapWeapon'](_0x1c46af[_0x48d183]))break;}const _0x3d53a1=_0x1c46af[_0x48d183];this['switchToWeaponType'](_0x3d53a1),_0x3d53a1!==_0x3a62ee&&this[_0x1ab8cf(0x9a)](!![]);},Game_Actor['prototype'][_0x3d79da(0x9a)]=function(_0x5cc672){const _0x515db7=_0x3d79da,_0x4b5169=this[_0x515db7(0xa1)]()[0x0];_0x4b5169&&_0x5cc672&&(this['_swappingWeapon']=!![],this[_0x515db7(0xdd)]());},Game_Actor['prototype']['switchToWeaponType']=function(_0x5bda6f){const _0x5adaf6=_0x3d79da;this[_0x5adaf6(0x86)]===undefined&&this[_0x5adaf6(0xb7)]();_0x5bda6f=_0x5bda6f||0x0;if(!this[_0x5adaf6(0xfd)]())return;if(!this[_0x5adaf6(0x84)](_0x5bda6f))return;this[_0x5adaf6(0xfb)]=_0x5bda6f,this[_0x5adaf6(0x86)][_0x5bda6f]=this['_swapWeapons'][_0x5bda6f]||0x0;const _0x1e5856=$dataWeapons[this[_0x5adaf6(0x86)][_0x5bda6f]]||null;this[_0x5adaf6(0x80)][0x0][_0x5adaf6(0xcd)](_0x1e5856);},VisuMZ[_0x3d79da(0x137)]['Game_Actor_changeEquip']=Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0xb0)],Game_Actor[_0x3d79da(0xb6)]['changeEquip']=function(_0x33eec8,_0x59154e){const _0xe824c6=_0x3d79da;DataManager[_0xe824c6(0xdf)](_0x59154e)||_0x33eec8===0x0&&this[_0xe824c6(0xfd)]()?this[_0xe824c6(0xa6)](_0x59154e):VisuMZ['WeaponSwapSystem'][_0xe824c6(0x142)]['call'](this,_0x33eec8,_0x59154e);},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0xa6)]=function(_0x16d566){const _0x272095=_0x3d79da;if(!!_0x16d566){const _0x52c6c1=_0x16d566[_0x272095(0x117)];this[_0x272095(0x6c)](_0x52c6c1);const _0x414ab6=this[_0x272095(0xa1)]()[0x0];!!_0x414ab6?this[_0x272095(0xa9)](_0x16d566,_0x414ab6):this[_0x272095(0xa9)](_0x16d566,null),this[_0x272095(0x76)](_0x52c6c1,_0x16d566['id']),this[_0x272095(0x6c)](_0x52c6c1);}else{if(!!this[_0x272095(0xa1)]()[0x0]){const _0x23bb5a=this[_0x272095(0xa1)]()[0x0],_0x1ccf88=_0x23bb5a[_0x272095(0x117)];this[_0x272095(0x6c)](_0x1ccf88),this[_0x272095(0xa9)](null,_0x23bb5a),this[_0x272095(0x76)](_0x1ccf88,0x0),this[_0x272095(0x6c)](this[_0x272095(0xe0)]()[0x0]||0x0);}}this['refresh']();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x7e)]=Game_Actor['prototype'][_0x3d79da(0x12a)],Game_Actor['prototype'][_0x3d79da(0x12a)]=function(_0x35b044){const _0x1a69de=_0x3d79da;VisuMZ[_0x1a69de(0x137)]['Game_Actor_releaseUnequippableItems']['call'](this,_0x35b044);if(this['_checkingWeaponSwaps']||_0x35b044)return;this[_0x1a69de(0xd8)]=!![];let _0x359e7e=![];for(let _0x123c99=0x1;_0x123c99<$dataSystem['weaponTypes']['length'];_0x123c99++){if(this[_0x1a69de(0x84)](_0x123c99))continue;const _0x4cbf3e=this['getSwapWeapon'](_0x123c99);if(!_0x4cbf3e)continue;this['_swapWeapons'][_0x123c99]=0x0,$gameParty['gainItem'](_0x4cbf3e,0x1),_0x359e7e=!![];}if(_0x359e7e){const _0x435175=this[_0x1a69de(0xa1)]()[0x0]||null;this[_0x1a69de(0xfb)]=_0x435175?_0x435175[_0x1a69de(0x117)]:0x0,this[_0x1a69de(0xf9)]();}this[_0x1a69de(0xd8)]=undefined;},Game_Actor['prototype'][_0x3d79da(0x146)]=function(){const _0x3fbb6c=_0x3d79da,_0x5be55c=this['_currentWeaponType'],_0xc7e265=DataManager['getFirstOfEachWeaponType']();for(const _0x1306de of this['weaponSwapTypes']()){if(this[_0x3fbb6c(0x106)](_0x1306de))continue;const _0x486e04=_0xc7e265[_0x1306de-0x1];_0x486e04&&this[_0x3fbb6c(0x76)](_0x1306de,_0x486e04['id']);}this[_0x3fbb6c(0x6c)](_0x5be55c);},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x115)]=function(_0x60f29f){const _0x4ea93e=_0x3d79da;return _0x60f29f&&_0x60f29f[_0x4ea93e(0xf8)]['match'](VisuMZ[_0x4ea93e(0x137)][_0x4ea93e(0xd3)]['RequireAnyWpn'])?!!this['weapons']()[0x0]:!![];},Game_Actor['prototype'][_0x3d79da(0xfc)]=function(_0x19f56d){const _0x2941d2=_0x3d79da,_0x53c548=_0x19f56d[_0x2941d2(0x136)],_0x5c5996=_0x19f56d['requiredWtypeId2'];if(_0x53c548===0x0&&_0x5c5996===0x0)return!![];if(_0x53c548>0x0&&!!this[_0x2941d2(0x106)](_0x53c548))return!![];if(_0x5c5996>0x0&&!!this[_0x2941d2(0x106)](_0x5c5996))return!![];return!![];},Game_Actor['prototype'][_0x3d79da(0xf4)]=function(_0x14083e){const _0x4ca8a4=_0x3d79da;if(!DataManager[_0x4ca8a4(0x120)](_0x14083e))return;const _0x102cd2=VisuMZ[_0x4ca8a4(0x137)][_0x4ca8a4(0xd3)];if(_0x14083e[_0x4ca8a4(0xf8)][_0x4ca8a4(0xf0)](_0x102cd2[_0x4ca8a4(0xe3)])){this[_0x4ca8a4(0x6c)](Number(RegExp['$1']));return;}else{if(_0x14083e[_0x4ca8a4(0xf8)][_0x4ca8a4(0xf0)](_0x102cd2[_0x4ca8a4(0x135)])){const _0x56d4df=DataManager[_0x4ca8a4(0x103)](RegExp['$1']);this[_0x4ca8a4(0x6c)](_0x56d4df);return;}}if(this[_0x4ca8a4(0xc5)]===_0x14083e['requiredWtypeId1']||this[_0x4ca8a4(0xc5)]===_0x14083e[_0x4ca8a4(0xa8)])return;if(_0x14083e[_0x4ca8a4(0x136)]>0x0)this['switchToWeaponType'](_0x14083e[_0x4ca8a4(0x136)]);else _0x14083e['requiredWtypeId2']>0x0&&this[_0x4ca8a4(0x6c)](_0x14083e[_0x4ca8a4(0xa8)]);},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x90)]=Game_Actor[_0x3d79da(0xb6)]['optimizeEquipments'],Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x9e)]=function(){const _0x544e06=_0x3d79da;VisuMZ['WeaponSwapSystem'][_0x544e06(0x90)]['call'](this),this[_0x544e06(0x10c)]();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x130)]=Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x9d)],Game_Actor['prototype'][_0x3d79da(0x9d)]=function(_0x2718d3){const _0x579d63=_0x3d79da;if(this[_0x579d63(0xfd)]()&&_0x2718d3===0x0)return![];return VisuMZ[_0x579d63(0x137)]['Game_Actor_isOptimizeEquipOk']['call'](this,_0x2718d3);},Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x10c)]=function(){const _0x352700=_0x3d79da;if(!this['canWeaponSwap']())return;if(!VisuMZ[_0x352700(0x137)]['Game_Actor_isOptimizeEquipOk'][_0x352700(0xb3)](this,0x0))return;const _0x513d50=this[_0x352700(0xfb)];for(const _0x29e9c7 of this[_0x352700(0xe0)]()){this[_0x352700(0x6c)](_0x29e9c7),this[_0x352700(0xa6)](this[_0x352700(0x7f)](_0x29e9c7));}this[_0x352700(0x6c)](_0x513d50),this[_0x352700(0xf9)]();},Game_Actor['prototype'][_0x3d79da(0x7f)]=function(_0x7d0e16){const _0xed33f9=_0x3d79da,_0x22604e=$gameParty[_0xed33f9(0xa1)]()['filter'](_0x3a334d=>_0x3a334d[_0xed33f9(0x117)]===_0x7d0e16);let _0x30d2a4=null,_0x7a60d4=-0x3e8;for(let _0x4f3e2f=0x0;_0x4f3e2f<_0x22604e['length'];_0x4f3e2f++){const _0x17e98e=this[_0xed33f9(0x12b)](_0x22604e[_0x4f3e2f]);_0x17e98e>_0x7a60d4&&(_0x7a60d4=_0x17e98e,_0x30d2a4=_0x22604e[_0x4f3e2f]);}return _0x30d2a4;},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x7d)]=Game_Actor['prototype'][_0x3d79da(0x148)],Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x148)]=function(){const _0x40d005=_0x3d79da;VisuMZ['WeaponSwapSystem'][_0x40d005(0x7d)]['call'](this),this[_0x40d005(0x13b)]();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x12d)]=Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x10b)],Game_Actor[_0x3d79da(0xb6)][_0x3d79da(0x10b)]=function(_0x1274b9){const _0x1a4cf3=_0x3d79da;if(this[_0x1a4cf3(0xfd)]()&&_0x1274b9===0x0)return![];return VisuMZ['WeaponSwapSystem'][_0x1a4cf3(0x12d)]['call'](this,_0x1274b9);},Game_Actor[_0x3d79da(0xb6)]['clearSwappableWeapons']=function(){const _0x493390=_0x3d79da;if(!this[_0x493390(0xfd)]())return;if(!VisuMZ[_0x493390(0x137)]['Game_Actor_isClearEquipOk'][_0x493390(0xb3)](this,0x0))return;for(let _0x2dddba=0x1;_0x2dddba<$dataSystem['weaponTypes']['length'];_0x2dddba++){this[_0x493390(0x6c)](_0x2dddba),this[_0x493390(0xa6)](null);}this[_0x493390(0xf9)]();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xd7)]=Game_Party['prototype'][_0x3d79da(0xa7)],Game_Party[_0x3d79da(0xb6)]['setupBattleTestMembers']=function(){const _0x5b95ca=_0x3d79da;VisuMZ[_0x5b95ca(0x137)][_0x5b95ca(0xd7)]['call'](this);for(const _0x3b0b01 of this[_0x5b95ca(0x14f)]()){if(!_0x3b0b01)continue;_0x3b0b01[_0x5b95ca(0x146)]();}this[_0x5b95ca(0x108)]=!![];},Scene_Equip[_0x3d79da(0xb6)][_0x3d79da(0x8b)]=function(){const _0x3a078b=_0x3d79da,_0x37ae75=this[_0x3a078b(0x13a)](),_0x421b33=this[_0x3a078b(0xd0)][_0x3a078b(0xc7)],_0xcbd3e9=this[_0x3a078b(0xd0)][_0x3a078b(0xb4)]();_0x37ae75['changeEquip'](_0x421b33,_0xcbd3e9);},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xbb)]=Scene_Battle['prototype'][_0x3d79da(0x95)],Scene_Battle['prototype'][_0x3d79da(0x95)]=function(){const _0x3e9246=_0x3d79da;VisuMZ[_0x3e9246(0x137)]['Scene_Battle_createActorCommandWindow'][_0x3e9246(0xb3)](this);const _0x14c8b4=this[_0x3e9246(0xd5)];_0x14c8b4['setHandler']('weaponSwap',this[_0x3e9246(0xc6)][_0x3e9246(0x97)](this));},Scene_Battle[_0x3d79da(0xb6)][_0x3d79da(0xc6)]=function(){const _0x6107ff=_0x3d79da,_0x2888e8=BattleManager[_0x6107ff(0x13a)]();_0x2888e8['swapWeaponNext'](),this[_0x6107ff(0xd5)]['activate'](),this[_0x6107ff(0xd5)][_0x6107ff(0xf9)]();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x128)]=Sprite_Actor[_0x3d79da(0xb6)][_0x3d79da(0xdc)],Sprite_Actor['prototype'][_0x3d79da(0xdc)]=function(){const _0x29643e=_0x3d79da;this['_actor']&&this['_actor']['_swappingWeapon']&&(this['_actor']['_swappingWeapon']=undefined),VisuMZ['WeaponSwapSystem'][_0x29643e(0x128)]['call'](this);},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xee)]=Window_Base[_0x3d79da(0xb6)][_0x3d79da(0xbe)],Window_Base[_0x3d79da(0xb6)][_0x3d79da(0xbe)]=function(){const _0x321c35=_0x3d79da;this[_0x321c35(0xc9)][_0x321c35(0xe4)]===_0x321c35(0x129)&&this[_0x321c35(0xbf)]()===_0x321c35(0xaa)?SoundManager[_0x321c35(0xce)]():VisuMZ[_0x321c35(0x137)][_0x321c35(0xee)][_0x321c35(0xb3)](this);},VisuMZ['WeaponSwapSystem'][_0x3d79da(0x116)]=Window_StatusBase[_0x3d79da(0xb6)][_0x3d79da(0x8c)],Window_StatusBase['prototype'][_0x3d79da(0x8c)]=function(_0x56f0f1,_0x56eb56){const _0x5bdbb5=_0x3d79da;return _0x56f0f1&&_0x56f0f1[_0x5bdbb5(0xfd)]()?this[_0x5bdbb5(0x110)](_0x56f0f1,_0x56eb56):VisuMZ[_0x5bdbb5(0x137)][_0x5bdbb5(0x116)][_0x5bdbb5(0xb3)](this,_0x56f0f1,_0x56eb56);},Window_StatusBase[_0x3d79da(0xb6)][_0x3d79da(0x110)]=function(_0x288bdd,_0x359a04){const _0xee87c0=_0x3d79da;let _0x1d6a95=_0x288bdd[_0xee87c0(0xe0)]()[_0xee87c0(0xd4)]-0x1;Window_EquipSlot[_0xee87c0(0x109)]&&(_0x1d6a95=$dataSystem[_0xee87c0(0x6f)][_0xee87c0(0xd4)]-0x2);if(_0x359a04>_0x1d6a95)return _0x359a04-=_0x1d6a95,VisuMZ[_0xee87c0(0x137)][_0xee87c0(0x116)][_0xee87c0(0xb3)](this,_0x288bdd,_0x359a04);else{let _0x2cadd9='';if(Window_EquipSlot[_0xee87c0(0x109)])_0x2cadd9=$dataSystem[_0xee87c0(0x6f)][_0x359a04+0x1]||'';else{const _0x280abf=_0x288bdd[_0xee87c0(0xe0)]()[_0x359a04];_0x2cadd9=$dataSystem[_0xee87c0(0x6f)][_0x280abf]||'';}return _0x2cadd9=_0x2cadd9[_0xee87c0(0x14d)](/\\I\[(\d+)\]/gi,''),_0x2cadd9;}},Window_EquipSlot['WEAPON_SWAP_SYSTEM_SHOW_UNEQUIPPABLE_SLOTS']=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['UI']['ShowUnequippable'],VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xe6)]=Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0xe9)],Window_EquipSlot[_0x3d79da(0xb6)]['maxItems']=function(){const _0x3cf46f=_0x3d79da;return this[_0x3cf46f(0x113)]&&this['_actor'][_0x3cf46f(0xfd)]()?this[_0x3cf46f(0x7a)]():VisuMZ['WeaponSwapSystem'][_0x3cf46f(0xe6)][_0x3cf46f(0xb3)](this);},Window_EquipSlot[_0x3d79da(0xb6)]['maxItemsWeaponSwap']=function(){const _0x1b0b8c=_0x3d79da;let _0x401854=this[_0x1b0b8c(0x113)][_0x1b0b8c(0xbd)]()[_0x1b0b8c(0xd4)]-0x1;return Window_EquipSlot['WEAPON_SWAP_SYSTEM_SHOW_UNEQUIPPABLE_SLOTS']?_0x401854+=$dataSystem[_0x1b0b8c(0x6f)][_0x1b0b8c(0xd4)]-0x1:_0x401854+=this[_0x1b0b8c(0x113)]['weaponSwapTypes']()[_0x1b0b8c(0xd4)],_0x401854;},VisuMZ[_0x3d79da(0x137)]['Window_EquipSlot_itemAt']=Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0x11d)],Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0x11d)]=function(_0x3929a9){const _0x475bf4=_0x3d79da;return this[_0x475bf4(0x113)]&&this[_0x475bf4(0x113)][_0x475bf4(0xfd)]()?this[_0x475bf4(0xe8)](_0x3929a9):VisuMZ['WeaponSwapSystem'][_0x475bf4(0x14a)][_0x475bf4(0xb3)](this,_0x3929a9);},Window_EquipSlot['prototype'][_0x3d79da(0xe8)]=function(_0x251fc7){const _0x33db5e=_0x3d79da;let _0x36241d=this[_0x33db5e(0x113)][_0x33db5e(0xe0)]()['length']-0x1;Window_EquipSlot[_0x33db5e(0x109)]&&(_0x36241d=$dataSystem[_0x33db5e(0x6f)][_0x33db5e(0xd4)]-0x2);if(_0x251fc7>_0x36241d)return _0x251fc7-=_0x36241d,VisuMZ[_0x33db5e(0x137)][_0x33db5e(0x14a)][_0x33db5e(0xb3)](this,_0x251fc7);else{let _0x2386da=this['_actor']['weaponSwapTypes']()[_0x251fc7];return Window_EquipSlot[_0x33db5e(0x109)]&&(_0x2386da=_0x251fc7+0x1),this[_0x33db5e(0x113)]['getSwapWeapon'](_0x2386da);}},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xf2)]=Window_EquipSlot[_0x3d79da(0xb6)]['isEnabled'],Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0x91)]=function(_0x5cc217){const _0x305032=_0x3d79da;return this[_0x305032(0x113)]&&this[_0x305032(0x113)][_0x305032(0xfd)]()?this[_0x305032(0x134)](_0x5cc217):VisuMZ[_0x305032(0x137)]['Window_EquipSlot_isEnabled'][_0x305032(0xb3)](this,_0x5cc217);},Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0x134)]=function(_0x311a4a){const _0x62a3e5=_0x3d79da;let _0x37dbab=this[_0x62a3e5(0x113)][_0x62a3e5(0xe0)]()[_0x62a3e5(0xd4)]-0x1;Window_EquipSlot[_0x62a3e5(0x109)]&&(_0x37dbab=$dataSystem[_0x62a3e5(0x6f)][_0x62a3e5(0xd4)]-0x2);if(_0x311a4a>_0x37dbab)return _0x311a4a-=_0x37dbab,VisuMZ['WeaponSwapSystem'][_0x62a3e5(0xf2)][_0x62a3e5(0xb3)](this,_0x311a4a);else{if(!this[_0x62a3e5(0x113)]['isEquipChangeOk'](0x0))return![];else return Window_EquipSlot['WEAPON_SWAP_SYSTEM_SHOW_UNEQUIPPABLE_SLOTS']?this['_actor'][_0x62a3e5(0xe0)]()[_0x62a3e5(0xad)](_0x311a4a+0x1):!![];}},Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0xd9)]=function(){const _0x1dc6bc=_0x3d79da;SoundManager[_0x1dc6bc(0xce)]();const _0x4c9716=SceneManager['_scene'][_0x1dc6bc(0x113)];this['_itemWindow']['_slotId']>0x0?_0x4c9716[_0x1dc6bc(0xb0)](this[_0x1dc6bc(0xd0)][_0x1dc6bc(0xc7)],null):(_0x4c9716[_0x1dc6bc(0x6c)](this[_0x1dc6bc(0xd0)][_0x1dc6bc(0x12f)]),_0x4c9716[_0x1dc6bc(0xa6)](null));this[_0x1dc6bc(0xf9)](),this[_0x1dc6bc(0xd0)][_0x1dc6bc(0xf9)](),this[_0x1dc6bc(0x139)]();const _0xb6ff93=SceneManager[_0x1dc6bc(0x119)][_0x1dc6bc(0x143)];if(_0xb6ff93)_0xb6ff93[_0x1dc6bc(0xf9)]();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0xde)]=Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0x112)],Window_EquipSlot[_0x3d79da(0xb6)][_0x3d79da(0x112)]=function(){const _0x36e753=_0x3d79da;let _0x25af34=VisuMZ['WeaponSwapSystem'][_0x36e753(0xde)],_0xe0c45b=this[_0x36e753(0x113)][_0x36e753(0xe0)]()['length']-0x1;return Window_EquipSlot[_0x36e753(0x109)]&&(_0xe0c45b=$dataSystem['weaponTypes'][_0x36e753(0xd4)]-0x2),Math['max'](0x0,_0x25af34-_0xe0c45b);},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x102)]=Window_EquipItem['prototype']['initialize'],Window_EquipItem['prototype'][_0x3d79da(0x145)]=function(_0x5dd9f7){const _0x5777e7=_0x3d79da;VisuMZ[_0x5777e7(0x137)][_0x5777e7(0x102)][_0x5777e7(0xb3)](this,_0x5dd9f7),this[_0x5777e7(0x12f)]=0x0;},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10e)]=Window_EquipItem[_0x3d79da(0xb6)]['setSlotId'],Window_EquipItem[_0x3d79da(0xb6)][_0x3d79da(0x11a)]=function(_0x5c9c08){const _0x4a9570=_0x3d79da;if(!this[_0x4a9570(0x113)])return VisuMZ[_0x4a9570(0x137)]['Window_EquipItem_setSlotId']['call'](this,_0x5c9c08);let _0x1b789d=this[_0x4a9570(0x113)]['weaponSwapTypes']()[_0x4a9570(0xd4)]-0x1;Window_EquipSlot[_0x4a9570(0x109)]&&(_0x1b789d=$dataSystem['weaponTypes'][_0x4a9570(0xd4)]-0x2),_0x5c9c08>_0x1b789d?(_0x5c9c08-=_0x1b789d,this[_0x4a9570(0x12f)]=0x0,VisuMZ['WeaponSwapSystem'][_0x4a9570(0x10e)][_0x4a9570(0xb3)](this,_0x5c9c08)):(Window_EquipSlot['WEAPON_SWAP_SYSTEM_SHOW_UNEQUIPPABLE_SLOTS']?this[_0x4a9570(0x12f)]=_0x5c9c08+0x1:this[_0x4a9570(0x12f)]=this['_actor']['weaponSwapTypes']()[_0x5c9c08],_0x5c9c08=0x0,VisuMZ[_0x4a9570(0x137)][_0x4a9570(0x10e)]['call'](this,_0x5c9c08),this[_0x4a9570(0x113)][_0x4a9570(0x6c)](this['_wtypeID']),this[_0x4a9570(0x143)]&&this[_0x4a9570(0x143)][_0x4a9570(0xf9)]());},VisuMZ[_0x3d79da(0x137)]['Window_EquipItem_includes']=Window_EquipItem[_0x3d79da(0xb6)]['includes'],Window_EquipItem['prototype'][_0x3d79da(0xad)]=function(_0x51953d){const _0x487809=_0x3d79da;if(_0x51953d===null)return!![];else return this[_0x487809(0xc7)]===0x0&&this[_0x487809(0x12f)]!==0x0?_0x51953d[_0x487809(0x117)]===this[_0x487809(0x12f)]:VisuMZ['WeaponSwapSystem'][_0x487809(0x83)][_0x487809(0xb3)](this,_0x51953d);},Window_ActorCommand['WEAPON_SWAP_CHANGE_ATTACK_ICON']=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['UI']['ChangeAttackIcon'],Window_ActorCommand[_0x3d79da(0x6d)]=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['UI']['SwapShortcut'],Window_ActorCommand[_0x3d79da(0x7b)]=VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)]['UI'][_0x3d79da(0x121)],Window_ActorCommand[_0x3d79da(0xa2)]=VisuMZ['WeaponSwapSystem'][_0x3d79da(0x10f)]['UI']['ShowSwapCommand'],VisuMZ['WeaponSwapSystem']['Window_ActorCommand_initialize']=Window_ActorCommand['prototype'][_0x3d79da(0x145)],Window_ActorCommand[_0x3d79da(0xb6)]['initialize']=function(_0x4d624b){const _0x2ab8a9=_0x3d79da;VisuMZ[_0x2ab8a9(0x137)]['Window_ActorCommand_initialize'][_0x2ab8a9(0xb3)](this,_0x4d624b),this[_0x2ab8a9(0x104)]();},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x14c)]=Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x101)],Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x101)]=function(){const _0x43a847=_0x3d79da;VisuMZ['WeaponSwapSystem'][_0x43a847(0x14c)]['call'](this);if(!this[_0x43a847(0x113)][_0x43a847(0xfd)]())return;this[_0x43a847(0x11c)](),this[_0x43a847(0x118)]();},Window_ActorCommand[_0x3d79da(0xb6)]['alterAttackCommand']=function(){const _0x20eba5=_0x3d79da,_0x553647=$dataSkills[this[_0x20eba5(0x113)]['attackSkillId']()];if(!_0x553647)return;if(!this[_0x20eba5(0x77)](_0x553647))return;if(!Window_ActorCommand[_0x20eba5(0xc4)])return;const _0x5996ea=this[_0x20eba5(0x113)][_0x20eba5(0xa1)]()[0x0];if(!_0x5996ea)return;const _0x139a30=this[_0x20eba5(0x96)](),_0x3c59e8=DataManager[_0x20eba5(0x12c)](_0x553647),_0x23f9aa=_0x5996ea[_0x20eba5(0x89)],_0x935b0b=_0x139a30===_0x20eba5(0x14b)?_0x3c59e8:_0x20eba5(0xc1)['format'](_0x23f9aa,_0x3c59e8),_0x47f5d6=this[_0x20eba5(0xa4)](_0x20eba5(0xe1));if(_0x47f5d6>=0x0){const _0x1c1da1=this[_0x20eba5(0x107)][_0x47f5d6];_0x1c1da1[_0x20eba5(0xe4)]=_0x935b0b;}},Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x118)]=function(_0x4ec813){const _0x402e5a=_0x3d79da;if(!Window_ActorCommand[_0x402e5a(0xa2)]&&!_0x4ec813)return;if(this['_actor']['weaponSwapTypes']()[_0x402e5a(0xd4)]<=0x1)return;if(_0x4ec813){const _0x232744=this[_0x402e5a(0xa4)](_0x402e5a(0xaa));_0x232744>=0x0&&this['_list'][_0x402e5a(0xe2)](_0x232744,0x1);}else{if(this[_0x402e5a(0xa4)](_0x402e5a(0xaa))>=0x0)return;}const _0x2871eb=this[_0x402e5a(0x96)](),_0x13b5ff=TextManager['swapWeaponCmd'],_0x3c0e6e=ImageManager[_0x402e5a(0x6e)],_0xe799a3=_0x2871eb===_0x402e5a(0x14b)?_0x13b5ff:'\x5cI[%1]%2'[_0x402e5a(0x122)](_0x3c0e6e,_0x13b5ff);this[_0x402e5a(0x125)](_0xe799a3,'weaponSwap');},Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0xcc)]=function(){const _0x2b4fe9=_0x3d79da;return Window_ActorCommand[_0x2b4fe9(0x6d)]&&this['currentSymbol']()===_0x2b4fe9(0xe1)&&this[_0x2b4fe9(0x113)]&&this['_actor']['canWeaponSwap']()&&this['_actor'][_0x2b4fe9(0xc0)]()['length']>0x1;},Window_ActorCommand['prototype'][_0x3d79da(0x10a)]=function(_0x2b43ae){const _0x4a2366=_0x3d79da;this[_0x4a2366(0xcc)]()?this[_0x4a2366(0x126)](!![]):Window_Command['prototype'][_0x4a2366(0x10a)]['call'](this,_0x2b43ae);},Window_ActorCommand['prototype'][_0x3d79da(0x133)]=function(_0x10e3c8){const _0x210a65=_0x3d79da;this[_0x210a65(0xcc)]()?this[_0x210a65(0x126)](![]):Window_Command[_0x210a65(0xb6)][_0x210a65(0x133)][_0x210a65(0xb3)](this,_0x10e3c8);},Window_ActorCommand['prototype'][_0x3d79da(0x126)]=function(_0x2b2434){const _0x5cb05c=_0x3d79da;_0x2b2434?this[_0x5cb05c(0x113)][_0x5cb05c(0x8a)]():this[_0x5cb05c(0x113)]['swapWeaponPrevious'](),SoundManager['playEquip'](),this['refresh']();},Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x104)]=function(){const _0x3a7c61=_0x3d79da;if(!Window_ActorCommand[_0x3a7c61(0x6d)])return;if(!Window_ActorCommand['WEAPON_SWAP_SHORTCUT_ARROWS'])return;const _0x32f591=[new Sprite(),new Sprite()];for(const _0x18afcd of _0x32f591){this[_0x3a7c61(0x8e)](_0x18afcd),_0x18afcd[_0x3a7c61(0xea)]=0x0,_0x18afcd[_0x3a7c61(0xb5)]['y']=0.5,_0x18afcd['bitmap']=ImageManager['loadSystem'](_0x3a7c61(0x144));}_0x32f591[0x0][_0x3a7c61(0xb5)]['x']=0x0,_0x32f591[0x0][_0x3a7c61(0x131)](0x78,0x24,0x18,0x18),_0x32f591[0x0]['x']=0x0,this['_weaponSwapShortcutSprite_Left']=_0x32f591[0x0],_0x32f591[0x1][_0x3a7c61(0xb5)]['x']=0x1,_0x32f591[0x1]['setFrame'](0x90,0x24,0x18,0x18),_0x32f591[0x1]['x']=this[_0x3a7c61(0x7c)],this[_0x3a7c61(0xae)]=_0x32f591[0x1];},Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x149)]=function(){const _0x1e9caa=_0x3d79da;Window_Scrollable[_0x1e9caa(0xb6)]['updateArrows'][_0x1e9caa(0xb3)](this),this['updateWeaponSwapShortcutSprites']();},Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x9f)]=function(){const _0x35b048=_0x3d79da;if(!Window_ActorCommand[_0x35b048(0x6d)])return;if(!Window_ActorCommand[_0x35b048(0x7b)])return;VisuMZ[_0x35b048(0x137)]['updateShortcutOpacity'][_0x35b048(0xb3)](this[_0x35b048(0x98)]),VisuMZ['WeaponSwapSystem'][_0x35b048(0x8d)]['call'](this[_0x35b048(0xae)]);},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x8d)]=function(){const _0x46e269=_0x3d79da;if(!this[_0x46e269(0x71)][_0x46e269(0xec)]||this[_0x46e269(0x71)][_0x46e269(0x9b)]<0xff||this[_0x46e269(0x71)][_0x46e269(0xfa)]<0xff)this[_0x46e269(0xea)]=0x0;else{if(this[_0x46e269(0x71)][_0x46e269(0xbf)]()===_0x46e269(0xe1)){var _0x62ebe7=this[_0x46e269(0x71)][_0x46e269(0xa3)](this['parent'][_0x46e269(0xa4)]('attack')),_0x54f422=_0x62ebe7['y']+this[_0x46e269(0x71)][_0x46e269(0x105)];_0x54f422>0x0&&_0x54f422<this['parent'][_0x46e269(0x79)]-this[_0x46e269(0x71)][_0x46e269(0x105)]*0x2&&(_0x54f422+=Math[_0x46e269(0xe5)](this[_0x46e269(0x71)][_0x46e269(0x13e)]()/0x2),this[_0x46e269(0xea)]=0xff,this['y']=_0x54f422);}else this['opacity']-=0x20;}},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x78)]=Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0xda)],Window_ActorCommand['prototype'][_0x3d79da(0xda)]=function(_0x49da8c){const _0x211a82=_0x3d79da;VisuMZ[_0x211a82(0x137)]['Window_ActorCommand_setup'][_0x211a82(0xb3)](this,_0x49da8c),this[_0x211a82(0xae)]['x']=this['width'];},VisuMZ[_0x3d79da(0x137)][_0x3d79da(0x10f)][_0x3d79da(0x9c)]=Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x14e)],Window_ActorCommand[_0x3d79da(0xb6)][_0x3d79da(0x14e)]=function(){const _0x1f38be=_0x3d79da,_0x4ff5af=this[_0x1f38be(0xbf)]();switch(_0x4ff5af){case _0x1f38be(0xaa):this[_0x1f38be(0x92)][_0x1f38be(0x147)](TextManager['swapWeaponHelp']);break;default:VisuMZ['WeaponSwapSystem'][_0x1f38be(0x10f)][_0x1f38be(0x9c)][_0x1f38be(0xb3)](this);break;}};