//=============================================================================
// VisuStella MZ - Battle System - OTB - Order Turn Battle
// VisuMZ_2_BattleSystemOTB.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_BattleSystemOTB = true;

var VisuMZ = VisuMZ || {};
VisuMZ.BattleSystemOTB = VisuMZ.BattleSystemOTB || {};
VisuMZ.BattleSystemOTB.version = 1.01;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.01] [BattleSystemOTB]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Battle_System_-_OTB_VisuStella_MZ
 * @base VisuMZ_0_CoreEngine
 * @base VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_BattleCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin changes the RPG Maker MZ battle system to "Order Turn Battle",
 * a turn-based battle system where actions are executed immediately and the
 * orders for both the current and next turn are not only visible, but also
 * malleable. New mechanics are introduced where the player can manipulate the
 * turn order of an action's user or action's target in various ways they want.
 * 
 * The two Turn Orders are displayed at the top of the top of the screen to
 * give the player a clear understanding of who's turn it will be when it
 * becomes time to act, making it easier and viable for the player to formulate
 * strategies and adapt to the situation in battle.
 * 
 * *NOTE* To use this battle system, you will need the updated version of
 * VisuStella's Core Engine. Go into its Plugin Parameters and change the
 * "Battle System" plugin parameter to "otb".
 *
 * Features include all (but not limited to) the following:
 * 
 * * Utilizes the balanced AGI nature of the Default Turn Battle system.
 * * Allows for actions to execute immediately upon selection.
 * * Two Turn Order Displays appear at the top of the screen, giving the player
 *   an idea of who's turn it will be and when, for both the current turn and
 *   the next turn.
 * * Skills and Items can have an "Instant Use" effect, which allows them to
 *   perform an action immediately without using up a turn.
 * * Skills and Items can manipulate the turn order of the action's user or the
 *   action's target(s). This can apply to either the current turn or the next
 *   turn, depending on the notetags and/or action effects used.
 * * The Turn Order Display will give a preview on how turn orders will change
 *   upon specific skills and/or items being used.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_0_CoreEngine
 * * VisuMZ_1_BattleCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 * 
 * Turn Order Displays
 * 
 * The Two Turn Order Displays will capture the battle's current and next turn
 * orders determined by the BattleManager. This feature does not overwrite any
 * functions, but the Turn Order Displays may or may not conflict with any
 * existing HUD elements that are already positioned on the screen. If so, you
 * can choose to offset the Turn Order Display or move it to a different part
 * of the screen through the plugin parameters.
 * 
 * ---
 * 
 * Agility
 * 
 * Agility behaves slightly different from normal when it comes to the Order
 * Turn Battle system. Aside from the first turn in battle, agility will always
 * calculate the turn order for the "Next Turn" when conducted. This means that
 * any changes to agility values will not have any effect on the next turn's
 * already established turn order.
 * 
 * However, this can be remedied by utilizing the notetags provided by this
 * plugin to alter the Next Turn orders for specific targets. In fact, for
 * skill and item "effects" that add AGI Buffs and/or Debuffs, the target's
 * turn position on the Turn Order Display will be manipulated in accordance.
 * This auto-conversion feature can be disabled in the Plugin Parameters.
 * 
 * ---
 * 
 * Action Speed
 * 
 * Because the Order Turn Battle system already calculates agility speeds
 * before selecting an action to perform, the effects of the actioon speed will
 * not work the same way it did with the default battle system. Instead, the
 * Action Speed will be sent through a formula to determine its effect on the
 * following turn, either pushing the user ahead in next turn's turn order
 * (with a positive speed value) or back (with a negative speed value).
 * 
 * This option can have its formula altered or straight up disabled in the
 * Plugin Parameters.
 * 
 * ---
 * 
 * Infinity Speed and Clamping
 * 
 * Since Action Speeds are decided in such a way, enemies that will survive a
 * stun state past two turns will have "Infinity" speed on the recovery turn,
 * allowing them to act first relative to the rest of the battle participants
 * in order to balance out the turns they've lost.
 * 
 * Enemies with "Infinity" speed cannot be overtaken through turn order
 * manipulation while they are on the "Next Turn" order. If anything, battlers
 * who shift their turn order faster will be just trailing behind them, thus
 * the "clamping" effect. However if this occurs during the "Current Turn"
 * order, all is fair game and any battler can overtake them. Plan out your
 * battle system effects carefully with these rules in mind.
 * 
 * If you do not like the idea of Infinity Speed and/or Clamping, you can turn
 * them off in the Plugin Parameters.
 * 
 * This effect does not affect stun states that last only one turn. The effect
 * will only occur with stun states that last 2 turns or more.
 * 
 * ---
 * 
 * Instant Use
 * 
 * Skills and Items can have an "Instant Use" property which allows them to be
 * used immediately without consuming a turn. This can be used for actions that
 * otherwise do not warrant a whole turn. These can be used for minor buffs,
 * debuffs, toggles, etc.
 * 
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins or specific
 * features. This section will highlight the main plugins/features that will
 * not be compatible with this plugin or put focus on how the make certain
 * features compatible.
 *
 * ---
 * 
 * VisuMZ_2_PartySystem
 * 
 * In battle, the player cannot change entire parties at once from the Party
 * Command Window. The feature will be unaccessible while Order Turn Battle is
 * in play. However, the player can still change party members through the
 * Actor Command Window by having actors replace other actors. Party changing
 * is also available through battle events, Common Events, and script calls.
 * 
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === General OTB-Related Notetags ===
 * 
 * These notetags are general purpose notetags that have became available
 * through this plugin.
 * 
 * ---
 * 
 * <OTB Help>
 *  description
 *  description
 * </OTB Help>
 *
 * - Used for: Skill, Item Notetags
 * - If your game happens to support the ability to change battle systems, this
 *   notetag lets you change how the skill/item's help description text will
 *   look under OTB.
 * - This is primarily used if the skill behaves differently in OTB versus any
 *   other battle system.
 * - Replace 'description' with help text that's only displayed if the game's
 *   battle system is set to OTB.
 *
 * ---
 * 
 * === OTB Turn Order Display-Related Notetags ===
 * 
 * These notetags affect the OTB Turn Order Display
 * 
 * ---
 *
 * <OTB Turn Order Icon: x>
 *
 * - Used for: Actor, Enemy Notetags
 * - Changes the slot graphic used for the battler to a specific icon.
 * - Replace 'x' with the icon index to be used.
 * 
 * ---
 *
 * <OTB Turn Order Face: filename, index>
 *
 * - Used for: Actor, Enemy Notetags
 * - Changes the slot graphic used for the enemy to a specific face.
 * - Replace 'filename' with the filename of the image.
 *   - Do not include the file extension.
 * - Replace 'index' with the index of the face. Index values start at 0.
 * - Example: <OTB Turn Order Face: Monster, 1>
 * 
 * ---
 * 
 * === Instant Use-Related Notetags ===
 * 
 * ---
 *
 * <OTB Instant>
 * <OTB Instant Use>
 * <OTB Instant Cast>
 *
 * - Used for: Skill, Item Notetags
 * - Allows the skill/item to be used immediately without consuming a turn.
 *
 * ---
 * 
 * === Turn Order Manipulation-Related Notetags ===
 * 
 * ---
 *
 * <OTB User Current Turn: +x>
 * <OTB User Next Turn: +x>
 * <OTB User Follow Turn: +x>
 *
 * <OTB User Current Turn: -x>
 * <OTB User Next Turn: -x>
 * <OTB User Follow Turn: -x>
 *
 * - Used for: Skill, Item Notetags
 * - Changes the user's position in the turn order for the current turn, next
 *   turn, or whichever turn is following.
 * - If using the "Follow" variant, if the user has actions left for the
 *   current turn, it will affect the current turn. If not, it affects the
 *   next turn instead.
 * - Replace 'x' with a number representing the number of slots to change.
 *   - Negative numbers move the user closer to the front.
 *   - Positive numbers move the user towards the back.
 * - This effect only occurs once per skill/item use and at the start of the
 *   action when initializing the skill/item.
 *
 * ---
 *
 * <OTB Target Current Turn: +x>
 * <OTB Target Next Turn: +x>
 * <OTB Target Follow Turn: +x>
 *
 * <OTB Target Current Turn: -x>
 * <OTB Target Next Turn: -x>
 * <OTB Target Follow Turn: -x>
 *
 * - Used for: Skill, Item Notetags
 * - Changes the target's position in the turn order for the current turn, next
 *   turn, or whichever turn is following.
 * - If using the "Follow" variant, if the target has actions left for the
 *   current turn, it will affect the current turn. If not, it affects the
 *   next turn instead.
 * - Replace 'x' with a number representing the number of slots to change.
 *   - Negative numbers move the target closer to the front.
 *   - Positive numbers move the target towards the back.
 * - This effect will occur as many times as there are successfully connected
 *   hits for each target, meaning a target can have its turn order shifted
 *   multiple times.
 * - These are best used with single target skills/items as multi-target skills
 *   may shift multiple targets back and forth with each other if they are
 *   adjacent to one another.
 *
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Actor Plugin Commands ===
 * 
 * ---
 *
 * Actor: Change OTB Turn Order Icon
 * - Changes the icons used for the specific actor(s) on the OTB Turn Order.
 *
 *   Actor ID(s):
 *   - Select which Actor ID(s) to affect.
 *
 *   Icon:
 *   - Changes the graphic to this icon.
 *
 * ---
 *
 * Actor: Change OTB Turn Order Face
 * - Changes the faces used for the specific actor(s) on the OTB Turn Order.
 *
 *   Actor ID(s):
 *   - Select which Actor ID(s) to affect.
 *
 *   Face Name:
 *   - This is the filename for the target face graphic.
 *
 *   Face Index:
 *   - This is the index for the target face graphic.
 *
 * ---
 *
 * Actor: Clear OTB Turn Order Graphic
 * - Clears the OTB Turn Order graphics for the actor(s).
 * - The settings will revert to the Plugin Parameter settings.
 *
 *   Actor ID(s):
 *   - Select which Actor ID(s) to affect.
 *
 * ---
 * 
 * === Enemy Plugin Commands ===
 * 
 * ---
 *
 * Enemy: Change OTB Turn Order Icon
 * - Changes the icons used for the specific enemy(ies) on the OTB Turn Order.
 *
 *   Enemy Index(es):
 *   - Select which enemy index(es) to affect.
 *
 *   Icon:
 *   - Changes the graphic to this icon.
 *
 * ---
 *
 * Enemy: Change OTB Turn Order Face
 * - Changes the faces used for the specific enemy(ies) on the OTB Turn Order.
 *
 *   Enemy Index(es):
 *   - Select which enemy index(es) to affect.
 *
 *   Face Name:
 *   - This is the filename for the target face graphic.
 *
 *   Face Index:
 *   - This is the index for the target face graphic.
 *
 * ---
 *
 * Enemy: Clear OTB Turn Order Graphic
 * - Clears the OTB Turn Order graphics for the enemy(ies).
 * - The settings will revert to the Plugin Parameter settings.
 *
 *   Enemy Index(es):
 *   - Select which enemy index(es) to affect.
 *
 * ---
 * 
 * === System Plugin Commands ===
 * 
 * ---
 *
 * System: OTB Turn Order Visibility
 * - Determine the visibility of the OTB Turn Order Display.
 *
 *   Visibility:
 *   - Changes the visibility of the OTB Turn Order Display.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Conversion Settings
 * ============================================================================
 *
 * Automatically converts specific mechanics to fit OTB.
 *
 * ---
 *
 * Buffs
 * 
 *   AGI Buff => Current:
 *   - Auto-convert AGI Buff effects for Items/Skills to speed up target's
 *     current Turn Order?
 * 
 *   AGI Buff => Next:
 *   - Auto-convert AGI Buff effects for Items/Skills to speed up target's
 *     next Turn Order?
 *
 * ---
 *
 * Debuffs
 * 
 *   AGI Debuff => Current:
 *   - Auto-convert AGI Debuff effects for Items/Skills to speed up target's
 *     current Turn Order?
 * 
 *   AGI Debuff => Next:
 *   - Auto-convert AGI Debuff effects for Items/Skills to speed up target's
 *     next Turn Order?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * Determines the mechanics of Battle System OTB. These range from how Action
 * Times are handled to speed.
 *
 * ---
 *
 * Action Times+
 * 
 *   Enable Action Times?:
 *   - Enable Action Times+ to have an effect on OTB?
 * 
 *     Randomize Order?:
 *     - If enabled, randomize the action order for added actions?
 *
 * ---
 *
 * Speed
 * 
 *   Allow Random Speed?:
 *   - Allow speed to be randomized base off the user's AGI?
 * 
 *   Post-Stun Infinity?:
 *   - After a 2+ turn stun states, battlers have infinity speed for their
 *     recovery turn.
 *   - Once again, this only applies to stun states that last 2+ turns.
 * 
 *     Infinity Clamp?:
 *     - Prevents turn order manipulation from going faster than infinity
 *       speed battlers.
 * 
 *   JS: Initial Speed:
 *   - Code used to calculate initial speed at the start of battle.
 * 
 *   JS: Speed => Order:
 *   - Code used to calculate how action speeds alter next turn's order.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Turn Order Display
 * ============================================================================
 *
 * Turn Order Display settings used for Battle System OTB. These adjust how the
 * two visible turn orders appears in-game.
 *
 * ---
 *
 * General
 * 
 *   Display Position:
 *   - Select where the Turn Order will appear on the screen.
 *     - Top
 *     - Bottom
 * 
 *     Offset X:
 *     - How much to offset the X coordinate by.
 *     - Negative: left. Positive: right.
 * 
 *     Offset Y:
 *     - How much to offset the Y coordinate by.
 *     - Negative: up. Positive: down.
 * 
 *   Reposition for Help?:
 *   - If the display position is at the top, reposition the display when the
 *     help window is open?
 * 
 *     Offset X:
 *     - Reposition the display's X coordinates by this much when the Help
 *       Window is visible.
 * 
 *     Offset Y:
 *     - Reposition the display's Y coordinates by this much when the Help
 *       Window is visible.
 * 
 *   Forward Direction:
 *   - Decide on the direction of the Turn Order.
 *     - Left to Right
 *     - Right to Left
 * 
 *   Subject Distance:
 *   - How far do you want the currently active battler to distance itself from
 *     the rest of the Turn Order?
 * 
 *   Screen Buffer:
 *   - What distance do you want the display to be away from the edge of the
 *     screen by?
 * 
 * ---
 * 
 * UI Background
 * 
 *   Background Style:
 *   - Select the style you want for the background.
 *     - fill
 *     - gradient
 *     - image
 *     - transparent
 * 
 *   Image Filename:
 *   - When using the "image" style, select an image from /img/system/ as the
 *     background image.
 * 
 *     Offset X:
 *     - How much do you want to offset the Background Image's X position?
 * 
 *     Offset Y:
 *     - How much do you want to offset the Background Image's Y position?
 * 
 * ---
 * 
 * UI Text
 * 
 *   Font Size:
 *   - The font size used for parameter values.
 * 
 *   Active Battler Text:
 *   - Text used to display the active battler.
 *   - This text will always be center aligned.
 * 
 *     Offset X:
 *     - How much do you want to offset the text's X position?
 * 
 *     Offset Y:
 *     - How much do you want to offset the text's Y position?
 * 
 *   Current Turn Text:
 *   - Text used to display the current turn.
 * 
 *     Offset X:
 *     - How much do you want to offset the text's X position?
 * 
 *     Offset Y:
 *     - How much do you want to offset the text's Y position?
 * 
 *   Next Turn Text:
 *   - Text used to display the next turn.
 * 
 *     Offset X:
 *     - How much do you want to offset the text's X position?
 * 
 *     Offset Y:
 *     - How much do you want to offset the text's Y position?
 * 
 *   Text Align:
 *   - Text alignment for the Current and Next Turn texts?
 *     - auto
 *     - left
 *     - center
 *     - right
 * 
 * ---
 * 
 * Slots
 * 
 *   Width:
 *   - How many pixels wide should the slots be on the Turn Order display?
 * 
 *   Height:
 *   - How many pixels tall should the slots be on the Turn Order display?
 * 
 *   Preview Scale:
 *   - How much do you want to scale the preview sprites by?
 *   - Use a number between 0 and 1 for the best results.
 * 
 *     Offset X:
 *     - How much do you want to offset the Preview Sprites' X position?
 * 
 *     Offset Y:
 *     - How much do you want to offset the Preview Sprites' Y position?
 * 
 *   Update Frames:
 *   - How many frames should it take for the slots to update their
 *     positions by?
 *
 * ---
 *
 * Slot Border
 * 
 *   Show Border?:
 *   - Show borders for the slot sprites?
 * 
 *   Border Thickness:
 *   - How many pixels thick should the colored portion of the border be?
 * 
 *   Actors
 *   Enemies
 * 
 *     Border Color:
 *     - Use #rrggbb for custom colors or regular numbers for text colors
 *       from the Window Skin.
 * 
 *       Preview Version:
 *       - A different setting is used for the preview version.
 * 
 *     Border Skin:
 *     - Optional. Place a skin on the actor/enemy borders instead of
 *       rendering them?
 * 
 *       Preview Version:
 *       - A different setting is used for the preview version.
 * 
 * ---
 * 
 * Slot Sprites
 * 
 *   Actors
 * 
 *     Sprite Type:
 *     - Select the type of sprite used for the actor graphic.
 *     - Face Graphic - Show the actor's face.
 *     - Icon - Show a specified icon.
 *     - Sideview Actor - Show the actor's sideview battler.
 * 
 *     Default Icon:
 *     - Which icon do you want to use for actors by default?
 * 
 *   Enemies
 * 
 *     Sprite Type:
 *     - Select the type of sprite used for the enemy graphic.
 *     - Face Graphic - Show a specified face graphic.
 *     - Icon - Show a specified icon.
 *     - Enemy - Show the enemy's graphic or sideview battler.
 * 
 *     Default Face Name:
 *     - Use this default face graphic if there is no specified face.
 * 
 *     Default Face Index:
 *     - Use this default face index if there is no specified index.
 * 
 *     Default Icon:
 *     - Which icon do you want to use for enemies by default?
 * 
 *     Match Hue?:
 *     - Match the hue for enemy battlers?
 *     - Does not apply if there's a sideview battler.
 *
 * ---
 *
 * Slot Letter
 * 
 *   Show Enemy Letter?:
 *   - Show the enemy's letter on the slot sprite?
 * 
 *   Font Name:
 *   - The font name used for the text of the Letter.
 *   - Leave empty to use the default game's font.
 * 
 *   Font Size:
 *   - The font size used for the text of the Letter.
 *
 * ---
 *
 * Slot Background
 * 
 *   Show Background?:
 *   - Show the background on the slot sprite?
 * 
 *   Actors
 *   Enemies
 * 
 *     Background Color 1:
 *     Background Color 2:
 *     - Use #rrggbb for custom colors or regular numbers for text colors
 *       from the Window Skin.
 * 
 *       Preview Version:
 *       - A different setting is used for the preview version.
 * 
 *     Background Skin:
 *     - Optional. Use a skin for the actor background instead of
 *       rendering them?
 * 
 *       Preview Version:
 *       - A different setting is used for the preview version.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.01: April 9, 2021
 * * Bug Fixes!
 * ** Subsequent battles will properly reset the turn order. Fix by Olivia.
 * 
 * Version 1.00 Official Release Date: April 26, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OtbTurnOrderActorIcon
 * @text Actor: Change OTB Turn Order Icon
 * @desc Changes the icons used for the specific actor(s) on the OTB Turn Order.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @arg IconIndex:num
 * @text Icon
 * @desc Changes the graphic to this icon.
 * @default 84
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OtbTurnOrderActorFace
 * @text Actor: Change OTB Turn Order Face
 * @desc Changes the faces used for the specific actor(s) on the OTB Turn Order.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @arg FaceName:str
 * @text Face Name
 * @type file
 * @dir img/faces/
 * @desc This is the filename for the target face graphic.
 * @default Actor1
 *
 * @arg FaceIndex:num
 * @text Face Index
 * @type number
 * @desc This is the index for the target face graphic.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OtbTurnOrderClearActorGraphic
 * @text Actor: Clear OTB Turn Order Graphic
 * @desc Clears the OTB Turn Order graphics for the actor(s).
 * The settings will revert to the Plugin Parameter settings.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OtbTurnOrderEnemyIcon
 * @text Enemy: Change OTB Turn Order Icon
 * @desc Changes the icons used for the specific enemy(ies) on the OTB Turn Order.
 *
 * @arg Enemies:arraynum
 * @text Enemy Index(es)
 * @type number[]
 * @desc Select which enemy index(es) to affect.
 * @default ["1"]
 *
 * @arg IconIndex:num
 * @text Icon
 * @desc Changes the graphic to this icon.
 * @default 298
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OtbTurnOrderEnemyFace
 * @text Enemy: Change OTB Turn Order Face
 * @desc Changes the faces used for the specific enemy(ies) on the OTB Turn Order.
 *
 * @arg Enemies:arraynum
 * @text Enemy Index(es)
 * @type number[]
 * @desc Select which enemy index(es) to affect.
 * @default ["1"]
 *
 * @arg FaceName:str
 * @text Face Name
 * @parent EnemySprite
 * @type file
 * @dir img/faces/
 * @desc This is the filename for the target face graphic.
 * @default Monster
 *
 * @arg FaceIndex:num
 * @text Face Index
 * @parent EnemySprite
 * @type number
 * @desc This is the index for the target face graphic.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OtbTurnOrderClearEnemyGraphic
 * @text Enemy: Clear OTB Turn Order Graphic
 * @desc Clears the OTB Turn Order graphics for the enemy(ies).
 * The settings will revert to the Plugin Parameter settings.
 *
 * @arg Enemies:arraynum
 * @text Enemy Index(es)
 * @type number[]
 * @desc Select which enemy index(es) to affect.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemTurnOrderVisibility
 * @text System: OTB Turn Order Visibility
 * @desc Determine the visibility of the OTB Turn Order Display.
 *
 * @arg Visible:eval
 * @text Visibility
 * @type boolean
 * @on Visible
 * @off Hidden
 * @desc Changes the visibility of the OTB Turn Order Display.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BattleSystemOTB
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Conversion:struct
 * @text Conversion Settings
 * @type struct<Conversion>
 * @desc Automatically converts specific mechanics to fit OTB.
 * @default {"Buffs":"","ConvertAgiBuffCurrent:eval":"true","ConvertAgiBuffNext:eval":"true","Debuffs":"","ConvertAgiDebuffCurrent:eval":"true","ConvertAgiDebuffNext:eval":"true"}
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Determines the mechanics of Battle System OTB.
 * @default {"Actions":"","EnableActionTimes:eval":"true","RandomizeActionTimesOrder:eval":"true","Speed":"","AllowRandomSpeed:eval":"false","PostStunInfinitySpeed:eval":"true","InfinityClamp:eval":"true","InitialSpeedJS:func":"\"// Declare Constants\\nconst agi = this.subject().agi;\\n\\n// Create Speed\\nlet speed = agi;\\nif (this.allowRandomSpeed()) {\\n    speed += Math.randomInt(Math.floor(5 + agi / 4));\\n}\\n\\n// Return Speed\\nreturn speed;\"","ConvertSpeedJS:func":"\"// Declare Constants\\nconst item = this.item();\\nconst modifier = 50;\\n\\n// Calculate Order Slots Changed\\nlet change = item.speed / (-modifier);\\nchange = (change >= 0) ? Math.ceil(change) : Math.floor(change);\\n\\n// Return Change\\nreturn change || 0;\""}
 *
 * @param TurnOrder:struct
 * @text Turn Order Display
 * @type struct<TurnOrder>
 * @desc Turn Order Display settings used for Battle System OTB.
 * @default {"General":"","DisplayPosition:str":"top","DisplayOffsetX:num":"0","DisplayOffsetY:num":"0","RepositionTopForHelp:eval":"true","RepositionTopHelpX:num":"+0","RepositionTopHelpY:num":"+96","RepositionLogWindow:eval":"true","LogWindowOffsetY:num":"+0","OrderDirection:eval":"false","SubjectDistance:num":"16","ScreenBuffer:num":"36","UiBackground":"","BgDimStyle:str":"gradient","BgImageFilename:str":"","BgImageOffsetX:num":"+0","BgImageOffsetY:num":"+0","UiText":"","UiFontSize:num":"16","UiSubjectText:str":"★","UiSubjectOffsetX:num":"+0","UiSubjectOffsetY:num":"-6","UiCurrentText:str":"✦CURRENT TURN✦","UiCurrentOffsetX:num":"+6","UiCurrentOffsetY:num":"-6","UiNextText:str":"✧NEXT TURN✧","UiNextOffsetX:num":"+6","UiNextOffsetY:num":"-6","UiAlignment:str":"auto","Slots":"","SpriteThin:num":"72","SpriteLength:num":"72","PreviewScale:num":"0.5","PreviewOffsetX:num":"+0","PreviewOffsetY:num":"+0","UpdateFrames:num":"24","Border":"","ShowMarkerBorder:eval":"true","BorderActor":"","ActorBorderColor:str":"4","PreviewActorBorderColor:str":"0","ActorSystemBorder:str":"","PreviewActorSystemBorder:str":"","BorderEnemy":"","EnemyBorderColor:str":"2","PreviewEnemyBorderColor:str":"0","EnemySystemBorder:str":"","PreviewEnemySystemBorder:str":"","BorderThickness:num":"2","Sprite":"","ActorSprite":"","ActorBattlerType:str":"face","ActorBattlerIcon:num":"84","EnemySprite":"","EnemyBattlerType:str":"enemy","EnemyBattlerFaceName:str":"Monster","EnemyBattlerFaceIndex:num":"1","EnemyBattlerIcon:num":"298","EnemyBattlerMatchHue:eval":"true","Letter":"","EnemyBattlerDrawLetter:eval":"true","EnemyBattlerFontFace:str":"","EnemyBattlerFontSize:num":"16","Background":"","ShowMarkerBg:eval":"true","BackgroundActor":"","ActorBgColor1:str":"19","PreviewActorBgColor1:str":"19","ActorBgColor2:str":"9","PreviewActorBgColor2:str":"0","ActorSystemBg:str":"","PreviewActorSystemBg:str":"","BackgroundEnemy":"","EnemyBgColor1:str":"19","PreviewEnemyBgColor1:str":"19","EnemyBgColor2:str":"18","PreviewEnemyBgColor2:str":"0","EnemySystemBg:str":"","PreviewEnemySystemBg:str":""}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Conversion Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Conversion:
 * 
 * @param Buffs
 *
 * @param ConvertAgiBuffCurrent:eval
 * @text AGI Buff => Current
 * @parent Buffs
 * @type boolean
 * @on Convert
 * @off Don't Convert
 * @desc Auto-convert AGI Buff effects for Items/Skills to speed up target's current Turn Order?
 * @default true
 *
 * @param ConvertAgiBuffNext:eval
 * @text AGI Buff => Next
 * @parent Buffs
 * @type boolean
 * @on Convert
 * @off Don't Convert
 * @desc Auto-convert AGI Buff effects for Items/Skills to speed up target's next Turn Order?
 * @default true
 * 
 * @param Debuffs
 *
 * @param ConvertAgiDebuffCurrent:eval
 * @text AGI Debuff => Current
 * @parent Debuffs
 * @type boolean
 * @on Convert
 * @off Don't Convert
 * @desc Auto-convert AGI Debuff effects for Items/Skills to speed up target's current Turn Order?
 * @default true
 *
 * @param ConvertAgiDebuffNext:eval
 * @text AGI Debuff => Next
 * @parent Debuffs
 * @type boolean
 * @on Convert
 * @off Don't Convert
 * @desc Auto-convert AGI Debuff effects for Items/Skills to speed up target's next Turn Order?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param Actions
 * @text Action Times+
 *
 * @param EnableActionTimes:eval
 * @text Enable Action Times?
 * @parent Actions
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enable Action Times+ to have an effect on OTB?
 * @default true
 *
 * @param RandomizeActionTimesOrder:eval
 * @text Randomize Order?
 * @parent EnableActionTimes:eval
 * @type boolean
 * @on Randomize
 * @off Clumped
 * @desc If enabled, randomize the action order for added actions?
 * @default true
 * 
 * @param Speed
 *
 * @param AllowRandomSpeed:eval
 * @text Allow Random Speed?
 * @parent Speed
 * @type boolean
 * @on Allow
 * @off Disable
 * @desc Allow speed to be randomized base off the user's AGI?
 * @default false
 *
 * @param PostStunInfinitySpeed:eval
 * @text Post-Stun Infinity?
 * @parent Speed
 * @type boolean
 * @on Infinity
 * @off Normal
 * @desc After a 2+ turn stun states, battlers have infinity speed for their recovery turn.
 * @default true
 *
 * @param InfinityClamp:eval
 * @text Infinity Clamp?
 * @parent PostStunInfinitySpeed:eval
 * @type boolean
 * @on Enable Clamp
 * @off Disable Clamp
 * @desc Prevents turn order manipulation from going faster than infinity speed battlers.
 * @default true
 *
 * @param InitialSpeedJS:func
 * @text JS: Initial Speed
 * @parent Speed
 * @type note
 * @desc Code used to calculate initial speed at the start of battle.
 * @default "// Declare Constants\nconst agi = this.subject().agi;\n\n// Create Speed\nlet speed = agi;\nif (this.allowRandomSpeed()) {\n    speed += Math.randomInt(Math.floor(5 + agi / 4));\n}\n\n// Return Speed\nreturn speed;"
 *
 * @param ConvertSpeedJS:func
 * @text JS: Speed => Order
 * @parent Speed
 * @type note
 * @desc Code used to calculate how action speeds alter next turn's order.
 * @default "// Declare Constants\nconst item = this.item();\nconst modifier = 50;\n\n// Calculate Order Slots Changed\nlet change = item.speed / (-modifier);\nchange = (change >= 0) ? Math.ceil(change) : Math.floor(change);\n\n// Return Change\nreturn change || 0;"
 * 
 */
/* ----------------------------------------------------------------------------
 * Turn Order Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~TurnOrder:
 *
 * @param General
 *
 * @param DisplayPosition:str
 * @text Display Position
 * @parent General
 * @type select
 * @option top
 * @option bottom
 * @desc Select where the Turn Order will appear on the screen.
 * @default top
 * 
 * @param DisplayOffsetX:num
 * @text Offset X
 * @parent DisplayPosition:str
 * @desc How much to offset the X coordinate by.
 * Negative: left. Positive: right.
 * @default 0
 * 
 * @param DisplayOffsetY:num
 * @text Offset Y
 * @parent DisplayPosition:str
 * @desc How much to offset the Y coordinate by.
 * Negative: up. Positive: down.
 * @default 0
 *
 * @param RepositionTopForHelp:eval
 * @text Reposition for Help?
 * @parent DisplayPosition:str
 * @type boolean
 * @on Reposition
 * @off Stay
 * @desc If the display position is at the top, reposition the
 * display when the help window is open?
 * @default true
 *
 * @param RepositionTopHelpX:num
 * @text Offset X
 * @parent RepositionTopForHelp:eval
 * @desc Reposition the display's X coordinates by this much when
 * the Help Window is visible.
 * @default +0
 *
 * @param RepositionTopHelpY:num
 * @text Offset Y
 * @parent RepositionTopForHelp:eval
 * @desc Reposition the display's Y coordinates by this much when
 * the Help Window is visible.
 * @default +96
 *
 * @param RepositionLogWindow:eval
 * @text Reposition Log?
 * @parent DisplayPosition:str
 * @type boolean
 * @on Reposition
 * @off Stay
 * @desc If the display position is at the top, reposition the
 * Battle Log Window to be lower?
 * @default true
 *
 * @param LogWindowOffsetY:num
 * @text Offset Y
 * @parent RepositionLogWindow:eval
 * @desc How much do you want to offset the Log Window's Y position?
 * @default +0
 *
 * @param OrderDirection:eval
 * @text Forward Direction
 * @parent General
 * @type boolean
 * @on Left to Right
 * @off Right to Left
 * @desc Decide on the direction of the Turn Order.
 * @default false
 *
 * @param SubjectDistance:num
 * @text Subject Distance
 * @parent General
 * @type number
 * @desc How far do you want the currently active battler to
 * distance itself from the rest of the Turn Order?
 * @default 16
 *
 * @param ScreenBuffer:num
 * @text Screen Buffer
 * @parent General
 * @type number
 * @desc What distance do you want the display to be away
 * from the edge of the screen by?
 * @default 36
 *
 * @param UiBackground
 * @text UI Background
 *
 * @param BgDimStyle:str
 * @text Background Style
 * @parent UiBackground
 * @type select
 * @option fill
 * @option gradient
 * @option image
 * @option transparent
 * @desc Select the style you want for the background.
 * @default gradient
 *
 * @param BgImageFilename:str
 * @text Image Filename
 * @parent UiBackground
 * @type file
 * @dir img/system/
 * @desc When using the "image" style, select an image from /img/system/ as the background image.
 * @default 
 *
 * @param BgImageOffsetX:num
 * @text Offset X
 * @parent BgImageFilename:str
 * @desc How much do you want to offset the Background Image's X position?
 * @default +0
 *
 * @param BgImageOffsetY:num
 * @text Offset Y
 * @parent BgImageFilename:str
 * @desc How much do you want to offset the Background Image's Y position?
 * @default +0
 *
 * @param UiText
 * @text UI Text
 *
 * @param UiFontSize:num
 * @text Font Size
 * @parent UiText
 * @desc The font size used for parameter values.
 * @default 16
 *
 * @param UiSubjectText:str
 * @text Active Battler Text
 * @parent UiText
 * @desc Text used to display the active battler.
 * This text will always be center aligned.
 * @default ★
 *
 * @param UiSubjectOffsetX:num
 * @text Offset X
 * @parent UiSubjectText:str
 * @desc How much do you want to offset the text's X position?
 * @default +0
 *
 * @param UiSubjectOffsetY:num
 * @text Offset Y
 * @parent UiSubjectText:str
 * @desc How much do you want to offset the text's Y position?
 * @default -6
 *
 * @param UiCurrentText:str
 * @text Current Turn Text
 * @parent UiText
 * @desc Text used to display the current turn.
 * @default ✦CURRENT TURN✦
 *
 * @param UiCurrentOffsetX:num
 * @text Offset X
 * @parent UiCurrentText:str
 * @desc How much do you want to offset the text's X position?
 * @default +6
 *
 * @param UiCurrentOffsetY:num
 * @text Offset Y
 * @parent UiCurrentText:str
 * @desc How much do you want to offset the text's Y position?
 * @default -6
 *
 * @param UiNextText:str
 * @text Next Turn Text
 * @parent UiText
 * @desc Text used to display the next turn.
 * @default ✧NEXT TURN✧
 *
 * @param UiNextOffsetX:num
 * @text Offset X
 * @parent UiNextText:str
 * @desc How much do you want to offset the text's X position?
 * @default +6
 *
 * @param UiNextOffsetY:num
 * @text Offset Y
 * @parent UiNextText:str
 * @desc How much do you want to offset the text's Y position?
 * @default -6
 *
 * @param UiAlignment:str
 * @text Text Align
 * @parent UiText
 * @type combo
 * @option auto
 * @option left
 * @option center
 * @option right
 * @desc Text alignment for the Current and Next Turn texts?
 * @default auto
 * 
 * @param Slots
 *
 * @param SpriteThin:num
 * @text Width
 * @parent Slots
 * @type number
 * @min 1
 * @desc How many pixels wide should the slots be on the
 * Turn Order display?
 * @default 72
 *
 * @param SpriteLength:num
 * @text Height
 * @parent Slots
 * @type number
 * @min 1
 * @desc How many pixels tall should the slots be on the
 * Turn Order display?
 * @default 72
 *
 * @param PreviewScale:num
 * @text Preview Scale
 * @parent Slots
 * @desc How much do you want to scale the preview sprites by?
 * Use a number between 0 and 1 for the best results.
 * @default 0.5
 *
 * @param PreviewOffsetX:num
 * @text Offset X
 * @parent PreviewScale:num
 * @desc How much do you want to offset the Preview Sprites' X position?
 * @default +0
 *
 * @param PreviewOffsetY:num
 * @text Offset Y
 * @parent PreviewScale:num
 * @desc How much do you want to offset the Preview Sprites' Y position?
 * @default +0
 *
 * @param UpdateFrames:num
 * @text Update Frames
 * @parent Slots
 * @type number
 * @min 1
 * @desc How many frames should it take for the slots to
 * update their positions by?
 * @default 24
 *
 * @param Border
 * @text Slot Border
 *
 * @param ShowMarkerBorder:eval
 * @text Show Border?
 * @parent Border
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show borders for the slot sprites?
 * @default true
 *
 * @param BorderThickness:num
 * @text Border Thickness
 * @parent Markers
 * @type number
 * @min 1
 * @desc How many pixels thick should the colored portion of the border be?
 * @default 2
 *
 * @param BorderActor
 * @text Actors
 * @parent Border
 *
 * @param ActorBorderColor:str
 * @text Border Color
 * @parent BorderActor
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 4
 *
 * @param PreviewActorBorderColor:str
 * @text Preview Version
 * @parent ActorBorderColor:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 0
 *
 * @param ActorSystemBorder:str
 * @text Border Skin
 * @parent BorderActor
 * @type file
 * @dir img/system/
 * @desc Optional. Place a skin on the actor borders instead of rendering them?
 * @default 
 *
 * @param PreviewActorSystemBorder:str
 * @text Preview Version
 * @parent ActorSystemBorder:str
 * @type file
 * @dir img/system/
 * @desc Optional. Place a skin on the actor borders instead of rendering them?
 * @default 
 *
 * @param BorderEnemy
 * @text Enemies
 * @parent Border
 *
 * @param EnemyBorderColor:str
 * @text Border Color
 * @parent BorderEnemy
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 2
 *
 * @param PreviewEnemyBorderColor:str
 * @text Preview Version
 * @parent EnemyBorderColor:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 0
 *
 * @param EnemySystemBorder:str
 * @text Border Skin
 * @parent BorderEnemy
 * @type file
 * @dir img/system/
 * @desc Optional. Place a skin on the enemy borders instead of rendering them?
 * @default 
 *
 * @param PreviewEnemySystemBorder:str
 * @text Preview Version
 * @parent EnemySystemBorder:str
 * @type file
 * @dir img/system/
 * @desc Optional. Place a skin on the enemy borders instead of rendering them?
 * @default 
 *
 * @param Sprite
 * @text Slot Sprites
 *
 * @param ActorSprite
 * @text Actors
 * @parent Sprite
 *
 * @param ActorBattlerType:str
 * @text Sprite Type
 * @parent ActorSprite
 * @type select
 * @option Face Graphic - Show the actor's face.
 * @value face
 * @option Icon - Show a specified icon.
 * @value icon
 * @option Sideview Actor - Show the actor's sideview battler.
 * @value svactor
 * @desc Select the type of sprite used for the actor graphic.
 * @default face
 *
 * @param ActorBattlerIcon:num
 * @text Default Icon
 * @parent ActorSprite
 * @desc Which icon do you want to use for actors by default?
 * @default 84
 *
 * @param EnemySprite
 * @text Enemies
 * @parent Sprite
 *
 * @param EnemyBattlerType:str
 * @text Sprite Type
 * @parent EnemySprite
 * @type select
 * @option Face Graphic - Show a specified face graphic.
 * @value face
 * @option Icon - Show a specified icon.
 * @value icon
 * @option Enemy - Show the enemy's graphic or sideview battler.
 * @value enemy
 * @desc Select the type of sprite used for the enemy graphic.
 * @default enemy
 *
 * @param EnemyBattlerFaceName:str
 * @text Default Face Name
 * @parent EnemySprite
 * @type file
 * @dir img/faces/
 * @desc Use this default face graphic if there is no specified face.
 * @default Monster
 *
 * @param EnemyBattlerFaceIndex:num
 * @text Default Face Index
 * @parent EnemySprite
 * @type number
 * @desc Use this default face index if there is no specified index.
 * @default 1
 *
 * @param EnemyBattlerIcon:num
 * @text Default Icon
 * @parent EnemySprite
 * @desc Which icon do you want to use for enemies by default?
 * @default 298
 *
 * @param EnemyBattlerMatchHue:eval
 * @text Match Hue?
 * @parent EnemySprite
 * @type boolean
 * @on Match
 * @off Don't Match
 * @desc Match the hue for enemy battlers?
 * Does not apply if there's a sideview battler.
 * @default true
 *
 * @param Letter
 * @text Slot Letter
 *
 * @param EnemyBattlerDrawLetter:eval
 * @text Show Enemy Letter?
 * @parent Letter
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the enemy's letter on the slot sprite?
 * @default true
 *
 * @param EnemyBattlerFontFace:str
 * @text Font Name
 * @parent Letter
 * @desc The font name used for the text of the Letter.
 * Leave empty to use the default game's font.
 * @default 
 *
 * @param EnemyBattlerFontSize:num
 * @text Font Size
 * @parent Letter
 * @min 1
 * @desc The font size used for the text of the Letter.
 * @default 16
 *
 * @param Background
 * @text Slot Background
 *
 * @param ShowMarkerBg:eval
 * @text Show Background?
 * @parent Background
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the background on the slot sprite?
 * @default true
 *
 * @param BackgroundActor
 * @text Actors
 * @parent Background
 *
 * @param ActorBgColor1:str
 * @text Background Color 1
 * @parent BackgroundActor
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param PreviewActorBgColor1:str
 * @text Preview Version
 * @parent ActorBgColor1:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param ActorBgColor2:str
 * @text Background Color 2
 * @parent BackgroundActor
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 9
 *
 * @param PreviewActorBgColor2:str
 * @text Preview Version
 * @parent ActorBgColor2:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 0
 *
 * @param ActorSystemBg:str
 * @text Background Skin
 * @parent BackgroundActor
 * @type file
 * @dir img/system/
 * @desc Optional. Use a skin for the actor background instead of rendering them?
 * @default 
 *
 * @param PreviewActorSystemBg:str
 * @text Preview Version
 * @parent ActorSystemBg:str
 * @type file
 * @dir img/system/
 * @desc Optional. Use a skin for the actor background instead of rendering them?
 * @default 
 *
 * @param BackgroundEnemy
 * @text Enemies
 * @parent Background
 *
 * @param EnemyBgColor1:str
 * @text Background Color 1
 * @parent BackgroundEnemy
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param PreviewEnemyBgColor1:str
 * @text Preview Version
 * @parent EnemyBgColor1:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param EnemyBgColor2:str
 * @text Background Color 2
 * @parent BackgroundEnemy
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 18
 *
 * @param PreviewEnemyBgColor2:str
 * @text Preview Version
 * @parent EnemyBgColor2:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 0
 *
 * @param EnemySystemBg:str
 * @text Background Skin
 * @parent BackgroundEnemy
 * @type file
 * @dir img/system/
 * @desc Optional. Use a skin for the enemy background instead of rendering them?
 * @default 
 *
 * @param PreviewEnemySystemBg:str
 * @text Preview Version
 * @parent EnemySystemBg:str
 * @type file
 * @dir img/system/
 * @desc Optional. Use a skin for the enemy background instead of rendering them?
 * @default 
 *
 */
//=============================================================================

const _0x184d=['createBackgroundSprite','_otbTurnOrderGraphicType','_homeDuration','singleSkill','BattleManager_startInput','PreviewScale','BattleManager_makeActionOrders','EnemyBattlerFaceIndex','removeSprite','currentAction','Game_Action_applyGlobal','OtbTurnOrderClearActorGraphic','Scene_Battle_commandGuard','calculateTargetPositions','removeUnableTurnOrderSprites','otbCalcTargetCurrentOrderChange','EnemyBattlerFontFace','Scene_Battle_actorCommandSingleSkill','bitmapHeight','applyGlobal','isBattleSystemOTBTurnOrderVisible','%1BgColor2','refreshTurnOrder','makeOTBSpeed','_graphicFaceIndex','randomInt','blt','Actors','11784vmelLN','changeFaceGraphicBitmap','inputtingAction','BattleManager_setup','auto','setAttack','max','EnemyBattlerFontSize','onBattleEndOTB','OTB_ADDED_RANDOMIZE_ADDED_ACTION_ORDER','ConvertSpeedJS','TurnOrderOTBGraphicFaceIndex','getColor','Scene_Battle_onItemCancel','onSkillOk','BorderThickness','gradientFillRect','ActorBattlerType','BgImageOffsetY','_hidden','loadSvActor','_enemyWindow','dataId','_graphicSprite','BattleManager_getNextSubject','_positionTargetY','UpdateFrames','updateStateTurns','fontFace','face','onBattleEnd','startActorInput','registerCommand','otbProcessActionCheck','otbAddBattlerToTurnOrderAtStart','applyBattleItemWindowOTB','repositionLogWindowOTB','Scene_Battle_onItemOk','Enemy','otbApplyActionTimes','create','Settings','center','RepositionTopHelpX','DisplayOffsetX','addBattlerToTurnOrderAtEnd','checkOpacity','ActionBattlersFilter','right','actorCommandSingleSkill','_otbTurnOrderFaceIndex','loadSvEnemy','_currentX','createTurnOrderOTBGraphicFaceIndex','SpriteLength','getInfinityClamp','getBorderThickness','index','isAppeared','PreviewOffsetX','TurnOrderOTBGraphicIconIndex','effects','applyGlobalBattleSystemOTB','description','shift','_targetHomeX','_statusWindow','InfinityClamp','_partyCommandWindow','_homeY','toUpperCase','makeActionOrders','_subjectX','updateSelectionEffect','addState','setBattleSystemOTBTurnOrderVisible','452219cntdTt','update','status','Game_Battler_performCollapse','_sourceArray','selectNextActorOTB','includes','_previewContainer','battleEnd','splice','iconHeight','calculateTargetIndex','getBattleSystem','GetAllIndicies','makeSpeed','TargetFollOrder','iconWidth','drawBgImage','commandGuard','_positionTargetX','loadEnemy','138599kJUMni','isAlive','enemy','performCollapse','battlerHue','onEnemyCancel','_surprise','_plural','Scene_Battle_onActorCancel','anchor','ARRAYSTRUCT','BattleManager_isActiveTpb','initBattleSystemOTB','EnableActionTimes','ceil','EVAL','changeSourceArray','OtbTurnOrderActorIcon','shiftTurnOrderForSubject','StatusWindow','removeChild','lineHeight','updatePosition','moveToPosition','_actorCommandWindow','makeActionTimes','EnemyBattlerType','getChildIndex','addChildAt','_homeX','Game_Party_addActor','JSON','2015141HLAOqC','IconIndex','drawUiText','stepForward','isTpb','min','otbRemoveUnableTurnOrderSprites','LogWindowOffsetY','actor','currentSymbol','faceIndex','dimColor2','loadSystem','FaceIndex','_otbTimesActedThisTurn','Scene_Battle_onEnemyOk','isInfinitySpeedOTB','_isBattleOver','faceHeight','getNextSubject','_forcedBattlers','OTB_CONVERT_AGI_DEBUFF_NEXT_TURN','_windowLayer','TargetNextOrder','UiSubjectOffsetY','_containerWidth','getStateIdWithName','createBorderSprite','resetFontSettings','code','parameters','_graphicEnemy','BgImageOffsetX','top','addActor','subject','PreviewEnemy','OTB_STUN_INFINITY_CLAMP','setText','_otbTurnOrderVisible','Game_Party_removeActor','_tempActor','setHue','_positionDuration','createChildren','BattleManager_isTurnBased','faceWidth','bitmapWidth','_unit','IconSet','setBlendColor','updateOpacity','FUNC','otbGainInstant','EFFECT_ADD_DEBUFF','clear','_isAlive','left','_graphicIconIndex','additionalTargetXAdjustments','Scene_Battle_onSkillOk','getStateTooltipBattler','bind','setItem','applyItemUserEffect','updateGraphicHue','otbShiftNextTurnSpritesToCurrentTurn','initMembersOTB','OTB_CONVERT_AGI_BUFF_CURRENT_TURN','1283351uppulQ','preEndActionOTB','battlerName','opacity','needsSelection','contentsBack','EnemyBattlerDrawLetter','members','17IIDvTV','isSceneBattle','updatePadding','processUpdateGraphic','isBattleItemWindowOTB','%1SystemBg','ConvertAgiBuffNext','scale','OTB_CONVERT_AGI_DEBUFF_CURRENT_TURN','DisplayPosition','removeActionBattlersOTB','BattleManager_endTurn','_tempBattler','createSpriteContainers','match','_otbTurnOrderFaceName','OTB_ADDED_ACTION_TIMES','BattleManager_battleSys','onItemOk','_ogWindowLayerY','drawText','height','updateGraphic','UiNextOffsetX','Game_Battler_removeState','name','createActorCommandWindowOTB','canInput','_backgroundSprite','width','Game_BattlerBase_appear','removeState','hide','_index','updateTurnOrders','UserNextOrder','_otbTurnOrderIconIndex','17297ZCqdYW','exit','_graphicSv','commandCancel','applyItemTargetEffectOTB','getUnitSideSide','createOrderPreviewSprite','ActionBattlersNextFilter','OtbTurnOrderClearEnemyGraphic','otbRemoveCurrentSubject','battleMembers','endTurn','_fadeSpeed','adjustForPreview','addBattlerToTurnOrderAtStart','onBattleStart','_containerHeight','startInputOTB','_preemptive','TurnOrder','TurnOrderOTBGraphicFaceName','Mechanics','currentExt','_forceAction','commandAttack','BgImageFilename','icon','containerWindow','indexOf','_ogWindowLayerX','UiCurrentOffsetX','guard','ARRAYNUM','contents','removeCurrentSubject','Game_Battler_makeSpeed','transparent','_isAppeared','_currentActor','_speed','otbReturnBattlerToTurnOrders','UserFollOrder','select','note','_phase','isOTB','Game_BattlerBase_hide','changeIconGraphicBitmap','canMove','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','SubjectDistance','mainFontFace','removeStatesAuto','call','UiSubjectText','bitmap','clearRect','_graphicHue','BattleManager_processTurn','startTurn','updateLetter','requestUpdateTurnOrders','UserCurrOrder','PreviewActor','SpriteThin','isTurnBased','otbShiftTurnOrderForSubject','boxWidth','otbCreateNewTurnOrderSprites','Game_Action_allowRandomSpeed','253090JuqGww','previewOrderByAction','otbCalcTargetNextOrderChange','turnOrderChangeOTB','_currentTurn','isPreviousSceneBattleTransitionable','floor','ScreenBuffer','changeEnemyGraphicBitmap','bottom','ConvertParams','otbPreviewOrderChange','Window_Selectable_select','onTurnEndOTB','VisuMZ_2_PartySystem','%1BorderColor','STR','processTurn','startActorCommandSelection','close','createTurnOrderSprites','prototype','onActorOk','visible','setSkill','Scene_Battle_commandCancel','svactor','windowRect','updateVisibility','UiSubjectOffsetX','format','UiNextText','EFFECT_ADD_BUFF','onItemCancel','initialize','item','performActionEndOTB','AllowRandomSpeed','createNewTurnOrderSprites','createTurnOrderOTBGraphicType','_blendColor','sortContainer','SideviewBattleUI','selectNextCommand','isHorz','_instance','boxHeight','FaceName','ConvertAgiDebuffNext','createActorCommandWindow','createLetterSprite','isActiveTpb','version','length','_offset','setup','_contentsBackSprite','Game_Actor_selectNextCommand','makeActions','processTurnOTB','commandCancelOTB','ARRAYJSON','round','_fadeDuration','UiCurrentOffsetY','clearTurnOrderOTBGraphics','MoveDistance','setGuard','createTurnOrderOTBGraphicFaceName','_logWindow','Game_Battler_onBattleEnd','BattleManager_isTpb','createInitialPositions','BgDimStyle','DisplayOffsetY','Game_Battler_performActionEnd','onTurnEnd','initHomePositions','Scene_Battle_onSkillCancel','active','_targetHomeY','Enemies','initMembers','selectNextActor','_nextX','_letter','makeNextActionOrdersOTB','isActor','TurnOrderOTBGraphicType','createGraphicSprite','ARRAYFUNC','clearOrderPreview','filter','UiNextOffsetY','Game_System_initialize','finishActorInput','UiCurrentText','_handlers','_requestTurnOrderUpdate','OTB','createOTBTurnOrderWindow','hasSvBattler','isEnemy','addLoadListener','createAllWindows','onEnemyOk','BattleManager_finishActorInput','otbAddBattlerToTurnOrderAtEnd','Game_Action_speed','endBattlerActions','RepositionTopHelpY','commandFight','_letterSprite','drawDimmedArea','otbPreviewOrderClear','Game_Battler_addState','speed','OrderDirection','return\x200','performActionEnd','_previewCurrent','_graphicType','NUM','8WmxFTy','allowRandomSpeed','_otb_actionBattlersNext','EnemyBattlerFaceName','clamp','constructor','createOrderPreview','Conversion','svBattlerName','_subject','createTurnOrderOTBGraphicIconIndex','RegExp','otbCalcUserNextOrderChange','_otb_createdFirstTurnOrders','findIndex','_fadeTarget','OTB_STUN_INFINITY_SPEED','_otbTurnOrderWindow','_spriteContainer','canChangeOtbTurnOrder','_scene','startInput','_nextTurn','ShowMarkerBg','unshift','battleSys','changeSvActorGraphicBitmap','faceName','addChild','battler','_graphicFaceName','startFade','endAction','fillRect','VisuMZ_3_SideviewBattleUI','_previewNext','addChildToBack','map','onSkillCancel','OtbTurnOrderEnemyFace','isBattleMember','BattleManager_endAction','allBattleMembers','removeActor','OtbTurnOrderEnemyIcon','parse','UiAlignment','onActorCancel','_bgImageSprite','Scene_Battle_createAllWindows','shiftNextTurnSpritesToCurrentTurn','EnemyBattlerIcon','Window_Help_setItem','padding','Scene_Battle_onActorOk','BattleSystemOTB','isPartyCommandWindowDisabled','2IWBFjo','PreviewOffsetY','remove','_spriteGroupWidth','push','trim','sort','_stateIDs','fontSize','defaultPosition','Instant','OtbTurnOrderActorFace','otbUnshiftBattlerToTurnOrders','BattleManager_selectNextActor','processSpriteRemoval','children','SystemTurnOrderVisibility','Game_Action_applyItemUserEffect','otbCalcUserCurrentOrderChange','Actor','_actionBattlers','makeActionOrdersOTB','makeDeepCopy','PostStunInfinitySpeed'];const _0x5b94=function(_0x535d5e,_0x422664){_0x535d5e=_0x535d5e-0xbd;let _0x184df0=_0x184d[_0x535d5e];return _0x184df0;};const _0x2b848d=_0x5b94;(function(_0x520293,_0x3d79c3){const _0x5745fd=_0x5b94;while(!![]){try{const _0x22400b=parseInt(_0x5745fd(0x162))*parseInt(_0x5745fd(0x12e))+parseInt(_0x5745fd(0x1ae))+-parseInt(_0x5745fd(0x255))*-parseInt(_0x5745fd(0x230))+-parseInt(_0x5745fd(0x228))+parseInt(_0x5745fd(0x1c3))*-parseInt(_0x5745fd(0xf5))+parseInt(_0x5745fd(0x29b))+parseInt(_0x5745fd(0x1e3));if(_0x22400b===_0x3d79c3)break;else _0x520293['push'](_0x520293['shift']());}catch(_0xfdbaaa){_0x520293['push'](_0x520293['shift']());}}}(_0x184d,0x9db24));var label=_0x2b848d(0x12c),tier=tier||0x0,dependencies=[],pluginData=$plugins['filter'](function(_0x3d6d17){const _0x2f08d0=_0x2b848d;return _0x3d6d17[_0x2f08d0(0x1b0)]&&_0x3d6d17[_0x2f08d0(0x1a1)][_0x2f08d0(0x1b4)]('['+label+']');})[0x0];VisuMZ[label]['Settings']=VisuMZ[label][_0x2b848d(0x18b)]||{},VisuMZ[_0x2b848d(0x2a5)]=function(_0x185e10,_0x11c9b0){const _0x1e0ca2=_0x2b848d;for(const _0x544cd1 in _0x11c9b0){if(_0x544cd1[_0x1e0ca2(0x23e)](/(.*):(.*)/i)){const _0x4acb9d=String(RegExp['$1']),_0x507a58=String(RegExp['$2'])[_0x1e0ca2(0x1a8)]()[_0x1e0ca2(0x133)]();let _0x12dade,_0x1be616,_0x4e8755;switch(_0x507a58){case _0x1e0ca2(0xf4):_0x12dade=_0x11c9b0[_0x544cd1]!==''?Number(_0x11c9b0[_0x544cd1]):0x0;break;case _0x1e0ca2(0x275):_0x1be616=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):[],_0x12dade=_0x1be616[_0x1e0ca2(0x11a)](_0x3e6d88=>Number(_0x3e6d88));break;case _0x1e0ca2(0x1d2):_0x12dade=_0x11c9b0[_0x544cd1]!==''?eval(_0x11c9b0[_0x544cd1]):null;break;case'ARRAYEVAL':_0x1be616=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):[],_0x12dade=_0x1be616['map'](_0x4be4e8=>eval(_0x4be4e8));break;case _0x1e0ca2(0x1e2):_0x12dade=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):'';break;case _0x1e0ca2(0x2d8):_0x1be616=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):[],_0x12dade=_0x1be616[_0x1e0ca2(0x11a)](_0x61f2ad=>JSON['parse'](_0x61f2ad));break;case _0x1e0ca2(0x217):_0x12dade=_0x11c9b0[_0x544cd1]!==''?new Function(JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1])):new Function(_0x1e0ca2(0xf0));break;case _0x1e0ca2(0xd4):_0x1be616=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):[],_0x12dade=_0x1be616['map'](_0xb81856=>new Function(JSON[_0x1e0ca2(0x122)](_0xb81856)));break;case _0x1e0ca2(0x2ab):_0x12dade=_0x11c9b0[_0x544cd1]!==''?String(_0x11c9b0[_0x544cd1]):'';break;case'ARRAYSTR':_0x1be616=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):[],_0x12dade=_0x1be616['map'](_0x1562b4=>String(_0x1562b4));break;case'STRUCT':_0x4e8755=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):{},_0x12dade=VisuMZ[_0x1e0ca2(0x2a5)]({},_0x4e8755);break;case _0x1e0ca2(0x1cd):_0x1be616=_0x11c9b0[_0x544cd1]!==''?JSON[_0x1e0ca2(0x122)](_0x11c9b0[_0x544cd1]):[],_0x12dade=_0x1be616[_0x1e0ca2(0x11a)](_0x17cc6d=>VisuMZ[_0x1e0ca2(0x2a5)]({},JSON[_0x1e0ca2(0x122)](_0x17cc6d)));break;default:continue;}_0x185e10[_0x4acb9d]=_0x12dade;}}return _0x185e10;},(_0x56a0d5=>{const _0x47c660=_0x2b848d,_0x3d8189=_0x56a0d5[_0x47c660(0x249)];for(const _0x13393a of dependencies){if(!Imported[_0x13393a]){alert('%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.'[_0x47c660(0x2b9)](_0x3d8189,_0x13393a)),SceneManager[_0x47c660(0x256)]();break;}}const _0x20076c=_0x56a0d5[_0x47c660(0x1a1)];if(_0x20076c[_0x47c660(0x23e)](/\[Version[ ](.*?)\]/i)){const _0x29caa5=Number(RegExp['$1']);_0x29caa5!==VisuMZ[label][_0x47c660(0x2cf)]&&(alert(_0x47c660(0x286)[_0x47c660(0x2b9)](_0x3d8189,_0x29caa5)),SceneManager[_0x47c660(0x256)]());}if(_0x20076c[_0x47c660(0x23e)](/\[Tier[ ](\d+)\]/i)){const _0x22b1c4=Number(RegExp['$1']);_0x22b1c4<tier?(alert('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0x47c660(0x2b9)](_0x3d8189,_0x22b1c4,tier)),SceneManager[_0x47c660(0x256)]()):tier=Math[_0x47c660(0x168)](_0x22b1c4,tier);}VisuMZ[_0x47c660(0x2a5)](VisuMZ[label][_0x47c660(0x18b)],_0x56a0d5[_0x47c660(0x201)]);})(pluginData),PluginManager['registerCommand'](pluginData[_0x2b848d(0x249)],_0x2b848d(0x1d4),_0x198be6=>{const _0x183d94=_0x2b848d;VisuMZ[_0x183d94(0x2a5)](_0x198be6,_0x198be6);const _0x220706=_0x198be6[_0x183d94(0x161)],_0x7ddc0e=_0x198be6[_0x183d94(0x1e4)];for(const _0x22c80f of _0x220706){const _0x39fcd3=$gameActors[_0x183d94(0x1eb)](_0x22c80f);if(!_0x39fcd3)continue;_0x39fcd3[_0x183d94(0x147)]=_0x183d94(0x26f),_0x39fcd3[_0x183d94(0x254)]=_0x7ddc0e;}}),PluginManager[_0x2b848d(0x182)](pluginData[_0x2b848d(0x249)],_0x2b848d(0x139),_0x48a0f8=>{const _0x84752a=_0x2b848d;VisuMZ['ConvertParams'](_0x48a0f8,_0x48a0f8);const _0x2a952e=_0x48a0f8['Actors'],_0x285279=_0x48a0f8[_0x84752a(0x2ca)],_0x27f817=_0x48a0f8[_0x84752a(0x1f0)];for(const _0x58bfeb of _0x2a952e){const _0x46d780=$gameActors[_0x84752a(0x1eb)](_0x58bfeb);if(!_0x46d780)continue;_0x46d780['_otbTurnOrderGraphicType']=_0x84752a(0x17f),_0x46d780['_otbTurnOrderFaceName']=_0x285279,_0x46d780[_0x84752a(0x194)]=_0x27f817;}}),PluginManager[_0x2b848d(0x182)](pluginData[_0x2b848d(0x249)],_0x2b848d(0x151),_0x20104d=>{const _0x559272=_0x2b848d;VisuMZ[_0x559272(0x2a5)](_0x20104d,_0x20104d);const _0x5f2c53=_0x20104d[_0x559272(0x161)];for(const _0x415d5d of _0x5f2c53){const _0xd85365=$gameActors[_0x559272(0x1eb)](_0x415d5d);if(!_0xd85365)continue;_0xd85365[_0x559272(0x2dc)]();}}),PluginManager[_0x2b848d(0x182)](pluginData['name'],_0x2b848d(0x121),_0x80722b=>{const _0x2d77bd=_0x2b848d;VisuMZ[_0x2d77bd(0x2a5)](_0x80722b,_0x80722b);const _0x1e8798=_0x80722b[_0x2d77bd(0xcb)],_0x1d20b4=_0x80722b[_0x2d77bd(0x1e4)];for(const _0x4d9fa2 of _0x1e8798){const _0x44a3cd=$gameTroop[_0x2d77bd(0x22f)]()[_0x4d9fa2];if(!_0x44a3cd)continue;_0x44a3cd[_0x2d77bd(0x147)]=_0x2d77bd(0x26f),_0x44a3cd[_0x2d77bd(0x254)]=_0x1d20b4;}}),PluginManager[_0x2b848d(0x182)](pluginData[_0x2b848d(0x249)],_0x2b848d(0x11c),_0x278cae=>{const _0x3b4fda=_0x2b848d;VisuMZ[_0x3b4fda(0x2a5)](_0x278cae,_0x278cae);const _0x10f27b=_0x278cae[_0x3b4fda(0xcb)],_0x496a18=_0x278cae[_0x3b4fda(0x2ca)],_0x30c95f=_0x278cae[_0x3b4fda(0x1f0)];for(const _0x491e3b of _0x10f27b){const _0x2576d6=$gameTroop[_0x3b4fda(0x22f)]()[_0x491e3b];if(!_0x2576d6)continue;_0x2576d6[_0x3b4fda(0x147)]=_0x3b4fda(0x17f),_0x2576d6[_0x3b4fda(0x23f)]=_0x496a18,_0x2576d6[_0x3b4fda(0x194)]=_0x30c95f;}}),PluginManager[_0x2b848d(0x182)](pluginData[_0x2b848d(0x249)],_0x2b848d(0x25d),_0x4b6dab=>{const _0x3ffa30=_0x2b848d;VisuMZ['ConvertParams'](_0x4b6dab,_0x4b6dab);const _0x149a51=_0x4b6dab[_0x3ffa30(0xcb)];for(const _0x3b54b7 of _0x149a51){const _0xed5eee=$gameTroop[_0x3ffa30(0x22f)]()[_0x3b54b7];if(!_0xed5eee)continue;_0xed5eee[_0x3ffa30(0x2dc)]();}}),PluginManager[_0x2b848d(0x182)](pluginData[_0x2b848d(0x249)],_0x2b848d(0x13e),_0x228ca1=>{const _0x31d712=_0x2b848d;VisuMZ['ConvertParams'](_0x228ca1,_0x228ca1);const _0x444169=_0x228ca1['Visible'];$gameSystem[_0x31d712(0x1ad)](_0x444169);}),VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x100)]={'Instant':/<OTB (?:INSTANT|INSTANT CAST|INSTANT USE)>/i,'UserFollOrder':/<OTB USER FOLLOW TURN: ([\+\-]\d+)>/i,'UserCurrOrder':/<OTB USER CURRENT TURN: ([\+\-]\d+)>/i,'UserNextOrder':/<OTB USER NEXT TURN: ([\+\-]\d+)>/i,'TargetFollOrder':/<OTB TARGET FOLLOW TURN: ([\+\-]\d+)>/i,'TargetCurrOrder':/<OTB TARGET CURRENT TURN: ([\+\-]\d+)>/i,'TargetNextOrder':/<OTB TARGET NEXT TURN: ([\+\-]\d+)>/i},DataManager[_0x2b848d(0x1fd)]=function(_0x318ad8){const _0x5aea52=_0x2b848d;_0x318ad8=_0x318ad8['toUpperCase']()[_0x5aea52(0x133)](),this[_0x5aea52(0x135)]=this[_0x5aea52(0x135)]||{};if(this['_stateIDs'][_0x318ad8])return this[_0x5aea52(0x135)][_0x318ad8];for(const _0x2e1ae4 of $dataStates){if(!_0x2e1ae4)continue;this[_0x5aea52(0x135)][_0x2e1ae4[_0x5aea52(0x249)][_0x5aea52(0x1a8)]()[_0x5aea52(0x133)]()]=_0x2e1ae4['id'];}return this[_0x5aea52(0x135)][_0x318ad8]||0x0;},SceneManager['isSceneBattle']=function(){const _0x42d2b7=_0x2b848d;return this[_0x42d2b7(0x109)]&&this[_0x42d2b7(0x109)][_0x42d2b7(0xfa)]===Scene_Battle;},VisuMZ['BattleSystemOTB'][_0x2b848d(0x165)]=BattleManager['setup'],BattleManager['setup']=function(_0x547aa7,_0x3fe3d3,_0xedbd9e){const _0x505b8c=_0x2b848d;VisuMZ[_0x505b8c(0x12c)][_0x505b8c(0x165)][_0x505b8c(0x28a)](this,_0x547aa7,_0x3fe3d3,_0xedbd9e),this[_0x505b8c(0x226)]();},BattleManager[_0x2b848d(0x226)]=function(){const _0x6a9c51=_0x2b848d;if(!this[_0x6a9c51(0x282)]())return;this['_otb_actionBattlersNext']=[],this[_0x6a9c51(0x102)]=![];},VisuMZ['BattleSystemOTB'][_0x2b848d(0x241)]=BattleManager['battleSys'],BattleManager[_0x2b848d(0x10e)]=function(){const _0x2556f7=_0x2b848d;if(this['isOTB']())return _0x2556f7(0xdd);return VisuMZ[_0x2556f7(0x12c)][_0x2556f7(0x241)]['call'](this);},BattleManager[_0x2b848d(0x282)]=function(){const _0x27b2b8=_0x2b848d;return $gameSystem[_0x27b2b8(0x1ba)]()==='OTB';},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0xc1)]=BattleManager[_0x2b848d(0x1e7)],BattleManager[_0x2b848d(0x1e7)]=function(){const _0x3bc4f2=_0x2b848d;if(this[_0x3bc4f2(0x282)]())return![];return VisuMZ[_0x3bc4f2(0x12c)]['BattleManager_isTpb'][_0x3bc4f2(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x1ce)]=BattleManager['isActiveTpb'],BattleManager[_0x2b848d(0x2ce)]=function(){const _0x1928ce=_0x2b848d;if(this['isOTB']())return![];return VisuMZ[_0x1928ce(0x12c)][_0x1928ce(0x1ce)]['call'](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x210)]=BattleManager[_0x2b848d(0x296)],BattleManager[_0x2b848d(0x296)]=function(){const _0x36a6b0=_0x2b848d;if(this[_0x36a6b0(0x282)]())return!![];return VisuMZ[_0x36a6b0(0x12c)]['BattleManager_isTurnBased']['call'](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x14a)]=BattleManager[_0x2b848d(0x10a)],BattleManager['startInput']=function(){const _0x30dd04=_0x2b848d;VisuMZ[_0x30dd04(0x12c)][_0x30dd04(0x14a)][_0x30dd04(0x28a)](this),this[_0x30dd04(0x282)]()&&$gameParty[_0x30dd04(0x24b)]()&&!this[_0x30dd04(0x1c9)]&&this['startInputOTB']();},BattleManager[_0x2b848d(0x266)]=function(){const _0x4d7124=_0x2b848d;this[_0x4d7124(0x290)]();},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x28f)]=BattleManager['processTurn'],BattleManager[_0x2b848d(0x2ac)]=function(){const _0x2a7935=_0x2b848d;this['isOTB']()?this[_0x2a7935(0x2d6)]():VisuMZ[_0x2a7935(0x12c)][_0x2a7935(0x28f)]['call'](this);},BattleManager[_0x2b848d(0x2d6)]=function(){const _0x7db7f6=_0x2b848d,_0x21f543=this[_0x7db7f6(0xfe)];if(_0x21f543[_0x7db7f6(0xd1)]()&&_0x21f543[_0x7db7f6(0x24b)]()){const _0x1cb612=_0x21f543['currentAction']();if(!_0x1cb612)VisuMZ[_0x7db7f6(0x12c)]['BattleManager_processTurn'][_0x7db7f6(0x28a)](this);else _0x1cb612[_0x7db7f6(0x26c)]?VisuMZ[_0x7db7f6(0x12c)][_0x7db7f6(0x28f)][_0x7db7f6(0x28a)](this):(this[_0x7db7f6(0x27b)]=_0x21f543,this[_0x7db7f6(0x181)]());}else VisuMZ['BattleSystemOTB']['BattleManager_processTurn'][_0x7db7f6(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0xe4)]=BattleManager[_0x2b848d(0xd9)],BattleManager['finishActorInput']=function(){const _0x36467e=_0x2b848d;this[_0x36467e(0x282)]()?VisuMZ[_0x36467e(0x12c)][_0x36467e(0x28f)][_0x36467e(0x28a)](this):VisuMZ['BattleSystemOTB'][_0x36467e(0xe4)][_0x36467e(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)]['BattleManager_selectNextActor']=BattleManager['selectNextActor'],BattleManager[_0x2b848d(0xcd)]=function(){const _0x11083c=_0x2b848d;this[_0x11083c(0x282)]()?this['selectNextActorOTB']():VisuMZ[_0x11083c(0x12c)][_0x11083c(0x13b)][_0x11083c(0x28a)](this);},BattleManager[_0x2b848d(0x1b3)]=function(){this['_currentActor']=null,this['_inputting']=![];},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x11e)]=BattleManager['endAction'],BattleManager[_0x2b848d(0x115)]=function(){const _0x367823=_0x2b848d;this['preEndActionOTB'](),VisuMZ['BattleSystemOTB'][_0x367823(0x11e)][_0x367823(0x28a)](this),this['postEndActionOTB']();},BattleManager[_0x2b848d(0x229)]=function(){const _0x45a33a=_0x2b848d;if(!this[_0x45a33a(0x282)]())return;this[_0x45a33a(0x23a)](),this[_0x45a33a(0xfe)]&&this[_0x45a33a(0xfe)]['canMove']()&&this[_0x45a33a(0x142)]['includes'](this[_0x45a33a(0xfe)])&&this[_0x45a33a(0xfe)][_0x45a33a(0x2d5)]();},BattleManager['postEndActionOTB']=function(){const _0x192009=_0x2b848d;if(!this[_0x192009(0x282)]())return;this['removeActionBattlersOTB']();this[_0x192009(0xfe)]&&(this[_0x192009(0xe7)](this['_subject']),this[_0x192009(0xfe)]=null);this['_forcedBattlers']['length']>0x0&&(this[_0x192009(0xfe)]=this[_0x192009(0x1f6)]());;},BattleManager[_0x2b848d(0x240)]=VisuMZ['BattleSystemOTB'][_0x2b848d(0x18b)]['Mechanics'][_0x2b848d(0x1d0)],BattleManager[_0x2b848d(0x16b)]=VisuMZ[_0x2b848d(0x12c)]['Settings'][_0x2b848d(0x26a)]['RandomizeActionTimesOrder'],BattleManager[_0x2b848d(0x208)]=VisuMZ['BattleSystemOTB']['Settings'][_0x2b848d(0x26a)][_0x2b848d(0x1a5)],VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x14c)]=BattleManager[_0x2b848d(0x1a9)],BattleManager['makeActionOrders']=function(){const _0x3fdb6c=_0x2b848d;this['isOTB']()?this['makeActionOrdersOTB']():VisuMZ[_0x3fdb6c(0x12c)][_0x3fdb6c(0x14c)][_0x3fdb6c(0x28a)](this);},BattleManager[_0x2b848d(0x143)]=function(){const _0x3a7e84=_0x2b848d;let _0x5c3ee4=this[_0x3a7e84(0x102)]?0x1:0x2;while(_0x5c3ee4--){this[_0x3a7e84(0xd0)]();}const _0x582cd9=!this['_otb_createdFirstTurnOrders'];this[_0x3a7e84(0x102)]=!![];},BattleManager[_0x2b848d(0xd0)]=function(){const _0x5c6a30=_0x2b848d;this['_actionBattlers']=this['_otb_actionBattlersNext'],this[_0x5c6a30(0x225)]();const _0x4934b1=[];_0x4934b1['push'](...$gameParty[_0x5c6a30(0x25f)]()),_0x4934b1['push'](...$gameTroop[_0x5c6a30(0x22f)]());for(const _0x4a49af of _0x4934b1){_0x4a49af[_0x5c6a30(0x1bc)]();}_0x4934b1[_0x5c6a30(0x134)]((_0x2771b0,_0x344596)=>_0x344596[_0x5c6a30(0xee)]()-_0x2771b0['speed']()),this[_0x5c6a30(0xf7)]=_0x4934b1,this[_0x5c6a30(0x189)](),this[_0x5c6a30(0x23a)](),this[_0x5c6a30(0x299)]();},BattleManager[_0x2b848d(0x189)]=function(){const _0xb8f38c=_0x2b848d;if(!BattleManager['OTB_ADDED_ACTION_TIMES'])return;const _0x506604=this[_0xb8f38c(0xf7)],_0x410dc0=this[_0xb8f38c(0x11f)]();for(const _0x1db38b of _0x410dc0){if(!_0x1db38b)continue;if(!_0x1db38b[_0xb8f38c(0x19c)]())continue;if(!_0x1db38b[_0xb8f38c(0x1c4)]())continue;if(!_0x506604[_0xb8f38c(0x1b4)](_0x1db38b))continue;const _0x536dbe=_0x506604[_0xb8f38c(0x271)](_0x1db38b);let _0xa3750a=_0x1db38b[_0xb8f38c(0x1dc)]()-0x1;while(_0xa3750a--){let _0x3a1b9b=_0x536dbe;BattleManager[_0xb8f38c(0x16b)]&&(_0x3a1b9b=Math[_0xb8f38c(0x15f)](_0x506604[_0xb8f38c(0x2d0)]-_0x536dbe)+_0x536dbe),_0x506604[_0xb8f38c(0x1b7)](_0x3a1b9b,0x0,_0x1db38b);}}},BattleManager[_0x2b848d(0x23a)]=function(){const _0x20d507=_0x2b848d;if(!this[_0x20d507(0x282)]())return;this[_0x20d507(0x142)]=this[_0x20d507(0x142)]||[],this[_0x20d507(0x142)][_0x20d507(0x130)](null),this[_0x20d507(0x142)]['remove'](undefined),this[_0x20d507(0x142)]=this[_0x20d507(0x142)][_0x20d507(0xd6)](_0x201c6d=>_0x201c6d[_0x20d507(0x11d)]()),this[_0x20d507(0x142)]=this[_0x20d507(0x142)][_0x20d507(0xd6)](_0x5191fb=>VisuMZ[_0x20d507(0x12c)]['ActionBattlersFilter'](_0x5191fb)),this['_surprise']&&(this[_0x20d507(0x142)]=this[_0x20d507(0x142)][_0x20d507(0xd6)](_0x5c348e=>!_0x5c348e['isActor']())),this[_0x20d507(0x267)]&&(this[_0x20d507(0x142)]=this['_actionBattlers'][_0x20d507(0xd6)](_0x5eca62=>!_0x5eca62[_0x20d507(0xe0)]())),this[_0x20d507(0xf7)]=this[_0x20d507(0xf7)]||[],this[_0x20d507(0xf7)][_0x20d507(0x130)](null),this[_0x20d507(0xf7)]['remove'](undefined),this[_0x20d507(0xf7)]=this[_0x20d507(0xf7)]['filter'](_0x17a82d=>_0x17a82d['isBattleMember']()),this['_otb_actionBattlersNext']=this[_0x20d507(0xf7)]['filter'](_0x23a32b=>VisuMZ[_0x20d507(0x12c)][_0x20d507(0x25c)](_0x23a32b)),this[_0x20d507(0x1e9)](),this[_0x20d507(0x15c)]();},VisuMZ[_0x2b848d(0x12c)]['ActionBattlersFilter']=function(_0x412573){const _0x2b81c6=_0x2b848d;if(!_0x412573)return![];if(!_0x412573[_0x2b81c6(0x1c4)]())return![];if(!_0x412573[_0x2b81c6(0x19c)]())return![];return _0x412573[_0x2b81c6(0x285)]();},VisuMZ[_0x2b848d(0x12c)]['ActionBattlersNextFilter']=function(_0x5803c8){const _0x32d599=_0x2b848d;if(!_0x5803c8)return![];const _0x2a0f98=JsonEx[_0x32d599(0x144)](_0x5803c8);return _0x2a0f98[_0x32d599(0x20c)]=!![],_0x2a0f98[_0x32d599(0x23c)]=!![],_0x2a0f98[_0x32d599(0x17d)](),_0x2a0f98['removeStatesAuto'](0x1),_0x2a0f98[_0x32d599(0x289)](0x2),_0x2a0f98['refresh'](),VisuMZ[_0x32d599(0x12c)][_0x32d599(0x191)](_0x2a0f98);},BattleManager[_0x2b848d(0x29e)]=function(_0x1d9cfd,_0x1486e4,_0x5492ce){const _0x4f53b9=_0x2b848d;if(!_0x1486e4)return;const _0x57c057=_0x5492ce?this['_otb_actionBattlersNext']:this[_0x4f53b9(0x142)];if(!_0x57c057)return;if(!_0x57c057['includes'](_0x1d9cfd))return;const _0x5c4bb0=VisuMZ[_0x4f53b9(0x12c)][_0x4f53b9(0x1bb)](_0x1d9cfd,_0x57c057),_0x2b0f88=_0x5492ce?VisuMZ['BattleSystemOTB'][_0x4f53b9(0x199)](_0x57c057):0x0,_0x391065=_0x5c4bb0[_0x4f53b9(0x2d0)]-0x1;for(let _0x12bf3d=_0x391065;_0x12bf3d>=0x0;_0x12bf3d--){_0x57c057[_0x4f53b9(0x1b7)](_0x5c4bb0[_0x12bf3d],0x1);}for(var _0x59dc38=0x0;_0x59dc38<_0x5c4bb0['length'];_0x59dc38++){var _0x9c78b5=(_0x5c4bb0[_0x59dc38]-_0x1486e4)[_0x4f53b9(0xf9)](_0x2b0f88,_0x57c057[_0x4f53b9(0x2d0)]);_0x57c057[_0x4f53b9(0x1b7)](_0x9c78b5,0x0,_0x1d9cfd);}this[_0x4f53b9(0x23a)](),this[_0x4f53b9(0x15c)]();},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x1bb)]=function(_0x2ad125,_0x3b3c83){const _0x23063b=_0x2b848d,_0x1919d3=[],_0x26bdf5=_0x3b3c83['length'];for(let _0x425528=0x0;_0x425528<_0x26bdf5;_0x425528++){if(_0x3b3c83[_0x425528]===_0x2ad125)_0x1919d3[_0x23063b(0x132)](_0x425528);}return _0x1919d3;},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x199)]=function(_0x412504){const _0x25b571=_0x2b848d;if(!BattleManager[_0x25b571(0x208)])return 0x0;if(!_0x412504)return 0x0;let _0x41c5bd=0x0;const _0x3969a4=_0x412504[_0x25b571(0x2d0)];for(let _0x25784b=0x0;_0x25784b<_0x3969a4;_0x25784b++){const _0x3b430c=_0x412504[_0x25784b];if(!_0x3b430c)continue;if(_0x3b430c[_0x25b571(0xee)]()!==Infinity)return _0x25784b;else _0x41c5bd++;}return _0x41c5bd;},BattleManager[_0x2b848d(0x225)]=function(){const _0x2e3974=_0x2b848d;if(!this[_0x2e3974(0x282)]())return;const _0x273396=SceneManager[_0x2e3974(0x109)][_0x2e3974(0x106)];if(!_0x273396)return;_0x273396[_0x2e3974(0x127)]();},BattleManager[_0x2b848d(0x299)]=function(){const _0x4facb0=_0x2b848d;if(!this[_0x4facb0(0x282)]())return;const _0x175e56=SceneManager[_0x4facb0(0x109)]['_otbTurnOrderWindow'];if(!_0x175e56)return;_0x175e56['createNewTurnOrderSprites']();},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x17a)]=BattleManager[_0x2b848d(0x1f6)],BattleManager['getNextSubject']=function(){const _0x465664=_0x2b848d;return this[_0x465664(0xfe)]=VisuMZ['BattleSystemOTB'][_0x465664(0x17a)]['call'](this),this[_0x465664(0x282)]()&&this[_0x465664(0xfe)]&&this[_0x465664(0x297)](this[_0x465664(0xfe)]),this[_0x465664(0xfe)];},BattleManager[_0x2b848d(0x297)]=function(_0x3467ad){const _0x5650c3=_0x2b848d;if(!this[_0x5650c3(0x282)]())return;const _0x17d787=SceneManager[_0x5650c3(0x109)][_0x5650c3(0x106)];if(!_0x17d787)return;if(!_0x3467ad)return;_0x17d787[_0x5650c3(0x1d5)](_0x3467ad);},BattleManager[_0x2b848d(0x15c)]=function(){const _0x13171e=_0x2b848d;if(!this[_0x13171e(0x282)]())return;const _0x12e54c=SceneManager[_0x13171e(0x109)][_0x13171e(0x106)];if(!_0x12e54c)return;_0x12e54c[_0x13171e(0x292)]();},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x23b)]=BattleManager[_0x2b848d(0x260)],BattleManager[_0x2b848d(0x260)]=function(){const _0x18ea08=_0x2b848d;VisuMZ['BattleSystemOTB'][_0x18ea08(0x23b)][_0x18ea08(0x28a)](this),this[_0x18ea08(0x282)]()&&this[_0x18ea08(0x25e)]();},BattleManager[_0x2b848d(0x25e)]=function(){const _0x1c92dc=_0x2b848d;if(!this[_0x1c92dc(0x282)]())return;const _0x3ef831=SceneManager[_0x1c92dc(0x109)][_0x1c92dc(0x106)];if(!_0x3ef831)return;_0x3ef831[_0x1c92dc(0x277)]();},BattleManager[_0x2b848d(0x1e9)]=function(){const _0x214fd0=_0x2b848d;if(!this[_0x214fd0(0x282)]())return;const _0x471733=SceneManager[_0x214fd0(0x109)][_0x214fd0(0x106)];if(!_0x471733)return;_0x471733['removeUnableTurnOrderSprites']();},BattleManager[_0x2b848d(0x27d)]=function(_0x3e02a5){const _0x45200e=_0x2b848d;if(!_0x3e02a5)return;const _0xc23d31=_0x3e02a5[_0x45200e(0x1dc)]();_0x3e02a5['makeActions']();if(!this[_0x45200e(0x142)][_0x45200e(0x1b4)](_0x3e02a5)){const _0x444fa9=Math[_0x45200e(0x168)](0x0,_0xc23d31-(_0x3e02a5[_0x45200e(0x1f1)]||0x0));this['otbAddBattlerToTurnOrderAtEnd'](_0x3e02a5,_0x444fa9,this[_0x45200e(0x142)]);}if(!this[_0x45200e(0xf7)][_0x45200e(0x1b4)](_0x3e02a5)){const _0x3605c9=_0xc23d31;this[_0x45200e(0xe5)](_0x3e02a5,_0x3605c9,this[_0x45200e(0xf7)]);}},BattleManager['otbAddBattlerToTurnOrderAtEnd']=function(_0x2d645a,_0x4f3743,_0x27c4dc){const _0x805b93=_0x2b848d;if(!this[_0x805b93(0x282)]())return;const _0x8f129b=SceneManager['_scene'][_0x805b93(0x106)];while(_0x4f3743--){_0x27c4dc[_0x805b93(0x132)](_0x2d645a),_0x8f129b&&_0x8f129b[_0x805b93(0x18f)](_0x2d645a,_0x27c4dc);}},BattleManager[_0x2b848d(0x13a)]=function(_0xbf6ced){const _0x3edf63=_0x2b848d;if(!_0xbf6ced)return;const _0x2f9388=_0xbf6ced['makeActionTimes']();_0xbf6ced['makeActions']();if(!this[_0x3edf63(0x142)]['includes'](_0xbf6ced)){const _0x3fea05=Math[_0x3edf63(0x168)](0x0,_0x2f9388-(_0xbf6ced['_otbTimesActedThisTurn']||0x0));this['addBattlerToTurnOrderAtStart'](_0xbf6ced,_0x3fea05,this[_0x3edf63(0x142)]);}if(!this[_0x3edf63(0xf7)]['includes'](_0xbf6ced)){const _0x4df8cd=_0x2f9388;this[_0x3edf63(0x263)](_0xbf6ced,_0x4df8cd,this[_0x3edf63(0xf7)]);}},BattleManager['otbAddBattlerToTurnOrderAtStart']=function(_0x566e11,_0xb73e6a,_0xe824cb){const _0x2a407e=_0x2b848d;if(!this['isOTB']())return;const _0x5c7cac=SceneManager[_0x2a407e(0x109)][_0x2a407e(0x106)];while(_0xb73e6a--){_0xe824cb[_0x2a407e(0x10d)](_0x566e11),_0x5c7cac&&_0x5c7cac[_0x2a407e(0x263)](_0x566e11,_0xe824cb);}},BattleManager[_0x2b848d(0xec)]=function(){const _0x2f3c20=_0x2b848d;if(!this[_0x2f3c20(0x282)]())return;const _0x10d5b7=SceneManager[_0x2f3c20(0x109)]['_otbTurnOrderWindow'];if(!_0x10d5b7)return;_0x10d5b7['previewOrderByAction'](null);},BattleManager[_0x2b848d(0x2a6)]=function(){const _0x2f93f1=_0x2b848d;if(!this[_0x2f93f1(0x282)]())return;const _0x2fefc3=SceneManager[_0x2f93f1(0x109)]['_otbTurnOrderWindow'];if(!_0x2fefc3)return;_0x2fefc3[_0x2f93f1(0x29c)](this[_0x2f93f1(0x164)]());},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0xd8)]=Game_System[_0x2b848d(0x2b0)][_0x2b848d(0x2bd)],Game_System[_0x2b848d(0x2b0)]['initialize']=function(){const _0x7fb3fd=_0x2b848d;VisuMZ[_0x7fb3fd(0x12c)]['Game_System_initialize'][_0x7fb3fd(0x28a)](this),this[_0x7fb3fd(0x1cf)]();},Game_System[_0x2b848d(0x2b0)][_0x2b848d(0x1cf)]=function(){this['_otbTurnOrderVisible']=!![];},Game_System['prototype'][_0x2b848d(0x15a)]=function(){const _0x403ad6=_0x2b848d;return this[_0x403ad6(0x20a)]===undefined&&this[_0x403ad6(0x1cf)](),this[_0x403ad6(0x20a)];},Game_System[_0x2b848d(0x2b0)][_0x2b848d(0x1ad)]=function(_0x249d0c){const _0x4959be=_0x2b848d;this['_otbTurnOrderVisible']===undefined&&this[_0x4959be(0x1cf)](),this['_otbTurnOrderVisible']=_0x249d0c;},Game_Action[_0x2b848d(0x227)]=VisuMZ['BattleSystemOTB'][_0x2b848d(0x18b)][_0x2b848d(0xfc)]['ConvertAgiBuffCurrent'],Game_Action[_0x2b848d(0x238)]=VisuMZ[_0x2b848d(0x12c)]['Settings'][_0x2b848d(0xfc)]['ConvertAgiDebuffCurrent'],Game_Action['OTB_CONVERT_AGI_BUFF_NEXT_TURN']=VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x18b)][_0x2b848d(0xfc)][_0x2b848d(0x236)],Game_Action[_0x2b848d(0x1f8)]=VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x18b)][_0x2b848d(0xfc)][_0x2b848d(0x2cb)],VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0xe6)]=Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0xee)],Game_Action['prototype'][_0x2b848d(0xee)]=function(){const _0x17a023=_0x2b848d;return BattleManager[_0x17a023(0x282)]()?0x0:VisuMZ['BattleSystemOTB'][_0x17a023(0xe6)]['call'](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x150)]=Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x159)],Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x159)]=function(){const _0x304559=_0x2b848d;VisuMZ[_0x304559(0x12c)][_0x304559(0x150)][_0x304559(0x28a)](this),this['applyGlobalBattleSystemOTB']();},Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x1a0)]=function(){const _0x3c3024=_0x2b848d;if(!SceneManager[_0x3c3024(0x231)]())return;if(!BattleManager[_0x3c3024(0x282)]())return;if(!this['item']())return;if(!this[_0x3c3024(0x206)]())return;const _0x527bb4=VisuMZ[_0x3c3024(0x12c)]['RegExp'],_0x3b28b9=this['item']()[_0x3c3024(0x280)];_0x3b28b9[_0x3c3024(0x23e)](_0x527bb4[_0x3c3024(0x138)])&&this['subject']()[_0x3c3024(0x218)](0x1);let _0x330d87=this['otbCalcUserCurrentOrderChange'](),_0x41d6a8=this[_0x3c3024(0x101)]();_0x330d87!==0x0&&BattleManager[_0x3c3024(0x29e)](this[_0x3c3024(0x206)](),-_0x330d87,![]),_0x41d6a8!==0x0&&BattleManager[_0x3c3024(0x29e)](this[_0x3c3024(0x206)](),-_0x41d6a8,!![]);},Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x140)]=function(){const _0x901596=_0x2b848d;if(!SceneManager[_0x901596(0x231)]())return 0x0;if(!BattleManager[_0x901596(0x282)]())return 0x0;if(!this[_0x901596(0x2be)]())return 0x0;if(!this[_0x901596(0x206)]())return 0x0;if(!this[_0x901596(0x206)]()['canChangeOtbTurnOrder']())return 0x0;const _0x5a0e58=VisuMZ[_0x901596(0x12c)][_0x901596(0x100)],_0xfec42=this[_0x901596(0x2be)]()['note'],_0x31e014=BattleManager[_0x901596(0x142)]||[];let _0x138447=0x0;return _0xfec42[_0x901596(0x23e)](_0x5a0e58['UserFollOrder'])&&(_0x31e014[_0x901596(0x1b4)](this[_0x901596(0x206)]())&&(_0x138447+=Number(RegExp['$1']))),_0xfec42[_0x901596(0x23e)](_0x5a0e58[_0x901596(0x293)])&&(_0x138447+=Number(RegExp['$1'])),_0x138447;},Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x101)]=function(){const _0x2ebd02=_0x2b848d;if(!SceneManager[_0x2ebd02(0x231)]())return 0x0;if(!BattleManager[_0x2ebd02(0x282)]())return 0x0;if(!this[_0x2ebd02(0x2be)]())return 0x0;if(!this[_0x2ebd02(0x206)]())return 0x0;if(!this[_0x2ebd02(0x206)]()['canChangeOtbTurnOrder']())return 0x0;const _0x28aaa5=VisuMZ[_0x2ebd02(0x12c)]['Settings'][_0x2ebd02(0x26a)],_0x211146=VisuMZ[_0x2ebd02(0x12c)][_0x2ebd02(0x100)],_0xa28c68=this[_0x2ebd02(0x2be)]()[_0x2ebd02(0x280)],_0x4520f1=BattleManager[_0x2ebd02(0xf7)]||[];let _0x515915=0x0;return _0x28aaa5[_0x2ebd02(0x16c)]&&(_0x515915+=_0x28aaa5['ConvertSpeedJS'][_0x2ebd02(0x28a)](this)),_0xa28c68[_0x2ebd02(0x23e)](_0x211146[_0x2ebd02(0x27e)])&&(_0x4520f1['includes'](this[_0x2ebd02(0x206)]())&&(_0x515915+=Number(RegExp['$1']))),_0xa28c68[_0x2ebd02(0x23e)](_0x211146[_0x2ebd02(0x253)])&&(_0x515915+=Number(RegExp['$1'])),_0x515915;},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x13f)]=Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x223)],Game_Action[_0x2b848d(0x2b0)]['applyItemUserEffect']=function(_0x136bda){const _0x2414aa=_0x2b848d;VisuMZ[_0x2414aa(0x12c)][_0x2414aa(0x13f)]['call'](this,_0x136bda),this[_0x2414aa(0x259)](_0x136bda);},Game_Action[_0x2b848d(0x2b0)]['applyItemTargetEffectOTB']=function(_0x2ceca1){const _0x1d7a35=_0x2b848d;if(!SceneManager['isSceneBattle']())return;if(!BattleManager['isOTB']())return;if(!this[_0x1d7a35(0x2be)]())return;if(!_0x2ceca1)return;if(!_0x2ceca1[_0x1d7a35(0x108)]())return 0x0;let _0x1442e7=this['otbCalcTargetCurrentOrderChange'](_0x2ceca1),_0x3f84f1=this[_0x1d7a35(0x29d)](_0x2ceca1);_0x1442e7!==0x0&&BattleManager[_0x1d7a35(0x29e)](_0x2ceca1,-_0x1442e7,![]),_0x3f84f1!==0x0&&BattleManager[_0x1d7a35(0x29e)](_0x2ceca1,-_0x3f84f1,!![]);},Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0x155)]=function(_0x3c3c2c){const _0x2aaca6=_0x2b848d;if(!SceneManager[_0x2aaca6(0x231)]())return 0x0;if(!BattleManager['isOTB']())return 0x0;if(!this['item']())return 0x0;if(!_0x3c3c2c)return 0x0;if(!_0x3c3c2c[_0x2aaca6(0x108)]())return 0x0;const _0x37c67f=VisuMZ['BattleSystemOTB'][_0x2aaca6(0x100)],_0x34e8a9=this['item']()[_0x2aaca6(0x280)],_0x583983=BattleManager[_0x2aaca6(0x142)]||[];let _0x464266=0x0;_0x34e8a9[_0x2aaca6(0x23e)](_0x37c67f[_0x2aaca6(0x1bd)])&&(_0x583983[_0x2aaca6(0x1b4)](_0x3c3c2c)&&(_0x464266+=Number(RegExp['$1'])));_0x34e8a9[_0x2aaca6(0x23e)](_0x37c67f['TargetCurrOrder'])&&(_0x464266+=Number(RegExp['$1']));const _0x34cbe1=this[_0x2aaca6(0x2be)]()[_0x2aaca6(0x19f)];for(const _0x1710a7 of _0x34cbe1){if(!_0x1710a7)continue;if(_0x1710a7['code']===Game_Action[_0x2aaca6(0x2bb)]&&_0x1710a7[_0x2aaca6(0x178)]===0x6){if(Game_Action[_0x2aaca6(0x227)])_0x464266-=0x1;}if(_0x1710a7['code']===Game_Action['EFFECT_ADD_DEBUFF']&&_0x1710a7[_0x2aaca6(0x178)]===0x6){if(Game_Action[_0x2aaca6(0x238)])_0x464266+=0x1;}}return _0x464266;},Game_Action['prototype'][_0x2b848d(0x29d)]=function(_0x1b5bbf){const _0x309a4c=_0x2b848d;if(!SceneManager[_0x309a4c(0x231)]())return 0x0;if(!BattleManager[_0x309a4c(0x282)]())return 0x0;if(!this[_0x309a4c(0x2be)]())return 0x0;if(!_0x1b5bbf)return 0x0;if(!_0x1b5bbf['canChangeOtbTurnOrder']())return 0x0;const _0x33946d=VisuMZ[_0x309a4c(0x12c)][_0x309a4c(0x100)],_0x25c6c9=this['item']()[_0x309a4c(0x280)],_0x209ccf=BattleManager[_0x309a4c(0xf7)]||[];let _0x7b758b=0x0;_0x25c6c9[_0x309a4c(0x23e)](_0x33946d[_0x309a4c(0x1bd)])&&(_0x209ccf[_0x309a4c(0x1b4)](_0x1b5bbf)&&(_0x7b758b+=Number(RegExp['$1'])));_0x25c6c9[_0x309a4c(0x23e)](_0x33946d[_0x309a4c(0x1fa)])&&(_0x7b758b+=Number(RegExp['$1']));const _0x44eeb3=this[_0x309a4c(0x2be)]()[_0x309a4c(0x19f)];for(const _0x797f6 of _0x44eeb3){if(!_0x797f6)continue;if(_0x797f6['code']===Game_Action[_0x309a4c(0x2bb)]&&_0x797f6[_0x309a4c(0x178)]===0x6){if(Game_Action['OTB_CONVERT_AGI_BUFF_NEXT_TURN'])_0x7b758b-=0x1;}if(_0x797f6[_0x309a4c(0x200)]===Game_Action[_0x309a4c(0x219)]&&_0x797f6[_0x309a4c(0x178)]===0x6){if(Game_Action['OTB_CONVERT_AGI_DEBUFF_NEXT_TURN'])_0x7b758b+=0x1;}}return _0x7b758b;},Game_BattlerBase['prototype'][_0x2b848d(0x2dc)]=function(){const _0x4044ec=_0x2b848d;delete this[_0x4044ec(0x147)],delete this[_0x4044ec(0x23f)],delete this['_otbTurnOrderFaceIndex'],delete this[_0x4044ec(0x254)];},Game_BattlerBase['prototype']['TurnOrderOTBGraphicType']=function(){const _0x25d186=_0x2b848d;return this[_0x25d186(0x147)]===undefined&&(this['_otbTurnOrderGraphicType']=this[_0x25d186(0x2c2)]()),this[_0x25d186(0x147)];},Game_BattlerBase['prototype'][_0x2b848d(0x2c2)]=function(){const _0x23fc21=_0x2b848d;return Window_OTB_TurnOrder[_0x23fc21(0x18b)][_0x23fc21(0x1dd)];},Game_BattlerBase[_0x2b848d(0x2b0)]['TurnOrderOTBGraphicFaceName']=function(){const _0x3bf66f=_0x2b848d;return this[_0x3bf66f(0x23f)]===undefined&&(this[_0x3bf66f(0x23f)]=this[_0x3bf66f(0xbe)]()),this[_0x3bf66f(0x23f)];},Game_BattlerBase['prototype'][_0x2b848d(0xbe)]=function(){const _0x11bc3b=_0x2b848d;return Window_OTB_TurnOrder[_0x11bc3b(0x18b)][_0x11bc3b(0xf8)];},Game_BattlerBase[_0x2b848d(0x2b0)][_0x2b848d(0x16d)]=function(){const _0x4d2910=_0x2b848d;return this[_0x4d2910(0x194)]===undefined&&(this[_0x4d2910(0x194)]=this['createTurnOrderOTBGraphicFaceIndex']()),this[_0x4d2910(0x194)];},Game_BattlerBase[_0x2b848d(0x2b0)]['createTurnOrderOTBGraphicFaceIndex']=function(){const _0x19d8bc=_0x2b848d;return Window_OTB_TurnOrder[_0x19d8bc(0x18b)][_0x19d8bc(0x14d)];},Game_BattlerBase[_0x2b848d(0x2b0)]['TurnOrderOTBGraphicIconIndex']=function(){const _0xaae32d=_0x2b848d;return this[_0xaae32d(0x254)]===undefined&&(this[_0xaae32d(0x254)]=this[_0xaae32d(0xff)]()),this[_0xaae32d(0x254)];},Game_BattlerBase['prototype']['createTurnOrderOTBGraphicIconIndex']=function(){const _0x1f4bd3=_0x2b848d;return Window_OTB_TurnOrder['Settings'][_0x1f4bd3(0x128)];},Game_BattlerBase[_0x2b848d(0x2b0)]['setOTBGraphicIconIndex']=function(_0x138b9c){const _0x4b8714=_0x2b848d;this[_0x4b8714(0x254)]=_0x138b9c;},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x283)]=Game_BattlerBase[_0x2b848d(0x2b0)][_0x2b848d(0x250)],Game_BattlerBase['prototype'][_0x2b848d(0x250)]=function(){const _0x57cc7b=_0x2b848d;VisuMZ[_0x57cc7b(0x12c)][_0x57cc7b(0x283)][_0x57cc7b(0x28a)](this),BattleManager[_0x57cc7b(0x23a)]();},VisuMZ['BattleSystemOTB'][_0x2b848d(0x24e)]=Game_BattlerBase[_0x2b848d(0x2b0)]['appear'],Game_BattlerBase['prototype']['appear']=function(){const _0x4b1677=_0x2b848d,_0x12fbbf=this[_0x4b1677(0x175)];VisuMZ[_0x4b1677(0x12c)]['Game_BattlerBase_appear'][_0x4b1677(0x28a)](this),BattleManager['isOTB']()&&SceneManager['isSceneBattle']()&&_0x12fbbf&&!this['_hidden']&&BattleManager[_0x4b1677(0x27d)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x1b1)]=Game_Battler[_0x2b848d(0x2b0)]['performCollapse'],Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1c6)]=function(){const _0x342179=_0x2b848d;VisuMZ[_0x342179(0x12c)]['Game_Battler_performCollapse']['call'](this),BattleManager[_0x342179(0x23a)]();},Game_Battler['OTB_STUN_INFINITY_SPEED']=VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x18b)][_0x2b848d(0x26a)][_0x2b848d(0x145)],VisuMZ[_0x2b848d(0x12c)]['Game_Battler_onBattleStart']=Game_Battler['prototype'][_0x2b848d(0x264)],Game_Battler['prototype'][_0x2b848d(0x264)]=function(_0x29706d){const _0x5eb33a=_0x2b848d;VisuMZ[_0x5eb33a(0x12c)]['Game_Battler_onBattleStart']['call'](this,_0x29706d),this['onBattleStartOTB'](_0x29706d);},Game_Battler[_0x2b848d(0x2b0)]['onBattleStartOTB']=function(_0x4e74d9){const _0x5119a3=_0x2b848d;if(!BattleManager[_0x5119a3(0x282)]())return;this[_0x5119a3(0x1f1)]=0x0;},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0xc0)]=Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x180)],Game_Battler[_0x2b848d(0x2b0)]['onBattleEnd']=function(){const _0x586227=_0x2b848d;VisuMZ[_0x586227(0x12c)][_0x586227(0xc0)][_0x586227(0x28a)](this),this[_0x586227(0x16a)]();},Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x16a)]=function(){const _0x5b0574=_0x2b848d;if(!BattleManager[_0x5b0574(0x282)]())return;this['_otbTimesActedThisTurn']=0x0;},VisuMZ['BattleSystemOTB']['Game_Battler_performActionEnd']=Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0xf1)],Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0xf1)]=function(){const _0x59ed30=_0x2b848d;VisuMZ[_0x59ed30(0x12c)][_0x59ed30(0xc5)]['call'](this),this[_0x59ed30(0x2bf)]();},Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x2bf)]=function(){const _0x5dc05f=_0x2b848d;if(!BattleManager[_0x5dc05f(0x282)]())return;this[_0x5dc05f(0x1f1)]=this[_0x5dc05f(0x1f1)]||0x0,this[_0x5dc05f(0x1f1)]++;if(this['numActions']()>0x0&&this===BattleManager[_0x5dc05f(0xfe)]){const _0x5c210a=BattleManager[_0x5dc05f(0x1f7)];if(_0x5c210a['length']>0x0&&_0x5c210a[0x0]!==this)return;const _0x107cc2=this[_0x5dc05f(0x112)]();if(_0x107cc2)_0x107cc2[_0x5dc05f(0x1e6)]();}},VisuMZ[_0x2b848d(0x12c)]['Game_Battler_onTurnEnd']=Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0xc6)],Game_Battler[_0x2b848d(0x2b0)]['onTurnEnd']=function(){VisuMZ['BattleSystemOTB']['Game_Battler_onTurnEnd']['call'](this),this['onTurnEndOTB']();},Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x2a8)]=function(){const _0x4b68ad=_0x2b848d;if(!BattleManager[_0x4b68ad(0x282)]())return;this['_otbTimesActedThisTurn']=0x0;},VisuMZ[_0x2b848d(0x12c)]['Game_Battler_makeSpeed']=Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1bc)],Game_Battler[_0x2b848d(0x2b0)]['makeSpeed']=function(){const _0x1ea4c0=_0x2b848d;BattleManager['isOTB']()?this[_0x1ea4c0(0x15d)]():VisuMZ[_0x1ea4c0(0x12c)][_0x1ea4c0(0x278)]['call'](this);},Game_Battler['prototype']['makeOTBSpeed']=function(){const _0x47ba10=_0x2b848d;if(this[_0x47ba10(0x1f3)]())this[_0x47ba10(0x27c)]=Infinity;else{const _0x234a1c=this[_0x47ba10(0x14f)]()||new Game_Action(this);this[_0x47ba10(0x27c)]=VisuMZ['BattleSystemOTB'][_0x47ba10(0x18b)][_0x47ba10(0x26a)]['InitialSpeedJS'][_0x47ba10(0x28a)](_0x234a1c);}},Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1f3)]=function(){const _0x12d5d9=_0x2b848d;if(!Game_Battler[_0x12d5d9(0x105)])return![];if(!this[_0x12d5d9(0x1c4)]())return![];if(!this[_0x12d5d9(0x19c)]())return![];if(this[_0x12d5d9(0x285)]())return![];const _0x5598ed=JsonEx[_0x12d5d9(0x144)](this);return _0x5598ed['_tempActor']=!![],_0x5598ed[_0x12d5d9(0x23c)]=!![],_0x5598ed['updateStateTurns'](),_0x5598ed[_0x12d5d9(0x289)](0x1),_0x5598ed['removeStatesAuto'](0x2),_0x5598ed['refresh'](),_0x5598ed[_0x12d5d9(0x285)]();},VisuMZ[_0x2b848d(0x12c)]['Game_Action_allowRandomSpeed']=Game_Action[_0x2b848d(0x2b0)][_0x2b848d(0xf6)],Game_Action['prototype'][_0x2b848d(0xf6)]=function(){const _0x1b1b94=_0x2b848d;return BattleManager[_0x1b1b94(0x282)]()?VisuMZ[_0x1b1b94(0x12c)][_0x1b1b94(0x18b)][_0x1b1b94(0x26a)][_0x1b1b94(0x2c0)]:VisuMZ[_0x1b1b94(0x12c)][_0x1b1b94(0x29a)][_0x1b1b94(0x28a)](this);},Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x218)]=function(_0x975994){const _0x1abcb6=_0x2b848d;if(!this['canMove']())return;this[_0x1abcb6(0x1f1)]=this[_0x1abcb6(0x1f1)]||0x0,this[_0x1abcb6(0x1f1)]--,BattleManager[_0x1abcb6(0x184)](this,_0x975994,BattleManager['_actionBattlers']);},Game_Battler['prototype'][_0x2b848d(0x108)]=function(){const _0x33f1eb=_0x2b848d;if(this[_0x33f1eb(0xee)]()===Infinity)return![];return!![];},Game_Battler['prototype'][_0x2b848d(0x183)]=function(_0x360539,_0x3735fc){const _0x18a40b=_0x2b848d;if(this[_0x18a40b(0x23c)]||this[_0x18a40b(0x20c)])return;if(!SceneManager[_0x18a40b(0x231)]())return;if(!BattleManager[_0x18a40b(0x282)]())return;if(_0x360539&&!this[_0x18a40b(0x285)]())BattleManager[_0x18a40b(0x23a)]();else!_0x360539&&this[_0x18a40b(0x285)]()&&BattleManager[_0x18a40b(0x27d)](this);if(this[_0x18a40b(0x285)]()){const _0xf72756=this[_0x18a40b(0x1dc)]()-_0x3735fc;_0xf72756>0x0&&(BattleManager['otbAddBattlerToTurnOrderAtEnd'](this,_0xf72756,BattleManager[_0x18a40b(0x142)]),BattleManager[_0x18a40b(0xe5)](this,_0xf72756,BattleManager[_0x18a40b(0xf7)]));}},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0xed)]=Game_Battler['prototype'][_0x2b848d(0x1ac)],Game_Battler['prototype'][_0x2b848d(0x1ac)]=function(_0x4c922b){const _0x312cd2=_0x2b848d,_0x3941ab=this[_0x312cd2(0x285)](),_0xf8ad97=this['makeActionTimes']();VisuMZ[_0x312cd2(0x12c)][_0x312cd2(0xed)][_0x312cd2(0x28a)](this,_0x4c922b),this['otbProcessActionCheck'](_0x3941ab,_0xf8ad97);},VisuMZ[_0x2b848d(0x12c)]['Game_Battler_removeState']=Game_Battler['prototype']['removeState'],Game_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x24f)]=function(_0x36a2e6){const _0x28def8=_0x2b848d,_0x231d08=this[_0x28def8(0x285)](),_0x4dda6e=this['makeActionTimes']();VisuMZ[_0x28def8(0x12c)][_0x28def8(0x248)][_0x28def8(0x28a)](this,_0x36a2e6),this[_0x28def8(0x183)](_0x231d08,_0x4dda6e);},VisuMZ[_0x2b848d(0x12c)]['Game_Actor_selectNextCommand']=Game_Actor[_0x2b848d(0x2b0)]['selectNextCommand'],Game_Actor[_0x2b848d(0x2b0)][_0x2b848d(0x2c6)]=function(){const _0x176d75=_0x2b848d;if(BattleManager[_0x176d75(0x282)]()){if(this[_0x176d75(0x112)]())this[_0x176d75(0x112)]()[_0x176d75(0x1e6)]();return![];}return VisuMZ[_0x176d75(0x12c)][_0x176d75(0x2d4)][_0x176d75(0x28a)](this);},Game_Actor['prototype']['createTurnOrderOTBGraphicType']=function(){const _0x628a7b=_0x2b848d,_0x4695d4=this[_0x628a7b(0x1eb)]()[_0x628a7b(0x280)];if(_0x4695d4[_0x628a7b(0x23e)](/<OTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return _0x628a7b(0x17f);else{if(_0x4695d4[_0x628a7b(0x23e)](/<OTB TURN ORDER ICON:[ ](\d+)>/i))return _0x628a7b(0x26f);}return Window_OTB_TurnOrder['Settings'][_0x628a7b(0x173)];},Game_Actor[_0x2b848d(0x2b0)][_0x2b848d(0x269)]=function(){const _0x59f4d2=_0x2b848d,_0x4ee12c=this[_0x59f4d2(0x1eb)]()[_0x59f4d2(0x280)];if(_0x4ee12c['match'](/<OTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return String(RegExp['$1']);return this[_0x59f4d2(0x110)]();},Game_Actor[_0x2b848d(0x2b0)][_0x2b848d(0x16d)]=function(){const _0x265f24=_0x2b848d,_0x4e902a=this['actor']()[_0x265f24(0x280)];if(_0x4e902a['match'](/<OTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return Number(RegExp['$2']);return this[_0x265f24(0x1ed)]();},Game_Actor[_0x2b848d(0x2b0)]['createTurnOrderOTBGraphicIconIndex']=function(){const _0x365f08=_0x2b848d,_0x38d275=this[_0x365f08(0x1eb)]()[_0x365f08(0x280)];if(_0x38d275[_0x365f08(0x23e)](/<OTB TURN ORDER ICON:[ ](\d+)>/i))return Number(RegExp['$1']);return Window_OTB_TurnOrder[_0x365f08(0x18b)]['ActorBattlerIcon'];},Game_Enemy[_0x2b848d(0x2b0)][_0x2b848d(0x2c2)]=function(){const _0x14fa59=_0x2b848d,_0x276310=this[_0x14fa59(0x1c5)]()[_0x14fa59(0x280)];if(_0x276310[_0x14fa59(0x23e)](/<OTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return'face';else{if(_0x276310[_0x14fa59(0x23e)](/<OTB TURN ORDER ICON:[ ](\d+)>/i))return _0x14fa59(0x26f);}return Window_OTB_TurnOrder[_0x14fa59(0x18b)][_0x14fa59(0x1dd)];},Game_Enemy[_0x2b848d(0x2b0)][_0x2b848d(0xbe)]=function(){const _0x15857b=_0x2b848d,_0x50bab6=this['enemy']()[_0x15857b(0x280)];if(_0x50bab6['match'](/<OTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return String(RegExp['$1']);return Window_OTB_TurnOrder[_0x15857b(0x18b)][_0x15857b(0xf8)];},Game_Enemy[_0x2b848d(0x2b0)][_0x2b848d(0x197)]=function(){const _0x5d787f=_0x2b848d,_0x2d359a=this[_0x5d787f(0x1c5)]()[_0x5d787f(0x280)];if(_0x2d359a[_0x5d787f(0x23e)](/<OTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return Number(RegExp['$2']);return Window_OTB_TurnOrder[_0x5d787f(0x18b)][_0x5d787f(0x14d)];},Game_Enemy[_0x2b848d(0x2b0)][_0x2b848d(0xff)]=function(){const _0xdde76=_0x2b848d,_0x46992b=this[_0xdde76(0x1c5)]()[_0xdde76(0x280)];if(_0x46992b['match'](/<OTB TURN ORDER ICON:[ ](\d+)>/i))return Number(RegExp['$1']);return Window_OTB_TurnOrder[_0xdde76(0x18b)][_0xdde76(0x128)];},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x1e1)]=Game_Party[_0x2b848d(0x2b0)][_0x2b848d(0x205)],Game_Party[_0x2b848d(0x2b0)][_0x2b848d(0x205)]=function(_0x564bde){const _0x1b2bc1=_0x2b848d;VisuMZ[_0x1b2bc1(0x12c)][_0x1b2bc1(0x1e1)][_0x1b2bc1(0x28a)](this,_0x564bde);if(Imported[_0x1b2bc1(0x2a9)])return;SceneManager[_0x1b2bc1(0x231)]()&&BattleManager[_0x1b2bc1(0x282)]()&&(BattleManager[_0x1b2bc1(0x23a)](),BattleManager[_0x1b2bc1(0x27d)]($gameActors['actor'](_0x564bde)));},VisuMZ[_0x2b848d(0x12c)]['Game_Party_removeActor']=Game_Party['prototype'][_0x2b848d(0x120)],Game_Party[_0x2b848d(0x2b0)][_0x2b848d(0x120)]=function(_0x86dd92){const _0x3d3ca6=_0x2b848d;VisuMZ[_0x3d3ca6(0x12c)][_0x3d3ca6(0x20b)][_0x3d3ca6(0x28a)](this,_0x86dd92),SceneManager['isSceneBattle']()&&BattleManager[_0x3d3ca6(0x282)]()&&BattleManager[_0x3d3ca6(0x23a)]();},VisuMZ['BattleSystemOTB']['Scene_Battle_createActorCommandWindow']=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x2cc)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x2cc)]=function(){const _0x1145de=_0x2b848d;VisuMZ['BattleSystemOTB']['Scene_Battle_createActorCommandWindow'][_0x1145de(0x28a)](this),BattleManager[_0x1145de(0x282)]()&&this[_0x1145de(0x24a)]();},Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x24a)]=function(){const _0x7d9c4e=_0x2b848d,_0x58aa34=this[_0x7d9c4e(0x1db)];this[_0x7d9c4e(0x12d)]()&&delete _0x58aa34[_0x7d9c4e(0xdb)]['cancel'];},VisuMZ['BattleSystemOTB'][_0x2b848d(0x2b4)]=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x258)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x258)]=function(){const _0x36406d=_0x2b848d;BattleManager['isOTB']()?this[_0x36406d(0x2d7)]():VisuMZ[_0x36406d(0x12c)][_0x36406d(0x2b4)][_0x36406d(0x28a)](this);},Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x2d7)]=function(){const _0x45d930=_0x2b848d;BattleManager['otbPreviewOrderClear'](),this[_0x45d930(0x1a6)][_0x45d930(0x2d2)](),this['_actorCommandWindow'][_0x45d930(0x2ae)]();},VisuMZ[_0x2b848d(0x12c)]['Scene_Battle_commandFight']=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0xe9)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0xe9)]=function(){const _0x223abc=_0x2b848d;BattleManager[_0x223abc(0x282)]()?this[_0x223abc(0x2ad)]():VisuMZ[_0x223abc(0x12c)]['Scene_Battle_commandFight'][_0x223abc(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x126)]=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0xe2)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0xe2)]=function(){const _0x4a4338=_0x2b848d;VisuMZ['BattleSystemOTB'][_0x4a4338(0x126)][_0x4a4338(0x28a)](this),this[_0x4a4338(0xde)]();},Scene_Battle['prototype']['createOTBTurnOrderWindow']=function(){const _0x10783a=_0x2b848d;if(!BattleManager[_0x10783a(0x282)]())return;this['_otbTurnOrderWindow']=new Window_OTB_TurnOrder();const _0x54914e=this[_0x10783a(0x1de)](this['_windowLayer']);this[_0x10783a(0x1df)](this[_0x10783a(0x106)],_0x54914e),this[_0x10783a(0x186)](),SceneManager[_0x10783a(0x2a0)]()&&this[_0x10783a(0x106)]['resumeTurnOrderSprites']();},Scene_Battle[_0x2b848d(0x2b0)]['repositionLogWindowOTB']=function(){const _0x4fc34a=_0x2b848d,_0x20b683=Window_OTB_TurnOrder[_0x4fc34a(0x18b)];if(_0x20b683[_0x4fc34a(0x239)]!==_0x4fc34a(0x204))return;if(!_0x20b683['RepositionLogWindow'])return;if(!this[_0x4fc34a(0xbf)])return;const _0x12be41=this[_0x4fc34a(0x106)]['y']-Math[_0x4fc34a(0x2d9)]((Graphics['height']-Graphics[_0x4fc34a(0x2c9)])/0x2),_0x514984=_0x12be41+this['_otbTurnOrderWindow'][_0x4fc34a(0x245)];this[_0x4fc34a(0xbf)]['y']=_0x514984+(_0x20b683[_0x4fc34a(0x1ea)]||0x0);},VisuMZ[_0x2b848d(0x12c)]['Scene_Battle_commandAttack']=Scene_Battle['prototype'][_0x2b848d(0x26d)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x26d)]=function(){const _0x3a9067=_0x2b848d;BattleManager['otbPreviewOrderClear'](),VisuMZ[_0x3a9067(0x12c)]['Scene_Battle_commandAttack'][_0x3a9067(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x152)]=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x1c0)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x1c0)]=function(){const _0x462339=_0x2b848d;BattleManager[_0x462339(0xec)](),VisuMZ[_0x462339(0x12c)][_0x462339(0x152)][_0x462339(0x28a)](this);},VisuMZ['BattleSystemOTB'][_0x2b848d(0x12b)]=Scene_Battle[_0x2b848d(0x2b0)]['onActorOk'],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x2b1)]=function(){const _0x1aad2f=_0x2b848d;BattleManager['otbPreviewOrderClear'](),VisuMZ[_0x1aad2f(0x12c)][_0x1aad2f(0x12b)][_0x1aad2f(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x1cb)]=Scene_Battle[_0x2b848d(0x2b0)]['onActorCancel'],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x124)]=function(){const _0xeb5662=_0x2b848d;BattleManager['otbPreviewOrderClear'](),VisuMZ[_0xeb5662(0x12c)]['Scene_Battle_onActorCancel'][_0xeb5662(0x28a)](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x1f2)]=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0xe3)],Scene_Battle['prototype']['onEnemyOk']=function(){const _0x431aef=_0x2b848d;BattleManager[_0x431aef(0xec)](),VisuMZ[_0x431aef(0x12c)][_0x431aef(0x1f2)]['call'](this);},VisuMZ[_0x2b848d(0x12c)]['Scene_Battle_onEnemyCancel']=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x1c8)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x1c8)]=function(){const _0x502ac3=_0x2b848d;BattleManager[_0x502ac3(0xec)](),VisuMZ[_0x502ac3(0x12c)]['Scene_Battle_onEnemyCancel']['call'](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x21f)]=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x170)],Scene_Battle[_0x2b848d(0x2b0)]['onSkillOk']=function(){const _0x4aa96b=_0x2b848d;BattleManager[_0x4aa96b(0xec)](),VisuMZ[_0x4aa96b(0x12c)][_0x4aa96b(0x21f)][_0x4aa96b(0x28a)](this);},VisuMZ['BattleSystemOTB'][_0x2b848d(0xc8)]=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x11b)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x11b)]=function(){const _0x5b8789=_0x2b848d;BattleManager[_0x5b8789(0xec)](),VisuMZ[_0x5b8789(0x12c)][_0x5b8789(0xc8)]['call'](this);},VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x187)]=Scene_Battle['prototype'][_0x2b848d(0x242)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x242)]=function(){const _0x5a7f71=_0x2b848d;BattleManager['otbPreviewOrderClear'](),VisuMZ['BattleSystemOTB'][_0x5a7f71(0x187)][_0x5a7f71(0x28a)](this);},VisuMZ['BattleSystemOTB']['Scene_Battle_onItemCancel']=Scene_Battle[_0x2b848d(0x2b0)]['onItemCancel'],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x2bc)]=function(){const _0x1c059b=_0x2b848d;BattleManager[_0x1c059b(0xec)](),VisuMZ[_0x1c059b(0x12c)][_0x1c059b(0x16f)]['call'](this);},VisuMZ[_0x2b848d(0x12c)]['Scene_Battle_actorCommandSingleSkill']=Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x193)],Scene_Battle[_0x2b848d(0x2b0)][_0x2b848d(0x193)]=function(){const _0x332fff=_0x2b848d;BattleManager[_0x332fff(0xec)](),VisuMZ[_0x332fff(0x12c)][_0x332fff(0x157)][_0x332fff(0x28a)](this);};function Sprite_OTB_TurnOrder_Battler(){this['initialize'](...arguments);}Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)]=Object[_0x2b848d(0x18a)](Sprite_Clickable['prototype']),Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0xfa)]=Sprite_OTB_TurnOrder_Battler,Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)]['initialize']=function(_0xf66843,_0x4f555b,_0xfd04aa){const _0x25c14b=_0x2b848d;this['initMembers'](_0xf66843,_0x4f555b,_0xfd04aa),Sprite_Clickable[_0x25c14b(0x2b0)]['initialize'][_0x25c14b(0x28a)](this),this[_0x25c14b(0x22b)]=0x0,this[_0x25c14b(0x20f)](),this[_0x25c14b(0x190)]();},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0xcc)]=function(_0x196c6d,_0x309392,_0x13d514){const _0x71484=_0x2b848d;this[_0x71484(0x213)]=_0x196c6d['isActor']()?$gameParty:$gameTroop,this[_0x71484(0x251)]=_0x196c6d[_0x71484(0x19b)](),this['_instance']=_0x309392,this[_0x71484(0x1b2)]=_0x13d514;const _0x59c187=Window_OTB_TurnOrder['Settings'],_0x338813=this[_0x71484(0x2c7)]();this[_0x71484(0x20e)]=0x0,this['_positionTargetX']=_0x59c187[_0x71484(0xef)]?-_0x59c187['SpriteThin']:this['containerWindow']()[_0x71484(0x24d)],this[_0x71484(0x17b)]=0x0,this[_0x71484(0x2da)]=0x0,this[_0x71484(0x104)]=0xff,this[_0x71484(0x21b)]=![],this['_isAppeared']=![],this[_0x71484(0x1fc)]=0x0,this[_0x71484(0x265)]=0x0;},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x20f)]=function(){const _0x4d2c3c=_0x2b848d;this[_0x4d2c3c(0xc2)](),this['createBackgroundSprite'](),this[_0x4d2c3c(0xd3)](),this[_0x4d2c3c(0x1fe)](),this[_0x4d2c3c(0x2cd)]();},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)]['createInitialPositions']=function(){const _0x1cfea4=_0x2b848d;this['x']=this[_0x1cfea4(0x1c1)],this['y']=this[_0x1cfea4(0x17b)];},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x2c7)]=function(){return!![];},Sprite_OTB_TurnOrder_Battler['prototype']['bitmapWidth']=function(){const _0x1fe44d=_0x2b848d,_0x1f385a=Window_OTB_TurnOrder[_0x1fe44d(0x18b)];return _0x1f385a[_0x1fe44d(0x295)];},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x158)]=function(){const _0x4dfa73=_0x2b848d,_0x45ec86=Window_OTB_TurnOrder['Settings'];return _0x45ec86[_0x4dfa73(0x198)];},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x25a)]=function(){const _0x509710=_0x2b848d;return this[_0x509710(0x213)]===$gameParty?_0x509710(0x141):_0x509710(0x188);},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x146)]=function(){const _0x3761f9=_0x2b848d;if(!Window_OTB_TurnOrder[_0x3761f9(0x18b)][_0x3761f9(0x10c)])return;const _0x21dfa1=Window_OTB_TurnOrder[_0x3761f9(0x18b)],_0x248a23=this[_0x3761f9(0x25a)](),_0x19c41b=_0x3761f9(0x235)[_0x3761f9(0x2b9)](_0x248a23),_0x42ab31=new Sprite();_0x42ab31[_0x3761f9(0x1cc)]['x']=this['anchor']['x'],_0x42ab31[_0x3761f9(0x1cc)]['y']=this[_0x3761f9(0x1cc)]['y'];if(_0x21dfa1[_0x19c41b])_0x42ab31[_0x3761f9(0x28c)]=ImageManager[_0x3761f9(0x1ef)](_0x21dfa1[_0x19c41b]);else{const _0x22a57=this[_0x3761f9(0x212)](),_0x11af86=this[_0x3761f9(0x158)]();_0x42ab31[_0x3761f9(0x28c)]=new Bitmap(_0x22a57,_0x11af86);const _0x1a0295=ColorManager['getColor'](_0x21dfa1['%1BgColor1'[_0x3761f9(0x2b9)](_0x248a23)]),_0x2e97f0=ColorManager[_0x3761f9(0x16e)](_0x21dfa1[_0x3761f9(0x15b)[_0x3761f9(0x2b9)](_0x248a23)]);_0x42ab31['bitmap']['gradientFillRect'](0x0,0x0,_0x22a57,_0x11af86,_0x1a0295,_0x2e97f0,!![]);}this['_backgroundSprite']=_0x42ab31,this[_0x3761f9(0x111)](this[_0x3761f9(0x24c)]),this[_0x3761f9(0x24d)]=this[_0x3761f9(0x24c)][_0x3761f9(0x24d)],this[_0x3761f9(0x245)]=this[_0x3761f9(0x24c)][_0x3761f9(0x245)];},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0xd3)]=function(){const _0x24d514=_0x2b848d,_0x1f3625=new Sprite();_0x1f3625[_0x24d514(0x1cc)]['x']=this[_0x24d514(0x1cc)]['x'],_0x1f3625[_0x24d514(0x1cc)]['y']=this['anchor']['y'],this[_0x24d514(0x179)]=_0x1f3625,this[_0x24d514(0x111)](this[_0x24d514(0x179)]),this[_0x24d514(0x233)]();},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1fe)]=function(){const _0x20d7a5=_0x2b848d;if(!Window_OTB_TurnOrder[_0x20d7a5(0x18b)]['ShowMarkerBorder'])return;const _0xd47cc9=Window_OTB_TurnOrder[_0x20d7a5(0x18b)],_0xc06912=this[_0x20d7a5(0x25a)](),_0x5260da='%1SystemBorder'[_0x20d7a5(0x2b9)](_0xc06912),_0xad8016=new Sprite();_0xad8016['anchor']['x']=this[_0x20d7a5(0x1cc)]['x'],_0xad8016[_0x20d7a5(0x1cc)]['y']=this[_0x20d7a5(0x1cc)]['y'];if(_0xd47cc9[_0x5260da])_0xad8016['bitmap']=ImageManager[_0x20d7a5(0x1ef)](_0xd47cc9[_0x5260da]);else{let _0x27236d=this['bitmapWidth'](),_0x53a094=this[_0x20d7a5(0x158)](),_0x513042=this[_0x20d7a5(0x19a)]();_0xad8016[_0x20d7a5(0x28c)]=new Bitmap(_0x27236d,_0x53a094);const _0x5e2e6b='#000000',_0x50a728=ColorManager['getColor'](_0xd47cc9[_0x20d7a5(0x2aa)['format'](_0xc06912)]);_0xad8016[_0x20d7a5(0x28c)][_0x20d7a5(0x116)](0x0,0x0,_0x27236d,_0x53a094,_0x5e2e6b),_0x27236d-=0x2,_0x53a094-=0x2,_0xad8016[_0x20d7a5(0x28c)][_0x20d7a5(0x116)](0x1,0x1,_0x27236d,_0x53a094,_0x50a728),_0x27236d-=_0x513042*0x2,_0x53a094-=_0x513042*0x2,_0xad8016[_0x20d7a5(0x28c)][_0x20d7a5(0x116)](0x1+_0x513042,0x1+_0x513042,_0x27236d,_0x53a094,_0x5e2e6b),_0x27236d-=0x2,_0x53a094-=0x2,_0x513042+=0x1,_0xad8016[_0x20d7a5(0x28c)][_0x20d7a5(0x28d)](0x1+_0x513042,0x1+_0x513042,_0x27236d,_0x53a094);}this[_0x20d7a5(0x24c)]=_0xad8016,this[_0x20d7a5(0x111)](this['_backgroundSprite']);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x19a)]=function(){const _0x525068=_0x2b848d,_0x434ff7=Window_OTB_TurnOrder[_0x525068(0x18b)];return _0x434ff7[_0x525068(0x171)];},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)]['createLetterSprite']=function(){const _0x2066b2=_0x2b848d,_0x30ee4f=Window_OTB_TurnOrder['Settings'];if(!_0x30ee4f[_0x2066b2(0x22e)])return;if(this[_0x2066b2(0x213)]===$gameParty)return;const _0x7f85ec=this[_0x2066b2(0x212)](),_0x5d71d3=this[_0x2066b2(0x158)](),_0x52e273=new Sprite();_0x52e273[_0x2066b2(0x1cc)]['x']=this[_0x2066b2(0x1cc)]['x'],_0x52e273['anchor']['y']=this[_0x2066b2(0x1cc)]['y'],_0x52e273[_0x2066b2(0x28c)]=new Bitmap(_0x7f85ec,_0x5d71d3),this[_0x2066b2(0xea)]=_0x52e273,this[_0x2066b2(0x111)](this['_letterSprite']);},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x112)]=function(){const _0x29c970=_0x2b848d;return this[_0x29c970(0x213)]?this[_0x29c970(0x213)][_0x29c970(0x22f)]()[this[_0x29c970(0x251)]]:null;},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1af)]=function(){const _0x9b06a7=_0x2b848d;Sprite_Clickable[_0x9b06a7(0x2b0)]['update']['call'](this),this[_0x9b06a7(0x1d9)](),this['checkOpacity'](),this[_0x9b06a7(0x216)](),this['updateGraphic'](),this[_0x9b06a7(0x224)](),this[_0x9b06a7(0x291)](),this[_0x9b06a7(0x1ab)]();},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1da)]=function(_0x5d4442,_0x3d9207){const _0xcfa5c3=_0x2b848d,_0x2bc836=Window_OTB_TurnOrder[_0xcfa5c3(0x18b)];this[_0xcfa5c3(0x20e)]=_0x2bc836[_0xcfa5c3(0x17c)],this[_0xcfa5c3(0x1c1)]=_0x5d4442,this[_0xcfa5c3(0x17b)]=_0x3d9207;},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1d9)]=function(){const _0x4abc4a=_0x2b848d;if(this[_0x4abc4a(0x20e)]>0x0){const _0x10010a=this[_0x4abc4a(0x20e)];this['x']=(this['x']*(_0x10010a-0x1)+this[_0x4abc4a(0x1c1)])/_0x10010a,this['y']=(this['y']*(_0x10010a-0x1)+this['_positionTargetY'])/_0x10010a,this[_0x4abc4a(0x20e)]--;}if(this[_0x4abc4a(0x20e)]<=0x0){this['x']=this['_positionTargetX'],this['y']=this['_positionTargetY'];if(this[_0x4abc4a(0x22b)]<0xff&&!this['_isBattleOver']&&this[_0x4abc4a(0x2da)]<=0x0){const _0x1d32bb=this[_0x4abc4a(0x112)]();_0x1d32bb&&(this[_0x4abc4a(0x104)]=_0x1d32bb[_0x4abc4a(0x1c4)]()&&_0x1d32bb['isAppeared']()?0xff:0x0);}}},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x137)]=function(){return 0x1;},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x270)]=function(){const _0x2418f6=_0x2b848d;return SceneManager[_0x2418f6(0x109)][_0x2418f6(0x106)];},Sprite_OTB_TurnOrder_Battler['prototype']['containerPosition']=function(){const _0x193842=_0x2b848d,_0x49a329=this['battler']();if(!_0x49a329)return this[_0x193842(0x137)]();if(_0x49a329===BattleManager[_0x193842(0xfe)])return 0x0;if(BattleManager[_0x193842(0x142)]['includes'](_0x49a329)){const _0x451d8a=BattleManager[_0x193842(0x142)]['indexOf'](_0x49a329)+0x1;return _0x451d8a;}return this[_0x193842(0x137)]();},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x114)]=function(_0x32f7d5){const _0x229959=_0x2b848d,_0x403e5d=Window_OTB_TurnOrder[_0x229959(0x18b)];this['_fadeDuration']=_0x403e5d[_0x229959(0x17c)],this[_0x229959(0x104)]=_0x32f7d5;},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x190)]=function(){const _0x1bb29f=_0x2b848d,_0x32c50f=this[_0x1bb29f(0x112)]();if(!_0x32c50f)return;if(this[_0x1bb29f(0x21b)]===_0x32c50f[_0x1bb29f(0x1c4)]()&&this[_0x1bb29f(0x27a)]===_0x32c50f[_0x1bb29f(0x19c)]())return;this[_0x1bb29f(0x21b)]=_0x32c50f['isAlive'](),this[_0x1bb29f(0x27a)]=_0x32c50f['isAppeared']();let _0x17e9b4=this[_0x1bb29f(0x21b)]&&this[_0x1bb29f(0x27a)]?0xff:0x0;this['startFade'](_0x17e9b4);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)]['updateOpacity']=function(){const _0x3140ae=_0x2b848d;if(this[_0x3140ae(0x2da)]>0x0){const _0x3e9d06=this[_0x3140ae(0x2da)];this[_0x3140ae(0x22b)]=(this[_0x3140ae(0x22b)]*(_0x3e9d06-0x1)+this[_0x3140ae(0x104)])/_0x3e9d06,this['_fadeDuration']--,this['_fadeDuration']<=0x0&&(this['opacity']=this[_0x3140ae(0x104)]);}if(this['_isBattleOver'])return;BattleManager[_0x3140ae(0x281)]===_0x3140ae(0x1b6)&&(this[_0x3140ae(0x1f4)]=!![],this['startFade'](0x0));},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x246)]=function(){const _0x502714=_0x2b848d,_0x5d1e5b=this[_0x502714(0x112)]();if(!_0x5d1e5b)return;const _0x215cde=Window_OTB_TurnOrder['Settings'],_0x253a8c=this['_unit']===$gameParty?_0x502714(0x141):'Enemy';let _0x197262=_0x5d1e5b[_0x502714(0xd2)]();if(_0x5d1e5b['isActor']()&&_0x197262===_0x502714(0x1c5))_0x197262='face';else _0x5d1e5b['isEnemy']()&&_0x197262===_0x502714(0x2b5)&&(_0x197262=_0x502714(0x1c5));if(this[_0x502714(0xf3)]!==_0x197262)return this['processUpdateGraphic']();switch(this[_0x502714(0xf3)]){case _0x502714(0x17f):if(this[_0x502714(0x113)]!==_0x5d1e5b[_0x502714(0x269)]())return this[_0x502714(0x233)]();if(this[_0x502714(0x15e)]!==_0x5d1e5b[_0x502714(0x16d)]())return this['processUpdateGraphic']();break;case _0x502714(0x26f):if(this[_0x502714(0x21d)]!==_0x5d1e5b[_0x502714(0x19e)]())return this['processUpdateGraphic']();break;case _0x502714(0x1c5):if(_0x5d1e5b[_0x502714(0xdf)]()){if(this[_0x502714(0x257)]!==_0x5d1e5b[_0x502714(0xfd)]())return this[_0x502714(0x233)]();}else{if(this[_0x502714(0x202)]!==_0x5d1e5b['battlerName']())return this['processUpdateGraphic']();}break;case _0x502714(0x2b5):if(_0x5d1e5b[_0x502714(0xd1)]()){if(this['_graphicSv']!==_0x5d1e5b[_0x502714(0x22a)]())return this[_0x502714(0x233)]();}else{if(this[_0x502714(0x202)]!==_0x5d1e5b[_0x502714(0x22a)]())return this['processUpdateGraphic']();}break;}},Sprite_OTB_TurnOrder_Battler['prototype']['processUpdateGraphic']=function(){const _0x30b16b=_0x2b848d,_0x525b4b=this[_0x30b16b(0x112)]();if(!_0x525b4b)return;this['_graphicType']=_0x525b4b[_0x30b16b(0xd2)]();if(_0x525b4b[_0x30b16b(0xd1)]()&&this[_0x30b16b(0xf3)]==='enemy')this[_0x30b16b(0xf3)]=_0x30b16b(0x17f);else _0x525b4b['isEnemy']()&&this[_0x30b16b(0xf3)]===_0x30b16b(0x2b5)&&(this[_0x30b16b(0xf3)]=_0x30b16b(0x1c5));let _0x293002;switch(this[_0x30b16b(0xf3)]){case _0x30b16b(0x17f):this[_0x30b16b(0x113)]=_0x525b4b[_0x30b16b(0x269)](),this[_0x30b16b(0x15e)]=_0x525b4b[_0x30b16b(0x16d)](),_0x293002=ImageManager['loadFace'](this[_0x30b16b(0x113)]),_0x293002[_0x30b16b(0xe1)](this[_0x30b16b(0x163)][_0x30b16b(0x221)](this,_0x293002));break;case _0x30b16b(0x26f):this[_0x30b16b(0x21d)]=_0x525b4b['createTurnOrderOTBGraphicIconIndex'](),_0x293002=ImageManager[_0x30b16b(0x1ef)](_0x30b16b(0x214)),_0x293002[_0x30b16b(0xe1)](this['changeIconGraphicBitmap'][_0x30b16b(0x221)](this,_0x293002));break;case _0x30b16b(0x1c5):if(_0x525b4b[_0x30b16b(0xdf)]())this[_0x30b16b(0x257)]=_0x525b4b['svBattlerName'](),_0x293002=ImageManager[_0x30b16b(0x176)](this[_0x30b16b(0x257)]),_0x293002[_0x30b16b(0xe1)](this[_0x30b16b(0x10f)][_0x30b16b(0x221)](this,_0x293002));else $gameSystem['isSideView']()?(this[_0x30b16b(0x202)]=_0x525b4b['battlerName'](),_0x293002=ImageManager[_0x30b16b(0x195)](this[_0x30b16b(0x202)]),_0x293002[_0x30b16b(0xe1)](this[_0x30b16b(0x2a3)][_0x30b16b(0x221)](this,_0x293002))):(this[_0x30b16b(0x202)]=_0x525b4b[_0x30b16b(0x22a)](),_0x293002=ImageManager[_0x30b16b(0x1c2)](this[_0x30b16b(0x202)]),_0x293002['addLoadListener'](this['changeEnemyGraphicBitmap'][_0x30b16b(0x221)](this,_0x293002)));break;case _0x30b16b(0x2b5):this[_0x30b16b(0x257)]=_0x525b4b[_0x30b16b(0x22a)](),_0x293002=ImageManager[_0x30b16b(0x176)](this[_0x30b16b(0x257)]),_0x293002['addLoadListener'](this['changeSvActorGraphicBitmap']['bind'](this,_0x293002));break;}},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x163)]=function(_0x196b73){const _0x13d6fe=_0x2b848d,_0x7b4cb3=this[_0x13d6fe(0x15e)],_0x228f1c=this[_0x13d6fe(0x212)](),_0x5aced2=this[_0x13d6fe(0x158)](),_0x5858b9=Math[_0x13d6fe(0x168)](_0x228f1c,_0x5aced2);this[_0x13d6fe(0x179)]['bitmap']=new Bitmap(_0x228f1c,_0x5aced2);const _0x4941f3=this[_0x13d6fe(0x179)]['bitmap'],_0x328369=ImageManager[_0x13d6fe(0x211)],_0x550f8a=ImageManager[_0x13d6fe(0x1f5)],_0x5a9bf6=_0x5858b9/Math[_0x13d6fe(0x168)](_0x328369,_0x550f8a),_0x1eab3c=ImageManager['faceWidth'],_0x5222d5=ImageManager['faceHeight'],_0x26ae3b=_0x7b4cb3%0x4*_0x328369+(_0x328369-_0x1eab3c)/0x2,_0xa78140=Math[_0x13d6fe(0x2a1)](_0x7b4cb3/0x4)*_0x550f8a+(_0x550f8a-_0x5222d5)/0x2,_0x1e1b6b=(_0x228f1c-_0x328369*_0x5a9bf6)/0x2,_0x2e3990=(_0x5aced2-_0x550f8a*_0x5a9bf6)/0x2;_0x4941f3['blt'](_0x196b73,_0x26ae3b,_0xa78140,_0x1eab3c,_0x5222d5,_0x1e1b6b,_0x2e3990,_0x5858b9,_0x5858b9);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x284)]=function(_0x144f4a){const _0x480baf=_0x2b848d,_0x211b85=this[_0x480baf(0x21d)],_0xbc0e40=this['bitmapWidth'](),_0x4c86bf=this['bitmapHeight']();this[_0x480baf(0x179)][_0x480baf(0x28c)]=new Bitmap(_0xbc0e40,_0x4c86bf);const _0x8056b7=this[_0x480baf(0x179)][_0x480baf(0x28c)],_0x625db2=ImageManager[_0x480baf(0x1be)],_0x1d8195=ImageManager[_0x480baf(0x1b8)],_0xf8bad9=Math[_0x480baf(0x1e8)](_0x625db2,_0x1d8195,_0xbc0e40,_0x4c86bf),_0x38dcc2=_0x211b85%0x10*_0x625db2,_0x494ff8=Math[_0x480baf(0x2a1)](_0x211b85/0x10)*_0x1d8195,_0x4ef16d=Math[_0x480baf(0x2a1)](Math[_0x480baf(0x168)](_0xbc0e40-_0xf8bad9,0x0)/0x2),_0x1c1582=Math[_0x480baf(0x2a1)](Math['max'](_0x4c86bf-_0xf8bad9,0x0)/0x2);_0x8056b7['blt'](_0x144f4a,_0x38dcc2,_0x494ff8,_0x625db2,_0x1d8195,_0x4ef16d,_0x1c1582,_0xf8bad9,_0xf8bad9);},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x10f)]=function(_0x242234){const _0x5cc4a2=_0x2b848d,_0x1dabb6=this[_0x5cc4a2(0x212)](),_0x388e1f=this['bitmapHeight'](),_0x5de5d2=Math[_0x5cc4a2(0x1e8)](_0x1dabb6,_0x388e1f);this[_0x5cc4a2(0x179)]['bitmap']=new Bitmap(_0x1dabb6,_0x388e1f);const _0x2a880d=this['_graphicSprite'][_0x5cc4a2(0x28c)],_0x31bb5f=0x9,_0x205f21=0x6,_0x484dad=_0x242234[_0x5cc4a2(0x24d)]/_0x31bb5f,_0x4da19d=_0x242234['height']/_0x205f21,_0x4073bb=Math[_0x5cc4a2(0x1e8)](0x1,_0x5de5d2/_0x484dad,_0x5de5d2/_0x4da19d),_0x584379=_0x484dad*_0x4073bb,_0x4dfdeb=_0x4da19d*_0x4073bb,_0x38ff9e=Math[_0x5cc4a2(0x2d9)]((_0x1dabb6-_0x584379)/0x2),_0x1e08f7=Math[_0x5cc4a2(0x2d9)]((_0x388e1f-_0x4dfdeb)/0x2);_0x2a880d[_0x5cc4a2(0x160)](_0x242234,0x0,0x0,_0x484dad,_0x4da19d,_0x38ff9e,_0x1e08f7,_0x584379,_0x4dfdeb);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x2a3)]=function(_0x893959){const _0x4548e6=_0x2b848d,_0x4928d0=Window_OTB_TurnOrder[_0x4548e6(0x18b)],_0x3c7393=this['bitmapWidth'](),_0x5a94c4=this[_0x4548e6(0x158)](),_0x4eaf48=Math[_0x4548e6(0x1e8)](_0x3c7393,_0x5a94c4);this['_graphicSprite']['bitmap']=new Bitmap(_0x3c7393,_0x5a94c4);const _0x1f0041=this[_0x4548e6(0x179)][_0x4548e6(0x28c)],_0x11574a=Math[_0x4548e6(0x1e8)](0x1,_0x4eaf48/_0x893959[_0x4548e6(0x24d)],_0x4eaf48/_0x893959[_0x4548e6(0x245)]),_0x37198b=_0x893959[_0x4548e6(0x24d)]*_0x11574a,_0x19b575=_0x893959[_0x4548e6(0x245)]*_0x11574a,_0x338142=Math[_0x4548e6(0x2d9)]((_0x3c7393-_0x37198b)/0x2),_0x2e39c1=Math[_0x4548e6(0x2d9)]((_0x5a94c4-_0x19b575)/0x2);_0x1f0041['blt'](_0x893959,0x0,0x0,_0x893959[_0x4548e6(0x24d)],_0x893959[_0x4548e6(0x245)],_0x338142,_0x2e39c1,_0x37198b,_0x19b575);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)]['updateGraphicHue']=function(){const _0x290143=_0x2b848d,_0x2862ce=this[_0x290143(0x112)]();if(!_0x2862ce)return;if(!_0x2862ce[_0x290143(0xe0)]())return;if(this[_0x290143(0x28e)]===_0x2862ce['battlerHue']())return;this[_0x290143(0x28e)]=_0x2862ce[_0x290143(0x1c7)]();if(_0x2862ce[_0x290143(0xdf)]())this[_0x290143(0x28e)]=0x0;this[_0x290143(0x179)][_0x290143(0x20d)](this[_0x290143(0x28e)]);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x291)]=function(){const _0x38c967=_0x2b848d;if(!this[_0x38c967(0xea)])return;const _0x1ee8c0=this[_0x38c967(0x112)]();if(!_0x1ee8c0)return;if(this[_0x38c967(0xcf)]===_0x1ee8c0[_0x38c967(0xcf)]&&this[_0x38c967(0x1ca)]===_0x1ee8c0[_0x38c967(0x1ca)])return;this[_0x38c967(0xcf)]=_0x1ee8c0['_letter'],this[_0x38c967(0x1ca)]=_0x1ee8c0[_0x38c967(0x1ca)];const _0x447a2c=Window_OTB_TurnOrder['Settings'],_0x2f4e23=this[_0x38c967(0x212)](),_0x7c2b44=this['bitmapHeight'](),_0x69763f=this[_0x38c967(0xea)][_0x38c967(0x28c)];_0x69763f[_0x38c967(0x21a)]();if(!this[_0x38c967(0x1ca)])return;_0x69763f[_0x38c967(0x17e)]=_0x447a2c[_0x38c967(0x156)]||$gameSystem[_0x38c967(0x288)](),_0x69763f[_0x38c967(0x136)]=_0x447a2c[_0x38c967(0x169)]||0x10,_0x447a2c[_0x38c967(0xef)]?_0x69763f[_0x38c967(0x244)](this[_0x38c967(0xcf)]['trim'](),_0x2f4e23*0x1/0x8,_0x7c2b44/0x2,_0x2f4e23,_0x7c2b44/0x2,_0x38c967(0x21c)):_0x69763f[_0x38c967(0x244)](this[_0x38c967(0xcf)]['trim'](),0x0,_0x7c2b44/0x2,_0x2f4e23*0x7/0x8,_0x7c2b44/0x2,_0x38c967(0x192));},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x1ab)]=function(){const _0x110269=_0x2b848d,_0x38f4e2=this[_0x110269(0x112)]();if(!_0x38f4e2)return;const _0x517fbe=_0x38f4e2['battler']();if(!_0x517fbe)return;const _0x36f794=_0x517fbe['mainSprite']();if(!_0x36f794)return;this[_0x110269(0x215)](_0x36f794[_0x110269(0x2c3)]);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x220)]=function(){return null;},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x1d3)]=function(_0x466e78){const _0x126142=_0x2b848d;this[_0x126142(0x1b2)]=_0x466e78,this[_0x126142(0x153)](),this[_0x126142(0x1b2)]===null&&(this[_0x126142(0x2c8)]=-0x1);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x153)]=function(){const _0x4aa676=_0x2b848d,_0x11b720=this[_0x4aa676(0x270)]();if(!_0x11b720)return;const _0x3cd79e=Window_OTB_TurnOrder[_0x4aa676(0x18b)],_0x38dc96=_0x3cd79e[_0x4aa676(0xef)],_0x421f12=this[_0x4aa676(0x1b2)]===_0x11b720[_0x4aa676(0x10b)]?!![]:![],_0x3586aa=this[_0x4aa676(0x2c8)]===-0x1&&BattleManager[_0x4aa676(0xfe)]===this[_0x4aa676(0x112)](),_0x1bc618=_0x11b720['_spriteGroupWidth']-_0x3cd79e[_0x4aa676(0x295)];let _0x5af951=Math[_0x4aa676(0x1d1)](_0x1bc618/(this[_0x4aa676(0x1b2)][_0x4aa676(0x2d0)]-0x1||0x1));_0x5af951=Math[_0x4aa676(0x1e8)](_0x3cd79e[_0x4aa676(0x295)],_0x5af951);let _0x2fb2b1=0x0,_0xb9667c=0x0,_0x599b4d=_0x3586aa?-0x1:this[_0x4aa676(0x1b2)][_0x4aa676(0x271)](this);!_0x3586aa&&(_0x599b4d=this[_0x4aa676(0x1b9)]());if(_0x3586aa)_0x2fb2b1=_0x11b720[_0x4aa676(0x1aa)];else _0x38dc96?(_0x2fb2b1=(_0x421f12?_0x11b720[_0x4aa676(0xce)]:_0x11b720[_0x4aa676(0x196)])+_0x1bc618,_0x2fb2b1-=_0x599b4d*_0x5af951):(_0x2fb2b1=_0x421f12?_0x11b720[_0x4aa676(0xce)]:_0x11b720[_0x4aa676(0x196)],_0x2fb2b1+=_0x599b4d*_0x5af951);_0x2fb2b1+=this['additionalTargetXAdjustments'](_0x599b4d,_0x3cd79e[_0x4aa676(0x295)]-_0x5af951),!_0x3586aa&&_0x599b4d<0x0&&(_0x2fb2b1=this['x'],_0xb9667c=this['y'],this[_0x4aa676(0x114)](0x0)),this[_0x4aa676(0x1da)](_0x2fb2b1,_0xb9667c);},Sprite_OTB_TurnOrder_Battler[_0x2b848d(0x2b0)][_0x2b848d(0x21e)]=function(_0x37997f,_0x10cbe7){return 0x0;},Sprite_OTB_TurnOrder_Battler['prototype'][_0x2b848d(0x1b9)]=function(){const _0x5820bb=_0x2b848d,_0xb33ac5=this[_0x5820bb(0x270)]();if(!_0xb33ac5)return 0x0;const _0x9f4dde=this[_0x5820bb(0x1b2)]===_0xb33ac5[_0x5820bb(0x10b)]?!![]:![],_0x32ba22=_0x9f4dde?BattleManager[_0x5820bb(0xf7)]:BattleManager[_0x5820bb(0x142)],_0xf0b180=this[_0x5820bb(0x112)](),_0x4155f9=VisuMZ[_0x5820bb(0x12c)]['GetAllIndicies'](_0xf0b180,_0x32ba22);return _0x4155f9[this[_0x5820bb(0x2c8)]]??_0x4155f9[_0x4155f9[_0x5820bb(0x2d0)]-0x1]??-0x1;};function Sprite_OTB_TurnOrder_Preview(){this['initialize'](...arguments);}Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)]=Object['create'](Sprite_OTB_TurnOrder_Battler['prototype']),Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0xfa)]=Sprite_OTB_TurnOrder_Preview,Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0x2bd)]=function(_0x542a1d,_0xc19d82,_0x432bd2,_0x4543c5){const _0x2e3320=_0x2b848d;this[_0x2e3320(0x2d1)]=_0x4543c5,Sprite_OTB_TurnOrder_Battler['prototype'][_0x2e3320(0x2bd)][_0x2e3320(0x28a)](this,_0x542a1d,_0xc19d82,_0x432bd2),this[_0x2e3320(0x262)]();},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0x262)]=function(){const _0x2ffbc4=_0x2b848d,_0x59f596=Window_OTB_TurnOrder['Settings'];this[_0x2ffbc4(0x237)]['x']=this[_0x2ffbc4(0x237)]['y']=_0x59f596[_0x2ffbc4(0x14b)];},Sprite_OTB_TurnOrder_Preview['prototype'][_0x2b848d(0x25a)]=function(){const _0x49fa5f=_0x2b848d;return this[_0x49fa5f(0x213)]===$gameParty?_0x49fa5f(0x294):_0x49fa5f(0x207);},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0x19a)]=function(){const _0x14a214=_0x2b848d,_0x1e97f2=Window_OTB_TurnOrder[_0x14a214(0x18b)];return Math[_0x14a214(0x1d1)](_0x1e97f2[_0x14a214(0x171)]/(_0x1e97f2[_0x14a214(0x14b)]||0.01));},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0x1da)]=function(_0x502081,_0x3d3af0){const _0x392e6d=_0x2b848d;Sprite_OTB_TurnOrder_Battler['prototype'][_0x392e6d(0x1da)]['call'](this,_0x502081,_0x3d3af0),this['x']=this[_0x392e6d(0x1c1)],this['y']=this[_0x392e6d(0x17b)];},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)]['startFade']=function(_0xa42d7){const _0x4fd198=_0x2b848d;Sprite_OTB_TurnOrder_Battler[_0x4fd198(0x2b0)]['startFade'][_0x4fd198(0x28a)](this,_0xa42d7),_0xa42d7>0x0?this['_fadeDuration']=0x1:(this[_0x4fd198(0x2da)]/=0x2,this[_0x4fd198(0x2da)]=Math[_0x4fd198(0x2a1)](this[_0x4fd198(0x2da)]));},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0x21e)]=function(_0x4bf4d0,_0x85f208){const _0x2b5e28=_0x2b848d,_0x1ab943=Window_OTB_TurnOrder[_0x2b5e28(0x18b)];if(_0x4bf4d0>0x0){if(this['_offset']>0x0)return _0x1ab943['OrderDirection']?-_0x1ab943[_0x2b5e28(0x295)]:_0x1ab943['SpriteThin'];else{if(this[_0x2b5e28(0x2d1)]<0x0)return _0x1ab943['OrderDirection']?-_0x85f208:_0x85f208;}}return 0x0;},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)][_0x2b848d(0x1b9)]=function(){const _0x258038=_0x2b848d,_0x3adec7=this[_0x258038(0x270)](),_0x10e94c=this[_0x258038(0x1b2)]===_0x3adec7[_0x258038(0x10b)]?!![]:![],_0x5e1b01=_0x10e94c?BattleManager['_otb_actionBattlersNext']:BattleManager[_0x258038(0x142)];let _0x461a4c=0x0,_0x24f414=_0x5e1b01[_0x258038(0x2d0)]-0x1;_0x10e94c&&(_0x461a4c=Math['max'](0x0,VisuMZ[_0x258038(0x12c)][_0x258038(0x199)](_0x5e1b01)-0x1));let _0x157e28=Sprite_OTB_TurnOrder_Battler[_0x258038(0x2b0)]['calculateTargetIndex']['call'](this);return _0x157e28+=this['_offset'],_0x157e28['clamp'](_0x461a4c,_0x24f414);},Sprite_OTB_TurnOrder_Preview[_0x2b848d(0x2b0)]['updateSelectionEffect']=function(){},Window_Selectable['prototype'][_0x2b848d(0x234)]=function(){return![];},VisuMZ['BattleSystemOTB'][_0x2b848d(0x2a7)]=Window_Selectable[_0x2b848d(0x2b0)][_0x2b848d(0x27f)],Window_Selectable['prototype'][_0x2b848d(0x27f)]=function(_0x238ce0){const _0x58f045=_0x2b848d;VisuMZ[_0x58f045(0x12c)]['Window_Selectable_select']['call'](this,_0x238ce0),this['isBattleItemWindowOTB']()&&this[_0x58f045(0xc9)]&&this[_0x58f045(0x185)]();},Window_Selectable[_0x2b848d(0x2b0)][_0x2b848d(0x185)]=function(){const _0xef09bd=_0x2b848d;BattleManager[_0xef09bd(0x2a6)]();},VisuMZ[_0x2b848d(0x12c)]['Window_Help_setItem']=Window_Help[_0x2b848d(0x2b0)][_0x2b848d(0x222)],Window_Help[_0x2b848d(0x2b0)][_0x2b848d(0x222)]=function(_0x3badc0){const _0x5d5474=_0x2b848d;BattleManager[_0x5d5474(0x282)]()&&_0x3badc0&&_0x3badc0['note']&&_0x3badc0[_0x5d5474(0x280)][_0x5d5474(0x23e)](/<(?:OTB) HELP>\s*([\s\S]*)\s*<\/(?:OTB) HELP>/i)?this[_0x5d5474(0x209)](String(RegExp['$1'])):VisuMZ[_0x5d5474(0x12c)][_0x5d5474(0x129)][_0x5d5474(0x28a)](this,_0x3badc0);},Window_ActorCommand[_0x2b848d(0x2b0)][_0x2b848d(0x234)]=function(){const _0x2b7b5c=_0x2b848d;return BattleManager[_0x2b7b5c(0x282)]();},Window_ActorCommand['prototype'][_0x2b848d(0x185)]=function(){const _0x1b6e75=_0x2b848d,_0x4b78e6=BattleManager[_0x1b6e75(0x164)]();if(_0x4b78e6){const _0x320c77=this[_0x1b6e75(0x1ec)]();switch(_0x320c77){case'attack':_0x4b78e6[_0x1b6e75(0x167)]();break;case _0x1b6e75(0x274):_0x4b78e6[_0x1b6e75(0xbd)]();break;case _0x1b6e75(0x149):_0x4b78e6[_0x1b6e75(0x2b3)](this[_0x1b6e75(0x26b)]());break;default:_0x4b78e6[_0x1b6e75(0x2b3)](null);break;}}Window_Command[_0x1b6e75(0x2b0)][_0x1b6e75(0x185)]['call'](this);},Window_BattleSkill[_0x2b848d(0x2b0)][_0x2b848d(0x234)]=function(){const _0x4f643b=_0x2b848d;return BattleManager[_0x4f643b(0x282)]();},Window_BattleSkill[_0x2b848d(0x2b0)][_0x2b848d(0x185)]=function(){const _0x5cb723=_0x2b848d,_0x28a37f=this[_0x5cb723(0x2be)](),_0x4817cc=BattleManager[_0x5cb723(0x164)]();if(_0x4817cc)_0x4817cc[_0x5cb723(0x2b3)](_0x28a37f?_0x28a37f['id']:null);Window_SkillList[_0x5cb723(0x2b0)]['applyBattleItemWindowOTB'][_0x5cb723(0x28a)](this);},Window_BattleItem['prototype'][_0x2b848d(0x234)]=function(){const _0x41f5c0=_0x2b848d;return BattleManager[_0x41f5c0(0x282)]();},Window_BattleItem['prototype']['applyBattleItemWindowOTB']=function(){const _0x2379a9=_0x2b848d,_0xcbfadb=this[_0x2379a9(0x2be)](),_0x50a6c0=BattleManager['inputtingAction']();if(_0x50a6c0)_0x50a6c0[_0x2379a9(0x222)](_0xcbfadb?_0xcbfadb['id']:null);Window_ItemList[_0x2379a9(0x2b0)][_0x2379a9(0x185)]['call'](this);},Window_BattleActor[_0x2b848d(0x2b0)][_0x2b848d(0x234)]=function(){return BattleManager['isOTB']();},Window_BattleEnemy['prototype']['isBattleItemWindowOTB']=function(){const _0x459f7a=_0x2b848d;return BattleManager[_0x459f7a(0x282)]();};function Window_OTB_TurnOrder(){const _0x4f0136=_0x2b848d;this[_0x4f0136(0x2bd)](...arguments);}Window_OTB_TurnOrder[_0x2b848d(0x2b0)]=Object[_0x2b848d(0x18a)](Window_Base[_0x2b848d(0x2b0)]),Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['constructor']=Window_OTB_TurnOrder,Window_OTB_TurnOrder[_0x2b848d(0x18b)]=VisuMZ[_0x2b848d(0x12c)][_0x2b848d(0x18b)][_0x2b848d(0x268)],Window_OTB_TurnOrder['prototype']['initialize']=function(){const _0x520aa8=_0x2b848d,_0x1649af=this[_0x520aa8(0x2b6)]();this[_0x520aa8(0xc7)](_0x1649af),Window_Base['prototype']['initialize'][_0x520aa8(0x28a)](this,_0x1649af),this[_0x520aa8(0x22b)]=0x0,this[_0x520aa8(0xeb)](),this[_0x520aa8(0x1e5)](),this[_0x520aa8(0x23d)](),this['updateVisibility']();},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x2b6)]=function(){const _0x10cb6c=_0x2b848d,_0x1b6ed3=Window_OTB_TurnOrder[_0x10cb6c(0x18b)],_0x432113=SceneManager[_0x10cb6c(0x109)][_0x10cb6c(0x1a4)][_0x10cb6c(0x245)];let _0x2b05cc=Graphics[_0x10cb6c(0x24d)]-_0x1b6ed3['ScreenBuffer']*0x2,_0x51b92e=_0x1b6ed3[_0x10cb6c(0x198)]+this[_0x10cb6c(0x1d8)](),_0x2819c8=_0x1b6ed3[_0x10cb6c(0x2a2)],_0x2e34a7=0x0;switch(_0x1b6ed3[_0x10cb6c(0x239)]){case _0x10cb6c(0x2a4):_0x2e34a7=Graphics[_0x10cb6c(0x245)]-_0x432113-_0x1b6ed3[_0x10cb6c(0x2a2)]-_0x51b92e;break;default:_0x2e34a7=_0x1b6ed3[_0x10cb6c(0x2a2)];break;}if(Imported[_0x10cb6c(0x117)]&&BattleManager['isUsingSideviewUiLayout']()){const _0x310ff9=VisuMZ[_0x10cb6c(0x2c5)][_0x10cb6c(0x18b)][_0x10cb6c(0x1d6)];_0x2b05cc-=_0x310ff9['WidthBase']+_0x310ff9[_0x10cb6c(0x2dd)],_0x2b05cc-=_0x1b6ed3[_0x10cb6c(0x2a2)];}return _0x2819c8+=_0x1b6ed3[_0x10cb6c(0x18e)]||0x0,_0x2e34a7+=_0x1b6ed3[_0x10cb6c(0xc4)]||0x0,new Rectangle(_0x2819c8,_0x2e34a7,_0x2b05cc,_0x51b92e);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0xc7)]=function(_0x24e6e2){const _0xab17af=_0x2b848d;this[_0xab17af(0x1a3)]=this[_0xab17af(0x1e0)]=_0x24e6e2['x'],this[_0xab17af(0xca)]=this[_0xab17af(0x1a7)]=_0x24e6e2['y'],this[_0xab17af(0x148)]=0x0;const _0x50469d=Window_OTB_TurnOrder['Settings'];this[_0xab17af(0x131)]=Math[_0xab17af(0x1d1)]((_0x24e6e2[_0xab17af(0x24d)]-_0x50469d[_0xab17af(0x295)]-_0x50469d[_0xab17af(0x287)]*0x2)/0x2),_0x50469d[_0xab17af(0xef)]?(this[_0xab17af(0x1aa)]=_0x24e6e2['width']-_0x50469d[_0xab17af(0x295)],this[_0xab17af(0x196)]=this['_spriteGroupWidth']+_0x50469d[_0xab17af(0x287)],this[_0xab17af(0xce)]=0x0):(this[_0xab17af(0x1aa)]=0x0,this[_0xab17af(0x196)]=_0x50469d[_0xab17af(0x295)]+_0x50469d[_0xab17af(0x287)],this[_0xab17af(0xce)]=this[_0xab17af(0x196)]+_0x50469d[_0xab17af(0x287)]+this['_spriteGroupWidth']);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x232)]=function(){const _0xc022f1=_0x2b848d;this[_0xc022f1(0x12a)]=0x0;},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0xeb)]=function(){const _0x1463c3=_0x2b848d,_0x5d3307=Window_OTB_TurnOrder['Settings'];if(_0x5d3307[_0x1463c3(0xc3)]===_0x1463c3(0x279))return;if(_0x5d3307[_0x1463c3(0xc3)]==='image'&&_0x5d3307[_0x1463c3(0x26e)]!==''){const _0x22d2dd=ImageManager['loadSystem'](_0x5d3307[_0x1463c3(0x26e)]);_0x22d2dd[_0x1463c3(0xe1)](this['drawBgImage'][_0x1463c3(0x221)](this,_0x22d2dd));return;};const _0x22c0f0=this[_0x1463c3(0x22d)],_0x49a267=ColorManager['dimColor1'](),_0x2ae8e3=ColorManager[_0x1463c3(0x1ee)](),_0x1eb6e4=this[_0x1463c3(0x1aa)],_0x1d4511=_0x5d3307[_0x1463c3(0x295)],_0x46a52c=0x0,_0x11cb40=_0x5d3307[_0x1463c3(0x198)],_0x3c2091=this['_currentX'],_0x19d6e4=this['_nextX'],_0x133f6a=this['_spriteGroupWidth'];switch(_0x5d3307[_0x1463c3(0xc3)]){case'gradient':_0x5d3307[_0x1463c3(0xef)]?(_0x22c0f0['gradientFillRect'](_0x1eb6e4,_0x46a52c,_0x1d4511/0x2,_0x11cb40,_0x2ae8e3,_0x49a267,![]),_0x22c0f0[_0x1463c3(0x116)](_0x1eb6e4+_0x1d4511/0x2,_0x46a52c,_0x1d4511/0x2,_0x11cb40,_0x49a267),_0x22c0f0[_0x1463c3(0x172)](_0x3c2091,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x2ae8e3,_0x49a267,![]),_0x22c0f0['fillRect'](_0x3c2091+_0x133f6a/0x2,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x49a267),_0x22c0f0['gradientFillRect'](_0x19d6e4,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x2ae8e3,_0x49a267,![]),_0x22c0f0[_0x1463c3(0x116)](_0x19d6e4+_0x133f6a/0x2,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x49a267)):(_0x22c0f0['fillRect'](_0x1eb6e4,_0x46a52c,_0x1d4511/0x2,_0x11cb40,_0x49a267),_0x22c0f0[_0x1463c3(0x172)](_0x1eb6e4+_0x1d4511/0x2,_0x46a52c,_0x1d4511/0x2,_0x11cb40,_0x49a267,_0x2ae8e3,![]),_0x22c0f0[_0x1463c3(0x116)](_0x3c2091,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x49a267),_0x22c0f0['gradientFillRect'](_0x3c2091+_0x133f6a/0x2,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x49a267,_0x2ae8e3,![]),_0x22c0f0['fillRect'](_0x19d6e4,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x49a267),_0x22c0f0[_0x1463c3(0x172)](_0x19d6e4+_0x133f6a/0x2,_0x46a52c,_0x133f6a/0x2,_0x11cb40,_0x49a267,_0x2ae8e3,![]));break;default:_0x22c0f0[_0x1463c3(0x116)](_0x1eb6e4,_0x46a52c,_0x1d4511,_0x11cb40,_0x49a267),_0x22c0f0[_0x1463c3(0x116)](_0x3c2091,_0x46a52c,_0x133f6a,_0x11cb40,_0x49a267),_0x22c0f0['fillRect'](_0x19d6e4,_0x46a52c,_0x133f6a,_0x11cb40,_0x49a267);break;}},Window_OTB_TurnOrder['prototype'][_0x2b848d(0x1bf)]=function(_0x5d29c0){const _0x2f9410=_0x2b848d;this[_0x2f9410(0x125)]=new Sprite(),this['_bgImageSprite']['bitmap']=_0x5d29c0,this[_0x2f9410(0x119)](this['_bgImageSprite']);const _0xb1f98e=Window_OTB_TurnOrder[_0x2f9410(0x18b)];this[_0x2f9410(0x125)]['x']=_0xb1f98e[_0x2f9410(0x203)],this[_0x2f9410(0x125)]['y']=_0xb1f98e[_0x2f9410(0x174)];},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x1e5)]=function(){const _0x14c3ae=_0x2b848d;this[_0x14c3ae(0x276)][_0x14c3ae(0x21a)](),this[_0x14c3ae(0x1ff)]();const _0x3929b0=Window_OTB_TurnOrder[_0x14c3ae(0x18b)];this[_0x14c3ae(0x276)][_0x14c3ae(0x136)]=_0x3929b0['UiFontSize'];let _0x5123ac=_0x3929b0[_0x14c3ae(0x123)];_0x5123ac===_0x14c3ae(0x166)&&(_0x5123ac=_0x3929b0['OrderDirection']?_0x14c3ae(0x192):_0x14c3ae(0x21c));let _0x94d8a8=_0x3929b0[_0x14c3ae(0x198)];if(_0x3929b0[_0x14c3ae(0x28b)]!==''){const _0x3c5cb5=this[_0x14c3ae(0x1aa)]+_0x3929b0[_0x14c3ae(0x2b8)],_0x3db43a=_0x94d8a8+_0x3929b0[_0x14c3ae(0x1fb)],_0x3e707b=_0x3929b0[_0x14c3ae(0x295)];this[_0x14c3ae(0x244)](_0x3929b0[_0x14c3ae(0x28b)],_0x3c5cb5,_0x3db43a,_0x3e707b,_0x14c3ae(0x18c));}if(_0x3929b0['UiCurrentText']!==''){const _0x31bbb4=this[_0x14c3ae(0x196)]+_0x3929b0[_0x14c3ae(0x273)],_0x25faf7=_0x94d8a8+_0x3929b0[_0x14c3ae(0x2db)],_0x45891a=this[_0x14c3ae(0x131)];this[_0x14c3ae(0x244)](_0x3929b0[_0x14c3ae(0xda)],_0x31bbb4,_0x25faf7,_0x45891a,_0x5123ac);}if(_0x3929b0[_0x14c3ae(0x2ba)]!==''){const _0x28be43=this['_nextX']+_0x3929b0[_0x14c3ae(0x247)],_0x10422b=_0x94d8a8+_0x3929b0[_0x14c3ae(0xd7)],_0x402c9a=this[_0x14c3ae(0x131)];this['drawText'](_0x3929b0['UiNextText'],_0x28be43,_0x10422b,_0x402c9a,_0x5123ac);}},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x23d)]=function(){const _0x562fbe=_0x2b848d,_0x970965=Window_OTB_TurnOrder[_0x562fbe(0x18b)];this['_spriteContainer']=new Sprite(),this[_0x562fbe(0x111)](this[_0x562fbe(0x107)]),this[_0x562fbe(0xfe)]=null,this[_0x562fbe(0x29f)]=[],this['_nextTurn']=[],this[_0x562fbe(0x1b5)]=new Sprite(),this['_previewContainer']['x']=_0x970965[_0x562fbe(0x19d)],this[_0x562fbe(0x1b5)]['y']=_0x970965[_0x562fbe(0x12f)],this[_0x562fbe(0x1b5)]['x']-=Math['ceil'](_0x970965[_0x562fbe(0x295)]*0.5*_0x970965[_0x562fbe(0x14b)]),_0x970965[_0x562fbe(0xef)]&&(this[_0x562fbe(0x1b5)]['x']+=_0x970965[_0x562fbe(0x295)]),this[_0x562fbe(0x1b5)]['y']-=Math[_0x562fbe(0x1d1)](_0x970965[_0x562fbe(0x198)]*0.5*_0x970965['PreviewScale']),this[_0x562fbe(0x111)](this[_0x562fbe(0x1b5)]),this[_0x562fbe(0xf2)]=[],this['_previewNext']=[];},Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['update']=function(){const _0x8f671d=_0x2b848d;Window_Base[_0x8f671d(0x2b0)][_0x8f671d(0x1af)][_0x8f671d(0x28a)](this),this[_0x8f671d(0x252)](),this['updatePosition'](),this[_0x8f671d(0x2b7)](),this['sortContainer']();},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x292)]=function(){const _0x374811=_0x2b848d;this[_0x374811(0xdc)]=!![];},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x252)]=function(){const _0x50a9f7=_0x2b848d;if(!this[_0x50a9f7(0xdc)])return;this[_0x50a9f7(0xdc)]=![];for(const _0x1f8f26 of this[_0x50a9f7(0x29f)]){if(!_0x1f8f26)continue;_0x1f8f26[_0x50a9f7(0x153)]();}for(const _0x19ac7c of this['_nextTurn']){if(!_0x19ac7c)continue;_0x19ac7c[_0x50a9f7(0x153)]();}},Window_OTB_TurnOrder['prototype'][_0x2b848d(0x1d9)]=function(){const _0x8fcfa3=_0x2b848d,_0x438d55=Window_OTB_TurnOrder[_0x8fcfa3(0x18b)];if(_0x438d55[_0x8fcfa3(0x239)]!==_0x8fcfa3(0x204))return;if(!_0x438d55['RepositionTopForHelp'])return;const _0x155057=SceneManager[_0x8fcfa3(0x109)]['_helpWindow'];if(!_0x155057)return;_0x155057[_0x8fcfa3(0x2b2)]?(this['x']=this[_0x8fcfa3(0x1e0)]+(_0x438d55[_0x8fcfa3(0x18d)]||0x0),this['y']=this[_0x8fcfa3(0x1a7)]+(_0x438d55[_0x8fcfa3(0xe8)]||0x0)):(this['x']=this[_0x8fcfa3(0x1e0)],this['y']=this[_0x8fcfa3(0x1a7)]);const _0x503b48=SceneManager[_0x8fcfa3(0x109)][_0x8fcfa3(0x1f9)];this[_0x8fcfa3(0x272)]===undefined&&(this[_0x8fcfa3(0x272)]=Math[_0x8fcfa3(0x2d9)]((Graphics[_0x8fcfa3(0x24d)]-Math[_0x8fcfa3(0x1e8)](Graphics[_0x8fcfa3(0x298)],_0x503b48[_0x8fcfa3(0x24d)]))/0x2),this[_0x8fcfa3(0x243)]=Math[_0x8fcfa3(0x2d9)]((Graphics[_0x8fcfa3(0x245)]-Math[_0x8fcfa3(0x1e8)](Graphics[_0x8fcfa3(0x2c9)],_0x503b48[_0x8fcfa3(0x245)]))/0x2)),this['x']+=_0x503b48['x']-this[_0x8fcfa3(0x272)],this['y']+=_0x503b48['y']-this[_0x8fcfa3(0x243)];},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x2b7)]=function(){const _0x1f3c42=_0x2b848d;this[_0x1f3c42(0x2b2)]=$gameSystem[_0x1f3c42(0x15a)]();if(BattleManager[_0x1f3c42(0x281)]===_0x1f3c42(0x1b6)){if(!this[_0x1f3c42(0x261)]){const _0x4ec32a=Window_OTB_TurnOrder[_0x1f3c42(0x18b)];this[_0x1f3c42(0x261)]=Math[_0x1f3c42(0x1d1)](0xff/(_0x4ec32a[_0x1f3c42(0x17c)]||0x1));}this['opacity']-=this[_0x1f3c42(0x261)],this['contentsOpacity']-=this['_fadeSpeed'],this[_0x1f3c42(0x2d3)][_0x1f3c42(0x22b)]-=this[_0x1f3c42(0x261)];}},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x2c4)]=function(){const _0x279f05=_0x2b848d;if(!this[_0x279f05(0x107)])return;const _0x340f0b=Window_OTB_TurnOrder['Settings'],_0x236a6f=_0x340f0b[_0x279f05(0xef)];_0x236a6f?this[_0x279f05(0x107)][_0x279f05(0x13d)][_0x279f05(0x134)]((_0x1ac7f9,_0x1e1c30)=>_0x1ac7f9['x']-_0x1e1c30['x']):this[_0x279f05(0x107)][_0x279f05(0x13d)][_0x279f05(0x134)]((_0x4901e3,_0x573a9e)=>_0x573a9e['x']-_0x4901e3['x']);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['removeSprite']=function(_0x38ccd6){const _0x121f07=_0x2b848d;if(!_0x38ccd6)return;_0x38ccd6[_0x121f07(0x1b2)]&&_0x38ccd6[_0x121f07(0x1b2)][_0x121f07(0x130)](_0x38ccd6);const _0x2be1bd=Window_OTB_TurnOrder[_0x121f07(0x18b)],_0x5569e5=0x3e8/0x3c*_0x2be1bd[_0x121f07(0x17c)]+0x1f4;_0x38ccd6['startFade'](0x0),setTimeout(this['processSpriteRemoval'][_0x121f07(0x221)](this,_0x38ccd6),_0x5569e5);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x13c)]=function(_0x3bf914){const _0x42be06=_0x2b848d;_0x3bf914[_0x42be06(0x1b2)]&&_0x3bf914['_sourceArray']['remove'](_0x3bf914),this[_0x42be06(0x107)][_0x42be06(0x1d7)](_0x3bf914),this[_0x42be06(0x1b5)][_0x42be06(0x1d7)](_0x3bf914);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x277)]=function(){const _0x4897fc=_0x2b848d;if(!this['_subject'])return;this[_0x4897fc(0x14e)](this['_subject']);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x127)]=function(){const _0x47ef7e=_0x2b848d;while(this['_currentTurn'][_0x47ef7e(0x2d0)]){const _0x3d3c1e=this[_0x47ef7e(0x29f)][_0x47ef7e(0x1a2)]();_0x3d3c1e[_0x47ef7e(0x114)](0x0);}while(this[_0x47ef7e(0x10b)][_0x47ef7e(0x2d0)]){const _0x5046af=this['_nextTurn'][_0x47ef7e(0x1a2)]();if(!_0x5046af)continue;this['_currentTurn'][_0x47ef7e(0x132)](_0x5046af);}for(const _0x1d0bbf of this['_currentTurn']){if(!_0x1d0bbf)continue;_0x1d0bbf[_0x47ef7e(0x1d3)](this[_0x47ef7e(0x29f)]);}},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x2af)]=function(_0x4165f6,_0x3d6f8e){const _0x5ecae1=_0x2b848d,_0x479fb0=_0x4165f6===BattleManager[_0x5ecae1(0x142)]?this['_currentTurn']:this[_0x5ecae1(0x10b)],_0x573001={};for(const _0x4a8659 of _0x4165f6){const _0x138dc4='%1-%2'[_0x5ecae1(0x2b9)](_0x4a8659[_0x5ecae1(0xd1)]()?_0x5ecae1(0x1eb):_0x5ecae1(0x1c5),_0x4a8659[_0x5ecae1(0x19b)]());_0x573001[_0x138dc4]=_0x573001[_0x138dc4]||0x0;const _0x3f03db=_0x573001[_0x138dc4]++,_0x505b03=new Sprite_OTB_TurnOrder_Battler(_0x4a8659,_0x3f03db,_0x479fb0);this[_0x5ecae1(0x107)][_0x5ecae1(0x111)](_0x505b03),_0x479fb0[_0x5ecae1(0x132)](_0x505b03);}for(const _0x22a588 of _0x479fb0){if(!_0x22a588)continue;_0x22a588[_0x5ecae1(0x114)](0xff),_0x22a588['calculateTargetPositions'](),_0x3d6f8e&&(_0x22a588[_0x5ecae1(0x22b)]=0xff,_0x22a588['x']=_0x22a588[_0x5ecae1(0x1c1)],_0x22a588[_0x5ecae1(0x20e)]=0x0);}},Window_OTB_TurnOrder['prototype'][_0x2b848d(0x2c1)]=function(){const _0x36c09=_0x2b848d,_0x877f24=BattleManager[_0x36c09(0xf7)];this[_0x36c09(0x2af)](_0x877f24);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['shiftTurnOrderForSubject']=function(_0x31874b,_0x7c787a){const _0x524b99=_0x2b848d;this[_0x524b99(0x277)]();for(const _0x4fb3b9 of this[_0x524b99(0x29f)]){if(!_0x4fb3b9)continue;_0x4fb3b9['battler']()===_0x31874b&&(_0x4fb3b9['_instance']=_0x4fb3b9[_0x524b99(0x2c8)]||0x0,_0x4fb3b9[_0x524b99(0x2c8)]--);}const _0x4e9a35=this['_currentTurn'][_0x524b99(0x103)](_0x168ad5=>_0x168ad5[_0x524b99(0x112)]()===_0x31874b);if(this[_0x524b99(0x29f)][_0x4e9a35])this[_0x524b99(0xfe)]=this[_0x524b99(0x29f)][_0x4e9a35],this[_0x524b99(0x29f)][_0x4e9a35]['calculateTargetPositions'](),this[_0x524b99(0x29f)][_0x524b99(0x1b7)](_0x4e9a35,0x1);else{const _0x2cc3de=new Sprite_OTB_TurnOrder_Battler(_0x31874b,-0x1,null);this[_0x524b99(0x107)][_0x524b99(0x111)](_0x2cc3de),this[_0x524b99(0xfe)]=_0x2cc3de,_0x2cc3de[_0x524b99(0x114)](0xff),_0x2cc3de[_0x524b99(0x20e)]=0x258,_0x2cc3de['x']=this[_0x524b99(0x1aa)],_0x2cc3de[_0x524b99(0x1c1)]=this['_subjectX'],_0x7c787a&&(_0x2cc3de[_0x524b99(0x22b)]=0xff);}for(const _0x4c9364 of this[_0x524b99(0x29f)]){if(!_0x4c9364)continue;_0x4c9364[_0x524b99(0x153)]();}},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x154)]=function(){const _0x14b900=_0x2b848d;for(const _0x43a8c6 of this['_currentTurn']){if(!_0x43a8c6)continue;const _0x90a3bf=_0x43a8c6[_0x14b900(0x112)]();if(BattleManager[_0x14b900(0x142)][_0x14b900(0x1b4)](_0x90a3bf))continue;this[_0x14b900(0x14e)](_0x43a8c6);}for(const _0xbd3885 of this[_0x14b900(0x10b)]){if(!_0xbd3885)continue;const _0x9bcde6=_0xbd3885['battler']();if(BattleManager[_0x14b900(0xf7)][_0x14b900(0x1b4)](_0x9bcde6))continue;this[_0x14b900(0x14e)](_0xbd3885);}},Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['addBattlerToTurnOrderAtEnd']=function(_0x26fee0,_0x472f77){const _0x45c466=_0x2b848d,_0x347127=_0x472f77===BattleManager[_0x45c466(0x142)]?this[_0x45c466(0x29f)]:this[_0x45c466(0x10b)];if(!_0x347127)return;const _0x24cb68=VisuMZ[_0x45c466(0x12c)]['GetAllIndicies'](_0x26fee0,_0x472f77),_0x1e39aa=_0x24cb68['length']-0x1,_0x43e0f2=new Sprite_OTB_TurnOrder_Battler(_0x26fee0,_0x1e39aa,_0x347127);this['_spriteContainer'][_0x45c466(0x111)](_0x43e0f2),_0x347127['push'](_0x43e0f2),_0x43e0f2[_0x45c466(0x114)](0xff),this[_0x45c466(0x292)]();},Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['addBattlerToTurnOrderAtStart']=function(_0x46b673,_0x12ab93){const _0x395324=_0x2b848d,_0x11e3e8=_0x12ab93===BattleManager[_0x395324(0x142)]?this[_0x395324(0x29f)]:this['_nextTurn'];if(!_0x11e3e8)return;for(const _0x3ba7de of _0x11e3e8){if(!_0x3ba7de)continue;_0x3ba7de[_0x395324(0x112)]()===_0x46b673&&(_0x3ba7de[_0x395324(0x2c8)]=_0x3ba7de[_0x395324(0x2c8)]||0x0,_0x3ba7de[_0x395324(0x2c8)]++);}const _0x32e62d=0x0,_0x49dd44=new Sprite_OTB_TurnOrder_Battler(_0x46b673,_0x32e62d,_0x11e3e8);this[_0x395324(0x107)][_0x395324(0x111)](_0x49dd44),_0x11e3e8[_0x395324(0x132)](_0x49dd44),_0x49dd44[_0x395324(0x114)](0xff),_0x49dd44[_0x395324(0x20e)]=0x258,_0x49dd44['x']=this[_0x395324(0x1aa)],this['requestUpdateTurnOrders']();},Window_OTB_TurnOrder[_0x2b848d(0x2b0)]['resumeTurnOrderSprites']=function(){const _0x1523cf=_0x2b848d;this[_0x1523cf(0x2af)](BattleManager['_actionBattlers'],!![]),this[_0x1523cf(0x2af)](BattleManager[_0x1523cf(0xf7)],!![]),this[_0x1523cf(0x1d5)](BattleManager[_0x1523cf(0xfe)],!![]),this[_0x1523cf(0x2c4)]();},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x29c)]=function(_0x698ea0){const _0x589cad=_0x2b848d;this['clearOrderPreview'](),_0x698ea0&&_0x698ea0[_0x589cad(0x2be)]()!==null&&this[_0x589cad(0xfb)](_0x698ea0);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0xd5)]=function(){const _0xf738ee=_0x2b848d;for(const _0x35a731 of this[_0xf738ee(0x1b5)][_0xf738ee(0x13d)]){if(!_0x35a731)continue;this[_0xf738ee(0x14e)](_0x35a731);}},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0xfb)]=function(_0x2d3e14){const _0x2c276b=_0x2b848d,_0x522275=_0x2d3e14[_0x2c276b(0x206)](),_0x4bae11=_0x2d3e14['otbCalcUserCurrentOrderChange'](),_0x28332a=_0x2d3e14[_0x2c276b(0x101)]();_0x4bae11!==0x0&&this[_0x2c276b(0x25b)](_0x522275,![],_0x4bae11);_0x28332a!==0x0&&this[_0x2c276b(0x25b)](_0x522275,!![],_0x28332a);if(!_0x2d3e14[_0x2c276b(0x22c)]())return;const _0x2022e6=SceneManager[_0x2c276b(0x109)]['_actorWindow'],_0x259807=SceneManager[_0x2c276b(0x109)][_0x2c276b(0x177)];let _0x43200f=null;if(_0x2022e6&&_0x2022e6['active'])_0x43200f=_0x2022e6[_0x2c276b(0x1eb)](_0x2022e6[_0x2c276b(0x19b)]());else _0x259807&&_0x259807[_0x2c276b(0xc9)]&&(_0x43200f=_0x259807[_0x2c276b(0x1c5)]());if(!_0x43200f)return;const _0x3f5440=_0x2d3e14[_0x2c276b(0x155)](_0x43200f),_0x4f456f=_0x2d3e14[_0x2c276b(0x29d)](_0x43200f);_0x3f5440!==0x0&&this[_0x2c276b(0x25b)](_0x43200f,![],_0x3f5440),_0x4f456f!==0x0&&this[_0x2c276b(0x25b)](_0x43200f,!![],_0x4f456f);},Window_OTB_TurnOrder[_0x2b848d(0x2b0)][_0x2b848d(0x25b)]=function(_0x515596,_0xe13dd4,_0x16135d){const _0x3f50ad=_0x2b848d;if(!_0x515596)return;if(_0x16135d===0x0)return;const _0x3b91a1=_0xe13dd4?BattleManager[_0x3f50ad(0xf7)]:BattleManager[_0x3f50ad(0x142)],_0x3fb2df=VisuMZ[_0x3f50ad(0x12c)][_0x3f50ad(0x1bb)](_0x515596,_0x3b91a1),_0x2e9dd0=_0xe13dd4?this[_0x3f50ad(0x10b)]:this[_0x3f50ad(0x29f)],_0x1a1f53=_0xe13dd4?this[_0x3f50ad(0x118)]:this['_previewCurrent'];if(_0x3fb2df['length']<=0x0)return;for(let _0x314e3c=0x0;_0x314e3c<_0x3fb2df[_0x3f50ad(0x2d0)];_0x314e3c++){const _0xc6f599=new Sprite_OTB_TurnOrder_Preview(_0x515596,_0x314e3c,_0x2e9dd0,_0x16135d);this[_0x3f50ad(0x1b5)]['addChild'](_0xc6f599),_0x1a1f53[_0x3f50ad(0x132)](_0xc6f599),_0xc6f599[_0x3f50ad(0x153)](),_0xc6f599[_0x3f50ad(0x114)](0xff);}};