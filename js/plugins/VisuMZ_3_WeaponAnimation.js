//=============================================================================
// VisuStella MZ - Weapon Animation
// VisuMZ_3_WeaponAnimation.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_3_WeaponAnimation = true;

var VisuMZ = VisuMZ || {};
VisuMZ.WeaponAnimation = VisuMZ.WeaponAnimation || {};
VisuMZ.WeaponAnimation.version = 1.05;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 3] [Version 1.05] [WeaponAnimation]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Weapon_Animation_VisuStella_MZ
 * @base VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_BattleCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * Ever wanted to give your swords different images despite being the same
 * sword type? Or how about your axes? Or any weapon? Now you can! On top of
 * that, you can even use custom images to accomplish this.
 * 
 * This plugin allows you to go past the standard weapon images and even using
 * custom images.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Select different weapon animation from the weapon sprite sheets.
 * * Use custom images for weapon animations.
 * * Allow weapons to have their own unique weapon animation sprites.
 * * Customize hues and motions for the weapon animations.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_1_BattleCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 3 ------
 *
 * This plugin is a Tier 3 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Sprite_Weapon loadBitmap function Change
 * 
 * Due to how this plugin works, loading bitmaps for the Sprite_Weapon
 * prototype class is now different. Depending if there is any data found for a
 * custom weapon animation, the bitmap data will be loaded differently to
 * accommodate the differences in file structure.
 *
 * ---
 * 
 * Sprite_Weapon updateFrame function Change
 * 
 * Due to how this plugin works, updating frames for the Sprite_Weapon
 * prototype class is now different. Depending if there is any data found for a
 * custom weapon animation, the frame data will be setup differently to
 * accommodate the differences in file structure.
 * 
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === Weapon Image-Related Notetags ===
 * 
 * ---
 *
 * <Weapon Image: x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Changes the weapon image used for the affected battler to a numeric type.
 * - Replace 'x' with a number representing the weapon image's ID.
 * - You'll get an image from "img/system/" folder's weapon sheets.
 * - Each sheet contains 12 weapon images. If you wish to load a weapon from
 *   the first sheet, it'll be within 1-12.
 * - If you wish to load a weapon from the second sheet, it'll be within 13-24,
 *   and so on.
 * - The weapon sheets increase in increments of 12, which means that if you
 *   wish to load a weapon from weapon sheet 50, x will be between 589 to 600.
 *
 *   By default, these are the number values associated with each:
 * 
 *   1 - Dagger   7 - Long Bow  13 - Mace       19 - Slingshot  25 - Book
 *   2 - Sword    8 - Crossbow  14 - Rod        20 - Shotgun    26 - Custom
 *   3 - Flail    9 - Gun       15 - Club       21 - Rifle      27 - Custom
 *   4 - Axe     10 - Claw      16 - Chain      22 - Chainsaw   28 - Custom
 *   5 - Whip    11 - Glove     17 - Sword#2    23 - Railgun    29 - Custom
 *   6 - Staff   12 - Spear     18 - Iron Pipe  24 - Stun Rod   30 - Custom
 *
 * ---
 *
 * <Weapon Image: filename>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Changes the weapon image used for the affected battler to a unique file.
 * - Replace 'filename' with the name of the file found in the "img/weapons/"
 *   folder (or whichever folder you've set it to in the plugin parameters).
 * - This is case sensitive.
 * - Do not include the file extension.
 * 
 *   Example:
 * 
 *   <Weapon Image: Beam Sword>
 *
 * ---
 *
 * <Weapon Motion: thrust>
 * <Weapon Motion: swing>
 * <Weapon Motion: missile>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - This notetag requires a <Weapon Image: x> or <Weapon Image: filename>
 *   notetag on the same trait object.
 * - Forces the weapon to play a specific motion when attacking.
 * - If this is not defined, the played motion will be the custom motion
 *   declared in the plugin parameters.
 * - You can also replace the motion type with the following:
 * 
 *   walk     wait     chant     guard     damage     evade
 *   thrust   swing    missile   skill     spell      item
 *   escape   victory  dying     abnormal  sleep      dead
 *
 * ---
 *
 * <Weapon Hue: x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - This notetag requires a <Weapon Image: x> or <Weapon Image: filename>
 *   notetag on the same trait object.
 * - Changes the hue of the custom weapon image.
 * - Replace 'x' with a hue number between 0 and 255.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * There's a couple of plugin parameters that can be adjusted for this plugin.
 *
 * ---
 *
 * General
 * 
 *   Image Filepath:
 *   - The filepath used for custom weapon images folder.
 *   - This defaults to "img/weapons/"
 * 
 *   Default Motion:
 *   - Default motion used for custom weapon images.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.05: April 9, 2021
 * * Bug Fixes!
 * ** Freeze Motions should now hide weapons instead of always displaying them
 *    when the hide option is enabled. Fix made by Olivia.
 * 
 * Version 1.04: February 12, 2021
 * * Bug Fixes!
 * ** Freeze frame now supports enemy custom weapon images. Fix made by Irina.
 * 
 * Version 1.03: January 29, 2021
 * * Bug Fixes!
 * ** Basic weapon animations should now show the proper weapon image.
 *    Fix made by Olivia.
 * ** Freeze frame now supports custom non-attack animations. Fix by Olivia.
 * 
 * Version 1.02: January 22, 2021
 * * Compatibility Update
 * ** Plugin is now compatible with Battle Core's Freeze Motion.
 * 
 * Version 1.01: November 22, 2020
 * * Bug Fixes!
 * ** If battlers with custom weapon animations perform an Action Sequence with
 *    "Show Weapon" set to false, they will no longer force the attack motion.
 *    Fix made by Yanfly.
 *
 * Version 1.00: November 25, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param WeaponAnimation
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param filepath:str
 * @text Image Filepath
 * @desc The filepath used for custom weapon images folder.
 * @default img/weapons/
 *
 * @param motion:str
 * @text Default Motion
 * @type combo
 * @option swing
 * @option thrust
 * @option missile
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Default motion used for custom weapon images.
 * @default swing
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
//=============================================================================

const _0x1443=['NUM','ARRAYNUM','call','weapons','Motion','checkCacheKey','EVAL','clamp','updateFrame','customWeaponGraphic','ImageNum','loadBitmap','max','2GNvnlV','refresh','motionType','updateFrameCustomWeaponGraphic','_customFrames','loadBitmapCustomWeapon','Game_BattlerBase_initMembers','createCustomWeaponGraphic','version','Hue','JSON','format','number','STRUCT','WeaponAnimation','_pattern','startWeaponAnimation','createCustomWeaponGraphicFromObj','description','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','exit','loadWeapon','parameters','3161987DgLARQ','FUNC','313iPNJHP','hue','_uniqueStartWeaponAnimation','motion','width','preloadCustomWeaponImage','match','_weaponImageId','isCustomWeaponGraphic','toUpperCase','requestMotion','parse','enemy','ConvertParams','filepath','initMembers','Weapons','swing','trim','885332JOfJAO','loadSystem','filter','freezeMotion','bitmap','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','prototype','toLowerCase','ARRAYFUNC','isActor','startAction','map','name','isEnemy','Settings','_freezeMotionData','RegExp','_cache','327480cfLTFc','1109674beVPea','893678rMqGvA','status','setHue','BattleManager_startAction','Sprite_Weapon_loadBitmap','_subject','1234341syFUwq','VisuMZ_1_BattleCore','1LAMHaN','weaponImageId','2701MgMtrv','Game_Battler_startWeaponAnimation','floor','ImageStr','Game_BattlerBase_refresh'];const _0x2efc=function(_0x46ea86,_0x5b8103){_0x46ea86=_0x46ea86-0xde;let _0x14437b=_0x1443[_0x46ea86];return _0x14437b;};const _0xb0be2=_0x2efc;(function(_0x4de889,_0x3cd186){const _0xca0e70=_0x2efc;while(!![]){try{const _0xd30e93=-parseInt(_0xca0e70(0x10c))+-parseInt(_0xca0e70(0xf2))+parseInt(_0xca0e70(0x106))+parseInt(_0xca0e70(0x104))*parseInt(_0xca0e70(0x122))+-parseInt(_0xca0e70(0x105))*parseInt(_0xca0e70(0x10e))+-parseInt(_0xca0e70(0xdf))*parseInt(_0xca0e70(0x110))+parseInt(_0xca0e70(0x139));if(_0xd30e93===_0x3cd186)break;else _0x4de889['push'](_0x4de889['shift']());}catch(_0x34d1bd){_0x4de889['push'](_0x4de889['shift']());}}}(_0x1443,0x9b3d9));var label=_0xb0be2(0x130),tier=tier||0x0,dependencies=[_0xb0be2(0x10d)],pluginData=$plugins[_0xb0be2(0xf4)](function(_0x1991c3){const _0x29316f=_0xb0be2;return _0x1991c3[_0x29316f(0x107)]&&_0x1991c3[_0x29316f(0x134)]['includes']('['+label+']');})[0x0];VisuMZ[label][_0xb0be2(0x100)]=VisuMZ[label][_0xb0be2(0x100)]||{},VisuMZ[_0xb0be2(0xec)]=function(_0x3ae9c2,_0x5cc9c2){const _0x4da11b=_0xb0be2;for(const _0x26f575 in _0x5cc9c2){if(_0x26f575[_0x4da11b(0xe5)](/(.*):(.*)/i)){const _0x26a915=String(RegExp['$1']),_0x34e0c7=String(RegExp['$2'])[_0x4da11b(0xe8)]()['trim']();let _0x20f34f,_0xda7d44,_0x47c383;switch(_0x34e0c7){case _0x4da11b(0x115):_0x20f34f=_0x5cc9c2[_0x26f575]!==''?Number(_0x5cc9c2[_0x26f575]):0x0;break;case _0x4da11b(0x116):_0xda7d44=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):[],_0x20f34f=_0xda7d44['map'](_0x38a478=>Number(_0x38a478));break;case _0x4da11b(0x11b):_0x20f34f=_0x5cc9c2[_0x26f575]!==''?eval(_0x5cc9c2[_0x26f575]):null;break;case'ARRAYEVAL':_0xda7d44=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):[],_0x20f34f=_0xda7d44['map'](_0x3c7908=>eval(_0x3c7908));break;case _0x4da11b(0x12c):_0x20f34f=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):'';break;case'ARRAYJSON':_0xda7d44=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):[],_0x20f34f=_0xda7d44[_0x4da11b(0xfd)](_0x2eebb6=>JSON[_0x4da11b(0xea)](_0x2eebb6));break;case _0x4da11b(0xde):_0x20f34f=_0x5cc9c2[_0x26f575]!==''?new Function(JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575])):new Function('return\x200');break;case _0x4da11b(0xfa):_0xda7d44=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):[],_0x20f34f=_0xda7d44['map'](_0x57f78e=>new Function(JSON[_0x4da11b(0xea)](_0x57f78e)));break;case'STR':_0x20f34f=_0x5cc9c2[_0x26f575]!==''?String(_0x5cc9c2[_0x26f575]):'';break;case'ARRAYSTR':_0xda7d44=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):[],_0x20f34f=_0xda7d44[_0x4da11b(0xfd)](_0xca71fe=>String(_0xca71fe));break;case _0x4da11b(0x12f):_0x47c383=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):{},_0x20f34f=VisuMZ[_0x4da11b(0xec)]({},_0x47c383);break;case'ARRAYSTRUCT':_0xda7d44=_0x5cc9c2[_0x26f575]!==''?JSON[_0x4da11b(0xea)](_0x5cc9c2[_0x26f575]):[],_0x20f34f=_0xda7d44['map'](_0x244611=>VisuMZ['ConvertParams']({},JSON[_0x4da11b(0xea)](_0x244611)));break;default:continue;}_0x3ae9c2[_0x26a915]=_0x20f34f;}}return _0x3ae9c2;},(_0x3462d3=>{const _0x1d228b=_0xb0be2,_0x405a78=_0x3462d3[_0x1d228b(0xfe)];for(const _0x5628b0 of dependencies){if(!Imported[_0x5628b0]){alert(_0x1d228b(0x135)[_0x1d228b(0x12d)](_0x405a78,_0x5628b0)),SceneManager[_0x1d228b(0x136)]();break;}}const _0x34124e=_0x3462d3[_0x1d228b(0x134)];if(_0x34124e[_0x1d228b(0xe5)](/\[Version[ ](.*?)\]/i)){const _0x251ae1=Number(RegExp['$1']);_0x251ae1!==VisuMZ[label][_0x1d228b(0x12a)]&&(alert('%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.'[_0x1d228b(0x12d)](_0x405a78,_0x251ae1)),SceneManager[_0x1d228b(0x136)]());}if(_0x34124e[_0x1d228b(0xe5)](/\[Tier[ ](\d+)\]/i)){const _0x2dbc14=Number(RegExp['$1']);_0x2dbc14<tier?(alert(_0x1d228b(0xf7)[_0x1d228b(0x12d)](_0x405a78,_0x2dbc14,tier)),SceneManager[_0x1d228b(0x136)]()):tier=Math[_0x1d228b(0x121)](_0x2dbc14,tier);}VisuMZ['ConvertParams'](VisuMZ[label][_0x1d228b(0x100)],_0x3462d3[_0x1d228b(0x138)]);})(pluginData),VisuMZ['WeaponAnimation'][_0xb0be2(0x102)]={'ImageNum':/<WEAPON IMAGE:[ ](\d+)>/i,'ImageStr':/<WEAPON IMAGE:[ ](.*)>/i,'Hue':/<WEAPON HUE:[ ](\d+)>/i,'Motion':/<WEAPON MOTION:[ ](.*)>/i},ImageManager[_0xb0be2(0x137)]=function(_0x13771a){const _0x58f37c=_0xb0be2,_0x3b8076=VisuMZ[_0x58f37c(0x130)][_0x58f37c(0x100)][_0x58f37c(0xed)];return this[_0x58f37c(0x120)](_0x3b8076,_0x13771a);},VisuMZ[_0xb0be2(0x130)][_0xb0be2(0x109)]=BattleManager[_0xb0be2(0xfc)],BattleManager[_0xb0be2(0xfc)]=function(){const _0x3e73bd=_0xb0be2;VisuMZ[_0x3e73bd(0x130)][_0x3e73bd(0x109)][_0x3e73bd(0x117)](this),this[_0x3e73bd(0x10b)]&&this[_0x3e73bd(0x10b)][_0x3e73bd(0xe4)]();},VisuMZ['WeaponAnimation'][_0xb0be2(0x128)]=Game_BattlerBase[_0xb0be2(0xf8)]['initMembers'],Game_BattlerBase['prototype'][_0xb0be2(0xee)]=function(){const _0x23d359=_0xb0be2;this[_0x23d359(0x103)]={},VisuMZ['WeaponAnimation'][_0x23d359(0x128)]['call'](this);},VisuMZ[_0xb0be2(0x130)]['Game_BattlerBase_refresh']=Game_BattlerBase[_0xb0be2(0xf8)][_0xb0be2(0x123)],Game_BattlerBase[_0xb0be2(0xf8)][_0xb0be2(0x123)]=function(){const _0xa4ae61=_0xb0be2;this[_0xa4ae61(0x103)]={},VisuMZ['WeaponAnimation'][_0xa4ae61(0x114)][_0xa4ae61(0x117)](this);},Game_BattlerBase[_0xb0be2(0xf8)][_0xb0be2(0x11a)]=function(_0x4ab1d0){const _0x20b2c9=_0xb0be2;return this['_cache']=this[_0x20b2c9(0x103)]||{},this['_cache'][_0x4ab1d0]!==undefined;},Game_BattlerBase[_0xb0be2(0xf8)][_0xb0be2(0x11e)]=function(){const _0x4b7899=_0xb0be2;let _0x3d0dda='customWeaponGraphic';if(this[_0x4b7899(0x11a)](_0x3d0dda))return this[_0x4b7899(0x103)][_0x3d0dda];return this[_0x4b7899(0x103)][_0x3d0dda]=this[_0x4b7899(0x129)](),this[_0x4b7899(0x103)][_0x3d0dda];},Game_BattlerBase[_0xb0be2(0xf8)][_0xb0be2(0x129)]=function(){const _0x3c713f=_0xb0be2;for(const _0x57827e of this['traitObjects']()){if(!_0x57827e)continue;const _0x2255d5=this['createCustomWeaponGraphicFromObj'](_0x57827e);if(_0x2255d5['name']!==0x0)return{'name':_0x2255d5[_0x3c713f(0xfe)],'hue':_0x2255d5[_0x3c713f(0xe0)],'motion':_0x2255d5['motion']};}return 0x0;},Game_BattlerBase[_0xb0be2(0xf8)][_0xb0be2(0x133)]=function(_0x185af2){const _0xebc22e=_0xb0be2,_0x48f7fa=VisuMZ[_0xebc22e(0x130)][_0xebc22e(0x102)];let _0x2a1fb6=0x0,_0x2fb422=0x0,_0x257cce=VisuMZ[_0xebc22e(0x130)]['Settings'][_0xebc22e(0xe2)];const _0x404fe3=_0x185af2['note'];if(_0x404fe3[_0xebc22e(0xe5)](_0x48f7fa[_0xebc22e(0x11f)]))_0x2a1fb6=Number(RegExp['$1'])||0x1;else _0x404fe3[_0xebc22e(0xe5)](_0x48f7fa[_0xebc22e(0x113)])&&(_0x2a1fb6=String(RegExp['$1']));return _0x404fe3[_0xebc22e(0xe5)](_0x48f7fa[_0xebc22e(0x12b)])&&(_0x2fb422=Number(RegExp['$1'])[_0xebc22e(0x11c)](0x0,0xff)),_0x404fe3[_0xebc22e(0xe5)](_0x48f7fa[_0xebc22e(0x119)])&&(_0x257cce=String(RegExp['$1'])[_0xebc22e(0xf9)]()[_0xebc22e(0xf1)]()),{'name':_0x2a1fb6,'hue':_0x2fb422,'motion':_0x257cce};},VisuMZ['WeaponAnimation'][_0xb0be2(0x111)]=Game_Battler[_0xb0be2(0xf8)][_0xb0be2(0x132)],Game_Battler[_0xb0be2(0xf8)][_0xb0be2(0x132)]=function(_0x114387){const _0x457ce4=_0xb0be2;if(this[_0x457ce4(0xe1)])return;let _0x2a55b5=![];this['customWeaponGraphic']()&&_0x114387>0x0&&(_0x114387=this[_0x457ce4(0x11e)](),_0x2a55b5=!![]);VisuMZ[_0x457ce4(0x130)][_0x457ce4(0x111)]['call'](this,_0x114387);if(!_0x2a55b5)return;if(_0x114387===0x0)return;this[_0x457ce4(0xe1)]=!![],this[_0x457ce4(0xe9)](_0x114387['motion']||_0x457ce4(0xf0)),this[_0x457ce4(0xe1)]=![];},Game_Battler['prototype']['preloadCustomWeaponImage']=function(){const _0x1c23c6=_0xb0be2;if(!this[_0x1c23c6(0x11e)]())return;const _0x7b98d7=this[_0x1c23c6(0x11e)]();if(typeof _0x7b98d7[_0x1c23c6(0xfe)]==='number'){const _0x37e9f8=Math[_0x1c23c6(0x112)]((_0x7b98d7[_0x1c23c6(0xfe)]-0x1)/0xc)+0x1;ImageManager[_0x1c23c6(0xf3)](_0x1c23c6(0xef)+_0x37e9f8);}else ImageManager['loadWeapon'](_0x7b98d7[_0x1c23c6(0xfe)]);},VisuMZ[_0xb0be2(0x130)]['Game_Battler_freezeMotion']=Game_Battler[_0xb0be2(0xf8)][_0xb0be2(0xf5)],Game_Battler[_0xb0be2(0xf8)][_0xb0be2(0xf5)]=function(_0x4d190b,_0x1535eb,_0x4d94cc){const _0x26283b=_0xb0be2;VisuMZ[_0x26283b(0x130)]['Game_Battler_freezeMotion'][_0x26283b(0x117)](this,_0x4d190b,_0x1535eb,_0x4d94cc);if(!_0x1535eb)return;let _0x402633=0x0;_0x4d190b['match'](/ATTACK[ ](\d+)/i)&&(_0x402633=Number(RegExp['$1']),_0x402633--);if(this[_0x26283b(0xfb)]()){const _0xad0c7f=this[_0x26283b(0x118)](),_0x317a72=_0xad0c7f[_0x402633]||null,_0x34bf85=this[_0x26283b(0x133)](_0x317a72);_0x34bf85[_0x26283b(0xfe)]!==0x0&&(_0x4d190b[_0x26283b(0xe5)](/ATTACK/i)&&(this['_freezeMotionData']['motionType']=_0x34bf85[_0x26283b(0xe2)]),this[_0x26283b(0x101)][_0x26283b(0x10f)]=_0x34bf85[_0x26283b(0xfe)]);}else{if(this[_0x26283b(0xff)]()){const _0xf3cbfb=this['createCustomWeaponGraphicFromObj'](this[_0x26283b(0xeb)]());_0xf3cbfb['name']!==0x0&&(_0x4d190b['match'](/ATTACK/i)&&(this[_0x26283b(0x101)][_0x26283b(0x124)]=_0xf3cbfb['motion']),this[_0x26283b(0x101)][_0x26283b(0x10f)]=_0xf3cbfb[_0x26283b(0xfe)]);}}},Sprite_Weapon[_0xb0be2(0xf8)][_0xb0be2(0xe7)]=function(){const _0x162a0d=_0xb0be2;return typeof this['_weaponImageId']!==_0x162a0d(0x12e);},VisuMZ['WeaponAnimation']['Sprite_Weapon_loadBitmap']=Sprite_Weapon[_0xb0be2(0xf8)][_0xb0be2(0x120)],Sprite_Weapon[_0xb0be2(0xf8)][_0xb0be2(0x120)]=function(){const _0x3aaa97=_0xb0be2;this[_0x3aaa97(0xe7)]()?this[_0x3aaa97(0x127)]():(this[_0x3aaa97(0x126)]=![],VisuMZ[_0x3aaa97(0x130)][_0x3aaa97(0x10a)]['call'](this),this[_0x3aaa97(0x108)](0x0));},Sprite_Weapon[_0xb0be2(0xf8)][_0xb0be2(0x127)]=function(){const _0x2e81fe=_0xb0be2;if(typeof this['_weaponImageId']['name']===_0x2e81fe(0x12e)){const _0x2cedbf=Math[_0x2e81fe(0x112)]((this[_0x2e81fe(0xe6)][_0x2e81fe(0xfe)]-0x1)/0xc)+0x1;_0x2cedbf>=0x1?this[_0x2e81fe(0xf6)]=ImageManager[_0x2e81fe(0xf3)](_0x2e81fe(0xef)+_0x2cedbf):this[_0x2e81fe(0xf6)]=ImageManager[_0x2e81fe(0xf3)]('');}else{this[_0x2e81fe(0x126)]=!![];const _0x537d4a=this[_0x2e81fe(0xe6)][_0x2e81fe(0xfe)]?this[_0x2e81fe(0xe6)][_0x2e81fe(0xfe)]:this['_weaponImageId'];this[_0x2e81fe(0xf6)]=ImageManager[_0x2e81fe(0x137)](_0x537d4a||'');}this[_0x2e81fe(0x108)](this[_0x2e81fe(0xe6)]['hue']||0x0);},VisuMZ[_0xb0be2(0x130)]['Sprite_Weapon_updateFrame']=Sprite_Weapon[_0xb0be2(0xf8)][_0xb0be2(0x11d)],Sprite_Weapon['prototype'][_0xb0be2(0x11d)]=function(){const _0x389688=_0xb0be2;this['isCustomWeaponGraphic']()?this[_0x389688(0x125)]():VisuMZ[_0x389688(0x130)]['Sprite_Weapon_updateFrame'][_0x389688(0x117)](this);},Sprite_Weapon[_0xb0be2(0xf8)][_0xb0be2(0x125)]=function(){const _0x29569a=_0xb0be2;if(typeof this['_weaponImageId'][_0x29569a(0xfe)]===_0x29569a(0x12e)){const _0x37d58d=(this[_0x29569a(0xe6)][_0x29569a(0xfe)]-0x1)%0xc,_0x5793a9=0x60,_0x4b0a2e=0x40,_0x3a0518=(Math[_0x29569a(0x112)](_0x37d58d/0x6)*0x3+this[_0x29569a(0x131)])*_0x5793a9,_0x1f7483=Math['floor'](_0x37d58d%0x6)*_0x4b0a2e;this['setFrame'](_0x3a0518,_0x1f7483,_0x5793a9,_0x4b0a2e);}else{const _0x42a8e7=Math[_0x29569a(0x112)](this[_0x29569a(0xf6)][_0x29569a(0xe3)]/0x3),_0x36d024=this[_0x29569a(0xf6)]['height'],_0x4bd281=this[_0x29569a(0x131)]*_0x42a8e7,_0x232a42=0x0;this['setFrame'](_0x4bd281,_0x232a42,_0x42a8e7,_0x36d024);}};