//=============================================================================
// VisuStella MZ - Skill Containers
// VisuMZ_4_SkillContainers.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_4_SkillContainers = true;

var VisuMZ = VisuMZ || {};
VisuMZ.SkillContainers = VisuMZ.SkillContainers || {};
VisuMZ.SkillContainers.version = 1.00;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 4] [Version 1.00] [SkillContainers]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Skill_Containers_VisuStella_MZ
 * @orderAfter VisuMZ_0_CoreEngine
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * Skill Containers let you transform skills in-game to contain an inner list
 * of skills, accessible to players. These container skills will draw from a
 * list of skills that either require the player to already have them or allow
 * them to even use skills they don't normally have access to.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Skill Containers let you condense skills to become containers for lists of
 *   other skills accessible to the player.
 * * Reduce the size of a skill library by grouping them together.
 * * Skill Containers can contain skills that require the actor to already know
 *   them (either through learning or traits) or forcefully allow them to be
 *   accessible regardless.
 * * These container skills don't appear unless the container itself has access
 *   to at least one skill.
 * * These container skills are usable from the skill menu or in-battle!
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 4 ------
 *
 * This plugin is a Tier 4 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === Skill Container-Related Notetags ===
 * 
 * ---
 *
 * <Known Skill List: id>
 * <Known Skills List: id, id, id>
 *
 * <Known Skill List: name>
 * <Known Skills List: name, name, name>
 * 
 * <Known Skills List: id To id>
 *
 * - Used for: Skill Notetags
 * - Turns the skill into a skill container, accessible to actors and players.
 * - Replace 'id' with a number representing the ID of the skill you wish to
 *   add to the skill container.
 * - Replace 'name' with the name of the skill you wish to add to the
 *   skill container.
 * - Use the 'id To id' version to get a range of skills to add to the list.
 *   - This will ignore any skills with no names or have ----- in their name.
 * - These skills require the actor to have learned the skill or to have access
 *   to the skill 
 * - Insert multiple copies of the notetag to add more.
 * - Skill Containers cannot be used as Single Skill Commands for the VisuMZ
 *   Battle Core's Actor Command Window (just use a Skill Type instead).
 * - Skill Containers can be stacked inside one another.
 * 
 *   Examples:
 * 
 *   <Known Skills List: 51, 52, 53>
 *   <Known Skills List: Heal I, Heal II, Heal III>
 *   <Known Skills List: 51 To 53>
 *
 * ---
 *
 * <Force Skill List: id>
 * <Force Skills List: id, id, id>
 *
 * <Force Skill List: name>
 * <Force Skills List: name, name, name>
 * 
 * <Force Skills List: id To id>
 *
 * - Used for: Skill Notetags
 * - Turns the skill into a skill container, accessible to actors and players.
 * - Replace 'id' with a number representing the ID of the skill you wish to
 *   add to the skill container.
 * - Replace 'name' with the name of the skill you wish to add to the
 *   skill container.
 * - Use the 'id To id' version to get a range of skills to add to the list.
 *   - This will ignore any skills with no names or have ----- in their name.
 * - These skills do NOT require the actor to have learned the skill. These
 *   listed skills will always be accessible.
 * - Insert multiple copies of the notetag to add more.
 * - Skill Containers cannot be used as Single Skill Commands for the VisuMZ
 *   Battle Core's Actor Command Window (just use a Skill Type instead).
 * - Skill Containers can be stacked inside one another.
 * 
 *   Examples:
 * 
 *   <Force Skills List: 51, 52, 53>
 *   <Force Skills List: Heal I, Heal II, Heal III>
 *   <Force Skills List: 51 To 53>
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * The Plugin Parameters allow you to adjust how the text for Skill Containers
 * appear in-game. This way, you can help your players differentiate them from
 * regular skills.
 *
 * ---
 *
 * General
 * 
 *   Skill Container Text:
 *   - Determines the text that appears where the skill costs normally would
 *     appear instead.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.00 Official Release Date: May 7, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PluginCommandFunctionName
 * @text Category: Function Name
 * @desc Plugin Command Description Text
 *
 * @arg Step1:arraynum
 * @text Step 1: Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @arg option:num
 * @text Option Text
 * @type number
 * @max 1
 * @desc Change the value to this number
 * @default 42069
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemEnableSkillContainersMenu
 * @text System: Enable SkillContainers in Menu?
 * @desc Enables/disables SkillContainers menu inside the main menu.
 *
 * @arg Enable:eval
 * @text Enable/Disable?
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables/disables SkillContainers menu inside the main menu.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemShowSkillContainersMenu
 * @text System: Show SkillContainers in Menu?
 * @desc Shows/hides SkillContainers menu inside the main menu.
 *
 * @arg Show:eval
 * @text Show/Hide?
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Shows/hides SkillContainers menu inside the main menu.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param SkillContainers
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param ContainerText:str
 * @text Skill Container Text
 * @desc Determines the text that appears where the skill costs
 * normally would appear instead.
 * @default \FS[22]......
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
//=============================================================================

const _0x3479=['onSkillCancel','Window_SkillList_drawSkillCost','KnownListRange','getSkillIdWithName','call','1HCAwRT','canAddSkillCommand','Window_SkillList_makeItemList','FUNC','getSkillContainerList','_skillWindow','resetFontSettings','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','length','parse','concat','ConvertParams','indexOf','ARRAYSTR','Scene_Skill_onItemOk','924196ZzUqai','onItemCancel','649496xHeLcd','onItemOk','_itemWindow','toUpperCase','62swOash','909165ITXItF','skillContainerText','Scene_Battle_onSkillOk','JSON','clearSkillContainerStacks','processSkillContainerCancel','Scene_Battle_onSkillCancel','addSkillContainerStack','Settings','forceSelect','parameters','1057830BlVQoo','drawSkillCost','format','_data','match','parseSkillContainerList','ForceListRange','_actor','refresh','Scene_Skill_onItemCancel','textSizeEx','STR','exit','EVAL','removeSkillContainerStack','isSkillContainer','Scene_Battle_selectNextCommand','includes','ARRAYJSON','initialize','488183TQbtZY','width','return\x200','313689DAbCNp','KnownList','prototype','onSkillOk','trim','index','filter','name','_skillIDs','Window_ActorCommand_canAddSkillCommand','processSkillContainerOk','SkillContainers','ARRAYFUNC','makeItemList','ARRAYEVAL','note','isShowingSkillContainerList','STRUCT','skill','push','16557qzfPhu','makeSkillContainerList','ForceList','selectNextCommand','description','map','split','activate','Window_SkillList_includes','_skillContainerStack','item','Window_SkillList_initialize','RegExp','drawSkillContainerText','version','NUM'];const _0xc705=function(_0x275384,_0x5600b0){_0x275384=_0x275384-0x177;let _0x3479e1=_0x3479[_0x275384];return _0x3479e1;};const _0x466303=_0xc705;(function(_0x41d06e,_0x10927b){const _0x2fd4a2=_0xc705;while(!![]){try{const _0x1428c9=parseInt(_0x2fd4a2(0x1ad))+-parseInt(_0x2fd4a2(0x1cc))+parseInt(_0x2fd4a2(0x1cf))*-parseInt(_0x2fd4a2(0x197))+parseInt(_0x2fd4a2(0x182))*parseInt(_0x2fd4a2(0x1ac))+-parseInt(_0x2fd4a2(0x1a8))+parseInt(_0x2fd4a2(0x1b8))+-parseInt(_0x2fd4a2(0x1a6));if(_0x1428c9===_0x10927b)break;else _0x41d06e['push'](_0x41d06e['shift']());}catch(_0x1d04e3){_0x41d06e['push'](_0x41d06e['shift']());}}}(_0x3479,0x96ded));var label=_0x466303(0x179),tier=tier||0x0,dependencies=[],pluginData=$plugins[_0x466303(0x1d5)](function(_0x237a7c){const _0x10fa30=_0x466303;return _0x237a7c['status']&&_0x237a7c[_0x10fa30(0x186)][_0x10fa30(0x1c9)]('['+label+']');})[0x0];VisuMZ[label][_0x466303(0x1b5)]=VisuMZ[label][_0x466303(0x1b5)]||{},VisuMZ[_0x466303(0x1a2)]=function(_0x5bfbdb,_0x360d30){const _0x539d37=_0x466303;for(const _0xe307d5 in _0x360d30){if(_0xe307d5[_0x539d37(0x1bc)](/(.*):(.*)/i)){const _0x4133da=String(RegExp['$1']),_0x255e06=String(RegExp['$2'])['toUpperCase']()[_0x539d37(0x1d3)]();let _0x3c2770,_0x20c0cf,_0x37feff;switch(_0x255e06){case _0x539d37(0x191):_0x3c2770=_0x360d30[_0xe307d5]!==''?Number(_0x360d30[_0xe307d5]):0x0;break;case'ARRAYNUM':_0x20c0cf=_0x360d30[_0xe307d5]!==''?JSON[_0x539d37(0x1a0)](_0x360d30[_0xe307d5]):[],_0x3c2770=_0x20c0cf[_0x539d37(0x187)](_0x4d5d6b=>Number(_0x4d5d6b));break;case _0x539d37(0x1c5):_0x3c2770=_0x360d30[_0xe307d5]!==''?eval(_0x360d30[_0xe307d5]):null;break;case _0x539d37(0x17c):_0x20c0cf=_0x360d30[_0xe307d5]!==''?JSON['parse'](_0x360d30[_0xe307d5]):[],_0x3c2770=_0x20c0cf[_0x539d37(0x187)](_0x5d7daa=>eval(_0x5d7daa));break;case _0x539d37(0x1b0):_0x3c2770=_0x360d30[_0xe307d5]!==''?JSON[_0x539d37(0x1a0)](_0x360d30[_0xe307d5]):'';break;case _0x539d37(0x1ca):_0x20c0cf=_0x360d30[_0xe307d5]!==''?JSON[_0x539d37(0x1a0)](_0x360d30[_0xe307d5]):[],_0x3c2770=_0x20c0cf[_0x539d37(0x187)](_0x3bc90c=>JSON['parse'](_0x3bc90c));break;case _0x539d37(0x19a):_0x3c2770=_0x360d30[_0xe307d5]!==''?new Function(JSON[_0x539d37(0x1a0)](_0x360d30[_0xe307d5])):new Function(_0x539d37(0x1ce));break;case _0x539d37(0x17a):_0x20c0cf=_0x360d30[_0xe307d5]!==''?JSON['parse'](_0x360d30[_0xe307d5]):[],_0x3c2770=_0x20c0cf[_0x539d37(0x187)](_0x8f5974=>new Function(JSON[_0x539d37(0x1a0)](_0x8f5974)));break;case _0x539d37(0x1c3):_0x3c2770=_0x360d30[_0xe307d5]!==''?String(_0x360d30[_0xe307d5]):'';break;case _0x539d37(0x1a4):_0x20c0cf=_0x360d30[_0xe307d5]!==''?JSON['parse'](_0x360d30[_0xe307d5]):[],_0x3c2770=_0x20c0cf['map'](_0x2de59f=>String(_0x2de59f));break;case _0x539d37(0x17f):_0x37feff=_0x360d30[_0xe307d5]!==''?JSON[_0x539d37(0x1a0)](_0x360d30[_0xe307d5]):{},_0x3c2770=VisuMZ[_0x539d37(0x1a2)]({},_0x37feff);break;case'ARRAYSTRUCT':_0x20c0cf=_0x360d30[_0xe307d5]!==''?JSON[_0x539d37(0x1a0)](_0x360d30[_0xe307d5]):[],_0x3c2770=_0x20c0cf[_0x539d37(0x187)](_0x1fae0e=>VisuMZ['ConvertParams']({},JSON[_0x539d37(0x1a0)](_0x1fae0e)));break;default:continue;}_0x5bfbdb[_0x4133da]=_0x3c2770;}}return _0x5bfbdb;},(_0x1fb74f=>{const _0x1f0c22=_0x466303,_0x30eae4=_0x1fb74f[_0x1f0c22(0x1d6)];for(const _0x118383 of dependencies){if(!Imported[_0x118383]){alert('%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.'['format'](_0x30eae4,_0x118383)),SceneManager[_0x1f0c22(0x1c4)]();break;}}const _0x5cbd56=_0x1fb74f['description'];if(_0x5cbd56[_0x1f0c22(0x1bc)](/\[Version[ ](.*?)\]/i)){const _0xcc4d48=Number(RegExp['$1']);_0xcc4d48!==VisuMZ[label][_0x1f0c22(0x190)]&&(alert('%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.'['format'](_0x30eae4,_0xcc4d48)),SceneManager['exit']());}if(_0x5cbd56[_0x1f0c22(0x1bc)](/\[Tier[ ](\d+)\]/i)){const _0x1a5800=Number(RegExp['$1']);_0x1a5800<tier?(alert(_0x1f0c22(0x19e)[_0x1f0c22(0x1ba)](_0x30eae4,_0x1a5800,tier)),SceneManager['exit']()):tier=Math['max'](_0x1a5800,tier);}VisuMZ[_0x1f0c22(0x1a2)](VisuMZ[label][_0x1f0c22(0x1b5)],_0x1fb74f[_0x1f0c22(0x1b7)]);})(pluginData),VisuMZ[_0x466303(0x179)][_0x466303(0x18e)]={'KnownList':/<(?:KNOWN|EXTRA) (?:SKILL|SKILLS) LIST:[ ](.*)>/gi,'KnownListRange':/<(?:KNOWN|EXTRA) (?:SKILL|SKILLS) LIST:[ ](\d+)[ ](?:THROUGH|to)[ ](\d+)>/gi,'ForceList':/<(?:FORCE|FORCED) (?:SKILL|SKILLS) LIST:[ ](.*)>/gi,'ForceListRange':/<(?:FORCE|FORCED) (?:SKILL|SKILLS) LIST:[ ](\d+)[ ](?:THROUGH|to)[ ](\d+)>/gi,'Type1':/<(?:NOTETAG):[ ](\d+)([%％])>/i,'Type2':/<(?:NOTETAG):[ ]([\+\-]\d+)>/i,'Type3':/<(?:NOTETAG):[ ](.*)>/i,'Type3nonGreedy':/<(?:NOTETAG):[ ](.*?)>/i,'Type4':/<(?:NOTETAG):[ ]*(\d+(?:\s*,\s*\d+)*)>/i,'Type5':/<(?:NOTETAG):[ ](\d+)[ ](?:THROUGH|to)[ ](\d+)>/i,'Type6':/<(?:NOTETAG)>/i,'Type7':/<\/(?:NOTETAG)>/i,'Type8':/<(?:NOTETAG)>\s*([\s\S]*)\s*<\/(?:NOTETAG)>/i},DataManager[_0x466303(0x1c7)]=function(_0x4e125e){const _0x328710=_0x466303;if(!_0x4e125e)return![];const _0x163ba7=VisuMZ[_0x328710(0x179)]['RegExp'],_0x5940da=_0x4e125e[_0x328710(0x17d)];return _0x5940da[_0x328710(0x1bc)](_0x163ba7[_0x328710(0x1d0)])||_0x5940da[_0x328710(0x1bc)](_0x163ba7[_0x328710(0x184)]);},DataManager[_0x466303(0x19b)]=function(_0x1f722b,_0x44550f){const _0x3bf9a2=_0x466303;if(!_0x44550f)return[];const _0x848fac=VisuMZ[_0x3bf9a2(0x179)][_0x3bf9a2(0x18e)],_0x17c09e=_0x44550f[_0x3bf9a2(0x17d)];let _0x351938=[];if(_0x1f722b){if(!![]){const _0x277487=_0x17c09e['match'](_0x848fac[_0x3bf9a2(0x1d0)]);if(_0x277487)for(const _0x298c9f of _0x277487){_0x298c9f['match'](_0x848fac[_0x3bf9a2(0x1d0)]);let _0x164e2e=DataManager[_0x3bf9a2(0x1bd)](RegExp['$1']);_0x164e2e=_0x164e2e[_0x3bf9a2(0x1d5)](_0x6efbc=>_0x1f722b['hasSkill'](_0x6efbc)),_0x351938=_0x351938[_0x3bf9a2(0x1a1)](_0x164e2e);}}if(!![]){const _0x11b2d4=_0x17c09e[_0x3bf9a2(0x1bc)](_0x848fac[_0x3bf9a2(0x194)]);if(_0x11b2d4)for(const _0x12e4f of _0x11b2d4){_0x12e4f[_0x3bf9a2(0x1bc)](_0x848fac['KnownListRange']);const _0x144aa1=Number(RegExp['$1']),_0x1baab3=Number(RegExp['$2']);let _0x359f2e=[];for(let _0x4cc131=_0x144aa1;_0x4cc131<=_0x1baab3;_0x4cc131++){_0x359f2e[_0x3bf9a2(0x181)](_0x4cc131);}_0x359f2e=_0x359f2e['filter'](_0x497fa6=>_0x1f722b['hasSkill'](_0x497fa6)),_0x351938=_0x351938[_0x3bf9a2(0x1a1)](_0x359f2e);}}}if(!![]){if(!![]){const _0x4dab82=_0x17c09e['match'](_0x848fac[_0x3bf9a2(0x184)]);if(_0x4dab82)for(const _0x16e52a of _0x4dab82){_0x16e52a['match'](_0x848fac['ForceList']);let _0x5a0e99=DataManager[_0x3bf9a2(0x1bd)](RegExp['$1']);_0x351938=_0x351938[_0x3bf9a2(0x1a1)](_0x5a0e99);}}if(!![]){const _0x345af0=_0x17c09e[_0x3bf9a2(0x1bc)](_0x848fac['ForceListRange']);if(_0x345af0)for(const _0xf94bf1 of _0x345af0){_0xf94bf1[_0x3bf9a2(0x1bc)](_0x848fac[_0x3bf9a2(0x1be)]);const _0x52b1b4=Number(RegExp['$1']),_0x106045=Number(RegExp['$2']);let _0x4c4384=[];for(let _0x233374=_0x52b1b4;_0x233374<=_0x106045;_0x233374++){_0x4c4384['push'](_0x233374);}_0x351938=_0x351938[_0x3bf9a2(0x1a1)](_0x4c4384);}}}return _0x351938=_0x351938[_0x3bf9a2(0x1d5)](_0x318cf1=>!!$dataSkills[_0x318cf1]),_0x351938=_0x351938[_0x3bf9a2(0x1d5)](_0x27bb89=>_0x27bb89!==_0x44550f['id']),_0x351938=_0x351938[_0x3bf9a2(0x1d5)](_0x518702=>$dataSkills[_0x518702][_0x3bf9a2(0x1d6)]['trim']()!==''),_0x351938=_0x351938['filter'](_0x4cfa81=>!$dataSkills[_0x4cfa81]['name'][_0x3bf9a2(0x1bc)](/-----/i)),_0x351938=_0x351938['filter']((_0x8f104f,_0x24e953,_0x1aba76)=>_0x1aba76[_0x3bf9a2(0x1a3)](_0x8f104f)===_0x24e953),_0x351938['sort']((_0x2febab,_0x441d5e)=>_0x2febab-_0x441d5e),_0x351938;},DataManager['parseSkillContainerList']=function(_0x15e3ba){const _0x456ad0=_0x466303;_0x15e3ba=_0x15e3ba[_0x456ad0(0x188)](',')[_0x456ad0(0x187)](_0x436850=>_0x436850[_0x456ad0(0x1d3)]());let _0x2d9681=[];for(let _0x4d761c of _0x15e3ba){_0x4d761c=(String(_0x4d761c)||'')[_0x456ad0(0x1d3)]();const _0x520211=/^\d+$/['test'](_0x4d761c);_0x520211?_0x2d9681['push'](Number(_0x4d761c)):_0x2d9681[_0x456ad0(0x181)](DataManager['getSkillIdWithName'](_0x4d761c));}return _0x2d9681;},DataManager[_0x466303(0x195)]=function(_0x2167d1){const _0x4a9684=_0x466303;_0x2167d1=_0x2167d1[_0x4a9684(0x1ab)]()[_0x4a9684(0x1d3)](),this['_skillIDs']=this[_0x4a9684(0x1d7)]||{};if(this[_0x4a9684(0x1d7)][_0x2167d1])return this[_0x4a9684(0x1d7)][_0x2167d1];for(const _0x27f5cb of $dataSkills){if(!_0x27f5cb)continue;this['_skillIDs'][_0x27f5cb['name']['toUpperCase']()[_0x4a9684(0x1d3)]()]=_0x27f5cb['id'];}return this['_skillIDs'][_0x2167d1]||0x0;},TextManager[_0x466303(0x1ae)]='\x5cFS[22]...',VisuMZ[_0x466303(0x179)]['Scene_Skill_onItemOk']=Scene_Skill[_0x466303(0x1d1)][_0x466303(0x1a9)],Scene_Skill[_0x466303(0x1d1)]['onItemOk']=function(){const _0x35499c=_0x466303,_0x312f7f=this['item']();DataManager[_0x35499c(0x1c7)](_0x312f7f)?this[_0x35499c(0x178)]():VisuMZ[_0x35499c(0x179)][_0x35499c(0x1a5)][_0x35499c(0x196)](this);},Scene_Skill[_0x466303(0x1d1)][_0x466303(0x178)]=function(){const _0x3afd7c=_0x466303,_0x47425d={'skill':this[_0x3afd7c(0x1aa)]['item'](),'index':this[_0x3afd7c(0x1aa)][_0x3afd7c(0x1d4)]()};this['_itemWindow']['addSkillContainerStack'](_0x47425d),this[_0x3afd7c(0x1aa)][_0x3afd7c(0x189)]();},VisuMZ[_0x466303(0x179)][_0x466303(0x1c1)]=Scene_Skill['prototype'][_0x466303(0x1a7)],Scene_Skill['prototype'][_0x466303(0x1a7)]=function(){const _0x113348=_0x466303;this[_0x113348(0x1aa)]['isShowingSkillContainerList']()?this[_0x113348(0x1b2)]():VisuMZ['SkillContainers'][_0x113348(0x1c1)]['call'](this);},Scene_Skill[_0x466303(0x1d1)][_0x466303(0x1b2)]=function(){const _0xab7d06=_0x466303;this[_0xab7d06(0x1aa)][_0xab7d06(0x1c6)](),this['_itemWindow'][_0xab7d06(0x189)]();},VisuMZ[_0x466303(0x179)][_0x466303(0x1af)]=Scene_Battle[_0x466303(0x1d1)][_0x466303(0x1d2)],Scene_Battle[_0x466303(0x1d1)]['onSkillOk']=function(){const _0x57d8f7=_0x466303,_0x870aff=this[_0x57d8f7(0x19c)][_0x57d8f7(0x18c)]();DataManager[_0x57d8f7(0x1c7)](_0x870aff)?this[_0x57d8f7(0x178)]():VisuMZ[_0x57d8f7(0x179)][_0x57d8f7(0x1af)]['call'](this);},Scene_Battle[_0x466303(0x1d1)]['processSkillContainerOk']=function(){const _0x2b9344=_0x466303,_0x253e73={'skill':this['_skillWindow']['item'](),'index':this['_skillWindow'][_0x2b9344(0x1d4)]()};this[_0x2b9344(0x19c)][_0x2b9344(0x1b4)](_0x253e73),this[_0x2b9344(0x19c)][_0x2b9344(0x189)]();},VisuMZ[_0x466303(0x179)][_0x466303(0x1b3)]=Scene_Battle[_0x466303(0x1d1)][_0x466303(0x192)],Scene_Battle[_0x466303(0x1d1)][_0x466303(0x192)]=function(){const _0xdb5ea=_0x466303;this[_0xdb5ea(0x19c)][_0xdb5ea(0x17e)]()?this[_0xdb5ea(0x1b2)]():VisuMZ['SkillContainers']['Scene_Battle_onSkillCancel']['call'](this);},Scene_Battle[_0x466303(0x1d1)][_0x466303(0x1b2)]=function(){const _0x36b74b=_0x466303;this['_skillWindow'][_0x36b74b(0x1c6)](),this['_skillWindow'][_0x36b74b(0x189)]();},VisuMZ[_0x466303(0x179)][_0x466303(0x1c8)]=Scene_Battle[_0x466303(0x1d1)][_0x466303(0x185)],Scene_Battle[_0x466303(0x1d1)][_0x466303(0x185)]=function(){const _0x2ee195=_0x466303;this[_0x2ee195(0x19c)]&&this[_0x2ee195(0x19c)][_0x2ee195(0x1b1)](![]),VisuMZ[_0x2ee195(0x179)][_0x2ee195(0x1c8)][_0x2ee195(0x196)](this);},VisuMZ[_0x466303(0x179)][_0x466303(0x18d)]=Window_SkillList['prototype'][_0x466303(0x1cb)],Window_SkillList[_0x466303(0x1d1)][_0x466303(0x1cb)]=function(_0x5262a5){const _0xc2223b=_0x466303;VisuMZ['SkillContainers']['Window_SkillList_initialize'][_0xc2223b(0x196)](this,_0x5262a5),this['_skillContainerStack']=[];},Window_SkillList[_0x466303(0x1d1)][_0x466303(0x1b4)]=function(_0x4dd20b){const _0x48aea2=_0x466303;this[_0x48aea2(0x18b)][_0x48aea2(0x181)](_0x4dd20b),this[_0x48aea2(0x1c0)](),this[_0x48aea2(0x1b6)](0x0);},Window_SkillList[_0x466303(0x1d1)][_0x466303(0x1c6)]=function(){const _0x4930ad=_0x466303;if(this[_0x4930ad(0x18b)][_0x4930ad(0x19f)]<=0x0)return;const _0x267fc0=this['_skillContainerStack'][this[_0x4930ad(0x18b)]['length']-0x1],_0x326b2d=_0x267fc0[_0x4930ad(0x1d4)]||0x0;this[_0x4930ad(0x18b)]['pop'](),this[_0x4930ad(0x1c0)](),this[_0x4930ad(0x1b6)](_0x326b2d);},Window_SkillList['prototype']['clearSkillContainerStacks']=function(_0x2d0c80){const _0x4e5ce8=_0x466303;if(this[_0x4e5ce8(0x18b)][_0x4e5ce8(0x19f)]<=0x0)return;const _0x2f4e12=this[_0x4e5ce8(0x18b)][0x0],_0x293b37=_0x2f4e12[_0x4e5ce8(0x1d4)]||0x0;this['_skillContainerStack']=[],_0x2d0c80&&(this['refresh'](),this[_0x4e5ce8(0x1b6)](_0x293b37));},Window_SkillList[_0x466303(0x1d1)][_0x466303(0x17e)]=function(){const _0x3b6c4b=_0x466303;return this[_0x3b6c4b(0x18b)]['length']>0x0;},VisuMZ['SkillContainers'][_0x466303(0x199)]=Window_SkillList[_0x466303(0x1d1)][_0x466303(0x17b)],Window_SkillList[_0x466303(0x1d1)]['makeItemList']=function(){const _0x4a0653=_0x466303;this[_0x4a0653(0x17e)]()?this[_0x4a0653(0x183)]():VisuMZ['SkillContainers'][_0x4a0653(0x199)]['call'](this);},VisuMZ[_0x466303(0x179)][_0x466303(0x18a)]=Window_SkillList[_0x466303(0x1d1)]['includes'],Window_SkillList[_0x466303(0x1d1)][_0x466303(0x1c9)]=function(_0x3b115f){const _0xcf8c28=_0x466303;if(_0x3b115f&&DataManager['isSkillContainer'](_0x3b115f)){const _0x5e67aa=DataManager['getSkillContainerList'](this[_0xcf8c28(0x1bf)],_0x3b115f);if(_0x5e67aa[_0xcf8c28(0x19f)]<=0x0)return![];}return VisuMZ[_0xcf8c28(0x179)][_0xcf8c28(0x18a)][_0xcf8c28(0x196)](this,_0x3b115f);},Window_SkillList[_0x466303(0x1d1)][_0x466303(0x183)]=function(){const _0x151278=_0x466303,_0x1a9bdd=this[_0x151278(0x18b)][this[_0x151278(0x18b)][_0x151278(0x19f)]-0x1],_0xae68bd=_0x1a9bdd[_0x151278(0x180)],_0x4f0800=DataManager['getSkillContainerList'](this[_0x151278(0x1bf)],_0xae68bd);this[_0x151278(0x1bb)]=_0x4f0800[_0x151278(0x187)](_0xbe3c3=>$dataSkills[_0xbe3c3])[_0x151278(0x1d5)](_0x5bd0ab=>!!_0x5bd0ab);},VisuMZ[_0x466303(0x179)][_0x466303(0x193)]=Window_SkillList['prototype'][_0x466303(0x1b9)],Window_SkillList[_0x466303(0x1d1)][_0x466303(0x1b9)]=function(_0xc235f4,_0x4a429a,_0x365dbb,_0x198e99){const _0x467fe2=_0x466303;DataManager[_0x467fe2(0x1c7)](_0xc235f4)?this[_0x467fe2(0x18f)](_0xc235f4,_0x4a429a,_0x365dbb,_0x198e99):VisuMZ[_0x467fe2(0x179)]['Window_SkillList_drawSkillCost'][_0x467fe2(0x196)](this,_0xc235f4,_0x4a429a,_0x365dbb,_0x198e99);},Window_SkillList[_0x466303(0x1d1)][_0x466303(0x18f)]=function(_0x3c2df8,_0xfee88d,_0x52889c,_0x11a96b){const _0x16eef4=_0x466303;if(!_0x3c2df8)return;this[_0x16eef4(0x19d)]();const _0x341b4f=TextManager[_0x16eef4(0x1ae)],_0x6c2ad5=this[_0x16eef4(0x1c2)](_0x341b4f)[_0x16eef4(0x1cd)];_0xfee88d+=_0x11a96b-_0x6c2ad5,this['drawTextEx'](_0x341b4f,_0xfee88d,_0x52889c,_0x6c2ad5),this[_0x16eef4(0x19d)]();};Imported['VisuMZ_1_BattleCore']&&(VisuMZ[_0x466303(0x179)][_0x466303(0x177)]=Window_ActorCommand[_0x466303(0x1d1)]['canAddSkillCommand'],Window_ActorCommand[_0x466303(0x1d1)][_0x466303(0x198)]=function(_0x331f07){const _0x189020=_0x466303;return DataManager[_0x189020(0x1c7)](_0x331f07)?![]:VisuMZ[_0x189020(0x179)][_0x189020(0x177)]['call'](this,_0x331f07);});;