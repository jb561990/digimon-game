//=============================================================================
// VisuStella MZ - Party System
// VisuMZ_2_PartySystem.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_PartySystem = true;

var VisuMZ = VisuMZ || {};
VisuMZ.PartySystem = VisuMZ.PartySystem || {};
VisuMZ.PartySystem.version = 1.17;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.17] [PartySystem]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Party_System_VisuStella_MZ
 * @orderAfter VisuMZ_0_CoreEngine
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * RPG Maker MZ only gives game projects the ability to switch party members
 * within the main menu and nothing more. There's no inherent functionality to
 * lock party members, make party members required, and/or give players the
 * ability to switch party members mid-battle.
 *
 * This plugin will add in all of those functions as well as a dedicated scene
 * for switching party members. Party switching will allow party members to be
 * removed, swapped, and sorted. Through the usage of Plugin Commands, party
 * members can also be locked and/or required for party presence.
 *
 * Those using the VisuStella MZ Battle Core will also have access to features
 * in this plugin that aren't available otherwise. These features give players
 * the functionality to switch out the whole party lineup mid-battle and/or
 * individual party member switching.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Custom scene dedicated to party management.
 * * Change the maximum number of party members that can participate in battle.
 * * Plugin Commands to lock party members.
 * * Plugin Commands to make certain party members required.
 * * Added functionality with Battle Core to switch party members mid-battle.
 * * This comes in the form of changing either the whole party at once.
 * * Or switching individual members out one at a time.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Main Menu Formation Command
 *
 * - This command is now changed to send the player to Scene_Party for the
 * player to have a dedicated scene for changing the party.
 *
 * ---
 *
 * Battle Members Array
 *
 * - Previously, the battle members are decided by which actors are lined up
 * first in the party roster. This has been changed to give players the freedom
 * to have a party size less than the maximum. This change is made by changing
 * the way the battle members are determined by using a new array. However, any
 * and all functions utilize the $gameParty.battleMembers() function will still
 * behave as normal.
 *
 * ---
 *
 * Formation Change OK Function
 *
 * - RPG Maker MZ did not do anything with the Game_Actor.isFormationChangeOk
 * function so this plugin overwrote it completely to allow for the new
 * lock and require features to work.
 *
 * ---
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 *
 * VisuMZ_1_BattleCore
 *
 * - If the VisuStella MZ Battle Core plugin is present, players are able to 
 * access party switching functionality mid-battle at will. This can be in the
 * form of switching out the entire active party roster at once or individually
 * for each actor.
 *
 * - Switching Entire Rosters: This can be done by going into this plugin's
 * Plugin Parameters => General => Party Command Window => Add Party Command.
 * If the Party Command Window is accessible, the player will be able to see
 * the option between 'Auto Battle' and 'Options'.
 *
 * - Individual Member Switching: This requires going to VisuMZ_1_BattleCore's
 * Plugin Parameters => Actor Command Window => Battle Commands => Command List
 * and add in the "party" option. The "party" option can also be added to the
 * <Battle Commands> notetag.
 *
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins or specific
 * features. This section will highlight the main plugins/features that will
 * not be compatible with this plugin or put focus on how the make certain
 * features compatible.
 *
 * ---
 * 
 * VisuMZ_2_BattleSystemOTB
 * 
 * With Battle System - OTB, the player cannot change entire parties at once
 * from the Party Command Window. The feature will be unaccessible while
 * Order Turn Battle is in play. However, the player can still change party
 * members through the Actor Command Window by having actors replace other
 * actors. Party changing is also available through battle events, Common
 * Events, and script calls.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Party Plugin Commands ===
 * 
 * ---
 *
 * Party: Call Party Scene
 * - Calls the party changing scene.
 *
 * ---
 *
 * Party: Change Max Battle Members
 * - Changes the number of max battle members possible.
 * - Cannot be use mid-battle.
 *
 *   Max Members:
 *   - Changes the number of max battle members possible.
 *   - Use 0 for the game's default number.
 *
 * ---
 *
 * Party: Lock/Unlock Member(s)
 * - Allows you to lock/unlock a party member.
 * - Locked actors cannot change their party position.
 *
 *   Actor ID(s):
 *   - Select which actor(s) to lock/unlock.
 *   - Locked actors cannot change their party position.
 *
 *   Lock?:
 *   - Lock the selected actor(s)?
 *
 * ---
 * 
 * Party: Move Actor(s) to Active
 * - Map Only.
 * - Moves an actor to the active party if there is room.
 * - The actor needs to have joined the party.
 * 
 *   Actor ID(s):
 *   - Select which actor(s) to move to the active party if there is room.
 * 
 * ---
 * 
 * Party: Move Actor(s) to Reserve
 * - Map Only.
 * - Moves an actor to the reserve party.
 * - Must be 1 actor left.
 * - The actor needs to have joined the party.
 * 
 *   Actor ID(s):
 *   - Select which actor(s) to move to the reserve party.
 * 
 * ---
 * 
 * Party: Move Party Index to Reserve
 * - Map only.
 * - Moves an actor in a specific party index to reserve.
 * - Must be 1 actor left.
 * 
 *   Index:
 *   - Type in which index to move.
 *   - Index values start at 0.
 *   - You may use JavaScript code.
 * 
 * ---
 * 
 * Party: Move Random Reserve to Active
 * - Map only.
 * - Moves a random actor from the reserve party to active.
 * - Must be enough space in active party.
 * 
 * ---
 *
 * Party: Require Member(s)
 * - Allows you to require/free a party member.
 * - Required actors must be in the party to exit the scene.
 *
 *   Actor ID(s):
 *   - Select which actor(s) to require/free.
 *   - Required actors must be in the party to exit the scene.
 *
 *   Require?:
 *   - Make the selected actor(s) required?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * These Plugin Parameters control the overall behaviors pertaining to the
 * Party System added with this plugin. These behaviors range from the maximum
 * number of members that can participate in battle to the availability of the
 * party switching mechanics.
 *
 * ---
 *
 * General
 * 
 *   Max Battle Members:
 *   - Maximum number of battle members.
 *
 * ---
 *
 * Party Scene
 * 
 *   Add Remove Command:
 *   - Add the 'Remove' command to the party scene?
 * 
 *   Locked Member Icon:
 *   - Icon used for a locked party member.
 * 
 *   Requiured Member Icon:
 *   - Icon used for a required party member.
 *
 * ---
 *
 * Party Command Window
 * - These require VisuMZ_1_BattleCore!
 * 
 *   Add Party Command:
 *   - Add the 'Party' command to the Party Command Window?
 * 
 *   Command Cooldown:
 *   - Cooldown (in turns) for this command to be available again.
 *
 * ---
 *
 * Actor Command Window
 * - These require VisuMZ_1_BattleCore!
 * 
 *   Add Switch Command:
 *   - Add the 'Switch' command to the Actor Command Window?
 * 
 *   Command Cooldown:
 *   - Cooldown (in turns) for this command to be available again.
 * 
 *   Switch Out Animation?:
 *   - Show the sprites switching out when using individual party
 *     member switching?
 * 
 *   TPB: Immediate Action:
 *   - Allow actors to immediate act upon switching in for TPB battle systems?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Vocabulary Settings
 * ============================================================================
 *
 * These Plugin Parameters control the text that you see in-game related to the
 * Party System plugin.
 *
 * ---
 *
 * General
 * 
 *   Active Party:
 *   - Vocabulary used to represent the Active Party.
 * 
 *   Reserve Party:
 *   - Vocabulary used to represent the Reserve Party.
 * 
 *   Status:
 *   - Vocabulary used to represent the Status Window.
 * 
 *   Show Back Rectangles?:
 *   - Show back rectangles of darker colors to display information better?
 * 
 *     Back Rectangle Color:
 *     - Use #rrggbb for custom colors or regular numbers for text colors
 *       from the Window Skin.
 *
 * ---
 *
 * Party Scene > Windows
 * 
 *   Empty:
 *   - For the party and status windows when no actor is selected.
 * 
 *   Remove:
 *   - For the remove option.
 *
 * ---
 *
 * Party Scene > Button Assist
 * 
 *   Swap Positions:
 *   - Button assist text for the page up/down commands.
 *   - Requires VisuMZ_0_CoreEngine!
 * 
 *   Remove:
 *   - Button assist text for the removal command.
 *   - Requires VisuMZ_0_CoreEngine!
 * 
 *   Sort:
 *   - Button assist text for the sort command.
 *   - Requires VisuMZ_0_CoreEngine!
 * 
 *   Swap In:
 *   - Button assist text for swapping in actors.
 *   - Requires VisuMZ_0_CoreEngine!
 * 
 *   Swap Out:
 *   - Button assist text for swapping out actors.
 *   - Requires VisuMZ_0_CoreEngine!
 *
 * ---
 *
 * Battle Scene
 * 
 *   Party Command:
 *   - Command text for entering Party Scene.
 *   - Requires VisuMZ_1_BattleCore!
 * 
 *   Help: Formation:
 *   - Help text for Formation command.
 *   - Requires VisuMZ_1_BattleCore!
 * 
 *   Queue Message:
 *   - Message to say the Party Scene is queued.
 *   - Requires VisuMZ_1_BattleCore!
 * 
 *   Switch Command:
 *   - Command text for switching out members.
 *   - Requires VisuMZ_1_BattleCore!
 * 
 *   Help: Switch:
 *   - Help text for Switch command.
 *   - Requires VisuMZ_1_BattleCore!
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Background Settings
 * ============================================================================
 *
 * Background settings for Scene_Party.
 *
 * ---
 *
 * Background Settings
 * 
 *   Snapshop Opacity:
 *   - Snapshot opacity for the scene.
 * 
 *   Background 1:
 *   - Filename used for the bottom background image.
 *   - Leave empty if you don't wish to use one.
 * 
 *   Background 2:
 *   - Filename used for the upper background image.
 *   - Leave empty if you don't wish to use one.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Window Settings
 * ============================================================================
 *
 * If you don't like the locations of the windows in Scene_Party, change them
 * up with these Plugin Parameters, provided that you have an understanding of
 * JavaScript code.
 *
 * ---
 *
 * Active Party Label
 * Active Party Window
 * Reserve Party Label
 * Reserve Party Window
 * Status Label
 * Status Window
 * Battle Switch Window
 * 
 *   Background Type:
 *   - Select background type for this window.
 * 
 *   Columns:
 *   - Available only for the Reserve Party Window.
 *   - How many columns do you want there to be for the window?
 * 
 *   Actor Graphic:
 *   - Available only for Active Party Window and Reserve Party Window.
 *   - Choose how the actor graphics appear in the specific windows.
 *     - Face
 *     - Map Sprite
 *     - Sideview Battler (Requires VisuMZ_1_MainMenuCore)
 * 
 *     Map Sprite:
 *     Sideview Battler:
 * 
 *       Offset X:
 *       Offset Y:
 *       - If showing map sprites, offset the x or y coordinates.
 * 
 *   JS: X, Y, W, H:
 *   - Code used to determine the dimensions for this window.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.17: March 26, 2021
 * * Documentation Update!
 * ** Added "VisuStella MZ Compatibility" section for detailed compatibility
 *    explanations with the VisuMZ_2_BattleSystemOTB plugin.
 * 
 * Version 1.16: March 19, 2021
 * * Compatibility Update
 * ** Added compatibility functionality for future plugins.
 * 
 * Version 1.15: March 5, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features
 * ** New Plugin Parameter added by Irina:
 * *** Plugin Parameters > Gneral > Battle Scene > Battle Party Icon
 * **** For some reason, we never had a setting that lets you change the party
 *      icon. Well, now there is!
 * 
 * Version 1.14: February 5, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features!
 * ** New Plugin Commands added by Arisu!
 * *** Party: Move Party Index to Reserve
 * **** Moves an actor in a specific party index to reserve.
 *      Map only. Must be 1 actor left. You may use code.
 * *** Party: Move Random Reserve to Active
 * **** Moves a random actor from the reserve party to active.
 *      Map only. Must be enough space in active party.
 * 
 * Version 1.13: January 29, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features!
 * ** New Plugin Commands added by Arisu!
 * *** Party: Move Actor(s) to Active
 * **** Map only. Moves an actor to the active party if there is room.
 * *** Party: Move Actor(s) to Reserve
 * **** Map only. Moves an actor to the reserve party.
 * 
 * Version 1.12: January 15, 2021
 * * Bug Fixes!
 * ** For battle testing, if the number of battle test members exceeds the
 *    maximum battle member slots, trim them until they match. Fix by Olivia.
 * 
 * Version 1.11: January 1, 2021
 * * Compatibility Update
 * ** Plugins should be more compatible with one another.
 * 
 * Version 1.10: December 25, 2020
 * * Compatibility Update
 * ** Plugins should be more compatible with one another.
 * 
 * Version 1.09: December 18, 2020
 * * Bug Fixes!
 * ** Removing party members in the active party by event command will now be
 *    properly removed from the party. Fix made by Yanfly.
 * 
 * Version 1.08: December 4, 2020
 * * Bug Fixes!
 * ** With TPB battle systems, after switching out party members, the battle
 *    system will no longer carry over any previous active battle members in
 *    the command window. Fix made by Yanfly.
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * 
 * Version 1.07: November 22, 2020
 * * Bug Fixes!
 * ** With Active TPB, switching out a party member mid-action is no longer
 *    possible to prevent bugs. Intead, there party switching action will be
 *    queued and take effect after the action has been completed. Fix made by
 *    Yanfly.
 * * Compatibility Update!
 * ** Game_Party.swapOrder function now works with this plugin. However, keep
 *    in mind that due to how this party system plugin allows you have empty
 *    slots in the active battle party, this function will fill in the empty
 *    slots upon usage. Update made by Yanfly.
 *
 * Version 1.06: November 1, 2020
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * 
 * Version 1.05: October 25, 2020
 * * Bug Fixes!
 * ** Plugin Command "Party: Change Max Battle Members" now works again.
 *    Fix made by Arisu.
 *
 * Version 1.04: October 18, 2020
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * 
 * Version 1.03: October 11, 2020
 * * Bug Fixes!
 * ** Adding party members during battle through the party window command will
 *    no longer cause crashes after they input an action. Fix made by Yanfly.
 * 
 * Version 1.02: October 4, 2020
 * * Bug Fixes!
 * ** Adding party members during test play should now work again.
 *    Fix made by Irina.
 * ** Changing party members mid-battle through the actor command should now
 *    refresh the party followers afterwards. Fix made by Yanfly.
 * * New Features!
 * ** New Plugin Parameter added by Arisu!
 * *** General > Party Command Window > TPB: Immediate Action
 * **** Allow actors to immediate act upon switching in for TPB battle systems?
 * 
 * Version 1.01: September 27, 2020
 * * Bug Fixes!
 * ** When switching actors with states, buffs, and/or debuffs already applied,
 *    the state icons found in the status window will now switch over properly,
 *    too. Fix made by Arisu.
 *
 * Version 1.00: September 7, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command CallPartyScene
 * @text Party: Call Party Scene
 * @desc Calls the party changing scene.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ChangeMaxBattleMembers
 * @text Party: Change Max Battle Members
 * @desc Changes the number of max battle members possible.
 * Cannot be use mid-battle.
 *
 * @arg Value:eval
 * @text Max Members
 * @desc Changes the number of max battle members possible.
 * Use 0 for the game's default number.
 * @default 4
 *
 * @ --------------------------------------------------------------------------
 *
 * @command LockPartyMembers
 * @text Party: Lock/Unlock Member(s)
 * @desc Allows you to lock/unlock a party member.
 * Locked actors cannot change their party position.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which actor(s) to lock/unlock.
 * Locked actors cannot change their party position.
 * @default ["1"]
 * 
 * @arg Lock:eval
 * @text Lock?
 * @type boolean
 * @on Lock
 * @off Unlock
 * @desc Lock the selected actor(s)?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command MoveActorsToActive
 * @text Party: Move Actor(s) to Active
 * @desc Moves an actor to the active party if there is room.
 * Map only. The actor needs to have joined the party.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which actor(s) to move to the active party if there is room.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command MoveActorsToReserve
 * @text Party: Move Actor(s) to Reserve
 * @desc Moves an actor to the reserve party. Must be 1 actor left.
 * Map only. The actor needs to have joined the party.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which actor(s) to move to the reserve party.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command MovePartyIndexToReserve
 * @text Party: Move Party Index to Reserve
 * @desc Moves an actor in a specific party index to reserve.
 * Map only. Must be 1 actor left.
 *
 * @arg Index:eval
 * @text Party Index
 * @desc Type in which index to move. Index values start at 0.
 * You may use JavaScript code.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command MoveRandomToActive
 * @text Party: Move Random Reserve to Active
 * @desc Moves a random actor from the reserve party to active.
 * Map only. Must be enough space in active party.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command RequirePartyMembers
 * @text Party: Require Member(s)
 * @desc Allows you to require/free a party member.
 * Required actors must be in the party to exit the scene.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which actor(s) to require/free.
 * Required actors must be in the party to exit the scene.
 * @default ["1"]
 * 
 * @arg Require:eval
 * @text Require?
 * @type boolean
 * @on Require
 * @off Don't Require
 * @desc Make the selected actor(s) required?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param PartySystem
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param General:struct
 * @text General Settings
 * @type struct<General>
 * @desc General settings pertaining to Party-related mechanics.
 * @default {"General":"","MaxBattleMembers:num":"4","PartyScene":"","AddRemoveCmd:eval":"true","LockIcon:num":"195","RequireIcon:num":"87","DrawBackRect:eval":"true","BackRectColor:str":"19","PartyCmdWin":"","PartyCmdWinAddParty:eval":"false","PartyCmdCooldown:num":"1","tpbImmediateAction:eval":"true","ActorCmdWin":"","ActorCmdWinAddParty:eval":"true","ActorCmdCooldown:num":"1","SwitchOutAnimation:eval":"true"}
 *
 * @param Vocab:struct
 * @text Vocabulary Settings
 * @type struct<Vocab>
 * @desc These settings let you adjust the text displayed for this plugin.
 * @default {"General":"","ActiveParty:str":"Active Party","ReserveParty:str":"Reserve Party","Status:str":"Status","PartyScene":"","Windows":"","Empty:str":"- Empty -","Remove:str":"Remove","ButtonAssist":"","AssistSwapPosition:str":"Quick Swap","AssistRemove:str":"Remove","AssistSort:str":"Sort","AssistSwapIn:str":"Swap In","AssistSwapOut:str":"Swap Out","BattleScene":"","BattlePartyCmd:str":"Party","BattleHelpFormation:json":"\"Change up your party formation.\"","QueuePartyScene:str":"%1 Menu queued after action is complete.","BattleSwitchOut:str":"Switch","BattleHelpSwitch:json":"\"Switch out this party member with another.\""}
 *
 * @param BgSettings:struct
 * @text Background Settings
 * @type struct<BgSettings>
 * @desc Background settings for Scene_Party.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Window:struct
 * @text Window Settings
 * @type struct<Window>
 * @desc These settings let you control how the windows appear in Scene_Party.
 * @default {"ActivePartyLabel":"","ActivePartyLabelBgType:num":"0","ActivePartyLabelRect:func":"\"const wx = 0;\\nconst wy = this.mainAreaTop();\\nconst ww = Graphics.boxWidth;\\nconst wh = Window_Base.prototype.lineHeight();\\nreturn new Rectangle(wx, wy, ww, wh);\"","ActivePartyWindow":"","ActivePartyWindowBgType:num":"0","ActivePartyGraphic:str":"face","ActivePartyMapSprite":"","ActiveSpriteOffsetX:num":"0","ActiveSpriteOffsetY:num":"4","ActivePartySvBattler":"","ActiveBattlerOffsetX:num":"0","ActiveBattlerOffsetY:num":"4","ActivePartyWindowRect:func":"\"const wx = 0;\\nconst wy = this._activePartyLabel.y + this._activePartyLabel.height;\\nconst ww = Graphics.boxWidth;\\nconst wh = ImageManager.faceHeight + $gameSystem.windowPadding() * 2 + 2;\\nreturn new Rectangle(wx, wy, ww, wh);\"","ReservePartyLabel":"","ReservePartyLabelBgType:num":"0","ReservePartyLabelRect:func":"\"const ww = Math.max(240, Math.min(Graphics.boxWidth - 576, Math.round(Graphics.boxWidth / 2)));\\nconst wx = this.isRightInputMode() ? (Graphics.boxWidth - ww) : 0;\\nconst wy = this._activePartyWindow.y + this._activePartyWindow.height;\\nconst wh = Window_Base.prototype.lineHeight();\\nreturn new Rectangle(wx, wy, ww, wh);\"","ReservePartyWindow":"","ReservePartyWindowBgType:num":"0","ReserveItemThickness:num":"2","ReservePartyGraphic:str":"face","ReservePartyMapSprite":"","ReserveSpriteOffsetX:num":"24","ReserveSpriteOffsetY:num":"4","ReservePartySvBattler":"","ReserveBattlerOffsetX:num":"48","ReserveBattlerOffsetY:num":"4","ReservePartyWindowRect:func":"\"const ww = this._reservePartyLabel.width;\\nconst wx = this._reservePartyLabel.x;\\nconst wy = this._reservePartyLabel.y + this._reservePartyLabel.height;\\nconst wh = this.mainAreaHeight() - this._reservePartyLabel.height - this._activePartyWindow.height - this._activePartyLabel.height;\\nreturn new Rectangle(wx, wy, ww, wh);\"","StatusLabel":"","StatusLabelBgType:num":"0","StatusLabelRect:func":"\"const ww = Graphics.boxWidth - this._reservePartyLabel.width;\\nconst wx = this.isRightInputMode() ? 0 : (Graphics.boxWidth - ww);\\nconst wy = this._activePartyWindow.y + this._activePartyWindow.height;\\nconst wh = Window_Base.prototype.lineHeight();\\nreturn new Rectangle(wx, wy, ww, wh);\"","StatusWindow":"","StatusWindowBgType:num":"0","StatusWindowDraw:func":"\"// Draw Empty\\nif (!this._actor) {\\n    this.drawItemDarkRect(0, 0, this.innerWidth, this.innerHeight);\\n    const y = Math.round((this.innerHeight - this.lineHeight()) / 2);\\n    this.changeTextColor(ColorManager.systemColor());\\n    this.drawText(TextManager.emptyPartyMember, 0, y, this.innerWidth, 'center');\\n    return;\\n}\\n\\n// Draw Face and Simple Status\\nthis.drawActorFace(this._actor, 1, 0, ImageManager.faceWidth, ImageManager.faceHeight);\\nthis.drawActorSimpleStatus(this._actor, ImageManager.faceWidth + 36, 0);\\n\\n// Declare Constants\\nconst lineHeight = this.lineHeight();\\nconst params = this.actorParams();\\nconst paramWidth = Math.round(this.innerWidth / 2);\\nconst paramHeight = Math.ceil(params.length / 2) * lineHeight;\\nconst baseX = 0;\\nlet x = 0;\\nlet y = ImageManager.faceHeight + lineHeight / 2;\\n\\n// Draw Parameters\\nfor (const param of params) {\\n    this.drawItemDarkRect(x, y, paramWidth, lineHeight);\\n    this.drawParamName(param, x, y, paramWidth);\\n    this.drawParamValue(param, x, y, paramWidth);\\n\\n    if (x === baseX) {\\n        x += paramWidth;\\n    } else {\\n        x = baseX;\\n        y += lineHeight;\\n    }\\n}\"","StatusWindowRect:func":"\"const ww = this._statusPartyLabel.width;\\nconst wx = this.isRightInputMode() ? 0 : (Graphics.boxWidth - ww);\\nconst wy = this._reservePartyWindow.y;\\nconst wh = this._reservePartyWindow.height;\\nreturn new Rectangle(wx, wy, ww, wh);\"","BattleSwitchWindow":"","BattleSwitchWindowBgType:num":"0","BattleSwitchWindowRect:func":"\"const padding = $gameSystem.windowPadding() * 2;\\nlet ww = 516 + padding;\\nlet wh = Window_PartyBattleSwitch.prototype.itemHeight() * 4 + padding;\\nlet wx = Math.round(Graphics.boxWidth - ww) / 2;\\nlet wy = Math.round(Graphics.boxHeight - wh - this._statusWindow.height) / 2;\\nwy = wy.clamp(0, Graphics.boxHeight - wh - this._statusWindow.height);\\nreturn new Rectangle(wx, wy, ww, wh);\""}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * General Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~General:
 *
 * @param General
 *
 * @param MaxBattleMembers:num
 * @text Max Battle Members
 * @parent General
 * @type number
 * @min 1
 * @desc Maximum number of battle members.
 * @default 4
 *
 * @param BattleScene
 * @text Battle Scene
 *
 * @param BattlePartyIcon:num
 * @text Battle Party Icon
 * @parent BattleScene
 * @desc Icon used for changing party members.
 * @default 75
 *
 * @param PartyScene
 * @text Party Scene
 *
 * @param AddRemoveCmd:eval
 * @text Add Remove Command
 * @parent PartyScene
 * @type boolean
 * @on Add Command
 * @off Don't Add
 * @desc Add the 'Remove' command to the party scene?
 * @default true
 *
 * @param LockIcon:num
 * @text Locked Member Icon
 * @parent PartyScene
 * @desc Icon used for a locked party member.
 * @default 195
 *
 * @param RequireIcon:num
 * @text Requiured Member Icon
 * @parent PartyScene
 * @desc Icon used for a required party member.
 * @default 87
 *
 * @param DrawBackRect:eval
 * @text Show Back Rectangles?
 * @parent PartyScene
 * @type boolean
 * @on Draw
 * @off Don't Draw
 * @desc Show back rectangles of darker colors to display information better?
 * @default true
 *
 * @param BackRectColor:str
 * @text Back Rectangle Color
 * @parent DrawBackRect:eval
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param PartyCmdWin
 * @text Party Command Window
 *
 * @param PartyCmdWinAddParty:eval
 * @text Add Party Command
 * @parent PartyCmdWin
 * @type boolean
 * @on Add Command
 * @off Don't Add
 * @desc Add the 'Party' command to the Party Command Window?
 * @default false
 *
 * @param PartyCmdCooldown:num
 * @text Command Cooldown
 * @parent PartyCmdWin
 * @desc Cooldown (in turns) for this command to be available again.
 * @default 1
 *
 * @param ActorCmdWin
 * @text Actor Command Window
 *
 * @param ActorCmdWinAddParty:eval
 * @text Add Switch Command
 * @parent ActorCmdWin
 * @type boolean
 * @on Add Command
 * @off Don't Add
 * @desc Add the 'Switch' command to the Actor Command Window?
 * @default true
 *
 * @param ActorCmdCooldown:num
 * @text Command Cooldown
 * @parent ActorCmdWin
 * @desc Cooldown (in turns) for this command to be available again.
 * @default 1
 *
 * @param SwitchOutAnimation:eval
 * @text Switch Out Animation?
 * @parent ActorCmdWin
 * @type boolean
 * @on Show
 * @off Don't
 * @desc Show the sprites switching out when using individual party member switching?
 * @default true
 *
 * @param tpbImmediateAction:eval
 * @text TPB: Immediate Action
 * @parent ActorCmdWin
 * @type boolean
 * @on Immediate Action
 * @off Empty Gauge
 * @desc Allow actors to immediate act upon switching in for TPB battle systems?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Vocabulary Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Vocab:
 *
 * @param General
 *
 * @param ActiveParty:str
 * @text Active Party
 * @parent General
 * @desc Vocabulary used to represent the Active Party.
 * @default Active Party
 *
 * @param ReserveParty:str
 * @text Reserve Party
 * @parent General
 * @desc Vocabulary used to represent the Reserve Party.
 * @default Reserve Party
 *
 * @param Status:str
 * @text Status
 * @parent General
 * @desc Vocabulary used to represent the Status Window.
 * @default Status
 *
 * @param PartyScene
 * @text Party Scene
 *
 * @param Windows
 * @parent PartyScene
 *
 * @param Empty:str
 * @text Empty
 * @parent Windows
 * @desc For the party and status windows when no actor is selected.
 * @default - Empty -
 *
 * @param Remove:str
 * @text Remove
 * @parent Windows
 * @desc For the remove option.
 * @default Remove
 *
 * @param ButtonAssist
 * @text Button Assist
 * @parent PartyScene
 *
 * @param AssistSwapPosition:str
 * @text Swap Positions
 * @parent ButtonAssist
 * @desc Button assist text for the page up/down commands.
 * Requires VisuMZ_0_CoreEngine!
 * @default Quick Swap
 *
 * @param AssistRemove:str
 * @text Remove
 * @parent ButtonAssist
 * @desc Button assist text for the removal command.
 * Requires VisuMZ_0_CoreEngine!
 * @default Remove
 *
 * @param AssistSort:str
 * @text Sort
 * @parent ButtonAssist
 * @desc Button assist text for the sort command.
 * Requires VisuMZ_0_CoreEngine!
 * @default Sort
 *
 * @param AssistSwapIn:str
 * @text Swap In
 * @parent ButtonAssist
 * @desc Button assist text for swapping in actors.
 * Requires VisuMZ_0_CoreEngine!
 * @default Swap In
 *
 * @param AssistSwapOut:str
 * @text Swap Out
 * @parent ButtonAssist
 * @desc Button assist text for swapping out actors.
 * Requires VisuMZ_0_CoreEngine!
 * @default Swap Out
 *
 * @param BattleScene
 * @text Battle Scene
 *
 * @param BattlePartyCmd:str
 * @text Party Command
 * @parent BattleScene
 * @desc Command text for entering Party Scene.
 * Requires VisuMZ_1_BattleCore!
 * @default Party
 *
 * @param BattleHelpFormation:json
 * @text Help: Formation
 * @parent BattlePartyCmd:str
 * @type note
 * @desc Help text for Formation command.
 * Requires VisuMZ_1_BattleCore!
 * @default "Change up your party formation."
 *
 * @param QueuePartyScene:str
 * @text Queue Message
 * @parent BattlePartyCmd:str
 * @desc Message to say the Party Scene is queued.
 * Requires VisuMZ_1_BattleCore!
 * @default %1 Menu queued after action is complete.
 *
 * @param BattleSwitchOut:str
 * @text Switch Command
 * @parent BattleScene
 * @desc Command text for switching out members.
 * Requires VisuMZ_1_BattleCore!
 * @default Switch
 *
 * @param BattleHelpSwitch:json
 * @text Help: Switch
 * @parent BattleSwitchOut:str
 * @type note
 * @desc Help text for Switch command.
 * Requires VisuMZ_1_BattleCore!
 * @default "Switch out this party member with another."
 *
 */
/* ----------------------------------------------------------------------------
 * Background Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~BgSettings:
 *
 * @param SnapshotOpacity:num
 * @text Snapshop Opacity
 * @type number
 * @min 0
 * @max 255
 * @desc Snapshot opacity for the scene.
 * @default 192
 *
 * @param BgFilename1:str
 * @text Background 1
 * @type file
 * @dir img/titles1/
 * @desc Filename used for the bottom background image.
 * Leave empty if you don't wish to use one.
 * @default 
 *
 * @param BgFilename2:str
 * @text Background 2
 * @type file
 * @dir img/titles2/
 * @desc Filename used for the upper background image.
 * Leave empty if you don't wish to use one.
 * @default 
 *
 */
/* ----------------------------------------------------------------------------
 * Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Window:
 *
 * @param ActivePartyLabel
 * @text Active Party Label
 *
 * @param ActivePartyLabelBgType:num
 * @text Background Type
 * @parent ActivePartyLabel
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ActivePartyLabelRect:func
 * @text JS: X, Y, W, H
 * @parent ActivePartyLabel
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const wx = 0;\nconst wy = this.mainAreaTop();\nconst ww = Graphics.boxWidth;\nconst wh = Window_Base.prototype.lineHeight();\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param ActivePartyWindow
 * @text Active Party Window
 *
 * @param ActivePartyWindowBgType:num
 * @text Background Type
 * @parent ActivePartyWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ActivePartyGraphic:str
 * @text Actor Graphic
 * @parent ActivePartyWindow
 * @type select
 * @option None
 * @value none
 * @option Face
 * @value face
 * @option Map Sprite
 * @value sprite
 * @option Sideview Battler (Requires VisuMZ_1_MainMenuCore)
 * @value svbattler
 * @desc Choose how the actor graphics appear in the active party menu.
 * @default face
 *
 * @param ActivePartyMapSprite
 * @text Map Sprite
 * @parent ActivePartyGraphic:str
 *
 * @param ActiveSpriteOffsetX:num
 * @text Offset X
 * @parent ActivePartyMapSprite
 * @desc If showing map sprites, offset the x coordinate here from center.
 * @default 0
 *
 * @param ActiveSpriteOffsetY:num
 * @text Offset Y
 * @parent ActivePartyMapSprite
 * @desc If showing map sprites, offset the y coordinate here from bottom.
 * @default 4
 *
 * @param ActivePartySvBattler
 * @text Sideview Battler
 * @parent ActivePartyGraphic:str
 *
 * @param ActiveBattlerOffsetX:num
 * @text Offset X
 * @parent ActivePartySvBattler
 * @desc If showing sideview battlers, offset the x coordinate here from center.
 * @default 0
 *
 * @param ActiveBattlerOffsetY:num
 * @text Offset Y
 * @parent ActivePartySvBattler
 * @desc If showing sideview battlers, offset the y coordinate here from bottom.
 * @default 4
 *
 * @param ActivePartyWindowRect:func
 * @text JS: X, Y, W, H
 * @parent ActivePartyWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const wx = 0;\nconst wy = this._activePartyLabel.y + this._activePartyLabel.height;\nconst ww = Graphics.boxWidth;\nconst wh = ImageManager.faceHeight + $gameSystem.windowPadding() * 2 + 2;\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param ReservePartyLabel
 * @text Reserve Party Label
 *
 * @param ReservePartyLabelBgType:num
 * @text Background Type
 * @parent ReservePartyLabel
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ReservePartyLabelRect:func
 * @text JS: X, Y, W, H
 * @parent ReservePartyLabel
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const ww = Math.max(240, Math.min(Graphics.boxWidth - 576, Math.round(Graphics.boxWidth / 2)));\nconst wx = this.isRightInputMode() ? (Graphics.boxWidth - ww) : 0;\nconst wy = this._activePartyWindow.y + this._activePartyWindow.height;\nconst wh = Window_Base.prototype.lineHeight();\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param ReservePartyWindow
 * @text Reserve Party Window
 *
 * @param ReservePartyWindowBgType:num
 * @text Background Type
 * @parent ReservePartyWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ReserveCol:num
 * @text Columns
 * @parent ReservePartyWindow
 * @type number
 * @min 1
 * @desc How many columns do you want there to be for the window?
 * @default 1
 *
 * @param ReserveItemThickness:num
 * @text Row Thickness
 * @parent ReservePartyWindow
 * @type number
 * @min 1
 * @desc How many rows thick do you want selectable items to be?
 * @default 2
 *
 * @param ReservePartyGraphic:str
 * @text Actor Graphic
 * @parent ReservePartyWindow
 * @type select
 * @option None
 * @value none
 * @option Face
 * @value face
 * @option Map Sprite
 * @value sprite
 * @option Sideview Battler (Requires VisuMZ_1_MainMenuCore)
 * @value svbattler
 * @desc Choose how the actor graphics appear in the reserve party menu.
 * @default face
 *
 * @param ReservePartyMapSprite
 * @text Map Sprite
 * @parent ReservePartyGraphic:str
 *
 * @param ReserveSpriteOffsetX:num
 * @text Offset X
 * @parent ReservePartyMapSprite
 * @desc If showing map sprites, offset the x coordinate here from left.
 * @default 24
 *
 * @param ReserveSpriteOffsetY:num
 * @text Offset Y
 * @parent ReservePartyMapSprite
 * @desc If showing map sprites, offset the y coordinate here from bottom.
 * @default 4
 *
 * @param ReservePartySvBattler
 * @text Sideview Battler
 * @parent ReservePartyGraphic:str
 *
 * @param ReserveBattlerOffsetX:num
 * @text Offset X
 * @parent ReservePartySvBattler
 * @desc If showing sideview battlers, offset the x coordinate here from left.
 * @default 48
 *
 * @param ReserveBattlerOffsetY:num
 * @text Offset Y
 * @parent ReservePartySvBattler
 * @desc If showing sideview battlers, offset the y coordinate here from bottom.
 * @default 4
 *
 * @param ReservePartyWindowRect:func
 * @text JS: X, Y, W, H
 * @parent ReservePartyWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const ww = this._reservePartyLabel.width;\nconst wx = this._reservePartyLabel.x;\nconst wy = this._reservePartyLabel.y + this._reservePartyLabel.height;\nconst wh = this.mainAreaHeight() - this._reservePartyLabel.height - this._activePartyWindow.height - this._activePartyLabel.height;\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param StatusLabel
 * @text Status Label
 *
 * @param StatusLabelBgType:num
 * @text Background Type
 * @parent StatusLabel
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusLabelRect:func
 * @text JS: X, Y, W, H
 * @parent StatusLabel
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const ww = Graphics.boxWidth - this._reservePartyLabel.width;\nconst wx = this.isRightInputMode() ? 0 : (Graphics.boxWidth - ww);\nconst wy = this._activePartyWindow.y + this._activePartyWindow.height;\nconst wh = Window_Base.prototype.lineHeight();\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param StatusWindow
 * @text Status Window
 *
 * @param StatusWindowBgType:num
 * @text Background Type
 * @parent StatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusWindowDraw:func
 * @text JS: Draw Data
 * @parent StatusWindow
 * @type note
 * @desc Code used to draw the display data in the Status Window.
 * @default "// Draw Empty\nif (!this._actor) {\n    this.drawItemDarkRect(0, 0, this.innerWidth, this.innerHeight);\n    const y = Math.round((this.innerHeight - this.lineHeight()) / 2);\n    this.changeTextColor(ColorManager.systemColor());\n    this.drawText(TextManager.emptyPartyMember, 0, y, this.innerWidth, 'center');\n    return;\n}\n\n// Draw Face and Simple Status\nthis.drawActorFace(this._actor, 1, 0, ImageManager.faceWidth, ImageManager.faceHeight);\nthis.drawActorSimpleStatus(this._actor, ImageManager.faceWidth + 36, 0);\n\n// Declare Constants\nconst lineHeight = this.lineHeight();\nconst params = this.actorParams();\nconst paramWidth = Math.round(this.innerWidth / 2);\nconst paramHeight = Math.ceil(params.length / 2) * lineHeight;\nconst baseX = 0;\nlet x = 0;\nlet y = ImageManager.faceHeight + lineHeight / 2;\n\n// Draw Parameters\nfor (const param of params) {\n    this.drawItemDarkRect(x, y, paramWidth, lineHeight);\n    this.drawParamName(param, x, y, paramWidth);\n    this.drawParamValue(param, x, y, paramWidth);\n\n    if (x === baseX) {\n        x += paramWidth;\n    } else {\n        x = baseX;\n        y += lineHeight;\n    }\n}"
 *
 * @param StatusWindowRect:func
 * @text JS: X, Y, W, H
 * @parent StatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const ww = this._statusPartyLabel.width;\nconst wx = this.isRightInputMode() ? 0 : (Graphics.boxWidth - ww);\nconst wy = this._reservePartyWindow.y;\nconst wh = this._reservePartyWindow.height;\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param BattleSwitchWindow
 * @text Battle Switch Window
 *
 * @param BattleSwitchWindowBgType:num
 * @text Background Type
 * @parent BattleSwitchWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param BattleSwitchWindowRect:func
 * @text JS: X, Y, W, H
 * @parent BattleSwitchWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * Does not apply to Border Battle Layout style.
 * @default "const padding = $gameSystem.windowPadding() * 2;\nlet ww = 516 + padding;\nlet wh = Window_PartyBattleSwitch.prototype.itemHeight() * 4 + padding;\nlet wx = Math.round(Graphics.boxWidth - ww) / 2;\nlet wy = Math.round(Graphics.boxHeight - wh - this._statusWindow.height) / 2;\nwy = wy.clamp(0, Graphics.boxHeight - wh - this._statusWindow.height);\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 */
//=============================================================================

const _0x4cbe=['itemLineRect','tpbImmediateAction','Game_Party_swapOrder','_callSceneParty','partySwitchWindowRectStandard','face','ceil','ActiveParty','isSceneBattle','reserveTransfer','CoreEngine','543849SBSjGD','shift','isRightInputMode','_backSprite2','clearBypassAutoSave','isNextSceneBattleTransitionable','_actor','drawItem','nameStartPosition','assistSortPartyMembers','innerHeight','registerCommand','formation','Scene_Battle_isAnyInputWindowActive','BattleManager_setup','createAllWindows','startOpacity','placeBasicGauges','height','_inputting','battler','CallPartyScene','isFTB','floor','lineHeight','isPreviousSceneBattleTransitionable','setHandler','addWindow','activate','snapForBackground','_tpbState','itemPadding','createStatusWindow','dimColor1','createInnerSprite','maxBattleMembers','select','index','otbReturnBattlerToTurnOrders','activeParty','isTriggered','isSceneParty','isShiftShortcutEnabled','refresh','_debug','border','text','VisuMZ_2_BattleSystemOTB','createPartyCommandWindowBattleCore','assistSwapPositions','setBackgroundType','loadCharacter','isBTB','fillRect','isAutosaveEnabled','playEquip','activePartyLabelRect','ActivePartyGraphic','battlePartyChangeCmd','isTpb','commandFormation','updateHelp','ActiveSpriteOffsetX','prototype','includes','getBackgroundOpacity','_partySwitchDuration','Scene_Base_isAutosaveEnabled','AssistSwapOut','_callPartyMemberSwitch','_clickHandler','emptyPartyMember','setupStartingMembers','postPartySwitchMenuTurnBased','callUpdateHelp','_currentActor','quickSwap','_partySwitchBattleCommandCooldown','_otb_actionBattlersNext','_partyRequired','postPartySwitchMenuTpb','partyChangeRefresh','scaleSprite','drawParamName','ReserveItemThickness','initPartySystem','addPartyCommand','assistSwapInPartyMember','ARRAYEVAL','_subject','processShiftRemoveShortcut','addText','reserveMembers','playCursorSound','1176286hBoSVl','battlePartySwitchCmd','drawSvActor','createActivePartyLabel','isShowPartySwitchOutAnimation','_pagedownButton','_backSprite1','removeActor','Empty','onBattleStart','isImmediateTpb','description','addRemoveCommand','drawParamText','onReserveCancel','_partyMemberSwitchWindow','isPartyCommandAdded','addLoadListener','lockPartyMemberIcon','isPreviousScene','updatePartySwitch','createBackground','makeActions','createReservePartyLabel','isShiftRemoveShortcutEnabled','addNonBattleTestMembers','PartyCmdCooldown','windowPadding','makeActionOrders','_tpbSceneChangeCacheActor','partySwitchWindowRectBorder','callPartyMemberSwitch','drawItemDarkRect','addActorToBattleMembers','_actorGraphic','cursorPageup','initBattleMembers','swapOrderPartySystemPlugin','changeTextColor','uiInputPosition','VisuMZ_2_BattleSystemBTB','innerWidth','startSwitchInAnimation','_partyLocked','centerSprite','requestRefresh','onPartySwitchCancel','_bypassAutoSavePartySystem','_actionBattlers','width','uiMenuStyle','push','reserveParty','_activePartyWindow','contents','setup','isFormationCommandAdded','textColor','battlePartyChangeCmdHelp','gaugeBackColor','_partySystemSwitchOut','rawBattleMembers','Game_Battler_regenerateAll','BgFilename2','toLowerCase','_reservePartyWindow','PartySystem','faceWidth','callFormation','max','addCustomCommands','Window_ActorCommand_updateHelp','partySwitchWindowRect','isCTB','BackRectColor','members','update','bitmap','MoveRandomToActive','createReservePartyWindow','commandStyle','deactivate','_rowThickness','setActor','cursorUp','_statusWindow','JSON','StatusWindowRect','onBattlePartySwitch','trim','sprite','ConvertParams','addActorToBattleMembersAtIndex','drawParamValue','processCursorMove','initialize','battleMembers','changePaintOpacity','Settings','Game_Party_setupBattleTest','isEnabled','QueuePartyScene','battlePartySwitchCmdHelp','4880XhPaRq','ARRAYNUM','processCancel','VisuMZ_2_BattleSystemFTB','isActor','updatePadding','popScene','anyRequiredPartyMembersInReserve','activePartyWindowRect','isFormationChangeOk','ReservePartyLabelRect','visible','\x5cI[%1]%2','actor','processShiftSortShortcut','ARRAYSTR','_battleMaxSize','Sprite_Actor_update','isPartyCommandEnabled','faceHeight','increaseTurn','updateBattleProcess','statusParty','RequirePartyMembers','exit','maxCols','toUpperCase','_battleSystemIncompatibilityError','drawActorSimpleStatus','Game_Party_setupStartingMembers','initEquips','drawActorName','center','DrawBackRect','maxItems','ActorCmdCooldown','processOk','ActiveBattlerOffsetX','Vocab','faceName','map','ActivePartyLabelBgType','sortActionOrdersBTB','ActiveSpriteOffsetY','open','setBackgroundOpacity','preparePartySwitchMember','cursorVisible','round','isQueueFormationMenu','checkShiftSortShortcut','Scene_Battle_createActorCommandWindow','bind','battlePartyChangeIcon','getColor','setBattlePartySwitchCooldown','isSceneMap','stepForward','loadPartyImages','loadTitle2','isRequiredInParty','Param','recoverAll','isActiveTpb','defaultMaxBattleMembers','onReserveOk','PartyCmdWinAddParty','charged','isTimeActive','createActivePartyWindow','systemColor','addChild','assistRemovePartyMember','drawActorFace','_statusPartyLabel','getParamValue','SceneManager_isNextSceneBattleTransitionable','BattleSwitchOut','inBattle','paintOpacity','MovePartyIndexToReserve','Game_Party_removeActor','BgSettings','buttonAssistText4','direction','battlePartySwitchCooldown','StatusWindowBgType','Window','removeActorFromBattleMembers','statusLabelRect','ChangeMaxBattleMembers','_partySwitchTargetActor','ReserveBattlerOffsetX','parse','drawItemEmpty','cursorDown','1235008VjkUfi','cursorPagedown','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','MoveActorsToActive','drawActorPartyIcons','DisplayedParams','Game_Actor_setup','StatusLabelRect','isNextScene','reservePartyWindowRect','79KOYmXd','102946utTwJo','requiredPartyMemberIcon','drawDarkRect','iconHeight','1RIfzpA','isCurrentItemEnabled','_logWindow','Value','processPartySwitchMember','call','updateBattlePartySwitchCooldown','STR','followers','STRUCT','ReserveSpriteOffsetX','filter','itemRectWithPadding','hasBattleSystemIncompatibilities','WARNING:\x20Party\x20Change\x20command\x20is\x20unavailable\x20for\x20Window_PartyCommand\x20for\x20this\x20Battle\x20System','drawActorCharacter','_lastIndex','ActivePartyWindowBgType','length','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','sortActors','changeMaxBattleMembers','remove','BattleHelpSwitch','actorId','Window_PartyCommand_updateHelp','close','drawText','StatusWindowDraw','addActor','switchStateIconActor','initMaxBattleMembers','name','ActivePartyWindowRect','battlerName','clear','return\x200','adjustSprite','status','processDrawItem','refreshAllWindows','refreshOG','rearrangePartyActors','BattlePartyCmd','checkInitBattleMembers','reselect','create','ReservePartyLabelBgType','drawItemImageSvActor','RequireIcon','currentSymbol','595559lfsutz','BattleSwitchWindowRect','slice','isOTB','loadSvActor','Require','setStatusWindow','isFormationCommandEnabled','_tpbChargeTime','format','addFormationCommand','mapId','Scene_Battle_createAllWindows','drawItemImageFace','_helpWindow','indexOf','VisuMZ_0_CoreEngine','reservePartyLabelRect','getPartySystemBackColor','log','startSwitchOutAnimation','AssistSwapPosition','AssistRemove','setText','createActorCommandWindow','Game_Battler_onBattleStart','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','active','skillItemWindowRectBorderStyle','ActorCmdWinAddParty','ReservePartyWindowBgType','ReservePartyWindowRect','match','setBattler','onPartySwitchOk','assistSwapOutPartyMember','createStatusLabel','BattleSwitchWindowBgType','setPartyLock','characterName','drawRemoveCommand','ReserveSpriteOffsetY','AddRemoveCmd','SnapshotOpacity','constructor','BattleHelpFormation','_actorCommandWindow','Scene_Battle_createPartyCommandWindowBattleCore','768799PEKACX','_partyCommandWindow','General','battleLayoutStyle','drawItemImageSprite','pop','_reservePartyLabel','applyBattlePartySwitchCooldown','_windowLayer','ARRAYJSON','VisuMZ_1_MainMenuCore','ReserveParty','iconWidth','itemRect','BattlePartyIcon','2YPUNVl','VisuMZ_1_BattleCore','Game_Party_initialize','Game_Unit_inBattle','resetFontSettings','drawActorPartyIconsVert','_partySystemBattleCommandCooldown','_activePartyLabel','min','buttonAssistText3','_scene','isAnyInputWindowActive','itemHeight','Game_Troop_increaseTurn','svbattler','_battleMembers','createPartySwitchWindow','canSwitchPartyInBattle','playOkSound','updateTurnOrderSTB','actorParams','ARRAYFUNC','VisuMZ_2_BattleSystemSTB','Game_Party_addActor','_actors','swapOrder','deselect','_statusPartyWindow','Status','parameters','drawItemImage','gradientFillRect','pendingIndex','isAlive','drawIcon','clearPartyBattleCommandCooldown','SceneManager_isPreviousSceneBattleTransitionable','loadFaceImages','Actors','needsPageButtons','_spriteset','NUM','regenerateAll','ActiveTpbFormationMessage','clearPartySwitchCommandCooldown','SwitchOutAnimation','equips','loadTitle1','isSTB','#%1','drawActorClass','loadFace','terminate','isPlaytest','ReservePartyGraphic','ReserveBattlerOffsetY','allMembers','createCustomBackgroundImages','currentActor','smoothSelect'];const _0x57ad=function(_0x1431b2,_0x18a2a3){_0x1431b2=_0x1431b2-0xd5;let _0x4cbe7e=_0x4cbe[_0x1431b2];return _0x4cbe7e;};const _0xa2c6e5=_0x57ad;(function(_0x5dcb46,_0xabaeae){const _0x5c5f09=_0x57ad;while(!![]){try{const _0x871ef5=-parseInt(_0x5c5f09(0x1b8))*parseInt(_0x5c5f09(0x155))+parseInt(_0x5c5f09(0x146))+parseInt(_0x5c5f09(0x150))*-parseInt(_0x5c5f09(0xe6))+parseInt(_0x5c5f09(0x26c))+parseInt(_0x5c5f09(0x20e))*-parseInt(_0x5c5f09(0x1c7))+parseInt(_0x5c5f09(0x151))+parseInt(_0x5c5f09(0x188));if(_0x871ef5===_0xabaeae)break;else _0x5dcb46['push'](_0x5dcb46['shift']());}catch(_0x12f08c){_0x5dcb46['push'](_0x5dcb46['shift']());}}}(_0x4cbe,0xd3dc6));var label=_0xa2c6e5(0x2ae),tier=tier||0x0,dependencies=[],pluginData=$plugins[_0xa2c6e5(0x160)](function(_0x22ed07){const _0x288a89=_0xa2c6e5;return _0x22ed07[_0x288a89(0x17b)]&&_0x22ed07[_0x288a89(0x277)]['includes']('['+label+']');})[0x0];VisuMZ[label]['Settings']=VisuMZ[label][_0xa2c6e5(0xe1)]||{},VisuMZ[_0xa2c6e5(0xda)]=function(_0x2dd231,_0x4e68fa){const _0x30a8e5=_0xa2c6e5;for(const _0xc46512 in _0x4e68fa){if(_0xc46512[_0x30a8e5(0x1a8)](/(.*):(.*)/i)){const _0x11e469=String(RegExp['$1']),_0x12ace7=String(RegExp['$2'])[_0x30a8e5(0x100)]()[_0x30a8e5(0xd8)]();let _0x418620,_0x3e0bed,_0x5d4560;switch(_0x12ace7){case _0x30a8e5(0x1f0):_0x418620=_0x4e68fa[_0xc46512]!==''?Number(_0x4e68fa[_0xc46512]):0x0;break;case _0x30a8e5(0xe7):_0x3e0bed=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):[],_0x418620=_0x3e0bed[_0x30a8e5(0x10e)](_0x1ea704=>Number(_0x1ea704));break;case'EVAL':_0x418620=_0x4e68fa[_0xc46512]!==''?eval(_0x4e68fa[_0xc46512]):null;break;case _0x30a8e5(0x266):_0x3e0bed=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):[],_0x418620=_0x3e0bed[_0x30a8e5(0x10e)](_0x46b44e=>eval(_0x46b44e));break;case _0x30a8e5(0xd5):_0x418620=_0x4e68fa[_0xc46512]!==''?JSON['parse'](_0x4e68fa[_0xc46512]):'';break;case _0x30a8e5(0x1c1):_0x3e0bed=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):[],_0x418620=_0x3e0bed[_0x30a8e5(0x10e)](_0x338bd0=>JSON['parse'](_0x338bd0));break;case'FUNC':_0x418620=_0x4e68fa[_0xc46512]!==''?new Function(JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512])):new Function(_0x30a8e5(0x179));break;case _0x30a8e5(0x1dc):_0x3e0bed=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):[],_0x418620=_0x3e0bed[_0x30a8e5(0x10e)](_0x3b803e=>new Function(JSON['parse'](_0x3b803e)));break;case _0x30a8e5(0x15c):_0x418620=_0x4e68fa[_0xc46512]!==''?String(_0x4e68fa[_0xc46512]):'';break;case _0x30a8e5(0xf5):_0x3e0bed=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):[],_0x418620=_0x3e0bed[_0x30a8e5(0x10e)](_0x21f0c5=>String(_0x21f0c5));break;case _0x30a8e5(0x15e):_0x5d4560=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):{},_0x418620=VisuMZ[_0x30a8e5(0xda)]({},_0x5d4560);break;case'ARRAYSTRUCT':_0x3e0bed=_0x4e68fa[_0xc46512]!==''?JSON[_0x30a8e5(0x143)](_0x4e68fa[_0xc46512]):[],_0x418620=_0x3e0bed[_0x30a8e5(0x10e)](_0x3fa6a8=>VisuMZ['ConvertParams']({},JSON[_0x30a8e5(0x143)](_0x3fa6a8)));break;default:continue;}_0x2dd231[_0x11e469]=_0x418620;}}return _0x2dd231;},(_0x29dace=>{const _0x17eaa6=_0xa2c6e5,_0x50bed2=_0x29dace[_0x17eaa6(0x175)];for(const _0x575cec of dependencies){if(!Imported[_0x575cec]){alert(_0x17eaa6(0x1a2)[_0x17eaa6(0x191)](_0x50bed2,_0x575cec)),SceneManager['exit']();break;}}const _0x80e578=_0x29dace[_0x17eaa6(0x277)];if(_0x80e578[_0x17eaa6(0x1a8)](/\[Version[ ](.*?)\]/i)){const _0x5b9cef=Number(RegExp['$1']);_0x5b9cef!==VisuMZ[label]['version']&&(alert(_0x17eaa6(0x168)[_0x17eaa6(0x191)](_0x50bed2,_0x5b9cef)),SceneManager[_0x17eaa6(0xfe)]());}if(_0x80e578[_0x17eaa6(0x1a8)](/\[Tier[ ](\d+)\]/i)){const _0x19fb3a=Number(RegExp['$1']);_0x19fb3a<tier?(alert(_0x17eaa6(0x148)[_0x17eaa6(0x191)](_0x50bed2,_0x19fb3a,tier)),SceneManager[_0x17eaa6(0xfe)]()):tier=Math[_0x17eaa6(0x2b1)](_0x19fb3a,tier);}VisuMZ[_0x17eaa6(0xda)](VisuMZ[label]['Settings'],_0x29dace[_0x17eaa6(0x1e4)]);})(pluginData),PluginManager['registerCommand'](pluginData[_0xa2c6e5(0x175)],_0xa2c6e5(0x223),_0x3b352e=>{const _0xd5186c=_0xa2c6e5;SceneManager[_0xd5186c(0x29f)](Scene_Party);}),PluginManager[_0xa2c6e5(0x219)](pluginData[_0xa2c6e5(0x175)],_0xa2c6e5(0x140),_0x447090=>{const _0x4a7546=_0xa2c6e5;if($gameParty[_0x4a7546(0x134)]())return;VisuMZ[_0x4a7546(0xda)](_0x447090,_0x447090);const _0x2c93bf=_0x447090[_0x4a7546(0x158)];$gameParty[_0x4a7546(0x16a)](_0x2c93bf);}),PluginManager[_0xa2c6e5(0x219)](pluginData[_0xa2c6e5(0x175)],_0xa2c6e5(0x149),_0x362a96=>{const _0x33bedf=_0xa2c6e5;if(!SceneManager[_0x33bedf(0x11e)]())return;VisuMZ['ConvertParams'](_0x362a96,_0x362a96);const _0xb18a6=_0x362a96[_0x33bedf(0x1ed)];for(const _0x30f09f of _0xb18a6){$gameParty[_0x33bedf(0x28d)](_0x30f09f);}$gamePlayer[_0x33bedf(0x239)]();}),PluginManager[_0xa2c6e5(0x219)](pluginData[_0xa2c6e5(0x175)],'MoveActorsToReserve',_0x439065=>{const _0x4da618=_0xa2c6e5;if(!SceneManager['isSceneMap']())return;VisuMZ['ConvertParams'](_0x439065,_0x439065);const _0x2604a8=_0x439065[_0x4da618(0x1ed)];for(const _0x503bd6 of _0x2604a8){if($gameParty[_0x4da618(0xdf)]()[_0x4da618(0x167)]<=0x1)break;$gameParty[_0x4da618(0x13e)](_0x503bd6);}$gamePlayer[_0x4da618(0x239)]();}),PluginManager[_0xa2c6e5(0x219)](pluginData['name'],_0xa2c6e5(0x136),_0x58ee81=>{const _0x5c1ad0=_0xa2c6e5;if(!SceneManager[_0x5c1ad0(0x11e)]())return;if($gameParty['battleMembers']()[_0x5c1ad0(0x167)]<=0x1)return;if(!$gameParty[_0x5c1ad0(0x1d6)])return;if($gameParty[_0x5c1ad0(0x1d6)][_0x5c1ad0(0x167)]<=0x0)return;VisuMZ[_0x5c1ad0(0xda)](_0x58ee81,_0x58ee81);const _0x33f914=_0x58ee81['Index'],_0x49df8e=$gameParty[_0x5c1ad0(0x1d6)][_0x33f914];$gameParty[_0x5c1ad0(0x13e)](_0x49df8e),$gamePlayer[_0x5c1ad0(0x239)]();}),PluginManager[_0xa2c6e5(0x219)](pluginData[_0xa2c6e5(0x175)],_0xa2c6e5(0x2ba),_0x353dfd=>{const _0x5c324c=_0xa2c6e5;if(!SceneManager[_0x5c324c(0x11e)]())return;if($gameParty[_0x5c324c(0xdf)]()[_0x5c324c(0x167)]>=$gameParty[_0x5c324c(0x231)]())return;if($gameParty[_0x5c324c(0x26a)]()[_0x5c324c(0x167)]<=0x0)return;const _0x3ca8f0=$gameParty[_0x5c324c(0x26a)](),_0x5a3f3a=_0x3ca8f0[Math[_0x5c324c(0x225)](Math['random']()*_0x3ca8f0[_0x5c324c(0x167)])],_0xa50b2a=_0x5a3f3a[_0x5c324c(0x16d)]();$gameParty[_0x5c324c(0x28d)](_0xa50b2a),$gamePlayer['refresh']();}),PluginManager[_0xa2c6e5(0x219)](pluginData[_0xa2c6e5(0x175)],'LockPartyMembers',_0x5e6524=>{const _0x120476=_0xa2c6e5;VisuMZ[_0x120476(0xda)](_0x5e6524,_0x5e6524);const _0x1c6b6c=_0x5e6524[_0x120476(0x1ed)][_0x120476(0x10e)](_0x53acb8=>$gameActors[_0x120476(0xf3)](_0x53acb8))[_0x120476(0x16b)](null),_0x224188=_0x5e6524['Lock'];for(const _0x4b56f0 of _0x1c6b6c){if(!_0x4b56f0)continue;_0x4b56f0[_0x120476(0x1ae)](_0x224188);}}),PluginManager[_0xa2c6e5(0x219)](pluginData[_0xa2c6e5(0x175)],_0xa2c6e5(0xfd),_0x2c3788=>{const _0x549318=_0xa2c6e5;VisuMZ[_0x549318(0xda)](_0x2c3788,_0x2c3788);const _0x4d7722=_0x2c3788[_0x549318(0x1ed)][_0x549318(0x10e)](_0x1c4289=>$gameActors['actor'](_0x1c4289))[_0x549318(0x16b)](null),_0x2daf44=_0x2c3788[_0x549318(0x18d)];for(const _0x551a78 of _0x4d7722){if(!_0x551a78)continue;_0x551a78['setPartyRequirement'](_0x2daf44);}}),ImageManager['lockPartyMemberIcon']=VisuMZ['PartySystem'][_0xa2c6e5(0xe1)][_0xa2c6e5(0x1ba)]['LockIcon'],ImageManager[_0xa2c6e5(0x152)]=VisuMZ[_0xa2c6e5(0x2ae)]['Settings'][_0xa2c6e5(0x1ba)][_0xa2c6e5(0x186)],TextManager[_0xa2c6e5(0x235)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0x20a)],TextManager[_0xa2c6e5(0x2a0)]=VisuMZ[_0xa2c6e5(0x2ae)]['Settings'][_0xa2c6e5(0x10c)][_0xa2c6e5(0x1c3)],TextManager[_0xa2c6e5(0xfc)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0x1e3)],TextManager[_0xa2c6e5(0x255)]=VisuMZ['PartySystem'][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0x274)],TextManager['removePartyMember']=VisuMZ[_0xa2c6e5(0x2ae)]['Settings'][_0xa2c6e5(0x10c)]['Remove'],TextManager[_0xa2c6e5(0x23f)]=VisuMZ[_0xa2c6e5(0x2ae)]['Settings'][_0xa2c6e5(0x10c)][_0xa2c6e5(0x19d)],TextManager[_0xa2c6e5(0x12e)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0x19e)],TextManager[_0xa2c6e5(0x217)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)]['AssistSort'],TextManager[_0xa2c6e5(0x265)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)]['Vocab']['AssistSwapIn'],TextManager[_0xa2c6e5(0x1ab)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0x252)],ColorManager[_0xa2c6e5(0x11c)]=function(_0x759dd1){const _0x1e4146=_0xa2c6e5;return _0x759dd1=String(_0x759dd1),_0x759dd1[_0x1e4146(0x1a8)](/#(.*)/i)?_0x1e4146(0x1f8)[_0x1e4146(0x191)](String(RegExp['$1'])):this[_0x1e4146(0x2a5)](Number(_0x759dd1));},SceneManager[_0xa2c6e5(0x237)]=function(){const _0x144336=_0xa2c6e5;return this[_0x144336(0x1d1)]&&this[_0x144336(0x1d1)][_0x144336(0x1b4)]===Scene_Party;},SceneManager[_0xa2c6e5(0x11e)]=function(){const _0x188490=_0xa2c6e5;return this[_0x188490(0x1d1)]&&this[_0x188490(0x1d1)][_0x188490(0x1b4)]===Scene_Map;},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x21c)]=BattleManager[_0xa2c6e5(0x2a3)],BattleManager[_0xa2c6e5(0x2a3)]=function(_0x4e66e6,_0xe47648,_0x4366c2){const _0x456c2f=_0xa2c6e5;VisuMZ[_0x456c2f(0x2ae)][_0x456c2f(0x21c)]['call'](this,_0x4e66e6,_0xe47648,_0x4366c2),$gameParty[_0x456c2f(0x1ea)]();},VisuMZ['PartySystem'][_0xa2c6e5(0x1a1)]=Game_Battler[_0xa2c6e5(0x24d)][_0xa2c6e5(0x275)],Game_Battler[_0xa2c6e5(0x24d)]['onBattleStart']=function(_0x592555){const _0x364a89=_0xa2c6e5;VisuMZ['PartySystem']['Game_Battler_onBattleStart'][_0x364a89(0x15a)](this,_0x592555);if(this['isActor']())this[_0x364a89(0x1f3)]();},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x2aa)]=Game_Battler[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1f1)],Game_Battler[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1f1)]=function(){const _0x15173b=_0xa2c6e5;VisuMZ[_0x15173b(0x2ae)]['Game_Battler_regenerateAll']['call'](this);if(this[_0x15173b(0xea)]())this[_0x15173b(0x15b)]();},VisuMZ['PartySystem'][_0xa2c6e5(0x14c)]=Game_Actor['prototype'][_0xa2c6e5(0x2a3)],Game_Actor['prototype'][_0xa2c6e5(0x2a3)]=function(_0x2c36a1){const _0x45149e=_0xa2c6e5;VisuMZ[_0x45149e(0x2ae)][_0x45149e(0x14c)][_0x45149e(0x15a)](this,_0x2c36a1),this['initPartySystem'](),this[_0x45149e(0x1f3)]();},Game_Actor['prototype']['initPartySystem']=function(){const _0x3325a2=_0xa2c6e5;this[_0x3325a2(0x297)]=![],this['_partyRequired']=![];},Game_Actor[_0xa2c6e5(0x24d)][_0xa2c6e5(0xef)]=function(){const _0x37f40e=_0xa2c6e5;if(this[_0x37f40e(0x297)]===undefined)this[_0x37f40e(0x263)]();return!this[_0x37f40e(0x297)];},Game_Actor['prototype'][_0xa2c6e5(0x1ae)]=function(_0x5d7fc4){const _0x5a7bb0=_0xa2c6e5;if(this[_0x5a7bb0(0x297)]===undefined)this['initPartySystem']();this['_partyLocked']=_0x5d7fc4;},Game_Actor[_0xa2c6e5(0x24d)][_0xa2c6e5(0x122)]=function(){const _0x45c76e=_0xa2c6e5;if(this['_partyRequired']===undefined)this[_0x45c76e(0x263)]();return this[_0x45c76e(0x25d)];},Game_Actor[_0xa2c6e5(0x24d)]['setPartyRequirement']=function(_0x51fc7e){const _0x5d513a=_0xa2c6e5;if(this[_0x5d513a(0x25d)]===undefined)this[_0x5d513a(0x263)]();this[_0x5d513a(0x25d)]=_0x51fc7e;},Game_Actor[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1f3)]=function(){const _0x4c88ad=_0xa2c6e5;this[_0x4c88ad(0x25b)]=0x0;},Game_Actor['prototype'][_0xa2c6e5(0x1d8)]=function(){const _0x14a926=_0xa2c6e5;if(this[_0x14a926(0x25b)]===undefined)this['clearPartySwitchCommandCooldown']();if(!this[_0x14a926(0xef)]())return![];if(this[_0x14a926(0x122)]())return![];return this[_0x14a926(0x25b)]<=0x0;},Game_Actor['prototype'][_0xa2c6e5(0x13b)]=function(){const _0x5e4a8c=_0xa2c6e5;if(this[_0x5e4a8c(0x25b)]===undefined)this[_0x5e4a8c(0x1f3)]();return this[_0x5e4a8c(0x25b)];},Game_Actor[_0xa2c6e5(0x24d)]['setBattlePartySwitchCooldown']=function(_0x36ef51){const _0x5117a2=_0xa2c6e5;if(this[_0x5117a2(0x25b)]===undefined)this[_0x5117a2(0x1f3)]();this['_partySwitchBattleCommandCooldown']=_0x36ef51||0x0;},Game_Actor['prototype'][_0xa2c6e5(0x1bf)]=function(){const _0x1cc876=_0xa2c6e5;if(this[_0x1cc876(0x25b)]===undefined)this[_0x1cc876(0x1f3)]();const _0x1caed3=VisuMZ[_0x1cc876(0x2ae)][_0x1cc876(0xe1)][_0x1cc876(0x1ba)][_0x1cc876(0x109)];this[_0x1cc876(0x11d)](_0x1caed3);},Game_Actor[_0xa2c6e5(0x24d)][_0xa2c6e5(0x15b)]=function(){const _0x148f46=_0xa2c6e5;if(this[_0x148f46(0x25b)]===undefined)this[_0x148f46(0x1f3)]();this[_0x148f46(0x25b)]--;},Game_Actor[_0xa2c6e5(0x24d)]['onBattlePartySwitch']=function(_0x568216){const _0x1e13ca=_0xa2c6e5;Imported['VisuMZ_2_BattleSystemCTB']&&BattleManager[_0x1e13ca(0x2b5)]()&&BattleManager['updateTurnOrderCTB']();Imported[_0x1e13ca(0x1dd)]&&BattleManager['isSTB']()&&(BattleManager[_0x1e13ca(0x1da)](),BattleManager[_0x1e13ca(0x267)]=this,BattleManager[_0x1e13ca(0x259)]=this);if(Imported['VisuMZ_2_BattleSystemBTB']&&BattleManager[_0x1e13ca(0x242)]()){BattleManager[_0x1e13ca(0x267)]=undefined,BattleManager[_0x1e13ca(0x259)]=this;const _0x2a4936=BattleManager[_0x1e13ca(0x29c)][_0x1e13ca(0x197)](_0x568216);BattleManager[_0x1e13ca(0x29c)][_0x2a4936]=this,BattleManager[_0x1e13ca(0x110)]();}Imported['VisuMZ_2_BattleSystemFTB']&&BattleManager[_0x1e13ca(0x224)]()&&(BattleManager['_subject']=this,BattleManager[_0x1e13ca(0x259)]=this);if(Imported[_0x1e13ca(0x23d)]&&BattleManager[_0x1e13ca(0x18b)]()){BattleManager[_0x1e13ca(0x267)]=this,BattleManager[_0x1e13ca(0x259)]=this;for(let _0x302aca=0x0;_0x302aca<BattleManager[_0x1e13ca(0x29c)][_0x1e13ca(0x167)];_0x302aca++){const _0x431f0b=BattleManager[_0x1e13ca(0x29c)][_0x302aca];_0x431f0b===_0x568216&&(BattleManager[_0x1e13ca(0x29c)][_0x302aca]=this);}for(let _0x2f7917=0x0;_0x2f7917<BattleManager[_0x1e13ca(0x25c)][_0x1e13ca(0x167)];_0x2f7917++){const _0x5cd076=BattleManager[_0x1e13ca(0x25c)][_0x2f7917];_0x5cd076===_0x568216&&(BattleManager[_0x1e13ca(0x25c)][_0x2f7917]=this);}}},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x1ca)]=Game_Unit[_0xa2c6e5(0x24d)][_0xa2c6e5(0x134)],Game_Unit['prototype']['inBattle']=function(){const _0x581e0c=_0xa2c6e5;if(SceneManager[_0x581e0c(0x237)]())return![];return VisuMZ[_0x581e0c(0x2ae)][_0x581e0c(0x1ca)][_0x581e0c(0x15a)](this);},Game_Party[_0xa2c6e5(0x126)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x1ba)]['MaxBattleMembers'],VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x1c9)]=Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0xde)],Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0xde)]=function(){const _0x4de3bc=_0xa2c6e5;VisuMZ[_0x4de3bc(0x2ae)][_0x4de3bc(0x1c9)]['call'](this),this[_0x4de3bc(0x1ea)](),this[_0x4de3bc(0x174)](),this[_0x4de3bc(0x290)]();},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1ea)]=function(){const _0x51fca7=_0xa2c6e5;this[_0x51fca7(0x1cd)]=0x0;},Game_Party[_0xa2c6e5(0x24d)]['canSwitchPartyInBattle']=function(){const _0x566a61=_0xa2c6e5;if(this[_0x566a61(0x1cd)]===undefined)this[_0x566a61(0x1ea)]();return this[_0x566a61(0x1cd)]<=0x0;},Game_Party['prototype'][_0xa2c6e5(0x13b)]=function(){const _0x273912=_0xa2c6e5;if(this[_0x273912(0x1cd)]===undefined)this['clearPartyBattleCommandCooldown']();return this[_0x273912(0x1cd)];},Game_Party[_0xa2c6e5(0x24d)]['setBattlePartySwitchCooldown']=function(_0x16b887){const _0x1c567d=_0xa2c6e5;if(this[_0x1c567d(0x1cd)]===undefined)this['clearPartyBattleCommandCooldown']();this[_0x1c567d(0x1cd)]=_0x16b887;},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1bf)]=function(){const _0x2f743b=_0xa2c6e5;if(this[_0x2f743b(0x1cd)]===undefined)this[_0x2f743b(0x1ea)]();this[_0x2f743b(0x1cd)]=VisuMZ[_0x2f743b(0x2ae)][_0x2f743b(0xe1)][_0x2f743b(0x1ba)][_0x2f743b(0x286)]||0x0;},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x15b)]=function(){const _0x59b190=_0xa2c6e5;if(this['_partySystemBattleCommandCooldown']===undefined)this['clearPartyBattleCommandCooldown']();this[_0x59b190(0x1cd)]--;},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x174)]=function(){const _0x4635f1=_0xa2c6e5;this[_0x4635f1(0xf6)]=0x0;},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x16a)]=function(_0x44211a){const _0x127eef=_0xa2c6e5;this[_0x127eef(0xf6)]=_0x44211a,this[_0x127eef(0x290)](!![]),$gamePlayer&&$gamePlayer[_0x127eef(0x15d)]()&&$gamePlayer['followers']()[_0x127eef(0x16a)]();},Game_Followers[_0xa2c6e5(0x24d)]['changeMaxBattleMembers']=function(){const _0x1c7e36=_0xa2c6e5;if(!SceneManager[_0x1c7e36(0x11e)]())return;this[_0x1c7e36(0x2a3)]();const _0x5546f8=$gameMap[_0x1c7e36(0x193)](),_0x1efaf5=$gamePlayer['x'],_0x3a48cb=$gamePlayer['y'],_0x180823=$gamePlayer[_0x1c7e36(0x13a)]();$gameTemp[_0x1c7e36(0x29b)]=!![],$gamePlayer[_0x1c7e36(0x20c)](_0x5546f8,_0x1efaf5,_0x3a48cb,_0x180823,0x0),setTimeout(this[_0x1c7e36(0x212)][_0x1c7e36(0x11a)](this),0x7d0);},Game_Followers['prototype']['clearBypassAutoSave']=function(){const _0x16bf4e=_0xa2c6e5;$gameTemp[_0x16bf4e(0x29b)]=![];},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x251)]=Scene_Base[_0xa2c6e5(0x24d)][_0xa2c6e5(0x244)],Scene_Base['prototype'][_0xa2c6e5(0x244)]=function(){const _0x4710a0=_0xa2c6e5;if($gameTemp[_0x4710a0(0x29b)])return![];return VisuMZ[_0x4710a0(0x2ae)]['Scene_Base_isAutosaveEnabled']['call'](this);},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x231)]=function(){const _0x45e744=_0xa2c6e5;if(this[_0x45e744(0xf6)]===undefined)this[_0x45e744(0x290)]();return this[_0x45e744(0xf6)]||Game_Party['defaultMaxBattleMembers'];},Game_Party['prototype'][_0xa2c6e5(0x181)]=function(){const _0x3aac86=_0xa2c6e5;if(this[_0x3aac86(0xf6)]===undefined)this[_0x3aac86(0x290)]();if(!this['_battleMembers'])this[_0x3aac86(0x290)]();while(this[_0x3aac86(0x1d6)][_0x3aac86(0x167)]<this[_0x3aac86(0xf6)]){this[_0x3aac86(0x1d6)][_0x3aac86(0x29f)](0x0);}},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x290)]=function(_0x2480e7){const _0x246d06=_0xa2c6e5;!_0x2480e7&&(this[_0x246d06(0xf6)]=Game_Party[_0x246d06(0x126)]);this[_0x246d06(0x1d6)]=this[_0x246d06(0x1df)][_0x246d06(0x18a)](0x0,this[_0x246d06(0xf6)]);while(this[_0x246d06(0x1d6)][_0x246d06(0x167)]<this['_battleMaxSize']){this[_0x246d06(0x1d6)][_0x246d06(0x29f)](0x0);}if($gamePlayer)$gamePlayer[_0x246d06(0x239)]();},Game_Party[_0xa2c6e5(0x24d)]['battleMembers']=function(){const _0x5c184b=_0xa2c6e5;return this[_0x5c184b(0x2a9)]()[_0x5c184b(0x160)](_0xbc7857=>!!_0xbc7857);},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2a9)]=function(){const _0x42aba0=_0xa2c6e5;this[_0x42aba0(0x181)]();const _0x55376d=this[_0x42aba0(0x1d6)][_0x42aba0(0x10e)](_0x55718b=>$gameActors[_0x42aba0(0xf3)](_0x55718b));return SceneManager[_0x42aba0(0x237)]()?_0x55376d:_0x55376d[_0x42aba0(0x160)](_0x2cece6=>_0x2cece6&&_0x2cece6['isAppeared']());},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x26a)]=function(){const _0x143516=_0xa2c6e5,_0x16c309=this[_0x143516(0xdf)]();return this[_0x143516(0x1ff)]()[_0x143516(0x160)](_0x24d4e7=>!_0x16c309['includes'](_0x24d4e7));},VisuMZ[_0xa2c6e5(0x2ae)]['Game_Party_setupStartingMembers']=Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x256)],Game_Party[_0xa2c6e5(0x24d)]['setupStartingMembers']=function(){const _0x238934=_0xa2c6e5;VisuMZ[_0x238934(0x2ae)][_0x238934(0x103)][_0x238934(0x15a)](this),this[_0x238934(0x290)]();},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe2)]=Game_Party[_0xa2c6e5(0x24d)]['setupBattleTest'],Game_Party['prototype']['setupBattleTest']=function(){const _0x493e8f=_0xa2c6e5;VisuMZ['PartySystem'][_0x493e8f(0xe2)][_0x493e8f(0x15a)](this),this[_0x493e8f(0x285)]();},Game_Party[_0xa2c6e5(0x24d)]['setupBattleTestMembers']=function(){const _0x253612=_0xa2c6e5;this[_0x253612(0xf6)]=Game_Party['defaultMaxBattleMembers'],this[_0x253612(0x1d6)]=[],this[_0x253612(0x1df)]=[];for(const _0x4b4877 of $dataSystem['testBattlers']){const _0x53893e=$gameActors[_0x253612(0xf3)](_0x4b4877[_0x253612(0x16d)]);if(!_0x53893e)continue;_0x53893e['changeLevel'](_0x4b4877['level'],![]),_0x53893e[_0x253612(0x104)](_0x4b4877[_0x253612(0x1f5)]),_0x53893e[_0x253612(0x124)](),this[_0x253612(0x1d6)]['push'](_0x4b4877['actorId']),this[_0x253612(0x1df)][_0x253612(0x29f)](_0x4b4877[_0x253612(0x16d)]);}while(this['_battleMembers'][_0x253612(0x167)]<this[_0x253612(0xf6)]){this[_0x253612(0x1d6)][_0x253612(0x29f)](0x0);}while(this['_battleMembers'][_0x253612(0x167)]>this[_0x253612(0x231)]()){this['_battleMembers'][_0x253612(0x1bd)]();}if($gamePlayer)$gamePlayer[_0x253612(0x239)]();},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x285)]=function(){const _0x11afd5=_0xa2c6e5,_0x453ce8=this[_0x11afd5(0xdf)]();for(let _0x49197d=0x1;_0x49197d<$dataActors['length'];_0x49197d++){const _0x18d4e9=$gameActors[_0x11afd5(0xf3)](_0x49197d);if(!_0x18d4e9)continue;if(_0x18d4e9[_0x11afd5(0x175)]()[_0x11afd5(0x167)]<=0x0)continue;if(_0x18d4e9['name']()['match'](/-----/i))continue;if(_0x453ce8[_0x11afd5(0x24e)](_0x18d4e9))continue;this['_actors']['push'](_0x18d4e9[_0x11afd5(0x16d)]());}},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x1de)]=Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x172)],Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x172)]=function(_0x57b58f){const _0x2681e4=_0xa2c6e5;VisuMZ['PartySystem'][_0x2681e4(0x1de)][_0x2681e4(0x15a)](this,_0x57b58f),this[_0x2681e4(0x28d)](_0x57b58f),SceneManager[_0x2681e4(0x20b)]()&&(Imported[_0x2681e4(0x23d)]&&BattleManager[_0x2681e4(0x18b)]()&&(BattleManager['removeActionBattlersOTB'](),BattleManager[_0x2681e4(0x234)]($gameActors['actor'](_0x57b58f))));},Game_Party[_0xa2c6e5(0x24d)]['addActorToBattleMembers']=function(_0x5565ad){const _0x4c70ad=_0xa2c6e5;this['checkInitBattleMembers']();if(this[_0x4c70ad(0x1d6)][_0x4c70ad(0x24e)](_0x5565ad))return;if(!this[_0x4c70ad(0x1df)]['includes'](_0x5565ad))return;if(!this[_0x4c70ad(0x1d6)][_0x4c70ad(0x24e)](0x0))return;const _0x3a9a99=$gameActors[_0x4c70ad(0xf3)](_0x5565ad);if(!_0x3a9a99)return;const _0xf01215=this['_battleMembers'][_0x4c70ad(0x197)](0x0);if(_0xf01215<0x0)return;this[_0x4c70ad(0x1d6)][_0xf01215]=_0x5565ad,_0x3a9a99[_0x4c70ad(0x282)](),this[_0x4c70ad(0x25f)]();},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0xdb)]=function(_0x2cf417,_0x8042d1){const _0x3121d3=_0xa2c6e5;this[_0x3121d3(0x181)]();if(this[_0x3121d3(0x1d6)][_0x3121d3(0x24e)](_0x2cf417))return;if(!this[_0x3121d3(0x1d6)][_0x3121d3(0x24e)](0x0))return;const _0x402dff=$gameActors[_0x3121d3(0xf3)](_0x2cf417);if(!_0x402dff)return;this['_battleMembers'][_0x8042d1]=_0x2cf417,_0x402dff[_0x3121d3(0x282)](),this[_0x3121d3(0x25f)]();},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x137)]=Game_Party['prototype'][_0xa2c6e5(0x273)],Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x273)]=function(_0x3f5051){const _0x4643ea=_0xa2c6e5;this[_0x4643ea(0x13e)](_0x3f5051),VisuMZ[_0x4643ea(0x2ae)][_0x4643ea(0x137)][_0x4643ea(0x15a)](this,_0x3f5051);},Game_Party['prototype'][_0xa2c6e5(0x13e)]=function(_0x29be0b){const _0x41f2cf=_0xa2c6e5;this[_0x41f2cf(0x181)]();if(!this['_battleMembers'][_0x41f2cf(0x24e)](_0x29be0b))return;if(_0x29be0b<=0x0)return;const _0x3ea12a=this[_0x41f2cf(0x1d6)][_0x41f2cf(0x197)](_0x29be0b);this[_0x41f2cf(0x1d6)][_0x3ea12a]=0x0,this[_0x41f2cf(0x1df)][_0x41f2cf(0x16b)](_0x29be0b),this[_0x41f2cf(0x1df)]['push'](_0x29be0b),this[_0x41f2cf(0x25f)]();},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x25f)]=function(){const _0x132d6b=_0xa2c6e5;this[_0x132d6b(0x17f)](),$gamePlayer[_0x132d6b(0x239)](),$gameMap[_0x132d6b(0x299)]();},Game_Party['prototype'][_0xa2c6e5(0x17f)]=function(){const _0xa8ce59=_0xa2c6e5;this[_0xa8ce59(0x181)]();const _0x1f7f24=this[_0xa8ce59(0xdf)]()['concat'](this['reserveMembers']());this[_0xa8ce59(0x1df)]=_0x1f7f24[_0xa8ce59(0x10e)](_0x3d56a3=>_0x3d56a3?_0x3d56a3[_0xa8ce59(0x16d)]():0x0)['remove'](0x0);},Game_Party['prototype'][_0xa2c6e5(0x169)]=function(){this['_actors']['sort']((_0x1ef059,_0x45f3b4)=>_0x1ef059-_0x45f3b4),this['rearrangePartyActors'](),this['partyChangeRefresh']();},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0xed)]=function(){const _0x59551f=_0xa2c6e5;for(const _0x274eda of this[_0x59551f(0x26a)]()){if(!_0x274eda)continue;if(_0x274eda['isRequiredInParty']())return!![];}return![];},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x205)]=Game_Party[_0xa2c6e5(0x24d)]['swapOrder'],Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1e0)]=function(_0x4aa9d8,_0x2a05f9){const _0x5665c4=_0xa2c6e5;VisuMZ[_0x5665c4(0x2ae)][_0x5665c4(0x205)]['call'](this,_0x4aa9d8,_0x2a05f9),this['swapOrderPartySystemPlugin'](_0x4aa9d8,_0x2a05f9);},Game_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x291)]=function(_0x77ccf,_0x3d97e4){const _0x254b0a=_0xa2c6e5;this[_0x254b0a(0x1d6)]=[];for(let _0x14f0be=0x0;_0x14f0be<this[_0x254b0a(0x1df)][_0x254b0a(0x167)];_0x14f0be++){if(this[_0x254b0a(0x1d6)][_0x254b0a(0x167)]>=this[_0x254b0a(0x231)]())break;this[_0x254b0a(0x1d6)][_0x14f0be]=this[_0x254b0a(0x1df)][_0x14f0be];}$gamePlayer[_0x254b0a(0x239)]();},VisuMZ[_0xa2c6e5(0x2ae)]['Game_Troop_increaseTurn']=Game_Troop[_0xa2c6e5(0x24d)][_0xa2c6e5(0xfa)],Game_Troop[_0xa2c6e5(0x24d)][_0xa2c6e5(0xfa)]=function(){const _0x401b44=_0xa2c6e5;VisuMZ['PartySystem'][_0x401b44(0x1d4)][_0x401b44(0x15a)](this),$gameParty[_0x401b44(0x15b)]();},Scene_Menu[_0xa2c6e5(0x24d)]['commandFormation']=function(){const _0x5d2386=_0xa2c6e5;SceneManager[_0x5d2386(0x29f)](Scene_Party);};function Scene_Party(){const _0x71ff00=_0xa2c6e5;this[_0x71ff00(0xde)](...arguments);}Scene_Party[_0xa2c6e5(0x24d)]=Object[_0xa2c6e5(0x183)](Scene_MenuBase[_0xa2c6e5(0x24d)]),Scene_Party[_0xa2c6e5(0x24d)]['constructor']=Scene_Party,Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0xde)]=function(){const _0x307674=_0xa2c6e5;this[_0x307674(0x120)](),Scene_MenuBase['prototype'][_0x307674(0xde)]['call'](this);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x210)]=function(){const _0x24242a=_0xa2c6e5;if(ConfigManager[_0x24242a(0x29e)]&&ConfigManager[_0x24242a(0x293)]!==undefined)return ConfigManager[_0x24242a(0x293)];else return ConfigManager[_0x24242a(0x29e)]===![]?![]:Scene_MenuBase[_0x24242a(0x24d)]['isRightInputMode']['call'](this);},Scene_Party['prototype']['helpAreaHeight']=function(){return 0x0;},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1ee)]=function(){return!![];},Scene_Party[_0xa2c6e5(0x24d)]['createPageButtons']=function(){const _0x35138f=_0xa2c6e5;Scene_MenuBase['prototype']['createPageButtons'][_0x35138f(0x15a)](this),this['_pageupButton'][_0x35138f(0x254)]=undefined,this[_0x35138f(0x271)][_0x35138f(0x254)]=undefined;},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x120)]=function(){const _0x189887=_0xa2c6e5;for(const _0x30a612 of $gameParty[_0x189887(0x2b7)]()){ImageManager['loadFace'](_0x30a612[_0x189887(0x10d)]()),ImageManager[_0x189887(0x241)](_0x30a612[_0x189887(0x1af)]()),ImageManager[_0x189887(0x18c)](_0x30a612['battlerName']());}},Scene_Party['prototype'][_0xa2c6e5(0x183)]=function(){const _0x5e9f56=_0xa2c6e5;Scene_MenuBase['prototype'][_0x5e9f56(0x183)][_0x5e9f56(0x15a)](this),this[_0x5e9f56(0x26f)](),this[_0x5e9f56(0x12b)](),this[_0x5e9f56(0x283)](),this[_0x5e9f56(0x2bb)](),this[_0x5e9f56(0x1ac)](),this[_0x5e9f56(0x22e)]();},Scene_Party['prototype']['createActivePartyLabel']=function(){const _0x553826=_0xa2c6e5,_0x35666b=this[_0x553826(0x246)]();this['_activePartyLabel']=new Window_PartyLabel(_0x35666b,TextManager[_0x553826(0x235)]),this[_0x553826(0x1ce)]['setBackgroundType'](VisuMZ[_0x553826(0x2ae)][_0x553826(0xe1)][_0x553826(0x13d)][_0x553826(0x10f)]),this[_0x553826(0x229)](this[_0x553826(0x1ce)]);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x246)]=function(){const _0x26e5c0=_0xa2c6e5;return VisuMZ[_0x26e5c0(0x2ae)][_0x26e5c0(0xe1)]['Window']['ActivePartyLabelRect'][_0x26e5c0(0x15a)](this);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x12b)]=function(){const _0x4dcbbe=_0xa2c6e5,_0x2927f9=this[_0x4dcbbe(0xee)]();this[_0x4dcbbe(0x2a1)]=new Window_PartyActive(_0x2927f9),this['_activePartyWindow']['setBackgroundType'](VisuMZ[_0x4dcbbe(0x2ae)][_0x4dcbbe(0xe1)]['Window'][_0x4dcbbe(0x166)]),this['_activePartyWindow'][_0x4dcbbe(0x228)]('ok',this['onActiveOk'][_0x4dcbbe(0x11a)](this)),this[_0x4dcbbe(0x2a1)][_0x4dcbbe(0x228)]('cancel',this[_0x4dcbbe(0xec)][_0x4dcbbe(0x11a)](this)),this[_0x4dcbbe(0x229)](this[_0x4dcbbe(0x2a1)]);},Scene_Party['prototype'][_0xa2c6e5(0xee)]=function(){const _0x1fd8a2=_0xa2c6e5;return VisuMZ[_0x1fd8a2(0x2ae)][_0x1fd8a2(0xe1)]['Window'][_0x1fd8a2(0x176)][_0x1fd8a2(0x15a)](this);},Scene_Party[_0xa2c6e5(0x24d)]['onActiveOk']=function(){const _0x20759d=_0xa2c6e5;this[_0x20759d(0x2ad)]['activate'](),this[_0x20759d(0x2ad)][_0x20759d(0x182)]();},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x283)]=function(){const _0x510b5b=_0xa2c6e5,_0x27340c=this[_0x510b5b(0x199)]();this[_0x510b5b(0x1be)]=new Window_PartyLabel(_0x27340c,TextManager[_0x510b5b(0x2a0)]),this[_0x510b5b(0x1be)]['setBackgroundType'](VisuMZ[_0x510b5b(0x2ae)]['Settings'][_0x510b5b(0x13d)][_0x510b5b(0x184)]),this[_0x510b5b(0x229)](this[_0x510b5b(0x1be)]);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x199)]=function(){const _0x366056=_0xa2c6e5;return VisuMZ[_0x366056(0x2ae)][_0x366056(0xe1)][_0x366056(0x13d)][_0x366056(0xf0)][_0x366056(0x15a)](this);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2bb)]=function(){const _0x158c74=_0xa2c6e5,_0x20e705=this[_0x158c74(0x14f)]();this[_0x158c74(0x2ad)]=new Window_PartyReserve(_0x20e705),this[_0x158c74(0x2ad)]['setBackgroundType'](VisuMZ[_0x158c74(0x2ae)][_0x158c74(0xe1)]['Window'][_0x158c74(0x1a6)]),this['_reservePartyWindow']['setHandler']('ok',this['onReserveOk'][_0x158c74(0x11a)](this)),this[_0x158c74(0x2ad)][_0x158c74(0x228)]('cancel',this['onReserveCancel'][_0x158c74(0x11a)](this)),this[_0x158c74(0x229)](this[_0x158c74(0x2ad)]);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x14f)]=function(){const _0x59a625=_0xa2c6e5;return VisuMZ[_0x59a625(0x2ae)]['Settings']['Window'][_0x59a625(0x1a7)][_0x59a625(0x15a)](this);},Scene_Party['prototype'][_0xa2c6e5(0x127)]=function(){const _0x15df5c=_0xa2c6e5,_0x146be4=this['_reservePartyWindow']['pendingIndex'](),_0x118388=this['_activePartyWindow'][_0x15df5c(0x201)]();if(_0x146be4<0x0){if(_0x118388)$gameParty[_0x15df5c(0x13e)](_0x118388['actorId']());}else{const _0xe3ee67=this[_0x15df5c(0x2ad)][_0x15df5c(0x201)]()[_0x15df5c(0x16d)](),_0x381722=this['_activePartyWindow'][_0x15df5c(0x233)]();if(_0x118388)$gameParty[_0x15df5c(0x13e)](_0x118388['actorId']());$gameParty[_0x15df5c(0xdb)](_0xe3ee67,_0x381722);}this[_0x15df5c(0x17d)](),this['onReserveCancel']();},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x17d)]=function(){const _0x4e0778=_0xa2c6e5;this[_0x4e0778(0x2a1)][_0x4e0778(0x239)](),this[_0x4e0778(0x2ad)][_0x4e0778(0x239)]();},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x27a)]=function(){const _0x2d6ec6=_0xa2c6e5;this[_0x2d6ec6(0x2ad)]['deactivate'](),this[_0x2d6ec6(0x2ad)][_0x2d6ec6(0x1e1)](),this['_activePartyWindow'][_0x2d6ec6(0x22a)]();},Scene_Party['prototype'][_0xa2c6e5(0x1ac)]=function(){const _0x11d038=_0xa2c6e5,_0x7526e=this[_0x11d038(0x13f)]();this['_statusPartyLabel']=new Window_PartyLabel(_0x7526e,TextManager[_0x11d038(0xfc)]),this[_0x11d038(0x130)][_0x11d038(0x240)](VisuMZ['PartySystem'][_0x11d038(0xe1)][_0x11d038(0x13d)]['StatusLabelBgType']),this[_0x11d038(0x229)](this[_0x11d038(0x130)]);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x13f)]=function(){const _0xb6e29a=_0xa2c6e5;return VisuMZ[_0xb6e29a(0x2ae)][_0xb6e29a(0xe1)][_0xb6e29a(0x13d)][_0xb6e29a(0x14d)][_0xb6e29a(0x15a)](this);},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x22e)]=function(){const _0x3d7a9d=_0xa2c6e5,_0x197059=this['statusWindowRect']();this[_0x3d7a9d(0x1e2)]=new Window_PartyStatus(_0x197059),this[_0x3d7a9d(0x1e2)][_0x3d7a9d(0x240)](VisuMZ['PartySystem'][_0x3d7a9d(0xe1)][_0x3d7a9d(0x13d)][_0x3d7a9d(0x13c)]),this[_0x3d7a9d(0x229)](this[_0x3d7a9d(0x1e2)]),this[_0x3d7a9d(0x2ad)]['setStatusWindow'](this[_0x3d7a9d(0x1e2)]),this[_0x3d7a9d(0x2a1)][_0x3d7a9d(0x18e)](this[_0x3d7a9d(0x1e2)]);},Scene_Party[_0xa2c6e5(0x24d)]['statusWindowRect']=function(){const _0x1b3fa7=_0xa2c6e5;return VisuMZ['PartySystem'][_0x1b3fa7(0xe1)]['Window'][_0x1b3fa7(0xd6)]['call'](this);},Scene_Party[_0xa2c6e5(0x24d)]['buttonAssistKey3']=function(){const _0x572eac=_0xa2c6e5;return TextManager['getInputButtonString'](_0x572eac(0x20f));},Scene_Party[_0xa2c6e5(0x24d)]['buttonAssistText1']=function(){const _0x4751cd=_0xa2c6e5;return TextManager[_0x4751cd(0x23f)];},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1d0)]=function(){const _0x3106a6=_0xa2c6e5,_0x52c0e3=this[_0x3106a6(0x2a1)],_0x222a97=this['_reservePartyWindow'];if(_0x52c0e3&&_0x52c0e3[_0x3106a6(0x1a3)]&&_0x52c0e3['currentActor']()&&_0x52c0e3[_0x3106a6(0x284)]())return TextManager['assistRemovePartyMember'];else return _0x222a97&&_0x222a97[_0x3106a6(0x1a3)]&&$gameParty[_0x3106a6(0x26a)]()[_0x3106a6(0x167)]>0x0?TextManager[_0x3106a6(0x217)]:'';},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x139)]=function(){const _0x3548bc=_0xa2c6e5;if(this[_0x3548bc(0x2a1)]&&this[_0x3548bc(0x2a1)][_0x3548bc(0x1a3)])return TextManager['assistSwapOutPartyMember'];else return this['_reservePartyWindow']&&this[_0x3548bc(0x2ad)]['active']?TextManager[_0x3548bc(0x265)]:Scene_MenuBase[_0x3548bc(0x24d)][_0x3548bc(0x139)]['call'](this);},Scene_Party['prototype'][_0xa2c6e5(0x281)]=function(){const _0x51b3db=_0xa2c6e5;Scene_MenuBase[_0x51b3db(0x24d)][_0x51b3db(0x281)][_0x51b3db(0x15a)](this),this[_0x51b3db(0x113)](this[_0x51b3db(0x24f)]()),this[_0x51b3db(0x200)]();},Scene_Party['prototype'][_0xa2c6e5(0x24f)]=function(){const _0x43bfb9=_0xa2c6e5;return VisuMZ[_0x43bfb9(0x2ae)]['Settings'][_0x43bfb9(0x138)][_0x43bfb9(0x1b3)];},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x200)]=function(){const _0x24b90b=_0xa2c6e5,_0x3afe8d={'BgFilename1':VisuMZ['PartySystem'][_0x24b90b(0xe1)][_0x24b90b(0x138)]['BgFilename1'],'BgFilename2':VisuMZ[_0x24b90b(0x2ae)][_0x24b90b(0xe1)][_0x24b90b(0x138)][_0x24b90b(0x2ab)]};_0x3afe8d&&(_0x3afe8d['BgFilename1']!==''||_0x3afe8d[_0x24b90b(0x2ab)]!=='')&&(this[_0x24b90b(0x272)]=new Sprite(ImageManager[_0x24b90b(0x1f6)](_0x3afe8d['BgFilename1'])),this[_0x24b90b(0x211)]=new Sprite(ImageManager[_0x24b90b(0x121)](_0x3afe8d[_0x24b90b(0x2ab)])),this[_0x24b90b(0x12d)](this['_backSprite1']),this['addChild'](this[_0x24b90b(0x211)]),this[_0x24b90b(0x272)][_0x24b90b(0x2b9)][_0x24b90b(0x27d)](this[_0x24b90b(0x17a)][_0x24b90b(0x11a)](this,this['_backSprite1'])),this[_0x24b90b(0x211)][_0x24b90b(0x2b9)][_0x24b90b(0x27d)](this[_0x24b90b(0x17a)][_0x24b90b(0x11a)](this,this['_backSprite2'])));},Scene_Party[_0xa2c6e5(0x24d)][_0xa2c6e5(0x17a)]=function(_0x36b3bc){const _0x3368f4=_0xa2c6e5;this[_0x3368f4(0x260)](_0x36b3bc),this[_0x3368f4(0x298)](_0x36b3bc);},Scene_Party['prototype'][_0xa2c6e5(0x1fb)]=function(){const _0xa2c308=_0xa2c6e5;Scene_MenuBase['prototype'][_0xa2c308(0x1fb)][_0xa2c308(0x15a)](this),$gameParty['partyChangeRefresh']();},Window_StatusBase[_0xa2c6e5(0x24d)][_0xa2c6e5(0x14a)]=function(_0x47d9af,_0x577d44,_0xc85258,_0x37a0b3){const _0x5bee22=_0xa2c6e5;if(!_0x47d9af)return;_0x37a0b3?this[_0x5bee22(0x1cc)](_0x47d9af,_0x577d44,_0xc85258):this['drawActorPartyIconsHorz'](_0x47d9af,_0x577d44,_0xc85258);},Window_StatusBase[_0xa2c6e5(0x24d)]['drawActorPartyIconsHorz']=function(_0x4e9585,_0x35d53b,_0xc24826){const _0x4e909d=_0xa2c6e5;_0xc24826+=Math[_0x4e909d(0x116)]((this[_0x4e909d(0x226)]()-ImageManager[_0x4e909d(0x154)])/0x2),!_0x4e9585['isFormationChangeOk']()&&(this[_0x4e909d(0x1e9)](ImageManager[_0x4e909d(0x27e)],_0x35d53b,_0xc24826),_0x35d53b+=ImageManager[_0x4e909d(0x1c4)]+0x4),_0x4e9585[_0x4e909d(0x122)]()&&(this[_0x4e909d(0x1e9)](ImageManager['requiredPartyMemberIcon'],_0x35d53b,_0xc24826),_0x35d53b+=ImageManager[_0x4e909d(0x1c4)]+0x4);},Window_StatusBase[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1cc)]=function(_0x38a3af,_0x4c6248,_0x746e6e){const _0xf086a7=_0xa2c6e5;let _0x350840=0x0;if(!_0x38a3af[_0xf086a7(0xef)]())_0x350840+=0x1;if(_0x38a3af[_0xf086a7(0x122)]())_0x350840+=0x1;if(_0x350840<=0x1)return this['drawActorPartyIconsHorz'](_0x38a3af,_0x4c6248,_0x746e6e);_0x746e6e+=Math[_0xf086a7(0x116)]((this[_0xf086a7(0x226)]()-ImageManager['iconHeight'])/0x2),_0x746e6e-=Math[_0xf086a7(0x116)](this['lineHeight']()/0x2),this[_0xf086a7(0x1e9)](ImageManager[_0xf086a7(0x27e)],_0x4c6248,_0x746e6e),_0x746e6e+=this['lineHeight'](),this['drawIcon'](ImageManager[_0xf086a7(0x152)],_0x4c6248,_0x746e6e);};function Window_PartyLabel(){this['initialize'](...arguments);}Window_PartyLabel[_0xa2c6e5(0x24d)]=Object[_0xa2c6e5(0x183)](Window_Base['prototype']),Window_PartyLabel[_0xa2c6e5(0x24d)]['constructor']=Window_PartyLabel,Window_PartyLabel[_0xa2c6e5(0x24d)][_0xa2c6e5(0xde)]=function(_0x54c3a8,_0x4cfd68){const _0x162801=_0xa2c6e5;Window_Base[_0x162801(0x24d)][_0x162801(0xde)]['call'](this,_0x54c3a8),this[_0x162801(0x19f)](_0x4cfd68);},Window_PartyLabel['prototype'][_0xa2c6e5(0xeb)]=function(){this['padding']=0x0;},Window_PartyLabel[_0xa2c6e5(0x24d)][_0xa2c6e5(0x19f)]=function(_0x505d4f){const _0x3715fc=_0xa2c6e5;this[_0x3715fc(0x2a2)][_0x3715fc(0x178)](),this[_0x3715fc(0x170)](_0x505d4f,0x0,0x0,this[_0x3715fc(0x295)],_0x3715fc(0x106));};function Window_PartyActive(){const _0x4938d9=_0xa2c6e5;this[_0x4938d9(0xde)](...arguments);}Window_PartyActive[_0xa2c6e5(0x24d)]=Object[_0xa2c6e5(0x183)](Window_StatusBase[_0xa2c6e5(0x24d)]),Window_PartyActive['prototype'][_0xa2c6e5(0x1b4)]=Window_PartyActive,Window_PartyActive[_0xa2c6e5(0x28e)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x13d)][_0xa2c6e5(0x247)],Window_PartyActive[_0xa2c6e5(0x24d)]['initialize']=function(_0x380db3){const _0x11f8b9=_0xa2c6e5;Window_StatusBase[_0x11f8b9(0x24d)][_0x11f8b9(0xde)][_0x11f8b9(0x15a)](this,_0x380db3),this[_0x11f8b9(0x239)](),this[_0x11f8b9(0x22a)](),this[_0x11f8b9(0x202)](0x0);},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x278)]=function(){const _0xf06793=_0xa2c6e5;return VisuMZ[_0xf06793(0x2ae)][_0xf06793(0xe1)][_0xf06793(0x1ba)][_0xf06793(0x1b2)];},Window_PartyActive[_0xa2c6e5(0x24d)]['maxItems']=function(){const _0x3ad2b6=_0xa2c6e5;return $gameParty[_0x3ad2b6(0x231)]();},Window_PartyActive[_0xa2c6e5(0x24d)]['maxCols']=function(){const _0x41ffbd=_0xa2c6e5;return $gameParty[_0x41ffbd(0x231)]();},Window_PartyActive['prototype']['itemHeight']=function(){return this['innerHeight'];},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0xf3)]=function(_0x170719){const _0x1922f8=_0xa2c6e5;return $gameParty[_0x1922f8(0x2a9)]()[_0x170719];},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x201)]=function(){const _0x53453d=_0xa2c6e5;return this[_0x53453d(0xf3)](this['index']());},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x156)]=function(){const _0x5119a8=_0xa2c6e5,_0x4693bf=this['actor'](this[_0x5119a8(0x233)]());return _0x4693bf?_0x4693bf['isFormationChangeOk']():!![];},Window_PartyActive[_0xa2c6e5(0x24d)]['isCancelEnabled']=function(){const _0x3476f0=_0xa2c6e5;if($gameParty['members']()[_0x3476f0(0x167)]<=0x0)return!![];if($gameParty['anyRequiredPartyMembersInReserve']())return![];return $gameParty['battleMembers']()[_0x3476f0(0x167)]>0x0;},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0xdd)]=function(){const _0x3599f0=_0xa2c6e5;Window_StatusBase[_0x3599f0(0x24d)]['processCursorMove']['call'](this),this['checkShiftRemoveShortcut']();},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x145)]=function(_0x5a29d4){const _0x2f5fcd=_0xa2c6e5;this['isOkEnabled']()&&this[_0x2f5fcd(0x10a)]();},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x147)]=function(){const _0x42a4e6=_0xa2c6e5,_0x11eff8=this['index'](),_0x40f8e7=_0x11eff8+0x1>=this[_0x42a4e6(0x108)]()?0x0:_0x11eff8+0x1;this[_0x42a4e6(0x25a)](_0x11eff8,_0x40f8e7);},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x28f)]=function(){const _0x2d5f90=_0xa2c6e5,_0x2166a1=this[_0x2d5f90(0x233)](),_0x49750f=_0x2166a1-0x1<0x0?this[_0x2d5f90(0x108)]()-0x1:_0x2166a1-0x1;this[_0x2d5f90(0x25a)](_0x2166a1,_0x49750f);},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x25a)]=function(_0x2d2ccb,_0x34c4fa){const _0x40ab47=_0xa2c6e5,_0x506954=this[_0x40ab47(0xf3)](_0x2d2ccb),_0x202918=this[_0x40ab47(0xf3)](_0x34c4fa);if(_0x506954&&!_0x506954[_0x40ab47(0xef)]())return;if(_0x202918&&!_0x202918[_0x40ab47(0xef)]())return;const _0x38fec6=$gameParty[_0x40ab47(0x1d6)];_0x38fec6[_0x2d2ccb]=_0x202918?_0x202918['actorId']():0x0,_0x38fec6[_0x34c4fa]=_0x506954?_0x506954['actorId']():0x0,this[_0x40ab47(0x239)](),this[_0x40ab47(0x26b)](),this[_0x40ab47(0x202)](_0x34c4fa);},Window_PartyActive[_0xa2c6e5(0x24d)]['checkShiftRemoveShortcut']=function(){const _0x2527fd=_0xa2c6e5;if(!this[_0x2527fd(0x284)]())return;if(Input[_0x2527fd(0x236)](_0x2527fd(0x20f))){const _0xcd888f=this[_0x2527fd(0x201)]();this[_0x2527fd(0x268)]();}},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x268)]=function(){const _0xb4f919=_0xa2c6e5;SoundManager[_0xb4f919(0x245)]();const _0x2d6331=this[_0xb4f919(0x201)]();$gameParty['removeActorFromBattleMembers'](_0x2d6331[_0xb4f919(0x16d)]()),this['callUpdateHelp'](),SceneManager[_0xb4f919(0x1d1)][_0xb4f919(0x17d)]();},Window_PartyActive['prototype'][_0xa2c6e5(0x284)]=function(){const _0x189f66=_0xa2c6e5;if(!this[_0x189f66(0x278)]())return![];const _0x404a3b=this[_0x189f66(0x201)]();return this[_0x189f66(0x1a3)]&&_0x404a3b&&_0x404a3b[_0x189f66(0xef)]();},Window_PartyActive[_0xa2c6e5(0x24d)]['drawItem']=function(_0x37307b){const _0x5a328d=_0xa2c6e5,_0xa64cd8=this[_0x5a328d(0xf3)](_0x37307b);if(!_0xa64cd8)return this[_0x5a328d(0x144)](_0x37307b);this[_0x5a328d(0x1cb)]();const _0x38fb98=this[_0x5a328d(0x1c5)](_0x37307b);this[_0x5a328d(0x1e5)](_0x37307b);const _0x556fd1=_0x38fb98['y']+_0x38fb98[_0x5a328d(0x220)]-this[_0x5a328d(0x226)]();this[_0x5a328d(0x153)](_0x38fb98['x'],_0x556fd1,_0x38fb98['width'],0x2),this[_0x5a328d(0x14a)](_0xa64cd8,_0x38fb98['x']+0x2,_0x38fb98['y']),this[_0x5a328d(0x105)](_0xa64cd8,_0x38fb98['x'],_0x556fd1,_0x38fb98[_0x5a328d(0x29d)]);},Window_PartyActive[_0xa2c6e5(0x24d)]['drawItemEmpty']=function(_0x5dbd49){const _0x2728a0=_0xa2c6e5;this[_0x2728a0(0x1cb)]();const _0x75d32a=this[_0x2728a0(0x1c5)](_0x5dbd49);this['drawItemDarkRect'](_0x75d32a['x'],_0x75d32a['y'],_0x75d32a[_0x2728a0(0x29d)],_0x75d32a[_0x2728a0(0x220)]);const _0x128bbf=_0x75d32a['y']+Math[_0x2728a0(0x116)]((_0x75d32a[_0x2728a0(0x220)]-this[_0x2728a0(0x226)]())/0x2);this[_0x2728a0(0x292)](ColorManager['systemColor']()),this[_0x2728a0(0x170)](TextManager[_0x2728a0(0x255)],_0x75d32a['x'],_0x128bbf,_0x75d32a[_0x2728a0(0x29d)],_0x2728a0(0x106));},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x28c)]=function(_0x258bff,_0x567cff,_0xb99324,_0x8c4ccd,_0x5002a9){const _0x8c5c21=_0xa2c6e5;_0x5002a9=Math[_0x8c5c21(0x2b1)](_0x5002a9||0x1,0x1);while(_0x5002a9--){_0x8c4ccd=_0x8c4ccd||this[_0x8c5c21(0x226)](),this[_0x8c5c21(0x2a2)][_0x8c5c21(0x135)]=0xa0;const _0x43ddb2=ColorManager[_0x8c5c21(0x2a7)]();this[_0x8c5c21(0x2a2)][_0x8c5c21(0x243)](_0x258bff+0x1,_0x567cff+0x1,_0xb99324-0x2,_0x8c4ccd-0x2,_0x43ddb2),this[_0x8c5c21(0x2a2)][_0x8c5c21(0x135)]=0xff;}},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1e5)]=function(_0x114729){const _0x5bfb32=_0xa2c6e5;switch(Window_PartyActive[_0x5bfb32(0x28e)]['toLowerCase']()[_0x5bfb32(0xd8)]()){case _0x5bfb32(0x208):this[_0x5bfb32(0x195)](_0x114729);break;case'sprite':this[_0x5bfb32(0x1bc)](_0x114729);break;case'svbattler':Imported[_0x5bfb32(0x1c2)]&&this[_0x5bfb32(0x185)](_0x114729);break;};},Window_PartyActive['prototype']['drawItemImageFace']=function(_0x535baf){const _0x4a140e=_0xa2c6e5,_0x4b7df0=this['actor'](_0x535baf),_0xf4711c=this['itemRect'](_0x535baf),_0x84ae43=Math['min'](ImageManager[_0x4a140e(0x2af)],_0xf4711c[_0x4a140e(0x29d)]-0x2),_0x1d84c4=_0xf4711c[_0x4a140e(0x220)]-0x2;this[_0x4a140e(0xe0)](_0x4b7df0[_0x4a140e(0xef)]());const _0xea0a3=Math[_0x4a140e(0x116)](_0xf4711c['x']+(_0xf4711c['width']-_0x84ae43)/0x2);this[_0x4a140e(0x12f)](_0x4b7df0,_0xea0a3,_0xf4711c['y']+0x1,_0x84ae43,_0x1d84c4),this[_0x4a140e(0xe0)](!![]);},Window_PartyActive['prototype'][_0xa2c6e5(0x1bc)]=function(_0x385a19){const _0x25b451=_0xa2c6e5,_0x100800=this[_0x25b451(0xf3)](_0x385a19),_0x21ba1c=this['itemRect'](_0x385a19),_0x55f7bd=VisuMZ[_0x25b451(0x2ae)]['Settings'][_0x25b451(0x13d)],_0x382c1d=_0x21ba1c['x']+Math[_0x25b451(0x116)](_0x21ba1c[_0x25b451(0x29d)]/0x2)+_0x55f7bd[_0x25b451(0x24c)],_0x35d80e=_0x21ba1c['y']+_0x21ba1c[_0x25b451(0x220)]-this[_0x25b451(0x226)]()-_0x55f7bd[_0x25b451(0x111)];this[_0x25b451(0x164)](_0x100800,_0x382c1d,_0x35d80e);},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x185)]=function(_0x100297){const _0x348eff=_0xa2c6e5,_0x139300=this['actor'](_0x100297),_0x34af2a=_0x139300[_0x348eff(0x177)](),_0x3f3403=this[_0x348eff(0x1c5)](_0x100297),_0xd868f0=VisuMZ[_0x348eff(0x2ae)]['Settings'][_0x348eff(0x13d)],_0x41c4ef=_0x3f3403['x']+Math[_0x348eff(0x116)](_0x3f3403[_0x348eff(0x29d)]/0x2)+_0xd868f0[_0x348eff(0x10b)],_0x528471=_0x3f3403['y']+_0x3f3403[_0x348eff(0x220)]-this[_0x348eff(0x226)]()-_0xd868f0['ActiveBattlerOffsetY'];this[_0x348eff(0x26e)](_0x34af2a,_0x41c4ef,_0x528471);},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x153)]=function(_0x4e5062,_0x4913f3,_0x1f8a97,_0x10d4ac){const _0x2cd17f=_0xa2c6e5,_0x3b0d61=ColorManager[_0x2cd17f(0x22f)](),_0x48cf05=ColorManager['dimColor2'](),_0x5b851d=_0x1f8a97/0x2,_0x4f548c=this[_0x2cd17f(0x226)]();while(_0x10d4ac--){this[_0x2cd17f(0x2a2)][_0x2cd17f(0x1e6)](_0x4e5062,_0x4913f3,_0x5b851d,_0x4f548c,_0x48cf05,_0x3b0d61),this[_0x2cd17f(0x2a2)][_0x2cd17f(0x1e6)](_0x4e5062+_0x5b851d,_0x4913f3,_0x5b851d,_0x4f548c,_0x3b0d61,_0x48cf05);}},Window_PartyActive['prototype']['drawActorName']=function(_0x4520c9,_0x2e6fa3,_0x30b4b7,_0x465bc8){const _0x4d8a5a=_0xa2c6e5;_0x465bc8=_0x465bc8||0xa8,this[_0x4d8a5a(0x292)](ColorManager['hpColor'](_0x4520c9)),this[_0x4d8a5a(0x170)](_0x4520c9['name'](),_0x2e6fa3,_0x30b4b7,_0x465bc8,'center');},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x18e)]=function(_0x1d693a){const _0x451b4e=_0xa2c6e5;this[_0x451b4e(0x2c1)]=_0x1d693a,this[_0x451b4e(0x258)]();},Window_PartyActive[_0xa2c6e5(0x24d)][_0xa2c6e5(0x258)]=function(){const _0x2ef5e5=_0xa2c6e5;if(this[_0x2ef5e5(0x2c1)])this[_0x2ef5e5(0x2c1)][_0x2ef5e5(0x2bf)](this[_0x2ef5e5(0xf3)](this[_0x2ef5e5(0x233)]()));};function Window_PartyReserve(){const _0x30f127=_0xa2c6e5;this[_0x30f127(0xde)](...arguments);}Window_PartyReserve['prototype']=Object[_0xa2c6e5(0x183)](Window_StatusBase['prototype']),Window_PartyReserve['prototype'][_0xa2c6e5(0x1b4)]=Window_PartyReserve,Window_PartyReserve[_0xa2c6e5(0x28e)]=VisuMZ['PartySystem'][_0xa2c6e5(0xe1)][_0xa2c6e5(0x13d)][_0xa2c6e5(0x1fd)],Window_PartyReserve['_rowThickness']=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x13d)][_0xa2c6e5(0x262)],Window_PartyReserve['prototype'][_0xa2c6e5(0xde)]=function(_0x5aa2fc){const _0x827f63=_0xa2c6e5;Window_StatusBase[_0x827f63(0x24d)][_0x827f63(0xde)][_0x827f63(0x15a)](this,_0x5aa2fc),this['_lastIndex']=0x0,this[_0x827f63(0x239)]();},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0xff)]=function(){const _0x38dee4=_0xa2c6e5;return VisuMZ['PartySystem'][_0x38dee4(0xe1)][_0x38dee4(0x13d)]['ReserveCol']||0x1;},Window_PartyReserve[_0xa2c6e5(0x24d)]['itemHeight']=function(){const _0xc9b042=_0xa2c6e5;return this[_0xc9b042(0x226)]()*Window_PartyReserve[_0xc9b042(0x2be)]+0x6;},Window_PartyReserve['prototype'][_0xa2c6e5(0x278)]=function(){const _0x4e078d=_0xa2c6e5;return VisuMZ[_0x4e078d(0x2ae)][_0x4e078d(0xe1)][_0x4e078d(0x1ba)]['AddRemoveCmd'];},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x108)]=function(){const _0x4b9a58=_0xa2c6e5;let _0xe7cfe8=$gameParty[_0x4b9a58(0x26a)]()[_0x4b9a58(0x167)];if(this[_0x4b9a58(0x278)]())_0xe7cfe8++;return _0xe7cfe8;},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0xf3)]=function(_0x1b9af1){const _0x43e777=_0xa2c6e5;return $gameParty[_0x43e777(0x26a)]()[_0x1b9af1];},Window_PartyReserve['prototype'][_0xa2c6e5(0x201)]=function(){const _0xa9288d=_0xa2c6e5;return this[_0xa9288d(0xf3)](this['index']());},Window_PartyReserve['prototype'][_0xa2c6e5(0x1d9)]=function(){const _0xbb6656=_0xa2c6e5;SoundManager[_0xbb6656(0x245)]();},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x156)]=function(){const _0x240c8a=_0xa2c6e5,_0x196304=this[_0x240c8a(0xf3)](this[_0x240c8a(0x233)]());return _0x196304?_0x196304[_0x240c8a(0xef)]():!![];},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0xdd)]=function(){const _0x473825=_0xa2c6e5;Window_StatusBase[_0x473825(0x24d)][_0x473825(0xdd)][_0x473825(0x15a)](this),this[_0x473825(0x118)]();},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2c0)]=function(_0x35568d){const _0x1de5df=_0xa2c6e5;this['index']()<=0x0?this[_0x1de5df(0xe8)]():Window_StatusBase[_0x1de5df(0x24d)][_0x1de5df(0x2c0)][_0x1de5df(0x15a)](this,_0x35568d);},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x147)]=function(){const _0x477032=_0xa2c6e5,_0x422743=this[_0x477032(0x233)](),_0x297d03=_0x422743+0x1>=this['maxItems']()-0x1?0x0:_0x422743+0x1;this[_0x477032(0x25a)](_0x422743,_0x297d03);},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x28f)]=function(){const _0x280d8c=_0xa2c6e5,_0x4b1b03=this['index'](),_0x4d462e=_0x4b1b03-0x1<0x0?this[_0x280d8c(0x108)]()-0x2:_0x4b1b03-0x1;this[_0x280d8c(0x25a)](_0x4b1b03,_0x4d462e);},Window_PartyReserve['prototype'][_0xa2c6e5(0x25a)]=function(_0x300d00,_0x57746c){const _0x131a92=_0xa2c6e5,_0x168a2e=this[_0x131a92(0xf3)](_0x300d00),_0x5d8955=this[_0x131a92(0xf3)](_0x57746c);if(!_0x168a2e?.[_0x131a92(0xef)]()||!_0x5d8955?.['isFormationChangeOk']())return;else{if(!_0x168a2e||!_0x5d8955)return;}const _0x5746a2=$gameParty[_0x131a92(0x1df)],_0x138634=_0x5746a2['indexOf'](_0x168a2e[_0x131a92(0x16d)]()),_0x1d04fe=_0x5746a2[_0x131a92(0x197)](_0x5d8955['actorId']());_0x5746a2[_0x138634]=_0x5d8955?_0x5d8955[_0x131a92(0x16d)]():0x0,_0x5746a2[_0x1d04fe]=_0x168a2e?_0x168a2e['actorId']():0x0,this[_0x131a92(0x239)](),this[_0x131a92(0x26b)](),this[_0x131a92(0x202)](_0x57746c);},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x118)]=function(){const _0x19a9d5=_0xa2c6e5;if(!this[_0x19a9d5(0x238)]())return;Input[_0x19a9d5(0x236)](_0x19a9d5(0x20f))&&this[_0x19a9d5(0xf4)]();},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0xf4)]=function(){const _0x2adb65=_0xa2c6e5;SoundManager['playEquip'](),$gameParty['sortActors'](),this[_0x2adb65(0x202)](0x0),SceneManager[_0x2adb65(0x1d1)][_0x2adb65(0x17d)]();},Window_PartyReserve[_0xa2c6e5(0x24d)]['isShiftShortcutEnabled']=function(){const _0x499f7e=_0xa2c6e5;return this[_0x499f7e(0x1a3)];},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1e7)]=function(){const _0x2a1362=_0xa2c6e5,_0x53176c=this[_0x2a1362(0x201)]();return _0x53176c?_0x53176c[_0x2a1362(0x233)]():-0x1;},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x232)]=function(_0x3fe110){const _0x5b2912=_0xa2c6e5;Window_StatusBase[_0x5b2912(0x24d)][_0x5b2912(0x232)][_0x5b2912(0x15a)](this,_0x3fe110);if(_0x3fe110>=0x0)this['_lastIndex']=_0x3fe110;},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x182)]=function(){const _0x436a16=_0xa2c6e5;this[_0x436a16(0x165)]=Math[_0x436a16(0x1cf)](this[_0x436a16(0x165)],this[_0x436a16(0x108)]()-0x1),this[_0x436a16(0x202)](this[_0x436a16(0x165)]),this['ensureCursorVisible'](!![]),this[_0x436a16(0x115)]=!![];},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x215)]=function(_0x2f5a5a){const _0x4ce0d2=_0xa2c6e5,_0x4a59e2=this[_0x4ce0d2(0xf3)](_0x2f5a5a);if(!_0x4a59e2)return this[_0x4ce0d2(0x1b0)](_0x2f5a5a);const _0x501846=this[_0x4ce0d2(0x203)](_0x2f5a5a);this[_0x4ce0d2(0x1e5)](_0x2f5a5a);const _0x2fcb79=0xa8,_0x2cb4bc=Window_PartyReserve[_0x4ce0d2(0x2be)]===0x1,_0x4240ab=ImageManager[_0x4ce0d2(0x1c4)]*(_0x2cb4bc?0x2:0x1),_0x31bc2b=this['nameStartPosition']()+this[_0x4ce0d2(0x22d)](),_0x314cd2=_0x501846['width']-_0x2fcb79,_0x2222da=_0x501846['x']+_0x4240ab+Math[_0x4ce0d2(0x1cf)](_0x31bc2b,_0x314cd2),_0x1fde34=_0x2cb4bc?![]:!![];this[_0x4ce0d2(0xe0)](_0x4a59e2[_0x4ce0d2(0xef)]()),this['drawActorPartyIcons'](_0x4a59e2,_0x501846['x'],_0x501846['y'],_0x1fde34),this['drawActorName'](_0x4a59e2,_0x2222da,_0x501846['y'],_0x2fcb79),this[_0x4ce0d2(0xe0)](!![]);},Window_PartyReserve['prototype'][_0xa2c6e5(0x216)]=function(){const _0x5ad512=_0xa2c6e5,_0x3a8d2a=VisuMZ[_0x5ad512(0x2ae)]['Settings'][_0x5ad512(0x13d)];switch(Window_PartyReserve[_0x5ad512(0x28e)][_0x5ad512(0x2ac)]()[_0x5ad512(0xd8)]()){case'face':return ImageManager[_0x5ad512(0x2af)];case _0x5ad512(0xd9):return _0x3a8d2a['ReserveSpriteOffsetX']*0x2;case'svbattler':return _0x3a8d2a['ReserveBattlerOffsetX']*0x2;};},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1b0)]=function(_0x4c387c){const _0xa79cfa=_0xa2c6e5,_0x2595c1=this[_0xa79cfa(0x203)](_0x4c387c);this['changePaintOpacity'](!![]);const _0x506eec=TextManager['removePartyMember'];this[_0xa79cfa(0x170)](_0x506eec,_0x2595c1['x'],_0x2595c1['y'],_0x2595c1[_0xa79cfa(0x29d)],_0xa79cfa(0x106));},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1e5)]=function(_0x8b046e){const _0x58db41=_0xa2c6e5;switch(Window_PartyReserve[_0x58db41(0x28e)][_0x58db41(0x2ac)]()[_0x58db41(0xd8)]()){case'face':this['drawItemImageFace'](_0x8b046e);break;case _0x58db41(0xd9):this[_0x58db41(0x1bc)](_0x8b046e);break;case _0x58db41(0x1d5):Imported[_0x58db41(0x1c2)]&&this[_0x58db41(0x185)](_0x8b046e);break;};},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x195)]=function(_0x575d02){const _0x12df29=_0xa2c6e5,_0x5a9b27=this['actor'](_0x575d02),_0x18a150=this[_0x12df29(0x1c5)](_0x575d02),_0xcf1ef9=Window_PartyReserve['_rowThickness']===0x1;_0x18a150['x']+=ImageManager[_0x12df29(0x1c4)]*(_0xcf1ef9?0x2:0x1);const _0x532063=ImageManager[_0x12df29(0x2af)],_0x544452=_0x18a150[_0x12df29(0x220)]-0x2;this[_0x12df29(0xe0)](_0x5a9b27['isFormationChangeOk']()),this['drawActorFace'](_0x5a9b27,_0x18a150['x']+0x1,_0x18a150['y']+0x1,_0x532063,_0x544452),this['changePaintOpacity'](!![]);},Window_PartyReserve[_0xa2c6e5(0x24d)]['drawItemImageSprite']=function(_0x5856ed){const _0x5963f3=_0xa2c6e5,_0x492c04=this[_0x5963f3(0xf3)](_0x5856ed),_0x5aa7cc=this[_0x5963f3(0x1c5)](_0x5856ed),_0x53ad31=Window_PartyReserve[_0x5963f3(0x2be)]===0x1;_0x5aa7cc['x']+=ImageManager[_0x5963f3(0x1c4)]*(_0x53ad31?0x2:0x1);const _0x5bb235=VisuMZ[_0x5963f3(0x2ae)]['Settings'][_0x5963f3(0x13d)],_0x4d5341=_0x5aa7cc['x']+_0x5bb235[_0x5963f3(0x15f)]+this[_0x5963f3(0x22d)](),_0x1aaf62=_0x5aa7cc['y']+_0x5aa7cc[_0x5963f3(0x220)]-_0x5bb235[_0x5963f3(0x1b1)];this[_0x5963f3(0x164)](_0x492c04,_0x4d5341,_0x1aaf62);},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x185)]=function(_0x3bbc75){const _0x38260c=_0xa2c6e5,_0x4fe5c4=this[_0x38260c(0xf3)](_0x3bbc75),_0xaceb1f=_0x4fe5c4[_0x38260c(0x177)](),_0x174c28=this[_0x38260c(0x1c5)](_0x3bbc75),_0x30e062=Window_PartyReserve[_0x38260c(0x2be)]===0x1;_0x174c28['x']+=ImageManager[_0x38260c(0x1c4)]*(_0x30e062?0x2:0x1);const _0x22435d=VisuMZ[_0x38260c(0x2ae)][_0x38260c(0xe1)][_0x38260c(0x13d)],_0xf6f280=_0x174c28['x']+_0x22435d[_0x38260c(0x142)]+this[_0x38260c(0x22d)](),_0x17bec6=_0x174c28['y']+_0x174c28[_0x38260c(0x220)]-_0x22435d[_0x38260c(0x1fe)];this[_0x38260c(0x26e)](_0xaceb1f,_0xf6f280,_0x17bec6);},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x18e)]=function(_0x5c03a7){const _0x4ae6cc=_0xa2c6e5;this[_0x4ae6cc(0x2c1)]=_0x5c03a7,this[_0x4ae6cc(0x258)]();},Window_PartyReserve[_0xa2c6e5(0x24d)][_0xa2c6e5(0x258)]=function(){const _0x567a44=_0xa2c6e5;this['_statusWindow']&&this[_0x567a44(0x2c1)]['setActor'](this[_0x567a44(0xf3)](this[_0x567a44(0x233)]()));};function Window_PartyStatus(){const _0x25deff=_0xa2c6e5;this[_0x25deff(0xde)](...arguments);}Window_PartyStatus[_0xa2c6e5(0x24d)]=Object[_0xa2c6e5(0x183)](Window_StatusBase[_0xa2c6e5(0x24d)]),Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1b4)]=Window_PartyStatus,Window_PartyStatus[_0xa2c6e5(0x24d)]['initialize']=function(_0x36b88d){const _0x3698a9=_0xa2c6e5;this[_0x3698a9(0x214)]=null,Window_StatusBase['prototype'][_0x3698a9(0xde)][_0x3698a9(0x15a)](this,_0x36b88d);},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x28c)]=function(_0x11ada8,_0x5014c0,_0x37d596,_0x51c66e,_0x13e4fa){const _0x4925ab=_0xa2c6e5;if(VisuMZ[_0x4925ab(0x2ae)][_0x4925ab(0xe1)][_0x4925ab(0x1ba)][_0x4925ab(0x107)]===![])return;_0x13e4fa=Math[_0x4925ab(0x2b1)](_0x13e4fa||0x1,0x1);while(_0x13e4fa--){_0x51c66e=_0x51c66e||this[_0x4925ab(0x226)](),this[_0x4925ab(0x2a2)][_0x4925ab(0x135)]=0xa0;const _0x218a3f=ColorManager[_0x4925ab(0x19a)]();this[_0x4925ab(0x2a2)]['fillRect'](_0x11ada8+0x1,_0x5014c0+0x1,_0x37d596-0x2,_0x51c66e-0x2,_0x218a3f),this[_0x4925ab(0x2a2)][_0x4925ab(0x135)]=0xff;}},ColorManager[_0xa2c6e5(0x19a)]=function(){const _0x5831b4=_0xa2c6e5,_0x3ee252=VisuMZ[_0x5831b4(0x2ae)]['Settings'][_0x5831b4(0x1ba)];let _0x4fd027=_0x3ee252[_0x5831b4(0x2b6)]!==undefined?_0x3ee252[_0x5831b4(0x2b6)]:0x13;return ColorManager[_0x5831b4(0x11c)](_0x4fd027);},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2bf)]=function(_0x386eac){const _0x4212f6=_0xa2c6e5;if(this[_0x4212f6(0x214)]===_0x386eac)return;this['_actor']=_0x386eac;if(_0x386eac){const _0x1977c1=ImageManager[_0x4212f6(0x1fa)](_0x386eac[_0x4212f6(0x10d)]());_0x1977c1[_0x4212f6(0x27d)](this[_0x4212f6(0x239)][_0x4212f6(0x11a)](this));}else this[_0x4212f6(0x239)]();},Window_PartyStatus[_0xa2c6e5(0x24d)]['refresh']=function(){const _0x2aa3bb=_0xa2c6e5;Window_StatusBase[_0x2aa3bb(0x24d)][_0x2aa3bb(0x239)][_0x2aa3bb(0x15a)](this),this['contents'][_0x2aa3bb(0x178)](),this[_0x2aa3bb(0x1cb)](),VisuMZ['PartySystem']['Settings'][_0x2aa3bb(0x13d)][_0x2aa3bb(0x171)][_0x2aa3bb(0x15a)](this);},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x17e)]=function(){const _0x2f15c1=_0xa2c6e5;if(!this[_0x2f15c1(0x214)]){this['drawItemDarkRect'](0x0,0x0,this[_0x2f15c1(0x295)],this[_0x2f15c1(0x218)]);const _0x3b7575=Math[_0x2f15c1(0x116)]((this[_0x2f15c1(0x218)]-this['lineHeight']())/0x2);this['changeTextColor'](ColorManager[_0x2f15c1(0x12c)]()),this[_0x2f15c1(0x170)](TextManager[_0x2f15c1(0x255)],0x0,_0x3b7575,this[_0x2f15c1(0x295)],_0x2f15c1(0x106));return;}this[_0x2f15c1(0x12f)](this[_0x2f15c1(0x214)],0x1,0x0,ImageManager[_0x2f15c1(0x2af)],ImageManager['faceHeight']),this[_0x2f15c1(0x102)](this[_0x2f15c1(0x214)],ImageManager[_0x2f15c1(0x2af)]+0x24,0x0);const _0x3b8d3a=this[_0x2f15c1(0x226)](),_0x33b66b=this[_0x2f15c1(0x1db)](),_0xf8d025=Math[_0x2f15c1(0x116)](this['innerWidth']/0x2),_0x315450=Math[_0x2f15c1(0x209)](_0x33b66b[_0x2f15c1(0x167)]/0x2)*_0x3b8d3a,_0x380627=0x0;let _0x4f41cc=0x0,_0x302843=ImageManager[_0x2f15c1(0xf9)]+_0x3b8d3a/0x2;for(const _0x145eb7 of _0x33b66b){this[_0x2f15c1(0x28c)](_0x4f41cc,_0x302843,_0xf8d025,_0x3b8d3a),this['drawParamName'](_0x145eb7,_0x4f41cc,_0x302843,_0xf8d025),this['drawParamValue'](_0x145eb7,_0x4f41cc,_0x302843,_0xf8d025),_0x4f41cc===_0x380627?_0x4f41cc+=_0xf8d025:(_0x4f41cc=_0x380627,_0x302843+=_0x3b8d3a);}},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1db)]=function(){const _0xe37efc=_0xa2c6e5;return Imported[_0xe37efc(0x198)]?VisuMZ[_0xe37efc(0x20d)]['Settings'][_0xe37efc(0x123)][_0xe37efc(0x14b)]:[0x2,0x3,0x4,0x5,0x6,0x7];},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x261)]=function(_0x30396a,_0xabd2e3,_0x13e359,_0x431b1e){const _0x397224=_0xa2c6e5,_0x5875ef=this[_0x397224(0x22d)]();_0x431b1e-=_0x5875ef*0x2;if(Imported[_0x397224(0x198)])this[_0x397224(0x279)](_0xabd2e3+_0x5875ef,_0x13e359,_0x431b1e,_0x30396a,![]);else{const _0x507c06=TextManager['param'](_0x30396a);this['changeTextColor'](ColorManager[_0x397224(0x12c)]()),this[_0x397224(0x170)](_0x507c06,_0xabd2e3+_0x5875ef,_0x13e359,_0x431b1e);}},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0xdc)]=function(_0xefadc4,_0x49d0a5,_0x45dd2a,_0x6674a2){const _0x5bf0b7=_0xa2c6e5;this[_0x5bf0b7(0x1cb)]();const _0x22ec87=this[_0x5bf0b7(0x22d)](),_0x3b1a00=this[_0x5bf0b7(0x131)](_0xefadc4);this[_0x5bf0b7(0x170)](_0x3b1a00,_0x49d0a5+_0x22ec87,_0x45dd2a,_0x6674a2-_0x22ec87*0x2,'right');},Window_PartyStatus[_0xa2c6e5(0x24d)][_0xa2c6e5(0x131)]=function(_0x374a6b){const _0x811e42=this['_actor'];return Imported['VisuMZ_0_CoreEngine']?_0x811e42['paramValueByName'](_0x374a6b,!![]):_0x811e42['param'](_0x374a6b);};function Window_PartyBattleSwitch(){this['initialize'](...arguments);}Window_PartyBattleSwitch[_0xa2c6e5(0x24d)]=Object[_0xa2c6e5(0x183)](Window_StatusBase['prototype']),Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1b4)]=Window_PartyBattleSwitch,Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0xde)]=function(_0x1c3719){const _0x293a76=_0xa2c6e5;Window_StatusBase[_0x293a76(0x24d)][_0x293a76(0xde)]['call'](this,_0x1c3719),this[_0x293a76(0x240)](VisuMZ[_0x293a76(0x2ae)]['Settings'][_0x293a76(0x13d)][_0x293a76(0x1ad)]),this['openness']=0x0;},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1ec)]=function(){const _0x1fa7d6=_0xa2c6e5;for(const _0x1ae8cf of $gameParty[_0x1fa7d6(0x1ff)]()){ImageManager[_0x1fa7d6(0x1fa)](_0x1ae8cf[_0x1fa7d6(0x10d)]());}},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0xff)]=function(){return 0x1;},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0xf3)]=function(_0x2130a1){const _0x48b637=_0xa2c6e5;return $gameParty[_0x48b637(0x26a)]()[_0x2130a1];},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x201)]=function(){const _0x2d64c3=_0xa2c6e5;return this['actor'](this[_0x2d64c3(0x233)]());},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1d3)]=function(){return this['lineHeight']()*0x2+0x8;},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x108)]=function(){const _0x5e46dd=_0xa2c6e5;return $gameParty[_0x5e46dd(0x26a)]()[_0x5e46dd(0x167)];},Window_PartyBattleSwitch['prototype'][_0xa2c6e5(0x22a)]=function(){const _0x1aa90b=_0xa2c6e5;Window_StatusBase[_0x1aa90b(0x24d)]['activate'][_0x1aa90b(0x15a)](this),this[_0x1aa90b(0x112)](),this[_0x1aa90b(0x239)](),this[_0x1aa90b(0x202)](0x0);},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2bd)]=function(){const _0xdf56ab=_0xa2c6e5;Window_StatusBase['prototype'][_0xdf56ab(0x2bd)][_0xdf56ab(0x15a)](this),this[_0xdf56ab(0x16f)]();},Window_PartyBattleSwitch['prototype'][_0xa2c6e5(0x156)]=function(){const _0xb50c09=_0xa2c6e5;return this[_0xb50c09(0xe3)](this[_0xb50c09(0x201)]());},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0xe3)]=function(_0x5a621e){const _0x96ec58=_0xa2c6e5;if(!_0x5a621e)return![];return _0x5a621e[_0x96ec58(0xef)]()&&_0x5a621e[_0x96ec58(0x1e8)]();},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x215)]=function(_0x157451){const _0x5f4105=_0xa2c6e5,_0x374a1f=this['actor'](_0x157451);if(!_0x374a1f)return;const _0x3b786f=ImageManager[_0x5f4105(0x1fa)](_0x374a1f[_0x5f4105(0x10d)]());_0x3b786f[_0x5f4105(0x27d)](this[_0x5f4105(0x17c)][_0x5f4105(0x11a)](this,_0x157451));},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x17c)]=function(_0x4a734f){const _0x393915=_0xa2c6e5;this[_0x393915(0x1e5)](_0x4a734f),this['drawItemStatus'](_0x4a734f);},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1e5)]=function(_0x27d945){const _0xa2a3e4=_0xa2c6e5,_0xfc1bec=this[_0xa2a3e4(0xf3)](_0x27d945),_0x36d30c=this[_0xa2a3e4(0x1c5)](_0x27d945);this[_0xa2a3e4(0xe0)](this[_0xa2a3e4(0xe3)](_0xfc1bec)),this[_0xa2a3e4(0x12f)](_0xfc1bec,_0x36d30c['x']+0x1,_0x36d30c['y']+0x1,ImageManager['faceWidth'],_0x36d30c[_0xa2a3e4(0x220)]-0x2),this[_0xa2a3e4(0xe0)](!![]);},Window_PartyBattleSwitch[_0xa2c6e5(0x24d)]['drawItemStatus']=function(_0x497628){const _0x4c3fbe=_0xa2c6e5,_0x42cad3=this['actor'](_0x497628),_0x127425=this[_0x4c3fbe(0x161)](_0x497628),_0xf40c3b=_0x127425['x']+ImageManager[_0x4c3fbe(0x2af)]+0x24,_0x290df9=_0xf40c3b+0xb4;this[_0x4c3fbe(0xe0)](this['isEnabled'](_0x42cad3)),this['drawActorName'](_0x42cad3,_0xf40c3b,_0x127425['y']),this[_0x4c3fbe(0x1f9)](_0x42cad3,_0xf40c3b,_0x127425['y']+this[_0x4c3fbe(0x226)]()),this[_0x4c3fbe(0x21f)](_0x42cad3,_0x290df9,_0x127425['y']),this['changePaintOpacity'](!![]);};Imported[_0xa2c6e5(0x1c8)]&&(ImageManager['battlePartyChangeIcon']=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)]['General'][_0xa2c6e5(0x1c6)]??0x4b,TextManager[_0xa2c6e5(0x248)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0x180)],TextManager[_0xa2c6e5(0x2a6)]=VisuMZ[_0xa2c6e5(0x2ae)]['Settings'][_0xa2c6e5(0x10c)][_0xa2c6e5(0x1b5)],TextManager[_0xa2c6e5(0x26d)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)]['Vocab'][_0xa2c6e5(0x133)],TextManager[_0xa2c6e5(0xe5)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)]['Vocab'][_0xa2c6e5(0x16c)],TextManager[_0xa2c6e5(0x1f2)]=VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x10c)][_0xa2c6e5(0xe4)],VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x1eb)]=SceneManager['isPreviousSceneBattleTransitionable'],SceneManager[_0xa2c6e5(0x227)]=function(){const _0x3ecce2=_0xa2c6e5;if(SceneManager[_0x3ecce2(0x27f)](Scene_Party))return!![];return VisuMZ[_0x3ecce2(0x2ae)]['SceneManager_isPreviousSceneBattleTransitionable'][_0x3ecce2(0x15a)](this);},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x132)]=SceneManager[_0xa2c6e5(0x213)],SceneManager[_0xa2c6e5(0x213)]=function(){const _0x1477c3=_0xa2c6e5;if(SceneManager[_0x1477c3(0x14e)](Scene_Party))return!![];return VisuMZ[_0x1477c3(0x2ae)][_0x1477c3(0x132)][_0x1477c3(0x15a)](this);},SceneManager[_0xa2c6e5(0x11e)]=function(){const _0x529d25=_0xa2c6e5;return this['_scene']&&this['_scene'][_0x529d25(0x1b4)]===Scene_Map;},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x194)]=Scene_Battle['prototype'][_0xa2c6e5(0x21d)],Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x21d)]=function(){const _0xfdc889=_0xa2c6e5;VisuMZ[_0xfdc889(0x2ae)]['Scene_Battle_createAllWindows'][_0xfdc889(0x15a)](this),this[_0xfdc889(0x1d7)](),this['postPartySwitchMenuTpb'](),this[_0xfdc889(0x257)]();},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1d7)]=function(){const _0x33c360=_0xa2c6e5,_0x47b447=this[_0x33c360(0x2b4)]();this['_partyMemberSwitchWindow']=new Window_PartyBattleSwitch(_0x47b447),this[_0x33c360(0x229)](this[_0x33c360(0x27b)]),this[_0x33c360(0x27b)][_0x33c360(0x228)]('ok',this[_0x33c360(0x1aa)][_0x33c360(0x11a)](this)),this[_0x33c360(0x27b)]['setHandler']('cancel',this[_0x33c360(0x29a)]['bind'](this));},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2b4)]=function(){const _0x5061ea=_0xa2c6e5,_0x1d5dac=this[_0x5061ea(0x1bb)]();return _0x1d5dac===_0x5061ea(0x23b)?this[_0x5061ea(0x28a)]():this[_0x5061ea(0x207)]();},Scene_Battle['prototype'][_0xa2c6e5(0x207)]=function(){const _0x1afdeb=_0xa2c6e5;return VisuMZ['PartySystem'][_0x1afdeb(0xe1)]['Window'][_0x1afdeb(0x189)][_0x1afdeb(0x15a)](this);},Scene_Battle['prototype']['partySwitchWindowRectBorder']=function(){const _0x2d7649=_0xa2c6e5,_0x2f334e=this[_0x2d7649(0x1a4)](),_0x533b81=$gameSystem[_0x2d7649(0x287)]()*0x2;return _0x2f334e[_0x2d7649(0x29d)]=0x204+_0x533b81,_0x2f334e;},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x21b)]=Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1d2)],Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1d2)]=function(){const _0x589dfc=_0xa2c6e5;if(this[_0x589dfc(0x27b)]&&this[_0x589dfc(0x27b)]['active'])return!![];if(this[_0x589dfc(0x2a8)])return!![];if(this[_0x589dfc(0x253)])return!![];if(this['_callSceneParty'])return!![];return VisuMZ[_0x589dfc(0x2ae)][_0x589dfc(0x21b)][_0x589dfc(0x15a)](this);},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x1b7)]=Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x23e)],Scene_Battle[_0xa2c6e5(0x24d)]['createPartyCommandWindowBattleCore']=function(){const _0x74d87c=_0xa2c6e5;VisuMZ[_0x74d87c(0x2ae)]['Scene_Battle_createPartyCommandWindowBattleCore'][_0x74d87c(0x15a)](this),this[_0x74d87c(0x1b9)][_0x74d87c(0x228)](_0x74d87c(0x21a),this[_0x74d87c(0x24a)]['bind'](this));},Scene_Battle['prototype'][_0xa2c6e5(0x24a)]=function(){const _0xb8833a=_0xa2c6e5;this[_0xb8833a(0x117)]()?(this[_0xb8833a(0x206)]=!![],this[_0xb8833a(0x157)][_0xb8833a(0x269)](TextManager[_0xb8833a(0x1f2)]['format'](TextManager['formation']))):this[_0xb8833a(0x2b0)]();},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x117)]=function(){const _0x2cb1ac=_0xa2c6e5;return BattleManager[_0x2cb1ac(0x125)]();},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2b0)]=function(){const _0x573a69=_0xa2c6e5;this[_0x573a69(0x206)]=![],this[_0x573a69(0x1ef)][_0x573a69(0x2b8)](),this[_0x573a69(0x1c0)][_0x573a69(0xf1)]=![],SceneManager[_0x573a69(0x22b)](),SceneManager[_0x573a69(0x29f)](Scene_Party),$gameParty[_0x573a69(0x1bf)](),BattleManager[_0x573a69(0x249)]()&&(BattleManager[_0x573a69(0x289)]=BattleManager['actor']());},VisuMZ['PartySystem']['Scene_Battle_updateBattleProcess']=Scene_Battle['prototype'][_0xa2c6e5(0xfb)],Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0xfb)]=function(){const _0x5852a2=_0xa2c6e5;VisuMZ[_0x5852a2(0x2ae)]['Scene_Battle_updateBattleProcess'][_0x5852a2(0x15a)](this),this['_callSceneParty']&&!BattleManager[_0x5852a2(0x267)]&&this[_0x5852a2(0x2b0)](),this[_0x5852a2(0x253)]&&!BattleManager[_0x5852a2(0x267)]&&this[_0x5852a2(0x28b)]();},VisuMZ[_0xa2c6e5(0x2ae)]['Scene_Battle_isTimeActive']=Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x12a)],Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x12a)]=function(){const _0x4bf22b=_0xa2c6e5;if(BattleManager['isActiveTpb']()){if(this['_partyMemberSwitchWindow']&&this[_0x4bf22b(0x27b)][_0x4bf22b(0x1a3)])return![];}return VisuMZ['PartySystem']['Scene_Battle_isTimeActive'][_0x4bf22b(0x15a)](this);},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0x119)]=Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1a0)],Scene_Battle[_0xa2c6e5(0x24d)]['createActorCommandWindow']=function(){const _0x44fd96=_0xa2c6e5;VisuMZ[_0x44fd96(0x2ae)][_0x44fd96(0x119)][_0x44fd96(0x15a)](this),this[_0x44fd96(0x1b6)][_0x44fd96(0x228)](_0x44fd96(0x21a),this['commandPartyMemberSwitch']['bind'](this));},Scene_Battle[_0xa2c6e5(0x24d)]['commandPartyMemberSwitch']=function(){const _0x35a970=_0xa2c6e5;this['isQueueFormationMenu']()?(this[_0x35a970(0x253)]=!![],this['_logWindow']['addText'](TextManager[_0x35a970(0x1f2)]['format'](TextManager['formation']))):this[_0x35a970(0x28b)]();},Scene_Battle[_0xa2c6e5(0x24d)]['callPartyMemberSwitch']=function(){const _0x2afd4c=_0xa2c6e5;this['_callPartyMemberSwitch']=![],this['_logWindow'][_0x2afd4c(0x178)](),BattleManager[_0x2afd4c(0xf3)]()&&this[_0x2afd4c(0x27b)][_0x2afd4c(0x22a)]();},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x1aa)]=function(){const _0xd94754=_0xa2c6e5,_0x4c4556=this[_0xd94754(0x27b)][_0xd94754(0x201)]();_0x4c4556?this[_0xd94754(0x114)](_0x4c4556):(this['_partyMemberSwitchWindow']['deactivate'](),this['_actorCommandWindow'][_0xd94754(0x22a)]());},Scene_Battle[_0xa2c6e5(0x24d)]['preparePartySwitchMember']=function(_0x402f1b){const _0x55f60c=_0xa2c6e5,_0x17b29e=BattleManager[_0x55f60c(0xf3)](),_0x13eaf8=_0x17b29e['battler']();this['_partyMemberSwitchWindow']['deactivate'](),this[_0x55f60c(0x270)]()&&_0x13eaf8?(this['_partySystemSwitchOut']=!![],_0x13eaf8[_0x55f60c(0x19c)](_0x402f1b)):this[_0x55f60c(0x159)](_0x402f1b);},Scene_Battle['prototype'][_0xa2c6e5(0x270)]=function(){const _0x5e57f0=_0xa2c6e5;return VisuMZ[_0x5e57f0(0x2ae)][_0x5e57f0(0xe1)][_0x5e57f0(0x1ba)][_0x5e57f0(0x1f4)];},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x159)]=function(_0x28cee1){const _0x38bc82=_0xa2c6e5;this[_0x38bc82(0x2a8)]=![];const _0x531387=BattleManager[_0x38bc82(0xf3)](),_0x469a85=_0x531387[_0x38bc82(0x222)]();$gameParty[_0x38bc82(0x1d6)][_0x531387[_0x38bc82(0x233)]()]=_0x28cee1[_0x38bc82(0x16d)](),$gameParty[_0x38bc82(0x25f)]();if(this[_0x38bc82(0x276)]())_0x28cee1[_0x38bc82(0x190)]=_0x531387[_0x38bc82(0x190)],_0x28cee1[_0x38bc82(0x22c)]=_0x38bc82(0x129);else BattleManager['isTpb']()&&_0x28cee1['clearTpbChargeTime']();BattleManager[_0x38bc82(0x259)]=_0x28cee1,_0x28cee1[_0x38bc82(0x1bf)](),_0x28cee1[_0x38bc82(0x282)](),_0x28cee1[_0x38bc82(0xd7)](_0x531387),_0x469a85&&_0x469a85[_0x38bc82(0x1a9)](_0x28cee1),this[_0x38bc82(0x2c1)][_0x38bc82(0x173)](_0x531387,_0x28cee1),this['_statusWindow'][_0x38bc82(0x239)](),this['_actorCommandWindow'][_0x38bc82(0x2a3)](_0x28cee1),this[_0x38bc82(0x1b6)][_0x38bc82(0x202)](0x0),this[_0x38bc82(0x1b6)][_0x38bc82(0x22a)](),this['_actorCommandWindow'][_0x38bc82(0x23a)]=!![];},Scene_Battle['prototype'][_0xa2c6e5(0x276)]=function(){const _0x4a43bd=_0xa2c6e5;if(!BattleManager['isTpb']())return![];const _0x54688f=VisuMZ[_0x4a43bd(0x2ae)]['Settings'][_0x4a43bd(0x1ba)];return _0x54688f['tpbImmediateAction']===undefined&&(_0x54688f[_0x4a43bd(0x204)]=!![]),_0x54688f[_0x4a43bd(0x204)];},Window_StatusBase[_0xa2c6e5(0x24d)][_0xa2c6e5(0x173)]=function(_0x443210,_0x470ffb){const _0x5ada3e=_0xa2c6e5,_0x48a4aa='actor%1-stateIcon'['format'](_0x443210[_0x5ada3e(0x16d)]()),_0x2afb14=this[_0x5ada3e(0x230)](_0x48a4aa,Sprite_StateIcon);_0x2afb14[_0x5ada3e(0x2a3)](_0x470ffb);},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x29a)]=function(){const _0xc54f79=_0xa2c6e5;this[_0xc54f79(0x27b)][_0xc54f79(0x2bd)](),this[_0xc54f79(0x1b6)][_0xc54f79(0x22a)](),this[_0xc54f79(0x1b6)]['refresh']();},Scene_Battle[_0xa2c6e5(0x24d)][_0xa2c6e5(0x25e)]=function(){const _0x28cda7=_0xa2c6e5;if(!BattleManager[_0x28cda7(0x249)]())return;if(!SceneManager['isPreviousScene'](Scene_Party))return;this[_0x28cda7(0x1b9)][_0x28cda7(0x2bd)](),this['_partyCommandWindow'][_0x28cda7(0x16f)](),this[_0x28cda7(0x1b6)][_0x28cda7(0x2bd)](),this[_0x28cda7(0x1b6)][_0x28cda7(0x16f)](),BattleManager['_currentActor']=null,BattleManager[_0x28cda7(0x221)]=![];},Scene_Battle['prototype'][_0xa2c6e5(0x257)]=function(){const _0x408143=_0xa2c6e5;if(BattleManager[_0x408143(0x249)]())return;if(!SceneManager[_0x408143(0x27f)](Scene_Party))return;Imported[_0x408143(0x294)]&&BattleManager[_0x408143(0x242)]()&&BattleManager[_0x408143(0x288)](),Imported[_0x408143(0xe9)]&&BattleManager[_0x408143(0x224)]()&&(BattleManager[_0x408143(0x259)]=$gameParty[_0x408143(0xdf)]()[0x0],BattleManager[_0x408143(0x267)]=BattleManager[_0x408143(0xf3)](),BattleManager[_0x408143(0x221)]=!![],this[_0x408143(0x1b6)]['setup'](BattleManager[_0x408143(0xf3)]()),this['_statusWindow']['selectActor'](BattleManager[_0x408143(0xf3)]()));},Sprite_Actor[_0xa2c6e5(0x250)]=0xc,Sprite_Actor['prototype'][_0xa2c6e5(0x19c)]=function(_0x56cc42){const _0x24d808=_0xa2c6e5;this[_0x24d808(0x141)]=_0x56cc42;const _0x2883fe=Sprite_Actor[_0x24d808(0x250)];this['startMove'](0x12c,0x0,_0x2883fe),this[_0x24d808(0x21e)](0x0,_0x2883fe),this[_0x24d808(0x250)]=_0x2883fe;},Sprite_Actor['prototype'][_0xa2c6e5(0x296)]=function(_0x32d391){const _0x57d81e=_0xa2c6e5;if(SceneManager[_0x57d81e(0x20b)]()){SceneManager['_scene'][_0x57d81e(0x159)](_0x32d391);const _0x4bbfdc=Sprite_Actor[_0x57d81e(0x250)];this[_0x57d81e(0x11f)](),this[_0x57d81e(0x21e)](0xff,_0x4bbfdc);}this[_0x57d81e(0x141)]=null;},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xf7)]=Sprite_Actor['prototype'][_0xa2c6e5(0x2b8)],Sprite_Actor[_0xa2c6e5(0x24d)]['update']=function(){const _0x32ed4c=_0xa2c6e5;VisuMZ['PartySystem'][_0x32ed4c(0xf7)][_0x32ed4c(0x15a)](this);if(this[_0x32ed4c(0x250)])this[_0x32ed4c(0x280)]();},Sprite_Actor[_0xa2c6e5(0x24d)][_0xa2c6e5(0x280)]=function(){const _0x577b21=_0xa2c6e5;this['_partySwitchDuration']=this[_0x577b21(0x250)]||0x0,this[_0x577b21(0x250)]--,this[_0x577b21(0x250)]<=0x0&&this[_0x577b21(0x296)](this[_0x577b21(0x141)]);},Window_PartyCommand[_0xa2c6e5(0x24d)][_0xa2c6e5(0x2b2)]=function(){const _0x5df256=_0xa2c6e5;this[_0x5df256(0x192)]();},Window_PartyCommand[_0xa2c6e5(0x24d)]['addFormationCommand']=function(){const _0x249e56=_0xa2c6e5;if(!this[_0x249e56(0x2a4)]())return;if(this[_0x249e56(0x162)]()){$gameTemp[_0x249e56(0x1fc)]()&&!BattleManager[_0x249e56(0x101)]&&(console[_0x249e56(0x19b)](_0x249e56(0x163)),BattleManager['_battleSystemIncompatibilityError']=!![]);return;}const _0x51d02c=this[_0x249e56(0x2bc)](),_0x577329=ImageManager[_0x249e56(0x11b)],_0x9aa38a=_0x51d02c===_0x249e56(0x23c)?TextManager[_0x249e56(0x248)]:'\x5cI[%1]%2'[_0x249e56(0x191)](_0x577329,TextManager[_0x249e56(0x248)]),_0x57ebc8=this[_0x249e56(0x18f)]();this['addCommand'](_0x9aa38a,'formation',_0x57ebc8);},Window_PartyCommand['prototype'][_0xa2c6e5(0x2a4)]=function(){const _0x470fcc=_0xa2c6e5;if(Imported[_0x470fcc(0x23d)]&&BattleManager[_0x470fcc(0x18b)]())return![];return VisuMZ[_0x470fcc(0x2ae)][_0x470fcc(0xe1)][_0x470fcc(0x1ba)][_0x470fcc(0x128)];},Window_PartyCommand[_0xa2c6e5(0x24d)][_0xa2c6e5(0x162)]=function(){const _0x5e9b72=_0xa2c6e5;if(Imported[_0x5e9b72(0x1dd)]&&BattleManager[_0x5e9b72(0x1f7)]())return!![];return![];},Window_PartyCommand['prototype']['isFormationCommandEnabled']=function(){const _0x1e5bd6=_0xa2c6e5;if($gameParty[_0x1e5bd6(0x1ff)]()[_0x1e5bd6(0x167)]<=0x1)return![];if(!$gameParty[_0x1e5bd6(0x1d8)]())return![];return $gameSystem['isFormationEnabled']();},VisuMZ[_0xa2c6e5(0x2ae)]['Settings']['Window_PartyCommand_updateHelp']=Window_PartyCommand[_0xa2c6e5(0x24d)][_0xa2c6e5(0x24b)],Window_PartyCommand['prototype'][_0xa2c6e5(0x24b)]=function(){const _0x1233cb=_0xa2c6e5,_0xe792ad=this[_0x1233cb(0x187)]();switch(_0xe792ad){case'formation':this[_0x1233cb(0x196)]['setText'](TextManager[_0x1233cb(0x2a6)]);break;default:VisuMZ[_0x1233cb(0x2ae)][_0x1233cb(0xe1)][_0x1233cb(0x16e)][_0x1233cb(0x15a)](this);break;}},Window_ActorCommand[_0xa2c6e5(0x24d)][_0xa2c6e5(0x264)]=function(){const _0x295739=_0xa2c6e5;if(!this[_0x295739(0x27c)]())return;const _0x353a19=this[_0x295739(0x2bc)](),_0x49fd9e=ImageManager[_0x295739(0x11b)],_0x222f0f=_0x353a19===_0x295739(0x23c)?TextManager['battlePartySwitchCmd']:_0x295739(0xf2)[_0x295739(0x191)](_0x49fd9e,TextManager[_0x295739(0x248)]),_0x4a06df=this['isPartyCommandEnabled']();this['addCommand'](_0x222f0f,_0x295739(0x21a),_0x4a06df);},Window_ActorCommand[_0xa2c6e5(0x24d)]['isPartyCommandAdded']=function(){const _0x3273ee=_0xa2c6e5;if(!this[_0x3273ee(0x214)])return![];return VisuMZ[_0x3273ee(0x2ae)][_0x3273ee(0xe1)][_0x3273ee(0x1ba)][_0x3273ee(0x1a5)];},Window_ActorCommand['prototype'][_0xa2c6e5(0xf8)]=function(){const _0x16054f=_0xa2c6e5;if($gameParty[_0x16054f(0x1ff)]()[_0x16054f(0x167)]<=0x1)return![];if(!this['_actor'])return![];if(!this[_0x16054f(0x214)]['canSwitchPartyInBattle']())return![];return this[_0x16054f(0x214)][_0x16054f(0xef)]();},VisuMZ[_0xa2c6e5(0x2ae)][_0xa2c6e5(0xe1)][_0xa2c6e5(0x2b3)]=Window_ActorCommand[_0xa2c6e5(0x24d)]['updateHelp'],Window_ActorCommand[_0xa2c6e5(0x24d)][_0xa2c6e5(0x24b)]=function(){const _0xe6fed0=_0xa2c6e5,_0x50a7a9=this[_0xe6fed0(0x187)]();switch(_0x50a7a9){case _0xe6fed0(0x21a):this['_helpWindow'][_0xe6fed0(0x19f)](TextManager[_0xe6fed0(0xe5)]);break;default:VisuMZ[_0xe6fed0(0x2ae)][_0xe6fed0(0xe1)][_0xe6fed0(0x2b3)][_0xe6fed0(0x15a)](this);break;}});;