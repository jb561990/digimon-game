//=============================================================================
// VisuStella MZ - Visual Battle Environment
// VisuMZ_2_VisualBattleEnv.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_VisualBattleEnv = true;

var VisuMZ = VisuMZ || {};
VisuMZ.VisualBattleEnv = VisuMZ.VisualBattleEnv || {};
VisuMZ.VisualBattleEnv.version = 1.01;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.01] [VisualBattleEnv]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Visual_Battle_Environment_VisuStella_MZ
 * @base VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_BattleCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * Add extra layers of images to your battle system for background purposes or
 * foreground purposes. These images can be battlebacks, pictures, parallaxes,
 * whatever you need them to be. Add extra settings to them, such as scrolling,
 * blend modes, different opacity levels, hues, and more.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Create battle environment images located behind battlers to function as a
 *   part of the background.
 * * Create battle environment images located in front of battlers to function
 *   as a part of the foreground.
 * * Apply custom settings to them, such as changing their blend modes, their
 *   scrolling speeds, and opacity levels.
 * * Customize their hue and if they have a hue shift at all.
 * * Apply color tones if needed to give more color control.
 * * Alter their opacity levels midway during battle.
 * * An unlimited amounts of back environments and front environments to add to
 *   the battle scene.
 * * Environment images are layered based on their ID's. Lower ID's appear
 *   below while higher ID's appear above.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_1_BattleCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Back Environment-Type Plugin Commands ===
 * 
 * ---
 *
 * Back Environment: Add/Change
 * - Adds/changes the target back environment.
 * 
 *   Settings:
 *
 *     ID:
 *     - Select the target environment ID to add/change.
 *     - Lower ID's appear below. Higher ID's appear above.
 *
 *     Folder and Filename:
 *     - What is the folder and filename?
 * 
 *   Extra Settings:
 *   - Extra settings that can be altered for the environment object.
 *   - For details, refer to section below.
 *
 *     Duration:
 *     - How many frames would it take to alter settings?
 *
 * ---
 *
 * Back Environment: Fade Opacity
 * - Fades the target back environment(s) opacity to a different value.
 *
 *   ID(s):
 *   - Target which back environment(s)?
 *   - Cannot target the default battlebacks.
 *
 *   Target Opacity:
 *   - What opacity level to this value (0-255).
 *   - You may use JavaScript code to determine the value.
 *
 *   Duration:
 *   - How many frames should this change take?
 *   - You may use JavaScript code to determine the value.
 *
 * ---
 *
 * Back Environment: Remove
 * - Removes target back environment(s).
 *
 *   ID(s):
 *   - Remove which back environment(s)?
 *   - Cannot remove the default battlebacks.
 *
 * ---
 * 
 * === Front Environment-Type Plugin Commands ===
 * 
 * ---
 *
 * Front Environment: Add/Change
 * - Adds/changes the target front environment.
 * 
 *   Settings:
 *
 *     ID:
 *     - Select the target environment ID to add/change.
 *     - Lower ID's appear below. Higher ID's appear above.
 *
 *     Folder and Filename:
 *     - What is the folder and filename?
 * 
 *   Extra Settings:
 *   - Extra settings that can be altered for the environment object.
 *   - For details, refer to section below.
 *
 *     Duration:
 *     - How many frames would it take to alter settings?
 *
 * ---
 *
 * Front Environment: Fade Opacity
 * - Fades the target front environment(s) opacity to a different value.
 *
 *   ID(s):
 *   - Target which front environment(s)?
 *   - Cannot target the default battlebacks.
 *
 *   Target Opacity:
 *   - What opacity level to this value (0-255).
 *   - You may use JavaScript code to determine the value.
 *
 *   Duration:
 *   - How many frames should this change take?
 *   - You may use JavaScript code to determine the value.
 *
 * ---
 *
 * Front Environment: Remove
 * - Removes target front environment(s).
 *
 *   ID(s):
 *   - Remove which front environment(s)?
 *   - Cannot remove the default battlebacks.
 *
 * ---
 * 
 * === Extra-Settings ===
 * 
 * ---
 *
 * Extra Settings
 * - These settings are used for both the "Back Environment: Add/Change" and
 *   "Front Environment: Add/Change" Plugin Commands.
 * 
 *   Appearance:
 *
 *     Scale Style:
 *     - The scaling style used for this environment image.
 *       - Battle Core Setting
 *       - MZ (MZ's default style)
 *       - 1:1 (No Scaling)
 *       - Scale To Fit (Scale to screen size)
 *       - Scale Down (Scale Downward if Larger than Screen)
 *       - Scale Up (Scale Upward if Smaller than Screen)
 *
 *     Opacity:
 *     - What is the opacity level for this image?
 *     - You may use JavaScript code.
 *
 *     Blend Mode:
 *     - What kind of blend mode do you wish to apply to the image?
 *     - You may use JavaScript code.
 *       - Normal
 *       - Additive
 *       - Multiply
 *       - Screen
 * 
 *     Hue: 
 *     - Do you wish to adjust this image's hue?
 *     - You may use JavaScript code.
 * 
 *     Hue Shift:
 *     - How much do you want the hue to shift each frame?
 *     - You may use JavaScript code.
 * 
 *     Color Tone:
 *     - What tone do you want for the motion trail?
 *     - Format: [Red, Green, Blue, Gray]
 * 
 *   Scrolling:
 *
 *     Horizontal Scroll:
 *     - What is the horizontal scroll speed?
 *     - Use a negative value to invert the direction.
 *
 *     Vertical Scroll:
 *     - What is the vertical scroll speed?
 *     - Use a negative value to invert the direction.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.01: April 9, 2021
 * * Bug Fixes!
 * ** Crashes should no longer occur when performing a troop transition from
 *    the map. Fix made by Olivia.
 *
 * Version 1.00 Official Release Date: May 10, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BackEnvironmentAddChange
 * @text Back Environment: Add/Change
 * @desc Adds/changes the target back environment.
 *
 * @arg Settings
 *
 * @arg ID:num
 * @text ID
 * @parent Settings
 * @type number
 * @min 1
 * @desc Select the target environment ID to add/change.
 * Lower ID's appear below. Higher ID's appear above.
 * @default 1
 *
 * @arg FolderFilename:str
 * @text Folder and Filename
 * @parent Settings
 * @type file
 * @dir img/
 * @desc What is the folder and filename?
 * @default 
 * 
 * @arg Extra:struct
 * @text Extra Settings
 * @type struct<Optional>
 * @desc Extra settings that can be altered for the environment object.
 * @default {"Appearance":"","ScaleStyle:str":"BattleCore","blendMode:eval":"0","opacity:eval":"255","Scrolling":"","ScrollHorz:eval":"+0","ScrollVert:eval":"+0"}
 *
 * @arg duration:num
 * @text Duration
 * @parent Extra:struct
 * @type number
 * @min 1
 * @desc How many frames would it take to alter settings?
 * @default 20
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BackEnvironmentFade
 * @text Back Environment: Fade Opacity
 * @desc Fades the target back environment(s) opacity to a different value.
 *
 * @arg list:arraynum
 * @text ID(s)
 * @type number[]
 * @min 1
 * @desc Target which back environment(s)?
 * Cannot target the default battlebacks.
 * @default ["1"]
 *
 * @arg opacity:eval
 * @text Target Opacity
 * @desc What opacity level to this value (0-255).
 * You may use JavaScript code to determine the value.
 * @default 255
 *
 * @arg duration:eval
 * @text Duration
 * @desc How many frames should this change take?
 * You may use JavaScript code to determine the value.
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BackEnvironmentRemove
 * @text Back Environment: Remove
 * @desc Removes target back environment(s).
 *
 * @arg list:arraynum
 * @text ID(s)
 * @type number[]
 * @min 1
 * @desc Remove which back environment(s)?
 * Cannot remove the default battlebacks.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FrontEnvironmentAddChange
 * @text Front Environment: Add/Change
 * @desc Adds/changes the target front environment.
 *
 * @arg Settings
 *
 * @arg ID:num
 * @text ID
 * @parent Settings
 * @type number
 * @min 1
 * @desc Select the target environment ID to add/change.
 * Lower ID's appear below. Higher ID's appear above.
 * @default 1
 *
 * @arg FolderFilename:str
 * @text Folder and Filename
 * @parent Settings
 * @type file
 * @dir img/
 * @desc What is the folder and filename?
 * @default 
 * 
 * @arg Extra:struct
 * @text Extra Settings
 * @type struct<Optional>
 * @desc Extra settings that can be altered for the environment object.
 * @default {"Appearance":"","ScaleStyle:str":"BattleCore","blendMode:eval":"0","opacity:eval":"255","Scrolling":"","ScrollHorz:eval":"+0","ScrollVert:eval":"+0"}
 *
 * @arg duration:num
 * @text Duration
 * @parent Extra:struct
 * @type number
 * @min 1
 * @desc How many frames would it take to alter settings?
 * @default 20
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FrontEnvironmentFade
 * @text Front Environment: Fade Opacity
 * @desc Fades the target front environment(s) opacity to a different value.
 *
 * @arg list:arraynum
 * @text ID(s)
 * @type number[]
 * @min 1
 * @desc Target which front environment(s)?
 * Cannot target the default battlebacks.
 * @default ["1"]
 *
 * @arg opacity:eval
 * @text Target Opacity
 * @desc What opacity level to this value (0-255).
 * You may use JavaScript code to determine the value.
 * @default 255
 *
 * @arg duration:eval
 * @text Duration
 * @desc How many frames should this change take?
 * You may use JavaScript code to determine the value.
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FrontEnvironmentRemove
 * @text Front Environment: Remove
 * @desc Removes target front environment(s).
 *
 * @arg list:arraynum
 * @text ID(s)
 * @type number[]
 * @min 1
 * @desc Remove which front environment(s)?
 * Cannot remove the default battlebacks.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param VisualBattleEnv
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Optional Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Optional:
 * 
 * @param Appearance
 *
 * @param ScaleStyle:str
 * @text Scale Style
 * @parent Appearance
 * @type select
 * @option Battle Core Setting
 * @value BattleCore
 * @option MZ (MZ's default style)
 * @value MZ
 * @option 1:1 (No Scaling)
 * @value 1:1
 * @option Scale To Fit (Scale to screen size)
 * @value ScaleToFit
 * @option Scale Down (Scale Downward if Larger than Screen)
 * @value ScaleDown
 * @option Scale Up (Scale Upward if Smaller than Screen)
 * @value ScaleUp
 * @desc The scaling style used for this environment image.
 * @default BattleCore
 *
 * @param opacity:eval
 * @text Opacity
 * @parent Appearance
 * @desc What is the opacity level for this image?
 * You may use JavaScript code.
 * @default 255
 *
 * @param blendMode:eval
 * @text Blend Mode
 * @parent Appearance
 * @type select
 * @option 0 - Normal
 * @value 0
 * @option 1 - Additive
 * @value 1
 * @option 2 - Multiply
 * @value 2
 * @option 3 - Screen
 * @value 3
 * @desc What kind of blend mode do you wish to apply to the image?
 * You may use JavaScript code.
 * @default 0
 *
 * @param hue:eval
 * @text Hue
 * @parent Appearance
 * @desc Do you wish to adjust this image's hue?
 * You may use JavaScript code.
 * @default 0
 *
 * @param hueShift:eval
 * @text Hue Shift
 * @parent hue:eval
 * @desc How much do you want the hue to shift each frame?
 * You may use JavaScript code.
 * @default +0
 *
 * @param colorTone:eval
 * @text Color Tone
 * @parent Appearance
 * @desc What tone do you want for the motion trail?
 * Format: [Red, Green, Blue, Gray]
 * @default [0, 0, 0, 0]
 * 
 * @param Scrolling
 *
 * @param ScrollHorz:eval
 * @text Horizontal Scroll
 * @parent Scrolling
 * @desc What is the horizontal scroll speed?
 * Use a negative value to invert the direction.
 * @default +0
 *
 * @param ScrollVert:eval
 * @text Vertical Scroll
 * @parent Scrolling
 * @desc What is the vertical scroll speed?
 * Use a negative value to invert the direction.
 * @default +0
 *
 */
//=============================================================================

const _0x3d4c=['ConvertParams','update','VisualBattleEnv','AdjustSettings','Spriteset_Battle_createWeather','getBattleEnvironmentSprite','list','FolderFilename','children','exit','removeChild','_scene','EVAL','384042HKIJke','addLoadListener','blendMode','description','setBackEnvironmentSettings','removeBattleEnvironmentSprite','_frontEnvironmentSettings','_updateColorFilter','registerCommand','ARRAYSTRUCT','ARRAYEVAL','createFrontEnvironmentContainer','constructor','setupVisualBattleEnvironment','Battleback','RegExp','getFrontEnvironmentSettings','settings','ScaleDown','createBattleback','adjustPosition','_baseSprite','version','adjustPosition_ScaleUp','Folder','1514788vLXIEL','ScaleStyle','ScrollHorz','origin','BackEnvironmentAddChange','prototype','1308352UBjsaq','setup','Extra','_id','filter','split','_backEnvironmentSettings','initialize','271290kQbgHk','ScaleToFit','colorTone','Spriteset_Battle_update','_folder','updateBlendMode','ScrollVert','updateOpacity','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','trim','name','createWeather','opacity','processBitmap','updateBattleEnvironmentContainers','push','Game_Troop_setup','updateScrolling','map','1575967NkbizF','getBackEnvironmentSettings','call','img/%1/','Settings','BackEnvironmentRemove','_frontEnvironmentContainer','setColorTone','filters','bitmap','FUNC','getBattleEnvironmentContainer','Filename','setFrontEnvironmentSettings','sort','parse','adjustPosition_ScaleDown','DefaultStyle','format','STRUCT','1ZSMriK','updateBitmap','create','isSceneBattle','_front','_colorFilter','_spriteset','639228WXcrPI','1:1','find','addChild','_backEnvironmentContainer','match','includes','max','ARRAYNUM','createBackEnvironmentContainer','FrontEnvironmentRemove','FrontEnvironmentFade','1ShXIPf','updateBattleEnvironmentSprite','STR','Spriteset_Battle_createBattleback','_filename','hueShift','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','ARRAYJSON','_createColorFilter','makeDeepCopy','ARRAYSTR','3455003MvhzTs','ScaleUp','parameters','Sprite_Battleback_adjustPosition','BattleCore','loadBitmap','duration'];const _0x8a2f=function(_0x47ec60,_0x21d896){_0x47ec60=_0x47ec60-0xea;let _0x3d4cfb=_0x3d4c[_0x47ec60];return _0x3d4cfb;};const _0x242feb=_0x8a2f;(function(_0x17476f,_0x391d5f){const _0x119ff8=_0x8a2f;while(!![]){try{const _0x598be8=-parseInt(_0x119ff8(0x15f))+-parseInt(_0x119ff8(0x106))*-parseInt(_0x119ff8(0xf2))+-parseInt(_0x119ff8(0x138))+-parseInt(_0x119ff8(0x10d))+-parseInt(_0x119ff8(0x157))+-parseInt(_0x119ff8(0x119))*parseInt(_0x119ff8(0x151))+parseInt(_0x119ff8(0x124));if(_0x598be8===_0x391d5f)break;else _0x17476f['push'](_0x17476f['shift']());}catch(_0x568add){_0x17476f['push'](_0x17476f['shift']());}}}(_0x3d4c,0xdef76));var label='VisualBattleEnv',tier=tier||0x0,dependencies=['VisuMZ_1_BattleCore'],pluginData=$plugins[_0x242feb(0x15b)](function(_0x12c82c){const _0x49a05e=_0x242feb;return _0x12c82c['status']&&_0x12c82c[_0x49a05e(0x13b)][_0x49a05e(0x113)]('['+label+']');})[0x0];VisuMZ[label][_0x242feb(0xf6)]=VisuMZ[label][_0x242feb(0xf6)]||{},VisuMZ[_0x242feb(0x12b)]=function(_0x470d69,_0x1b4a58){const _0x40e1da=_0x242feb;for(const _0x504e09 in _0x1b4a58){if(_0x504e09[_0x40e1da(0x112)](/(.*):(.*)/i)){const _0x236a76=String(RegExp['$1']),_0x1d0a2b=String(RegExp['$2'])['toUpperCase']()[_0x40e1da(0x168)]();let _0x3570c0,_0x332e24,_0x1773f7;switch(_0x1d0a2b){case'NUM':_0x3570c0=_0x1b4a58[_0x504e09]!==''?Number(_0x1b4a58[_0x504e09]):0x0;break;case _0x40e1da(0x115):_0x332e24=_0x1b4a58[_0x504e09]!==''?JSON[_0x40e1da(0x101)](_0x1b4a58[_0x504e09]):[],_0x3570c0=_0x332e24[_0x40e1da(0xf1)](_0x16db46=>Number(_0x16db46));break;case _0x40e1da(0x137):_0x3570c0=_0x1b4a58[_0x504e09]!==''?eval(_0x1b4a58[_0x504e09]):null;break;case _0x40e1da(0x142):_0x332e24=_0x1b4a58[_0x504e09]!==''?JSON[_0x40e1da(0x101)](_0x1b4a58[_0x504e09]):[],_0x3570c0=_0x332e24[_0x40e1da(0xf1)](_0x33f0de=>eval(_0x33f0de));break;case'JSON':_0x3570c0=_0x1b4a58[_0x504e09]!==''?JSON[_0x40e1da(0x101)](_0x1b4a58[_0x504e09]):'';break;case _0x40e1da(0x120):_0x332e24=_0x1b4a58[_0x504e09]!==''?JSON[_0x40e1da(0x101)](_0x1b4a58[_0x504e09]):[],_0x3570c0=_0x332e24[_0x40e1da(0xf1)](_0x4d6fec=>JSON['parse'](_0x4d6fec));break;case _0x40e1da(0xfc):_0x3570c0=_0x1b4a58[_0x504e09]!==''?new Function(JSON['parse'](_0x1b4a58[_0x504e09])):new Function('return\x200');break;case'ARRAYFUNC':_0x332e24=_0x1b4a58[_0x504e09]!==''?JSON[_0x40e1da(0x101)](_0x1b4a58[_0x504e09]):[],_0x3570c0=_0x332e24[_0x40e1da(0xf1)](_0x2166ab=>new Function(JSON[_0x40e1da(0x101)](_0x2166ab)));break;case _0x40e1da(0x11b):_0x3570c0=_0x1b4a58[_0x504e09]!==''?String(_0x1b4a58[_0x504e09]):'';break;case _0x40e1da(0x123):_0x332e24=_0x1b4a58[_0x504e09]!==''?JSON['parse'](_0x1b4a58[_0x504e09]):[],_0x3570c0=_0x332e24[_0x40e1da(0xf1)](_0x239e50=>String(_0x239e50));break;case _0x40e1da(0x105):_0x1773f7=_0x1b4a58[_0x504e09]!==''?JSON[_0x40e1da(0x101)](_0x1b4a58[_0x504e09]):{},_0x3570c0=VisuMZ[_0x40e1da(0x12b)]({},_0x1773f7);break;case _0x40e1da(0x141):_0x332e24=_0x1b4a58[_0x504e09]!==''?JSON['parse'](_0x1b4a58[_0x504e09]):[],_0x3570c0=_0x332e24['map'](_0x157f80=>VisuMZ[_0x40e1da(0x12b)]({},JSON[_0x40e1da(0x101)](_0x157f80)));break;default:continue;}_0x470d69[_0x236a76]=_0x3570c0;}}return _0x470d69;},(_0x33c084=>{const _0x182d74=_0x242feb,_0xb03c64=_0x33c084[_0x182d74(0x169)];for(const _0x4a385f of dependencies){if(!Imported[_0x4a385f]){alert('%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.'[_0x182d74(0x104)](_0xb03c64,_0x4a385f)),SceneManager[_0x182d74(0x134)]();break;}}const _0x1221b8=_0x33c084[_0x182d74(0x13b)];if(_0x1221b8[_0x182d74(0x112)](/\[Version[ ](.*?)\]/i)){const _0x27f5bb=Number(RegExp['$1']);_0x27f5bb!==VisuMZ[label][_0x182d74(0x14e)]&&(alert(_0x182d74(0x167)[_0x182d74(0x104)](_0xb03c64,_0x27f5bb)),SceneManager[_0x182d74(0x134)]());}if(_0x1221b8[_0x182d74(0x112)](/\[Tier[ ](\d+)\]/i)){const _0x1ea841=Number(RegExp['$1']);_0x1ea841<tier?(alert(_0x182d74(0x11f)[_0x182d74(0x104)](_0xb03c64,_0x1ea841,tier)),SceneManager[_0x182d74(0x134)]()):tier=Math[_0x182d74(0x114)](_0x1ea841,tier);}VisuMZ[_0x182d74(0x12b)](VisuMZ[label][_0x182d74(0xf6)],_0x33c084[_0x182d74(0x126)]);})(pluginData),VisuMZ[_0x242feb(0x12d)][_0x242feb(0x12e)]=function(_0x12ebff){const _0xe6a30=_0x242feb;_0x12ebff=JsonEx[_0xe6a30(0x122)](_0x12ebff);if(_0x12ebff[_0xe6a30(0x132)]){const _0x94874a=_0x12ebff['FolderFilename'][_0xe6a30(0x15c)]('/');_0x12ebff[_0xe6a30(0x150)]=_0x94874a[0x0]||'',_0x12ebff['Filename']=_0x94874a[0x1]||'';}else _0x12ebff['Folder']='',_0x12ebff[_0xe6a30(0xfe)]='';return _0x12ebff[_0xe6a30(0x159)]=_0x12ebff['Extra']||{},_0x12ebff['Extra'][_0xe6a30(0x152)]=_0x12ebff['Extra'][_0xe6a30(0x152)]??_0xe6a30(0x128),_0x12ebff[_0xe6a30(0x159)]['blendMode']=_0x12ebff[_0xe6a30(0x159)][_0xe6a30(0x13a)]??0x0,_0x12ebff[_0xe6a30(0x159)][_0xe6a30(0xeb)]=_0x12ebff[_0xe6a30(0x159)][_0xe6a30(0xeb)]??0xff,_0x12ebff['Extra'][_0xe6a30(0x153)]=_0x12ebff['Extra'][_0xe6a30(0x153)]??0x0,_0x12ebff[_0xe6a30(0x159)][_0xe6a30(0x165)]=_0x12ebff[_0xe6a30(0x159)][_0xe6a30(0x165)]??0x0,_0x12ebff;},PluginManager[_0x242feb(0x140)](pluginData[_0x242feb(0x169)],_0x242feb(0x155),_0x1cf3a6=>{const _0x6203d6=_0x242feb;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x6203d6(0x12b)](_0x1cf3a6,_0x1cf3a6);const _0x268cb7=VisuMZ[_0x6203d6(0x12d)]['AdjustSettings'](_0x1cf3a6);if(_0x268cb7['Folder'][_0x6203d6(0x168)]()===''||_0x268cb7[_0x6203d6(0xfe)]==='')return;const _0x524a3d=_0x268cb7['ID']||0x0;$gameTroop[_0x6203d6(0x13c)](_0x524a3d,_0x268cb7);}),PluginManager[_0x242feb(0x140)](pluginData[_0x242feb(0x169)],'BackEnvironmentFade',_0x14148e=>{const _0xd8810c=_0x242feb;if(!SceneManager[_0xd8810c(0x109)]())return;VisuMZ[_0xd8810c(0x12b)](_0x14148e,_0x14148e);const _0x465536=_0x14148e[_0xd8810c(0xeb)],_0x49ca0a=_0x14148e[_0xd8810c(0x12a)];for(const _0x19ada3 of _0x14148e['list']){const _0x1776e7=$gameTroop['getBackEnvironmentSettings'](_0x19ada3);_0x1776e7[_0xd8810c(0x159)]['opacity']=_0x465536,_0x1776e7['duration']=_0x49ca0a;}}),PluginManager[_0x242feb(0x140)](pluginData[_0x242feb(0x169)],_0x242feb(0xf7),_0x4b85fc=>{const _0x12e8a1=_0x242feb;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x12e8a1(0x12b)](_0x4b85fc,_0x4b85fc);const _0x5bca71=SceneManager[_0x12e8a1(0x136)][_0x12e8a1(0x10c)],_0x558d99=![];for(const _0x34cc85 of _0x4b85fc[_0x12e8a1(0x131)]){_0x5bca71['removeBattleEnvironmentSprite'](_0x34cc85,_0x558d99);}}),PluginManager[_0x242feb(0x140)](pluginData[_0x242feb(0x169)],'FrontEnvironmentAddChange',_0x3758b9=>{const _0x32b04f=_0x242feb;if(!SceneManager[_0x32b04f(0x109)]())return;VisuMZ[_0x32b04f(0x12b)](_0x3758b9,_0x3758b9);const _0x3a701f=VisuMZ[_0x32b04f(0x12d)]['AdjustSettings'](_0x3758b9);if(_0x3a701f[_0x32b04f(0x150)][_0x32b04f(0x168)]()===''||_0x3a701f[_0x32b04f(0xfe)]==='')return;const _0x580694=_0x3a701f['ID']||0x0;$gameTroop[_0x32b04f(0xff)](_0x580694,_0x3a701f);}),PluginManager[_0x242feb(0x140)](pluginData[_0x242feb(0x169)],_0x242feb(0x118),_0xa308d8=>{const _0xb140ba=_0x242feb;if(!SceneManager[_0xb140ba(0x109)]())return;VisuMZ[_0xb140ba(0x12b)](_0xa308d8,_0xa308d8);const _0x1e4afa=_0xa308d8[_0xb140ba(0xeb)],_0x66cdc1=_0xa308d8[_0xb140ba(0x12a)];for(const _0xf3b559 of _0xa308d8[_0xb140ba(0x131)]){const _0x3e0688=$gameTroop[_0xb140ba(0x148)](_0xf3b559);_0x3e0688[_0xb140ba(0x159)][_0xb140ba(0xeb)]=_0x1e4afa,_0x3e0688[_0xb140ba(0x12a)]=_0x66cdc1;}}),PluginManager[_0x242feb(0x140)](pluginData[_0x242feb(0x169)],_0x242feb(0x117),_0x385eea=>{const _0xc75e5e=_0x242feb;if(!SceneManager[_0xc75e5e(0x109)]())return;VisuMZ[_0xc75e5e(0x12b)](_0x385eea,_0x385eea);const _0x548792=SceneManager[_0xc75e5e(0x136)][_0xc75e5e(0x10c)],_0x32e6b3=!![];for(const _0x42e0fd of _0x385eea[_0xc75e5e(0x131)]){_0x548792[_0xc75e5e(0x13d)](_0x42e0fd,_0x32e6b3);}}),VisuMZ[_0x242feb(0x12d)][_0x242feb(0x147)]={'Type1':/<(?:NOTETAG):[ ](\d+)([%％])>/i,'Type2':/<(?:NOTETAG):[ ]([\+\-]\d+)>/i,'Type3':/<(?:NOTETAG):[ ](.*)>/i,'Type3nonGreedy':/<(?:NOTETAG):[ ](.*?)>/i,'Type4':/<(?:NOTETAG):[ ]*(\d+(?:\s*,\s*\d+)*)>/i,'Type5':/<(?:NOTETAG):[ ](\d+)[ ](?:THROUGH|to)[ ](\d+)>/i,'Type6':/<(?:NOTETAG)>/i,'Type7':/<\/(?:NOTETAG)>/i,'Type8':/<(?:NOTETAG)>\s*([\s\S]*)\s*<\/(?:NOTETAG)>/i},VisuMZ['VisualBattleEnv'][_0x242feb(0xef)]=Game_Troop['prototype'][_0x242feb(0x158)],Game_Troop['prototype'][_0x242feb(0x158)]=function(_0x4e2c99){const _0x87423d=_0x242feb;VisuMZ[_0x87423d(0x12d)][_0x87423d(0xef)][_0x87423d(0xf4)](this,_0x4e2c99),this[_0x87423d(0x145)]();},Game_Troop[_0x242feb(0x156)][_0x242feb(0x145)]=function(){const _0xda55bc=_0x242feb;this[_0xda55bc(0x15d)]=[],this[_0xda55bc(0x13e)]=[];},Game_Troop[_0x242feb(0x156)]['getBackEnvironmentSettings']=function(_0x2fdf9c){const _0x133a49=_0x242feb;return this[_0x133a49(0x15d)]===undefined&&this['setupVisualBattleEnvironment'](),this[_0x133a49(0x15d)][_0x2fdf9c]=this['_backEnvironmentSettings'][_0x2fdf9c]||{},this[_0x133a49(0x15d)][_0x2fdf9c];},Game_Troop['prototype']['setBackEnvironmentSettings']=function(_0x21389e,_0x221c2b){const _0x237524=_0x242feb;this[_0x237524(0x15d)]===undefined&&this[_0x237524(0x145)]();this[_0x237524(0x15d)][_0x21389e]=JsonEx[_0x237524(0x122)](_0x221c2b);if(SceneManager[_0x237524(0x109)]()){const _0x78d8a9=SceneManager[_0x237524(0x136)][_0x237524(0x10c)];_0x78d8a9[_0x237524(0x11a)](_0x21389e,![]);}},Game_Troop[_0x242feb(0x156)][_0x242feb(0x148)]=function(_0xb3ee3d){const _0x36d7a8=_0x242feb;return this['_frontEnvironmentSettings']===undefined&&this[_0x36d7a8(0x145)](),this[_0x36d7a8(0x13e)][_0xb3ee3d]=this[_0x36d7a8(0x13e)][_0xb3ee3d]||{},this['_frontEnvironmentSettings'][_0xb3ee3d];},Game_Troop[_0x242feb(0x156)][_0x242feb(0xff)]=function(_0x23f71b,_0x2b51cc){const _0x3ee15a=_0x242feb;this[_0x3ee15a(0x13e)]===undefined&&this[_0x3ee15a(0x145)]();this[_0x3ee15a(0x13e)][_0x23f71b]=JsonEx[_0x3ee15a(0x122)](_0x2b51cc);if(SceneManager['isSceneBattle']()){const _0xeab020=SceneManager[_0x3ee15a(0x136)][_0x3ee15a(0x10c)];_0xeab020['updateBattleEnvironmentSprite'](_0x23f71b,!![]);}};function Sprite_BattleEnvironment(){const _0xbc3c66=_0x242feb;this[_0xbc3c66(0x15e)](...arguments);}Sprite_BattleEnvironment[_0x242feb(0x156)]=Object[_0x242feb(0x108)](Sprite_Battleback['prototype']),Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x144)]=Sprite_BattleEnvironment,Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x15e)]=function(_0x34a7ca,_0x30944d){const _0x217ff0=_0x242feb;this[_0x217ff0(0x15a)]=_0x34a7ca,this[_0x217ff0(0x10a)]=_0x30944d,Sprite_Battleback[_0x217ff0(0x156)][_0x217ff0(0x15e)][_0x217ff0(0xf4)](this,0x0),this[_0x217ff0(0x121)](),this[_0x217ff0(0xeb)]=0x0;},Sprite_BattleEnvironment[_0x242feb(0x156)]['battleback1Bitmap']=function(){},Sprite_BattleEnvironment['prototype']['_createColorFilter']=function(){const _0x4cacad=_0x242feb;!this[_0x4cacad(0x10b)]&&(this[_0x4cacad(0x10b)]=new ColorFilter()),!this[_0x4cacad(0xfa)]&&(this[_0x4cacad(0xfa)]=[]),this['filters'][_0x4cacad(0xee)](this[_0x4cacad(0x10b)]);},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x149)]=function(){const _0xe16b1b=_0x242feb;return this[_0xe16b1b(0x10a)]?$gameTroop[_0xe16b1b(0x148)](this[_0xe16b1b(0x15a)]):$gameTroop[_0xe16b1b(0xf3)](this[_0xe16b1b(0x15a)]);},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x12c)]=function(){const _0xe54dbe=_0x242feb;Sprite_Battleback[_0xe54dbe(0x156)][_0xe54dbe(0x12c)][_0xe54dbe(0xf4)](this),this[_0xe54dbe(0x107)](),this[_0xe54dbe(0x164)](),this['updateOpacity'](),this[_0xe54dbe(0xf0)](),this[_0xe54dbe(0x13f)]();},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x107)]=function(){const _0x373185=_0x242feb,_0x436195=this[_0x373185(0x149)]();if(!_0x436195)return;if(this[_0x373185(0x163)]===_0x436195[_0x373185(0x150)]&&this[_0x373185(0x11d)]===_0x436195[_0x373185(0xfe)])return;this['_folder']=_0x436195[_0x373185(0x150)],this[_0x373185(0x11d)]=_0x436195[_0x373185(0xfe)];const _0x2f1158=_0x373185(0xf5)[_0x373185(0x104)](this[_0x373185(0x163)][_0x373185(0x168)]()),_0x25009a=ImageManager[_0x373185(0x129)](_0x2f1158,this[_0x373185(0x11d)][_0x373185(0x168)]());_0x25009a[_0x373185(0x139)](this[_0x373185(0xec)]['bind'](this,_0x25009a));},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0xec)]=function(_0x53dd57){const _0x4345e7=_0x242feb;this[_0x4345e7(0xfb)]=_0x53dd57,this[_0x4345e7(0x14c)](),this['origin']['x']=0x0,this['origin']['y']=0x0;},Sprite_BattleEnvironment[_0x242feb(0x156)]['adjustPosition']=function(){const _0x34ed98=_0x242feb,_0x58ea12=this['settings']();if(!_0x58ea12)return;let _0x1eca4e=_0x58ea12[_0x34ed98(0x159)][_0x34ed98(0x152)]||'BattleCore';_0x1eca4e==='BattleCore'&&(_0x1eca4e=VisuMZ[_0x34ed98(0x128)][_0x34ed98(0xf6)][_0x34ed98(0x146)][_0x34ed98(0x103)]||'MZ');switch(_0x1eca4e){case'MZ':VisuMZ[_0x34ed98(0x128)][_0x34ed98(0x127)]['call'](this);break;case _0x34ed98(0x10e):this['adjustPosition_1for1']();break;case _0x34ed98(0x160):this['adjustPosition_ScaleToFit']();break;case _0x34ed98(0x14a):this[_0x34ed98(0x102)]();break;case _0x34ed98(0x125):this[_0x34ed98(0x14f)]();break;}},Sprite_BattleEnvironment[_0x242feb(0x156)]['updateBlendMode']=function(){const _0x3e6184=_0x242feb,_0x4e3111=this[_0x3e6184(0x149)]();if(!_0x4e3111)return;this[_0x3e6184(0x13a)]!==_0x4e3111[_0x3e6184(0x159)]['blendMode']&&(this[_0x3e6184(0x13a)]=_0x4e3111[_0x3e6184(0x159)][_0x3e6184(0x13a)]);},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x166)]=function(){const _0x6b2ac2=_0x242feb,_0xa09c21=this[_0x6b2ac2(0x149)]();if(!_0xa09c21)return;if(_0xa09c21[_0x6b2ac2(0x12a)]>0x0){const _0x25ae56=_0xa09c21[_0x6b2ac2(0x12a)],_0x3a93b5=_0xa09c21[_0x6b2ac2(0x159)][_0x6b2ac2(0xeb)];this['opacity']=(this[_0x6b2ac2(0xeb)]*(_0x25ae56-0x1)+_0x3a93b5)/_0x25ae56,_0xa09c21[_0x6b2ac2(0x12a)]--;}},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0xf0)]=function(){const _0x3d832f=_0x242feb,_0x34b6b0=this['settings']();if(!_0x34b6b0)return;this['origin']['x']+=_0x34b6b0[_0x3d832f(0x159)][_0x3d832f(0x153)],this[_0x3d832f(0x154)]['y']+=_0x34b6b0[_0x3d832f(0x159)][_0x3d832f(0x165)];},Sprite_BattleEnvironment[_0x242feb(0x156)][_0x242feb(0x13f)]=function(){const _0x1cffe1=_0x242feb,_0x2269c6=this[_0x1cffe1(0x149)]();if(!_0x2269c6)return;!this[_0x1cffe1(0x10b)]&&this[_0x1cffe1(0x121)](),this[_0x1cffe1(0x10b)]['setHue'](_0x2269c6[_0x1cffe1(0x159)]['hue']),this[_0x1cffe1(0x10b)][_0x1cffe1(0xf9)](_0x2269c6[_0x1cffe1(0x159)][_0x1cffe1(0x161)]),_0x2269c6['Extra']['hue']+=_0x2269c6[_0x1cffe1(0x159)][_0x1cffe1(0x11e)];},VisuMZ[_0x242feb(0x12d)][_0x242feb(0x11c)]=Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x14b)],Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x14b)]=function(){const _0x541032=_0x242feb;VisuMZ[_0x541032(0x12d)]['Spriteset_Battle_createBattleback'][_0x541032(0xf4)](this),this[_0x541032(0x116)]();},Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x116)]=function(){const _0x5619f7=_0x242feb;this['_backEnvironmentContainer']=new Sprite(),this[_0x5619f7(0x14d)][_0x5619f7(0x110)](this[_0x5619f7(0x111)]);},VisuMZ[_0x242feb(0x12d)][_0x242feb(0x12f)]=Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0xea)],Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0xea)]=function(){const _0xe2fb42=_0x242feb;this[_0xe2fb42(0x143)](),VisuMZ[_0xe2fb42(0x12d)][_0xe2fb42(0x12f)]['call'](this);},Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x143)]=function(){const _0x461336=_0x242feb;this[_0x461336(0xf8)]=new Sprite(),this['_battleField'][_0x461336(0x110)](this[_0x461336(0xf8)]);},VisuMZ[_0x242feb(0x12d)][_0x242feb(0x162)]=Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x12c)],Spriteset_Battle[_0x242feb(0x156)]['update']=function(){const _0x2db46f=_0x242feb;VisuMZ[_0x2db46f(0x12d)][_0x2db46f(0x162)][_0x2db46f(0xf4)](this);},Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0xed)]=function(){const _0x20550c=_0x242feb;if(this[_0x20550c(0xf8)]){}},Spriteset_Battle[_0x242feb(0x156)]['getBattleEnvironmentContainer']=function(_0x3e19e1){const _0x4db0b2=_0x242feb;return _0x3e19e1?this[_0x4db0b2(0xf8)]:this[_0x4db0b2(0x111)];},Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x130)]=function(_0x11c58d,_0x42bfa3){const _0x5aa759=_0x242feb,_0x2c3734=this['getBattleEnvironmentContainer'](_0x42bfa3);return _0x2c3734[_0x5aa759(0x133)][_0x5aa759(0x10f)](_0xe9758=>_0xe9758[_0x5aa759(0x15a)]===_0x11c58d);},Spriteset_Battle[_0x242feb(0x156)][_0x242feb(0x11a)]=function(_0xea3d53,_0x15c5bf){const _0x348393=_0x242feb,_0xef994d=this[_0x348393(0xfd)](_0x15c5bf);if(!_0xef994d)return;!this[_0x348393(0x130)](_0xea3d53,_0x15c5bf)&&this['createBattleEnvironmentSprite'](_0xea3d53,_0x15c5bf);},Spriteset_Battle[_0x242feb(0x156)]['createBattleEnvironmentSprite']=function(_0x32f85e,_0x1f2508){const _0x5469ba=_0x242feb,_0x39c29e=this[_0x5469ba(0xfd)](_0x1f2508);if(!_0x39c29e)return;if(!this[_0x5469ba(0x130)](_0x32f85e,_0x1f2508)){const _0x376008=new Sprite_BattleEnvironment(_0x32f85e,_0x1f2508);_0x39c29e['addChild'](_0x376008),_0x39c29e['children']['sort']((_0x32a13f,_0x138287)=>_0x32a13f['_id']-_0x138287[_0x5469ba(0x15a)]);}},Spriteset_Battle[_0x242feb(0x156)]['removeBattleEnvironmentSprite']=function(_0x529582,_0x45c4e9){const _0x3eaab6=_0x242feb,_0x5f5669=this[_0x3eaab6(0xfd)](_0x45c4e9);if(!_0x5f5669)return;const _0x2bef5e=this[_0x3eaab6(0x130)](_0x529582,_0x45c4e9);_0x2bef5e&&(_0x5f5669[_0x3eaab6(0x135)](_0x2bef5e),_0x5f5669['children'][_0x3eaab6(0x100)]((_0x3accee,_0x2e2e8b)=>_0x3accee['_id']-_0x2e2e8b[_0x3eaab6(0x15a)]));};