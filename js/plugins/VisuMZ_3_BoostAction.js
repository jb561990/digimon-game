//=============================================================================
// VisuStella MZ - Boost Action
// VisuMZ_3_BoostAction.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_3_BoostAction = true;

var VisuMZ = VisuMZ || {};
VisuMZ.BoostAction = VisuMZ.BoostAction || {};
VisuMZ.BoostAction.version = 1.01;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 3] [Version 1.01] [BoostAction]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Boost_Action_VisuStella_MZ
 * @base VisuMZ_0_CoreEngine
 * @base VisuMZ_1_BattleCore
 * @base VisuMZ_1_MessageCore
 * @base VisuMZ_1_SkillsStatesCore
 * @orderAfter VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_SkillsStatesCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin adds Boost Points, a mechanic which augments the potency of
 * skills and/or items based on the type of notetag they have. The newly added
 * mechanic allows actors and/or enemies to temporarily power themselves up for
 * the current turn by using the Boost Points resource. Boost Points are gained
 * at the end of each turn if the battler did not use any Boost Points. While
 * Boosted, actions can deal more damage, hit more times, make buffs/debuffs or
 * states last longer, and more!
 *
 * Features include all (but not limited to) the following:
 * 
 * * Add a new battle resource to your game: Boost Points!
 * * Determine how many Boost Points can be stored at a time!
 * * Also determine how many Boost Points can be used at a time!
 * * Determine how Boosting affects skills and items through the different
 *   kinds of notetags provided through this plug.
 * * Enemies can Boost, too! As long as the proper notetags are in place!
 * * Utilize Shortcut Keys to quickly Boost skills and/or items.
 * * Boosting skills and/or items can also affect the text displayed in the
 *   Help Window, too!
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_0_CoreEngine
 * * VisuMZ_1_BattleCore
 * * VisuMZ_1_SkillsStatesCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 3 ------
 *
 * This plugin is a Tier 3 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 *
 * VisuMZ_3_WeaknessDisplay
 *
 * The "Analyze" ability in the VisuStella MZ Weakness Display can be Boosted
 * through this plugin and reveal multiple weaknesses at a time.
 *
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins or specific
 * features. This section will highlight the main plugins/features that will
 * not be compatible with this plugin or put focus on how the make certain
 * features compatible.
 *
 * ---
 * 
 * VisuMZ_1_BattleCore
 * 
 * When using Action Sequences, Boost effects for damage, turn extensions,
 * analyze, etc. will not occur for anything other than the Action Sequence:
 * "MECH: Action Effect" in order to maintain controlled effects. However, if
 * you do want to apply bonuses for Boosts, utilize "MECH: Boost Store Data" to
 * store inside a variable how many times Boosts were used. This can be used
 * however which way you want it to as long as it is manageable through events
 * and Common Events.
 * 
 * ---
 *
 * VisuMZ_2_BattleSystemBTB
 * 
 * The Boost Actions plugin cannot be used together with Battle System - BTB.
 * If the Battle System is switched to using Battle System - BTB, then the
 * Boost Actions plugin will shut itself off.
 * 
 * The reason why these plugins cannot work together is because their mechanics
 * play off too similarly to each other and cause conflicts. We, the plugin
 * developer team, highly recommend that you utilize Battle System - BTB's
 * Brave system instead of the Boost system to make the best use of the battle
 * system in effect.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === Boost Effect-Related Notetags ===
 * 
 * ---
 *
 * <Boost Damage>
 *
 * - Used for: Skill, Item Notetags
 * - Boosts will alter the damage dealt by this skill/item.
 * - The amount of damage increased will be determined by the Plugin Parameter
 *   Mechanical settings for Damage Multipliers and Addition.
 * - When using Action Sequences, this will have no effect outside of the
 *   MECH: Action Effect command.
 *
 * ---
 *
 * <Boost Turns>
 *
 * - Used for: Skill, Item Notetags
 * - Boosts will alter the duration of skills, buffs, and debuffs added by
 *   this skill/item.
 * - The amount of turns increased will be determined by the Plugin Parameter
 *   Mechanical settings for Turn Multipliers and Addition.
 * - When using Action Sequences, this will have no effect outside of the
 *   MECH: Action Effect command.
 *
 * ---
 *
 * <Boost Repeat>
 *
 * - Used for: Skill, Item Notetags
 * - Boosts will alter the number of repeated hits dealt by this skill/item.
 * - The amount of hits increased will be determined by the Plugin Parameter
 *   Mechanical settings for Repeated Hits Multipliers and Addition.
 * - When using Action Sequences, this will have no effect outside of the
 *   MECH: Action Effect command.
 *
 * ---
 *
 * <Boost Effect Gain>
 *
 * - Used for: Skill, Item Notetags
 * - Boosts will alter the number of Boost Points acquired through the
 *   <Target Boost Points: +x> and <User Boost Points: +x> notetags.
 * - The power of the effect will be determined by the Plugin Parameter
 *   Mechanical settings for Effect Multipliers and Addition.
 * - When using Action Sequences, this will have no effect outside of the
 *   MECH: Action Effect command.
 *
 * ---
 *
 * <Boost Analyze>
 *
 * - Used for: Skill, Item Notetags
 * - Requires VisuMZ_3_WeaknessDisplay!
 * - Boosts will alter the number of revealed weaknesses by this skill/item.
 * - The amount of weaknesses revealed will be determined by the Plugin
 *   Parameter Mechanical settings for Analyze Multipliers and Addition.
 * - When using Action Sequences, this will have no effect outside of the
 *   MECH: Action Effect command.
 * 
 * ---
 * 
 * === Boost Points Gain/Loss-Related Notetags ===
 * 
 * ---
 *
 * <User Boost Points: +x>
 * <User Boost Points: -x>
 *
 * <Target Boost Points: +x>
 * <Target Boost Points: -x>
 *
 * - Used for: Skill, Item Notetags
 * - The user/target will gain/lose Boost Points if this skill/item connects.
 * - Replace 'x' with a number representing the number of Boost Points for the
 *   user/target to gain/lose.
 *
 * ---
 *
 * <Boost Points Battle Start: x%>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Determines how many extra (or less) Boost Points the battler will start
 *   a new battle with.
 * - Replace 'x' with a number representing the amount of Boost Points to
 *   increase or decrease the starting Boost Points value by multiplicatively.
 *
 * ---
 *
 * <Boost Points Battle Start: +x>
 * <Boost Points Battle Start: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Determines how many extra (or less) Boost Points the battler will start
 *   a new battle with.
 * - Replace 'x' with a number representing the amount of Boost Points to
 *   increase or decrease the starting Boost Points value by additively.
 *
 * ---
 * 
 * === Boost Point Requirements-Related Notetags ===
 * 
 * The following are notetags that make skills/items require a certain amount
 * of Boost Points to be in "use" before they can become enabled.
 * 
 * ---
 *
 * <Require x Boost Points>
 *
 * - Used for: Skill, Item Notetags
 * - This causes the skill to require at least x Boost Points to be spent.
 * - Replace 'x' with a number value representing the Boost Points to be spent.
 *
 * ---
 *
 * <Require >= x Boost Points>
 *
 * - Used for: Skill, Item Notetags
 * - This causes the skill to require at least x Boost Points to be spent.
 * - Replace 'x' with a number value representing the Boost Points to be spent.
 *
 * ---
 *
 * <Require > x Boost Points>
 *
 * - Used for: Skill, Item Notetags
 * - This causes the skill to require more than x Boost Points to be spent.
 * - Replace 'x' with a number value representing the Boost Points to be spent.
 *
 * ---
 *
 * <Require = x Boost Points>
 *
 * - Used for: Skill, Item Notetags
 * - This causes the skill to require exactly x Boost Points to be spent.
 * - Replace 'x' with a number value representing the Boost Points to be spent.
 *
 * ---
 *
 * <Require < x Boost Points>
 *
 * - Used for: Skill, Item Notetags
 * - This causes the skill to require less than x Boost Points to be spent.
 * - Replace 'x' with a number value representing the Boost Points to be spent.
 *
 * ---
 *
 * <Require <= x Boost Points>
 *
 * - Used for: Skill, Item Notetags
 * - This causes the skill to require at most x Boost Points to be spent.
 * - Replace 'x' with a number value representing the Boost Points to be spent.
 *
 * ---
 * 
 * === Boosting-Related Notetags ===
 * 
 * ---
 *
 * <Boost Stealed>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - As long as the battler is affected by a trait object with this notetag,
 *   the battler cannot Boost.
 *
 * ---
 * 
 * === Enemy-Related Notetags ===
 * 
 * ---
 *
 * <Boost Skill id: Full>
 * <Boost name: Full>
 *
 * - Used for: Enemy Notetags
 * - Determines which skills the notetag'd enemy will Boost and their range
 *   for Boosting it.
 * - When the enemy Boosts for this skill, the enemy will use as many Boost
 *   Points as it can to cast it.
 * - Replace 'id' with a number representing the skill ID to Boost.
 * - Replace 'name' with the skill's name to Boost.
 *
 * ---
 *
 * <Boost Skill id: At Least x>
 * <Boost name: At Least x>
 *
 * - Used for: Enemy Notetags
 * - Determines which skills the notetag'd enemy will Boost and their range
 *   for Boosting it.
 * - When the enemy Boosts for this skill, the enemy will only use Boost Points
 *   after reaching 'x' Boost Points and will use as much as it can.
 * - Replace 'id' with a number representing the skill ID to Boost.
 * - Replace 'name' with the skill's name to Boost.
 *
 * ---
 *
 * <Boost Skill id: At Most x>
 * <Boost name: At Most x>
 *
 * - Used for: Enemy Notetags
 * - Determines which skills the notetag'd enemy will Boost and their range
 *   for Boosting it.
 * - When the enemy Boosts for this skill, the enemy will only as many Boost
 *   Points as it can unless the Boost Points spent go over 'x'.
 * - Replace 'id' with a number representing the skill ID to Boost.
 * - Replace 'name' with the skill's name to Boost.
 *
 * ---
 *
 * <Boost Skill id: Exactly x>
 * <Boost name: Exactly x>
 *
 * - Used for: Enemy Notetags
 * - Determines which skills the notetag'd enemy will Boost and their range
 *   for Boosting it.
 * - When the enemy Boosts for this skill, the enemy will only use 'x' Boost
 *   Points when Boosting for the skill.
 * - Replace 'id' with a number representing the skill ID to Boost.
 * - Replace 'name' with the skill's name to Boost.
 *
 * ---
 * 
 * === Regeneration-Related Notetags ===
 * 
 * ---
 *
 * <Boost Points Regen: x%>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Alters the amount of Boost Points gained when regenerating Boost Points
 *   (as long as the battler can).
 * - Replace 'x' with a number value representing the amount of Boost Points
 *   to increase or decrease the regenerated amount by multiplicatively.
 *
 * ---
 *
 * <Boost Points Regen: +x>
 * <Boost Points Regen: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Alters the amount of Boost Points gained when regenerating Boost Points
 *   (as long as the battler can).
 * - Replace 'x' with a number value representing the amount of Boost Points
 *   to increase or decrease the regenerated amount by additively.
 *
 * ---
 *
 * ============================================================================
 * Available Text Codes
 * ============================================================================
 *
 * The following are text codes that you may use with this plugin. 
 *
 * === Boosting-Related Text Codes ===
 *
 * These text codes are used for Help Window descriptions. When Boosting, the
 * text displayed in the Help Window can change to reflect the amount boosted.
 *
 * ---
 *
 * ------------------   -------------------------------------------------------
 * Text Code            Effect (Help Window Text Only)
 * ------------------   -------------------------------------------------------
 * 
 * \boostDamage[x]      This will apply damage modifiers to number x based on
 *                      the actor's currently used Boost amount.
 * 
 * \boostTurn[x]        This will apply turn modifiers to number x based on
 *                      the actor's currently used Boost amount.
 * 
 * \boostRepeat[x]      This will apply repeat hit modifiers to number x based
 *                      on the actor's currently used Boost amount.
 * 
 * \boostEffect[x]      This will apply Boost Point effect modifiers to number
 *                      x based on the actor's currently used Boost amount.
 * 
 * \boostAnalyze[x]     This will apply analyze modifiers to number x based on
 *                      the actor's currently used Boost amount.
 * 
 * ---
 *
 * ------------------   -------------------------------------------------------
 * Text Code            Effect (Help Window Text Only)
 * ------------------   -------------------------------------------------------
 * 
 * \boost[text]         The text inside the brackets won't appear unless at
 *                      least 1 Boost is used.
 * 
 * \boost0[text]        The text inside the brackets won't appear unless if any
 *                      Boost is used.
 * 
 * \boost>=x[text]      The text inside the brackets will only appear if at
 *                      least x Boosts are used.
 * 
 * \boost>x[text]       The text inside the brackets will only appear if more
 *                      than x Boosts are used.
 * 
 * \boost=x[text]       The text inside the brackets will only appear if
 *                      exactly x Boosts are used.
 * 
 * \boost<x[text]       The text inside the brackets will only appear if less
 *                      than x Boosts are used.
 * 
 * \boost<=x[text]      The text inside the brackets will only appear if at
 *                      most x Boosts are used.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * These Plugin Parameters govern the mechanics behind Boost Points and
 * Boosting for this RPG Maker MZ plugin.
 *
 * ---
 *
 * Boost Points
 * 
 *   Max Stored:
 *   - Maximum Boost Points that can be stored at any time.
 * 
 *   Max Usable:
 *   - How many Boost Points can be usable at a time?
 * 
 *   Start Battle:
 *   - How many Boost Points as a base do you want battlers to start
 *     battles with?
 * 
 *   Regeneration:
 *   - How many Boost Points do you want battlers to regenerate each
 *     turn applicable?
 * 
 *     Always Regen?:
 *     - Always regenerate Boost Points each turn?
 *     - Otherwise, regenerate on turns when Boost Points weren't used.
 * 
 *     Death Regen?:
 *     - Regenerate Boost Points while dead?
 *     - Otherwise, don't.
 * 
 *   Death Removal?:
 *   - Remove all stored Boost Points on death?
 *   - Otherwise, keep them.
 *
 * ---
 *
 * Boost Animations
 * 
 *   Animation ID's:
 *   - Animation IDs start from 0 Boosts to max.
 *   - These animations play when Boosting/Unboosting.
 * 
 *   Animation Delay:
 *   - How many milliseconds to play between animations when enemies
 *     Boost actions?
 *
 * ---
 *
 * Boost Modifiers
 * 
 *   Damage:
 * 
 *     Multipliers:
 *     - Damage multipliers start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Damage> notetag.
 * 
 *     Addition:
 *     - Damage addition start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Damage> notetag.
 * 
 *   State/Buff Turns:
 * 
 *     Multipliers:
 *     - Turn multipliers start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Turns> notetag.
 * 
 *     Addition:
 *     - Turn addition start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Turns> notetag.
 * 
 *   Repeated Hits:
 * 
 *     Multipliers:
 *     - Repeat multipliers start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Repeat> notetag.
 * 
 *     Addition:
 *     - Repeat addition start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Repeat> notetag.
 * 
 *   Effect:
 * 
 *     Multipliers:
 *     - Effect multipliers start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Effect Gain> notetag.
 * 
 *     Addition:
 *     - Effect addition start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Effect Gain> notetag.
 * 
 *   Analyze:
 * 
 *     Multipliers:
 *     - Analyze multipliers start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Analyze> notetag.
 * 
 *     Addition:
 *     - Analyze addition start from 0 Boosts to max.
 *     - Affects skills/items with <Boost Analyze> notetag.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: UI Settings
 * ============================================================================
 *
 * These Plugin Parameter settings govern the visual elements related to Boost
 * Points and Boosting.
 *
 * ---
 *
 * Icons
 * 
 *   Boost Icon:
 *   - What icon do you wish to represent Boosting and
 *     Boost Point availability?
 * 
 *   Empty Icon:
 *   - What icon do you wish to represent Unboosting and
 *     Boost Point absence?
 * 
 *   Icon Size Rate:
 *   - What size do you wish the icons to be displayed at?
 *   - Use a number between 0 and 1 for the best results.
 * 
 *   Smooth Icons?:
 *   - Do you wish to smooth out the icons or pixelate them?
 *
 * ---
 *
 * Vocab
 * 
 *   Boost Command:
 *   - This is the text used for the "Boost" command displayed in the
 *     Actor Command Window.
 * 
 *     Show?:
 *     - Show this command in the Actor Command Window?
 * 
 *   Unboost Command:
 *   - This is the text used for the "Unboost" command displayed in the
 *     Actor Command Window.
 * 
 *     Show?:
 *     - Show this command in the Actor Command Window?
 *
 * ---
 *
 * Shortcut Controls
 * 
 *   Page Up/Dn Shortcuts?:
 *   - Enable Page Up/Down keys to adjust Boost points as a shortcut?
 * 
 *   Bypassed Windows:
 *   - These are constructor names for windows that the shortcut key will not
 *     work on.
 *
 * ---
 *
 * Battle Status
 * 
 *   Show Boost Points?:
 *   - Show Boost Points in the Battle Status Window?
 * 
 *   Auto-Position?:
 *   - Automatically position the Boost Points?
 *   - If not, it'll position it to the upper left.
 * 
 *   Offset X/Y:
 *   - How much to offset the Boost icons X/Y position by?
 *   - For X: Negative goes left. Positive goes right.
 *   - For Y: Negative goes up. Positive goes down.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.01: April 9, 2021
 * * Bug Fixes!
 * ** <User Boost Points: +x> notetag should now work properly. Fix by Olivia.
 * 
 * Version 1.00 Official Release Date: May 5, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BoostAction
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Settings for the Boost Action mechanics.
 * @default {"BoostPoints":"","MaxStored:num":"5","Usable:num":"3","StartBattle:num":"1","Regen:num":"1","AlwaysRegen:eval":"false","DeathRegen:eval":"false","DeathRemoval:eval":"true","Animations":"","Animations:arraynum":"[\"12\",\"13\",\"15\",\"14\",\"2\",\"51\",\"52\",\"53\",\"67\",\"66\",\"107\"]","AnimationDelay:num":"800","Modifiers":"","Damage":"","DmgMultiply:arraynum":"[\"1.0\",\"2.0\",\"3.0\",\"4.0\",\"5.0\",\"6.0\",\"7.0\",\"8.0\",\"9.0\",\"10.0\",\"11.0\"]","DmgAddition:arraynum":"[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]","Turns":"","TurnMultiply:arraynum":"[\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\",\"1.0\"]","TurnAddition:arraynum":"[\"0\",\"2\",\"4\",\"6\",\"8\",\"10\",\"12\",\"14\",\"16\",\"18\",\"20\"]","Repeat":"","RepeatMultiply:arraynum":"[\"1.0\",\"2.0\",\"3.0\",\"4.0\",\"5.0\",\"6.0\",\"7.0\",\"8.0\",\"9.0\",\"10.0\",\"11.0\"]","RepeatAddition:arraynum":"[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]","Effect":"","EffectMultiply:arraynum":"[\"1.0\",\"2.0\",\"3.0\",\"4.0\",\"5.0\",\"6.0\",\"7.0\",\"8.0\",\"9.0\",\"10.0\",\"11.0\"]","EffectAddition:arraynum":"[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]","Analyze":"","AnalyzeMultiply:arraynum":"[\"1.0\",\"2.0\",\"3.0\",\"4.0\",\"5.0\",\"6.0\",\"7.0\",\"8.0\",\"9.0\",\"10.0\",\"11.0\"]","AnalyzeAddition:arraynum":"[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"]"}
 *
 * @param UI:struct
 * @text UI Settings
 * @type struct<UI>
 * @desc Settings for the Boost Action UI.
 * @default {"Icons":"","BoostIcon:num":"163","EmptyIcon:num":"161","IconSizeRate:num":"0.5","SmoothIcons:eval":"true","Vocab":"","BoostCmd:str":"Boost","ShowBoostCmd:eval":"true","UnboostCmd:str":"Unboost","ShowUnboostCmd:eval":"true","Controls":"","PgUpDnShortcuts:eval":"true","BypassConstructors:arraystr":"[\"Window_BattleActor\",\"Window_BattleEnemy\",\"Window_BattleItem\",\"Window_PartyBattleSwitch\"]","BattleStatus":"","BattleStatusShow:eval":"true","BattleStatusAutoPosition:eval":"true","BattleStatusOffsetX:num":"+0","BattleStatusOffsetY:num":"+0"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param BoostPoints
 * @text Boost Points
 *
 * @param MaxStored:num
 * @text Max Stored
 * @parent BoostPoints
 * @type number
 * @min 1
 * @desc Maximum Boost Points that can be stored at any time.
 * @default 5
 *
 * @param Usable:num
 * @text Max Usable
 * @parent BoostPoints
 * @type number
 * @min 1
 * @desc How many Boost Points can be usable at a time?
 * @default 3
 *
 * @param StartBattle:num
 * @text Start Battle
 * @parent BoostPoints
 * @type number
 * @desc How many Boost Points as a base do you want battlers
 * to start battles with?
 * @default 1
 *
 * @param Regen:num
 * @text Regeneration
 * @parent BoostPoints
 * @type number
 * @desc How many Boost Points do you want battlers to regenerate
 * each turn applicable?
 * @default 1
 *
 * @param AlwaysRegen:eval
 * @text Always Regen?
 * @parent Regen:num
 * @type boolean
 * @on Always
 * @off Other
 * @desc Always regenerate Boost Points each turn? Otherwise,
 * regenerate on turns when Boost Points weren't used.
 * @default false
 *
 * @param DeathRegen:eval
 * @text Death Regen?
 * @parent Regen:num
 * @type boolean
 * @on Regen on Death
 * @off No Regen
 * @desc Regenerate Boost Points while dead?
 * Otherwise, don't.
 * @default false
 *
 * @param DeathRemoval:eval
 * @text Death Removal?
 * @parent BoostPoints
 * @type boolean
 * @on Remove on Death
 * @off No Removal
 * @desc Remove all stored Boost Points on death?
 * Otherwise, keep them.
 * @default true
 * 
 * @param Animations
 * @text Boost Animations
 *
 * @param Animations:arraynum
 * @text Animation ID's
 * @parent Animations
 * @type animation[]
 * @desc Animation IDs start from 0 Boosts to max.
 * These animations play when Boosting/Unboosting.
 * @default ["12","13","15","14","2","51","52","53","67","66","107"]
 *
 * @param AnimationDelay:num
 * @text Animation Delay
 * @parent Animations
 * @type number
 * @desc How many milliseconds to play between animations when
 * enemies Boost actions?
 * @default 1000
 * 
 * @param Modifiers
 * @text Boost Modifiers
 * 
 * @param Damage
 * @parent Modifiers
 *
 * @param DmgMultiply:arraynum
 * @text Multipliers
 * @parent Damage
 * @type string[]
 * @desc Damage multipliers start from 0 Boosts to max.
 * Affects skills/items with <Boost Damage> notetag.
 * @default ["1.0","2.0","3.0","4.0","5.0","6.0","7.0","8.0","9.0","10.0","11.0"]
 *
 * @param DmgAddition:arraynum
 * @text Addition
 * @parent Damage
 * @type string[]
 * @desc Damage addition start from 0 Boosts to max.
 * Affects skills/items with <Boost Damage> notetag.
 * @default ["0","0","0","0","0","0","0","0","0","0","0"]
 * 
 * @param Turns
 * @parent Modifiers
 * @text State/Buff Turns
 *
 * @param TurnMultiply:arraynum
 * @text Multipliers
 * @parent Turns
 * @type string[]
 * @desc Turn multipliers start from 0 Boosts to max.
 * Affects skills/items with <Boost Turns> notetag.
 * @default ["1.0","1.0","1.0","1.0","1.0","1.0","1.0","1.0","1.0","1.0","1.0"]
 *
 * @param TurnAddition:arraynum
 * @text Addition
 * @parent Turns
 * @type string[]
 * @desc Turn addition start from 0 Boosts to max.
 * Affects skills/items with <Boost Turns> notetag.
 * @default ["0","2","4","6","8","10","12","14","16","18","20"]
 * 
 * @param Repeat
 * @parent Modifiers
 * @text Repeated Hits
 *
 * @param RepeatMultiply:arraynum
 * @text Multipliers
 * @parent Repeat
 * @type string[]
 * @desc Repeat multipliers start from 0 Boosts to max.
 * Affects skills/items with <Boost Repeat> notetag.
 * @default ["1.0","2.0","3.0","4.0","5.0","6.0","7.0","8.0","9.0","10.0","11.0"]
 *
 * @param RepeatAddition:arraynum
 * @text Addition
 * @parent Repeat
 * @type string[]
 * @desc Repeat addition start from 0 Boosts to max.
 * Affects skills/items with <Boost Repeat> notetag.
 * @default ["0","0","0","0","0","0","0","0","0","0","0"]
 * 
 * @param Effect
 * @parent Modifiers
 *
 * @param EffectMultiply:arraynum
 * @text Multipliers
 * @parent Effect
 * @type string[]
 * @desc Effect multipliers start from 0 Boosts to max.
 * Affects skills/items with <Boost Effect Gain> notetag.
 * @default ["1.0","2.0","3.0","4.0","5.0","6.0","7.0","8.0","9.0","10.0","11.0"]
 *
 * @param EffectAddition:arraynum
 * @text Addition
 * @parent Effect
 * @type string[]
 * @desc Effect addition start from 0 Boosts to max.
 * Affects skills/items with <Boost Effect Gain> notetag.
 * @default ["0","0","0","0","0","0","0","0","0","0","0"]
 * 
 * @param Analyze
 * @parent Modifiers
 *
 * @param AnalyzeMultiply:arraynum
 * @text Multipliers
 * @parent Analyze
 * @type string[]
 * @desc Analyze multipliers start from 0 Boosts to max.
 * Affects skills/items with <Boost Analyze> notetag.
 * @default ["1.0","2.0","3.0","4.0","5.0","6.0","7.0","8.0","9.0","10.0","11.0"]
 *
 * @param AnalyzeAddition:arraynum
 * @text Addition
 * @parent Analyze
 * @type string[]
 * @desc Analyze addition start from 0 Boosts to max.
 * Affects skills/items with <Boost Analyze> notetag.
 * @default ["0","0","0","0","0","0","0","0","0","0","0"]
 *
 */
/* ----------------------------------------------------------------------------
 * UI Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~UI:
 *
 * @param Icons
 *
 * @param BoostIcon:num
 * @text Boost Icon
 * @parent Icons
 * @desc What icon do you wish to represent Boosting
 * and Boost Point availability?
 * @default 163
 *
 * @param EmptyIcon:num
 * @text Empty Icon
 * @parent Icons
 * @desc What icon do you wish to represent Unboosting
 * and Boost Point absence?
 * @default 161
 *
 * @param IconSizeRate:num
 * @text Icon Size Rate
 * @parent Icons
 * @desc What size do you wish the icons to be displayed at?
 * Use a number between 0 and 1 for the best results.
 * @default 0.5
 *
 * @param SmoothIcons:eval
 * @text Smooth Icons?
 * @parent Icons
 * @type boolean
 * @on Smooth
 * @off Pixelate
 * @desc Do you wish to smooth out the icons or pixelate them?
 * @default true
 * 
 * @param Vocab
 *
 * @param BoostCmd:str
 * @text Boost Command
 * @parent Vocab
 * @desc This is the text used for the "Boost" command
 * displayed in the Actor Command Window.
 * @default Boost
 *
 * @param ShowBoostCmd:eval
 * @text Show?
 * @parent BoostCmd:str
 * @type boolean
 * @on Show Command
 * @off Hide Command
 * @desc Show this command in the Actor Command Window?
 * @default true
 *
 * @param UnboostCmd:str
 * @text Unboost Command
 * @parent Vocab
 * @desc This is the text used for the "Unboost" command
 * displayed in the Actor Command Window.
 * @default Unboost
 *
 * @param ShowUnboostCmd:eval
 * @text Show?
 * @parent UnboostCmd:str
 * @type boolean
 * @on Show Command
 * @off Hide Command
 * @desc Show this command in the Actor Command Window?
 * @default true
 * 
 * @param Controls
 * @text Shortcut Controls
 *
 * @param PgUpDnShortcuts:eval
 * @text Page Up/Dn Shortcuts?
 * @parent Controls
 * @type boolean
 * @on Enable Shortcuts
 * @off Disable Shortcuts
 * @desc Enable Page Up/Down keys to adjust Boost points
 * as a shortcut?
 * @default true
 *
 * @param BypassConstructors:arraystr
 * @text Bypassed Windows
 * @parent Controls
 * @type string[]
 * @desc These are constructor names for windows that the shortcut
 * key will not work on.
 * @default ["Window_BattleActor","Window_BattleEnemy","Window_BattleItem","Window_PartyBattleSwitch"]
 * 
 * @param BattleStatus
 * @text Battle Status
 *
 * @param BattleStatusShow:eval
 * @text Show Boost Points?
 * @parent BattleStatus
 * @type boolean
 * @on Show Boost Points
 * @off Hide Boost Points
 * @desc Show Boost Points in the Battle Status Window?
 * @default true
 *
 * @param BattleStatusAutoPosition:eval
 * @text Auto-Position?
 * @parent BattleStatus
 * @type boolean
 * @on Auto-Position
 * @off Manual Position
 * @desc Automatically position the Boost Points?
 * If not, it'll position it to the upper left.
 * @default true
 *
 * @param BattleStatusOffsetX:num
 * @text Offset X
 * @parent BattleStatus
 * @desc How much to offset the Boost icons X position by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param BattleStatusOffsetY:num
 * @text Offset Y
 * @parent BattleStatus
 * @desc How much to offset the Boost icons Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 *
 */
//=============================================================================

var _0x5541=['initBoostAction','itemRect','isActor','addLoadListener','setupBattleBoostPointsAdded','update','initMembers','setupBattleBoostPoints','setup','commandBoost','toLowerCase','BoostTurns','ARRAYEVAL','createChildSprites','setStoredBoostPoints','faceWidth','item','convertBoostEffectEscape','BOOST_POINTS_MAX_STORED','playOkSound','UserBoostPoints','currentSymbol','isDead','convertBoost0Escape','bitmap','default','_icons','activate','BOOST_POINTS_DEATH_REMOVE','3SAxGkz','isBoostSealed','callUpdateHelp','applyGuard','Turn','Settings','requestFauxAnimation','drawItemStatusBoostPoints','BattleStatusShow','name','BOOST_POINTS_MULTIPLIERS','addBoostCommand','BOOST_POINTS_ANIMATIONS','BattleManager_processTurn','_boostIconSheet','status','Animations','canUseBoostShortcut','boostIcon','addGuardCommand','push','isHidden','iconWidth','_slot','smooth','AnalyzeAddition','portrait','allowBoostAction','createActorCommandWindow','width','unboostIcon','63568femPgN','1260696rUslue','_actor','note','_inBattle','RegExp','cursorPagedown','Require','addCommand','TurnAddition','setBoostSubject','enemy','addBuff','Game_Battler_regenerateAll','DmgMultiply','initialize','SmoothIcons','Analyze','loadBitmap','optDisplayTp','Game_Battler_regenerateTp','BoostPointsRegenFlat','description','Game_Battler_performActionEnd','BattleStatusOffsetX','Amount','_iconIndex','_scene','198769ARCWdk','BoostSealed','applyBoostPointTurns','_helpWindow','convertBoostTurnEscape','Game_BattlerBase_resetStateCounts','FUNC','Window_Selectable_cursorPagedown','selectNextCommand','constructor','max','BOOST_ACTION_BYPASS_CONSTRUCTORS','BoostAction','17topYrA','DeathRemoval','Scene_Battle_selectNextCommand','_storedBoostPoints','BoostDamage','canUndoBoostPoints','Game_BattlerBase_initialize','_turnUsedBoostPoints','isSkill','boostPointsRegenValue','performActionEnd','processEnemyBPUsage','minTurns','commandStyle','unboostCommandName','round','860717OtrEFQ','Usable','MaxStored','setupBattleBoostPointsMultiplier','_bpTurnFlat','border','setFrame','BattleCore','Skill\x20','scale','BoostPointsRegenRate','_battler','regenerateBoostPoints','_subject','Window_Base_convertEscapeCharacters','boostSmooth','IconSet','convertBoostRepeatEscape','regenerateAll','meetsBoostShortcutRequirements','Scene_Battle_startActorCommandSelection','BattleStatusAutoPosition','_logWindow','traitObjects','loadSystem','Game_Battler_removeBattleStates','BOOST_POINTS_START_BATTLE','toUseBoostPoints','randomInt','_boostAI','length','GreaterEqual','AlwaysRegen','filter','ConvertParams','replace','BOOST_POINTS_TURN_REGEN','convertBoostUpEscape','toUpperCase','applyBoostPointDamage','height','DmgAddition','meetstoUseBoostPointsRequirement','regenerateTp','ceil','EffectAddition','RepeatMultiply','RepeatAddition','convertBoostAnalyzeEscape','boostIconsheetBitmap','bind','addChild','bpRegenAdded','Window_Selectable_cursorPageup','shouldDrawBoostIcons','gainStoredBoostPoints','meetsUsableItemConditions','59441uDvslO','gainToUseBoostPoints','ShowBoostCmd','call','ARRAYSTRUCT','BOOST_POINTS_DISPLAY_BATTLE_STATUS','canUseBoostPoints','convertBoostDamageEscape','convertBoostLessEscape','boost','processTurn','actor','apply','floor','ARRAYFUNC','unboost','BOOST_POINTS_MAX_TOUSE','numRepeats','_toUseBoostPoints','updateFrame','some','setToUseBoostPoints','BpEffect','BoostIcon','AnalyzeMultiply','BOOST_POINTS_DEATH_REGEN','convertEscapeCharacters','setHandler','_bpTurnRate','clear','ShowFacesListStyle','parse','EnemyBoostSkillName','_bpSubject','BP\x20Effect','map','list','BOOST_POINTS_DISPLAY_OFFSET_Y','1dYcvrD','_waitCount','processtoUseBoostPoints','Mechanics','refresh','subject','JSON','BattleStatusOffsetY','applyItemUserEffect','clearBoostSubject','removeBattleStates','VisuMZ_1_SkillsStatesCore','Game_BattlerBase_meetsUsableItemConditions','addState','EnemyBoostSkillID','blt','applyBPEffects','prototype','Scene_Battle_createActorCommandWindow','BypassConstructors','convertBoostLessEqualEscape','BOOST_ACTION_SHORTCUT_PAGEUP_PAGEDN','ARRAYNUM','BOOST_ACTION_SHOW','actor%1-boostPoints','UNBOOST_ACTION_SHOW','202908nBhdja','startActorCommandSelection','currentAction','text','boostAddition','exit','boostMultiplier','match','Less','trim','boostTransferBitmap','includes','DeathRegen','create','startChangeBoostPointsAnimation','Game_Action_apply','AnimationDelay','drawItemStatus','parameters','EVAL','908923sdSGlO','placeBoostPoints','__Game_Action_applyItemUserEffect','move','BOOST_POINTS_DISPLAY_OFFSET_X','BOOST_POINTS_DISPLAY_AUTO_POS','BoostGainPoints','BOOST_POINTS_REGEN_ALWAYS','Window_BattleStatus_drawItemStatus','resetStateCounts','version','Repeat','Damage','show','clamp','setupBoostAI','Game_Enemy_setup','calculateBPtoUse','format','BattleLayout','Game_Battler_addDebuff','Equal','BOOST_POINTS_ANIMATION_DELAY','_actorCommandWindow','Game_Action_numRepeats','lineHeight','convertBoostGreaterEqualEscape','isSceneBattle','resize','Game_Battler_addBuff','STRUCT','_stateTurns','\x5cI[%1]%2','ICON_SIZE_RATE','IconSizeRate','PgUpDnShortcuts','battleLayoutStyle','BoostBattleStartRate','BattleManager_setup','BOOST_POINTS_ADDITION','storedBoostPoints','boostCommandName','processEnemyUseBoost','updateIcon','addDebuff','actorId','isEnemy','VisuMZ_2_BattleSystemBTB','drawItemStatusBoostPointsDefault','Game_Battler_addState','convertBoostEqualEscape','Window_ActorCommand_addGuardCommand','addUnboostCommand'];var _0x119c=function(_0x3bd403,_0x57501a){_0x3bd403=_0x3bd403-0x1df;var _0x5541eb=_0x5541[_0x3bd403];return _0x5541eb;};var _0xb5f0d6=_0x119c;(function(_0x59c8fa,_0x3f5972){var _0x1c5cde=_0x119c;while(!![]){try{var _0x25be8c=parseInt(_0x1c5cde(0x2f1))+-parseInt(_0x1c5cde(0x280))*parseInt(_0x1c5cde(0x252))+parseInt(_0x1c5cde(0x30d))*parseInt(_0x1c5cde(0x2d2))+-parseInt(_0x1c5cde(0x26c))+parseInt(_0x1c5cde(0x1e3))*-parseInt(_0x1c5cde(0x22c))+parseInt(_0x1c5cde(0x1f3))+parseInt(_0x1c5cde(0x2f2));if(_0x25be8c===_0x3f5972)break;else _0x59c8fa['push'](_0x59c8fa['shift']());}catch(_0x460a54){_0x59c8fa['push'](_0x59c8fa['shift']());}}}(_0x5541,0xa0e10));var label=_0xb5f0d6(0x1e2),tier=tier||0x0,dependencies=['VisuMZ_0_CoreEngine','VisuMZ_1_BattleCore',_0xb5f0d6(0x25d)],pluginData=$plugins[_0xb5f0d6(0x214)](function(_0x40b940){var _0x4f4ea1=_0xb5f0d6;return _0x40b940[_0x4f4ea1(0x2e1)]&&_0x40b940[_0x4f4ea1(0x307)][_0x4f4ea1(0x277)]('['+label+']');})[0x0];VisuMZ[label][_0xb5f0d6(0x2d7)]=VisuMZ[label][_0xb5f0d6(0x2d7)]||{},VisuMZ['ConvertParams']=function(_0xce4d64,_0xc076e5){var _0xb06bd5=_0xb5f0d6;for(const _0xe29f8e in _0xc076e5){if(_0xe29f8e[_0xb06bd5(0x273)](/(.*):(.*)/i)){const _0x2f3c39=String(RegExp['$1']),_0xeba712=String(RegExp['$2'])[_0xb06bd5(0x219)]()['trim']();let _0x4d09e5,_0x3e02d1,_0x2f76cd;switch(_0xeba712){case'NUM':_0x4d09e5=_0xc076e5[_0xe29f8e]!==''?Number(_0xc076e5[_0xe29f8e]):0x0;break;case _0xb06bd5(0x268):_0x3e02d1=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):[],_0x4d09e5=_0x3e02d1['map'](_0x3fa9b3=>Number(_0x3fa9b3));break;case _0xb06bd5(0x27f):_0x4d09e5=_0xc076e5[_0xe29f8e]!==''?eval(_0xc076e5[_0xe29f8e]):null;break;case _0xb06bd5(0x2c1):_0x3e02d1=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):[],_0x4d09e5=_0x3e02d1[_0xb06bd5(0x24f)](_0x29ae52=>eval(_0x29ae52));break;case _0xb06bd5(0x258):_0x4d09e5=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):'';break;case'ARRAYJSON':_0x3e02d1=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):[],_0x4d09e5=_0x3e02d1[_0xb06bd5(0x24f)](_0xe549ba=>JSON['parse'](_0xe549ba));break;case _0xb06bd5(0x313):_0x4d09e5=_0xc076e5[_0xe29f8e]!==''?new Function(JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e])):new Function('return\x200');break;case _0xb06bd5(0x23a):_0x3e02d1=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):[],_0x4d09e5=_0x3e02d1[_0xb06bd5(0x24f)](_0x51a348=>new Function(JSON['parse'](_0x51a348)));break;case'STR':_0x4d09e5=_0xc076e5[_0xe29f8e]!==''?String(_0xc076e5[_0xe29f8e]):'';break;case'ARRAYSTR':_0x3e02d1=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):[],_0x4d09e5=_0x3e02d1[_0xb06bd5(0x24f)](_0x5b9f52=>String(_0x5b9f52));break;case _0xb06bd5(0x29e):_0x2f76cd=_0xc076e5[_0xe29f8e]!==''?JSON[_0xb06bd5(0x24b)](_0xc076e5[_0xe29f8e]):{},_0x4d09e5=VisuMZ[_0xb06bd5(0x215)]({},_0x2f76cd);break;case _0xb06bd5(0x230):_0x3e02d1=_0xc076e5[_0xe29f8e]!==''?JSON['parse'](_0xc076e5[_0xe29f8e]):[],_0x4d09e5=_0x3e02d1['map'](_0xc9eab7=>VisuMZ[_0xb06bd5(0x215)]({},JSON[_0xb06bd5(0x24b)](_0xc9eab7)));break;default:continue;}_0xce4d64[_0x2f3c39]=_0x4d09e5;}}return _0xce4d64;},(_0x12124d=>{var _0x4e9e66=_0xb5f0d6;const _0x465ee9=_0x12124d['name'];for(const _0x3c842a of dependencies){if(!Imported[_0x3c842a]){alert('%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.'[_0x4e9e66(0x292)](_0x465ee9,_0x3c842a)),SceneManager[_0x4e9e66(0x271)]();break;}}const _0x5c8ce7=_0x12124d['description'];if(_0x5c8ce7[_0x4e9e66(0x273)](/\[Version[ ](.*?)\]/i)){const _0x12b577=Number(RegExp['$1']);_0x12b577!==VisuMZ[label][_0x4e9e66(0x28a)]&&(alert('%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.'[_0x4e9e66(0x292)](_0x465ee9,_0x12b577)),SceneManager[_0x4e9e66(0x271)]());}if(_0x5c8ce7[_0x4e9e66(0x273)](/\[Tier[ ](\d+)\]/i)){const _0x509a63=Number(RegExp['$1']);_0x509a63<tier?(alert('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0x4e9e66(0x292)](_0x465ee9,_0x509a63,tier)),SceneManager[_0x4e9e66(0x271)]()):tier=Math[_0x4e9e66(0x1e0)](_0x509a63,tier);}VisuMZ[_0x4e9e66(0x215)](VisuMZ[label][_0x4e9e66(0x2d7)],_0x12124d[_0x4e9e66(0x27e)]);})(pluginData),VisuMZ['BoostAction'][_0xb5f0d6(0x2f6)]={'BoostDamage':/<(?:BP|BOOST) (?:DMG|DAMAGE)>/i,'BoostTurns':/<(?:BP|BOOST) (?:TURN|TURNS)>/i,'BoostRepeat':/<(?:BP|BOOST) (?:REPEAT|REPEATS|HIT|HITS)>/i,'BoostAnalyze':/<(?:BP|BOOST) (?:ANALYZE|ANALYSIS)>/i,'TargetBoostPoints':/<TARGET (?:BP|BOOST POINT|BOOST POINTS): ([\+\-]\d+)>/i,'UserBoostPoints':/<USER (?:BP|BOOST POINT|BOOST POINTS): ([\+\-]\d+)>/i,'BoostGainPoints':/<(?:BP|BOOST) (?:BP|BOOST POINT|BOOST POINTS|POINT|POINTS|EFFECT|EFFECTS) (?:EFFECT|GAIN|LOSS)>/i,'Require':{'Amount':/<REQUIRE (\d+) (?:BP|BOOST POINT|BOOST POINTS)>/i,'GreaterEqual':/<REQUIRE >= (\d+) (?:BP|BOOST POINT|BOOST POINTS)>/i,'Greater':/<REQUIRE > (\d+) (?:BP|BOOST POINT|BOOST POINTS)>/i,'Equal':/<REQUIRE = (\d+) (?:BP|BOOST POINT|BOOST POINTS)>/i,'Less':/<REQUIRE < (\d+) (?:BP|BOOST POINT|BOOST POINTS)>/i,'LessEqual':/<REQUIRE <= (\d+) (?:BP|BOOST POINT|BOOST POINTS)>/i},'BoostSealed':/<(?:BP|BOOST) (?:SEAL|SEALED)>/i,'BoostBattleStartRate':/<(?:BP|BOOST|BOOST POINT|BOOST POINTS) BATTLE START: (\d+)([%％])>/i,'BoostBattleStartFlat':/<(?:BP|BOOST|BOOST POINT|BOOST POINTS) BATTLE START: ([\+\-]\d+)>/i,'BoostPointsRegenRate':/<(?:BP|BOOST|BOOST POINT|BOOST POINTS) REGEN: (\d+)([%％])>/i,'BoostPointsRegenFlat':/<(?:BP|BOOST|BOOST POINT|BOOST POINTS) REGEN: ([\+\-]\d+)>/i,'EnemyBoostSkillID':/<BOOST SKILL (\d+):[ ](.*)>/i,'EnemyBoostSkillName':/<BOOST (.*):[ ](.*)>/i},ImageManager[_0xb5f0d6(0x2e4)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x243)],ImageManager['unboostIcon']=VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)]['UI']['EmptyIcon'],ImageManager[_0xb5f0d6(0x202)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x301)],ImageManager[_0xb5f0d6(0x224)]=function(){var _0x2cefdd=_0xb5f0d6;if(!this[_0x2cefdd(0x2e0)]){this[_0x2cefdd(0x2e0)]=new Bitmap();const _0x28730f=ImageManager[_0x2cefdd(0x20b)](_0x2cefdd(0x203));_0x28730f[_0x2cefdd(0x2b8)](this['boostTransferBitmap'][_0x2cefdd(0x225)](this,_0x28730f));}return this['_boostIconSheet'];},ImageManager[_0xb5f0d6(0x276)]=function(_0x2c9f06){var _0x17ab3e=_0xb5f0d6;this[_0x17ab3e(0x2e0)][_0x17ab3e(0x29c)](_0x2c9f06[_0x17ab3e(0x2ef)],_0x2c9f06[_0x17ab3e(0x21b)]),this[_0x17ab3e(0x2e0)][_0x17ab3e(0x261)](_0x2c9f06,0x0,0x0,_0x2c9f06[_0x17ab3e(0x2ef)],_0x2c9f06[_0x17ab3e(0x21b)],0x0,0x0),this[_0x17ab3e(0x2e0)][_0x17ab3e(0x2ea)]=ImageManager[_0x17ab3e(0x202)];},TextManager[_0xb5f0d6(0x2a9)]=VisuMZ[_0xb5f0d6(0x1e2)]['Settings']['UI']['BoostCmd'],TextManager[_0xb5f0d6(0x1f1)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI']['UnboostCmd'],VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2a6)]=BattleManager['setup'],BattleManager[_0xb5f0d6(0x2bd)]=function(_0x22bdff,_0x4d07ea,_0x2f61f4){var _0x49cf9f=_0xb5f0d6;VisuMZ[_0x49cf9f(0x1e2)][_0x49cf9f(0x2a6)][_0x49cf9f(0x22f)](this,_0x22bdff,_0x4d07ea,_0x2f61f4),$gameParty[_0x49cf9f(0x2bc)](),$gameTroop[_0x49cf9f(0x2bc)]();},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2df)]=BattleManager[_0xb5f0d6(0x236)],BattleManager[_0xb5f0d6(0x236)]=function(){var _0x3c6357=_0xb5f0d6;this[_0x3c6357(0x2aa)](),VisuMZ[_0x3c6357(0x1e2)]['BattleManager_processTurn'][_0x3c6357(0x22f)](this);},BattleManager['processEnemyUseBoost']=function(){var _0x54a34d=_0xb5f0d6,_0x364ea2=this[_0x54a34d(0x200)],_0x5594ad=_0x364ea2[_0x54a34d(0x26e)]();!!_0x364ea2&&_0x364ea2[_0x54a34d(0x2ae)]()&&!!_0x5594ad&&_0x5594ad[_0x54a34d(0x1eb)]()&&_0x364ea2[_0x54a34d(0x2a8)]()>0x0&&!_0x364ea2[_0x54a34d(0x2d3)]()&&_0x364ea2[_0x54a34d(0x254)](_0x5594ad[_0x54a34d(0x2c5)]());},BattleManager['allowBoostAction']=function(){var _0xc4a9ee=_0xb5f0d6;if(Imported[_0xc4a9ee(0x2af)]&&this['isBTB']())return![];return!![];},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x298)]=Game_Action['prototype'][_0xb5f0d6(0x23d)],Game_Action['prototype']['numRepeats']=function(){var _0x24c205=_0xb5f0d6,_0x5b68a8=VisuMZ[_0x24c205(0x1e2)][_0x24c205(0x298)]['call'](this);_0x5b68a8=this['applyBoostPointRepeats'](_0x5b68a8);return Math[_0x24c205(0x1f2)](_0x5b68a8);;},Game_Action[_0xb5f0d6(0x263)]['applyBoostPointRepeats']=function(_0x1c2a41){var _0x1a972e=_0xb5f0d6;const _0x40adf6=VisuMZ['BoostAction'][_0x1a972e(0x2f6)];if(!!this[_0x1a972e(0x257)]()&&this[_0x1a972e(0x2c5)]()[_0x1a972e(0x2f4)]['match'](_0x40adf6['BoostRepeat'])){var _0x404feb=this[_0x1a972e(0x257)]()[_0x1a972e(0x272)](_0x1a972e(0x28b));_0x1c2a41=Math[_0x1a972e(0x1f2)](_0x1c2a41*_0x404feb),_0x1c2a41+=this[_0x1a972e(0x257)]()[_0x1a972e(0x270)](_0x1a972e(0x28b));}return _0x1c2a41;},VisuMZ['BoostAction']['Game_Action_applyGuard']=Game_Action['prototype'][_0xb5f0d6(0x2d5)],Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x2d5)]=function(_0xbde58b,_0x3f2f57){var _0x1245ba=_0xb5f0d6;return _0xbde58b=this[_0x1245ba(0x21a)](_0xbde58b),VisuMZ['BoostAction']['Game_Action_applyGuard']['call'](this,_0xbde58b,_0x3f2f57);},Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x21a)]=function(_0x5d4fac){var _0x83227b=_0xb5f0d6;const _0x306bd6=VisuMZ[_0x83227b(0x1e2)][_0x83227b(0x2f6)];if(!!this['subject']()&&this[_0x83227b(0x2c5)]()[_0x83227b(0x2f4)]['match'](_0x306bd6[_0x83227b(0x1e7)])){var _0x2bea8a=this[_0x83227b(0x257)]()[_0x83227b(0x272)](_0x83227b(0x28c));_0x5d4fac=Math[_0x83227b(0x1f2)](_0x5d4fac*_0x2bea8a),_0x5d4fac+=this[_0x83227b(0x257)]()[_0x83227b(0x270)]('Damage');}return _0x5d4fac;},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x27b)]=Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x238)],Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x238)]=function(_0xbc5ba0){var _0x43f32a=_0xb5f0d6;this[_0x43f32a(0x30f)](![]),VisuMZ['BoostAction'][_0x43f32a(0x27b)][_0x43f32a(0x22f)](this,_0xbc5ba0),this[_0x43f32a(0x30f)](!![]);},Game_Action[_0xb5f0d6(0x263)]['applyBoostPointTurns']=function(_0x571f5a){var _0xe6bed=_0xb5f0d6;const _0x22462a=VisuMZ[_0xe6bed(0x1e2)][_0xe6bed(0x2f6)];if(!!this[_0xe6bed(0x257)]()&&this[_0xe6bed(0x2c5)]()[_0xe6bed(0x2f4)][_0xe6bed(0x273)](_0x22462a[_0xe6bed(0x2c0)])){var _0x25ff67=this['subject']()[_0xe6bed(0x272)](_0xe6bed(0x2d6));$gameTemp[_0xe6bed(0x248)]=_0x25ff67,$gameTemp[_0xe6bed(0x1f7)]=this[_0xe6bed(0x257)]()[_0xe6bed(0x270)]('Turn');}_0x571f5a&&($gameTemp[_0xe6bed(0x248)]=undefined,$gameTemp['_bpTurnFlat']=undefined);},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x282)]=Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x25a)],Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x25a)]=function(_0x152640){var _0x1785fc=_0xb5f0d6;VisuMZ[_0x1785fc(0x1e2)][_0x1785fc(0x282)][_0x1785fc(0x22f)](this,_0x152640),this[_0x1785fc(0x262)](_0x152640);},Game_Action[_0xb5f0d6(0x263)][_0xb5f0d6(0x262)]=function(_0x18ae17){var _0x2e48bf=_0xb5f0d6;const _0x41bffa=VisuMZ[_0x2e48bf(0x1e2)][_0x2e48bf(0x2f6)];if(!!_0x18ae17&&this['item']()[_0x2e48bf(0x2f4)][_0x2e48bf(0x273)](_0x41bffa['TargetBoostPoints'])){var _0x17da1c=parseInt(RegExp['$1']);this['item']()[_0x2e48bf(0x2f4)][_0x2e48bf(0x273)](_0x41bffa[_0x2e48bf(0x286)])&&(_0x17da1c=Math['round'](this[_0x2e48bf(0x257)]()[_0x2e48bf(0x272)]('BP\x20Effect')*_0x17da1c),_0x17da1c+=this[_0x2e48bf(0x257)]()[_0x2e48bf(0x270)](_0x2e48bf(0x24e))),_0x18ae17[_0x2e48bf(0x22a)](_0x17da1c);}if(!!this[_0x2e48bf(0x257)]()&&this[_0x2e48bf(0x2c5)]()[_0x2e48bf(0x2f4)][_0x2e48bf(0x273)](_0x41bffa[_0x2e48bf(0x2c9)])){var _0x17da1c=parseInt(RegExp['$1']);this[_0x2e48bf(0x2c5)]()[_0x2e48bf(0x2f4)][_0x2e48bf(0x273)](_0x41bffa['BoostGainPoints'])&&(_0x17da1c=Math[_0x2e48bf(0x1f2)](this[_0x2e48bf(0x257)]()[_0x2e48bf(0x272)]('BP\x20Effect')*_0x17da1c),_0x17da1c+=this[_0x2e48bf(0x257)]()[_0x2e48bf(0x270)](_0x2e48bf(0x24e))),this[_0x2e48bf(0x257)]()[_0x2e48bf(0x22a)](_0x17da1c);}},Game_BattlerBase[_0xb5f0d6(0x2c7)]=VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x1f5)],Game_BattlerBase['BOOST_POINTS_MAX_TOUSE']=VisuMZ[_0xb5f0d6(0x1e2)]['Settings'][_0xb5f0d6(0x255)][_0xb5f0d6(0x1f4)],Game_BattlerBase[_0xb5f0d6(0x245)]=VisuMZ[_0xb5f0d6(0x1e2)]['Settings']['Mechanics'][_0xb5f0d6(0x278)],Game_BattlerBase['BOOST_POINTS_DEATH_REMOVE']=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x1e4)],Game_BattlerBase[_0xb5f0d6(0x287)]=VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x213)],Game_BattlerBase[_0xb5f0d6(0x217)]=VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)]['Regen'],Game_BattlerBase[_0xb5f0d6(0x20d)]=VisuMZ[_0xb5f0d6(0x1e2)]['Settings']['Mechanics']['StartBattle'],VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x1e9)]=Game_BattlerBase['prototype']['initialize'],Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x300)]=function(){var _0x68af09=_0xb5f0d6;VisuMZ[_0x68af09(0x1e2)][_0x68af09(0x1e9)][_0x68af09(0x22f)](this),this['initBoostAction']();},Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x2b5)]=function(){var _0x23a48b=_0xb5f0d6;this[_0x23a48b(0x1e6)]=this[_0x23a48b(0x1e6)]||0x0,this[_0x23a48b(0x23e)]=this['_toUseBoostPoints']||0x0,this[_0x23a48b(0x1ea)]=this[_0x23a48b(0x1ea)]||0x0;},Game_BattlerBase['prototype']['storedBoostPoints']=function(){var _0x55f3ca=_0xb5f0d6;return this['_storedBoostPoints']===undefined&&this[_0x55f3ca(0x2b5)](),this['_storedBoostPoints'];},Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x2c3)]=function(_0x2e3efd){var _0x44e471=_0xb5f0d6;this[_0x44e471(0x1e6)]===undefined&&this[_0x44e471(0x2b5)](),_0x2e3efd=Math[_0x44e471(0x1f2)](_0x2e3efd),this[_0x44e471(0x1e6)]=_0x2e3efd['clamp'](0x0,Game_BattlerBase[_0x44e471(0x2c7)]),this['refresh']();},Game_BattlerBase[_0xb5f0d6(0x263)]['toUseBoostPoints']=function(){var _0x3627b1=_0xb5f0d6;return this[_0x3627b1(0x23e)]===undefined&&this[_0x3627b1(0x2b5)](),this['_toUseBoostPoints'];},Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x241)]=function(_0x25f795){var _0x3b5e47=_0xb5f0d6;this['_toUseBoostPoints']===undefined&&this[_0x3b5e47(0x2b5)](),_0x25f795=Math['round'](_0x25f795),this['_toUseBoostPoints']=_0x25f795['clamp'](0x0,Game_BattlerBase[_0x3b5e47(0x23c)]),this['refresh']();},Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x1ec)]=function(){var _0x990387=_0xb5f0d6;if(!Game_BattlerBase['BOOST_POINTS_DEATH_REGEN']&&(this[_0x990387(0x2cb)]()||this[_0x990387(0x2e7)]()))return 0x0;else{var _0x39a4c1=Game_BattlerBase[_0x990387(0x217)];return _0x39a4c1=this['bpRegenMultipliers'](_0x39a4c1),_0x39a4c1=this['bpRegenAdded'](_0x39a4c1),_0x39a4c1;}},Game_BattlerBase[_0xb5f0d6(0x263)]['isBoostSealed']=function(){var _0xc1ad83=_0xb5f0d6;const _0x27604f=this[_0xc1ad83(0x20a)](),_0x5e0309=VisuMZ[_0xc1ad83(0x1e2)]['RegExp'];return _0x27604f[_0xc1ad83(0x240)](_0x129d5e=>_0x129d5e&&_0x129d5e['note'][_0xc1ad83(0x273)](_0x5e0309[_0xc1ad83(0x30e)]));},VisuMZ[_0xb5f0d6(0x1e2)]['Game_BattlerBase_resetStateCounts']=Game_BattlerBase['prototype'][_0xb5f0d6(0x289)],Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x289)]=function(_0x905a64){var _0x3445fa=_0xb5f0d6,_0xc527=this[_0x3445fa(0x29f)][_0x905a64]||0x0;VisuMZ['BoostAction'][_0x3445fa(0x312)]['call'](this,_0x905a64);if(!!$gameTemp['_bpTurnRate']){$gameTemp[_0x3445fa(0x1f7)]=$gameTemp[_0x3445fa(0x1f7)]||0x0;var _0x393823=$dataStates[_0x905a64],_0x4b8cf7=Math[_0x3445fa(0x1f2)](_0x393823['maxTurns']*$gameTemp[_0x3445fa(0x248)])+$gameTemp['_bpTurnFlat'],_0x53f514=Math[_0x3445fa(0x1f2)](_0x393823[_0x3445fa(0x1ef)]*$gameTemp[_0x3445fa(0x248)])+$gameTemp[_0x3445fa(0x1f7)],_0x1f057e=0x1+Math[_0x3445fa(0x1e0)](_0x4b8cf7-_0x53f514,0x0);const _0xf67706=this['getStateReapplyRulings'](_0x393823)['toLowerCase']()[_0x3445fa(0x275)]();switch(_0xf67706){case'reset':this[_0x3445fa(0x29f)][_0x905a64]=_0x53f514+Math['randomInt'](_0x1f057e);break;case'greater':const _0x1a543a=this[_0x3445fa(0x29f)][_0x905a64],_0x48b5a4=_0x53f514+Math[_0x3445fa(0x20f)](_0x1f057e);this['_stateTurns'][_0x905a64]=Math[_0x3445fa(0x1e0)](_0x1a543a,_0x48b5a4);break;case'add':this['_stateTurns'][_0x905a64]=_0x53f514+Math[_0x3445fa(0x20f)](_0x1f057e)+_0xc527;break;}}},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x25e)]=Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x22b)],Game_BattlerBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x22b)]=function(_0x46fad7){var _0x1b77a5=_0xb5f0d6;return VisuMZ[_0x1b77a5(0x1e2)]['Game_BattlerBase_meetsUsableItemConditions'][_0x1b77a5(0x22f)](this,_0x46fad7)?this[_0x1b77a5(0x21d)](_0x46fad7):![];},Game_BattlerBase[_0xb5f0d6(0x263)]['meetstoUseBoostPointsRequirement']=function(_0x420ab6){var _0x62c1e=_0xb5f0d6;const _0x48f5ec=VisuMZ[_0x62c1e(0x1e2)][_0x62c1e(0x2f6)];var _0x5e33a7=_0x420ab6[_0x62c1e(0x2f4)];if(_0x5e33a7['match'](_0x48f5ec['Require'][_0x62c1e(0x30a)])||_0x5e33a7['match'](_0x48f5ec[_0x62c1e(0x2f8)][_0x62c1e(0x212)])){var _0xc15578=parseInt(RegExp['$1']);return this[_0x62c1e(0x2b7)]()?this[_0x62c1e(0x20e)]()>=_0xc15578:this[_0x62c1e(0x2a8)]()>=_0xc15578;}else{if(_0x420ab6['note'][_0x62c1e(0x273)](_0x48f5ec[_0x62c1e(0x2f8)][_0x62c1e(0x212)])){var _0xc15578=parseInt(RegExp['$1']);return this[_0x62c1e(0x2b7)]()?this[_0x62c1e(0x20e)]()>_0xc15578:this[_0x62c1e(0x2a8)]()>_0xc15578;}else{if(_0x420ab6[_0x62c1e(0x2f4)][_0x62c1e(0x273)](_0x48f5ec[_0x62c1e(0x2f8)][_0x62c1e(0x295)])){var _0xc15578=parseInt(RegExp['$1']);return this[_0x62c1e(0x2b7)]()?this[_0x62c1e(0x20e)]()===_0xc15578:this['storedBoostPoints']()===_0xc15578;}else{if(_0x420ab6[_0x62c1e(0x2f4)][_0x62c1e(0x273)](_0x48f5ec['Require'][_0x62c1e(0x274)])){var _0xc15578=parseInt(RegExp['$1']);return this['isActor']()?this[_0x62c1e(0x20e)]()<_0xc15578:this[_0x62c1e(0x2a8)]()<_0xc15578;}else{if(_0x420ab6[_0x62c1e(0x2f4)][_0x62c1e(0x273)](_0x48f5ec[_0x62c1e(0x2f8)]['LessEqual'])){var _0xc15578=parseInt(RegExp['$1']);return this[_0x62c1e(0x2b7)]()?this[_0x62c1e(0x20e)]()<=_0xc15578:this[_0x62c1e(0x2a8)]()<=_0xc15578;}else return!![];}}}}},Game_Battler['BOOST_POINTS_MULTIPLIERS']={'Damage':VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x2ff)],'Turn':VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)]['TurnMultiply'],'Repeat':VisuMZ[_0xb5f0d6(0x1e2)]['Settings']['Mechanics'][_0xb5f0d6(0x221)],'BpEffect':VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)]['EffectMultiply'],'Analyze':VisuMZ['BoostAction']['Settings'][_0xb5f0d6(0x255)][_0xb5f0d6(0x244)]},Game_Battler[_0xb5f0d6(0x2a7)]={'Damage':VisuMZ['BoostAction']['Settings'][_0xb5f0d6(0x255)][_0xb5f0d6(0x21c)],'Turn':VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x2fa)],'Repeat':VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x222)],'BpEffect':VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x220)],'Analyze':VisuMZ[_0xb5f0d6(0x1e2)]['Settings']['Mechanics'][_0xb5f0d6(0x2eb)]},Game_Battler[_0xb5f0d6(0x2de)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x2e2)],Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x22a)]=function(_0x305268){var _0x779c3=_0xb5f0d6;this[_0x779c3(0x2c3)](this['storedBoostPoints']()+_0x305268);},Game_Battler[_0xb5f0d6(0x263)]['gainToUseBoostPoints']=function(_0x31bdd9){var _0x3bf492=_0xb5f0d6;this[_0x3bf492(0x241)](this[_0x3bf492(0x20e)]()+_0x31bdd9);},Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x272)]=function(_0x20141e){var _0x2cf76f=_0xb5f0d6;const _0xd76fb0=Game_Battler[_0x2cf76f(0x2dc)];if(_0x20141e[_0x2cf76f(0x273)](/Damage/i))var _0x36383e=_0xd76fb0[_0x2cf76f(0x28c)];else{if(_0x20141e['match'](/Turn/i))var _0x36383e=_0xd76fb0[_0x2cf76f(0x2d6)];else{if(_0x20141e[_0x2cf76f(0x273)](/Repeat/i))var _0x36383e=_0xd76fb0['Repeat'];else{if(_0x20141e[_0x2cf76f(0x273)](/BP Effect/i))var _0x36383e=_0xd76fb0[_0x2cf76f(0x242)];else{if(_0x20141e[_0x2cf76f(0x273)](/Analyze/i))var _0x36383e=_0xd76fb0[_0x2cf76f(0x302)];else return this[_0x2cf76f(0x20e)]();}}}}var _0x3c66fd=this['toUseBoostPoints']();return _0x36383e[_0x3c66fd]||_0x36383e[0x0];},Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x270)]=function(_0x401f65){var _0x53f6d2=_0xb5f0d6;const _0x3dc011=Game_Battler['BOOST_POINTS_ADDITION'];if(_0x401f65[_0x53f6d2(0x273)](/Damage/i))var _0x3e4f8e=_0x3dc011['Damage'];else{if(_0x401f65[_0x53f6d2(0x273)](/Turn/i))var _0x3e4f8e=_0x3dc011[_0x53f6d2(0x2d6)];else{if(_0x401f65[_0x53f6d2(0x273)](/Repeat/i))var _0x3e4f8e=_0x3dc011[_0x53f6d2(0x28b)];else{if(_0x401f65[_0x53f6d2(0x273)](/BP Effect/i))var _0x3e4f8e=_0x3dc011['BpEffect'];else{if(_0x401f65['match'](/Analyze/i))var _0x3e4f8e=_0x3dc011['Analyze'];else return this[_0x53f6d2(0x20e)]();}}}}var _0x18ad7a=this[_0x53f6d2(0x20e)]();return parseInt(_0x3e4f8e[_0x18ad7a]||_0x3e4f8e[0x0]);},Game_Battler[_0xb5f0d6(0x263)]['setupBattleBoostPoints']=function(){var _0x4f44d5=_0xb5f0d6,_0x16ec41=Game_BattlerBase[_0x4f44d5(0x20d)];_0x16ec41=this[_0x4f44d5(0x1f6)](_0x16ec41),_0x16ec41=this[_0x4f44d5(0x2b9)](_0x16ec41),_0x16ec41=Math[_0x4f44d5(0x1f2)](_0x16ec41),this['setStoredBoostPoints'](_0x16ec41);},Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x1f6)]=function(_0x552446){var _0x4d09d7=_0xb5f0d6;const _0x2c43d4=this['traitObjects'](),_0x20c816=VisuMZ['BoostAction'][_0x4d09d7(0x2f6)];for(const _0x179a3e of _0x2c43d4){if(!_0x179a3e)continue;_0x179a3e['note'][_0x4d09d7(0x273)](_0x20c816[_0x4d09d7(0x2a5)])&&(_0x552446*=Number(RegExp['$1'])*0.01);}return _0x552446;},Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x2b9)]=function(_0x9e1f76){var _0x17745f=_0xb5f0d6;const _0x42a4e=this[_0x17745f(0x20a)](),_0x281b87=VisuMZ[_0x17745f(0x1e2)][_0x17745f(0x2f6)];for(const _0x4c95d9 of _0x42a4e){if(!_0x4c95d9)continue;_0x4c95d9[_0x17745f(0x2f4)][_0x17745f(0x273)](_0x281b87['BoostBattleStartFlat'])&&(_0x9e1f76+=Number(RegExp['$1']));}return _0x9e1f76;},Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x27a)]=function(){var _0x8ddb60=_0xb5f0d6,_0x26586a=this['toUseBoostPoints']()[_0x8ddb60(0x28e)](0x0,Game_BattlerBase['BOOST_POINTS_MAX_TOUSE']);const _0x39a59e=Game_Battler[_0x8ddb60(0x2de)];var _0x35f63c=Number(_0x39a59e[_0x26586a]||_0x39a59e[0x0]);_0x35f63c>0x0&&$gameTemp[_0x8ddb60(0x2d8)]([this],_0x35f63c,![],![]);},Game_Battler['prototype'][_0xb5f0d6(0x232)]=function(){var _0x369dc5=_0xb5f0d6;if(this['isBoostSealed']())return![];return this[_0x369dc5(0x20e)]()<Game_BattlerBase[_0x369dc5(0x23c)]&&this[_0x369dc5(0x2a8)]()>0x0;},Game_Battler['prototype'][_0xb5f0d6(0x1e8)]=function(){var _0x41c378=_0xb5f0d6;return this[_0x41c378(0x20e)]()>0x0;},VisuMZ['BoostAction'][_0xb5f0d6(0x20c)]=Game_Battler['prototype'][_0xb5f0d6(0x25c)],Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x25c)]=function(){var _0x1dd853=_0xb5f0d6;VisuMZ['BoostAction']['Game_Battler_removeBattleStates'][_0x1dd853(0x22f)](this),this[_0x1dd853(0x1e6)]=0x0,this[_0x1dd853(0x23e)]=0x0;},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x305)]=Game_Battler['prototype'][_0xb5f0d6(0x21e)],Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x21e)]=function(){var _0x3646a4=_0xb5f0d6;VisuMZ['BoostAction'][_0x3646a4(0x305)][_0x3646a4(0x22f)](this),this[_0x3646a4(0x1ff)]();},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2fe)]=Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x205)],Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x205)]=function(){var _0x1b8819=_0xb5f0d6;VisuMZ[_0x1b8819(0x1e2)][_0x1b8819(0x2fe)][_0x1b8819(0x22f)](this),Game_BattlerBase[_0x1b8819(0x245)]&&this['isDead']()&&this[_0x1b8819(0x1ff)]();},Game_Battler['prototype']['regenerateBoostPoints']=function(){var _0x18a28b=_0xb5f0d6;(Game_BattlerBase[_0x18a28b(0x287)]||this[_0x18a28b(0x1ea)]<=0x0)&&this[_0x18a28b(0x22a)](this[_0x18a28b(0x1ec)]()),this[_0x18a28b(0x1ea)]=0x0;},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x308)]=Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x1ed)],Game_Battler['prototype'][_0xb5f0d6(0x1ed)]=function(){var _0xd051d6=_0xb5f0d6;VisuMZ[_0xd051d6(0x1e2)][_0xd051d6(0x308)][_0xd051d6(0x22f)](this),this[_0xd051d6(0x1ea)]+=this[_0xd051d6(0x20e)](),this[_0xd051d6(0x241)](0x0);},Game_Battler['prototype']['bpRegenMultipliers']=function(_0x49bed3){var _0x410ec7=_0xb5f0d6;const _0xd1229b=this[_0x410ec7(0x20a)](),_0x39ee3b=VisuMZ[_0x410ec7(0x1e2)][_0x410ec7(0x2f6)];for(const _0x69516f of _0xd1229b){if(!_0x69516f)continue;_0x69516f[_0x410ec7(0x2f4)][_0x410ec7(0x273)](_0x39ee3b[_0x410ec7(0x1fd)])&&(_0x49bed3*=Number(RegExp['$1'])*0.01);}return _0x49bed3;},Game_Battler['prototype'][_0xb5f0d6(0x227)]=function(_0x293359){var _0x44e5e9=_0xb5f0d6;const _0x50a159=this['traitObjects'](),_0xd2f7af=VisuMZ[_0x44e5e9(0x1e2)][_0x44e5e9(0x2f6)];for(const _0x2f3029 of _0x50a159){if(!_0x2f3029)continue;_0x2f3029['note'][_0x44e5e9(0x273)](_0xd2f7af[_0x44e5e9(0x306)])&&(_0x293359+=Number(RegExp['$1']));}return _0x293359;},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2b1)]=Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x25f)],Game_Battler[_0xb5f0d6(0x263)]['addState']=function(_0x50347a){var _0x54df68=_0xb5f0d6,_0x586ddd=this[_0x54df68(0x2cb)]();VisuMZ[_0x54df68(0x1e2)][_0x54df68(0x2b1)][_0x54df68(0x22f)](this,_0x50347a),Game_BattlerBase[_0x54df68(0x2d1)]&&!_0x586ddd&&this[_0x54df68(0x2cb)]()&&this['setStoredBoostPoints'](0x0);},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x29d)]=Game_Battler['prototype']['addBuff'],Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x2fd)]=function(_0x701e8d,_0x9fd3c5){var _0x9487f=_0xb5f0d6;!!$gameTemp['_bpTurnRate']&&($gameTemp['_bpTurnFlat']=$gameTemp[_0x9487f(0x1f7)]||0x0,_0x9fd3c5=Math[_0x9487f(0x1f2)]($gameTemp[_0x9487f(0x248)]*_0x9fd3c5)+$gameTemp[_0x9487f(0x1f7)]),VisuMZ[_0x9487f(0x1e2)][_0x9487f(0x29d)][_0x9487f(0x22f)](this,_0x701e8d,_0x9fd3c5);},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x294)]=Game_Battler['prototype'][_0xb5f0d6(0x2ac)],Game_Battler[_0xb5f0d6(0x263)][_0xb5f0d6(0x2ac)]=function(_0x24fb8e,_0x1766ea){var _0x22126d=_0xb5f0d6;!!$gameTemp[_0x22126d(0x248)]&&($gameTemp[_0x22126d(0x1f7)]=$gameTemp['_bpTurnFlat']||0x0,_0x1766ea=Math[_0x22126d(0x1f2)]($gameTemp[_0x22126d(0x248)]*_0x1766ea)+$gameTemp[_0x22126d(0x1f7)]),VisuMZ[_0x22126d(0x1e2)]['Game_Battler_addDebuff'][_0x22126d(0x22f)](this,_0x24fb8e,_0x1766ea);},Game_Enemy[_0xb5f0d6(0x296)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)][_0xb5f0d6(0x255)][_0xb5f0d6(0x27c)],VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x290)]=Game_Enemy[_0xb5f0d6(0x263)]['setup'],Game_Enemy[_0xb5f0d6(0x263)][_0xb5f0d6(0x2bd)]=function(_0x444ca3,_0x4e4a24,_0x5ca137){var _0xf4587d=_0xb5f0d6;VisuMZ['BoostAction'][_0xf4587d(0x290)]['call'](this,_0x444ca3,_0x4e4a24,_0x5ca137),this[_0xf4587d(0x28f)]();},Game_Enemy[_0xb5f0d6(0x263)][_0xb5f0d6(0x28f)]=function(){var _0x2f9ecb=_0xb5f0d6;const _0x5de1b8=VisuMZ[_0x2f9ecb(0x1e2)]['RegExp'];if(this[_0x2f9ecb(0x2fc)]()[_0x2f9ecb(0x210)]===undefined){this[_0x2f9ecb(0x2fc)]()[_0x2f9ecb(0x210)]={};var _0x3eff51=this[_0x2f9ecb(0x2fc)]()[_0x2f9ecb(0x2f4)]['split'](/[\r\n]+/);for(var _0x19f341=0x0;_0x19f341<_0x3eff51[_0x2f9ecb(0x211)];_0x19f341++){var _0x10b20e=_0x3eff51[_0x19f341];if(_0x10b20e[_0x2f9ecb(0x273)](_0x5de1b8[_0x2f9ecb(0x260)])){var _0x3e285c=_0x2f9ecb(0x1fb)+parseInt(RegExp['$1']),_0xd3fd94=String(RegExp['$2'])['toLowerCase']();this[_0x2f9ecb(0x2fc)]()['_boostAI'][_0x3e285c]=_0xd3fd94;}else{if(_0x10b20e[_0x2f9ecb(0x273)](_0x5de1b8[_0x2f9ecb(0x24c)])){var _0x3fd877=String(RegExp['$1']),_0xd3fd94=String(RegExp['$2'])[_0x2f9ecb(0x2bf)]();this[_0x2f9ecb(0x2fc)]()[_0x2f9ecb(0x210)][_0x3fd877]=_0xd3fd94;}}}}},Game_Enemy['prototype']['processtoUseBoostPoints']=function(_0x104039){var _0x444417=_0xb5f0d6;this[_0x444417(0x28f)]();var _0x55c5b7=this[_0x444417(0x291)](_0x104039);_0x55c5b7>0x0&&(this[_0x444417(0x1ee)](_0x55c5b7),this[_0x444417(0x27a)]());},Game_Enemy[_0xb5f0d6(0x263)][_0xb5f0d6(0x291)]=function(_0x2cfac7){var _0x4a4408=_0xb5f0d6;if(this[_0x4a4408(0x2a8)]()<=0x0)return 0x0;var _0x777860=_0x2cfac7[_0x4a4408(0x2db)],_0x34b53a=_0x4a4408(0x1fb)+_0x2cfac7['id'],_0x20886f=0x0;if(this[_0x4a4408(0x2fc)]()[_0x4a4408(0x210)][_0x777860]||this[_0x4a4408(0x2fc)]()[_0x4a4408(0x210)][_0x34b53a]){var _0x52b4aa=this[_0x4a4408(0x2fc)]()[_0x4a4408(0x210)][_0x777860]||this['enemy']()[_0x4a4408(0x210)][_0x34b53a];if(_0x52b4aa[_0x4a4408(0x273)](/(?:ALL|FULL)/i))_0x20886f=this[_0x4a4408(0x2a8)]();else{if(_0x52b4aa[_0x4a4408(0x273)](/AT LEAST (\d+)/i)){var _0x2cf30f=parseInt(RegExp['$1']);this['storedBoostPoints']()>=_0x2cf30f&&(_0x20886f=this['storedBoostPoints']());}else{if(_0x52b4aa[_0x4a4408(0x273)](/AT MOST (\d+)/i)){var _0x2cf30f=parseInt(RegExp['$1']);this[_0x4a4408(0x2a8)]()<=_0x2cf30f&&(_0x20886f=this[_0x4a4408(0x2a8)]());}else{if(_0x52b4aa['match'](/EXACTLY (\d+)/i)){var _0x2cf30f=parseInt(RegExp['$1']);this[_0x4a4408(0x2a8)]()===_0x2cf30f&&(_0x20886f=_0x2cf30f);}}}}}return _0x20886f[_0x4a4408(0x28e)](0x0,Game_BattlerBase[_0x4a4408(0x23c)]);},Game_Enemy['prototype'][_0xb5f0d6(0x1ee)]=function(_0x2f06a8){var _0xb34016=_0xb5f0d6;_0x2f06a8=_0x2f06a8[_0xb34016(0x28e)](0x0,this['storedBoostPoints']()),_0x2f06a8=_0x2f06a8[_0xb34016(0x28e)](0x0,Game_BattlerBase['BOOST_POINTS_MAX_TOUSE']),this[_0xb34016(0x22a)](-_0x2f06a8),this[_0xb34016(0x22d)](_0x2f06a8);},Game_Enemy[_0xb5f0d6(0x263)][_0xb5f0d6(0x27a)]=function(){var _0x2fba1c=_0xb5f0d6,_0x5a4a02=0x0,_0x26b591=this[_0x2fba1c(0x20e)]()[_0x2fba1c(0x28e)](0x0,Game_BattlerBase['BOOST_POINTS_MAX_TOUSE']);const _0x4eeda0=Game_Battler[_0x2fba1c(0x2de)],_0x58fdd3=Game_Enemy[_0x2fba1c(0x296)],_0x237aaf=0x3e8/0x3c;for(var _0x24ee37=0x1;_0x24ee37<=_0x26b591;_0x24ee37++){var _0xfb3f2f=_0x4eeda0[_0x24ee37]||_0x4eeda0[0x0];if(_0xfb3f2f>0x0){let _0x20082f=_0x58fdd3*(_0x24ee37-0x1);setTimeout($gameTemp[_0x2fba1c(0x2d8)][_0x2fba1c(0x225)]($gameTemp,[this],_0xfb3f2f,![],![]),_0x20082f);}_0x5a4a02+=_0x58fdd3/_0x237aaf;}_0x5a4a02=Math[_0x2fba1c(0x21f)](_0x5a4a02),SceneManager[_0x2fba1c(0x30c)][_0x2fba1c(0x209)][_0x2fba1c(0x253)]=_0x5a4a02;},Game_Unit[_0xb5f0d6(0x263)][_0xb5f0d6(0x2bc)]=function(){var _0x495aa4=_0xb5f0d6,_0x411d04=this['_inBattle'];this[_0x495aa4(0x2f5)]=![];for(const _0x33ab2b of this['members']()){if(!_0x33ab2b)continue;_0x33ab2b['setupBattleBoostPoints']();}this['_inBattle']=_0x411d04;},VisuMZ['BoostAction']['Scene_Battle_createActorCommandWindow']=Scene_Battle[_0xb5f0d6(0x263)][_0xb5f0d6(0x2ee)],Scene_Battle['prototype'][_0xb5f0d6(0x2ee)]=function(){var _0x4ecc68=_0xb5f0d6;VisuMZ[_0x4ecc68(0x1e2)][_0x4ecc68(0x264)]['call'](this),this['_actorCommandWindow'][_0x4ecc68(0x247)](_0x4ecc68(0x235),this[_0x4ecc68(0x2be)][_0x4ecc68(0x225)](this)),this[_0x4ecc68(0x297)]['setHandler']('unboost',this['commandUnboost']['bind'](this));},Scene_Battle[_0xb5f0d6(0x263)][_0xb5f0d6(0x2be)]=function(_0x202ee4){var _0x2e9efc=_0xb5f0d6;BattleManager[_0x2e9efc(0x237)]()[_0x2e9efc(0x22a)](-0x1),BattleManager['actor']()[_0x2e9efc(0x22d)](0x1),BattleManager['actor']()[_0x2e9efc(0x27a)](),this[_0x2e9efc(0x310)][_0x2e9efc(0x256)](),!_0x202ee4&&this[_0x2e9efc(0x297)]['activate'](),this[_0x2e9efc(0x297)][_0x2e9efc(0x256)]();},Scene_Battle[_0xb5f0d6(0x263)]['commandUnboost']=function(_0x1b54c3){var _0x26b746=_0xb5f0d6;BattleManager[_0x26b746(0x237)]()['gainToUseBoostPoints'](-0x1),BattleManager[_0x26b746(0x237)]()[_0x26b746(0x22a)](0x1),BattleManager[_0x26b746(0x237)]()[_0x26b746(0x27a)](),this[_0x26b746(0x310)][_0x26b746(0x256)](),!_0x1b54c3&&this['_actorCommandWindow'][_0x26b746(0x2d0)](),this['_actorCommandWindow'][_0x26b746(0x256)]();},VisuMZ['BoostAction'][_0xb5f0d6(0x1e5)]=Scene_Battle[_0xb5f0d6(0x263)][_0xb5f0d6(0x315)],Scene_Battle[_0xb5f0d6(0x263)][_0xb5f0d6(0x315)]=function(){var _0x33f9eb=_0xb5f0d6;this['_helpWindow']&&this[_0x33f9eb(0x310)][_0x33f9eb(0x25b)](),VisuMZ['BoostAction']['Scene_Battle_selectNextCommand'][_0x33f9eb(0x22f)](this);},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x207)]=Scene_Battle[_0xb5f0d6(0x263)][_0xb5f0d6(0x26d)],Scene_Battle['prototype'][_0xb5f0d6(0x26d)]=function(){var _0x2a7148=_0xb5f0d6;VisuMZ[_0x2a7148(0x1e2)][_0x2a7148(0x207)][_0x2a7148(0x22f)](this),this['_helpWindow']&&this[_0x2a7148(0x310)][_0x2a7148(0x2fb)](BattleManager[_0x2a7148(0x237)]());};function Sprite_BoostContainer(){var _0x4bd1a1=_0xb5f0d6;this[_0x4bd1a1(0x300)](...arguments);}Sprite_BoostContainer[_0xb5f0d6(0x263)]=Object[_0xb5f0d6(0x279)](Sprite[_0xb5f0d6(0x263)]),Sprite_BoostContainer[_0xb5f0d6(0x263)][_0xb5f0d6(0x1df)]=Sprite_BoostContainer,Sprite_BoostContainer[_0xb5f0d6(0x2a1)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x2a2)],Sprite_BoostContainer[_0xb5f0d6(0x263)]['initialize']=function(){var _0x4564ff=_0xb5f0d6;Sprite[_0x4564ff(0x263)][_0x4564ff(0x300)][_0x4564ff(0x22f)](this),this[_0x4564ff(0x2bb)](),this[_0x4564ff(0x2c2)]();},Sprite_BoostContainer['prototype'][_0xb5f0d6(0x2bb)]=function(){var _0x40cef6=_0xb5f0d6;this[_0x40cef6(0x1fc)]['x']=Sprite_BoostContainer[_0x40cef6(0x2a1)],this[_0x40cef6(0x1fc)]['y']=Sprite_BoostContainer[_0x40cef6(0x2a1)];},Sprite_BoostContainer[_0xb5f0d6(0x263)][_0xb5f0d6(0x2c2)]=function(){var _0x97b4de=_0xb5f0d6;this['_icons']=[];for(let _0x220a1a=0x1;_0x220a1a<=Game_BattlerBase[_0x97b4de(0x2c7)];_0x220a1a++){const _0xc10d26=new Sprite_BoostIcon(_0x220a1a);this[_0x97b4de(0x226)](_0xc10d26),this[_0x97b4de(0x2cf)][_0x97b4de(0x2e6)](_0xc10d26);}},Sprite_BoostContainer[_0xb5f0d6(0x263)][_0xb5f0d6(0x2bd)]=function(_0x197943){var _0x53dbff=_0xb5f0d6;if(!this['_icons'])return;for(const _0x457c7c of this[_0x53dbff(0x2cf)]){_0x457c7c['setup'](_0x197943);}};function Sprite_BoostIcon(){this['initialize'](...arguments);}Sprite_BoostIcon[_0xb5f0d6(0x263)]=Object['create'](Sprite[_0xb5f0d6(0x263)]),Sprite_BoostIcon['prototype'][_0xb5f0d6(0x1df)]=Sprite_BoostIcon,Sprite_BoostIcon[_0xb5f0d6(0x263)][_0xb5f0d6(0x300)]=function(_0x527e94){var _0x1034c5=_0xb5f0d6;this[_0x1034c5(0x2e9)]=_0x527e94,Sprite['prototype'][_0x1034c5(0x300)][_0x1034c5(0x22f)](this),this['initMembers'](),this[_0x1034c5(0x303)]();},Sprite_BoostIcon['prototype']['initMembers']=function(){var _0xedeb52=_0xb5f0d6;this[_0xedeb52(0x30b)]=ImageManager[_0xedeb52(0x2f0)],this['x']=ImageManager[_0xedeb52(0x2e8)]*(this['_slot']-0x1);},Sprite_BoostIcon[_0xb5f0d6(0x263)][_0xb5f0d6(0x303)]=function(){var _0x5904d4=_0xb5f0d6;this[_0x5904d4(0x2cd)]=ImageManager[_0x5904d4(0x224)](),this[_0x5904d4(0x1f9)](0x0,0x0,0x0,0x0);},Sprite_BoostIcon['prototype'][_0xb5f0d6(0x2bd)]=function(_0x2aefa7){var _0xc3a2a8=_0xb5f0d6;this[_0xc3a2a8(0x1fe)]!==_0x2aefa7&&(this[_0xc3a2a8(0x1fe)]=_0x2aefa7);},Sprite_BoostIcon[_0xb5f0d6(0x263)][_0xb5f0d6(0x2ba)]=function(){var _0x1a76ac=_0xb5f0d6;Sprite['prototype']['update'][_0x1a76ac(0x22f)](this),this[_0x1a76ac(0x2ab)](),this[_0x1a76ac(0x23f)]();},Sprite_BoostIcon['prototype'][_0xb5f0d6(0x2ab)]=function(){var _0x42fb81=_0xb5f0d6;if(this[_0x42fb81(0x1fe)]){let _0x10f16a=this['_battler'][_0x42fb81(0x2a8)]();_0x10f16a>=this['_slot']?this[_0x42fb81(0x30b)]=ImageManager[_0x42fb81(0x2e4)]:this[_0x42fb81(0x30b)]=ImageManager[_0x42fb81(0x2f0)];}else this[_0x42fb81(0x30b)]=0x0;},Sprite_BoostIcon['prototype'][_0xb5f0d6(0x23f)]=function(){var _0x2770b1=_0xb5f0d6;const _0x407882=ImageManager[_0x2770b1(0x2e8)],_0x2fc36d=ImageManager['iconHeight'],_0x4f465a=this[_0x2770b1(0x30b)]%0x10*_0x407882,_0x5cce1a=Math[_0x2770b1(0x239)](this['_iconIndex']/0x10)*_0x2fc36d;this['setFrame'](_0x4f465a,_0x5cce1a,_0x407882,_0x2fc36d);},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x201)]=Window_Base[_0xb5f0d6(0x263)][_0xb5f0d6(0x246)],Window_Base['prototype'][_0xb5f0d6(0x246)]=function(_0x2593b4){var _0x5d62cd=_0xb5f0d6;return _0x2593b4=VisuMZ[_0x5d62cd(0x1e2)]['Window_Base_convertEscapeCharacters'][_0x5d62cd(0x22f)](this,_0x2593b4),_0x2593b4=this['convertBoostEscapeCharacters'](_0x2593b4),_0x2593b4;},Window_Base[_0xb5f0d6(0x263)]['convertBoostEscapeCharacters']=function(_0x918a60){var _0x550318=_0xb5f0d6;return _0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)DMG\[(\d+)\]/gi,function(){return this['convertBoostDamageEscape'](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)DAMAGE\[(\d+)\]/gi,function(){var _0x491530=_0x550318;return this[_0x491530(0x233)](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)TURN\[(\d+)\]/gi,function(){var _0x32c44e=_0x550318;return this[_0x32c44e(0x311)](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60['replace'](/\x1b(?:BP|BOOST)TURNS\[(\d+)\]/gi,function(){var _0xf1b0a4=_0x550318;return this[_0xf1b0a4(0x311)](parseInt(arguments[0x1]));}['bind'](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)REP\[(\d+)\]/gi,function(){return this['convertBoostRepeatEscape'](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)REPEAT\[(\d+)\]/gi,function(){var _0x54da74=_0x550318;return this[_0x54da74(0x204)](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)REPEATS\[(\d+)\]/gi,function(){return this['convertBoostRepeatEscape'](parseInt(arguments[0x1]));}['bind'](this)),_0x918a60=_0x918a60['replace'](/\x1b(?:BP|BOOST)HITS\[(\d+)\]/gi,function(){var _0x61c18d=_0x550318;return this[_0x61c18d(0x204)](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)ANALYZE\[(\d+)\]/gi,function(){return this['convertBoostAnalyzeEscape'](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)EFFECT\[(\d+)\]/gi,function(){var _0x164553=_0x550318;return this[_0x164553(0x2c6)](parseInt(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)\[(.*?)\]/gi,function(){var _0x2be2ac=_0x550318;return this[_0x2be2ac(0x218)](String(arguments[0x1]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)0\[(.*?)\]/gi,function(){var _0x35f250=_0x550318;return this[_0x35f250(0x2cc)](String(arguments[0x1]));}['bind'](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)=(\d+)\[(.*?)\]/gi,function(){var _0x3f3668=_0x550318;return this[_0x3f3668(0x2b2)](parseInt(arguments[0x1]),String(arguments[0x2]));}['bind'](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)=(\d+)\[(.*?)\]/gi,function(){var _0x34e24e=_0x550318;return this[_0x34e24e(0x2b2)](parseInt(arguments[0x1]),String(arguments[0x2]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)\<=(\d+)\[(.*?)\]/gi,function(){var _0x54008d=_0x550318;return this[_0x54008d(0x266)](parseInt(arguments[0x1]),String(arguments[0x2]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)\<(\d+)\[(.*?)\]/gi,function(){var _0x6b51fa=_0x550318;return this[_0x6b51fa(0x234)](parseInt(arguments[0x1]),String(arguments[0x2]));}[_0x550318(0x225)](this)),_0x918a60=_0x918a60['replace'](/\x1b(?:BP|BOOST)\>=(\d+)\[(.*?)\]/gi,function(){var _0x2b4433=_0x550318;return this[_0x2b4433(0x29a)](parseInt(arguments[0x1]),String(arguments[0x2]));}['bind'](this)),_0x918a60=_0x918a60[_0x550318(0x216)](/\x1b(?:BP|BOOST)\>(\d+)\[(.*?)\]/gi,function(){return this['convertBoostGreaterEscape'](parseInt(arguments[0x1]),String(arguments[0x2]));}[_0x550318(0x225)](this)),_0x918a60;},Window_Base[_0xb5f0d6(0x263)][_0xb5f0d6(0x233)]=function(_0x3214a7){var _0x15c2f4=_0xb5f0d6;if(!!this[_0x15c2f4(0x24d)]){var _0x44a557=this[_0x15c2f4(0x24d)]['boostMultiplier'](_0x15c2f4(0x28c));_0x3214a7=Math[_0x15c2f4(0x1f2)](_0x3214a7*_0x44a557),_0x3214a7+=this[_0x15c2f4(0x24d)][_0x15c2f4(0x270)](_0x15c2f4(0x28c));}return _0x3214a7;},Window_Base[_0xb5f0d6(0x263)][_0xb5f0d6(0x311)]=function(_0x1a9d26){var _0x309fd3=_0xb5f0d6;if(!!this[_0x309fd3(0x24d)]){var _0x4d6387=this[_0x309fd3(0x24d)]['boostMultiplier'](_0x309fd3(0x2d6));_0x1a9d26=Math[_0x309fd3(0x1f2)](_0x1a9d26*_0x4d6387),_0x1a9d26+=this['_bpSubject'][_0x309fd3(0x270)]('Turn');}return _0x1a9d26;},Window_Base['prototype']['convertBoostRepeatEscape']=function(_0x2c4597){var _0x2e6ba6=_0xb5f0d6;if(!!this[_0x2e6ba6(0x24d)]){var _0xd1cda2=this[_0x2e6ba6(0x24d)][_0x2e6ba6(0x272)](_0x2e6ba6(0x28b));_0x2c4597=Math[_0x2e6ba6(0x1f2)](_0x2c4597*_0xd1cda2),_0x2c4597+=this[_0x2e6ba6(0x24d)]['boostAddition'](_0x2e6ba6(0x28b));}return _0x2c4597;},Window_Base[_0xb5f0d6(0x263)][_0xb5f0d6(0x223)]=function(_0xc2d86e){var _0x392fa8=_0xb5f0d6;if(!!this[_0x392fa8(0x24d)]){var _0x768000=this[_0x392fa8(0x24d)][_0x392fa8(0x272)]('Analyze');_0xc2d86e=Math[_0x392fa8(0x1f2)](_0xc2d86e*_0x768000),_0xc2d86e+=this[_0x392fa8(0x24d)][_0x392fa8(0x270)](_0x392fa8(0x302));}return _0xc2d86e;},Window_Base[_0xb5f0d6(0x263)]['convertBoostEffectEscape']=function(_0x562a57){var _0x29a09d=_0xb5f0d6;if(!!this[_0x29a09d(0x24d)]){var _0x1dd302=this[_0x29a09d(0x24d)][_0x29a09d(0x272)](_0x29a09d(0x24e));_0x562a57=Math[_0x29a09d(0x1f2)](_0x562a57*_0x1dd302),_0x562a57+=this['_bpSubject'][_0x29a09d(0x270)](_0x29a09d(0x24e));}return _0x562a57;},Window_Base[_0xb5f0d6(0x263)][_0xb5f0d6(0x218)]=function(_0x2adaf9){var _0x338335=_0xb5f0d6;return!!this[_0x338335(0x24d)]&&this['_bpSubject']['toUseBoostPoints']()>0x0?_0x2adaf9:'';},Window_Base[_0xb5f0d6(0x263)][_0xb5f0d6(0x2cc)]=function(_0x33ac6d){var _0x9a4998=_0xb5f0d6;return!this[_0x9a4998(0x24d)]||this['_bpSubject']['toUseBoostPoints']()<=0x0?_0x33ac6d:'';},Window_Base[_0xb5f0d6(0x263)]['convertBoostEqualEscape']=function(_0x36d54f,_0x1ad976){var _0x44772a=_0xb5f0d6;return!!this[_0x44772a(0x24d)]&&this[_0x44772a(0x24d)][_0x44772a(0x20e)]()===_0x36d54f?_0x1ad976:'';},Window_Base['prototype'][_0xb5f0d6(0x2b2)]=function(_0x3b2213,_0x245597){var _0x114970=_0xb5f0d6;return!!this[_0x114970(0x24d)]&&this[_0x114970(0x24d)][_0x114970(0x20e)]()===_0x3b2213?_0x245597:'';},Window_Base['prototype'][_0xb5f0d6(0x266)]=function(_0x14a282,_0x5114fe){var _0x49b790=_0xb5f0d6;return!!this[_0x49b790(0x24d)]&&this['_bpSubject']['toUseBoostPoints']()<=_0x14a282?_0x5114fe:'';},Window_Base[_0xb5f0d6(0x263)]['convertBoostLessEscape']=function(_0x197118,_0x21bd0d){var _0x54d974=_0xb5f0d6;return!!this['_bpSubject']&&this[_0x54d974(0x24d)][_0x54d974(0x20e)]()<_0x197118?_0x21bd0d:'';},Window_Base['prototype'][_0xb5f0d6(0x29a)]=function(_0x4c6773,_0x38a718){var _0x265df0=_0xb5f0d6;return!!this[_0x265df0(0x24d)]&&this[_0x265df0(0x24d)][_0x265df0(0x20e)]()>=_0x4c6773?_0x38a718:'';},Window_Base[_0xb5f0d6(0x263)]['convertBoostGreaterEscape']=function(_0x5db3a9,_0x4919f9){var _0xb5f7b9=_0xb5f0d6;return!!this[_0xb5f7b9(0x24d)]&&this['_bpSubject'][_0xb5f7b9(0x20e)]()>_0x5db3a9?_0x4919f9:'';},Window_Selectable['BOOST_ACTION_SHORTCUT_PAGEUP_PAGEDN']=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x2a3)],Window_Selectable[_0xb5f0d6(0x1e1)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x265)],Window_Selectable[_0xb5f0d6(0x263)][_0xb5f0d6(0x2e3)]=function(){var _0x2dc3c3=_0xb5f0d6;const _0x19247d=this[_0x2dc3c3(0x1df)]['name'];return Window_Selectable[_0x2dc3c3(0x1e1)][_0x2dc3c3(0x277)](_0x19247d)?![]:!![];},Window_Selectable[_0xb5f0d6(0x263)][_0xb5f0d6(0x206)]=function(){var _0x228b25=_0xb5f0d6;if(!SceneManager[_0x228b25(0x29b)]())return![];if(!Window_Selectable[_0x228b25(0x267)])return![];if(!BattleManager['allowBoostAction']())return![];return this[_0x228b25(0x2e3)]();},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x314)]=Window_Selectable['prototype'][_0xb5f0d6(0x2f7)],Window_Selectable[_0xb5f0d6(0x263)][_0xb5f0d6(0x2f7)]=function(){var _0x321fca=_0xb5f0d6;if(this[_0x321fca(0x206)]()){const _0x17e88b=BattleManager['actor']();_0x17e88b&&_0x17e88b['canUseBoostPoints']()&&(SceneManager['_scene'][_0x321fca(0x2be)](!![]),this[_0x321fca(0x256)](),this[_0x321fca(0x2d4)]()),Input['clear']();}else VisuMZ[_0x321fca(0x1e2)][_0x321fca(0x314)][_0x321fca(0x22f)](this);},VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x228)]=Window_Selectable[_0xb5f0d6(0x263)]['cursorPageup'],Window_Selectable['prototype']['cursorPageup']=function(){var _0x36f827=_0xb5f0d6;if(this[_0x36f827(0x206)]()){const _0x19c726=BattleManager['actor']();_0x19c726&&_0x19c726[_0x36f827(0x1e8)]()&&(SceneManager[_0x36f827(0x30c)]['commandUnboost'](!![]),this[_0x36f827(0x256)](),this[_0x36f827(0x2d4)]()),Input[_0x36f827(0x249)]();}else VisuMZ[_0x36f827(0x1e2)][_0x36f827(0x228)][_0x36f827(0x22f)](this);},Window_Help[_0xb5f0d6(0x263)][_0xb5f0d6(0x2fb)]=function(_0x461590){var _0x1abe81=_0xb5f0d6;this[_0x1abe81(0x24d)]=_0x461590;},Window_Help[_0xb5f0d6(0x263)]['clearBoostSubject']=function(){var _0x37190c=_0xb5f0d6;this[_0x37190c(0x24d)]=undefined;},Window_StatusBase[_0xb5f0d6(0x263)][_0xb5f0d6(0x229)]=function(){var _0x2c2b84=_0xb5f0d6;return BattleManager[_0x2c2b84(0x2ed)]();},Window_StatusBase[_0xb5f0d6(0x263)]['placeBoostPoints']=function(_0x2f4599,_0x393e15,_0x352cae){var _0x550330=_0xb5f0d6;if(!this['shouldDrawBoostIcons']())return;const _0x458bfe=_0x550330(0x26a)['format'](_0x2f4599[_0x550330(0x2ad)]()),_0x65e56f=this['createInnerSprite'](_0x458bfe,Sprite_BoostContainer);_0x65e56f[_0x550330(0x2bd)](_0x2f4599),_0x65e56f[_0x550330(0x283)](_0x393e15,_0x352cae),_0x65e56f[_0x550330(0x28d)]();},Window_ActorCommand[_0xb5f0d6(0x269)]=VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x22e)],Window_ActorCommand[_0xb5f0d6(0x26b)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI']['ShowUnboostCmd'],VisuMZ[_0xb5f0d6(0x1e2)]['Window_ActorCommand_addGuardCommand']=Window_ActorCommand[_0xb5f0d6(0x263)]['addGuardCommand'],Window_ActorCommand[_0xb5f0d6(0x263)][_0xb5f0d6(0x2e5)]=function(){var _0x2a0fc0=_0xb5f0d6;BattleManager[_0x2a0fc0(0x2ed)]()&&(this[_0x2a0fc0(0x2dd)](),this[_0x2a0fc0(0x2b4)]()),VisuMZ[_0x2a0fc0(0x1e2)][_0x2a0fc0(0x2b3)][_0x2a0fc0(0x22f)](this);},Window_ActorCommand[_0xb5f0d6(0x263)]['addBoostCommand']=function(){var _0x30702c=_0xb5f0d6;if(!Window_ActorCommand[_0x30702c(0x269)])return;const _0x2d4892=this[_0x30702c(0x1f0)](),_0x52a23b=TextManager[_0x30702c(0x2a9)],_0xf33cc5=ImageManager['boostIcon'],_0x184cba=_0x2d4892===_0x30702c(0x26f)?_0x52a23b:_0x30702c(0x2a0)[_0x30702c(0x292)](_0xf33cc5,_0x52a23b);var _0x452409=this[_0x30702c(0x2f3)][_0x30702c(0x232)]();this[_0x30702c(0x2f9)](_0x184cba,'boost',_0x452409);},Window_ActorCommand[_0xb5f0d6(0x263)][_0xb5f0d6(0x2b4)]=function(){var _0x4688dd=_0xb5f0d6;if(!Window_ActorCommand[_0x4688dd(0x26b)])return;const _0x4e4cda=this['commandStyle'](),_0x5e567f=TextManager[_0x4688dd(0x1f1)],_0x283f35=ImageManager[_0x4688dd(0x2f0)],_0x3215af=_0x4e4cda===_0x4688dd(0x26f)?_0x5e567f:_0x4688dd(0x2a0)[_0x4688dd(0x292)](_0x283f35,_0x5e567f);var _0x28215b=this[_0x4688dd(0x2f3)][_0x4688dd(0x1e8)]();this['addCommand'](_0x3215af,_0x4688dd(0x23b),_0x28215b);},Window_ActorCommand[_0xb5f0d6(0x263)][_0xb5f0d6(0x2c8)]=function(){var _0x59cd15=_0xb5f0d6;this[_0x59cd15(0x2ca)]()!==_0x59cd15(0x235)&&this[_0x59cd15(0x2ca)]()!==_0x59cd15(0x23b)&&Window_Selectable[_0x59cd15(0x263)][_0x59cd15(0x2c8)]['call'](this);},Window_BattleStatus[_0xb5f0d6(0x231)]=VisuMZ['BoostAction'][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x2da)],Window_BattleStatus[_0xb5f0d6(0x285)]=VisuMZ[_0xb5f0d6(0x1e2)]['Settings']['UI'][_0xb5f0d6(0x208)],Window_BattleStatus[_0xb5f0d6(0x284)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x309)],Window_BattleStatus[_0xb5f0d6(0x251)]=VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x2d7)]['UI'][_0xb5f0d6(0x259)],VisuMZ[_0xb5f0d6(0x1e2)][_0xb5f0d6(0x288)]=Window_BattleStatus['prototype']['drawItemStatus'],Window_BattleStatus[_0xb5f0d6(0x263)][_0xb5f0d6(0x27d)]=function(_0x457211){var _0x59842c=_0xb5f0d6;VisuMZ[_0x59842c(0x1e2)][_0x59842c(0x288)][_0x59842c(0x22f)](this,_0x457211),this['drawItemStatusBoostPoints'](_0x457211);},Window_BattleStatus[_0xb5f0d6(0x263)][_0xb5f0d6(0x2d9)]=function(_0x4164a5){var _0x539bda=_0xb5f0d6;if(!Window_BattleStatus[_0x539bda(0x231)])return;const _0x185b0e=this[_0x539bda(0x237)](_0x4164a5);if(!_0x185b0e)return;!Window_BattleStatus[_0x539bda(0x285)]?this[_0x539bda(0x2b0)](_0x4164a5):this['drawItemStatusBoostPointsAuto'](_0x4164a5);},Window_BattleStatus[_0xb5f0d6(0x263)][_0xb5f0d6(0x2b0)]=function(_0x4679fe){var _0x338721=_0xb5f0d6;const _0x447f6d=this[_0x338721(0x237)](_0x4679fe),_0x4d3e96=this['itemRectWithPadding'](_0x4679fe);let _0x25b4e1=_0x4d3e96['x']-0x4+Window_BattleStatus['BOOST_POINTS_DISPLAY_OFFSET_X'],_0x15cca5=_0x4d3e96['y']+0x4+Window_BattleStatus['BOOST_POINTS_DISPLAY_OFFSET_Y'];this[_0x338721(0x281)](_0x447f6d,_0x25b4e1,_0x15cca5);},Window_BattleStatus[_0xb5f0d6(0x263)]['drawItemStatusBoostPointsAuto']=function(_0x1df654){var _0x21044a=_0xb5f0d6;const _0x5ce051=this[_0x21044a(0x237)](_0x1df654),_0x3f440f=this[_0x21044a(0x2b6)](_0x1df654),_0x143381=Math['ceil'](ImageManager[_0x21044a(0x2e8)]*Game_BattlerBase[_0x21044a(0x2c7)]*Sprite_BoostContainer['ICON_SIZE_RATE']),_0x1d53a1=Math[_0x21044a(0x21f)](ImageManager['iconHeight']*Sprite_BoostContainer[_0x21044a(0x2a1)]);let _0x126bb2=_0x3f440f['x']+0x4,_0x664045=_0x3f440f['y']+0x4;const _0x5321bd=this[_0x21044a(0x2a4)]();switch(_0x5321bd){case _0x21044a(0x250):VisuMZ[_0x21044a(0x1fa)]['Settings'][_0x21044a(0x293)][_0x21044a(0x24a)]?_0x126bb2+=ImageManager[_0x21044a(0x2c4)]+0x8:_0x126bb2+=ImageManager[_0x21044a(0x2e8)]+0x8;_0x126bb2+=0x88,_0x126bb2+=0x88*0x2;$dataSystem[_0x21044a(0x304)]&&(_0x126bb2+=0x88);_0x664045+=Math[_0x21044a(0x1e0)](0x0,Math[_0x21044a(0x1f2)]((this[_0x21044a(0x299)]()-_0x1d53a1)/0x2));break;case'xp':case _0x21044a(0x2ce):case _0x21044a(0x1f8):_0x126bb2=Math[_0x21044a(0x1f2)](_0x3f440f['x']+(_0x3f440f[_0x21044a(0x2ef)]-_0x143381)/0x2);break;case _0x21044a(0x2ec):_0x126bb2=Math[_0x21044a(0x1f2)](_0x3f440f['x']+(_0x3f440f[_0x21044a(0x2ef)]-_0x143381)/0x2);const _0x151f81=$dataSystem['optDisplayTp']?0x4:0x3;_0x664045=Math[_0x21044a(0x1f2)](_0x3f440f['y']+_0x3f440f['height']-0x4-this[_0x21044a(0x299)]()*_0x151f81);break;}_0x126bb2+=Window_BattleStatus['BOOST_POINTS_DISPLAY_OFFSET_X'],_0x664045+=Window_BattleStatus[_0x21044a(0x251)],this[_0x21044a(0x281)](_0x5ce051,_0x126bb2,_0x664045);};