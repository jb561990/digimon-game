//=============================================================================
// VisuStella MZ - Sideview Battle UI
// VisuMZ_3_SideviewBattleUI.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_3_SideviewBattleUI = true;

var VisuMZ = VisuMZ || {};
VisuMZ.SideviewBattleUI = VisuMZ.SideviewBattleUI || {};
VisuMZ.SideviewBattleUI.version = 1.00;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 3] [Version 1.00] [SideviewBattleUI]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Sideview_Battle_UI_VisuStella_MZ
 * @base VisuMZ_0_CoreEngine
 * @base VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_BattleCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * This plugin changes the RPG Maker MZ Battle UI for Sideview Battle Systems
 * into something more minimalistic. The menus are placed towards the player's
 * party to let the player focus their attention to the center of the screen
 * instead of to the lower ledges of the screen. The input command windows show
 * up near the inputting actor to give the player a clear understanding of who
 * is performing what action.
 * 
 * *NOTE* To use this battle layout, you will need the updated version of
 * VisuStella's Battle Core. Go into its Plugin Parameters and change the
 * Battle Layout Settings > Battle Layout Style > plugin parameter to this
 * value: "Sideview Battle UI" or "sideview_ui".
 *
 * Features include all (but not limited to) the following:
 * 
 * * This plugin changes the UI for the RPG Maker MZ Sideview Battle System.
 * * Status windows appear on the side of the screen for each actor in battle.
 * * The appearance is more compact for both the status windows and input
 *   command windows.
 * * More of the battlefield can be seen with this kind of layout.
 * * Lots of customization options to adjust the status windows.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_0_CoreEngine
 * * VisuMZ_1_BattleCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 3 ------
 *
 * This plugin is a Tier 3 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Sideview Only
 * 
 * This plugin only works for the sideview battle system. If this layout is
 * selected in the Battle Core, the battle system will automatically shift to
 * sideview regardless of the settings.
 * 
 * *NOTE* To use this battle layout, you will need the updated version of
 * VisuStella's Battle Core. Go into its Plugin Parameters and change the
 * Battle Layout Settings > Battle Layout Style > plugin parameter to this
 * value: "Sideview Battle UI" or "sideview_ui".
 *
 * ---
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 *
 * VisuMZ_2_AggroControlSystem
 * VisuMZ_2_BattleSystemBTB
 * VisuMZ_3_BoostAction
 * VisuMZ_3_StateTooltips
 * VisuMZ_4_BreakShields
 *
 * There are features provided in this plugin for the above plugins. Their UI
 * elements can be shown with this plugin's status windows.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Battler Offset Settings
 * ============================================================================
 *
 * Settings for battler sprite offsets when using the Sideview Battle UI.
 * Since there's more room on the screen, placing them lower will help adjust
 * for the player's visual comfort.
 *
 * ---
 *
 * Settings
 * 
 *   Perform Offset?:
 *   - Offsets the battler sprite positions when using Sideview Battle UI.
 * 
 *   Offset X:
 *   - How much to offset the sprite positions by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the sprite positions by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Window Settings
 * ============================================================================
 *
 * Settings for general windows when using the Sideview Battle UI. These
 * settings are made for the windows that aren't the status windows but are
 * affected by this plugin.
 *
 * ---
 *
 * Global
 * 
 *   UI Scale:
 *   - What is the scaling rate for battle windows?
 *   - Use a number between 0 and 1 for the best results.
 *
 * ---
 *
 * Help Window
 * 
 *   Fade BG Style?:
 *   - Fade the Help Window background with this UI?
 *
 * ---
 *
 * Actor Command Window
 * 
 *   Max Rows:
 *   - What is the maximum number of rows for the actor command window with
 *     this UI?
 *
 * ---
 *
 * Party Command Window
 * 
 *   Max Rows:
 *   - What is the maximum number of rows for the party command window with
 *     this UI?
 *
 * ---
 *
 * Item Window
 * 
 *   Max Rows:
 *   - What is the maximum number of rows for the item window with this UI?
 * 
 *   Width:
 *   - What is the width item window with this UI?
 *   - This is the width BEFORE scaling.
 * 
 *   Offset X:
 *   - How much to offset the window X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the window Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Skill Window
 * 
 *   Max Rows:
 *   - What is the maximum number of rows for the skill window with this UI?
 * 
 *   Width:
 *   - What is the width skill window with this UI?
 *   - This is the width BEFORE scaling.
 * 
 *   Offset X:
 *   - How much to offset the window X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the window Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Status Window Settings
 * ============================================================================
 *
 * Settings for the status window when using the Sideview Battle UI. Each of
 * these plugin parameters allow you to adjust many of the various elements
 * found inside of this window.
 *
 * ---
 *
 * Dimensions
 * 
 *   Width Base:
 *   - How width is each actor's status window?
 *   - This is the width AFTER scaling.
 * 
 *   Height Base:
 *   - How tall do you want the status window to be?
 *   - 'auto' for automatic calculations.
 *   - This is the height BEFORE scaling.
 * 
 *     Height Buffer:
 *     - How much space do you want there to be vertically from window
 *       to window?
 *     - This is the height BEFORE scaling.
 * 
 *   Move Distance:
 *   - How far will the status window move when the actor is selected
 *     or active?
 * 
 *     Move Speed:
 *     - How many pixels with the status window move per frame?
 *
 * ---
 *
 * Standard UI > Name
 * 
 *   Show?:
 *   - Show this UI element?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Standard UI > States
 * 
 *   Show?:
 *   - Show this UI element?
 * 
 *   Ignore Scale?:
 *   - Ignore scaling to show icons at their real size?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Standard UI > TPB/ATB Gauge
 * 
 *   Show?:
 *   - Show this UI element?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Standard UI > HP Gauge
 * 
 *   Show?:
 *   - Show this UI element?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Standard UI > MP Gauge
 * 
 *   Show?:
 *   - Show this UI element?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Standard UI > TP Gauge
 * 
 *   Show?:
 *   - Show this UI element?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Compatibility UI > Aggro Gauge
 * 
 *   Show?:
 *   - Show this UI element?
 *   - Requires VisuMZ_2_AggroControlSystem!
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Compatibility UI > Boost Points
 * 
 *   Show?:
 *   - Show this UI element?
 *   - Requires VisuMZ_3_BoostAction!
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Compatibility UI > Brave Points
 * 
 *   Show?:
 *   - Show this UI element?
 *   - Requires VisuMZ_2_BattleSystemBTB!
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Compatibility UI > Break Shield
 * 
 *   Show?:
 *   - Show this UI element?
 *   - Requires VisuMZ_4_BreakShields!
 * 
 *   Ignore Scale?:
 *   - Ignore scaling to show icons at their real size?
 * 
 *   Offset X:
 *   - How much to offset the UI X position by?
 *   - Negative goes left. Positive goes right.
 * 
 *   Offset Y:
 *   - How much to offset the UI Y position by?
 *   - Negative goes up. Positive goes down.
 *
 * ---
 *
 * Compatibility UI > State Tooltips
 * 
 *   Show?:
 *   - Show this UI element?
 *   - Requires VisuMZ_3_StateTooltips!
 *
 * ---
 * 
 * JS
 * 
 *   JS: Custom UI:
 *   - JavaScript used to add custom elements to each status window.
 * 
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 *
 * Version 1.00 Official Release Date: May 12, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param SideviewBattleUI
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Battler:struct
 * @text Battler Offset Settings
 * @type struct<Battler>
 * @desc Settings for battler sprite offsets when using the Sideview Battle UI.
 * @default {"Enable:eval":"true","OffsetX:num":"+0","OffsetY:num":"+128"}
 *
 * @param GeneralWindow:struct
 * @text General Window Settings
 * @type struct<GeneralWindow>
 * @desc Settings for general windows when using the Sideview Battle UI.
 * @default {"Global":"","UiScale:num":"0.80","HelpWindow":"","HelpFadeStyle:eval":"true","ActorCommandWindow":"","ActorCommandWindowMaxRows:num":"8","PartyCommandWindow":"","PartyCommandWindowMaxRows:num":"8","ItemWindow":"","ItemWindowMaxRows:num":"8","ItemWindowWidth:num":"400","ItemWindowOffsetX:num":"+16","ItemWindowOffsetY:num":"+16","SkillWindow":"","SkillWindowMaxRows:num":"8","SkillWindowWidth:num":"400","SkillWindowOffsetX:num":"+16","SkillWindowOffsetY:num":"+16"}
 *
 * @param StatusWindow:struct
 * @text Status Window Settings
 * @type struct<StatusWindow>
 * @desc Settings for the status window when using the Sideview Battle UI.
 * @default {"Dimensions":"","WidthBase:num":"200","HeightBase:str":"auto","HeightBuffer:num":"4","MoveDistance:num":"48","MoveSpeed:num":"4","Standard":"","Name":"","NameShow:eval":"true","NameOffsetX:num":"+48","NameOffsetY:num":"+0","States":"","StatesShow:eval":"true","StatesIgnoreScale:eval":"true","StatesOffsetX:num":"+20","StatesOffsetY:num":"+20","Tpb":"","TpbShow:eval":"true","TpbOffsetX:num":"+44","TpbOffsetY:num":"+0","Hp":"","HpShow:eval":"true","HpOffsetX:num":"+60","HpOffsetY:num":"+0","Mp":"","MpShow:eval":"true","MpOffsetX:num":"+68","MpOffsetY:num":"+0","Tp":"","TpShow:eval":"true","TpOffsetX:num":"+74","TpOffsetY:num":"+0","Compatibility":"","Aggro":"","AggroShow:eval":"true","AggroOffsetX:num":"+44","AggroOffsetY:num":"+0","Boost":"","BoostShow:eval":"true","BoostOffsetX:num":"+52","BoostOffsetY:num":"+2","Brave":"","BraveShow:eval":"true","BraveOffsetX:num":"+52","BraveOffsetY:num":"-6","BreakShield":"","BreakShieldShow:eval":"true","BreakShieldIgnoreScale:eval":"true","BreakShieldOffsetX:num":"+20","BreakShieldOffsetY:num":"+20","StateTooltips":"","StateTooltipsShow:eval":"true","JS":"","CustomUi:func":"\"// Declare Variables\\nconst actor = arguments[0];\\nlet x = 0;\\nlet y = 0;\\nlet width = this.innerWidth;\\nlet height = this.innerHeight;\\n\\n// Draw Custom Elements\\n// Put in code you want here used for windows classes\""}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Battler Offset Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Battler:
 *
 * @param Enable:eval
 * @text Perform Offset?
 * @type boolean
 * @on Do Offset
 * @off Don't Offset
 * @desc Offsets the battler sprite positions when using Sideview Battle UI.
 * @default true
 *
 * @param OffsetX:num
 * @text Offset X
 * @desc How much to offset the sprite positions by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param OffsetY:num
 * @text Offset Y
 * @desc How much to offset the sprite positions by?
 * Negative goes up. Positive goes down.
 * @default +128
 *
 */
/* ----------------------------------------------------------------------------
 * GeneralWindow Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~GeneralWindow:
 *
 * @param Global
 *
 * @param UiScale:num
 * @text UI Scale
 * @parent Global
 * @desc What is the scaling rate for battle windows?
 * Use a number between 0 and 1 for the best results.
 * @default 0.80
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpFadeStyle:eval
 * @text Fade BG Style?
 * @parent HelpWindow
 * @type boolean
 * @on Fade Background
 * @off Default Background
 * @desc Fade the Help Window background with this UI?
 * @default true
 *
 * @param ActorCommandWindow
 * @text Actor Command Window
 *
 * @param ActorCommandWindowMaxRows:num
 * @text Max Rows
 * @parent ActorCommandWindow
 * @type number
 * @min 1
 * @desc What is the maximum number of rows for the actor command window with this UI?
 * @default 8
 *
 * @param PartyCommandWindow
 * @text Party Command Window
 *
 * @param PartyCommandWindowMaxRows:num
 * @text Max Rows
 * @parent PartyCommandWindow
 * @type number
 * @min 1
 * @desc What is the maximum number of rows for the party command window with this UI?
 * @default 8
 *
 * @param ItemWindow
 * @text Item Window
 *
 * @param ItemWindowMaxRows:num
 * @text Max Rows
 * @parent ItemWindow
 * @type number
 * @min 1
 * @desc What is the maximum number of rows for the item window with this UI?
 * @default 8
 *
 * @param ItemWindowWidth:num
 * @text Width
 * @parent ItemWindow
 * @type number
 * @min 1
 * @desc What is the width item window with this UI?
 * This is the width BEFORE scaling.
 * @default 400
 *
 * @param ItemWindowOffsetX:num
 * @text Offset X
 * @parent ItemWindow
 * @desc How much to offset the window X position by?
 * Negative goes left. Positive goes right.
 * @default +16
 *
 * @param ItemWindowOffsetY:num
 * @text Offset Y
 * @parent ItemWindow
 * @desc How much to offset the window Y position by?
 * Negative goes up. Positive goes down.
 * @default +16
 *
 * @param SkillWindow
 * @text Skill Window
 *
 * @param SkillWindowMaxRows:num
 * @text Max Rows
 * @parent SkillWindow
 * @type number
 * @min 1
 * @desc What is the maximum number of rows for the skill window with this UI?
 * @default 8
 *
 * @param SkillWindowWidth:num
 * @text Width
 * @parent SkillWindow
 * @type number
 * @min 1
 * @desc What is the width skill window with this UI?
 * This is the width BEFORE scaling.
 * @default 400
 *
 * @param SkillWindowOffsetX:num
 * @text Offset X
 * @parent SkillWindow
 * @desc How much to offset the window X position by?
 * Negative goes left. Positive goes right.
 * @default +16
 *
 * @param SkillWindowOffsetY:num
 * @text Offset Y
 * @parent SkillWindow
 * @desc How much to offset the window Y position by?
 * Negative goes up. Positive goes down.
 * @default +16
 *
 */
/* ----------------------------------------------------------------------------
 * Status Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~StatusWindow:
 *
 * @param Dimensions
 *
 * @param WidthBase:num
 * @text Width Base
 * @parent Dimensions
 * @type number
 * @desc How width is each actor's status window?
 * This is the width AFTER scaling.
 * @default 200
 *
 * @param HeightBase:str
 * @text Height Base
 * @parent Dimensions
 * @type number
 * @desc How tall do you want the status window to be?
 * 'auto' for automatic calculations. Value is BEFORE scaling.
 * @default auto
 *
 * @param HeightBuffer:num
 * @text Height Buffer
 * @parent HeightBase:str
 * @type number
 * @desc How much space do you want there to be vertically from window to window?
 * @default 4
 *
 * @param MoveDistance:num
 * @text Move Distance
 * @parent Dimensions
 * @type number
 * @desc How far will the status window move when
 * the actor is selected or active?
 * @default 48
 *
 * @param MoveSpeed:num
 * @text Move Speed
 * @parent MoveDistance:num
 * @type number
 * @desc How many pixels with the status window move per frame?
 * @default 4
 *
 * @param Standard
 * @text Standard UI
 * 
 * @param Name
 * @parent Standard
 *
 * @param NameShow:eval
 * @text Show?
 * @parent Name
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * @default true
 *
 * @param NameOffsetX:num
 * @text Offset X
 * @parent Name
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +48
 *
 * @param NameOffsetY:num
 * @text Offset Y
 * @parent Name
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param States
 * @parent Standard
 *
 * @param StatesShow:eval
 * @text Show?
 * @parent States
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * @default true
 *
 * @param StatesIgnoreScale:eval
 * @text Ignore Scale?
 * @parent States
 * @type boolean
 * @on Ignore Scaling
 * @off Use Scaling
 * @desc Ignore scaling to show icons at their real size?
 * @default true
 *
 * @param StatesOffsetX:num
 * @text Offset X
 * @parent States
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +20
 *
 * @param StatesOffsetY:num
 * @text Offset Y
 * @parent States
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +20
 * 
 * @param Tpb
 * @text TPB/ATB Gauge
 * @parent Standard
 *
 * @param TpbShow:eval
 * @text Show?
 * @parent Tpb
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * @default true
 *
 * @param TpbOffsetX:num
 * @text Offset X
 * @parent Tpb
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +44
 *
 * @param TpbOffsetY:num
 * @text Offset Y
 * @parent Tpb
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param Hp
 * @text HP Gauge
 * @parent Standard
 *
 * @param HpShow:eval
 * @text Show?
 * @parent Hp
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * @default true
 *
 * @param HpOffsetX:num
 * @text Offset X
 * @parent Hp
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +60
 *
 * @param HpOffsetY:num
 * @text Offset Y
 * @parent Hp
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param Mp
 * @text MP Gauge
 * @parent Standard
 *
 * @param MpShow:eval
 * @text Show?
 * @parent Mp
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * @default true
 *
 * @param MpOffsetX:num
 * @text Offset X
 * @parent Mp
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +68
 *
 * @param MpOffsetY:num
 * @text Offset Y
 * @parent Mp
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param Tp
 * @text TP Gauge
 * @parent Standard
 *
 * @param TpShow:eval
 * @text Show?
 * @parent Tp
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * @default true
 *
 * @param TpOffsetX:num
 * @text Offset X
 * @parent Tp
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +74
 *
 * @param TpOffsetY:num
 * @text Offset Y
 * @parent Tp
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 *
 * @param Compatibility
 * @text Compatibility UI
 * 
 * @param Aggro
 * @text Aggro Gauge
 * @parent Compatibility
 * @default VisuMZ_2_AggroControlSystem
 *
 * @param AggroShow:eval
 * @text Show?
 * @parent Aggro
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * Requires VisuMZ_2_AggroControlSystem!
 * @default true
 *
 * @param AggroOffsetX:num
 * @text Offset X
 * @parent Aggro
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +44
 *
 * @param AggroOffsetY:num
 * @text Offset Y
 * @parent Aggro
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +0
 * 
 * @param Boost
 * @text Boost Points
 * @parent Compatibility
 * @default VisuMZ_3_BoostAction
 *
 * @param BoostShow:eval
 * @text Show?
 * @parent Boost
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * Requires VisuMZ_3_BoostAction!
 * @default true
 *
 * @param BoostOffsetX:num
 * @text Offset X
 * @parent Boost
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +52
 *
 * @param BoostOffsetY:num
 * @text Offset Y
 * @parent Boost
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +2
 * 
 * @param Brave
 * @text Brave Points
 * @parent Compatibility
 * @default VisuMZ_2_BattleSystemBTB
 *
 * @param BraveShow:eval
 * @text Show?
 * @parent Brave
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * Requires VisuMZ_2_BattleSystemBTB!
 * @default true
 *
 * @param BraveOffsetX:num
 * @text Offset X
 * @parent Brave
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +52
 *
 * @param BraveOffsetY:num
 * @text Offset Y
 * @parent Brave
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default -6
 * 
 * @param BreakShield
 * @text Break Shield
 * @parent Compatibility
 * @default VisuMZ_4_BreakShields
 *
 * @param BreakShieldShow:eval
 * @text Show?
 * @parent BreakShield
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * Requires VisuMZ_4_BreakShields!
 * @default true
 *
 * @param BreakShieldIgnoreScale:eval
 * @text Ignore Scale?
 * @parent BreakShield
 * @type boolean
 * @on Ignore Scaling
 * @off Use Scaling
 * @desc Ignore scaling to show icons at their real size?
 * @default true
 *
 * @param BreakShieldOffsetX:num
 * @text Offset X
 * @parent BreakShield
 * @desc How much to offset the UI X position by?
 * Negative goes left. Positive goes right.
 * @default +20
 *
 * @param BreakShieldOffsetY:num
 * @text Offset Y
 * @parent BreakShield
 * @desc How much to offset the UI Y position by?
 * Negative goes up. Positive goes down.
 * @default +20
 * 
 * @param StateTooltips
 * @text State Tooltips
 * @parent Compatibility
 * @default VisuMZ_3_StateTooltips
 *
 * @param StateTooltipsShow:eval
 * @text Show?
 * @parent StateTooltips
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show this UI element?
 * Requires VisuMZ_3_StateTooltips!
 * @default true
 *
 * @param JS
 *
 * @param CustomUi:func
 * @text JS: Custom UI
 * @parent JS
 * @type note
 * @desc JavaScript used to add custom elements to each status window.
 * @default "// Declare Variables\nconst actor = arguments[0];\nlet x = 0;\nlet y = 0;\nlet width = this.innerWidth;\nlet height = this.innerHeight;\n\n// Draw Custom Elements\n// Put in code you want here used for windows classes"
 *
 */
//=============================================================================

const _0x2e56=['updateStatusWindowPosition','GeneralWindow','isAdjustBoostPoints','TpbOffsetY','HelpFadeStyle','EVAL','addWindow','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','colSpacing','sideview_ui','Window_ActorCommand_makeCommandList','BraveOffsetY','STATE_TOOLTIPS_SHOWN','length','WIDTH_MOVE','HpOffsetX','AGGRO_OFFSET_X','adjustSideviewUiHeight','STATES_REVERSE_SCALE','format','BoostOffsetX','placeStateIcon','BREAK_SHIELD_REVERSE_SCALE','Window_PartyCommand_initialize','innerRect','boxHeight','SIDEVIEW_BATTLE_UI_BATTLER_OFFSET_Y','adjustSideviewUiWidth','BattleLayout','SideviewBattleUI','BREAK_SHIELD_SHOWN','HP_GAUGE_OFFSET_Y','BOOST_OFFSET_Y','_data','STATES_OFFSET_X','SkillWindowMaxRows','Scene_Battle_createStatusWindow','version','placeGauge','ItemWindowOffsetX','initMembersSideviewUi','auto','NUM','dimColor1','_dimmerSprite','BreakShieldOffsetY','autoRowCount','CustomUi','isAdjustBravePoints','_sideviewUiBattleStatusWindows','maxCols','Window_PartyCommand_makeCommandList','refreshDimmerBitmap','allowBoostAction','dataSideviewUiLength','Window_SkillList_maxCols','Window_ItemList_makeItemList','updateSideviewBattleUIPositions','_requestRefresh','MpOffsetX','TP_GAUGE_OFFSET_Y','isTpb','BOOST_OFFSET_X','battleMembers','HpShow','scale','VisuMZ_2_BattleSystemCTB','TPB_SHOWN','isSideView','maxSideviewUiRows','SIDEVIEW_BATTLE_UI_WINDOW_WIDTH','237963SuXmWJ','updateSideviewUiFadeIn','createContents','MP_GAUGE_OFFSET_Y','SIDEVIEW_BATTLE_UI_WINDOW_MAX_ROWS','sideviewUiWidth','StateTooltipsShow','StatesOffsetY','description','139103CviStK','Scene_Battle_updateStatusWindowPosition','_targetX','HeightBuffer','SkillWindowWidth','ARRAYNUM','prototype','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','HP_GAUGE_SHOWN','1549msjiPc','AggroOffsetY','resize','_scene','TpOffsetX','setHome','MoveSpeed','toUpperCase','updateSideviewUiFadeOut','maxBattleMembers','Scene_Base_isWindowMaskingEnabled','AggroControlSystem','isStateTooltipTouched','map','BreakShieldOffsetX','refreshSideviewUiBattleStatusWindows','ARRAYEVAL','BRAVE_SHOWN','BRAVE_OFFSET_X','BraveShow','SIDEVIEW_BATTLE_UI_BATTLER_WINDOW_OFFSET_X','NAME_SHOWN','name','actor','battler','Scene_Battle_statusWindowRect','StatusWindow','push','SIDEVIEW_BATTLE_UI_BATTLER_WINDOW_OFFSET_Y','call','placeAggroGauge','Battler','createCancelButton','drawActorBravePoints','actorId','fillRect','Window_BattleStatus_updateRefresh','updateSideviewUiPosition','sideviewUiPositionOffsetX','min','setBackgroundType','padding','updateRefreshSideviewUi','STATES_OFFSET_Y','iconHeight','NameShow','drawCustomJS','parameters','includes','BreakShieldShow','filter','isActivePosition','Window_SkillList_makeItemList','round','SIDEVIEW_BATTLE_UI_BATTLER_OFFSET_X','updateRefresh','parse','_homeX','SIDEVIEW_BATTLE_UI_MOVE_BATTLERS','isWindowMaskingEnabled','HpOffsetY','TpbShow','isSceneBattle','BOOST_SHOWN','applyInverse','Scene_Battle_createCancelButton','NAME_OFFSET_Y','bitmap','createSideviewUiDimmerSprite','BattleCore','StatesOffsetX','_battler','_partyCommandWindow','Aggro','isUsingSideviewUiLayout','isBattleRefreshRequested','ItemWindowOffsetY','Window_SkillList_colSpacing','_skillWindow','isShowTpbGauge','sideviewUiTargetActor','battleLayoutStyle','220456cxvTSz','Window_Help_initialize','HP_GAUGE_OFFSET_X','_itemWindow','clearBattleRefreshRequest','height','ICON_SIZE_RATE','SIDEVIEW_BATTLE_UI_SCALE','HeightBase','create','update','placeBoostPoints','VisuMZ_1_BattleCore','Window_SkillList_initialize','TpbOffsetX','BoostShow','Window_ItemList_maxCols','visible','placeTimeGauge','isCTB','ceil','TPB_OFFSET_Y','SkillWindowOffsetY','updateBattler','BRAVE_OFFSET_Y','statusWindowRect','VisuMZ_2_AggroControlSystem','SIDEVIEW_BATTLE_UI_FADE_STYLE','fittingHeight','CommandWidth','PartyCommandWindowMaxRows','updatePosition','isShowAggro','aliveMembers','_battleField','max','Scene_Battle_actorWindowRect','VisuMZ_4_BreakShields','addChildToBack','gradientFillRect','406498UVxJaR','_partyIndex','HEIGHT_BUFFER','createStatusWindow','BREAK_SHIELD_OFFSET_Y','STR','placeBreakShieldIcon','SkillWindowOffsetX','_activeX','4174MkUUKK','getStateTooltipBattler','Enable','worldTransform','ConvertParams','Window_ItemList_colSpacing','MpShow','sideviewUiPositionOffsetY','opacity','HEIGHT_BASE','StatesShow','_actorCommandWindow','boxWidth','TpShow','NAME_OFFSET_X','dimColor2','active','_additionalSprites','MP_GAUGE_SHOWN','trim','AGGRO_SHOWN','Window_ActorCommand_initialize','BREAK_SHIELD_OFFSET_X','makeItemList','_actorWindow','_list','isBTB','123129zDDxic','isInputting','MoveDistance','createSideviewUiBattleStatusWindows','ARRAYJSON','AggroOffsetX','actorWindowRect','BreakShieldIgnoreScale','status','STRUCT','ItemWindowWidth','Game_System_isSideView','gaugeLineHeight','refresh','clampSideviewUiPlacementPosition','width','11QXaUrd','STATES_SHOWN','Settings','VisuMZ_3_BoostAction','updatePadding','match','70zftlxF','makeCommandList','TP_GAUGE_SHOWN','Sprite_Battler_setHome','3YkLICQ','constructor','initialize','TPB_OFFSET_X','clamp','createWindowRect','contains','Window_ItemList_initialize','MP_GAUGE_OFFSET_X'];const _0x36d8=function(_0x542376,_0x328a18){_0x542376=_0x542376-0xc8;let _0x2e5611=_0x2e56[_0x542376];return _0x2e5611;};const _0x2f592a=_0x36d8;(function(_0x17b33c,_0x3778af){const _0x392359=_0x36d8;while(!![]){try{const _0x59a453=-parseInt(_0x392359(0x113))*parseInt(_0x392359(0xf9))+parseInt(_0x392359(0x109))*-parseInt(_0x392359(0xde))+-parseInt(_0x392359(0x16c))+parseInt(_0x392359(0x163))+parseInt(_0x392359(0x10f))*-parseInt(_0x392359(0x175))+parseInt(_0x392359(0x1c7))+parseInt(_0x392359(0xd5));if(_0x59a453===_0x3778af)break;else _0x17b33c['push'](_0x17b33c['shift']());}catch(_0x118922){_0x17b33c['push'](_0x17b33c['shift']());}}}(_0x2e56,0x31563));var label=_0x2f592a(0x139),tier=tier||0x0,dependencies=[_0x2f592a(0x1d3)],pluginData=$plugins[_0x2f592a(0x1a7)](function(_0x683609){const _0x244d98=_0x2f592a;return _0x683609[_0x244d98(0x101)]&&_0x683609['description'][_0x244d98(0x1a5)]('['+label+']');})[0x0];VisuMZ[label][_0x2f592a(0x10b)]=VisuMZ[label][_0x2f592a(0x10b)]||{},VisuMZ[_0x2f592a(0xe2)]=function(_0x178888,_0x51bc68){const _0x59eb8a=_0x2f592a;for(const _0x33cede in _0x51bc68){if(_0x33cede[_0x59eb8a(0x10e)](/(.*):(.*)/i)){const _0x3c0913=String(RegExp['$1']),_0x5bed9b=String(RegExp['$2'])[_0x59eb8a(0x17c)]()[_0x59eb8a(0xf1)]();let _0x5a820c,_0x29c302,_0x9eb76a;switch(_0x5bed9b){case _0x59eb8a(0x146):_0x5a820c=_0x51bc68[_0x33cede]!==''?Number(_0x51bc68[_0x33cede]):0x0;break;case _0x59eb8a(0x171):_0x29c302=_0x51bc68[_0x33cede]!==''?JSON['parse'](_0x51bc68[_0x33cede]):[],_0x5a820c=_0x29c302[_0x59eb8a(0x182)](_0x331430=>Number(_0x331430));break;case _0x59eb8a(0x121):_0x5a820c=_0x51bc68[_0x33cede]!==''?eval(_0x51bc68[_0x33cede]):null;break;case _0x59eb8a(0x185):_0x29c302=_0x51bc68[_0x33cede]!==''?JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede]):[],_0x5a820c=_0x29c302[_0x59eb8a(0x182)](_0x5c6629=>eval(_0x5c6629));break;case'JSON':_0x5a820c=_0x51bc68[_0x33cede]!==''?JSON['parse'](_0x51bc68[_0x33cede]):'';break;case _0x59eb8a(0xfd):_0x29c302=_0x51bc68[_0x33cede]!==''?JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede]):[],_0x5a820c=_0x29c302[_0x59eb8a(0x182)](_0x28bc77=>JSON[_0x59eb8a(0x1ad)](_0x28bc77));break;case'FUNC':_0x5a820c=_0x51bc68[_0x33cede]!==''?new Function(JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede])):new Function('return\x200');break;case'ARRAYFUNC':_0x29c302=_0x51bc68[_0x33cede]!==''?JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede]):[],_0x5a820c=_0x29c302[_0x59eb8a(0x182)](_0x51cb75=>new Function(JSON['parse'](_0x51cb75)));break;case _0x59eb8a(0xda):_0x5a820c=_0x51bc68[_0x33cede]!==''?String(_0x51bc68[_0x33cede]):'';break;case'ARRAYSTR':_0x29c302=_0x51bc68[_0x33cede]!==''?JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede]):[],_0x5a820c=_0x29c302[_0x59eb8a(0x182)](_0x472d2b=>String(_0x472d2b));break;case _0x59eb8a(0x102):_0x9eb76a=_0x51bc68[_0x33cede]!==''?JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede]):{},_0x5a820c=VisuMZ[_0x59eb8a(0xe2)]({},_0x9eb76a);break;case'ARRAYSTRUCT':_0x29c302=_0x51bc68[_0x33cede]!==''?JSON[_0x59eb8a(0x1ad)](_0x51bc68[_0x33cede]):[],_0x5a820c=_0x29c302[_0x59eb8a(0x182)](_0x214120=>VisuMZ[_0x59eb8a(0xe2)]({},JSON[_0x59eb8a(0x1ad)](_0x214120)));break;default:continue;}_0x178888[_0x3c0913]=_0x5a820c;}}return _0x178888;},(_0x3d7ddd=>{const _0x30aff6=_0x2f592a,_0x295eee=_0x3d7ddd[_0x30aff6(0x18b)];for(const _0x19b75a of dependencies){if(!Imported[_0x19b75a]){alert(_0x30aff6(0x123)[_0x30aff6(0x12f)](_0x295eee,_0x19b75a)),SceneManager['exit']();break;}}const _0x3d2ed7=_0x3d7ddd[_0x30aff6(0x16b)];if(_0x3d2ed7[_0x30aff6(0x10e)](/\[Version[ ](.*?)\]/i)){const _0x6388fc=Number(RegExp['$1']);_0x6388fc!==VisuMZ[label][_0x30aff6(0x141)]&&(alert(_0x30aff6(0x173)[_0x30aff6(0x12f)](_0x295eee,_0x6388fc)),SceneManager['exit']());}if(_0x3d2ed7[_0x30aff6(0x10e)](/\[Tier[ ](\d+)\]/i)){const _0x24be24=Number(RegExp['$1']);_0x24be24<tier?(alert('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0x30aff6(0x12f)](_0x295eee,_0x24be24,tier)),SceneManager['exit']()):tier=Math['max'](_0x24be24,tier);}VisuMZ[_0x30aff6(0xe2)](VisuMZ[label][_0x30aff6(0x10b)],_0x3d7ddd[_0x30aff6(0x1a4)]);})(pluginData),BattleManager['isUsingSideviewUiLayout']=function(){const _0x32e596=_0x2f592a;return SceneManager[_0x32e596(0x1b3)]()&&SceneManager[_0x32e596(0x178)][_0x32e596(0x1c6)]()===_0x32e596(0x125);},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x104)]=Game_System[_0x2f592a(0x172)][_0x2f592a(0x160)],Game_System[_0x2f592a(0x172)][_0x2f592a(0x160)]=function(){const _0x2ed6a9=_0x2f592a;if(BattleManager[_0x2ed6a9(0x1bf)]())return!![];return VisuMZ['SideviewBattleUI'][_0x2ed6a9(0x104)][_0x2ed6a9(0x192)](this);},VisuMZ['SideviewBattleUI']['Scene_Base_isWindowMaskingEnabled']=Scene_Base[_0x2f592a(0x172)][_0x2f592a(0x1b0)],Scene_Base['prototype'][_0x2f592a(0x1b0)]=function(){const _0x1d2f32=_0x2f592a;return BattleManager[_0x1d2f32(0x1bf)]()?![]:VisuMZ[_0x1d2f32(0x139)][_0x1d2f32(0x17f)]['call'](this);},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x18e)]=Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0x1e0)],Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0x1e0)]=function(){const _0x20bf3f=_0x2f592a,_0xbbb0dc=VisuMZ[_0x20bf3f(0x139)]['Scene_Battle_statusWindowRect'][_0x20bf3f(0x192)](this);return BattleManager[_0x20bf3f(0x1bf)]()&&(_0xbbb0dc['y']=Graphics['height']*0xa,_0xbbb0dc['height']=0x0),_0xbbb0dc;},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0xd1)]=Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0xff)],Scene_Battle[_0x2f592a(0x172)]['actorWindowRect']=function(){const _0x2335dc=_0x2f592a,_0x247e29=VisuMZ[_0x2335dc(0x139)]['Scene_Battle_actorWindowRect'][_0x2335dc(0x192)](this);return BattleManager[_0x2335dc(0x1bf)]()&&(_0x247e29['y']=Graphics['height']*0xa,_0x247e29['height']=0x0),_0x247e29;},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x16d)]=Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0x11c)],Scene_Battle['prototype']['updateStatusWindowPosition']=function(){const _0x3fefe3=_0x2f592a;VisuMZ['SideviewBattleUI'][_0x3fefe3(0x16d)]['call'](this),this[_0x3fefe3(0x155)]();},Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0x155)]=function(){const _0x56b00c=_0x2f592a;if(!BattleManager[_0x56b00c(0xfa)]())return;if(!BattleManager[_0x56b00c(0x1bf)]())return;this['_partyCommandWindow'][_0x56b00c(0xee)]&&this[_0x56b00c(0x1bd)]['updateSideviewUiPosition'](),this[_0x56b00c(0xe9)][_0x56b00c(0xee)]&&this['_actorCommandWindow'][_0x56b00c(0x19a)](),this[_0x56b00c(0x1c3)][_0x56b00c(0xee)]&&(this[_0x56b00c(0xe9)]['updateSideviewUiPosition'](),this['_skillWindow'][_0x56b00c(0x19a)]()),this[_0x56b00c(0x1ca)]['active']&&(this[_0x56b00c(0xe9)][_0x56b00c(0x19a)](),this[_0x56b00c(0x1ca)]['updateSideviewUiPosition']()),this[_0x56b00c(0xf6)]['active']&&(this[_0x56b00c(0xe9)][_0x56b00c(0x17d)](),this[_0x56b00c(0x1c3)]['updateSideviewUiFadeOut'](),this['_itemWindow'][_0x56b00c(0x17d)]()),this['_enemyWindow']['active']&&(this[_0x56b00c(0xe9)][_0x56b00c(0x17d)](),this[_0x56b00c(0x1c3)]['updateSideviewUiFadeOut'](),this[_0x56b00c(0x1ca)][_0x56b00c(0x17d)]());},VisuMZ['SideviewBattleUI'][_0x2f592a(0x140)]=Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0xd8)],Scene_Battle['prototype'][_0x2f592a(0xd8)]=function(){const _0x500841=_0x2f592a;VisuMZ[_0x500841(0x139)]['Scene_Battle_createStatusWindow'][_0x500841(0x192)](this),this[_0x500841(0xfc)]();},Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0xfc)]=function(){const _0x133ffe=_0x2f592a;if(!BattleManager[_0x133ffe(0x1bf)]())return;this[_0x133ffe(0x14d)]=[];const _0x2c082a=$gameParty[_0x133ffe(0x17e)]();for(let _0x3b54c3=0x0;_0x3b54c3<_0x2c082a;_0x3b54c3++){const _0x563e42=new Window_SideviewUiBattleStatus(_0x3b54c3);this[_0x133ffe(0x122)](_0x563e42),this[_0x133ffe(0x14d)][_0x133ffe(0x190)](_0x563e42);}},Scene_Battle[_0x2f592a(0x172)]['refreshSideviewUiBattleStatusWindows']=function(){const _0x2858bc=_0x2f592a;if(!this[_0x2858bc(0x14d)])return;for(const _0x52992f of this['_sideviewUiBattleStatusWindows']){if(!_0x52992f)continue;_0x52992f[_0x2858bc(0x106)]();}},VisuMZ[_0x2f592a(0x139)]['Scene_Battle_createCancelButton']=Scene_Battle[_0x2f592a(0x172)][_0x2f592a(0x195)],Scene_Battle[_0x2f592a(0x172)]['createCancelButton']=function(){const _0x374488=_0x2f592a;if(BattleManager[_0x374488(0x1bf)]())return;VisuMZ[_0x374488(0x139)][_0x374488(0x1b6)]['call'](this);},Sprite_Battler[_0x2f592a(0x1af)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['Battler'][_0x2f592a(0xe0)]??!![],Sprite_Battler[_0x2f592a(0x1ab)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x194)]['OffsetX']??0x0,Sprite_Battler[_0x2f592a(0x136)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)][_0x2f592a(0x194)]['OffsetY']??0x80,VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x112)]=Sprite_Battler['prototype'][_0x2f592a(0x17a)],Sprite_Battler[_0x2f592a(0x172)]['setHome']=function(_0x1b17f8,_0x4d9fb9){const _0x38d24d=_0x2f592a;BattleManager[_0x38d24d(0x1bf)]()&&Sprite_Battler[_0x38d24d(0x1af)]&&(_0x1b17f8+=Sprite_Battler[_0x38d24d(0x1ab)],_0x4d9fb9+=Sprite_Battler[_0x38d24d(0x136)]),VisuMZ[_0x38d24d(0x139)][_0x38d24d(0x112)]['call'](this,_0x1b17f8,_0x4d9fb9);},Window_Base[_0x2f592a(0x1ce)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x11d)]['UiScale']??0.8,Window_Base[_0x2f592a(0x189)]=0x0,Window_Base[_0x2f592a(0x191)]=0x0,Window_Base[_0x2f592a(0x172)]['initMembersSideviewUi']=function(){const _0x2986a5=_0x2f592a;if(!this['isUsingSideviewUiLayout']())return;const _0x4e53b2=Window_Base[_0x2986a5(0x1ce)];this[_0x2986a5(0x15d)]['x']=this[_0x2986a5(0x15d)]['y']=_0x4e53b2;},Window_Base[_0x2f592a(0x172)]['isUsingSideviewUiLayout']=function(){return BattleManager['isUsingSideviewUiLayout']();},Window_Base['prototype'][_0x2f592a(0x107)]=function(){const _0x55b29d=_0x2f592a;if(!this[_0x55b29d(0x1bf)]())return;const _0x20e339=this[_0x55b29d(0x15d)]['x'],_0x4ff35f=-(Math['floor'](Graphics[_0x55b29d(0x108)]-Graphics[_0x55b29d(0xea)])/0x2),_0xc4fa80=_0x4ff35f+Graphics[_0x55b29d(0x108)]-Math['ceil'](this[_0x55b29d(0x108)]*_0x20e339),_0x34d2dc=-(Math['floor'](Graphics['height']-Graphics[_0x55b29d(0x135)])/0x2),_0x5276bd=_0x34d2dc+Graphics[_0x55b29d(0x1cc)]-Math['ceil'](this[_0x55b29d(0x1cc)]*_0x20e339);this['x']=this['x'][_0x55b29d(0x117)](_0x4ff35f,_0xc4fa80),this['y']=this['y'][_0x55b29d(0x117)](_0x34d2dc,_0x5276bd);},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x1c5)]=function(){const _0x31c0ac=_0x2f592a;return $gameParty[_0x31c0ac(0xce)]()[0x0];},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x19a)]=function(){const _0x1431dd=_0x2f592a;if(!this['isUsingSideviewUiLayout']())return;const _0x36e8c0=this[_0x1431dd(0x1c5)]();if(!_0x36e8c0)return;const _0x5c561d=_0x36e8c0['battler']();this['x']=_0x5c561d['x']+Math[_0x1431dd(0x1aa)](_0x5c561d[_0x1431dd(0x108)]/0x2),this['x']+=Math[_0x1431dd(0x1aa)]((Graphics[_0x1431dd(0x108)]-Graphics[_0x1431dd(0xea)])/0x2),this['x']+=SceneManager[_0x1431dd(0x178)]['_spriteset'][_0x1431dd(0xcf)]['x'],this['x']+=this['sideviewUiPositionOffsetX'](),this['y']=_0x5c561d['y']-_0x5c561d[_0x1431dd(0x1cc)],this['y']+=Math['round']((Graphics[_0x1431dd(0x1cc)]-Graphics[_0x1431dd(0x135)])/0x2),this['y']+=SceneManager[_0x1431dd(0x178)]['_spriteset'][_0x1431dd(0xcf)]['y'],this['y']+=this['sideviewUiPositionOffsetY'](),this[_0x1431dd(0x107)](),this[_0x1431dd(0x164)]();},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x19b)]=function(){const _0x17564d=_0x2f592a;return Window_Base[_0x17564d(0x189)];},Window_Base[_0x2f592a(0x172)][_0x2f592a(0xe5)]=function(){const _0x53e5ca=_0x2f592a;return Window_Base[_0x53e5ca(0x191)];},Window_Base[_0x2f592a(0x172)]['adjustSideviewUiWidth']=function(){const _0x508524=_0x2f592a;if(!this[_0x508524(0x1bf)]())return;const _0x249d97=this[_0x508524(0x108)];this[_0x508524(0x108)]=this[_0x508524(0x168)](),_0x249d97!==this['width']&&this[_0x508524(0x165)]();},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x168)]=function(){const _0x5a1d50=_0x2f592a;return VisuMZ[_0x5a1d50(0x1ba)][_0x5a1d50(0x10b)][_0x5a1d50(0x138)][_0x5a1d50(0xca)]||0xc0;},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x12d)]=function(){const _0x3110c3=_0x2f592a;if(!this['isUsingSideviewUiLayout']())return;const _0x4a0b77=this[_0x3110c3(0x1cc)],_0x346dc3=this[_0x3110c3(0x152)](),_0xa4677d=this[_0x3110c3(0xc9)](_0x346dc3),_0x190b56=this[_0x3110c3(0xc9)](this[_0x3110c3(0x161)]());this[_0x3110c3(0x1cc)]=Math[_0x3110c3(0x19c)](_0xa4677d,_0x190b56),_0x4a0b77!==this[_0x3110c3(0x1cc)]&&this['createContents']();},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x152)]=function(){const _0x31dc28=_0x2f592a;if(this[_0x31dc28(0x13d)])return this[_0x31dc28(0x13d)]['length'];if(this['_list'])return this[_0x31dc28(0xf7)][_0x31dc28(0x129)];return 0x4;},Window_Base['prototype'][_0x2f592a(0x161)]=function(){return 0x8;},Window_Base['prototype'][_0x2f592a(0x164)]=function(){this['visible']=!![];},Window_Base[_0x2f592a(0x172)][_0x2f592a(0x17d)]=function(){const _0x3c49bb=_0x2f592a;this[_0x3c49bb(0x1d8)]=![];},Window_Help[_0x2f592a(0xc8)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x11d)][_0x2f592a(0x120)]??!![],VisuMZ['SideviewBattleUI'][_0x2f592a(0x1c8)]=Window_Help[_0x2f592a(0x172)][_0x2f592a(0x115)],Window_Help[_0x2f592a(0x172)][_0x2f592a(0x115)]=function(_0x1026e4){const _0x43a23b=_0x2f592a;VisuMZ[_0x43a23b(0x139)][_0x43a23b(0x1c8)][_0x43a23b(0x192)](this,_0x1026e4),this[_0x43a23b(0x1b9)]();},Window_Help[_0x2f592a(0x172)]['createSideviewUiDimmerSprite']=function(){const _0x391262=_0x2f592a;if(!this[_0x391262(0x1bf)]())return;if(!Window_Help[_0x391262(0xc8)])return;this[_0x391262(0xe6)]=0x0;!this[_0x391262(0x148)]&&(this[_0x391262(0x148)]=new Sprite(),this[_0x391262(0xd3)](this['_dimmerSprite']));const _0x1f2f1d=this[_0x391262(0x108)]-Window_SideviewUiBattleStatus['WIDTH_BASE'],_0x13c173=this['lineHeight']()*0x2;this[_0x391262(0x148)][_0x391262(0x1b8)]=new Bitmap(_0x1f2f1d,_0x13c173),this[_0x391262(0x148)]['x']=-0x4,this['_dimmerSprite']['y']=this[_0x391262(0x19e)];const _0x3acd61=this[_0x391262(0x148)][_0x391262(0x1b8)],_0x1f91e0=ColorManager[_0x391262(0x147)](),_0xc15855=ColorManager[_0x391262(0xed)]();_0x3acd61[_0x391262(0x198)](0x0,0x0,Math['round'](_0x1f2f1d/0x2),_0x13c173,_0x1f91e0),_0x3acd61[_0x391262(0xd4)](Math[_0x391262(0x1aa)](_0x1f2f1d/0x2),0x0,Math[_0x391262(0x1aa)](_0x1f2f1d/0x2),_0x13c173,_0x1f91e0,_0xc15855);},Window_ItemList[_0x2f592a(0x167)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)]['ItemWindowMaxRows']??0x8,Window_ItemList[_0x2f592a(0x162)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)][_0x2f592a(0x103)]??0x190,Window_ItemList[_0x2f592a(0x189)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x11d)][_0x2f592a(0x143)]??0x10,Window_ItemList[_0x2f592a(0x191)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)][_0x2f592a(0x1c1)]??0x10,VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x11a)]=Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x115)],Window_ItemList[_0x2f592a(0x172)]['initialize']=function(_0xc36274){const _0x3c6ead=_0x2f592a;VisuMZ['SideviewBattleUI'][_0x3c6ead(0x11a)]['call'](this,_0xc36274),this[_0x3c6ead(0x144)]();},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x1d7)]=Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x14e)],Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x14e)]=function(){const _0x6cdb58=_0x2f592a;return this['isUsingSideviewUiLayout']()?0x1:VisuMZ['SideviewBattleUI'][_0x6cdb58(0x1d7)][_0x6cdb58(0x192)](this);},VisuMZ['SideviewBattleUI']['Window_ItemList_colSpacing']=Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x124)],Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x124)]=function(){const _0x61626d=_0x2f592a;return this[_0x61626d(0x1bf)]()?0x0:VisuMZ[_0x61626d(0x139)][_0x61626d(0xe3)]['call'](this);},VisuMZ['SideviewBattleUI']['Window_ItemList_makeItemList']=Window_ItemList['prototype'][_0x2f592a(0xf5)],Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0xf5)]=function(){const _0x4e13d4=_0x2f592a;VisuMZ[_0x4e13d4(0x139)][_0x4e13d4(0x154)][_0x4e13d4(0x192)](this),this[_0x4e13d4(0x137)](),this['adjustSideviewUiHeight'](),this['updateSideviewUiPosition']();},Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x1c5)]=function(){const _0x813711=_0x2f592a;return this['_actor']||Window_Base[_0x813711(0x172)]['sideviewUiTargetActor'][_0x813711(0x192)](this);},Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x168)]=function(){const _0x1cddcb=_0x2f592a;return Window_ItemList[_0x1cddcb(0x162)]||0xc0;},Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0x19b)]=function(){const _0x718e2=_0x2f592a;let _0x5c9554=Window_Selectable[_0x718e2(0x172)][_0x718e2(0x19b)][_0x718e2(0x192)](this);return _0x5c9554+Window_ItemList[_0x718e2(0x189)];},Window_ItemList[_0x2f592a(0x172)][_0x2f592a(0xe5)]=function(){const _0x31b8b0=_0x2f592a;let _0x2cc33e=Window_Selectable['prototype']['sideviewUiPositionOffsetY'][_0x31b8b0(0x192)](this);return _0x2cc33e+Window_ItemList[_0x31b8b0(0x191)];},Window_SkillList[_0x2f592a(0x167)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)][_0x2f592a(0x11d)][_0x2f592a(0x13f)]??0x8,Window_SkillList[_0x2f592a(0x162)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)][_0x2f592a(0x170)]??0x190,Window_SkillList[_0x2f592a(0x189)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)][_0x2f592a(0xdc)]??0x10,Window_SkillList['SIDEVIEW_BATTLE_UI_BATTLER_WINDOW_OFFSET_Y']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)][_0x2f592a(0x1dd)]??0x10,VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x1d4)]=Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x115)],Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x115)]=function(_0xdd46e6){const _0x2917b4=_0x2f592a;VisuMZ['SideviewBattleUI']['Window_SkillList_initialize'][_0x2917b4(0x192)](this,_0xdd46e6),this[_0x2917b4(0x144)]();},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x153)]=Window_SkillList['prototype'][_0x2f592a(0x14e)],Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x14e)]=function(){const _0x8bf1e1=_0x2f592a;return this['isUsingSideviewUiLayout']()?0x1:VisuMZ[_0x8bf1e1(0x139)]['Window_SkillList_maxCols'][_0x8bf1e1(0x192)](this);},VisuMZ['SideviewBattleUI'][_0x2f592a(0x1c2)]=Window_SkillList['prototype'][_0x2f592a(0x124)],Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x124)]=function(){const _0x19eb0a=_0x2f592a;return this['isUsingSideviewUiLayout']()?0x0:VisuMZ[_0x19eb0a(0x139)][_0x19eb0a(0x1c2)][_0x19eb0a(0x192)](this);},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x1a9)]=Window_SkillList['prototype'][_0x2f592a(0xf5)],Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0xf5)]=function(){const _0x9dd312=_0x2f592a;VisuMZ[_0x9dd312(0x139)][_0x9dd312(0x1a9)][_0x9dd312(0x192)](this),this[_0x9dd312(0x137)](),this[_0x9dd312(0x12d)](),this[_0x9dd312(0x19a)]();},Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x1c5)]=function(){const _0x55101c=_0x2f592a;return this['_actor']||Window_Base[_0x55101c(0x172)][_0x55101c(0x1c5)][_0x55101c(0x192)](this);},Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x168)]=function(){const _0x5252d2=_0x2f592a;return Window_SkillList[_0x5252d2(0x162)]||0xc0;},Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0x19b)]=function(){const _0x134483=_0x2f592a;let _0x39ab3c=Window_Selectable[_0x134483(0x172)]['sideviewUiPositionOffsetX'][_0x134483(0x192)](this);return _0x39ab3c+Window_SkillList[_0x134483(0x189)];},Window_SkillList[_0x2f592a(0x172)][_0x2f592a(0xe5)]=function(){const _0x5c11d9=_0x2f592a;let _0x1c7926=Window_Selectable[_0x5c11d9(0x172)]['sideviewUiPositionOffsetY'][_0x5c11d9(0x192)](this);return _0x1c7926+Window_SkillList['SIDEVIEW_BATTLE_UI_BATTLER_WINDOW_OFFSET_Y'];},Window_PartyCommand[_0x2f592a(0x167)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['GeneralWindow'][_0x2f592a(0xcb)]??0x8,VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x133)]=Window_PartyCommand['prototype'][_0x2f592a(0x115)],Window_PartyCommand[_0x2f592a(0x172)][_0x2f592a(0x115)]=function(_0x5cb980){const _0xef4fc8=_0x2f592a;VisuMZ['SideviewBattleUI'][_0xef4fc8(0x133)][_0xef4fc8(0x192)](this,_0x5cb980),this[_0xef4fc8(0x144)]();},VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x14f)]=Window_PartyCommand[_0x2f592a(0x172)][_0x2f592a(0x110)],Window_PartyCommand['prototype'][_0x2f592a(0x110)]=function(){const _0x32e659=_0x2f592a;VisuMZ['SideviewBattleUI'][_0x32e659(0x14f)]['call'](this),this['adjustSideviewUiWidth'](),this['adjustSideviewUiHeight']();},Window_PartyCommand[_0x2f592a(0x172)][_0x2f592a(0x1c5)]=function(){const _0xec2aa7=_0x2f592a;return $gameParty[_0xec2aa7(0xce)]()[0x0];},Window_PartyCommand[_0x2f592a(0x172)][_0x2f592a(0x161)]=function(){return Window_PartyCommand['SIDEVIEW_BATTLE_UI_WINDOW_MAX_ROWS'];},Window_ActorCommand[_0x2f592a(0x167)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x11d)]['ActorCommandWindowMaxRows']??0x8,VisuMZ[_0x2f592a(0x139)][_0x2f592a(0xf3)]=Window_ActorCommand[_0x2f592a(0x172)][_0x2f592a(0x115)],Window_ActorCommand[_0x2f592a(0x172)][_0x2f592a(0x115)]=function(_0x2f295d){const _0x262762=_0x2f592a;VisuMZ[_0x262762(0x139)][_0x262762(0xf3)][_0x262762(0x192)](this,_0x2f295d),this[_0x262762(0x144)]();},VisuMZ['SideviewBattleUI'][_0x2f592a(0x126)]=Window_ActorCommand['prototype'][_0x2f592a(0x110)],Window_ActorCommand['prototype']['makeCommandList']=function(){const _0x29eb17=_0x2f592a;VisuMZ[_0x29eb17(0x139)][_0x29eb17(0x126)]['call'](this),this[_0x29eb17(0x137)](),this['adjustSideviewUiHeight'](),this['updateSideviewUiPosition']();},Window_ActorCommand[_0x2f592a(0x172)][_0x2f592a(0x1c5)]=function(){const _0x5eaabe=_0x2f592a;return this['_actor']||Window_Base[_0x5eaabe(0x172)]['sideviewUiTargetActor'][_0x5eaabe(0x192)](this);},Window_ActorCommand[_0x2f592a(0x172)]['maxSideviewUiRows']=function(){const _0x2ba35c=_0x2f592a;return Window_ActorCommand[_0x2ba35c(0x167)];},VisuMZ[_0x2f592a(0x139)]['Window_BattleStatus_updateRefresh']=Window_BattleStatus[_0x2f592a(0x172)][_0x2f592a(0x1ac)],Window_BattleStatus['prototype']['updateRefresh']=function(){const _0x155c48=_0x2f592a;this[_0x155c48(0x1bf)]()?this[_0x155c48(0x19f)]():VisuMZ['SideviewBattleUI'][_0x155c48(0x199)][_0x155c48(0x192)](this);},Window_BattleStatus[_0x2f592a(0x172)][_0x2f592a(0x19f)]=function(){const _0x5b1031=_0x2f592a;if($gameTemp[_0x5b1031(0x1c0)]())this['_requestRefresh']=![],$gameTemp[_0x5b1031(0x1cb)](),SceneManager[_0x5b1031(0x178)][_0x5b1031(0x184)]();else this[_0x5b1031(0x156)]&&(this[_0x5b1031(0x156)]=![],SceneManager[_0x5b1031(0x178)]['refreshSideviewUiBattleStatusWindows']());};function Window_SideviewUiBattleStatus(){const _0x4207d2=_0x2f592a;this[_0x4207d2(0x115)](...arguments);}Window_SideviewUiBattleStatus[_0x2f592a(0x172)]=Object[_0x2f592a(0x1d0)](Window_StatusBase[_0x2f592a(0x172)]),Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x114)]=Window_SideviewUiBattleStatus,Window_SideviewUiBattleStatus['WIDTH_BASE']=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)]['WidthBase']??0xc8,Window_SideviewUiBattleStatus[_0x2f592a(0xe7)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x1cf)]??_0x2f592a(0x145),Window_SideviewUiBattleStatus['HEIGHT_BUFFER']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x16f)]??0x4,Window_SideviewUiBattleStatus[_0x2f592a(0x12a)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0xfb)]??0x30,Window_SideviewUiBattleStatus['MOVE_SPEED']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x17b)]??0x4,Window_SideviewUiBattleStatus[_0x2f592a(0x18a)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['StatusWindow'][_0x2f592a(0x1a2)]??!![],Window_SideviewUiBattleStatus['NAME_OFFSET_X']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['StatusWindow']['NameOffsetX']??0x30,Window_SideviewUiBattleStatus[_0x2f592a(0x1b7)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['StatusWindow']['NameOffsetY']??0x0,Window_SideviewUiBattleStatus[_0x2f592a(0x10a)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0xe8)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x12e)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)]['StatesIgnoreScale']??!![],Window_SideviewUiBattleStatus['STATES_OFFSET_X']=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x1bb)]??0x14,Window_SideviewUiBattleStatus[_0x2f592a(0x1a0)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x16a)]??0x14,Window_SideviewUiBattleStatus[_0x2f592a(0x15f)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)]['StatusWindow'][_0x2f592a(0x1b2)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x116)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x1d5)]??0x2c,Window_SideviewUiBattleStatus['TPB_OFFSET_Y']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x11f)]??0x0,Window_SideviewUiBattleStatus[_0x2f592a(0x174)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x15c)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x1c9)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)][_0x2f592a(0x12b)]??0x3c,Window_SideviewUiBattleStatus['HP_GAUGE_OFFSET_Y']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x1b1)]??0x0,Window_SideviewUiBattleStatus['MP_GAUGE_SHOWN']=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)][_0x2f592a(0xe4)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x11b)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['StatusWindow'][_0x2f592a(0x157)]??0x44,Window_SideviewUiBattleStatus[_0x2f592a(0x166)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)][_0x2f592a(0x18f)]['MpOffsetY']??0x0,Window_SideviewUiBattleStatus[_0x2f592a(0x111)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0xeb)]??!![],Window_SideviewUiBattleStatus['TP_GAUGE_OFFSET_X']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x179)]??0x4a,Window_SideviewUiBattleStatus[_0x2f592a(0x158)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)]['TpOffsetY']??0x0,Window_SideviewUiBattleStatus['AGGRO_SHOWN']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)]['AggroShow']??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x12c)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0xfe)]??0x2c,Window_SideviewUiBattleStatus['AGGRO_OFFSET_Y']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x176)]??0x0,Window_SideviewUiBattleStatus[_0x2f592a(0x1b4)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x1d6)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x15a)]=VisuMZ['SideviewBattleUI']['Settings'][_0x2f592a(0x18f)][_0x2f592a(0x130)]??0x34,Window_SideviewUiBattleStatus[_0x2f592a(0x13c)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)]['StatusWindow']['BoostOffsetY']??0x2,Window_SideviewUiBattleStatus[_0x2f592a(0x186)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x188)]??!![],Window_SideviewUiBattleStatus['BRAVE_OFFSET_X']=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)]['BraveOffsetX']??0x34,Window_SideviewUiBattleStatus['BRAVE_OFFSET_Y']=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)][_0x2f592a(0x127)]??-0x6,Window_SideviewUiBattleStatus[_0x2f592a(0x13a)]=VisuMZ[_0x2f592a(0x139)][_0x2f592a(0x10b)][_0x2f592a(0x18f)][_0x2f592a(0x1a6)]??!![],Window_SideviewUiBattleStatus['BREAK_SHIELD_REVERSE_SCALE']=VisuMZ[_0x2f592a(0x139)]['Settings']['StatusWindow'][_0x2f592a(0x100)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0xf4)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)][_0x2f592a(0x183)]??0x14,Window_SideviewUiBattleStatus[_0x2f592a(0xd9)]=VisuMZ['SideviewBattleUI'][_0x2f592a(0x10b)]['StatusWindow'][_0x2f592a(0x149)]??0x14,Window_SideviewUiBattleStatus[_0x2f592a(0x128)]=VisuMZ[_0x2f592a(0x139)]['Settings'][_0x2f592a(0x18f)][_0x2f592a(0x169)]??!![],Window_SideviewUiBattleStatus[_0x2f592a(0x172)]['initialize']=function(_0x5713ae){const _0x211d03=_0x2f592a;this['_partyIndex']=_0x5713ae;const _0x4566d7=this['createWindowRect']();Window_StatusBase[_0x211d03(0x172)][_0x211d03(0x115)][_0x211d03(0x192)](this,_0x4566d7),this[_0x211d03(0x144)](),this[_0x211d03(0x19d)](0x2);},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x118)]=function(){const _0x530a82=_0x2f592a,_0xa8467f=Window_Base[_0x530a82(0x1ce)];let _0x4509d6=Window_SideviewUiBattleStatus['WIDTH_BASE'],_0x2e0a68=Graphics['boxWidth']-_0x4509d6;_0x2e0a68+=Math[_0x530a82(0x1db)]((Graphics[_0x530a82(0x108)]-Graphics[_0x530a82(0xea)])/0x2),_0x4509d6/=_0xa8467f,_0x4509d6=Math[_0x530a82(0x1db)](_0x4509d6),_0x4509d6+=Math[_0x530a82(0x1db)](Window_SideviewUiBattleStatus[_0x530a82(0x12a)]*0x4/_0xa8467f);let _0xe06ccf=Window_SideviewUiBattleStatus[_0x530a82(0xe7)];_0xe06ccf==='auto'?(_0xe06ccf=Window_SideviewUiBattleStatus['HEIGHT_BUFFER']*0x2,_0xe06ccf+=this[_0x530a82(0x105)]()*this['autoRowCount'](),_0xe06ccf=Math[_0x530a82(0x1db)](_0xe06ccf*_0xa8467f),_0xe06ccf/=_0xa8467f):_0xe06ccf=eval(_0xe06ccf)||0x0;let _0x4d9998=Math[_0x530a82(0x1db)](_0xe06ccf*_0xa8467f)*this[_0x530a82(0xd6)];return _0x4d9998-=Math[_0x530a82(0x1db)]((Graphics[_0x530a82(0x1cc)]-Graphics[_0x530a82(0x135)])/0x2),this[_0x530a82(0x1ae)]=_0x2e0a68,this['_activeX']=this[_0x530a82(0x1ae)]-Math[_0x530a82(0x1db)](Window_SideviewUiBattleStatus['WIDTH_MOVE']/_0xa8467f),this['_targetX']=this[_0x530a82(0x1ae)],new Rectangle(_0x2e0a68,_0x4d9998,_0x4509d6,_0xe06ccf);},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x14a)]=function(){const _0x138a49=_0x2f592a;let _0x199045=0x0;if(Window_SideviewUiBattleStatus[_0x138a49(0x18a)])_0x199045+=0x1;if(Window_SideviewUiBattleStatus[_0x138a49(0x174)])_0x199045+=0x1;if(Window_SideviewUiBattleStatus[_0x138a49(0xf0)])_0x199045+=0x1;if(Window_SideviewUiBattleStatus[_0x138a49(0x111)])_0x199045+=0x1;if(this['isAdjustBoostPoints']())_0x199045+=0x1;if(this['isAdjustBravePoints']())_0x199045+=0x1;return _0x199045||0x1;},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x10d)]=function(){const _0x33c802=_0x2f592a;this[_0x33c802(0x19e)]=0x0;},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x150)]=function(){const _0x347437=_0x2f592a;if(!this[_0x347437(0x148)])return;const _0x49dacf=this['_dimmerSprite'][_0x347437(0x1b8)];var _0x1b1b41=ColorManager['dimColor1'](),_0x4e8c8e=ColorManager['dimColor2'](),_0x1180ca=Math['ceil'](this[_0x347437(0x108)]/0x4),_0x3d3e71=this[_0x347437(0x108)]-_0x1180ca,_0x1bba5e=this[_0x347437(0x1cc)];_0x49dacf[_0x347437(0x177)](this[_0x347437(0x108)],_0x1bba5e),_0x49dacf[_0x347437(0xd4)](0x0,0x0,_0x1180ca,_0x1bba5e,_0x4e8c8e,_0x1b1b41),_0x49dacf[_0x347437(0x198)](_0x1180ca,0x0,_0x3d3e71,_0x1bba5e,_0x1b1b41),this[_0x347437(0x148)]['setFrame'](0x0,0x0,_0x3d3e71,_0x1bba5e);},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x1d1)]=function(){const _0x54b7e7=_0x2f592a;Window_StatusBase[_0x54b7e7(0x172)][_0x54b7e7(0x1d1)][_0x54b7e7(0x192)](this),this[_0x54b7e7(0x1de)](),this[_0x54b7e7(0xcc)]();},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x18d)]=function(){const _0x5a479b=_0x2f592a;return $gameParty[_0x5a479b(0x15b)]()[this[_0x5a479b(0xd6)]];},Window_SideviewUiBattleStatus['prototype'][_0x2f592a(0x1de)]=function(){const _0x2f5b6=_0x2f592a;if(this['_battler']===this[_0x2f5b6(0x18d)]())return;this[_0x2f5b6(0x1bc)]=this[_0x2f5b6(0x18d)](),this[_0x2f5b6(0x106)](),this[_0x2f5b6(0x1bc)]?this[_0x2f5b6(0x19d)](0x1):this[_0x2f5b6(0x19d)](0x2);},Window_SideviewUiBattleStatus['prototype'][_0x2f592a(0xcc)]=function(){const _0x33b7ec=_0x2f592a;if(!this[_0x33b7ec(0x1bc)])return;this[_0x33b7ec(0x16e)]=this[_0x33b7ec(0x1a8)]()?this[_0x33b7ec(0xdd)]:this[_0x33b7ec(0x1ae)];const _0x718e73=Window_SideviewUiBattleStatus['MOVE_SPEED'];if(this[_0x33b7ec(0x16e)]>this['x'])this['x']=Math[_0x33b7ec(0x19c)](this['x']+_0x718e73,this['_targetX']);else this[_0x33b7ec(0x16e)]<this['x']&&(this['x']=Math[_0x33b7ec(0xd0)](this['x']-_0x718e73,this[_0x33b7ec(0x16e)]));},Window_SideviewUiBattleStatus[_0x2f592a(0x172)]['isActivePosition']=function(){const _0x1980f7=_0x2f592a;if(this[_0x1980f7(0x1bc)]===BattleManager[_0x1980f7(0x18c)]())return!![];if(this[_0x1980f7(0x1bc)]===BattleManager['_subject'])return!![];if(this[_0x1980f7(0x1bc)]['isSelected']())return!![];return![];},Window_SideviewUiBattleStatus[_0x2f592a(0x172)]['isStateTooltipEnabled']=function(){const _0x13c25e=_0x2f592a;return Window_SideviewUiBattleStatus[_0x13c25e(0x128)];},Window_SideviewUiBattleStatus['prototype'][_0x2f592a(0xdf)]=function(){const _0x4eb579=_0x2f592a;return this[_0x4eb579(0x1bc)];},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x181)]=function(){const _0x2850a2=_0x2f592a,_0x52a581=new Point(TouchInput['x'],TouchInput['y']),_0x2c2cae=this[_0x2850a2(0xe1)][_0x2850a2(0x1b5)](_0x52a581);return this[_0x2850a2(0x134)][_0x2850a2(0x119)](_0x2c2cae['x'],_0x2c2cae['y']);},Window_SideviewUiBattleStatus['prototype']['drawAllItems']=function(){const _0x14c9b6=_0x2f592a;this['hideAdditionalSprites']();if(!this['_battler'])return;this['drawBasicStatus'](),this[_0x14c9b6(0x1a3)]();},Window_SideviewUiBattleStatus[_0x2f592a(0x172)]['drawBasicStatus']=function(){const _0x12e703=_0x2f592a,_0x3c04ba=this[_0x12e703(0x1bc)];let _0xc86211=0x4,_0x54b136=Window_SideviewUiBattleStatus[_0x12e703(0xd7)];if(Imported[_0x12e703(0xd2)]&&Window_SideviewUiBattleStatus['BREAK_SHIELD_SHOWN']){let _0x627d1a=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0xf4)],_0x1cb9ff=_0x54b136+Window_SideviewUiBattleStatus[_0x12e703(0xd9)];this[_0x12e703(0xdb)](_0x3c04ba,_0x627d1a,_0x1cb9ff);if(Window_SideviewUiBattleStatus[_0x12e703(0x12e)]){const _0x2fc97f='actor%1-breakShieldIcon'[_0x12e703(0x12f)](_0x3c04ba['actorId']()),_0x330b69=this[_0x12e703(0xef)];if(_0x330b69[_0x2fc97f]){const _0x23a58f=_0x330b69[_0x2fc97f];_0x23a58f[_0x12e703(0x15d)]['x']=_0x23a58f[_0x12e703(0x15d)]['y']=0x1/this['scale']['y'];};}}if(Window_SideviewUiBattleStatus[_0x12e703(0x10a)]){let _0x568835=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x13e)],_0xd9e6bc=_0x54b136+Window_SideviewUiBattleStatus['STATES_OFFSET_Y'];Imported[_0x12e703(0xd2)]&&Window_SideviewUiBattleStatus[_0x12e703(0x13a)]&&(Window_SideviewUiBattleStatus[_0x12e703(0x132)]?_0xd9e6bc+=Math[_0x12e703(0x1db)](ImageManager[_0x12e703(0x1a1)]/this[_0x12e703(0x15d)]['y']):_0xd9e6bc+=ImageManager[_0x12e703(0x1a1)],_0xd9e6bc+=0x4);this[_0x12e703(0x131)](_0x3c04ba,_0x568835,_0xd9e6bc);if(Window_SideviewUiBattleStatus[_0x12e703(0x12e)]){const _0x18affa='actor%1-stateIcon'[_0x12e703(0x12f)](_0x3c04ba[_0x12e703(0x197)]()),_0x4ba260=this['_additionalSprites'];if(_0x4ba260[_0x18affa]){const _0x161ed6=_0x4ba260[_0x18affa];_0x161ed6['scale']['x']=_0x161ed6[_0x12e703(0x15d)]['y']=0x1/this['scale']['y'];};}}if(this[_0x12e703(0x1c4)]()){let _0x1dece9=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x116)],_0x13d12c=_0x54b136+Window_SideviewUiBattleStatus[_0x12e703(0x1dc)];this[_0x12e703(0x1d9)](_0x3c04ba,_0x1dece9,_0x13d12c);}if(this[_0x12e703(0xcd)]()){let _0x50a87f=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x12c)],_0xadeb11=_0x54b136+Window_SideviewUiBattleStatus['AGGRO_OFFSET_Y'];this[_0x12e703(0x1c4)]()&&(_0xadeb11-=Sprite_Gauge[_0x12e703(0x172)]['gaugeHeight']()-0x1),this[_0x12e703(0x193)](_0x3c04ba,_0x50a87f,_0xadeb11);}if(Window_SideviewUiBattleStatus[_0x12e703(0x18a)]){let _0x53e691=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0xec)],_0x3f6445=_0x54b136+Window_SideviewUiBattleStatus[_0x12e703(0x1b7)];this['placeActorName'](_0x3c04ba,_0x53e691,_0x3f6445);}(Window_SideviewUiBattleStatus[_0x12e703(0x18a)]||this[_0x12e703(0x1c4)]()||this[_0x12e703(0xcd)]())&&(_0x54b136+=this[_0x12e703(0x105)]());if(this[_0x12e703(0x11e)]()){const _0x4777d1=Math['ceil'](ImageManager[_0x12e703(0x1a1)]*Sprite_BoostContainer[_0x12e703(0x1cd)]);let _0xe449fb=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x15a)],_0x7e03d5=_0x54b136+Window_SideviewUiBattleStatus[_0x12e703(0x13c)];_0x7e03d5+=Math[_0x12e703(0xd0)](0x0,Math['round']((this['gaugeLineHeight']()-_0x4777d1)/0x2)),this[_0x12e703(0x1d2)](_0x3c04ba,_0xe449fb,_0x7e03d5),_0x54b136+=this[_0x12e703(0x105)]();}if(this[_0x12e703(0x14c)]()){let _0x3a9ab4=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x187)],_0x4a1cb1=_0x54b136+Window_SideviewUiBattleStatus[_0x12e703(0x1df)],_0x4304c6=Math[_0x12e703(0x1db)](Window_SideviewUiBattleStatus['WIDTH_BASE']/this[_0x12e703(0x15d)]['x']);this[_0x12e703(0x196)](_0x3c04ba,_0x3a9ab4,_0x4a1cb1,_0x4304c6,'left'),_0x54b136+=this[_0x12e703(0x105)]();}if(Window_SideviewUiBattleStatus[_0x12e703(0x174)]){let _0x3aa2cf=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x1c9)],_0xf27c56=_0x54b136+Window_SideviewUiBattleStatus[_0x12e703(0x13b)];this[_0x12e703(0x142)](_0x3c04ba,'hp',_0x3aa2cf,_0xf27c56),_0x54b136+=this[_0x12e703(0x105)]();}if(Window_SideviewUiBattleStatus[_0x12e703(0xf0)]){let _0x229ba5=_0xc86211+Window_SideviewUiBattleStatus[_0x12e703(0x11b)],_0x31e69e=_0x54b136+Window_SideviewUiBattleStatus['MP_GAUGE_OFFSET_Y'];this[_0x12e703(0x142)](_0x3c04ba,'mp',_0x229ba5,_0x31e69e),_0x54b136+=this['gaugeLineHeight']();}if(Window_SideviewUiBattleStatus[_0x12e703(0x111)]){let _0x192eaa=_0xc86211+Window_SideviewUiBattleStatus['TP_GAUGE_OFFSET_X'],_0x110c10=_0x54b136+Window_SideviewUiBattleStatus['TP_GAUGE_OFFSET_Y'];this[_0x12e703(0x142)](_0x3c04ba,'tp',_0x192eaa,_0x110c10),_0x54b136+=this[_0x12e703(0x105)]();}},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x1c4)]=function(){const _0x43673a=_0x2f592a;if(Imported[_0x43673a(0x15e)]&&BattleManager[_0x43673a(0x1da)]())return![];return BattleManager[_0x43673a(0x159)]()&&Window_SideviewUiBattleStatus[_0x43673a(0x18a)]&&Window_SideviewUiBattleStatus[_0x43673a(0x15f)];},Window_SideviewUiBattleStatus[_0x2f592a(0x172)]['isShowAggro']=function(){const _0x597427=_0x2f592a;return Window_SideviewUiBattleStatus[_0x597427(0x18a)]&&Window_SideviewUiBattleStatus[_0x597427(0xf2)]&&Imported[_0x597427(0x1e1)]&&ConfigManager['aggroGauge']&&VisuMZ[_0x597427(0x180)]['Settings'][_0x597427(0x1be)]['StatusGauge'];},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x11e)]=function(){const _0x45b2c6=_0x2f592a;return Imported[_0x45b2c6(0x10c)]&&Window_SideviewUiBattleStatus[_0x45b2c6(0x1b4)]&&BattleManager[_0x45b2c6(0x151)]();},Window_SideviewUiBattleStatus['prototype']['isAdjustBravePoints']=function(){const _0x32ce0f=_0x2f592a;return Imported['VisuMZ_2_BattleSystemBTB']&&Window_SideviewUiBattleStatus[_0x32ce0f(0x186)]&&BattleManager[_0x32ce0f(0xf8)]();},Window_SideviewUiBattleStatus[_0x2f592a(0x172)][_0x2f592a(0x1a3)]=function(){const _0x58efad=_0x2f592a;VisuMZ[_0x58efad(0x139)][_0x58efad(0x10b)][_0x58efad(0x18f)][_0x58efad(0x14b)]&&VisuMZ[_0x58efad(0x139)][_0x58efad(0x10b)][_0x58efad(0x18f)][_0x58efad(0x14b)]['call'](this,this[_0x58efad(0x1bc)]);};