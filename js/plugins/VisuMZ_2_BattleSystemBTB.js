//=============================================================================
// VisuStella MZ - Battle System BTB - Brave Turn Battle
// VisuMZ_2_BattleSystemBTB.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_BattleSystemBTB = true;

var VisuMZ = VisuMZ || {};
VisuMZ.BattleSystemBTB = VisuMZ.BattleSystemBTB || {};
VisuMZ.BattleSystemBTB.version = 1.06;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.06] [BattleSystemBTB]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Battle_System_-_BTB_VisuStella_MZ
 * @base VisuMZ_0_CoreEngine
 * @base VisuMZ_1_BattleCore
 * @base VisuMZ_1_ItemsEquipsCore
 * @base VisuMZ_1_SkillsStatesCore
 * @orderAfter VisuMZ_1_BattleCore
 * @orderAfter VisuMZ_1_ItemsEquipsCore
 * @orderAfter VisuMZ_1_SkillsStatesCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * The Brave Turn Battle (BTB) system plays off RPG Maker MZ's default battle
 * system with a twist of allowing actors (and enemies) to use up actions from
 * the future or save up for later. These actions will be queued and delivered
 * all in one go! Any borrowed actions from the future will result in following
 * turns without any actions to use. Should a player decide to save up their
 * actions instead through Guarding, they can charge actions with less
 * repercussions. Players will have to be brave about how to go about the
 * battle system strategically.
 * 
 * Because multiple actions can be queued up all at once, they can result in
 * the creation of an action fusion. Some skills (and items) can appear instead
 * of the originally queued actions to result in stronger, better, and more
 * awesome effects, all of which, can be defined by the game dev.
 * 
 * A Turn Order Display will also appear on the screen to show the order the
 * battlers will take their turns in. This lets the player plan in advance on
 * how to go about the rest of the turn.
 * 
 * *NOTE* To use this battle system, you will need the updated version of
 * VisuStella's Core Engine. Go into its Plugin Parameters and change the
 * "Battle System" plugin parameter to "btb".
 *
 * Features include all (but not limited to) the following:
 * 
 * * Puts a twist on the Default Turn Battle system by allowing brave players
 *   to borrow actions from the future turns or save them up for later turns.
 * * Brave Points, a new currency, are added to mark how many saved turns there
 *   are for each battler.
 * * Certain actions can cost more Brave Points than others.
 * * Effects that allow battlers to alter the Brave Points of their targets.
 * * A Turn Order Display to show the player when each battler will have its
 *   turn to perform an action.
 * * Action fusion system which takes any of the queued up skills and/or items
 *   to bring forth new ones.
 * * Action fusion combinations can be either flexible or strict.
 * * Flexible action fusion combinations can have their actions queued up in
 *   any order to bring forth the result.
 * * Strict action fusion combinations must require their actions to be queued
 *   up in a specific order in order to bring forth the result.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Required Plugin List ------
 *
 * * VisuMZ_0_CoreEngine
 * * VisuMZ_1_BattleCore
 * * VisuMZ_1_ItemsEquipsCore
 * * VisuMZ_1_SkillsStatesCore
 *
 * This plugin requires the above listed plugins to be installed inside your
 * game's Plugin Manager list in order to work. You cannot start your game with
 * this plugin enabled without the listed plugins.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 * 
 * Turn Order Display
 * 
 * The Turn Order Display will capture the battle's currently active battler
 * and any battlers found in the active battlers array for the BattleManager.
 * This does not overwrite any functions, but the Turn Order Display may or may
 * not conflict with any existing HUD elements that are already positioned on
 * the screen. If so, you can choose to offset the Turn Order Display or move
 * it to a different part of the screen through the plugin parameters.
 * 
 * ---
 * 
 * Brave Points and the Brave Command
 * 
 * Abbreviated to "BP", Brave Points are a new currency available through the
 * Brave Turn Battle system. Battlers require at least 0 BP in order to perform
 * any actions for that turn. By default, each action consumes 1 BP. At the end
 * of each turn, each battler regenerates 1 BP. With the normal flow of battle,
 * this results in a net balance.
 * 
 * However, the player can activate the "Brave Command" located right above the
 * Guard Command. This lets the battler create an extra action to perform. When
 * used, the flow of battle will result in a negative net of BP. When BP is at
 * -1 or under, that battler's turn is skipped until it raises back to 0. This
 * effectively means that the "Brave Command" will borrow actions from future
 * turns.
 * 
 * The Guard Command, however will never consume any BP for its actions even if
 * replaced as it is always determined by the battler's current guard skill.
 * This means that when used, the Guard Command lets a battler save up BP for
 * future turns, allowing BP to go net positive for the turn.
 * 
 * By strategically deciding when to borrow actions or save up for them, whole
 * new strategies can be created for battle.
 * 
 * The game dev has control over how many max actions can be borrowed at once,
 * the maximum and minimum amounts for BP to go to, how much BP will cost at
 * default, and how much BP can be regenerated by default. These settings can
 * all be made within the Plugin Parameters.
 * 
 * ---
 *
 * Action Times +
 * 
 * While the Brave Turn Battle system is active, the "Action Times +" trait
 * is disabled. This is to prevent any conflicts with the Brave system. If the
 * Brave Turn Battle system is disabled during the course of the game, then the
 * "Action Times +" will resume working like normal.
 *
 * ---
 * 
 * Can Input
 * 
 * As mentioned in the "Brave Points and the Brave Command" above, if BP is
 * under 0, then that battler cannot input or act for that turn. The battler
 * would have to wait for BP regenerate back up to 0 first.
 * 
 * ---
 * 
 * Can Guard
 * 
 * The Guard action is only enabled when there's one action to use for that
 * turn. This means that if the "Brave Command" is used to generate new actions
 * to perform during that turn, the Guard Command will be disabled. It can be
 * enabled once again if the player cancels out the Brave Command until the
 * action count reaches 1.
 * 
 * ---
 * 
 * Enemy Brave Actions
 * 
 * Enemies can also use the "Brave Command" by faking it. By making a dummy
 * skill with the <BTB Multiple Actions: id, id, id, id> skill notetag or the
 * <BTB Multiple Actions: name, name, name, name> skill notetag, you can have
 * the enemy perform the exact skills you want in a multi-action queue.
 * 
 * Enemies that use this will also suffer from heavy BP expenditure and wait on
 * subsequent turns until they have enough BP to perform actions again.
 * 
 * This is also how you can have enemies perform Action Fusions. For the queued
 * skills, load up the Action Fusion's skill combination you want for the enemy
 * to perform.
 * 
 * ---
 *
 * ============================================================================
 * Action Fusions
 * ============================================================================
 *
 * This feature deserves its own section as it's quite indepth with how it
 * works. Action Fusions can be performed by either the actor and/or enemy
 * (though this can be disabled in the Plugin Parameters or through traits).
 * In order for them to occur, the queued up action list must have a certain
 * combination of skills/items for the Action Fusion to occur.
 *
 * ---
 * 
 * Fusion Types
 * 
 * There are two types of Action Fusions: Flexible and Strict. Flexible Action
 * Fusions can use a combination of skills/items in any order (thus flexible),
 * while Strict Action Fusions must have their skill/item combinations queued
 * up in the exact order they're listed (thus strict).
 * 
 * They all share the following properties:
 * 
 * Skill Action Fusions can only use skills for combinations. This means that
 * Action Fusions made as a skill database object cannot have item requirements
 * for the combinations.
 * 
 * Item Action Fusions can only use items for combinations. This means that
 * Action Fusions made as an item database object cannot have skills for the
 * combination requirements.
 * 
 * Skills and items that have selectable targets need to have matching targets
 * to be a part of the same Action Fusion combination. For example, if "Quad
 * Attack" requires "Attack", "Attack", "Attack", "Attack", then the player
 * would have to target the same enemy for each of the "Attack" actions. This
 * is to prevent the cases where the player wants to spread out the damage
 * evenly across various enemies without forming it into a single target "Quad
 * Attack" against one.
 * 
 * Skills and items that do not have selectable targets are combination targets
 * for any and all candidates. This means an area of effect "Flame" spell can
 * combine with any target selectable or otherwise skill.
 * 
 * When an Action Fusion is performed, it will not consume the resources for
 * the database object itself, but instead, from each of the skills/items used
 * to bring it out. This means the skill costs of the Action Fusion itself are
 * irrelevant, but the skill costs of the combinations do matter and will be
 * consumed instead. The same applies to items.
 * 
 * If the Action Fusion skill/item is used directly, its resource consumption
 * will be performed as if it was not an Action Fusion skill/item. The "Quad
 * Attack" skill will use its regular MP and TP costs while the "Double Elixir"
 * item will consume itself.
 * 
 * If a queue could potentially meet the demands of multiple Action Fusions,
 * then the Action Fusion with the highest database ID will be given priority,
 * as to make it less complicated. This means if the "Double Attack" Action
 * Fusion and "Triple Attack" Action Fusion were to occur at the same time,
 * if the "Triple Attack" skill has a higher ID than "Double Attack", then
 * "Triple Attack" will take priority instead.
 * 
 * The battler must be able to pay the actions of each of the queued actions
 * used to form the Action Fusion. This means if a battler would run out of MP
 * or items for the cost, it will just simply not occur.
 * 
 * An Action Fusion can have multiple combinations that create it as long as
 * there are multiple notetags that determine the Action Fusion. As an example,
 * the "Flame Strike" can occur with the "Attack" and "Flame" combination or
 * the "Strike" and "Flame" combination.
 * 
 * ---
 *
 * Flexible Action Fusion
 *
 * <BTB Flexible Fusion: id, id>
 * <BTB Flexible Fusion: id, id, id>
 * <BTB Flexible Fusion: id, id, id, id>
 *
 * <BTB Flexible Fusion: name, name>
 * <BTB Flexible Fusion: name, name, name>
 * <BTB Flexible Fusion: name, name, name, name>
 *
 * - Used for: Skill, Item Notetags
 * - This Action Fusion skill/item will occur as long as any of the listed
 *   combination skills/items are queued in the action list for that turn.
 *   These actions can be queued in any order.
 * - Replace 'id' with the database ID of the skill/item to use as a
 *   combination requirement.
 * - Replace 'name' with the name of the skill/item to use as a combination
 *   requirement.
 * - Skill Action Fusions can only use skills for combinations.
 * - Item Action Fusions can only use items for combinations.
 * - Skills and items that have selectable targets need to have matching
 *   targets to be a part of the same Action Fusion combination.
 * - Skills and items that do not have selectable targets are combination
 *   targets for any and all candidates.
 * - When an Action Fusion is performed, it will not consume the resources for
 *   the database object itself, but instead, from each of the skills/items
 *   used to bring it out.
 * - Is used directly, this action's resource consumption will be performed as
 *   if it was not an Action Fusion skill/item.
 * - If a queue could potentially meet the demands of multiple Action Fusions,
 *   then the Action Fusion with the highest database ID is given priority.
 * - The battler must be able to pay the actions of each of the queued actions
 *   used to form the Action Fusion.
 * - Insert multiple copies of this notetag to give this Action Fusion more
 *   combinations that can activate it.
 * 
 * Examples:
 * 
 *   ---
 * 
 *   Fire Strike
 * 
 *   <BTB Flexible Fusion: Attack, Fire>
 * 
 *   This Action Fusion will occur if a battler has the "Attack" and "Fire"
 *   actions queued up in any order. "Attack" can come before "Fire" or "Fire"
 *   can come before "Attack" and it would still call upon "Fire Strike".
 * 
 *   ---
 * 
 *   Flame Strike
 * 
 *   <BTB Flexible Fusion: Attack, Flame>
 *   <BTB Flexible Fusion: Strike, Flame>
 * 
 *   This Action Fusion will occur if a battler has "Attack" and "Flame",
 *   "Flame" and "Attack", "Strike" and "Flame", or "Flame" and "Strike" in its
 *   action queue.
 * 
 *   ---
 *
 * ---
 * 
 * Strict Action Fusion
 *
 * <BTB Strict Fusion: id, id>
 * <BTB Strict Fusion: id, id, id>
 * <BTB Strict Fusion: id, id, id, id>
 *
 * <BTB Strict Fusion: name, name>
 * <BTB Strict Fusion: name, name, name>
 * <BTB Strict Fusion: name, name, name, name>
 *
 * - Used for: Skill, Item Notetags
 * - This Action Fusion skill/item will occur as long as the exact listed
 *   combination(s) of skills/items is queued in the action list for that turn.
 *   These actions can be queued in any order.
 * - Replace 'id' with the database ID of the skill/item to use as a
 *   combination requirement.
 * - Replace 'name' with the name of the skill/item to use as a combination
 *   requirement.
 * - Skill Action Fusions can only use skills for combinations.
 * - Item Action Fusions can only use items for combinations.
 * - Skills and items that have selectable targets need to have matching
 *   targets to be a part of the same Action Fusion combination.
 * - Skills and items that do not have selectable targets are combination
 *   targets for any and all candidates.
 * - When an Action Fusion is performed, it will not consume the resources for
 *   the database object itself, but instead, from each of the skills/items
 *   used to bring it out.
 * - Is used directly, this action's resource consumption will be performed as
 *   if it was not an Action Fusion skill/item.
 * - If a queue could potentially meet the demands of multiple Action Fusions,
 *   then the Action Fusion with the highest database ID is given priority.
 * - The battler must be able to pay the actions of each of the queued actions
 *   used to form the Action Fusion.
 * - Insert multiple copies of this notetag to give this Action Fusion more
 *   combinations that can activate it.
 * 
 * Example:
 * 
 *   ---
 * 
 *   Shadow Flare Blade
 * 
 *   <BTB Strict Fusion: Shade II, Fire II, Attack>
 * 
 *   The battler must queue up "Shade II", "Fire II", and "Attack" in that
 *   exact order or else "Shadow Flare Blade" will not occur. Even if the
 *   battler changed the order to "Fire II", "Shade II", and "Attack", the
 *   Action Fusion will not occur.
 * 
 *   ---
 * 
 * ---
 *
 * ============================================================================
 * VisuStella MZ Compatibility
 * ============================================================================
 *
 * While this plugin is compatible with the majority of the VisuStella MZ
 * plugin library, it is not compatible with specific plugins or specific
 * features. This section will highlight the main plugins/features that will
 * not be compatible with this plugin or put focus on how the make certain
 * features compatible.
 *
 * ---
 *
 * VisuMZ_3_BoostAction
 * 
 * The Boost Actions plugin cannot be used together with Battle System - BTB.
 * If the Battle System is switched to using Battle System - BTB, then the
 * Boost Actions plugin will shut itself off.
 * 
 * The reason why these plugins cannot work together is because their mechanics
 * play off too similarly to each other and cause conflicts. We, the plugin
 * developer team, highly recommend that you utilize Battle System - BTB's
 * Brave system instead of the Boost system to make the best use of the battle
 * system in effect.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * ---
 * 
 * === General BTB-Related Notetags ===
 * 
 * These notetags are general purpose notetags that have became available
 * through this plugin.
 * 
 * ---
 * 
 * <BTB Help>
 *  description
 *  description
 * </BTB Help>
 *
 * - Used for: Skill, Item Notetags
 * - If your game happens to support the ability to change battle systems, this
 *   notetag lets you change how the skill/item's help description text will
 *   look under BTB.
 * - This is primarily used if the skill behaves differently in BTB versus any
 *   other battle system.
 * - Replace 'description' with help text that's only displayed if the game's
 *   battle system is set to BTB.
 *
 * ---
 *
 * <BTB Cannot Brave>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object with one of these notetags,
 *   that battler cannot use Brave to generate more actions.
 * - For actors, this will come with the Brave Command disabled.
 *
 * ---
 *
 * <BTB Hide Brave>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object with one of these notetags,
 *   that battler cannot use Brave to generate more actions.
 * - For actors, this will come with the Brave Command hidden along with their
 *   BP values.
 *
 * ---
 * 
 * === BTB Turn Order Display-Related Notetags ===
 * 
 * These notetags affect the BTB Turn Order Display
 * 
 * ---
 *
 * <BTB Turn Order Icon: x>
 *
 * - Used for: Actor, Enemy Notetags
 * - Changes the slot graphic used for the battler to a specific icon.
 * - Replace 'x' with the icon index to be used.
 * 
 * ---
 *
 * <BTB Turn Order Face: filename, index>
 *
 * - Used for: Actor, Enemy Notetags
 * - Changes the slot graphic used for the enemy to a specific face.
 * - Replace 'filename' with the filename of the image.
 *   - Do not include the file extension.
 * - Replace 'index' with the index of the face. Index values start at 0.
 * - Example: <BTB Turn Order Face: Monster, 1>
 * 
 * ---
 * 
 * === Brave Points Cost-Related Notetags ===
 * 
 * The following notetags are used to manage Brave Point (BP) costs, how some
 * actions can alter other BP values, and more.
 * 
 * ---
 *
 * <BTB BP Cost: x>
 *
 * - Used for: Skill, Item Notetags
 * - Determines how much BP the battler uses when performing this action.
 * - Replace 'x' with a number value to determine its BP cost.
 *
 * ---
 *
 * <BTB Hide BP Cost>
 *
 * - Used for: Skill, Item Notetags
 * - Prevents the BP cost from being shown for this action.
 *
 * ---
 * 
 * === Brave Point Manipulation-Related Notetags ===
 * 
 * The following notetags are used to manage Brave Point (BP) costs, how some
 * actions can alter other BP values, and more.
 * 
 * ---
 *
 * <BTB User Set BP: x>
 * <BTB Target Set BP: x>
 *
 * - Used for: Skill, Item Notetags
 * - Sets the user/target's current BP to a specific value.
 * - Replace 'x' with a number value to determine how much you want the user
 *   or target's BP to be set to.
 * - The 'user' variant only affects the action's user.
 * - The 'target' variant only affects the action's target.
 *
 * ---
 *
 * <BTB User Gain BP: +x>
 * <BTB Target Gain BP: +x>
 *
 * <BTB User Lose BP: -x>
 * <BTB Target Lose BP: -x>
 *
 * - Used for: Skill, Item Notetags
 * - Causes the action to alter how much BP the user/target has.
 * - Replace 'x' with a number value to determine how much BP is gained/lost
 *   for the user/target.
 * - The 'user' variant only affects the action's user.
 * - The 'target' variant only affects the action's target.
 *
 * ---
 * 
 * === JavaScript Notetags: Brave Point Manipulation ===
 *
 * The following are notetags made for users with JavaScript knowledge to
 * give more control over Brave Point alteration.
 * 
 * ---
 *
 * <JS BTB User BP>
 *  code
 *  code
 *  value = code;
 * </JS BTB User BP>
 *
 * - Used for: Skill, Item Notetags
 * - Replace 'code' with JavaScript code to determine what is the user's final
 *   BP value after all of the code is ran.
 * - The 'value' variable is the returned value to be set as the user's BP.
 *   This value also starts off as the user's current BP.
 * - The 'user' variable refers to the action's user.
 * - The 'target' variable refers to the action's current target.
 * 
 * ---
 *
 * <JS BTB Target BP>
 *  code
 *  code
 *  value = code;
 * </JS BTB Target BP>
 *
 * - Used for: Skill, Item Notetags
 * - Replace 'code' with JavaScript code to determine what is the current
 *   target's final BP value after all of the code is ran.
 * - The 'value' variable is the returned value to be set as the target's BP.
 *   This value also starts off as the target's current BP.
 * - The 'user' variable refers to the action's user.
 * - The 'target' variable refers to the action's current target.
 * 
 * ---
 * 
 * === Brave Point Managment-Related Notetags ===
 * 
 * The following notetags are used to for battlers to manage their BP settings
 * throughout the course of the fight.
 * 
 * ---
 *
 * <BTB Initial BP: +x>
 * <BTB Initial BP: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object with one of these notetags,
 *   alter that battler's initial BP at the start of battle.
 * - Replace 'x' with a number value representing how much you want to alter
 *   the affected battler's initial BP at the start of battle.
 *
 * ---
 *
 * <BTB BP Regen: +x>
 * <BTB BP Degen: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object with one of these notetags,
 *   alter the amount of BP regenerated at the end of each battle turn.
 * - Replace 'x' with a number value representing how much BP is regenerated
 *   (or decreased). 
 *   - Use a positive number for gaining BP at the end of each turn.
 *   - Use a negative number for losing BP at the end of each turn.
 *
 * ---
 *
 * <BTB Maximum BP: +x>
 * <BTB Maximum BP: -x>
 *
 * <BTB Minimum BP: +x>
 * <BTB Minimum BP: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object with one of these notetags,
 *   increase or decrease the maximum/minimum BP that battler can have by 'x'.
 * - Replace 'x' with a number value representing the amount to change the
 *   battler's maximum/minimum BP by.
 * - These numbers cannot exceed or go under the designated amounts set by the
 *   hard cap in this plugin's Plugin Parameters.
 *
 * ---
 * 
 * === Multiple Action-Related Notetags ===
 * 
 * These notetags allow you to determine how multiple actions are handled
 * through the Brave Turn Battle system.
 * 
 * ---
 *
 * <BTB Maximum Actions: +x>
 * <BTB Maximum Actions: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object with one of these notetags,
 *   increase/decrease the maximum number of actions that battler can have
 *   through the Brave Command.
 * - Replace 'x' with a number value representing the amount of maximum actions
 *   to increase/decrease by.
 * - This value cannot make a battler go below 1 maximum action.
 * - This value cannot make a battler go above the hard cap set in this
 *   plugin's Plugin Parameters.
 *
 * ---
 *
 * <BTB Multiple Actions: id, id>
 * <BTB Multiple Actions: id, id, id>
 * <BTB Multiple Actions: id, id, id, id>
 *
 * <BTB Multiple Actions: name, name>
 * <BTB Multiple Actions: name, name, name>
 * <BTB Multiple Actions: name, name, name, name>
 *
 * - Used for: Skill Notetags
 * - When an enemy (NOT ACTOR) uses this skill, the game will appear as if the
 *   enemy is using the Brave Command to load up multiple actions at a time.
 * - Replace 'id' with the database ID of the skill to use in the multiple
 *   action queue.
 * - Replace 'name' with the name of the skill to use in the enemy's multiple
 *   action queue.
 * 
 * ---
 * 
 * === Action Fusion-Related Notetags ===
 * 
 * For more details, please refer to the Action Fusion dedicated section listed
 * earlier in the documentation.
 * 
 * ---
 *
 * Flexible Action Fusion
 *
 * <BTB Flexible Fusion: id, id>
 * <BTB Flexible Fusion: id, id, id>
 * <BTB Flexible Fusion: id, id, id, id>
 *
 * <BTB Flexible Fusion: name, name>
 * <BTB Flexible Fusion: name, name, name>
 * <BTB Flexible Fusion: name, name, name, name>
 *
 * - Used for: Skill, Item Notetags
 * - This Action Fusion skill/item will occur as long as any of the listed
 *   combination skills/items are queued in the action list for that turn.
 *   These actions can be queued in any order.
 * - Replace 'id' with the database ID of the skill/item to use as a
 *   combination requirement.
 * - Replace 'name' with the name of the skill/item to use as a combination
 *   requirement.
 * - Skill Action Fusions can only use skills for combinations.
 * - Item Action Fusions can only use items for combinations.
 * - Skills and items that have selectable targets need to have matching
 *   targets to be a part of the same Action Fusion combination.
 * - Skills and items that do not have selectable targets are combination
 *   targets for any and all candidates.
 * - When an Action Fusion is performed, it will not consume the resources for
 *   the database object itself, but instead, from each of the skills/items
 *   used to bring it out.
 * - Is used directly, this action's resource consumption will be performed as
 *   if it was not an Action Fusion skill/item.
 * - If a queue could potentially meet the demands of multiple Action Fusions,
 *   then the Action Fusion with the highest database ID is given priority.
 * - The battler must be able to pay the actions of each of the queued actions
 *   used to form the Action Fusion.
 * - Insert multiple copies of this notetag to give this Action Fusion more
 *   combinations that can activate it.
 *
 * ---
 * 
 * Strict Action Fusion
 *
 * <BTB Strict Fusion: id, id>
 * <BTB Strict Fusion: id, id, id>
 * <BTB Strict Fusion: id, id, id, id>
 *
 * <BTB Strict Fusion: name, name>
 * <BTB Strict Fusion: name, name, name>
 * <BTB Strict Fusion: name, name, name, name>
 *
 * - Used for: Skill, Item Notetags
 * - This Action Fusion skill/item will occur as long as the exact listed
 *   combination(s) of skills/items is queued in the action list for that turn.
 *   These actions can be queued in any order.
 * - Replace 'id' with the database ID of the skill/item to use as a
 *   combination requirement.
 * - Replace 'name' with the name of the skill/item to use as a combination
 *   requirement.
 * - Skill Action Fusions can only use skills for combinations.
 * - Item Action Fusions can only use items for combinations.
 * - Skills and items that have selectable targets need to have matching
 *   targets to be a part of the same Action Fusion combination.
 * - Skills and items that do not have selectable targets are combination
 *   targets for any and all candidates.
 * - When an Action Fusion is performed, it will not consume the resources for
 *   the database object itself, but instead, from each of the skills/items
 *   used to bring it out.
 * - Is used directly, this action's resource consumption will be performed as
 *   if it was not an Action Fusion skill/item.
 * - If a queue could potentially meet the demands of multiple Action Fusions,
 *   then the Action Fusion with the highest database ID is given priority.
 * - The battler must be able to pay the actions of each of the queued actions
 *   used to form the Action Fusion.
 * - Insert multiple copies of this notetag to give this Action Fusion more
 *   combinations that can activate it.
 *
 * ---
 *
 * <BTB Cannot Fusion>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object that has this notetag, that
 *   battler cannot perform any Action Fusions. Queued skills will occur
 *   normally instead.
 * - If the actor is affected by both notetags for <BTB Cannot Fusion> and
 *   <BTB Enable Fusion> priority will be given based on the order of their
 *   trait objects.
 *
 * ---
 *
 * <BTB Enable Fusion>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - If a battler is affected by a trait object that has this notetag, that
 *   battler is allowed to perform any Action Fusions. Queued skills will occur
 *   normally instead.
 * - If the actor is affected by both notetags for <BTB Cannot Fusion> and
 *   <BTB Enable Fusion> priority will be given based on the order of their
 *   trait objects.
 *
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Actor Plugin Commands ===
 * 
 * ---
 *
 * Actor: Change BTB Turn Order Icon
 * - Changes the icons used for the specific actor(s) on the BTB Turn Order.
 *
 *   Actor ID(s):
 *   - Select which Actor ID(s) to affect.
 *
 *   Icon:
 *   - Changes the graphic to this icon.
 *
 * ---
 *
 * Actor: Change BTB Turn Order Face
 * - Changes the faces used for the specific actor(s) on the BTB Turn Order.
 *
 *   Actor ID(s):
 *   - Select which Actor ID(s) to affect.
 *
 *   Face Name:
 *   - This is the filename for the target face graphic.
 *
 *   Face Index:
 *   - This is the index for the target face graphic.
 *
 * ---
 *
 * Actor: Clear BTB Turn Order Graphic
 * - Clears the BTB Turn Order graphics for the actor(s).
 * - The settings will revert to the Plugin Parameter settings.
 *
 *   Actor ID(s):
 *   - Select which Actor ID(s) to affect.
 *
 * ---
 * 
 * === Enemy Plugin Commands ===
 * 
 * ---
 *
 * Enemy: Change BTB Turn Order Icon
 * - Changes the icons used for the specific enemy(ies) on the BTB Turn Order.
 *
 *   Enemy Index(es):
 *   - Select which enemy index(es) to affect.
 *
 *   Icon:
 *   - Changes the graphic to this icon.
 *
 * ---
 *
 * Enemy: Change BTB Turn Order Face
 * - Changes the faces used for the specific enemy(ies) on the BTB Turn Order.
 *
 *   Enemy Index(es):
 *   - Select which enemy index(es) to affect.
 *
 *   Face Name:
 *   - This is the filename for the target face graphic.
 *
 *   Face Index:
 *   - This is the index for the target face graphic.
 *
 * ---
 *
 * Enemy: Clear BTB Turn Order Graphic
 * - Clears the BTB Turn Order graphics for the enemy(ies).
 * - The settings will revert to the Plugin Parameter settings.
 *
 *   Enemy Index(es):
 *   - Select which enemy index(es) to affect.
 *
 * ---
 * 
 * === System Plugin Commands ===
 * 
 * ---
 *
 * System: BTB Turn Order Visibility
 * - Determine the visibility of the BTB Turn Order Display.
 *
 *   Visibility:
 *   - Changes the visibility of the BTB Turn Order Display.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Settings
 * ============================================================================
 *
 * General settings regarding Battle System BTB. These range from how Brave
 * Points (BP) appear in-game to how their costs are displayed.
 *
 * ---
 *
 * Brave Points
 * 
 *   Full Name:
 *   - What is the full name of "Brave Points" in your game?
 * 
 *   Abbreviation:
 *   - What is the abbreviation of "Brave Points" in your game?
 * 
 *   Icon:
 *   - What icon do you wish to use to represent Brave Points?
 * 
 *   Cost Format:
 *   - How are Brave Point costs displayed?
 *   - %1 - Cost, %2 - BP Text, %3 - Icon
 *
 * ---
 *
 * Displayed Costs
 * 
 *   Cost Position Front?:
 *   - Put the BP Cost at the front of skill/item costs?
 * 
 *   Show Cost: Attack:
 *   - Show the BP cost for the Attack command?
 * 
 *   Show Cost: Guard:
 *   - Show the BP cost for the Guard command?
 * 
 *   Reduce Shown BP Cost:
 *   - Reduce shown BP costs by this much.
 *   - Used to match traditional games.
 * 
 *   Show Cost: 0 BP:
 *   - Show the BP cost when the cost is 0 BP?
 *   - Shown BP Cost reduction is applied.
 * 
 *   Show Cost: 1 BP:
 *   - Show the BP cost when the cost is 1 BP?
 *   - Shown BP Cost reduction is applied.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * Adjust the mechanics settings for the Battle System BTB. Mechanics range
 * from how speed is handled to Brave action caps, how Brave Points are
 * managed, and Action Fusions.
 *
 * ---
 *
 * Action Speed
 * 
 *   Allow Random Speed?:
 *   - Allow speed to be randomized base off the user's AGI?
 * 
 *   JS: Calculate:
 *   - Code used to calculate action speed.
 *
 * ---
 *
 * Brave Action Max
 * 
 *   Default:
 *   - What is the default number of max actions a battler can have from the
 *     Brave system?
 * 
 *   Hard Cap:
 *   - What is the absolute highest for maximum actions a battler can have
 *     from the Brave system?
 *
 * ---
 *
 * Brave Points > Limits
 * 
 *   Default Maximum:
 *   - What is the default maximum number of Brave Points a battler can have at
 *     a time?
 * 
 *   Default Minimum:
 *   - What is the default minimum number of Brave Points a battler can have at
 *     a time?
 * 
 *   Hard Cap Maximum:
 *   - What is the absolute maximum number of Brave Points a battler can have
 *     at a time?
 * 
 *   Hard Cap Minimum:
 *   - What is the absolute minimum number of Brave Points a battler can have
 *     at a time?
 *
 * ---
 *
 * Brave Points > Costs
 * 
 *   Default Skill Cost:
 *   - How many Brave Points does a skill cost by default?
 * 
 *   Default Item Cost:
 *   - How many Brave Points does an item cost by default?
 * 
 *   Predicted Cost:
 *   - What is considered predicted cost?
 *
 * ---
 *
 * Brave Points > Start Battle
 * 
 *   Neutral:
 *   - How many Brave Points should a battler have if the battle advantage is
 *     neutral?
 * 
 *   Favored:
 *   - How many Brave Points should a battler have if the battle advantage is
 *     favored?
 *
 * ---
 *
 * Brave Points > Regeneration
 * 
 *   Base Recovery:
 *   - How many Brave Points are regenerated at the end of each turn?
 * 
 *   Needs to be Alive?:
 *   - Do battlers need to be alive to regenerate Brave Points?
 *
 * ---
 *
 * Action Fusions
 * 
 *   Actor Access?:
 *   - Allow actors access to Action Fusions?
 * 
 *   Enemy Access?:
 *   - Allow enemies access to Action Fusions?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Brave Animations Settings
 * ============================================================================
 *
 * Animation when applying/canceling Brave effects.
 *
 * ---
 *
 * On Brave
 * 
 *   Animation ID:
 *   - Play this animation when the effect activates.
 * 
 *   Mirror Animation:
 *   - Mirror the effect animation?
 * 
 *   Mute Animation:
 *   - Mute the effect animation?
 *
 * ---
 *
 * Cancel Brave
 * 
 *   Animation ID:
 *   - Play this animation when the effect activates.
 * 
 *   Mirror Animation:
 *   - Mirror the effect animation?
 * 
 *   Mute Animation:
 *   - Mute the effect animation?
 *
 * ---
 *
 * Enemy Brave
 * 
 *   Show Activation?:
 *   - Show the enemy activating Brave?
 * 
 *   Wait Frames:
 *   - This is the number of frames to wait between activations.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Turn Order Display Settings
 * ============================================================================
 *
 * Turn Order Display settings used for Battle System BTB. These adjust how the
 * visible turn order appears in-game.
 *
 * ---
 *
 * General
 * 
 *   Display Position:
 *   - Select where the Turn Order will appear on the screen.
 * 
 *     Offset X:
 *     - How much to offset the X coordinate by.
 *     - Negative: left. Positive: right.
 * 
 *     Offset Y:
 *     - How much to offset the Y coordinate by.
 *     - Negative: up. Positive: down.
 * 
 *   Center Horizontal?:
 *   - Reposition the Turn Order Display to always be centered if it is a
 *     'top' or 'bottom' position?
 * 
 *   Reposition for Help?:
 *   - If the display position is at the top, reposition the display when the
 *     help window is open?
 * 
 *   Reposition Log?:
 *   - If the display position is at the top, reposition the Battle Log Window
 *     to be lower?
 * 
 *   Forward Direction:
 *   - Decide on the direction of the Turn Order.
 *   - Settings may vary depending on position.
 *   - Left to Right / Down to Up
 *   - Right to Left / Up to Down
 * 
 *   Subject Distance:
 *   - How far do you want the currently active battler to distance itself from
 *     the rest of the Turn Order?
 * 
 *   Screen Buffer:
 *   - What distance do you want the display to be away from the edge of the
 *     screen by?
 *
 * ---
 *
 * Reposition For Help
 * 
 *   Repostion X By:
 *   Repostion Y By:
 *   - Reposition the display's coordinates by this much when the Help Window
 *     is visible.
 *
 * ---
 *
 * Slots
 * 
 *   Max Horizontal:
 *   - Maximum slots you want to display for top and bottom Turn Order Display
 *     positions?
 * 
 *   Max Vertical:
 *   - Maximum slots you want to display for left and right Turn Order Display
 *     positions?
 * 
 *   Length:
 *   - How many pixels long should the slots be on the Turn Order display?
 * 
 *   Thin:
 *   - How many pixels thin should the slots be on the Turn Order display?
 * 
 *   Update Frames:
 *   - How many frames should it take for the slots to update their
 *     positions by?
 *
 * ---
 *
 * Slot Border
 * 
 *   Show Border?:
 *   - Show borders for the slot sprites?
 * 
 *   Border Thickness:
 *   - How many pixels thick should the colored portion of the border be?
 * 
 *   Actors
 *   Enemies
 * 
 *     Border Color:
 *     - Use #rrggbb for custom colors or regular numbers for text colors
 *       from the Window Skin.
 * 
 *     Border Skin:
 *     - Optional. Place a skin on the actor/enemy borders instead of
 *       rendering them?
 *
 * ---
 *
 * Slot Sprites
 * 
 *   Actors
 * 
 *     Sprite Type:
 *     - Select the type of sprite used for the actor graphic.
 *     - Face Graphic - Show the actor's face.
 *     - Icon - Show a specified icon.
 *     - Sideview Actor - Show the actor's sideview battler.
 * 
 *     Default Icon:
 *     - Which icon do you want to use for actors by default?
 * 
 *   Enemies
 * 
 *     Sprite Type:
 *     - Select the type of sprite used for the enemy graphic.
 *     - Face Graphic - Show a specified face graphic.
 *     - Icon - Show a specified icon.
 *     - Enemy - Show the enemy's graphic or sideview battler.
 * 
 *     Default Face Name:
 *     - Use this default face graphic if there is no specified face.
 * 
 *     Default Face Index:
 *     - Use this default face index if there is no specified index.
 * 
 *     Default Icon:
 *     - Which icon do you want to use for enemies by default?
 * 
 *     Match Hue?:
 *     - Match the hue for enemy battlers?
 *     - Does not apply if there's a sideview battler.
 *
 * ---
 *
 * Slot Letter
 * 
 *   Show Enemy Letter?:
 *   - Show the enemy's letter on the slot sprite?
 * 
 *   Font Name:
 *   - The font name used for the text of the Letter.
 *   - Leave empty to use the default game's font.
 * 
 *   Font Size:
 *   - The font size used for the text of the Letter.
 *
 * ---
 *
 * Slot Background
 * 
 *   Show Background?:
 *   - Show the background on the slot sprite?
 * 
 *   Actors
 *   Enemies
 * 
 *     Background Color 1:
 *     Background Color 2:
 *     - Use #rrggbb for custom colors or regular numbers for text colors
 *       from the Window Skin.
 * 
 *     Background Skin:
 *     - Optional. Use a skin for the actor background instead of
 *       rendering them?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Window Settings Settings
 * ============================================================================
 *
 * Settings regarding the windows of the Battle System BTB. These mostly adjust
 * how certain aspects of the Brave Turn Battle system appear in-game.
 *
 * ---
 *
 * Window_ActorCommand
 * 
 *   Command Text:
 *   - What is the text that appears for the Brave command?
 * 
 *   Show Command?:
 *   - Show the Brave command in the Actor Command Window?
 * 
 *   Page Up/Dn Shortcuts?:
 *   - Use Page Up/Down for shortcuts on activating Brave?
 * 
 *   JS: Draw Counters:
 *   - Code used to determine how the action counters are displayed on
 *     the window.
 * 
 *     Action Slot:
 *     - This is the text used to represent a non-selected action slot.
 * 
 *     Current Action:
 *     - This is the text used to represent the current action slot.
 *
 * ---
 *
 * Window_BattleStatus
 * 
 *   Display Format:
 *   - How are actor Brave Point displayed?
 *   - %1 - Total BP, %2 - BP Text, %3 - Icon
 * 
 *   Predict Format:
 *   - How are predicted Brave Point displayed?
 *   - %1 - Total BP, %2 - BP Text, %3 - Icon, %4 - Predicted
 *
 * ---
 *
 * Window_BattleStatus > Text Colors
 * 
 *   Neutral Color:
 *   - Text code color for neutral number values.
 * 
 *   Positive Color:
 *   - Text code color for positive number values.
 * 
 *   Negative Color:
 *   - Text code color for negative number values.
 *
 * ---
 *
 * Window_BattleStatus > Style Settings > Default Style
 *
 * Window_BattleStatus > Style Settings > List Style
 *
 * Window_BattleStatus > Style Settings > XP Style
 *
 * Window_BattleStatus > Style Settings > Portrait Style
 *
 * Window_BattleStatus > Style Settings > Border Style
 *
 * Window_BattleStatus > Style Settings > Alignment Style
 * 
 *   Show Display?:
 *   - Show the actor's BP values in the Battle Status Window?
 * 
 *   Alignment:
 *   - How do you want the actor BP values to be aligned?
 * 
 *   Offset X:
 *   Offset Y:
 *   - Offset the actor BP display X/Y by how many pixels?
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.06: March 26, 2021
 * * Documentation Update!
 * ** Added "VisuStella MZ Compatibility" section for detailed compatibility
 *    explanations with the VisuMZ_3_BoostAction plugin.
 * 
 * Version 1.05: March 19, 2021
 * * Feature Update!
 * ** Turn Order Window calculations slightly tweaked for times when the window
 *    layer is bigger than it should be. Update made by Olivia.
 * 
 * Version 1.04: March 5, 2021
 * * Bug Fixes!
 * ** <BTB User Set BP: x>, <BTB User Gain BP: +x>, <BTB User Lose BP: -x>
 *    notetags should no work properly. Fix made by Arisu.
 * 
 * Version 1.03: January 22, 2021
 * * Feature Update!
 * ** A different kind of end battle check is now made to determine hiding the
 *    turn order display. Update made by Olivia.
 * 
 * Version 1.02: January 1, 2021
 * * Compatibility Update
 * ** Added compatibility functionality for future plugins.
 * 
 * Version 1.01: December 25, 2020
 * * Bug Fixes!
 * ** Brave Point preview in the battle status will now be bound by the
 *    absolute minimum hard card and the maximum soft cap. Fixed by Yanfly.
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features!
 * ** New Notetag added by Yanfly.
 * *** <BTB Enable Fusion>
 *
 * Version 1.00: January 4, 2021
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BtbTurnOrderActorIcon
 * @text Actor: Change BTB Turn Order Icon
 * @desc Changes the icons used for the specific actor(s) on the BTB Turn Order.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @arg IconIndex:num
 * @text Icon
 * @desc Changes the graphic to this icon.
 * @default 84
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BtbTurnOrderActorFace
 * @text Actor: Change BTB Turn Order Face
 * @desc Changes the faces used for the specific actor(s) on the BTB Turn Order.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @arg FaceName:str
 * @text Face Name
 * @type file
 * @dir img/faces/
 * @desc This is the filename for the target face graphic.
 * @default Actor1
 *
 * @arg FaceIndex:num
 * @text Face Index
 * @type number
 * @desc This is the index for the target face graphic.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BtbTurnOrderClearActorGraphic
 * @text Actor: Clear BTB Turn Order Graphic
 * @desc Clears the BTB Turn Order graphics for the actor(s).
 * The settings will revert to the Plugin Parameter settings.
 *
 * @arg Actors:arraynum
 * @text Actor ID(s)
 * @type actor[]
 * @desc Select which Actor ID(s) to affect.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BtbTurnOrderEnemyIcon
 * @text Enemy: Change BTB Turn Order Icon
 * @desc Changes the icons used for the specific enemy(ies) on the BTB Turn Order.
 *
 * @arg Enemies:arraynum
 * @text Enemy Index(es)
 * @type number[]
 * @desc Select which enemy index(es) to affect.
 * @default ["1"]
 *
 * @arg IconIndex:num
 * @text Icon
 * @desc Changes the graphic to this icon.
 * @default 298
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BtbTurnOrderEnemyFace
 * @text Enemy: Change BTB Turn Order Face
 * @desc Changes the faces used for the specific enemy(ies) on the BTB Turn Order.
 *
 * @arg Enemies:arraynum
 * @text Enemy Index(es)
 * @type number[]
 * @desc Select which enemy index(es) to affect.
 * @default ["1"]
 *
 * @arg FaceName:str
 * @text Face Name
 * @parent EnemySprite
 * @type file
 * @dir img/faces/
 * @desc This is the filename for the target face graphic.
 * @default Monster
 *
 * @arg FaceIndex:num
 * @text Face Index
 * @parent EnemySprite
 * @type number
 * @desc This is the index for the target face graphic.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command BtbTurnOrderClearEnemyGraphic
 * @text Enemy: Clear BTB Turn Order Graphic
 * @desc Clears the BTB Turn Order graphics for the enemy(ies).
 * The settings will revert to the Plugin Parameter settings.
 *
 * @arg Enemies:arraynum
 * @text Enemy Index(es)
 * @type number[]
 * @desc Select which enemy index(es) to affect.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemTurnOrderVisibility
 * @text System: BTB Turn Order Visibility
 * @desc Determine the visibility of the BTB Turn Order Display.
 *
 * @arg Visible:eval
 * @text Visibility
 * @type boolean
 * @on Visible
 * @off Hidden
 * @desc Changes the visibility of the BTB Turn Order Display.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BattleSystemBTB
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param General:struct
 * @text General Settings
 * @type struct<General>
 * @desc General settings regarding Battle System BTB.
 * @default {"BravePoints":"","BravePointsFull:str":"Brave Points","BravePointsAbbr:str":"BP","BravePointsIcon:num":"73","BravePointCostFmt:str":"\\FS[22]\\C[4]%1\\C[6]%2\\C[0]","DisplayedCosts":"","CostPosition:eval":"false","ShowCostForAttack:eval":"false","ShowCostForGuard:eval":"false","ReduceShownBPCost:num":"0","Show_0_BP_Cost:eval":"true","Show_1_BP_Cost:eval":"true"}
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Adjust the mechanics settings for the Battle System BTB.
 * @default {"ActionSpeed":"","AllowRandomSpeed:eval":"false","CalcActionSpeedJS:func":"\"// Declare Constants\\nconst agi = this.subject().agi;\\n\\n// Create Speed\\nlet speed = agi;\\nif (this.allowRandomSpeed()) {\\n    speed += Math.randomInt(Math.floor(5 + agi / 4));\\n}\\nif (this.item()) {\\n    speed += this.item().speed;\\n}\\nif (this.isAttack()) {\\n    speed += this.subject().attackSpeed();\\n}\\n\\n// Return Speed\\nreturn speed;\"","ActionMax":"","MaxActionsDefault:num":"4","MaxActionsHardCap:num":"9","BravePoints":"","BravePointsLimits":"","MaxBravePointsDefault:num":"3","MinBravePointsDefault:num":"-4","MaxBravePointsHardCap:num":"9","MinBravePointsHardCap:num":"-9","BravePointsCosts":"","BravePointSkillCost:num":"1","BravePointItemCost:num":"1","BravePointPredictedCost:num":"1","BravePointsStartBattle":"","BravePointStartNeutral:num":"0","BravePointStartFavor:num":"3","BravePointsRegen":"","BravePointRegenBase:num":"1","BravePointsRegenAlive:eval":"true","ActionFusions":"","ActorActionFusions:eval":"true","EnemyActionFusions:eval":"true"}
 *
 * @param BraveAnimation:struct
 * @text Brave Animations
 * @type struct<BraveAnimation>
 * @desc Animation when applying/canceling Brave effects.
 * @default {"OnBrave":"","BraveAnimationID:num":"12","BraveMirror:eval":"false","BraveMute:eval":"false","CancelBrave":"","CancelAnimationID:num":"62","CancelMirror:eval":"false","CancelMute:eval":"false"}
 *
 * @param TurnOrder:struct
 * @text Turn Order Display
 * @type struct<TurnOrder>
 * @desc Turn Order Display settings used for Battle System BTB.
 * @default {"General":"","DisplayPosition:str":"top","DisplayOffsetX:num":"0","DisplayOffsetY:num":"0","CenterHorz:eval":"true","RepositionTopForHelp:eval":"true","RepositionLogWindow:eval":"true","OrderDirection:eval":"true","SubjectDistance:num":"8","ScreenBuffer:num":"20","Reposition":"","RepositionTopHelpX:num":"0","RepositionTopHelpY:num":"96","Slots":"","MaxHorzSprites:num":"16","MaxVertSprites:num":"10","SpriteLength:num":"72","SpriteThin:num":"36","UpdateFrames:num":"24","Border":"","ShowMarkerBorder:eval":"true","BorderActor":"","ActorBorderColor:str":"4","ActorSystemBorder:str":"","BorderEnemy":"","EnemyBorderColor:str":"2","EnemySystemBorder:str":"","BorderThickness:num":"2","Sprite":"","ActorSprite":"","ActorBattlerType:str":"face","ActorBattlerIcon:num":"84","EnemySprite":"","EnemyBattlerType:str":"enemy","EnemyBattlerFaceName:str":"Monster","EnemyBattlerFaceIndex:num":"1","EnemyBattlerIcon:num":"298","EnemyBattlerMatchHue:eval":"true","Letter":"","EnemyBattlerDrawLetter:eval":"true","EnemyBattlerFontFace:str":"","EnemyBattlerFontSize:num":"16","Background":"","ShowMarkerBg:eval":"true","BackgroundActor":"","ActorBgColor1:str":"19","ActorBgColor2:str":"9","ActorSystemBg:str":"","BackgroundEnemy":"","EnemyBgColor1:str":"19","EnemyBgColor2:str":"18","EnemySystemBg:str":""}
 *
 * @param Window:struct
 * @text Window Settings
 * @type struct<Window>
 * @desc Settings regarding the windows of the Battle System BTB.
 * @default {"Window_ActorCommand":"","CommandName:str":"Brave","ShowCommand:eval":"true","BraveShortcuts:eval":"true","DrawActionCountersJS:func":"\"// Declare Constants\\nconst sprite = arguments[0];\\nconst parentWindow = arguments[1];\\nconst actor = arguments[2];\\n\\n// Set Location\\nsprite.x = Math.round(parentWindow.width / 2);\\nsprite.y = 0;\\nsprite.anchor.x = 0.5\\nsprite.anchor.y = 0.5\\n\\n// Create Text\\nconst textSlot = TextManager.btbActionSlot;\\nconst textCurrent = TextManager.btbActionCurrent;\\nlet text = textSlot.repeat(actor.numActions());\\nconst index = actor._actionInputIndex;\\ntext = text.substring(0, index) + textCurrent + text.substring(index + 1);\\n\\n// Create and Draw Bitmap\\nconst bitmap = new Bitmap(parentWindow.width, parentWindow.lineHeight());\\nbitmap.fontSize = 36;\\nbitmap.drawText(text, 0, 0, bitmap.width, bitmap.height, 'center');\\nsprite.bitmap = bitmap;\"","ActionSlot:str":"○","ActionCurrent:str":"◉","Window_BattleStatus":"","StatusDisplayFmt:str":"\\FS[16]\\C[6]%2\\C[0] \\FS[22]%1","StatusPredictFmt:str":"\\FS[16]\\C[6]%2\\C[0] \\FS[22]%1\\FS[16] → \\FS[22]%4","TextColors":"","NeutralColor:num":"0","PositiveColor:num":"4","NegativeColor:num":"2","Styles":"","DefaultStyle":"","default_display:eval":"true","default_align:str":"right","default_offsetX:num":"16","default_offsetY:num":"0","ListStyle":"","list_display:eval":"true","list_align:str":"left","list_offsetX:num":"-8","list_offsetY:num":"0","XPStyle":"","xp_display:eval":"true","xp_align:str":"right","xp_offsetX:num":"16","xp_offsetY:num":"0","PortraitStyle":"","portrait_display:eval":"true","portrait_align:str":"right","portrait_offsetX:num":"-8","portrait_offsetY:num":"56","BorderStyle":"","border_display:eval":"true","border_align:str":"right","border_offsetX:num":"16","border_offsetY:num":"0"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * General Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~General:
 *
 * @param BravePoints
 * @text Brave Points
 *
 * @param BravePointsFull:str
 * @text Full Name
 * @parent BravePoints
 * @desc What is the full name of "Brave Points" in your game?
 * @default Brave Points
 *
 * @param BravePointsAbbr:str
 * @text Abbreviation
 * @parent BravePoints
 * @desc What is the abbreviation of "Brave Points" in your game?
 * @default BP
 *
 * @param BravePointsIcon:num
 * @text Icon
 * @parent BravePoints
 * @desc What icon do you wish to use to represent Brave Points?
 * @default 73
 *
 * @param BravePointCostFmt:str
 * @text Cost Format
 * @parent BravePoints
 * @desc How are Brave Point costs displayed?
 * %1 - Cost, %2 - BP Text, %3 - Icon
 * @default \FS[22]\C[4]%1\C[6]%2\C[0]
 *
 * @param DisplayedCosts
 * @text Displayed Costs
 *
 * @param CostPosition:eval
 * @text Cost Position Front?
 * @parent DisplayedCosts
 * @type boolean
 * @on Front
 * @off Back
 * @desc Put the BP Cost at the front of skill/item costs?
 * @default false
 *
 * @param ShowCostForAttack:eval
 * @text Show Cost: Attack
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the BP cost for the Attack command?
 * @default false
 *
 * @param ShowCostForGuard:eval
 * @text Show Cost: Guard
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the BP cost for the Guard command?
 * @default false
 *
 * @param ReduceShownBPCost:num
 * @text Reduce Shown BP Cost
 * @parent DisplayedCosts
 * @type number
 * @desc Reduce shown BP costs by this much.
 * Used to match traditional games.
 * @default 0
 *
 * @param Show_0_BP_Cost:eval
 * @text Show Cost: 0 BP
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the BP cost when the cost is 0 BP?
 * Shown BP Cost reduction is applied.
 * @default true
 *
 * @param Show_1_BP_Cost:eval
 * @text Show Cost: 1 BP
 * @parent DisplayedCosts
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the BP cost when the cost is 1 BP?
 * Shown BP Cost reduction is applied.
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param ActionSpeed
 * @text Action Speed
 *
 * @param AllowRandomSpeed:eval
 * @text Allow Random Speed?
 * @parent ActionSpeed
 * @type boolean
 * @on Allow
 * @off Disable
 * @desc Allow speed to be randomized base off the user's AGI?
 * @default false
 *
 * @param CalcActionSpeedJS:func
 * @text JS: Calculate
 * @parent ActionSpeed
 * @type note
 * @desc Code used to calculate action speed.
 * @default "// Declare Constants\nconst agi = this.subject().agi;\n\n// Create Speed\nlet speed = agi;\nif (this.allowRandomSpeed()) {\n    speed += Math.randomInt(Math.floor(5 + agi / 4));\n}\nif (this.item()) {\n    speed += this.item().speed;\n}\nif (this.isAttack()) {\n    speed += this.subject().attackSpeed();\n}\n\n// Return Speed\nreturn speed;"
 *
 * @param ActionMax
 * @text Brave Action Max
 *
 * @param MaxActionsDefault:num
 * @text Default
 * @parent ActionMax
 * @type number
 * @min 1
 * @desc What is the default number of max actions a battler can 
 * have from the Brave system?
 * @default 4
 *
 * @param MaxActionsHardCap:num
 * @text Hard Cap
 * @parent ActionMax
 * @type number
 * @min 1
 * @desc What is the absolute highest for maximum actions a battler
 * can have from the Brave system?
 * @default 9
 *
 * @param BravePoints
 * @text Brave Points
 *
 * @param BravePointsLimits
 * @text Limits
 * @parent BravePoints
 *
 * @param MaxBravePointsDefault:num
 * @text Default Maximum
 * @parent BravePointsLimits
 * @type number
 * @min 1
 * @desc What is the default maximum number of Brave Points a
 * battler can have at a time?
 * @default 3
 *
 * @param MinBravePointsDefault:num
 * @text Default Minimum
 * @parent BravePointsLimits
 * @desc What is the default minimum number of Brave Points a
 * battler can have at a time?
 * @default -4
 *
 * @param MaxBravePointsHardCap:num
 * @text Hard Cap Maximum
 * @parent BravePointsLimits
 * @type number
 * @min 1
 * @desc What is the absolute maximum number of Brave Points a
 * battler can have at a time?
 * @default 9
 *
 * @param MinBravePointsHardCap:num
 * @text Hard Cap Minimum
 * @parent BravePointsLimits
 * @desc What is the absolute minimum number of Brave Points a
 * battler can have at a time?
 * @default -9
 *
 * @param BravePointsCosts
 * @text Costs
 * @parent BravePoints
 *
 * @param BravePointSkillCost:num
 * @text Default Skill Cost
 * @parent BravePointsCosts
 * @type number
 * @min 0
 * @desc How many Brave Points does a skill cost by default?
 * @default 1
 *
 * @param BravePointItemCost:num
 * @text Default Item Cost
 * @parent BravePointsCosts
 * @type number
 * @min 0
 * @desc How many Brave Points does an item cost by default?
 * @default 1
 *
 * @param BravePointPredictedCost:num
 * @text Predicted Cost
 * @parent BravePointsCosts
 * @type number
 * @min 0
 * @desc What is considered predicted cost?
 * @default 1
 *
 * @param BravePointsStartBattle
 * @text Start Battle
 * @parent BravePoints
 *
 * @param BravePointStartNeutral:num
 * @text Neutral
 * @parent BravePointsStartBattle
 * @desc How many Brave Points should a battler have if the
 * battle advantage is neutral?
 * @default 0
 *
 * @param BravePointStartFavor:num
 * @text Favored
 * @parent BravePointsStartBattle
 * @desc How many Brave Points should a battler have if the
 * battle advantage is favored?
 * @default 3
 *
 * @param BravePointsRegen
 * @text Regeneration
 * @parent BravePoints
 *
 * @param BravePointRegenBase:num
 * @text Base Recovery
 * @parent BravePointsRegen
 * @type number
 * @min 0
 * @desc How many Brave Points are regenerated at the end
 * of each turn?
 * @default 1
 *
 * @param BravePointsRegenAlive:eval
 * @text Needs to be Alive?
 * @parent BravePointsRegen
 * @type boolean
 * @on Alive
 * @off Can Be Dead
 * @desc Do battlers need to be alive to regenerate Brave Points?
 * @default true
 *
 * @param ActionFusions
 * @text Action Fusions
 *
 * @param ActorActionFusions:eval
 * @text Actor Access?
 * @parent ActionFusions
 * @type boolean
 * @on Allow
 * @off Disable
 * @desc Allow actors access to Action Fusions?
 * @default true
 *
 * @param EnemyActionFusions:eval
 * @text Enemy Access?
 * @parent ActionFusions
 * @type boolean
 * @on Allow
 * @off Disable
 * @desc Allow enemies access to Action Fusions?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * BraveAnimation Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~BraveAnimation:
 *
 * @param OnBrave
 * @text On Brave
 *
 * @param BraveAnimationID:num
 * @text Animation ID
 * @parent OnBrave
 * @type animation
 * @desc Play this animation when the effect activates.
 * @default 12
 *
 * @param BraveMirror:eval
 * @text Mirror Animation
 * @parent OnBrave
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the effect animation?
 * @default false
 *
 * @param BraveMute:eval
 * @text Mute Animation
 * @parent OnBrave
 * @type boolean
 * @on Mute
 * @off Normal
 * @desc Mute the effect animation?
 * @default false
 *
 * @param CancelBrave
 * @text Cancel Brave
 *
 * @param CancelAnimationID:num
 * @text Animation ID
 * @parent CancelBrave
 * @type animation
 * @desc Play this animation when the effect activates.
 * @default 62
 *
 * @param CancelMirror:eval
 * @text Mirror Animation
 * @parent CancelBrave
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the effect animation?
 * @default false
 *
 * @param CancelMute:eval
 * @text Mute Animation
 * @parent CancelBrave
 * @type boolean
 * @on Mute
 * @off Normal
 * @desc Mute the effect animation?
 * @default false
 *
 * @param EnemyBrave
 * @text Enemy Brave
 *
 * @param ShowEnemyBrave:eval
 * @text Show Activation?
 * @parent EnemyBrave
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the enemy activating Brave?
 * @default true
 *
 * @param WaitFrames:num
 * @text Wait Frames
 * @parent EnemyBrave
 * @type number
 * @desc This is the number of frames to wait between activations.
 * @default 20
 *
 */
/* ----------------------------------------------------------------------------
 * Turn Order Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~TurnOrder:
 *
 * @param General
 *
 * @param DisplayPosition:str
 * @text Display Position
 * @parent General
 * @type select
 * @option top
 * @option bottom
 * @option left
 * @option right
 * @desc Select where the Turn Order will appear on the screen.
 * @default top
 * 
 * @param DisplayOffsetX:num
 * @text Offset X
 * @parent DisplayPosition:str
 * @desc How much to offset the X coordinate by.
 * Negative: left. Positive: right.
 * @default 0
 * 
 * @param DisplayOffsetY:num
 * @text Offset Y
 * @parent DisplayPosition:str
 * @desc How much to offset the Y coordinate by.
 * Negative: up. Positive: down.
 * @default 0
 *
 * @param CenterHorz:eval
 * @text Center Horizontal?
 * @parent DisplayPosition:str
 * @type boolean
 * @on Center
 * @off Stay
 * @desc Reposition the Turn Order Display to always be centered
 * if it is a 'top' or 'bottom' position?
 * @default true
 *
 * @param RepositionTopForHelp:eval
 * @text Reposition for Help?
 * @parent DisplayPosition:str
 * @type boolean
 * @on Reposition
 * @off Stay
 * @desc If the display position is at the top, reposition the
 * display when the help window is open?
 * @default true
 *
 * @param RepositionLogWindow:eval
 * @text Reposition Log?
 * @parent DisplayPosition:str
 * @type boolean
 * @on Reposition
 * @off Stay
 * @desc If the display position is at the top, reposition the
 * Battle Log Window to be lower?
 * @default true
 *
 * @param OrderDirection:eval
 * @text Forward Direction
 * @parent General
 * @type boolean
 * @on Left to Right / Down to Up
 * @off Right to Left / Up to Down
 * @desc Decide on the direction of the Turn Order.
 * Settings may vary depending on position.
 * @default true
 *
 * @param SubjectDistance:num
 * @text Subject Distance
 * @parent General
 * @type number
 * @desc How far do you want the currently active battler to
 * distance itself from the rest of the Turn Order?
 * @default 8
 *
 * @param ScreenBuffer:num
 * @text Screen Buffer
 * @parent General
 * @type number
 * @desc What distance do you want the display to be away
 * from the edge of the screen by?
 * @default 20
 * 
 * @param Reposition
 * @text Reposition For Help
 *
 * @param RepositionTopHelpX:num
 * @text Repostion X By
 * @parent Reposition
 * @desc Reposition the display's X coordinates by this much when
 * the Help Window is visible.
 * @default 0
 *
 * @param RepositionTopHelpY:num
 * @text Repostion Y By
 * @parent Reposition
 * @desc Reposition the display's Y coordinates by this much when
 * the Help Window is visible.
 * @default 96
 * 
 * @param Slots
 *
 * @param MaxHorzSprites:num
 * @text Max Horizontal
 * @parent Slots
 * @type number
 * @min 1
 * @desc Maximum slots you want to display for top and
 * bottom Turn Order Display positions?
 * @default 16
 *
 * @param MaxVertSprites:num
 * @text Max Vertical
 * @parent Slots
 * @type number
 * @min 1
 * @desc Maximum slots you want to display for left and
 * right Turn Order Display positions?
 * @default 10
 *
 * @param SpriteLength:num
 * @text Length
 * @parent Slots
 * @type number
 * @min 1
 * @desc How many pixels long should the slots be on the
 * Turn Order display?
 * @default 72
 *
 * @param SpriteThin:num
 * @text Thin
 * @parent Slots
 * @type number
 * @min 1
 * @desc How many pixels thin should the slots be on the
 * Turn Order display?
 * @default 36
 *
 * @param UpdateFrames:num
 * @text Update Frames
 * @parent Slots
 * @type number
 * @min 1
 * @desc How many frames should it take for the slots to
 * update their positions by?
 * @default 24
 *
 * @param Border
 * @text Slot Border
 *
 * @param ShowMarkerBorder:eval
 * @text Show Border?
 * @parent Border
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show borders for the slot sprites?
 * @default true
 *
 * @param BorderThickness:num
 * @text Border Thickness
 * @parent Markers
 * @type number
 * @min 1
 * @desc How many pixels thick should the colored portion of the border be?
 * @default 2
 *
 * @param BorderActor
 * @text Actors
 * @parent Border
 *
 * @param ActorBorderColor:str
 * @text Border Color
 * @parent BorderActor
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 4
 *
 * @param ActorSystemBorder:str
 * @text Border Skin
 * @parent BorderActor
 * @type file
 * @dir img/system/
 * @desc Optional. Place a skin on the actor borders instead of rendering them?
 * @default 
 *
 * @param BorderEnemy
 * @text Enemies
 * @parent Border
 *
 * @param EnemyBorderColor:str
 * @text Border Color
 * @parent BorderEnemy
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 2
 *
 * @param EnemySystemBorder:str
 * @text Border Skin
 * @parent BorderEnemy
 * @type file
 * @dir img/system/
 * @desc Optional. Place a skin on the enemy borders instead of rendering them?
 * @default 
 *
 * @param Sprite
 * @text Slot Sprites
 *
 * @param ActorSprite
 * @text Actors
 * @parent Sprite
 *
 * @param ActorBattlerType:str
 * @text Sprite Type
 * @parent ActorSprite
 * @type select
 * @option Face Graphic - Show the actor's face.
 * @value face
 * @option Icon - Show a specified icon.
 * @value icon
 * @option Sideview Actor - Show the actor's sideview battler.
 * @value svactor
 * @desc Select the type of sprite used for the actor graphic.
 * @default face
 *
 * @param ActorBattlerIcon:num
 * @text Default Icon
 * @parent ActorSprite
 * @desc Which icon do you want to use for actors by default?
 * @default 84
 *
 * @param EnemySprite
 * @text Enemies
 * @parent Sprite
 *
 * @param EnemyBattlerType:str
 * @text Sprite Type
 * @parent EnemySprite
 * @type select
 * @option Face Graphic - Show a specified face graphic.
 * @value face
 * @option Icon - Show a specified icon.
 * @value icon
 * @option Enemy - Show the enemy's graphic or sideview battler.
 * @value enemy
 * @desc Select the type of sprite used for the enemy graphic.
 * @default enemy
 *
 * @param EnemyBattlerFaceName:str
 * @text Default Face Name
 * @parent EnemySprite
 * @type file
 * @dir img/faces/
 * @desc Use this default face graphic if there is no specified face.
 * @default Monster
 *
 * @param EnemyBattlerFaceIndex:num
 * @text Default Face Index
 * @parent EnemySprite
 * @type number
 * @desc Use this default face index if there is no specified index.
 * @default 1
 *
 * @param EnemyBattlerIcon:num
 * @text Default Icon
 * @parent EnemySprite
 * @desc Which icon do you want to use for enemies by default?
 * @default 298
 *
 * @param EnemyBattlerMatchHue:eval
 * @text Match Hue?
 * @parent EnemySprite
 * @type boolean
 * @on Match
 * @off Don't Match
 * @desc Match the hue for enemy battlers?
 * Does not apply if there's a sideview battler.
 * @default true
 *
 * @param Letter
 * @text Slot Letter
 *
 * @param EnemyBattlerDrawLetter:eval
 * @text Show Enemy Letter?
 * @parent Letter
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the enemy's letter on the slot sprite?
 * @default true
 *
 * @param EnemyBattlerFontFace:str
 * @text Font Name
 * @parent Letter
 * @desc The font name used for the text of the Letter.
 * Leave empty to use the default game's font.
 * @default 
 *
 * @param EnemyBattlerFontSize:num
 * @text Font Size
 * @parent Letter
 * @min 1
 * @desc The font size used for the text of the Letter.
 * @default 16
 *
 * @param Background
 * @text Slot Background
 *
 * @param ShowMarkerBg:eval
 * @text Show Background?
 * @parent Background
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the background on the slot sprite?
 * @default true
 *
 * @param BackgroundActor
 * @text Actors
 * @parent Background
 *
 * @param ActorBgColor1:str
 * @text Background Color 1
 * @parent BackgroundActor
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param ActorBgColor2:str
 * @text Background Color 2
 * @parent BackgroundActor
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 9
 *
 * @param ActorSystemBg:str
 * @text Background Skin
 * @parent BackgroundActor
 * @type file
 * @dir img/system/
 * @desc Optional. Use a skin for the actor background instead of rendering them?
 * @default 
 *
 * @param BackgroundEnemy
 * @text Enemies
 * @parent Background
 *
 * @param EnemyBgColor1:str
 * @text Background Color 1
 * @parent BackgroundEnemy
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param EnemyBgColor2:str
 * @text Background Color 2
 * @parent BackgroundEnemy
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 18
 *
 * @param EnemySystemBg:str
 * @text Background Skin
 * @parent BackgroundEnemy
 * @type file
 * @dir img/system/
 * @desc Optional. Use a skin for the enemy background instead of rendering them?
 * @default 
 *
 */
/* ----------------------------------------------------------------------------
 * Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Window:
 *
 * @param Window_ActorCommand
 *
 * @param CommandName:str
 * @text Command Text
 * @parent Window_ActorCommand
 * @desc What is the text that appears for the Brave command?
 * @default Brave
 *
 * @param ShowCommand:eval
 * @text Show Command?
 * @parent Window_ActorCommand
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show the Brave command in the Actor Command Window?
 * @default true
 *
 * @param BraveShortcuts:eval
 * @text Page Up/Dn Shortcuts?
 * @parent Window_ActorCommand
 * @type boolean
 * @on Use Shortcuts
 * @off Don't Use
 * @desc Use Page Up/Down for shortcuts on activating Brave?
 * @default true
 *
 * @param DrawActionCountersJS:func
 * @text JS: Draw Counters
 * @parent Window_ActorCommand
 * @type note
 * @desc Code used to determine how the action counters are
 * displayed on the window.
 * @default "// Declare Constants\nconst sprite = arguments[0];\nconst parentWindow = arguments[1];\nconst actor = arguments[2];\n\n// Set Location\nsprite.x = Math.round(parentWindow.width / 2);\nsprite.y = 0;\nsprite.anchor.x = 0.5\nsprite.anchor.y = 0.5\n\n// Create Text\nconst textSlot = TextManager.btbActionSlot;\nconst textCurrent = TextManager.btbActionCurrent;\nlet text = textSlot.repeat(actor.numActions());\nconst index = actor._actionInputIndex;\ntext = text.substring(0, index) + textCurrent + text.substring(index + 1);\n\n// Create and Draw Bitmap\nconst bitmap = new Bitmap(parentWindow.width, parentWindow.lineHeight());\nbitmap.fontSize = 36;\nbitmap.drawText(text, 0, 0, bitmap.width, bitmap.height, 'center');\nsprite.bitmap = bitmap;"
 *
 * @param ActionSlot:str
 * @text Action Slot
 * @parent DrawActionCountersJS:func
 * @desc This is the text used to represent a non-selected action slot.
 * @default ○
 *
 * @param ActionCurrent:str
 * @text Current Action
 * @parent DrawActionCountersJS:func
 * @desc This is the text used to represent the current action slot.
 * @default ◉
 *
 * @param Window_BattleStatus
 *
 * @param StatusDisplayFmt:str
 * @text Display Format
 * @parent Window_BattleStatus
 * @desc How are actor Brave Point displayed?
 * %1 - Total BP, %2 - BP Text, %3 - Icon
 * @default \FS[16]\C[6]%2\C[0] \FS[22]%1
 *
 * @param StatusPredictFmt:str
 * @text Predict Format
 * @parent Window_BattleStatus
 * @desc How are predicted Brave Point displayed?
 * %1 - Total BP, %2 - BP Text, %3 - Icon, %4 - Predicted
 * @default \FS[16]\C[6]%2\C[0] \FS[22]%1\FS[16] → \FS[22]%4
 *
 * @param TextColors
 * @text Text Colors
 * @parent Window_BattleStatus
 *
 * @param NeutralColor:num
 * @text Neutral Color
 * @parent TextColors
 * @desc Text code color for neutral number values.
 * @default 0
 *
 * @param PositiveColor:num
 * @text Positive Color
 * @parent TextColors
 * @desc Text code color for positive number values.
 * @default 4
 *
 * @param NegativeColor:num
 * @text Negative Color
 * @parent TextColors
 * @desc Text code color for negative number values.
 * @default 2
 *
 * @param Styles
 * @text Style Settings
 * @parent Window_BattleStatus
 *
 * @param DefaultStyle
 * @text Default Style
 * @parent Styles
 *
 * @param default_display:eval
 * @text Show Display?
 * @parent DefaultStyle
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show the actor's BP values in the Battle Status Window?
 * @default true
 *
 * @param default_align:str
 * @text Alignment
 * @parent DefaultStyle
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc How do you want the actor BP values to be aligned?
 * @default right
 *
 * @param default_offsetX:num
 * @text Offset X
 * @parent DefaultStyle
 * @desc Offset the actor BP display X by how many pixels?
 * @default 16
 *
 * @param default_offsetY:num
 * @text Offset Y
 * @parent DefaultStyle
 * @desc Offset the actor BP display Y by how many pixels?
 * @default 0
 *
 * @param ListStyle
 * @text List Style
 * @parent Styles
 *
 * @param list_display:eval
 * @text Show Display?
 * @parent ListStyle
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show the actor's BP values in the Battle Status Window?
 * @default true
 *
 * @param list_align:str
 * @text Alignment
 * @parent ListStyle
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc How do you want the actor BP values to be aligned?
 * @default left
 *
 * @param list_offsetX:num
 * @text Offset X
 * @parent ListStyle
 * @desc Offset the actor BP display X by how many pixels?
 * @default -8
 *
 * @param list_offsetY:num
 * @text Offset Y
 * @parent ListStyle
 * @desc Offset the actor BP display Y by how many pixels?
 * @default 0
 *
 * @param XPStyle
 * @text XP Style
 * @parent Styles
 *
 * @param xp_display:eval
 * @text Show Display?
 * @parent XPStyle
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show the actor's BP values in the Battle Status Window?
 * @default true
 *
 * @param xp_align:str
 * @text Alignment
 * @parent XPStyle
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc How do you want the actor BP values to be aligned?
 * @default right
 *
 * @param xp_offsetX:num
 * @text Offset X
 * @parent XPStyle
 * @desc Offset the actor BP display X by how many pixels?
 * @default 16
 *
 * @param xp_offsetY:num
 * @text Offset Y
 * @parent XPStyle
 * @desc Offset the actor BP display Y by how many pixels?
 * @default 0
 *
 * @param PortraitStyle
 * @text Portrait Style
 * @parent Styles
 *
 * @param portrait_display:eval
 * @text Show Display?
 * @parent PortraitStyle
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show the actor's BP values in the Battle Status Window?
 * @default true
 *
 * @param portrait_align:str
 * @text Alignment
 * @parent PortraitStyle
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc How do you want the actor BP values to be aligned?
 * @default right
 *
 * @param portrait_offsetX:num
 * @text Offset X
 * @parent PortraitStyle
 * @desc Offset the actor BP display X by how many pixels?
 * @default -8
 *
 * @param portrait_offsetY:num
 * @text Offset Y
 * @parent PortraitStyle
 * @desc Offset the actor BP display Y by how many pixels?
 * @default 56
 *
 * @param BorderStyle
 * @text Border Style
 * @parent Styles
 *
 * @param border_display:eval
 * @text Show Display?
 * @parent BorderStyle
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show the actor's BP values in the Battle Status Window?
 * @default true
 *
 * @param border_align:str
 * @text Alignment
 * @parent BorderStyle
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc How do you want the actor BP values to be aligned?
 * @default right
 *
 * @param border_offsetX:num
 * @text Offset X
 * @parent BorderStyle
 * @desc Offset the actor BP display X by how many pixels?
 * @default 16
 *
 * @param border_offsetY:num
 * @text Offset Y
 * @parent BorderStyle
 * @desc Offset the actor BP display Y by how many pixels?
 * @default 0
 *
 */
//=============================================================================

const _0x2b07=['_bypassAiValidCheck','isUsePageUpDnShortcutBTB','_graphicSprite','getOffsetY_BTB','clearActions','btbPayItemFusionCosts','HideBravePointCost','onDatabaseLoaded','_isAlive','applyBattleItemWindowBTB','CannotFusion','BravePointRegenBase','1011pybjTt','BravePointSetUser','_graphicType','EVAL','remove','_fullHeight','process_VisuMZ_BattleSystemBTB_JS','removeActionBattlersBTB','containerPosition','ActorBattlerType','Scene_Battle_createActorCommandWindow','calcRegenBravePoints','Scene_Boot_onDatabaseLoaded','optDisplayTp','checkOpacity','BraveAnimation','getAlignmentBTB','makeSpeed','%1_offsetX','startActionBTB','ScreenBuffer','%1BgColor2','showBravePoints','isSkill','index','BattleLayout','_btbTurnOrderFaceName','ItemScene','11lwfVai','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','MaxActionsHardCap','Enemies','CommandName','min','clearRect','_fadeDuration','ShowEnemyBrave','Brave','createBackgroundSprite','_btbTurnOrderFaceIndex','parameters','actor','isSideView','_tempBattler','battlerHue','BattleCore','Enemy-%1-%2','singleSkill','ActionSlot','_btbTurnOrderGraphicType','createActorCommandWindowBTB','ReduceShownBPCost','textSizeEx','MaxBravePointsHardCap','isForFriend','getActionFusionRecipeSkills','PositiveColor','DisplayPosition','ItemsEquipsCore','_homeDuration','BravePointsAbbr','gradientFillRect','TurnOrderBTBGraphicType','StatusPredictFmt','OrderDirection','maxBattleMembers','JsBravePointsTarget','constructor','_unit','_windowLayer','makeDeepCopy','updateOpacity','parse','Game_Action_applyItemUserEffect','isItem','canAddBraveCommand','top','BraveShortcuts','icon','RepositionTopForHelp','drawText','onTurnEnd','startInput','drawItemStatusListStyle','visible','Window_Selectable_select','clamp','cursorPageup','length','floor','BTB_MAX_BRAVEPOINTS_DEFAULT','filter','BravePointBattleStart','_positionTargetY','_logWindow','createBattlerRect','guardSkillId','update','canBrave','updateTurnOrderBTB','split','Game_Action_allowRandomSpeed','getActionFusionRecipeItems','addGuardCommand','itemLineRect','BattleManager_startInput','24721LdDCXN','queueBraveAnimationsBTB','btbActionCurrent','_graphicHue','_letter','CancelAnimationID','attackSkillId','1VhRqcR','sort','_scene','_isAppeared','makeAdditionalSkillCostText','repeat','name','fontSize','Actor-%1-%2','TurnOrderBTBGraphicFaceIndex','BravePointAlterTarget','_btbSkillStrictFusion','54318YKeNfH','initHomePositions','getSkillIdWithName','toUpperCase','battlerName','setBlendColor','Show_1_BP_Cost','%1Mute','%1BorderColor','subject','BtbTurnOrderClearEnemyGraphic','btbBravePointsAbbr','_ogWindowLayerX','getChildIndex','loadSvActor','version','setGuard','_btbTurnOrderWindow','process_VisuMZ_BattleSystemBTB_Notetags','_targetIndex','setBattleSystemBTBTurnOrderVisible','_btbActionSprite','STRUCT','_positionDuration','_targetHomeY','MaxVertSprites','_graphicSv','btbRegisterFusions','BTB_MAX_ACTIONS_HARD_CAP','SubjectDistance','_skillIDs','startTurn','checkTargetPositions','getTotalActionFusionRecipes','_actionFusionRecipe','makeActions','traitObjects','maxBravePoints','createBattlerSprites','max','clearTurnOrderBTBGraphics','MaxHorzSprites','checkPosition','initMembers','includes','EnemyBattlerFaceIndex','removeChild','_backgroundSprite','BattleSystemBTB','EnemyActionFusions','BTB','makeAdditionalCostTextBTB','bitmapWidth','commandBrave','bravePoints','_graphicFaceIndex','Window_Base_drawItemNumber','Game_BattlerBase_canGuard','Window_BattleLog_startAction','close','ShowCostForGuard','Enemy','_graphicIconIndex','setBravePoints','%1_offsetY','setItem','Parse_Notetags_BravePointsUserJS','drawTextEx','_scrollY','_graphicEnemy','onTurnEndBTB','applyItemUserEffect','BTB_MIN_BRAVEPOINTS_DEFAULT','Game_Battler_useItem','makeCommandList','updateVisibility','canActionFusionWithBTB','_plural','fillRect','create','createGraphicSprite','bitmapHeight','addBraveCommand','_homeY','createBorderSprite','prototype','cursorPagedown','Window_Help_setItem','isEnemy','getActionFusionCombinationsBTB','isTpb','boxHeight','Window_ActorCommand_addGuardCommand','_braveStartupAnimation','needsSelection','anchor','BTB_Help','MinBravePointsHardCap','TurnOrderBTBGraphicIconIndex','Item-%1-%2','_armors','_graphicFaceName','btbActionSlot','registerCommand','setSkill','setAttack','getColor','bravePointsCost','Window_Base_makeAdditionalSkillCostText','removeActor','CostPosition','UpdateFrames','contents','ARRAYFUNC','Game_Enemy_makeActions','payBravePointsCost','predictedBravePointCost','updatePosition','calculateTargetPositions','push','canUse','Game_Actor_makeActions','loseBravePoints','ceil','getBattleSystem','allowRandomSpeed','updateHomePosition','Window_BattleStatus_drawItemStatusListStyle','BravePointCost','requestRefresh','Game_BattlerBase_canUse','item','isInputting','applyItemBattleSystemBTBUserEffect','BtbTurnOrderActorIcon','brave','isActor','changeEnemyGraphicBitmap','41uQXtkT','_positionTargetX','loadSvEnemy','SpriteThin','center','gainBravePoints','opacity','text','canInput','updatePadding','EnableFusion','_btbTurnOrderVisible','Settings','_ogWindowLayerY','speed','updateSelectionEffect','TurnOrderBTBGraphicFaceName','guard','cancel','isBattleSystemBTBTurnOrderVisible','predictedBravePoints','members','createLetterSprite','resetFontSettings','mainFontFace','hide','BravePointsRegenAlive','EnemyBattlerDrawLetter','canProcessActionFusionsBTB','process_VisuMZ_BattleSystemBTB','sortActionOrdersBTB','pop','ShowCostForAttack','BtbTurnOrderEnemyFace','isHorz','BravePointPredictedCost','\x5cC[%1]%2\x5cC[0]','Game_System_initialize','DisplayOffsetY','description','_turnOrderContainer','%1-%2','allBattleMembers','destroyBTBActionCounters','slice','BTB_MAX_BRAVEPOINTS_HARD_CAP','createBTBTurnOrderWindow','createTurnOrderBTBGraphicType','\x5cI[%1]%2','ItemQuantityFontSize','Show_0_BP_Cost','setHandler','drawItemStatusXPStyle','RepositionTopHelpX','MaxBravePoints','loadSystem','_actions','IconSet','%1AnimationID','onBattleStartBTB','blt','braveAnimationTimes','faceWidth','State-%1-%2','btbMatchesCurrentFusionAction','reduceBrave','Window_BattleStatus_drawItemStatusXPStyle','HideBrave','commandCancelBTB','recalculateHome','numActions','FaceIndex','ParseSkillNotetags','_actorCommandWindow','FusionFlex','Weapon-%1-%2','createAllWindows','Game_Action_setSkill','svactor','indexOf','useItemBTB','ActorBattlerIcon','\x5cI[%1]','createKeyJS','ShowCommand','createTurnOrderBTBGraphicIconIndex','isAppeared','getItemIdWithName','General','useItem','_items','skillCostSeparator','addCommand','createChildren','ParseAllNotetags','setText','isSTB','maxBraveActions','battler','FUNC','round','_statusWindow','showBraveAnimationBTB','createTurnOrderBTBGraphicFaceName','Actor','updateBattleContainerOrder','_homeX','updateGraphicHue','BravePointAlterUser','BattleManager_startAction','Window_Base_close','createTurnOrderBTBGraphicFaceIndex','showNormalAnimation','status','map','clear','canPayActionFusionCombination','repositionLogWindowBTB','shift','bottom','some','JsBravePointsUser','requestFauxAnimation','iconHeight','ActionCurrent','width','defaultPosition','processUpdateGraphic','boxWidth','setup','ActorActionFusions','IconIndex','BraveAnimationID','_scrollX','Window','getOffsetX_BTB','initialize','initBattleSystemBTB','JSON','enemy','RegExp','updateTurnOrder','BattleManager_isTurnBased','processActionFusionsBTB','changeFaceGraphicBitmap','Mechanics','Class-%1-%2','addChild','destroy','startAction','_btbItemStrictFusion','refresh','_btbTurnOrderIconIndex','selectNextCommand','203961fzGZXo','Scene_Battle_onDisabledPartyCommandSelection','waitForAnimation','containerWindow','%1SystemBg','_bravePoints','changeSvActorGraphicBitmap','removeActionFusionIngredients','trim','BravePointItemCost','regenerateBravePoints','_position','waitCount','RepositionLogWindow','NeutralColor','1674pzOwEu','BTB_MIN_BRAVEPOINTS_HARD_CAP','BtbTurnOrderActorFace','Game_BattlerBase_canInput','inputtingAction','Game_Battler_makeActionTimes','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','battleLayoutStyle','itemRectPortraitBTB','CalcActionSpeedJS','_fadeTarget','TurnOrder','bind','left','Scene_Battle_createAllWindows','MaxBravePointsDefault','cannotFusionNotetagBTB','hasSvBattler','BravePointRegen','cancelBrave','Game_BattlerBase_hide','battleSys','Visible','_helpWindow','Game_Battler_onBattleStart','_actor','createInitialPositions','_subject','substring','isAlive','BtbTurnOrderClearActorGraphic','canGuard','updateGraphic','modifyBTBActionCounterSprite','compareBattlerSprites','Game_BattlerBase_appear','ConvertParams','FusionStrict','%1SystemBorder','Game_Action_setItem','commandStyle','478JJIczd','EnemyBattlerType','splice','BtbTurnOrderEnemyIcon','BTB_MAX_ACTIONS_DEFAULT','_btbSkillFlexFusion','_targetHomeX','ShowMarkerBorder','lineHeight','BorderThickness','2045094tbLgNq','Actors','Game_Battler_onTurnEnd','ARRAYNUM','changeIconGraphicBitmap','attack','isBTB','fontFace','btbBravePointsIcon','EnemyBattlerFaceName','commandCancel','iconWidth','startFade','performBrave','_containerHeight','Cancel','Game_Action_speed','CenterHorz','face','setActionFusionBTB','_turnOrderInnerSprite','_isBattleOver','ARRAYJSON','currentAction','%1Mirror','BattleManager_makeActionOrders','ItemQuantityFmt','isSceneBattle','MaxActionsDefault','onBattleStart','addLoadListener','applyBattleSystemBTBUserEffect','ParseItemNotetags','faceHeight','exit','_btbItemFlexFusion','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','formFlexCombo','onDisabledPartyCommandSelection','SpriteLength','makeMultiActionsBTB','EnemyBattlerIcon','_phase','faceIndex','svBattlerName','makeActionTimes','right','drawActorBravePoints','BattleManager_startTurn','ARRAYEVAL','EnemyMultiAction','cannotBraveTrait','active','_letterSprite','btbParseFusionData','isBattleItemWindowBTB','Game_Battler_performCollapse','Game_Party_removeActor','BravePointCostFmt','call','windowRect','hideBraveTrait','BravePointStartFavor','setHue','_actionBattlers','296497rAVgNd','223GAmOHz','FaceName','loadFace','textWidth','minBravePoints','Window_ActorCommand_makeCommandList','isTurnBased','select','height','btbPaySkillFusionCosts','itemRect','modifyBTBActionCounterSprite_Fallback','btbBravePointsFull','bitmap','drawItemNumber','drawItemNumberBTB','concat','format','_itemIDs','match','BravePointsFull','note','_index','currentSymbol','makeActionOrders','join'];const _0x58f0=function(_0x40be41,_0x1d264f){_0x40be41=_0x40be41-0x98;let _0x2b07b2=_0x2b07[_0x40be41];return _0x2b07b2;};const _0xfb7938=_0x58f0;(function(_0x2a71f7,_0x21dcfc){const _0x321401=_0x58f0;while(!![]){try{const _0x4092dc=-parseInt(_0x321401(0x23d))*parseInt(_0x321401(0x29e))+-parseInt(_0x321401(0x1fa))+-parseInt(_0x321401(0xdd))*parseInt(_0x321401(0x28b))+-parseInt(_0x321401(0x1af))*-parseInt(_0x321401(0x186))+parseInt(_0x321401(0x221))*-parseInt(_0x321401(0x1fb))+parseInt(_0x321401(0x292))*-parseInt(_0x321401(0x177))+parseInt(_0x321401(0x1b9));if(_0x4092dc===_0x21dcfc)break;else _0x2a71f7['push'](_0x2a71f7['shift']());}catch(_0x1d7525){_0x2a71f7['push'](_0x2a71f7['shift']());}}}(_0x2b07,0x7c188));var label=_0xfb7938(0x2ce),tier=tier||0x0,dependencies=[],pluginData=$plugins['filter'](function(_0x3bb42d){const _0x9c00c3=_0xfb7938;return _0x3bb42d[_0x9c00c3(0x14e)]&&_0x3bb42d[_0x9c00c3(0x104)][_0x9c00c3(0x2ca)]('['+label+']');})[0x0];VisuMZ[label][_0xfb7938(0xe9)]=VisuMZ[label][_0xfb7938(0xe9)]||{},VisuMZ[_0xfb7938(0x1aa)]=function(_0x1f04a3,_0x5bb10e){const _0x2c1332=_0xfb7938;for(const _0x17eb61 in _0x5bb10e){if(_0x17eb61['match'](/(.*):(.*)/i)){const _0x5704c5=String(RegExp['$1']),_0x5c0893=String(RegExp['$2'])[_0x2c1332(0x2a1)]()[_0x2c1332(0x17f)]();let _0x3d5647,_0x2a0b23,_0x273edd;switch(_0x5c0893){case'NUM':_0x3d5647=_0x5bb10e[_0x17eb61]!==''?Number(_0x5bb10e[_0x17eb61]):0x0;break;case _0x2c1332(0x1bc):_0x2a0b23=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):[],_0x3d5647=_0x2a0b23[_0x2c1332(0x14f)](_0x1d3c68=>Number(_0x1d3c68));break;case _0x2c1332(0x224):_0x3d5647=_0x5bb10e[_0x17eb61]!==''?eval(_0x5bb10e[_0x17eb61]):null;break;case _0x2c1332(0x1ea):_0x2a0b23=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):[],_0x3d5647=_0x2a0b23[_0x2c1332(0x14f)](_0x74d967=>eval(_0x74d967));break;case _0x2c1332(0x167):_0x3d5647=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):'';break;case _0x2c1332(0x1cf):_0x2a0b23=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):[],_0x3d5647=_0x2a0b23[_0x2c1332(0x14f)](_0x388e06=>JSON[_0x2c1332(0x269)](_0x388e06));break;case _0x2c1332(0x140):_0x3d5647=_0x5bb10e[_0x17eb61]!==''?new Function(JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61])):new Function('return\x200');break;case _0x2c1332(0xc4):_0x2a0b23=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):[],_0x3d5647=_0x2a0b23[_0x2c1332(0x14f)](_0xb6b6ad=>new Function(JSON[_0x2c1332(0x269)](_0xb6b6ad)));break;case'STR':_0x3d5647=_0x5bb10e[_0x17eb61]!==''?String(_0x5bb10e[_0x17eb61]):'';break;case'ARRAYSTR':_0x2a0b23=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):[],_0x3d5647=_0x2a0b23['map'](_0xe06c84=>String(_0xe06c84));break;case _0x2c1332(0x2b4):_0x273edd=_0x5bb10e[_0x17eb61]!==''?JSON['parse'](_0x5bb10e[_0x17eb61]):{},_0x3d5647=VisuMZ[_0x2c1332(0x1aa)]({},_0x273edd);break;case'ARRAYSTRUCT':_0x2a0b23=_0x5bb10e[_0x17eb61]!==''?JSON[_0x2c1332(0x269)](_0x5bb10e[_0x17eb61]):[],_0x3d5647=_0x2a0b23[_0x2c1332(0x14f)](_0x25953b=>VisuMZ['ConvertParams']({},JSON[_0x2c1332(0x269)](_0x25953b)));break;default:continue;}_0x1f04a3[_0x5704c5]=_0x3d5647;}}return _0x1f04a3;},(_0x4e69a4=>{const _0xd8b5d4=_0xfb7938,_0x5db4a2=_0x4e69a4[_0xd8b5d4(0x298)];for(const _0x22c3fa of dependencies){if(!Imported[_0x22c3fa]){alert(_0xd8b5d4(0x23e)[_0xd8b5d4(0x20c)](_0x5db4a2,_0x22c3fa)),SceneManager[_0xd8b5d4(0x1db)]();break;}}const _0x541dce=_0x4e69a4[_0xd8b5d4(0x104)];if(_0x541dce[_0xd8b5d4(0x20e)](/\[Version[ ](.*?)\]/i)){const _0x222949=Number(RegExp['$1']);_0x222949!==VisuMZ[label][_0xd8b5d4(0x2ad)]&&(alert(_0xd8b5d4(0x1dd)[_0xd8b5d4(0x20c)](_0x5db4a2,_0x222949)),SceneManager[_0xd8b5d4(0x1db)]());}if(_0x541dce[_0xd8b5d4(0x20e)](/\[Tier[ ](\d+)\]/i)){const _0x500183=Number(RegExp['$1']);_0x500183<tier?(alert(_0xd8b5d4(0x18c)['format'](_0x5db4a2,_0x500183,tier)),SceneManager[_0xd8b5d4(0x1db)]()):tier=Math[_0xd8b5d4(0x2c5)](_0x500183,tier);}VisuMZ[_0xd8b5d4(0x1aa)](VisuMZ[label][_0xd8b5d4(0xe9)],_0x4e69a4[_0xd8b5d4(0x249)]);})(pluginData),PluginManager[_0xfb7938(0xba)](pluginData[_0xfb7938(0x298)],_0xfb7938(0xd9),_0x388a6f=>{const _0x3967c4=_0xfb7938;VisuMZ['ConvertParams'](_0x388a6f,_0x388a6f);const _0x23f5ee=_0x388a6f[_0x3967c4(0x1ba)],_0x13c480=_0x388a6f[_0x3967c4(0x160)];for(const _0x18274f of _0x23f5ee){const _0x187236=$gameActors[_0x3967c4(0x24a)](_0x18274f);if(!_0x187236)continue;_0x187236[_0x3967c4(0x252)]=_0x3967c4(0x26f),_0x187236[_0x3967c4(0x175)]=_0x13c480;}}),PluginManager[_0xfb7938(0xba)](pluginData[_0xfb7938(0x298)],_0xfb7938(0x188),_0x5c6513=>{const _0x3f9d74=_0xfb7938;VisuMZ[_0x3f9d74(0x1aa)](_0x5c6513,_0x5c6513);const _0x44e36d=_0x5c6513[_0x3f9d74(0x1ba)],_0x2ba86c=_0x5c6513[_0x3f9d74(0x1fc)],_0x32b13d=_0x5c6513[_0x3f9d74(0x124)];for(const _0x3af57a of _0x44e36d){const _0x515426=$gameActors[_0x3f9d74(0x24a)](_0x3af57a);if(!_0x515426)continue;_0x515426[_0x3f9d74(0x252)]=_0x3f9d74(0x1cb),_0x515426[_0x3f9d74(0x23b)]=_0x2ba86c,_0x515426['_btbTurnOrderFaceIndex']=_0x32b13d;}}),PluginManager['registerCommand'](pluginData[_0xfb7938(0x298)],_0xfb7938(0x1a4),_0x3beee1=>{const _0x322b7f=_0xfb7938;VisuMZ[_0x322b7f(0x1aa)](_0x3beee1,_0x3beee1);const _0x1da145=_0x3beee1[_0x322b7f(0x1ba)];for(const _0x1d4fb1 of _0x1da145){const _0x59d1a7=$gameActors[_0x322b7f(0x24a)](_0x1d4fb1);if(!_0x59d1a7)continue;_0x59d1a7[_0x322b7f(0x2c6)]();}}),PluginManager[_0xfb7938(0xba)](pluginData[_0xfb7938(0x298)],_0xfb7938(0x1b2),_0x57a801=>{const _0x454107=_0xfb7938;VisuMZ[_0x454107(0x1aa)](_0x57a801,_0x57a801);const _0x29c118=_0x57a801[_0x454107(0x240)],_0x4d7595=_0x57a801[_0x454107(0x160)];for(const _0x503e2e of _0x29c118){const _0x350b57=$gameTroop[_0x454107(0xf2)]()[_0x503e2e];if(!_0x350b57)continue;_0x350b57['_btbTurnOrderGraphicType']='icon',_0x350b57['_btbTurnOrderIconIndex']=_0x4d7595;}}),PluginManager['registerCommand'](pluginData[_0xfb7938(0x298)],_0xfb7938(0xfe),_0x236389=>{const _0x54a111=_0xfb7938;VisuMZ[_0x54a111(0x1aa)](_0x236389,_0x236389);const _0x41c413=_0x236389[_0x54a111(0x240)],_0x1cb54e=_0x236389[_0x54a111(0x1fc)],_0x53f393=_0x236389['FaceIndex'];for(const _0x142190 of _0x41c413){const _0x399dcc=$gameTroop['members']()[_0x142190];if(!_0x399dcc)continue;_0x399dcc['_btbTurnOrderGraphicType']=_0x54a111(0x1cb),_0x399dcc[_0x54a111(0x23b)]=_0x1cb54e,_0x399dcc[_0x54a111(0x248)]=_0x53f393;}}),PluginManager['registerCommand'](pluginData[_0xfb7938(0x298)],_0xfb7938(0x2a8),_0x26ade0=>{const _0x2ec882=_0xfb7938;VisuMZ[_0x2ec882(0x1aa)](_0x26ade0,_0x26ade0);const _0x13c24d=_0x26ade0[_0x2ec882(0x240)];for(const _0x194fab of _0x13c24d){const _0x3d0d76=$gameTroop[_0x2ec882(0xf2)]()[_0x194fab];if(!_0x3d0d76)continue;_0x3d0d76[_0x2ec882(0x2c6)]();}}),PluginManager['registerCommand'](pluginData[_0xfb7938(0x298)],'SystemTurnOrderVisibility',_0x846520=>{const _0x239891=_0xfb7938;VisuMZ[_0x239891(0x1aa)](_0x846520,_0x846520);const _0x2f737b=_0x846520[_0x239891(0x19c)];$gameSystem[_0x239891(0x2b2)](_0x2f737b);}),VisuMZ[_0xfb7938(0x2ce)]['RegExp']={'EnemyMultiAction':/<BTB (?:MULTI|MULTIPLE) (?:ACTION|ACTIONS):[ ](.*)>/i,'BravePointCost':/<BTB (?:BRAVE|BP) COST:[ ](\d+)>/i,'BravePointSetUser':/<BTB USER SET (?:BRAVE|BP):[ ](\d+)>/i,'BravePointSetTarget':/<BTB TARGET SET (?:BRAVE|BP):[ ](\d+)>/i,'BravePointAlterUser':/<BTB USER (?:GAIN|LOSE) (?:BRAVE|BP):[ ]([\+\-]\d+)>/i,'BravePointAlterTarget':/<BTB TARGET (?:GAIN|LOSE) (?:BRAVE|BP):[ ]([\+\-]\d+)>/i,'HideBravePointCost':/<BTB HIDE (?:BRAVE|BP) COST>/i,'BTB_Help':/<BTB HELP>\s*([\s\S]*)\s*<\/BTB HELP>/i,'FusionFlex':/<BTB (?:FLEX|FLEXIBLE) FUSION:[ ](.*)>/gi,'FusionStrict':/<BTB (?:STRICT|EXACT) FUSION:[ ](.*)>/gi,'JsBravePointsUser':/<JS BTB USER (?:BRAVE|BP)>\s*([\s\S]*)\s*<\/JS BTB USER (?:BRAVE|BP)>/i,'JsBravePointsTarget':/<JS BTB TARGET (?:BRAVE|BP)>\s*([\s\S]*)\s*<\/JS BTB TARGET (?:BRAVE|BP)>/i,'BravePointBattleStart':/<BTB INITIAL (?:BRAVE|BP):[ ]([\+\-]\d+)>/i,'BravePointRegen':/<BTB (?:BRAVE|BP) (?:REGEN|DEGEN):[ ]([\+\-]\d+)>/i,'MaxBravePoints':/<BTB (?:MAXIMUM|MAX) (?:BRAVE|BP):[ ]([\+\-]\d+)>/i,'MinBravePoints':/<BTB (?:MINIMUM|MIN) (?:BRAVE|BP):[ ]([\+\-]\d+)>/i,'MaxActions':/<BTB (?:MAXIMUM|MAX) (?:ACTION|ACTIONS):[ ]([\+\-]\d+)>/i,'CannotBrave':/<BTB CANNOT BRAVE>/i,'HideBrave':/<BTB HIDE BRAVE>/i,'CannotFusion':/<BTB CANNOT FUSION>/i,'EnableFusion':/<BTB ENABLE FUSION>/i},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x22d)]=Scene_Boot['prototype'][_0xfb7938(0x21c)],Scene_Boot[_0xfb7938(0xa8)][_0xfb7938(0x21c)]=function(){const _0x5063ba=_0xfb7938;VisuMZ[_0x5063ba(0x2ce)][_0x5063ba(0x22d)]['call'](this),this[_0x5063ba(0xfa)]();},Scene_Boot[_0xfb7938(0xa8)][_0xfb7938(0xfa)]=function(){this['process_VisuMZ_BattleSystemBTB_Notetags'](),this['process_VisuMZ_BattleSystemBTB_JS']();},Scene_Boot[_0xfb7938(0xa8)][_0xfb7938(0x2b0)]=function(){const _0x4e633d=_0xfb7938;if(VisuMZ['ParseAllNotetags'])return;const _0x359e38=$dataSkills[_0x4e633d(0x20b)]($dataItems);for(const _0x3d91c2 of _0x359e38){if(!_0x3d91c2)continue;DataManager[_0x4e633d(0x2b9)](_0x3d91c2);}},VisuMZ[_0xfb7938(0x2ce)]['JS']={},Scene_Boot['prototype'][_0xfb7938(0x227)]=function(){const _0x5c10a9=_0xfb7938;if(VisuMZ[_0x5c10a9(0x13b)])return;const _0x32e3a6=VisuMZ[_0x5c10a9(0x2ce)][_0x5c10a9(0x169)],_0x4659f3=$dataSkills['concat'](dataItems);for(const _0x4dbcb4 of _0x4659f3){if(!_0x4dbcb4)continue;VisuMZ[_0x5c10a9(0x2ce)][_0x5c10a9(0x2e0)](_0x4dbcb4,_0x5c10a9(0x156)),VisuMZ[_0x5c10a9(0x2ce)][_0x5c10a9(0x2e0)](_0x4dbcb4,_0x5c10a9(0x263));}},VisuMZ['BattleSystemBTB'][_0xfb7938(0x2e0)]=function(_0x49311b,_0x42fff6){const _0x12e20f=_0xfb7938,_0x30fc04=VisuMZ[_0x12e20f(0x2ce)]['RegExp'][_0x42fff6],_0x4f773d=_0x49311b[_0x12e20f(0x210)];if(_0x4f773d['match'](_0x30fc04)){const _0x2a1dde=String(RegExp['$1']),_0x5e205e='\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Declare\x20Variables\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20arguments[0];\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20arguments[1];\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20value\x20=\x20arguments[2];\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Process\x20Code\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Return\x20Value\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20return\x20value;\x0a\x20\x20\x20\x20\x20\x20\x20\x20'[_0x12e20f(0x20c)](_0x2a1dde),_0x1d6bac=VisuMZ[_0x12e20f(0x2ce)][_0x12e20f(0x130)](_0x49311b,_0x42fff6);VisuMZ[_0x12e20f(0x2ce)]['JS'][_0x1d6bac]=new Function(_0x5e205e);}},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x130)]=function(_0x745614,_0x5de3f8){const _0x30b90a=_0xfb7938;let _0x2af760='';if($dataActors[_0x30b90a(0x2ca)](_0x745614))_0x2af760=_0x30b90a(0x29a)['format'](_0x745614['id'],_0x5de3f8);if($dataClasses[_0x30b90a(0x2ca)](_0x745614))_0x2af760=_0x30b90a(0x16f)[_0x30b90a(0x20c)](_0x745614['id'],_0x5de3f8);if($dataSkills[_0x30b90a(0x2ca)](_0x745614))_0x2af760='Skill-%1-%2'['format'](_0x745614['id'],_0x5de3f8);if($dataItems[_0x30b90a(0x2ca)](_0x745614))_0x2af760=_0x30b90a(0xb6)[_0x30b90a(0x20c)](_0x745614['id'],_0x5de3f8);if($dataWeapons[_0x30b90a(0x2ca)](_0x745614))_0x2af760=_0x30b90a(0x128)['format'](_0x745614['id'],_0x5de3f8);if($dataArmors['includes'](_0x745614))_0x2af760='Armor-%1-%2'['format'](_0x745614['id'],_0x5de3f8);if($dataEnemies['includes'](_0x745614))_0x2af760=_0x30b90a(0x24f)[_0x30b90a(0x20c)](_0x745614['id'],_0x5de3f8);if($dataStates['includes'](_0x745614))_0x2af760=_0x30b90a(0x11c)[_0x30b90a(0x20c)](_0x745614['id'],_0x5de3f8);return _0x2af760;},VisuMZ[_0xfb7938(0x2ce)]['ParseSkillNotetags']=VisuMZ[_0xfb7938(0x125)],VisuMZ[_0xfb7938(0x125)]=function(_0x76fe9f){const _0x213ba5=_0xfb7938;VisuMZ[_0x213ba5(0x2ce)][_0x213ba5(0x125)]['call'](this,_0x76fe9f),DataManager[_0x213ba5(0x2b9)](_0x76fe9f),VisuMZ[_0x213ba5(0x2ce)][_0x213ba5(0x2e0)](_0x76fe9f,_0x213ba5(0x156)),VisuMZ[_0x213ba5(0x2ce)][_0x213ba5(0x2e0)](_0x76fe9f,_0x213ba5(0x263));},VisuMZ['BattleSystemBTB'][_0xfb7938(0x1d9)]=VisuMZ['ParseItemNotetags'],VisuMZ[_0xfb7938(0x1d9)]=function(_0x56dc59){const _0x1049d6=_0xfb7938;VisuMZ[_0x1049d6(0x2ce)][_0x1049d6(0x1d9)]['call'](this,_0x56dc59),DataManager[_0x1049d6(0x2b9)](_0x56dc59),VisuMZ[_0x1049d6(0x2ce)][_0x1049d6(0x2e0)](_0x56dc59,'JsBravePointsUser'),VisuMZ[_0x1049d6(0x2ce)]['Parse_Notetags_BravePointsUserJS'](_0x56dc59,'JsBravePointsTarget');},DataManager[_0xfb7938(0x2a0)]=function(_0x5a833a){const _0x98e6a5=_0xfb7938;_0x5a833a=_0x5a833a[_0x98e6a5(0x2a1)]()['trim'](),this[_0x98e6a5(0x2bc)]=this[_0x98e6a5(0x2bc)]||{};if(this['_skillIDs'][_0x5a833a])return this[_0x98e6a5(0x2bc)][_0x5a833a];for(const _0x391a83 of $dataSkills){if(!_0x391a83)continue;this[_0x98e6a5(0x2bc)][_0x391a83['name']['toUpperCase']()['trim']()]=_0x391a83['id'];}return this['_skillIDs'][_0x5a833a]||0x0;},DataManager[_0xfb7938(0x134)]=function(_0x22180b){const _0x1ca167=_0xfb7938;_0x22180b=_0x22180b[_0x1ca167(0x2a1)]()['trim'](),this[_0x1ca167(0x20d)]=this[_0x1ca167(0x20d)]||{};if(this[_0x1ca167(0x20d)][_0x22180b])return this['_itemIDs'][_0x22180b];for(const _0x4f99ce of $dataItems){if(!_0x4f99ce)continue;this['_itemIDs'][_0x4f99ce[_0x1ca167(0x298)][_0x1ca167(0x2a1)]()['trim']()]=_0x4f99ce['id'];}return this['_itemIDs'][_0x22180b]||0x0;},DataManager['_btbSkillFlexFusion']={},DataManager[_0xfb7938(0x29d)]={},DataManager[_0xfb7938(0x1dc)]={},DataManager['_btbItemStrictFusion']={},DataManager[_0xfb7938(0x2b9)]=function(_0x1ab7fc){const _0x3caa89=_0xfb7938;if(!_0x1ab7fc)return;const _0x5371be=VisuMZ['BattleSystemBTB'][_0x3caa89(0x169)],_0x1fc8cf=_0x1ab7fc['note'],_0x4bc89b=DataManager[_0x3caa89(0x238)](_0x1ab7fc),_0xa06114=_0x1fc8cf[_0x3caa89(0x20e)](_0x5371be[_0x3caa89(0x127)]);if(_0xa06114)for(const _0x10ecc5 of _0xa06114){if(!_0x10ecc5)continue;_0x10ecc5[_0x3caa89(0x20e)](_0x5371be[_0x3caa89(0x127)]);const _0x17248c=String(RegExp['$1'])[_0x3caa89(0x285)](','),_0x1e6ce5=this[_0x3caa89(0x1ef)](_0x17248c,_0x4bc89b)[_0x3caa89(0x293)]((_0x5c1968,_0x52eb45)=>_0x5c1968-_0x52eb45);if(_0x1e6ce5['length']<=0x1)continue;const _0x398c5a=_0x1e6ce5[_0x3caa89(0x214)]('-'),_0x23b3ff=_0x4bc89b?DataManager[_0x3caa89(0x1b4)]:DataManager[_0x3caa89(0x1dc)];_0x23b3ff[_0x398c5a]=_0x1ab7fc['id'];}const _0x5d69cf=_0x1fc8cf[_0x3caa89(0x20e)](_0x5371be[_0x3caa89(0x1ab)]);if(_0x5d69cf)for(const _0x35decc of _0x5d69cf){if(!_0x35decc)continue;_0x35decc[_0x3caa89(0x20e)](_0x5371be[_0x3caa89(0x1ab)]);const _0x1452d7=String(RegExp['$1'])[_0x3caa89(0x285)](','),_0x47386c=this[_0x3caa89(0x1ef)](_0x1452d7,_0x4bc89b);if(_0x47386c['length']<=0x1)continue;const _0x5609f4=_0x47386c[_0x3caa89(0x214)]('-'),_0x532d37=_0x4bc89b?DataManager[_0x3caa89(0x1b4)]:DataManager[_0x3caa89(0x1dc)];_0x532d37[_0x5609f4]=_0x1ab7fc['id'];}},DataManager[_0xfb7938(0x1ef)]=function(_0x16a9ee,_0x17acfe){const _0x1dfc2d=_0xfb7938,_0x1b72f9=[];for(let _0x3c894d of _0x16a9ee){_0x3c894d=(String(_0x3c894d)||'')[_0x1dfc2d(0x17f)]();const _0x4b3c1a=/^\d+$/['test'](_0x3c894d);if(_0x4b3c1a)_0x1b72f9[_0x1dfc2d(0xca)](Number(_0x3c894d));else _0x17acfe?_0x1b72f9['push'](DataManager[_0x1dfc2d(0x2a0)](_0x3c894d)):_0x1b72f9[_0x1dfc2d(0xca)](DataManager['getItemIdWithName'](_0x3c894d));}return _0x1b72f9;},ImageManager[_0xfb7938(0x1c1)]=VisuMZ[_0xfb7938(0x2ce)]['Settings'][_0xfb7938(0x135)]['BravePointsIcon'],TextManager[_0xfb7938(0x207)]=VisuMZ[_0xfb7938(0x2ce)]['Settings'][_0xfb7938(0x135)][_0xfb7938(0x20f)],TextManager[_0xfb7938(0x2a9)]=VisuMZ['BattleSystemBTB'][_0xfb7938(0xe9)][_0xfb7938(0x135)][_0xfb7938(0x25d)],TextManager['btbCostFormat']=VisuMZ['BattleSystemBTB'][_0xfb7938(0xe9)][_0xfb7938(0x135)][_0xfb7938(0x1f3)],TextManager['btbBraveCommand']=VisuMZ['BattleSystemBTB'][_0xfb7938(0xe9)]['Window'][_0xfb7938(0x241)],TextManager['btbActionSlot']=VisuMZ[_0xfb7938(0x2ce)]['Settings'][_0xfb7938(0x163)][_0xfb7938(0x251)],TextManager['btbActionCurrent']=VisuMZ[_0xfb7938(0x2ce)]['Settings']['Window'][_0xfb7938(0x159)],SceneManager[_0xfb7938(0x1d4)]=function(){const _0x5ba5da=_0xfb7938;return this[_0x5ba5da(0x294)]&&this['_scene']['constructor']===Scene_Battle;},VisuMZ[_0xfb7938(0x2ce)]['BattleManager_battleSys']=BattleManager[_0xfb7938(0x19b)],BattleManager[_0xfb7938(0x19b)]=function(){const _0x83257=_0xfb7938;if(this[_0x83257(0x1bf)]())return'BTB';return VisuMZ['BattleSystemBTB']['BattleManager_battleSys'][_0x83257(0x1f4)](this);},BattleManager['isBTB']=function(){const _0x21a043=_0xfb7938;return $gameSystem[_0x21a043(0xcf)]()===_0x21a043(0x2d0);},VisuMZ[_0xfb7938(0x2ce)]['BattleManager_isTpb']=BattleManager['isTpb'],BattleManager[_0xfb7938(0xad)]=function(){const _0x50b63f=_0xfb7938;if(this[_0x50b63f(0x1bf)]())return![];return VisuMZ[_0x50b63f(0x2ce)]['BattleManager_isTpb']['call'](this);},VisuMZ[_0xfb7938(0x2ce)]['BattleManager_isActiveTpb']=BattleManager['isActiveTpb'],BattleManager['isActiveTpb']=function(){const _0x566bde=_0xfb7938;if(this[_0x566bde(0x1bf)]())return![];return VisuMZ[_0x566bde(0x2ce)]['BattleManager_isActiveTpb'][_0x566bde(0x1f4)](this);},VisuMZ['BattleSystemBTB'][_0xfb7938(0x16b)]=BattleManager[_0xfb7938(0x201)],BattleManager[_0xfb7938(0x201)]=function(){const _0x2ebf97=_0xfb7938;if(this['isBTB']())return!![];return VisuMZ[_0x2ebf97(0x2ce)][_0x2ebf97(0x16b)]['call'](this);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x28a)]=BattleManager[_0xfb7938(0x273)],BattleManager[_0xfb7938(0x273)]=function(){const _0x6deec9=_0xfb7938;VisuMZ['BattleSystemBTB'][_0x6deec9(0x28a)][_0x6deec9(0x1f4)](this),this[_0x6deec9(0x1bf)]()&&this['isSkipPartyCommandWindow']()&&!this['_surprise']&&$gameParty[_0x6deec9(0xe5)]()&&this[_0x6deec9(0x176)]();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1e9)]=BattleManager['startTurn'],BattleManager[_0xfb7938(0x2bd)]=function(){const _0x4b5672=_0xfb7938;VisuMZ[_0x4b5672(0x2ce)][_0x4b5672(0x1e9)]['call'](this),this['refreshStatusBTB']();},BattleManager['refreshStatusBTB']=function(){const _0x5d9312=_0xfb7938;if(!SceneManager[_0x5d9312(0x1d4)]())return;if(!this[_0x5d9312(0x1bf)]())return;const _0x32301d=SceneManager['_scene'];if(!_0x32301d)return;const _0x4ae0df=_0x32301d[_0x5d9312(0x142)];if(!_0x4ae0df)return;_0x4ae0df[_0x5d9312(0xd4)]();},VisuMZ[_0xfb7938(0x2ce)]['BattleManager_makeActionOrders']=BattleManager[_0xfb7938(0x213)],BattleManager['makeActionOrders']=function(){const _0x513055=_0xfb7938;VisuMZ[_0x513055(0x2ce)][_0x513055(0x1d2)][_0x513055(0x1f4)](this),this[_0x513055(0x1bf)]()&&(this[_0x513055(0x1f9)]=this['_actionBattlers'][_0x513055(0x27c)](_0x208510=>_0x208510&&_0x208510[_0x513055(0x115)][_0x513055(0x279)]>0x0),this[_0x513055(0x284)]());},BattleManager[_0xfb7938(0xfb)]=function(){const _0x5c813d=_0xfb7938;if(!this[_0x5c813d(0x1bf)]())return;if(!SceneManager[_0x5c813d(0x1d4)]())return;const _0x48b559=this['_actionBattlers'];for(const _0x37cef8 of _0x48b559){_0x37cef8[_0x5c813d(0x232)]();}_0x48b559[_0x5c813d(0x293)]((_0x54c5e6,_0xa5308d)=>_0xa5308d[_0x5c813d(0xeb)]()-_0x54c5e6['speed']()),this[_0x5c813d(0x1bf)]()&&this[_0x5c813d(0x284)]();},BattleManager['removeActionBattlersBTB']=function(){const _0x384ca3=_0xfb7938;if(!this['isBTB']())return;this['_actionBattlers']=this[_0x384ca3(0x1f9)]||[],this[_0x384ca3(0x1f9)]=this['_actionBattlers'][_0x384ca3(0x27c)](_0x145227=>_0x145227&&_0x145227[_0x384ca3(0x133)]()&&_0x145227[_0x384ca3(0x1a3)]()),this[_0x384ca3(0x284)]();},BattleManager[_0xfb7938(0x284)]=function(_0xfba0ce){const _0x25c504=_0xfb7938;if(!this[_0x25c504(0x1bf)]())return;const _0x3bc607=SceneManager['_scene'][_0x25c504(0x2af)];if(!_0x3bc607)return;_0x3bc607[_0x25c504(0x16a)](_0xfba0ce);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x14a)]=BattleManager[_0xfb7938(0x172)],BattleManager[_0xfb7938(0x172)]=function(){const _0x542fcd=_0xfb7938;BattleManager[_0x542fcd(0x1bf)]()&&this[_0x542fcd(0x1a1)]&&this[_0x542fcd(0x1a1)][_0x542fcd(0x16c)](),VisuMZ[_0x542fcd(0x2ce)][_0x542fcd(0x14a)]['call'](this);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x102)]=Game_System[_0xfb7938(0xa8)][_0xfb7938(0x165)],Game_System[_0xfb7938(0xa8)][_0xfb7938(0x165)]=function(){const _0xf7c3bc=_0xfb7938;VisuMZ[_0xf7c3bc(0x2ce)][_0xf7c3bc(0x102)][_0xf7c3bc(0x1f4)](this),this[_0xf7c3bc(0x166)]();},Game_System[_0xfb7938(0xa8)][_0xfb7938(0x166)]=function(){const _0x24a0e7=_0xfb7938;this[_0x24a0e7(0xe8)]=!![];},Game_System[_0xfb7938(0xa8)][_0xfb7938(0xf0)]=function(){const _0x30ee23=_0xfb7938;return this[_0x30ee23(0xe8)]===undefined&&this['initBattleSystemBTB'](),this[_0x30ee23(0xe8)];},Game_System[_0xfb7938(0xa8)][_0xfb7938(0x2b2)]=function(_0x3a9a76){const _0x602b31=_0xfb7938;this['_btbTurnOrderVisible']===undefined&&this['initBattleSystemBTB'](),this[_0x602b31(0xe8)]=_0x3a9a76;},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x26a)]=Game_Action['prototype']['applyItemUserEffect'],Game_Action[_0xfb7938(0xa8)][_0xfb7938(0x9a)]=function(_0x13a6e7){const _0x5138a8=_0xfb7938;VisuMZ[_0x5138a8(0x2ce)][_0x5138a8(0x26a)][_0x5138a8(0x1f4)](this,_0x13a6e7),this[_0x5138a8(0x1d8)](_0x13a6e7);},Game_Action['prototype'][_0xfb7938(0x1d8)]=function(_0x23b112){const _0x1eca46=_0xfb7938;if(!BattleManager['isBTB']())return;if(this[_0x1eca46(0xd6)]())this[_0x1eca46(0xd8)](_0x23b112);},Game_Action[_0xfb7938(0xa8)][_0xfb7938(0xd8)]=function(_0xcbb55f){const _0x33e784=_0xfb7938,_0xbd9dab=VisuMZ[_0x33e784(0x2ce)][_0x33e784(0x169)],_0x443a97=this[_0x33e784(0xd6)]()[_0x33e784(0x210)],_0x3c42f8=this[_0x33e784(0xd6)]();if(this[_0x33e784(0x2a7)]()){if(_0x443a97[_0x33e784(0x20e)](_0xbd9dab[_0x33e784(0x222)])){const _0x42dd2e=Number(RegExp['$1']);this[_0x33e784(0x2a7)]()[_0x33e784(0x2dd)](_0x42dd2e);}if(_0x443a97[_0x33e784(0x20e)](_0xbd9dab[_0x33e784(0x149)])){const _0x568433=Number(RegExp['$1']);this[_0x33e784(0x2a7)]()[_0x33e784(0xe2)](_0x568433);}const _0x196168=_0x33e784(0x156),_0x5082c5=VisuMZ[_0x33e784(0x2ce)][_0x33e784(0x130)](_0x3c42f8,_0x196168);if(VisuMZ[_0x33e784(0x2ce)]['JS'][_0x5082c5]){const _0x5b82fc=VisuMZ[_0x33e784(0x2ce)]['JS'][_0x5082c5]['call'](this,this[_0x33e784(0x2a7)](),_0xcbb55f,this[_0x33e784(0x2a7)]()[_0x33e784(0x2d4)]());this[_0x33e784(0x2a7)]()[_0x33e784(0x2dd)](_0x5b82fc);}}if(_0xcbb55f){if(_0x443a97[_0x33e784(0x20e)](_0xbd9dab['BravePointSetTarget'])){const _0x39e8bb=Number(RegExp['$1']);_0xcbb55f[_0x33e784(0x2dd)](_0x39e8bb);}if(_0x443a97[_0x33e784(0x20e)](_0xbd9dab[_0x33e784(0x29c)])){const _0x456287=Number(RegExp['$1']);_0xcbb55f[_0x33e784(0xe2)](_0x456287);}const _0x51c977=_0x33e784(0x263),_0x3f6665=VisuMZ['BattleSystemBTB'][_0x33e784(0x130)](_0x3c42f8,_0x51c977);if(VisuMZ['BattleSystemBTB']['JS'][_0x3f6665]){const _0x2d7e4e=VisuMZ[_0x33e784(0x2ce)]['JS'][_0x3f6665][_0x33e784(0x1f4)](this,this['subject'](),_0xcbb55f,_0xcbb55f['bravePoints']());_0xcbb55f[_0x33e784(0x2dd)](_0x2d7e4e);}}},VisuMZ['BattleSystemBTB'][_0xfb7938(0x1c9)]=Game_Action[_0xfb7938(0xa8)][_0xfb7938(0xeb)],Game_Action[_0xfb7938(0xa8)]['speed']=function(){const _0x3f0ae8=_0xfb7938;return BattleManager[_0x3f0ae8(0x1bf)]()?VisuMZ[_0x3f0ae8(0x2ce)][_0x3f0ae8(0xe9)][_0x3f0ae8(0x16e)][_0x3f0ae8(0x18f)][_0x3f0ae8(0x1f4)](this):VisuMZ[_0x3f0ae8(0x2ce)][_0x3f0ae8(0x1c9)][_0x3f0ae8(0x1f4)](this);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x286)]=Game_Action[_0xfb7938(0xa8)][_0xfb7938(0xd0)],Game_Action[_0xfb7938(0xa8)][_0xfb7938(0xd0)]=function(){const _0x232cc1=_0xfb7938;return BattleManager[_0x232cc1(0x1bf)]()?VisuMZ['BattleSystemBTB'][_0x232cc1(0xe9)][_0x232cc1(0x16e)]['AllowRandomSpeed']:VisuMZ['BattleSystemBTB'][_0x232cc1(0x286)][_0x232cc1(0x1f4)](this);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x12a)]=Game_Action[_0xfb7938(0xa8)]['setSkill'],Game_Action[_0xfb7938(0xa8)]['setSkill']=function(_0x591070){const _0x4816ea=_0xfb7938;VisuMZ[_0x4816ea(0x2ce)][_0x4816ea(0x12a)][_0x4816ea(0x1f4)](this,_0x591070),BattleManager[_0x4816ea(0xfb)]();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1ad)]=Game_Action[_0xfb7938(0xa8)][_0xfb7938(0x2df)],Game_Action['prototype'][_0xfb7938(0x2df)]=function(_0x19c608){const _0xa6ba4e=_0xfb7938;VisuMZ[_0xa6ba4e(0x2ce)][_0xa6ba4e(0x1ad)][_0xa6ba4e(0x1f4)](this,_0x19c608),BattleManager[_0xa6ba4e(0xfb)]();},Game_Action[_0xfb7938(0xa8)][_0xfb7938(0x1cc)]=function(_0x19c168){this['_actionFusionRecipe']=_0x19c168;},Game_Action[_0xfb7938(0xa8)][_0xfb7938(0x2bf)]=function(){const _0x4767c9=_0xfb7938;if(this[_0x4767c9(0x2c0)]===undefined)return 0x0;return this[_0x4767c9(0x2c0)]['split']('-')['length']-0x1;},Game_Action['prototype']['getActionFusionRecipeSkills']=function(){const _0x942e89=_0xfb7938;if(this['_actionFusionRecipe']===undefined)return[];return this[_0x942e89(0x2c0)][_0x942e89(0x285)]('-')['map'](_0x2eb029=>$dataSkills[Number(_0x2eb029)]);},Game_Action[_0xfb7938(0xa8)]['getActionFusionRecipeItems']=function(){const _0x28be7d=_0xfb7938;if(this[_0x28be7d(0x2c0)]===undefined)return[];return this['_actionFusionRecipe'][_0x28be7d(0x285)]('-')[_0x28be7d(0x14f)](_0x337cd0=>$dataItems[Number(_0x337cd0)]);},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x2d4)]=function(){const _0x5f50c7=_0xfb7938;return this[_0x5f50c7(0x17c)]||0x0;},Game_BattlerBase[_0xfb7938(0x1b3)]=VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0xe9)]['Mechanics'][_0xfb7938(0x1d5)],Game_BattlerBase[_0xfb7938(0x2ba)]=VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0xe9)][_0xfb7938(0x16e)][_0xfb7938(0x23f)],Game_BattlerBase[_0xfb7938(0xa8)]['maxBraveActions']=function(){const _0x17e3cd=_0xfb7938;if(this[_0x17e3cd(0x1ec)]())return 0x1;if(this['hideBraveTrait']())return 0x1;const _0xb90a5c=VisuMZ[_0x17e3cd(0x2ce)]['RegExp'],_0x51d739=_0xb90a5c['MaxActions'];let _0x2ce8ca=Game_BattlerBase[_0x17e3cd(0x1b3)];const _0x35e02f=this[_0x17e3cd(0x2c2)]();for(const _0x2c4afa of _0x35e02f){if(!_0x2c4afa)continue;const _0xde6eac=_0x2c4afa[_0x17e3cd(0x210)];_0xde6eac[_0x17e3cd(0x20e)](_0x51d739)&&(_0x2ce8ca+=Number(RegExp['$1']));}return _0x2ce8ca[_0x17e3cd(0x277)](0x1,Game_BattlerBase[_0x17e3cd(0x2ba)]);},Game_BattlerBase[_0xfb7938(0x27b)]=VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0xe9)]['Mechanics'][_0xfb7938(0x195)],Game_BattlerBase[_0xfb7938(0x9b)]=VisuMZ[_0xfb7938(0x2ce)]['Settings'][_0xfb7938(0x16e)]['MinBravePointsDefault'],Game_BattlerBase['BTB_MAX_BRAVEPOINTS_HARD_CAP']=VisuMZ['BattleSystemBTB']['Settings'][_0xfb7938(0x16e)][_0xfb7938(0x256)],Game_BattlerBase['BTB_MIN_BRAVEPOINTS_HARD_CAP']=VisuMZ['BattleSystemBTB'][_0xfb7938(0xe9)]['Mechanics'][_0xfb7938(0xb4)],Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x2c3)]=function(){const _0x216022=_0xfb7938,_0x4841d3=VisuMZ[_0x216022(0x2ce)][_0x216022(0x169)],_0x3490d4=_0x4841d3[_0x216022(0x113)];let _0x20929e=Game_BattlerBase[_0x216022(0x27b)];const _0x13fd32=this[_0x216022(0x2c2)]();for(const _0x38df8e of _0x13fd32){if(!_0x38df8e)continue;const _0x4163ac=_0x38df8e[_0x216022(0x210)];_0x4163ac[_0x216022(0x20e)](_0x3490d4)&&(_0x20929e+=Number(RegExp['$1']));}return Math[_0x216022(0x242)](_0x20929e,Game_BattlerBase[_0x216022(0x10a)]);},Game_BattlerBase['prototype'][_0xfb7938(0x1ff)]=function(){const _0x1ac474=_0xfb7938,_0x343701=VisuMZ[_0x1ac474(0x2ce)]['RegExp'],_0x55980a=_0x343701['MinBravePoints'];let _0xbe8d77=Game_BattlerBase[_0x1ac474(0x9b)];const _0x19f7ca=this['traitObjects']();for(const _0x574045 of _0x19f7ca){if(!_0x574045)continue;const _0x3b4097=_0x574045[_0x1ac474(0x210)];_0x3b4097[_0x1ac474(0x20e)](_0x55980a)&&(_0xbe8d77+=Number(RegExp['$1']));}return Math[_0x1ac474(0x2c5)](_0xbe8d77,Game_BattlerBase[_0x1ac474(0x187)]);},Game_BattlerBase[_0xfb7938(0xa8)]['setBravePoints']=function(_0x32ce56){const _0x4246c7=_0xfb7938;this[_0x4246c7(0x17c)]=Math[_0x4246c7(0x242)](_0x32ce56,this['maxBravePoints']()),this[_0x4246c7(0x174)]();},Game_BattlerBase[_0xfb7938(0xa8)]['gainBravePoints']=function(_0x67a33d){_0x67a33d+=this['_bravePoints']||0x0,this['setBravePoints'](_0x67a33d);},Game_BattlerBase['prototype'][_0xfb7938(0xcd)]=function(_0x3e8fa3){this['gainBravePoints'](-_0x3e8fa3);},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0xbe)]=function(_0x14067d){const _0x40ded5=_0xfb7938,_0x46cee1=VisuMZ[_0x40ded5(0x2ce)][_0x40ded5(0xe9)][_0x40ded5(0x16e)];if(!_0x14067d)return _0x46cee1[_0x40ded5(0x100)];if(DataManager['isSkill'](_0x14067d)){if(_0x14067d['id']===this[_0x40ded5(0x281)]())return 0x0;if(this[_0x40ded5(0x1d0)]()&&this[_0x40ded5(0x1d0)]()[_0x40ded5(0xd6)]()===_0x14067d&&this['currentAction']()['_guardUnleash'])return 0x0;}const _0x9e560a=VisuMZ[_0x40ded5(0x2ce)][_0x40ded5(0x169)],_0x41bc4a=_0x14067d[_0x40ded5(0x210)];if(_0x41bc4a['match'](_0x9e560a[_0x40ded5(0xd3)]))return Number(RegExp['$1']);let _0x474f4e=0x0;if(DataManager[_0x40ded5(0x238)](_0x14067d))_0x474f4e=_0x46cee1['BravePointSkillCost'];else DataManager['isItem'](_0x14067d)&&(_0x474f4e=_0x46cee1[_0x40ded5(0x180)]);return _0x474f4e[_0x40ded5(0x277)](0x0,Game_BattlerBase['BTB_MAX_BRAVEPOINTS_HARD_CAP']);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0xd5)]=Game_BattlerBase['prototype']['canUse'],Game_BattlerBase['prototype']['canUse']=function(_0x4a2c83){const _0x1bb3ac=_0xfb7938;if(_0x4a2c83&&BattleManager[_0x1bb3ac(0x1bf)]()){const _0x249387=this[_0x1bb3ac(0xbe)](_0x4a2c83);if(this['bravePoints']()-_0x249387<this[_0x1bb3ac(0x1ff)]())return![];}return VisuMZ['BattleSystemBTB'][_0x1bb3ac(0xd5)]['call'](this,_0x4a2c83);},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0xc6)]=function(_0xa6f441){const _0x20ed0c=_0xfb7938;if(!BattleManager[_0x20ed0c(0x1bf)]())return;const _0xfdb63=this['bravePointsCost'](_0xa6f441);this[_0x20ed0c(0xcd)](_0xfdb63);},VisuMZ['BattleSystemBTB'][_0xfb7938(0x9c)]=Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x136)],Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x136)]=function(_0x342c45){const _0x36b1fe=_0xfb7938;if(this[_0x36b1fe(0x11d)](_0x342c45)){this[_0x36b1fe(0x12d)](_0x342c45);return;}VisuMZ[_0x36b1fe(0x2ce)]['Game_Battler_useItem'][_0x36b1fe(0x1f4)](this,_0x342c45),this[_0x36b1fe(0xc6)](_0x342c45);},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x11d)]=function(_0x3cc240){const _0x461805=_0xfb7938;if(!BattleManager[_0x461805(0x1bf)]())return![];if(!SceneManager[_0x461805(0x1d4)]())return![];if(!this[_0x461805(0xdb)]())return![];if(this!==BattleManager[_0x461805(0x1a1)])return![];if(!this[_0x461805(0x1d0)]())return![];if(!this[_0x461805(0x1d0)]()[_0x461805(0xd6)]())return![];if(this[_0x461805(0x1d0)]()['item']()!==_0x3cc240)return![];if(this[_0x461805(0x1d0)]()[_0x461805(0x238)]())return this[_0x461805(0x1d0)]()[_0x461805(0x258)]()[_0x461805(0x279)]>0x0;else return this[_0x461805(0x1d0)]()[_0x461805(0x26b)]()?this[_0x461805(0x1d0)]()[_0x461805(0x287)]()[_0x461805(0x279)]>0x0:![];},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x12d)]=function(_0x2197cc){const _0x4dcaca=_0xfb7938;DataManager['isSkill'](_0x2197cc)?this['btbPaySkillFusionCosts']():this[_0x4dcaca(0x21a)]();},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x204)]=function(){const _0x2f9648=_0xfb7938,_0x5d9617=this[_0x2f9648(0x1d0)]()['getActionFusionRecipeSkills']();if(!_0x5d9617)return;for(const _0x4c7f3f of _0x5d9617){if(!_0x4c7f3f)continue;if(!this['canUse'](_0x4c7f3f))return![];VisuMZ['BattleSystemBTB']['Game_Battler_useItem'][_0x2f9648(0x1f4)](this,_0x4c7f3f),this[_0x2f9648(0xc6)](_0x4c7f3f);}return!![];},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x21a)]=function(){const _0x4afa64=_0xfb7938,_0x10e67c=this['currentAction']()['getActionFusionRecipeItems']();if(!_0x10e67c)return;for(const _0x1d009b of _0x10e67c){if(!_0x1d009b)continue;if(!this[_0x4afa64(0xcb)](_0x1d009b))return![];VisuMZ[_0x4afa64(0x2ce)][_0x4afa64(0x9c)]['call'](this,_0x1d009b),this[_0x4afa64(0xc6)](_0x1d009b);}return!![];},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0xf1)]=function(){const _0x5f568d=_0xfb7938,_0x141080=this['bravePoints']()-this[_0x5f568d(0xc7)]()+this[_0x5f568d(0x22c)]();return _0x141080[_0x5f568d(0x277)](Game_BattlerBase[_0x5f568d(0x187)],this[_0x5f568d(0x2c3)]());},Game_BattlerBase[_0xfb7938(0xa8)]['predictedBravePointCost']=function(){const _0x549e1e=_0xfb7938;let _0x80f4f4=0x0;for(const _0x1538e3 of this[_0x549e1e(0x115)]){if(!_0x1538e3)continue;const _0xf12174=_0x1538e3[_0x549e1e(0xd6)]();_0x80f4f4+=this[_0x549e1e(0xbe)](_0xf12174);}return _0x80f4f4;},VisuMZ[_0xfb7938(0x2ce)]['Game_BattlerBase_canInput']=Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0xe5)],Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0xe5)]=function(){const _0x523fd7=_0xfb7938;return BattleManager['isBTB']()&&this['bravePoints']()<0x0?![]:VisuMZ[_0x523fd7(0x2ce)][_0x523fd7(0x189)]['call'](this);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x2d7)]=Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x1a5)],Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x1a5)]=function(){const _0x183b76=_0xfb7938;return BattleManager['isBTB']()&&this[_0x183b76(0x123)]()>0x1?![]:VisuMZ['BattleSystemBTB'][_0x183b76(0x2d7)]['call'](this);},Game_BattlerBase['prototype'][_0xfb7938(0x283)]=function(){const _0x3106fe=_0xfb7938;if(this[_0x3106fe(0x1ec)]())return![];return this[_0x3106fe(0x123)]()<this[_0x3106fe(0x13e)]()&&this['_bravePoints']>this[_0x3106fe(0x1ff)]();},Game_BattlerBase['prototype']['cannotBraveTrait']=function(){const _0x375d5f=_0xfb7938,_0x1abc07=VisuMZ['BattleSystemBTB'][_0x375d5f(0x169)],_0x244faa=_0x1abc07['CannotBrave'];return this[_0x375d5f(0x2c2)]()[_0x375d5f(0x155)](_0x4ea5c9=>_0x4ea5c9&&_0x4ea5c9[_0x375d5f(0x210)][_0x375d5f(0x20e)](_0x244faa));},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x1f6)]=function(){const _0x1c2f81=_0xfb7938,_0x21c3ff=VisuMZ[_0x1c2f81(0x2ce)][_0x1c2f81(0x169)],_0x485247=_0x21c3ff[_0x1c2f81(0x120)];return this[_0x1c2f81(0x2c2)]()[_0x1c2f81(0x155)](_0x583215=>_0x583215&&_0x583215['note'][_0x1c2f81(0x20e)](_0x485247));},Game_BattlerBase[_0xfb7938(0xa8)]['clearTurnOrderBTBGraphics']=function(){const _0x2aaef2=_0xfb7938;delete this[_0x2aaef2(0x252)],delete this['_btbTurnOrderFaceName'],delete this[_0x2aaef2(0x248)],delete this[_0x2aaef2(0x175)];},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x25f)]=function(){const _0x37b717=_0xfb7938;return this[_0x37b717(0x252)]===undefined&&(this['_btbTurnOrderGraphicType']=this[_0x37b717(0x10c)]()),this[_0x37b717(0x252)];},Game_BattlerBase['prototype'][_0xfb7938(0x10c)]=function(){return Window_BTB_TurnOrder['Settings']['EnemyBattlerType'];},Game_BattlerBase[_0xfb7938(0xa8)]['TurnOrderBTBGraphicFaceName']=function(){const _0x33358f=_0xfb7938;return this[_0x33358f(0x23b)]===undefined&&(this[_0x33358f(0x23b)]=this[_0x33358f(0x144)]()),this[_0x33358f(0x23b)];},Game_BattlerBase['prototype'][_0xfb7938(0x144)]=function(){const _0x16000c=_0xfb7938;return Window_BTB_TurnOrder[_0x16000c(0xe9)][_0x16000c(0x1c2)];},Game_BattlerBase['prototype'][_0xfb7938(0x29b)]=function(){const _0x252141=_0xfb7938;return this[_0x252141(0x248)]===undefined&&(this['_btbTurnOrderFaceIndex']=this[_0x252141(0x14c)]()),this[_0x252141(0x248)];},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x14c)]=function(){const _0xaf6af3=_0xfb7938;return Window_BTB_TurnOrder[_0xaf6af3(0xe9)][_0xaf6af3(0x2cb)];},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0xb5)]=function(){const _0x148043=_0xfb7938;return this[_0x148043(0x175)]===undefined&&(this[_0x148043(0x175)]=this[_0x148043(0x132)]()),this[_0x148043(0x175)];},Game_BattlerBase[_0xfb7938(0xa8)][_0xfb7938(0x132)]=function(){const _0x1b60ba=_0xfb7938;return Window_BTB_TurnOrder['Settings'][_0x1b60ba(0x1e2)];},Game_BattlerBase['prototype']['setBTBGraphicIconIndex']=function(_0x4c9960){this['_btbTurnOrderIconIndex']=_0x4c9960;},VisuMZ['BattleSystemBTB'][_0xfb7938(0x19a)]=Game_BattlerBase['prototype'][_0xfb7938(0xf6)],Game_BattlerBase['prototype'][_0xfb7938(0xf6)]=function(){const _0x31e867=_0xfb7938;VisuMZ[_0x31e867(0x2ce)]['Game_BattlerBase_hide'][_0x31e867(0x1f4)](this),BattleManager['removeActionBattlersBTB']();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1a9)]=Game_BattlerBase['prototype']['appear'],Game_BattlerBase[_0xfb7938(0xa8)]['appear']=function(){const _0x4f1f8e=_0xfb7938;VisuMZ[_0x4f1f8e(0x2ce)][_0x4f1f8e(0x1a9)][_0x4f1f8e(0x1f4)](this),BattleManager[_0x4f1f8e(0x228)]();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1f1)]=Game_Battler[_0xfb7938(0xa8)]['performCollapse'],Game_Battler['prototype']['performCollapse']=function(){const _0x3110a0=_0xfb7938;VisuMZ[_0x3110a0(0x2ce)][_0x3110a0(0x1f1)][_0x3110a0(0x1f4)](this),BattleManager[_0x3110a0(0x228)]();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x18b)]=Game_Battler[_0xfb7938(0xa8)]['makeActionTimes'],Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x1e6)]=function(){const _0x2a8bd8=_0xfb7938;return BattleManager[_0x2a8bd8(0x1bf)]()?0x1:VisuMZ[_0x2a8bd8(0x2ce)][_0x2a8bd8(0x18b)][_0x2a8bd8(0x1f4)](this);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x19e)]=Game_Battler[_0xfb7938(0xa8)]['onBattleStart'],Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x1d6)]=function(_0x2b1329){const _0x3d1571=_0xfb7938;VisuMZ['BattleSystemBTB'][_0x3d1571(0x19e)]['call'](this,_0x2b1329),this[_0x3d1571(0x118)](_0x2b1329);},Game_Battler[_0xfb7938(0xa8)]['onBattleStartBTB']=function(_0x31bee2){const _0x5d3b73=_0xfb7938;if(!BattleManager[_0x5d3b73(0x1bf)]())return;const _0x511ec0=VisuMZ['BattleSystemBTB'][_0x5d3b73(0xe9)]['Mechanics'],_0x58525b=VisuMZ[_0x5d3b73(0x2ce)][_0x5d3b73(0x169)];let _0x2218f1=_0x31bee2?_0x511ec0[_0x5d3b73(0x1f7)]:_0x511ec0['BravePointStartNeutral'];const _0x401c1d=this[_0x5d3b73(0x2c2)]();for(const _0x2bbc8f of _0x401c1d){if(!_0x2bbc8f)continue;const _0x1a8982=_0x2bbc8f[_0x5d3b73(0x210)];_0x1a8982[_0x5d3b73(0x20e)](_0x58525b[_0x5d3b73(0x27d)])&&(_0x2218f1+=Number(RegExp['$1']));}this[_0x5d3b73(0x2dd)](_0x2218f1);},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x1c6)]=function(){const _0x212d33=_0xfb7938;this[_0x212d33(0x115)][_0x212d33(0xca)](new Game_Action(this));const _0x15a0dc=VisuMZ[_0x212d33(0x2ce)][_0x212d33(0xe9)][_0x212d33(0x230)];if(_0x15a0dc[_0x212d33(0x161)]){const _0x4abfe9=_0x212d33(0x246),_0x386394=_0x15a0dc[_0x212d33(0x117)['format'](_0x4abfe9)],_0x40da5b=_0x15a0dc[_0x212d33(0x1d1)[_0x212d33(0x20c)](_0x4abfe9)],_0x998e36=_0x15a0dc[_0x212d33(0x2a5)[_0x212d33(0x20c)](_0x4abfe9)];$gameTemp[_0x212d33(0x157)]([this],_0x386394,_0x40da5b,_0x998e36);}},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x199)]=function(){const _0x48e4f8=_0xfb7938;if(this[_0x48e4f8(0x115)]['length']<=0x1)return;this[_0x48e4f8(0x115)]['pop']();const _0x298cc1=VisuMZ[_0x48e4f8(0x2ce)][_0x48e4f8(0xe9)][_0x48e4f8(0x230)];if(_0x298cc1[_0x48e4f8(0x290)]){const _0x378bf2=_0x48e4f8(0x1c8),_0x58208c=_0x298cc1[_0x48e4f8(0x117)['format'](_0x378bf2)],_0x5c6be6=_0x298cc1[_0x48e4f8(0x1d1)['format'](_0x378bf2)],_0x2d7ea1=_0x298cc1['%1Mute'[_0x48e4f8(0x20c)](_0x378bf2)];$gameTemp[_0x48e4f8(0x157)]([this],_0x58208c,_0x5c6be6,_0x2d7ea1);}},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1bb)]=Game_Battler[_0xfb7938(0xa8)]['onTurnEnd'],Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x272)]=function(){const _0x18032e=_0xfb7938;VisuMZ['BattleSystemBTB'][_0x18032e(0x1bb)][_0x18032e(0x1f4)](this),this[_0x18032e(0x99)]();},Game_Battler['prototype'][_0xfb7938(0x99)]=function(){const _0x44b048=_0xfb7938;if(!BattleManager[_0x44b048(0x1bf)]())return;this[_0x44b048(0x181)]();},Game_Battler['prototype'][_0xfb7938(0x181)]=function(){const _0x3e7fc1=_0xfb7938,_0x11b217=VisuMZ[_0x3e7fc1(0x2ce)][_0x3e7fc1(0xe9)][_0x3e7fc1(0x16e)],_0x4a34a0=_0x11b217[_0x3e7fc1(0xf7)];if(_0x4a34a0&&!this['isAlive']())return;const _0xa3b441=this[_0x3e7fc1(0x22c)]();this[_0x3e7fc1(0xe2)](_0xa3b441);},Game_Battler[_0xfb7938(0xa8)][_0xfb7938(0x22c)]=function(){const _0x4e657e=_0xfb7938,_0x349aa0=VisuMZ[_0x4e657e(0x2ce)]['RegExp'],_0x5458f0=VisuMZ[_0x4e657e(0x2ce)]['Settings']['Mechanics'];let _0x55121f=_0x5458f0[_0x4e657e(0x220)]||0x0;const _0x6e3561=this[_0x4e657e(0x2c2)]();for(const _0x3f7f11 of _0x6e3561){if(!_0x3f7f11)continue;const _0x1c4644=_0x3f7f11['note'];_0x1c4644[_0x4e657e(0x20e)](_0x349aa0[_0x4e657e(0x198)])&&(_0x55121f+=Number(RegExp['$1']));}return _0x55121f;},Game_Battler['prototype'][_0xfb7938(0x16c)]=function(){const _0x1cf134=_0xfb7938;if(!this[_0x1cf134(0xf9)]())return;if(this[_0x1cf134(0x123)]()<=0x1)return;if(!this[_0x1cf134(0x1d0)]())return;if(!this['currentAction']()[_0x1cf134(0xd6)]())return;const _0x26a4c8=this['getActionFusionCombinationsBTB']();if(_0x26a4c8[_0x1cf134(0x279)]<=0x0)return;let _0x23c8b4='',_0x46c4a5=0x0;const _0xd831f6=this['currentAction']()[_0x1cf134(0x238)](),_0x10d96f=_0xd831f6?DataManager['_btbSkillFlexFusion']:DataManager[_0x1cf134(0x1dc)],_0x2e0744=_0xd831f6?DataManager[_0x1cf134(0x29d)]:DataManager[_0x1cf134(0x173)];for(const _0x461ade of _0x26a4c8){if(!_0x461ade)continue;_0x10d96f[_0x461ade]&&_0x10d96f[_0x461ade]>=_0x46c4a5&&(this[_0x1cf134(0x151)](_0x461ade)&&(_0x23c8b4=_0x461ade,_0x46c4a5=_0x10d96f[_0x461ade])),_0x2e0744[_0x461ade]&&_0x2e0744[_0x461ade]>=_0x46c4a5&&(this['canPayActionFusionCombination'](_0x461ade)&&(_0x23c8b4=_0x461ade,_0x46c4a5=_0x10d96f[_0x461ade]));}if(_0x46c4a5<=0x0)return;this[_0x1cf134(0x17e)](_0x23c8b4),this[_0x1cf134(0x1d0)]()[_0x1cf134(0x1cc)](_0x23c8b4),_0xd831f6?this[_0x1cf134(0x1d0)]()[_0x1cf134(0xbb)](_0x46c4a5):this[_0x1cf134(0x1d0)]()[_0x1cf134(0x2df)](_0x46c4a5);},Game_Battler[_0xfb7938(0xa8)]['canProcessActionFusionsBTB']=function(){const _0x4f75fa=_0xfb7938;if(this['cannotFusionNotetagBTB']())return![];const _0x5a6613=VisuMZ[_0x4f75fa(0x2ce)]['Settings'][_0x4f75fa(0x16e)];if(this[_0x4f75fa(0xdb)]()){if(_0x5a6613[_0x4f75fa(0x15f)]===undefined)return!![];return _0x5a6613['ActorActionFusions'];}else{if(_0x5a6613['EnemyActionFusions']===undefined)return!![];return _0x5a6613[_0x4f75fa(0x2cf)];}},Game_BattlerBase['prototype'][_0xfb7938(0x196)]=function(){const _0x10dc39=_0xfb7938,_0x36f459=VisuMZ[_0x10dc39(0x2ce)][_0x10dc39(0x169)],_0x2c68e1=this['traitObjects']();for(const _0x230111 of _0x2c68e1){if(!_0x230111)continue;const _0x2b22ec=_0x230111['note'];if(_0x2b22ec[_0x10dc39(0x20e)](_0x36f459[_0x10dc39(0x21f)]))return!![];if(_0x2b22ec[_0x10dc39(0x20e)](_0x36f459[_0x10dc39(0xe7)]))return![];}return![];},Game_Battler['prototype'][_0xfb7938(0xac)]=function(){const _0x8750f9=_0xfb7938,_0x5ad81a=this[_0x8750f9(0x1d0)](),_0xe898ee=this['_actions'],_0x1a75fc=_0xe898ee[_0x8750f9(0x27c)](_0x56daa1=>this[_0x8750f9(0x9f)](_0x5ad81a,_0x56daa1)),_0x468b05=_0x1a75fc['map'](_0x3e561a=>_0x3e561a[_0x8750f9(0xd6)]()['id']),_0x416b99=VisuMZ['BattleSystemBTB'][_0x8750f9(0x1de)](_0x5ad81a[_0x8750f9(0xd6)]()['id'],_0x468b05);let _0x502e00=String(_0x5ad81a[_0x8750f9(0xd6)]()['id']);for(let _0x45046c=0x1;_0x45046c<_0xe898ee['length'];_0x45046c++){const _0x2f15ae=_0xe898ee[_0x45046c];if(this[_0x8750f9(0x9f)](_0x5ad81a,_0x2f15ae))_0x502e00=_0x8750f9(0x106)[_0x8750f9(0x20c)](_0x502e00,_0x2f15ae[_0x8750f9(0xd6)]()['id']),_0x416b99[_0x8750f9(0xca)](_0x502e00);else break;}return _0x416b99[_0x8750f9(0x27c)]((_0x3323de,_0x18e8ee,_0x1b4e6)=>_0x1b4e6[_0x8750f9(0x12c)](_0x3323de)===_0x18e8ee);},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1de)]=function(_0x3eb18d,_0x4c1d22){const _0x5efb68=[],_0x5259dc=function(_0x52e07e,_0xb5d643){const _0x45bdab=_0x58f0;for(var _0xbdc72c=0x0;_0xbdc72c<_0xb5d643[_0x45bdab(0x279)];_0xbdc72c++){_0x5efb68[_0x45bdab(0xca)](_0x52e07e+'-'+_0xb5d643[_0xbdc72c]),_0x5259dc(_0x52e07e+'-'+_0xb5d643[_0xbdc72c],_0xb5d643[_0x45bdab(0x109)](_0xbdc72c+0x1));}};return _0x5259dc(_0x3eb18d,_0x4c1d22),_0x5efb68;},Game_Battler['prototype'][_0xfb7938(0x9f)]=function(_0x1d6c34,_0x25fd3d){const _0x4ec1ba=_0xfb7938;if(!_0x1d6c34||!_0x25fd3d)return![];if(_0x1d6c34===_0x25fd3d)return![];if(!_0x1d6c34[_0x4ec1ba(0xd6)]()||!_0x25fd3d[_0x4ec1ba(0xd6)]())return![];if(_0x1d6c34[_0x4ec1ba(0x238)]()!==_0x25fd3d[_0x4ec1ba(0x238)]())return![];return!![];},Game_Battler[_0xfb7938(0xa8)]['canPayActionFusionCombination']=function(_0xb147c0){const _0x27c488=_0xfb7938,_0x87d287=this[_0x27c488(0x1d0)]()['isSkill'](),_0x55864a=JsonEx[_0x27c488(0x267)](this);_0x55864a[_0x27c488(0x24c)]=!![],_0x55864a[_0x27c488(0x1d0)]()[_0x27c488(0x1cc)](_0xb147c0);if(_0x87d287)return _0x55864a[_0x27c488(0x204)]();else{const _0x4e1d64=JsonEx[_0x27c488(0x267)]($gameParty[_0x27c488(0x137)]),_0x1a8b75=JsonEx[_0x27c488(0x267)]($gameParty['_weapons']),_0x279f2d=JsonEx['makeDeepCopy']($gameParty[_0x27c488(0xb7)]);let _0x421eb6=_0x55864a[_0x27c488(0x21a)]();return $gameParty[_0x27c488(0x137)]=_0x4e1d64,$gameParty['_weapons']=_0x1a8b75,$gameParty['_armors']=_0x279f2d,_0x421eb6;}},Game_Battler[_0xfb7938(0xa8)]['removeActionFusionIngredients']=function(_0x2af7c1){const _0x30c36b=_0xfb7938,_0x261c0c=this[_0x30c36b(0x1d0)](),_0x2bfd60=_0x2af7c1[_0x30c36b(0x285)]('-')[_0x30c36b(0x14f)](_0x2063e0=>Number(_0x2063e0));_0x2bfd60[_0x30c36b(0x153)]();const _0x380620=this['_actions'],_0x22f407=[];for(const _0xaf5fc8 of _0x380620){this[_0x30c36b(0x9f)](_0x261c0c,_0xaf5fc8)&&(_0x2bfd60[_0x30c36b(0x2ca)](_0xaf5fc8['item']()['id'])&&(_0x22f407[_0x30c36b(0xca)](_0xaf5fc8),_0x2bfd60[_0x30c36b(0x1b1)](_0x2bfd60[_0x30c36b(0x12c)](_0xaf5fc8['item']()['id']),0x1)));}for(const _0x4c2981 of _0x22f407){_0x380620[_0x30c36b(0x225)](_0x4c2981);}},Game_Actor[_0xfb7938(0xa8)]['setBravePoints']=function(_0x578674){const _0x1a27c8=_0xfb7938;Game_Battler[_0x1a27c8(0xa8)][_0x1a27c8(0x2dd)][_0x1a27c8(0x1f4)](this,_0x578674);if(!SceneManager['isSceneBattle']())return;if(!BattleManager[_0x1a27c8(0x107)]()['includes'](this))return;BattleManager['refreshStatusBTB']();},VisuMZ[_0xfb7938(0x2ce)]['Game_Actor_makeActions']=Game_Actor['prototype'][_0xfb7938(0x2c1)],Game_Actor['prototype'][_0xfb7938(0x2c1)]=function(){const _0x3016a1=_0xfb7938;VisuMZ[_0x3016a1(0x2ce)][_0x3016a1(0xcc)][_0x3016a1(0x1f4)](this),BattleManager[_0x3016a1(0x1bf)]()&&this['bravePoints']()<0x0&&this['clearActions']();},Game_Actor[_0xfb7938(0xa8)][_0xfb7938(0x10c)]=function(){const _0x183b93=_0xfb7938,_0xfac899=this[_0x183b93(0x24a)]()['note'];if(_0xfac899['match'](/<BTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return'face';else{if(_0xfac899['match'](/<BTB TURN ORDER ICON:[ ](\d+)>/i))return _0x183b93(0x26f);}return Window_BTB_TurnOrder[_0x183b93(0xe9)][_0x183b93(0x22a)];},Game_Actor[_0xfb7938(0xa8)][_0xfb7938(0xed)]=function(){const _0x10ac19=_0xfb7938,_0x3db8ca=this[_0x10ac19(0x24a)]()[_0x10ac19(0x210)];if(_0x3db8ca[_0x10ac19(0x20e)](/<BTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return String(RegExp['$1']);return this['faceName']();},Game_Actor[_0xfb7938(0xa8)][_0xfb7938(0x29b)]=function(){const _0x34c320=_0xfb7938,_0x4676c5=this[_0x34c320(0x24a)]()[_0x34c320(0x210)];if(_0x4676c5[_0x34c320(0x20e)](/<BTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return Number(RegExp['$2']);return this[_0x34c320(0x1e4)]();},Game_Actor[_0xfb7938(0xa8)][_0xfb7938(0x132)]=function(){const _0x30ae27=_0xfb7938,_0x5f0758=this[_0x30ae27(0x24a)]()[_0x30ae27(0x210)];if(_0x5f0758['match'](/<BTB TURN ORDER ICON:[ ](\d+)>/i))return Number(RegExp['$1']);return Window_BTB_TurnOrder[_0x30ae27(0xe9)][_0x30ae27(0x12e)];},Game_Actor[_0xfb7938(0xa8)]['canActionFusionWithBTB']=function(_0x3c5967,_0x41e9c9){const _0x2e67b2=_0xfb7938;if(!Game_Battler[_0x2e67b2(0xa8)][_0x2e67b2(0x9f)][_0x2e67b2(0x1f4)](this,_0x3c5967,_0x41e9c9))return![];if(_0x3c5967[_0x2e67b2(0xb1)]()&&_0x41e9c9[_0x2e67b2(0xb1)]()){if(_0x3c5967[_0x2e67b2(0x257)]()!==_0x41e9c9[_0x2e67b2(0x257)]())return![];if(_0x3c5967[_0x2e67b2(0x2b1)]!==_0x41e9c9[_0x2e67b2(0x2b1)])return![];}return!![];},Game_Enemy[_0xfb7938(0xa8)][_0xfb7938(0x10c)]=function(){const _0x15b426=_0xfb7938,_0x2ae015=this[_0x15b426(0x168)]()[_0x15b426(0x210)];if(_0x2ae015[_0x15b426(0x20e)](/<BTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return _0x15b426(0x1cb);else{if(_0x2ae015[_0x15b426(0x20e)](/<BTB TURN ORDER ICON:[ ](\d+)>/i))return'icon';}return Window_BTB_TurnOrder[_0x15b426(0xe9)][_0x15b426(0x1b0)];},Game_Enemy['prototype'][_0xfb7938(0x144)]=function(){const _0x5ca9d4=_0xfb7938,_0x293c7c=this[_0x5ca9d4(0x168)]()[_0x5ca9d4(0x210)];if(_0x293c7c['match'](/<BTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return String(RegExp['$1']);return Window_BTB_TurnOrder[_0x5ca9d4(0xe9)][_0x5ca9d4(0x1c2)];},Game_Enemy[_0xfb7938(0xa8)][_0xfb7938(0x14c)]=function(){const _0x17906a=_0xfb7938,_0x374079=this[_0x17906a(0x168)]()[_0x17906a(0x210)];if(_0x374079[_0x17906a(0x20e)](/<BTB TURN ORDER FACE:[ ](.*),[ ](\d+)>/i))return Number(RegExp['$2']);return Window_BTB_TurnOrder[_0x17906a(0xe9)][_0x17906a(0x2cb)];},Game_Enemy[_0xfb7938(0xa8)][_0xfb7938(0x132)]=function(){const _0x2d314f=_0xfb7938,_0x5d3149=this[_0x2d314f(0x168)]()[_0x2d314f(0x210)];if(_0x5d3149[_0x2d314f(0x20e)](/<BTB TURN ORDER ICON:[ ](\d+)>/i))return Number(RegExp['$1']);return Window_BTB_TurnOrder[_0x2d314f(0xe9)][_0x2d314f(0x1e2)];},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0xc5)]=Game_Enemy[_0xfb7938(0xa8)]['makeActions'],Game_Enemy[_0xfb7938(0xa8)][_0xfb7938(0x2c1)]=function(){const _0x791b6f=_0xfb7938;VisuMZ['BattleSystemBTB'][_0x791b6f(0xc5)]['call'](this),this['checkActionsBTB'](),this['makeMultiActionsBTB']();},Game_Enemy[_0xfb7938(0xa8)]['checkActionsBTB']=function(){const _0x55ea1d=_0xfb7938;if(!BattleManager[_0x55ea1d(0x1bf)]())return;if(this['numActions']()<=0x0)return;this[_0x55ea1d(0xb0)]=![],this[_0x55ea1d(0x2d4)]()<0x0&&this[_0x55ea1d(0x219)]();},Game_Enemy[_0xfb7938(0xa8)][_0xfb7938(0x1e1)]=function(){const _0x313194=_0xfb7938;if(!BattleManager[_0x313194(0x1bf)]())return;if(this['numActions']()<=0x0)return;const _0x269e57=this[_0x313194(0x115)][0x0];if(!_0x269e57)return;const _0x205867=_0x269e57[_0x313194(0xd6)]();if(!_0x205867)return;const _0x266b77=VisuMZ['BattleSystemBTB'][_0x313194(0x169)],_0x2428fe=_0x205867['note'];let _0x3ab21e=[];if(_0x2428fe['match'](_0x266b77[_0x313194(0x1eb)])){const _0x4746ab=String(RegExp['$1'])[_0x313194(0x285)](',');for(let _0x2471bb of _0x4746ab){_0x2471bb=(String(_0x2471bb)||'')[_0x313194(0x17f)]();const _0x34a20d=/^\d+$/['test'](_0x2471bb);_0x34a20d?_0x3ab21e[_0x313194(0xca)](Number(_0x2471bb)):_0x3ab21e[_0x313194(0xca)](DataManager[_0x313194(0x2a0)](_0x2471bb));}}if(_0x3ab21e[_0x313194(0x279)]<=0x0)return;while(_0x3ab21e[_0x313194(0x279)]>this[_0x313194(0x13e)]()){_0x3ab21e[_0x313194(0xfc)]();}if(_0x3ab21e[_0x313194(0x279)]<=0x0)return;this[_0x313194(0x219)]();for(const _0x28bf3b of _0x3ab21e){const _0x15f66c=new Game_Action(this);_0x15f66c['setSkill'](_0x28bf3b),_0x15f66c[_0x313194(0x215)]=!![],this['_actions'][_0x313194(0xca)](_0x15f66c);}},Game_Enemy[_0xfb7938(0xa8)][_0xfb7938(0x11a)]=function(){const _0x55626b=_0xfb7938;let _0x3db2c4=this[_0x55626b(0x123)]();for(const _0x303bf4 of this['_actions']){if(!_0x303bf4)continue;_0x3db2c4+=_0x303bf4['getTotalActionFusionRecipes']();}return _0x3db2c4-0x1;},VisuMZ['BattleSystemBTB']['Game_Unit_makeActions']=Game_Unit[_0xfb7938(0xa8)][_0xfb7938(0x2c1)],Game_Unit[_0xfb7938(0xa8)][_0xfb7938(0x2c1)]=function(){const _0x5d5b92=_0xfb7938;VisuMZ[_0x5d5b92(0x2ce)]['Game_Unit_makeActions'][_0x5d5b92(0x1f4)](this),BattleManager[_0x5d5b92(0x1bf)]()&&this===$gameTroop&&SceneManager[_0x5d5b92(0x1d4)]()&&BattleManager[_0x5d5b92(0x213)]();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x1f2)]=Game_Party['prototype'][_0xfb7938(0xc0)],Game_Party[_0xfb7938(0xa8)][_0xfb7938(0xc0)]=function(_0x22bb1){const _0x90a56b=_0xfb7938;VisuMZ[_0x90a56b(0x2ce)]['Game_Party_removeActor'][_0x90a56b(0x1f4)](this,_0x22bb1),SceneManager[_0x90a56b(0x1d4)]()&&BattleManager[_0x90a56b(0x13d)]()&&BattleManager['_actionBattlers'][_0x90a56b(0x225)]($gameActors[_0x90a56b(0x24a)](_0x22bb1));},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x178)]=Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x1df)],Scene_Battle[_0xfb7938(0xa8)]['onDisabledPartyCommandSelection']=function(){const _0x292d78=_0xfb7938;BattleManager['isBTB']()?this['selectNextCommand']():VisuMZ['BattleSystemBTB'][_0x292d78(0x178)]['call'](this);},VisuMZ[_0xfb7938(0x2ce)]['Scene_Battle_createActorCommandWindow']=Scene_Battle['prototype']['createActorCommandWindow'],Scene_Battle[_0xfb7938(0xa8)]['createActorCommandWindow']=function(){const _0x27acf3=_0xfb7938;VisuMZ[_0x27acf3(0x2ce)][_0x27acf3(0x22b)][_0x27acf3(0x1f4)](this),this[_0x27acf3(0x253)]();},Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x253)]=function(){const _0x423444=_0xfb7938;if(!BattleManager[_0x423444(0x1bf)]())return;const _0x451c82=this[_0x423444(0x126)];if(!_0x451c82)return;_0x451c82['setHandler']('brave',this[_0x423444(0x2d3)][_0x423444(0x192)](this)),_0x451c82[_0x423444(0x110)](_0x423444(0xef),this[_0x423444(0x121)][_0x423444(0x192)](this));},Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x2d3)]=function(){const _0x4a5385=_0xfb7938;this[_0x4a5385(0x1c6)]();},Scene_Battle[_0xfb7938(0xa8)]['commandCancelBTB']=function(){const _0x2bd797=_0xfb7938,_0x228d03=BattleManager[_0x2bd797(0x24a)]();if(!_0x228d03)this[_0x2bd797(0x1c3)]();else{if(_0x228d03[_0x2bd797(0x123)]()<=0x1)this[_0x2bd797(0x1c3)]();else _0x228d03['_actionInputIndex']>0x0?this[_0x2bd797(0x1c3)]():this[_0x2bd797(0x11e)]();}},Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x1c6)]=function(){const _0x984363=_0xfb7938,_0x197327=BattleManager[_0x984363(0x24a)]();if(!_0x197327)return;_0x197327[_0x984363(0x1c6)]();const _0x1152e8=this[_0x984363(0x126)][_0x984363(0x162)],_0x1672ce=this['_actorCommandWindow'][_0x984363(0x2e2)],_0xed18a2=this[_0x984363(0x126)][_0x984363(0x239)]();this['_actorCommandWindow'][_0x984363(0x15e)](_0x197327),this[_0x984363(0x126)]['select'](_0xed18a2),this[_0x984363(0x126)]['_scrollX']=_0x1152e8,this[_0x984363(0x126)][_0x984363(0x2e2)]=_0x1672ce;},Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x11e)]=function(){const _0x10ce42=_0xfb7938,_0x55a7fc=BattleManager['actor']();if(!_0x55a7fc)return;_0x55a7fc['cancelBrave']();const _0x2b29fa=this[_0x10ce42(0x126)][_0x10ce42(0x162)],_0x2d90f9=this[_0x10ce42(0x126)][_0x10ce42(0x2e2)],_0x132cbf=this[_0x10ce42(0x126)][_0x10ce42(0x239)]();this[_0x10ce42(0x126)]['setup'](_0x55a7fc),this['_actorCommandWindow'][_0x10ce42(0x202)](_0x132cbf),this['_actorCommandWindow']['_scrollX']=_0x2b29fa,this[_0x10ce42(0x126)][_0x10ce42(0x2e2)]=_0x2d90f9;},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x194)]=Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x129)],Scene_Battle[_0xfb7938(0xa8)]['createAllWindows']=function(){const _0x4c3abe=_0xfb7938;VisuMZ[_0x4c3abe(0x2ce)][_0x4c3abe(0x194)][_0x4c3abe(0x1f4)](this),this[_0x4c3abe(0x10b)]();},Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x10b)]=function(){const _0x36df80=_0xfb7938;if(!BattleManager[_0x36df80(0x1bf)]())return;this[_0x36df80(0x2af)]=new Window_BTB_TurnOrder();const _0x57ee2b=this[_0x36df80(0x2ab)](this[_0x36df80(0x266)]);this['addChildAt'](this[_0x36df80(0x2af)],_0x57ee2b),this[_0x36df80(0x152)](),BattleManager[_0x36df80(0x284)](!![]);},Scene_Battle[_0xfb7938(0xa8)][_0xfb7938(0x152)]=function(){const _0x520f30=_0xfb7938,_0x4c00d2=Window_BTB_TurnOrder[_0x520f30(0xe9)];if(_0x4c00d2[_0x520f30(0x25a)]!==_0x520f30(0x26d))return;if(!_0x4c00d2[_0x520f30(0x184)])return;if(!this['_logWindow'])return;const _0x1f86ce=this[_0x520f30(0x2af)]['y']-Math[_0x520f30(0x141)]((Graphics['height']-Graphics[_0x520f30(0xae)])/0x2),_0x3e1cde=_0x1f86ce+this['_btbTurnOrderWindow']['height'];this[_0x520f30(0x27f)]['y']=_0x3e1cde+_0x4c00d2[_0x520f30(0x235)];};function Sprite_BTB_TurnOrder_Battler(){const _0x2929ea=_0xfb7938;this[_0x2929ea(0x165)](...arguments);}Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]=Object[_0xfb7938(0xa2)](Sprite_Clickable[_0xfb7938(0xa8)]),Sprite_BTB_TurnOrder_Battler['prototype'][_0xfb7938(0x264)]=Sprite_BTB_TurnOrder_Battler,Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x165)]=function(_0x433db6,_0xe373ea){const _0xb6cceb=_0xfb7938;this[_0xb6cceb(0x2c9)](_0x433db6,_0xe373ea),Sprite_Clickable[_0xb6cceb(0xa8)][_0xb6cceb(0x165)][_0xb6cceb(0x1f4)](this),this[_0xb6cceb(0xe3)]=0x0,this[_0xb6cceb(0x13a)](),this[_0xb6cceb(0x22f)]();},Sprite_BTB_TurnOrder_Battler['prototype']['initMembers']=function(_0x2d78df,_0x1dec82){const _0x417373=_0xfb7938;this[_0x417373(0x265)]=_0x2d78df,this[_0x417373(0x211)]=_0x1dec82;const _0x2cfcfd=Window_BTB_TurnOrder['Settings'],_0x6f58=this[_0x417373(0xff)](),_0x264926=this['defaultPosition']();this[_0x417373(0x2b5)]=0x0,this['_positionTargetX']=_0x6f58?_0x2cfcfd['SpriteThin']*_0x264926:0x0,this[_0x417373(0x27e)]=_0x6f58?0x0:_0x2cfcfd[_0x417373(0xe0)]*_0x264926,this[_0x417373(0x244)]=0x0,this['_fadeTarget']=0xff,this[_0x417373(0x21d)]=![],this[_0x417373(0x295)]=![],this['_containerWidth']=0x0,this[_0x417373(0x1c7)]=0x0;},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x13a)]=function(){const _0x16761e=_0xfb7938;this[_0x16761e(0x1a0)](),this['createBackgroundSprite'](),this['createGraphicSprite'](),this['createBorderSprite'](),this['createLetterSprite']();},Sprite_BTB_TurnOrder_Battler['prototype']['createInitialPositions']=function(){const _0x499e6a=_0xfb7938;this['x']=this[_0x499e6a(0xde)],this['y']=this['_positionTargetY'];},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0xff)]=function(){const _0x3df1ef=_0xfb7938,_0x314720=Window_BTB_TurnOrder[_0x3df1ef(0xe9)],_0x204545=[_0x3df1ef(0x26d),_0x3df1ef(0x154)][_0x3df1ef(0x2ca)](_0x314720[_0x3df1ef(0x25a)]);return _0x204545;},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x2d2)]=function(){const _0x14b2b8=_0xfb7938,_0x3bce09=Window_BTB_TurnOrder[_0x14b2b8(0xe9)];return this[_0x14b2b8(0xff)]()?_0x3bce09[_0x14b2b8(0xe0)]:_0x3bce09[_0x14b2b8(0x1e0)];},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0xa4)]=function(){const _0x4a3e5d=_0xfb7938,_0x3f3848=Window_BTB_TurnOrder[_0x4a3e5d(0xe9)];return this[_0x4a3e5d(0xff)]()?_0x3f3848[_0x4a3e5d(0x1e0)]:_0x3f3848[_0x4a3e5d(0xe0)];},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]['createTestBitmap']=function(){const _0x12bc89=_0xfb7938;this['bitmap']=new Bitmap(0x48,0x24);const _0x308d9d=this['battler']()?this[_0x12bc89(0x13f)]()[_0x12bc89(0x298)]():'%1\x20%2\x20%3'['format'](this[_0x12bc89(0x265)],this[_0x12bc89(0x211)]);this[_0x12bc89(0x208)]['drawText'](_0x308d9d,0x0,0x0,0x48,0x24,_0x12bc89(0xe1));},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x247)]=function(){const _0x420532=_0xfb7938;if(!Window_BTB_TurnOrder[_0x420532(0xe9)]['ShowMarkerBg'])return;const _0x5d04e3=Window_BTB_TurnOrder['Settings'],_0x3f479d=this[_0x420532(0x265)]===$gameParty?_0x420532(0x145):'Enemy',_0x13b440=_0x420532(0x17b)[_0x420532(0x20c)](_0x3f479d),_0x56df2a=new Sprite();_0x56df2a[_0x420532(0xb2)]['x']=this[_0x420532(0xb2)]['x'],_0x56df2a['anchor']['y']=this[_0x420532(0xb2)]['y'];if(_0x5d04e3[_0x13b440])_0x56df2a['bitmap']=ImageManager['loadSystem'](_0x5d04e3[_0x13b440]);else{const _0x3cac7c=this['bitmapWidth'](),_0x5f3bd2=this[_0x420532(0xa4)]();_0x56df2a[_0x420532(0x208)]=new Bitmap(_0x3cac7c,_0x5f3bd2);const _0x32b6a0=ColorManager['getColor'](_0x5d04e3['%1BgColor1'[_0x420532(0x20c)](_0x3f479d)]),_0xf3ffd0=ColorManager[_0x420532(0xbd)](_0x5d04e3[_0x420532(0x236)['format'](_0x3f479d)]);_0x56df2a[_0x420532(0x208)][_0x420532(0x25e)](0x0,0x0,_0x3cac7c,_0x5f3bd2,_0x32b6a0,_0xf3ffd0,!![]);}this[_0x420532(0x2cd)]=_0x56df2a,this[_0x420532(0x170)](this[_0x420532(0x2cd)]);},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0xa3)]=function(){const _0x2a8873=_0xfb7938,_0x2e11d6=new Sprite();_0x2e11d6['anchor']['x']=this[_0x2a8873(0xb2)]['x'],_0x2e11d6[_0x2a8873(0xb2)]['y']=this[_0x2a8873(0xb2)]['y'],this[_0x2a8873(0x217)]=_0x2e11d6,this['addChild'](this[_0x2a8873(0x217)]),this[_0x2a8873(0x15c)]();},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0xa7)]=function(){const _0x5a613b=_0xfb7938;if(!Window_BTB_TurnOrder[_0x5a613b(0xe9)][_0x5a613b(0x1b6)])return;const _0x7665dd=Window_BTB_TurnOrder[_0x5a613b(0xe9)],_0x128d11=this[_0x5a613b(0x265)]===$gameParty?'Actor':_0x5a613b(0x2db),_0x50f1b7=_0x5a613b(0x1ac)[_0x5a613b(0x20c)](_0x128d11),_0xe056a4=new Sprite();_0xe056a4['anchor']['x']=this[_0x5a613b(0xb2)]['x'],_0xe056a4[_0x5a613b(0xb2)]['y']=this[_0x5a613b(0xb2)]['y'];if(_0x7665dd[_0x50f1b7])_0xe056a4[_0x5a613b(0x208)]=ImageManager[_0x5a613b(0x114)](_0x7665dd[_0x50f1b7]);else{let _0x47d4a5=this[_0x5a613b(0x2d2)](),_0x386b97=this[_0x5a613b(0xa4)](),_0x12192a=_0x7665dd[_0x5a613b(0x1b8)];_0xe056a4[_0x5a613b(0x208)]=new Bitmap(_0x47d4a5,_0x386b97);const _0x51501b='#000000',_0x2068a5=ColorManager[_0x5a613b(0xbd)](_0x7665dd[_0x5a613b(0x2a6)[_0x5a613b(0x20c)](_0x128d11)]);_0xe056a4[_0x5a613b(0x208)][_0x5a613b(0xa1)](0x0,0x0,_0x47d4a5,_0x386b97,_0x51501b),_0x47d4a5-=0x2,_0x386b97-=0x2,_0xe056a4['bitmap'][_0x5a613b(0xa1)](0x1,0x1,_0x47d4a5,_0x386b97,_0x2068a5),_0x47d4a5-=_0x12192a*0x2,_0x386b97-=_0x12192a*0x2,_0xe056a4['bitmap'][_0x5a613b(0xa1)](0x1+_0x12192a,0x1+_0x12192a,_0x47d4a5,_0x386b97,_0x51501b),_0x47d4a5-=0x2,_0x386b97-=0x2,_0x12192a+=0x1,_0xe056a4[_0x5a613b(0x208)][_0x5a613b(0x243)](0x1+_0x12192a,0x1+_0x12192a,_0x47d4a5,_0x386b97);}this[_0x5a613b(0x2cd)]=_0xe056a4,this[_0x5a613b(0x170)](this[_0x5a613b(0x2cd)]),this['width']=this[_0x5a613b(0x2cd)][_0x5a613b(0x15a)],this[_0x5a613b(0x203)]=this[_0x5a613b(0x2cd)]['height'];},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0xf3)]=function(){const _0x252d02=_0xfb7938,_0x5b7748=Window_BTB_TurnOrder[_0x252d02(0xe9)];if(!_0x5b7748[_0x252d02(0xf8)])return;if(this['_unit']===$gameParty)return;const _0x50676f=this['bitmapWidth'](),_0x255f75=this[_0x252d02(0xa4)](),_0x230e1c=new Sprite();_0x230e1c[_0x252d02(0xb2)]['x']=this['anchor']['x'],_0x230e1c[_0x252d02(0xb2)]['y']=this[_0x252d02(0xb2)]['y'],_0x230e1c[_0x252d02(0x208)]=new Bitmap(_0x50676f,_0x255f75),this['_letterSprite']=_0x230e1c,this['addChild'](this[_0x252d02(0x1ee)]);},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x13f)]=function(){const _0x363739=_0xfb7938;return this[_0x363739(0x265)]?this[_0x363739(0x265)][_0x363739(0xf2)]()[this[_0x363739(0x211)]]:null;},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x282)]=function(){const _0x4e6156=_0xfb7938;Sprite_Clickable[_0x4e6156(0xa8)][_0x4e6156(0x282)][_0x4e6156(0x1f4)](this),this[_0x4e6156(0x2c8)](),this[_0x4e6156(0xc8)](),this[_0x4e6156(0x22f)](),this[_0x4e6156(0x268)](),this[_0x4e6156(0x1a6)](),this['updateGraphicHue'](),this['updateLetter'](),this[_0x4e6156(0xec)]();},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x2c8)]=function(){const _0xaa4bc5=_0xfb7938,_0x4e0995=this['containerPosition']();if(this[_0xaa4bc5(0x182)]===_0x4e0995)return;this[_0xaa4bc5(0x182)]=_0x4e0995;this[_0xaa4bc5(0xe3)]<0xff&&this[_0xaa4bc5(0x13f)]()&&_0x4e0995!==this[_0xaa4bc5(0x15b)]()&&this[_0xaa4bc5(0x1c5)](0xff);if(_0x4e0995===this['defaultPosition']()&&this['_fadeDuration']<=0x0&&this[_0xaa4bc5(0xe3)]>0x0)this[_0xaa4bc5(0x1c5)](0x0);else this[_0xaa4bc5(0x244)]<=0x0&&this[_0xaa4bc5(0xe3)]<0xff&&this[_0xaa4bc5(0x22f)]();this[_0xaa4bc5(0xc9)]();},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x2be)]=function(){const _0x1d0942=_0xfb7938,_0x45b494=this[_0x1d0942(0x17a)]();if(!_0x45b494)return;let _0xab260e=![];if(this['_containerWidth']!==_0x45b494[_0x1d0942(0x15a)])_0xab260e=!![];else this['_containerHeight']!==_0x45b494[_0x1d0942(0x203)]&&(_0xab260e=!![]);_0xab260e&&this['calculateTargetPositions']();},Sprite_BTB_TurnOrder_Battler['prototype'][_0xfb7938(0xc9)]=function(){const _0x238528=_0xfb7938,_0x420641=Window_BTB_TurnOrder[_0x238528(0xe9)],_0x24caf2=this[_0x238528(0xff)](),_0x3a0622=_0x420641[_0x238528(0x261)],_0x40b861=_0x420641[_0x238528(0x2bb)],_0x29a406=SceneManager['_scene']['_btbTurnOrderWindow'];if(!_0x29a406)return;const _0x443a3d=this[_0x238528(0x229)]();this[_0x238528(0x2b5)]=_0x420641[_0x238528(0xc2)],this[_0x238528(0xde)]=_0x24caf2?_0x420641['SpriteThin']*_0x443a3d:0x0,this[_0x238528(0x27e)]=_0x24caf2?0x0:_0x420641['SpriteThin']*_0x443a3d,_0x443a3d>0x0&&(this[_0x238528(0xde)]+=_0x24caf2?_0x40b861:0x0,this[_0x238528(0x27e)]+=_0x24caf2?0x0:_0x40b861),_0x3a0622?this[_0x238528(0xde)]=_0x24caf2?_0x29a406[_0x238528(0x15a)]-this[_0x238528(0xde)]-_0x420641[_0x238528(0xe0)]:0x0:this[_0x238528(0x27e)]=_0x24caf2?0x0:_0x29a406['height']-this['_positionTargetY']-_0x420641[_0x238528(0xe0)];},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]['updatePosition']=function(){const _0x14a917=_0xfb7938;if(this[_0x14a917(0x244)]>0x0)return;if(this[_0x14a917(0x2b5)]>0x0){const _0x11b7e0=this[_0x14a917(0x2b5)];this['x']=(this['x']*(_0x11b7e0-0x1)+this[_0x14a917(0xde)])/_0x11b7e0,this['y']=(this['y']*(_0x11b7e0-0x1)+this[_0x14a917(0x27e)])/_0x11b7e0,this[_0x14a917(0x2b5)]--;}if(this['_positionDuration']<=0x0){this['x']=this[_0x14a917(0xde)],this['y']=this[_0x14a917(0x27e)];if(this['opacity']<0xff&&!this[_0x14a917(0x1ce)]&&this['_fadeDuration']<=0x0){const _0x226d33=this['battler']();_0x226d33&&(this[_0x14a917(0x190)]=_0x226d33[_0x14a917(0x1a3)]()&&_0x226d33[_0x14a917(0x133)]()?0xff:0x0);}}},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x15b)]=function(){const _0x1c434c=_0xfb7938,_0x2b1144=Window_BTB_TurnOrder[_0x1c434c(0xe9)],_0x470a8a=this[_0x1c434c(0xff)]()?_0x2b1144[_0x1c434c(0x2c7)]:_0x2b1144[_0x1c434c(0x2b7)];return _0x470a8a+0x1;},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x17a)]=function(){const _0x3ae5bf=_0xfb7938;return SceneManager['_scene'][_0x3ae5bf(0x2af)];},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x229)]=function(){const _0x5cb342=_0xfb7938,_0x2b59b9=this['battler']();if(!_0x2b59b9)return this[_0x5cb342(0x15b)]();if(_0x2b59b9===BattleManager[_0x5cb342(0x1a1)])return 0x0;if(BattleManager['_actionBattlers'][_0x5cb342(0x2ca)](_0x2b59b9)){const _0x3ed104=BattleManager[_0x5cb342(0x1f9)][_0x5cb342(0x12c)](_0x2b59b9)+0x1;return _0x3ed104;}return this[_0x5cb342(0x15b)]();},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x1c5)]=function(_0x3682aa){const _0x1c1d0f=_0xfb7938,_0x5e32d7=Window_BTB_TurnOrder[_0x1c1d0f(0xe9)];this[_0x1c1d0f(0x244)]=_0x5e32d7[_0x1c1d0f(0xc2)],this[_0x1c1d0f(0x190)]=_0x3682aa;},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]['checkOpacity']=function(){const _0x45d43f=_0xfb7938,_0x321dbe=this[_0x45d43f(0x13f)]();if(!_0x321dbe)return;if(this['_isAlive']===_0x321dbe[_0x45d43f(0x1a3)]()&&this[_0x45d43f(0x295)]===_0x321dbe[_0x45d43f(0x133)]())return;this[_0x45d43f(0x21d)]=_0x321dbe[_0x45d43f(0x1a3)](),this[_0x45d43f(0x295)]=_0x321dbe['isAppeared']();let _0x5c0e12=this[_0x45d43f(0x21d)]&&this[_0x45d43f(0x295)]?0xff:0x0;this[_0x45d43f(0x1c5)](_0x5c0e12);},Sprite_BTB_TurnOrder_Battler['prototype'][_0xfb7938(0x268)]=function(){const _0x14e500=_0xfb7938;if(this[_0x14e500(0x244)]>0x0){const _0x32088e=this['_fadeDuration'];this[_0x14e500(0xe3)]=(this['opacity']*(_0x32088e-0x1)+this[_0x14e500(0x190)])/_0x32088e,this[_0x14e500(0x244)]--,this[_0x14e500(0x244)]<=0x0&&(this[_0x14e500(0x2c8)](),this[_0x14e500(0x2b5)]=0x0,this[_0x14e500(0xc8)](),this[_0x14e500(0xe3)]=this[_0x14e500(0x190)]);}if(this[_0x14e500(0x1ce)])return;BattleManager[_0x14e500(0x1e3)]==='battleEnd'&&(this[_0x14e500(0x1ce)]=!![],this[_0x14e500(0x1c5)](0x0));},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]['updateGraphic']=function(){const _0x19456c=_0xfb7938,_0x305f4c=this['battler']();if(!_0x305f4c)return;const _0xbea050=Window_BTB_TurnOrder[_0x19456c(0xe9)],_0x561001=this[_0x19456c(0x265)]===$gameParty?'Actor':_0x19456c(0x2db);let _0x51a51f=_0x305f4c[_0x19456c(0x25f)]();if(_0x305f4c[_0x19456c(0xdb)]()&&_0x51a51f===_0x19456c(0x168))_0x51a51f=_0x19456c(0x1cb);else _0x305f4c[_0x19456c(0xab)]()&&_0x51a51f==='svactor'&&(_0x51a51f='enemy');if(this[_0x19456c(0x223)]!==_0x51a51f)return this[_0x19456c(0x15c)]();switch(this[_0x19456c(0x223)]){case _0x19456c(0x1cb):if(this[_0x19456c(0xb8)]!==_0x305f4c['TurnOrderBTBGraphicFaceName']())return this['processUpdateGraphic']();if(this['_graphicFaceIndex']!==_0x305f4c[_0x19456c(0x29b)]())return this['processUpdateGraphic']();break;case'icon':if(this['_graphicIconIndex']!==_0x305f4c[_0x19456c(0xb5)]())return this[_0x19456c(0x15c)]();break;case _0x19456c(0x168):if(_0x305f4c['hasSvBattler']()){if(this[_0x19456c(0x2b8)]!==_0x305f4c[_0x19456c(0x1e5)]())return this['processUpdateGraphic']();}else{if(this['_graphicEnemy']!==_0x305f4c[_0x19456c(0x2a2)]())return this[_0x19456c(0x15c)]();}break;case'svactor':if(_0x305f4c[_0x19456c(0xdb)]()){if(this['_graphicSv']!==_0x305f4c[_0x19456c(0x2a2)]())return this[_0x19456c(0x15c)]();}else{if(this['_graphicEnemy']!==_0x305f4c[_0x19456c(0x2a2)]())return this[_0x19456c(0x15c)]();}break;}},Sprite_BTB_TurnOrder_Battler['prototype'][_0xfb7938(0x15c)]=function(){const _0x19f06b=_0xfb7938,_0x2bfb3a=this[_0x19f06b(0x13f)]();if(!_0x2bfb3a)return;this[_0x19f06b(0x223)]=_0x2bfb3a[_0x19f06b(0x25f)]();if(_0x2bfb3a['isActor']()&&this['_graphicType']===_0x19f06b(0x168))this['_graphicType']=_0x19f06b(0x1cb);else _0x2bfb3a['isEnemy']()&&this['_graphicType']==='svactor'&&(this['_graphicType']='enemy');let _0xe87638;switch(this[_0x19f06b(0x223)]){case'face':this['_graphicFaceName']=_0x2bfb3a[_0x19f06b(0xed)](),this['_graphicFaceIndex']=_0x2bfb3a['TurnOrderBTBGraphicFaceIndex'](),_0xe87638=ImageManager[_0x19f06b(0x1fd)](this['_graphicFaceName']),_0xe87638[_0x19f06b(0x1d7)](this[_0x19f06b(0x16d)][_0x19f06b(0x192)](this,_0xe87638));break;case _0x19f06b(0x26f):this[_0x19f06b(0x2dc)]=_0x2bfb3a[_0x19f06b(0x132)](),_0xe87638=ImageManager[_0x19f06b(0x114)](_0x19f06b(0x116)),_0xe87638[_0x19f06b(0x1d7)](this[_0x19f06b(0x1bd)]['bind'](this,_0xe87638));break;case _0x19f06b(0x168):if(_0x2bfb3a[_0x19f06b(0x197)]())this['_graphicSv']=_0x2bfb3a[_0x19f06b(0x1e5)](),_0xe87638=ImageManager['loadSvActor'](this['_graphicSv']),_0xe87638[_0x19f06b(0x1d7)](this['changeSvActorGraphicBitmap']['bind'](this,_0xe87638));else $gameSystem[_0x19f06b(0x24b)]()?(this[_0x19f06b(0x98)]=_0x2bfb3a['battlerName'](),_0xe87638=ImageManager[_0x19f06b(0xdf)](this[_0x19f06b(0x98)]),_0xe87638[_0x19f06b(0x1d7)](this[_0x19f06b(0xdc)][_0x19f06b(0x192)](this,_0xe87638))):(this[_0x19f06b(0x98)]=_0x2bfb3a[_0x19f06b(0x2a2)](),_0xe87638=ImageManager['loadEnemy'](this[_0x19f06b(0x98)]),_0xe87638['addLoadListener'](this['changeEnemyGraphicBitmap']['bind'](this,_0xe87638)));break;case _0x19f06b(0x12b):this[_0x19f06b(0x2b8)]=_0x2bfb3a['battlerName'](),_0xe87638=ImageManager[_0x19f06b(0x2ac)](this[_0x19f06b(0x2b8)]),_0xe87638[_0x19f06b(0x1d7)](this[_0x19f06b(0x17d)]['bind'](this,_0xe87638));break;}},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x16d)]=function(_0x1eff33){const _0x46d0f4=_0xfb7938,_0x6fd045=this[_0x46d0f4(0x2d5)],_0xcb927c=this[_0x46d0f4(0x2d2)](),_0x3b4c16=this[_0x46d0f4(0xa4)](),_0x54f839=Math['max'](_0xcb927c,_0x3b4c16);this[_0x46d0f4(0x217)]['bitmap']=new Bitmap(_0xcb927c,_0x3b4c16);const _0x31e87d=this[_0x46d0f4(0x217)]['bitmap'],_0x33352c=ImageManager['faceWidth'],_0x4aa787=ImageManager[_0x46d0f4(0x1da)],_0x2ffd95=_0x54f839/Math[_0x46d0f4(0x2c5)](_0x33352c,_0x4aa787),_0x502e0c=ImageManager[_0x46d0f4(0x11b)],_0x37b804=ImageManager[_0x46d0f4(0x1da)],_0x4f6cf8=_0x6fd045%0x4*_0x33352c+(_0x33352c-_0x502e0c)/0x2,_0x4f1dce=Math[_0x46d0f4(0x27a)](_0x6fd045/0x4)*_0x4aa787+(_0x4aa787-_0x37b804)/0x2,_0x1132b9=(_0xcb927c-_0x33352c*_0x2ffd95)/0x2,_0x280835=(_0x3b4c16-_0x4aa787*_0x2ffd95)/0x2;_0x31e87d['blt'](_0x1eff33,_0x4f6cf8,_0x4f1dce,_0x502e0c,_0x37b804,_0x1132b9,_0x280835,_0x54f839,_0x54f839);},Sprite_BTB_TurnOrder_Battler['prototype'][_0xfb7938(0x1bd)]=function(_0x3026fe){const _0x3e7212=_0xfb7938,_0x269084=this[_0x3e7212(0x2dc)],_0xe0fb6=this[_0x3e7212(0x2d2)](),_0x46ed06=this[_0x3e7212(0xa4)]();this[_0x3e7212(0x217)][_0x3e7212(0x208)]=new Bitmap(_0xe0fb6,_0x46ed06);const _0x102e6a=this[_0x3e7212(0x217)][_0x3e7212(0x208)],_0x150925=ImageManager[_0x3e7212(0x1c4)],_0x39a9c6=ImageManager[_0x3e7212(0x158)],_0xeac285=Math['min'](_0x150925,_0x39a9c6,_0xe0fb6,_0x46ed06),_0x22d562=_0x269084%0x10*_0x150925,_0x10f2be=Math[_0x3e7212(0x27a)](_0x269084/0x10)*_0x39a9c6,_0x5742e1=Math[_0x3e7212(0x27a)](Math[_0x3e7212(0x2c5)](_0xe0fb6-_0xeac285,0x0)/0x2),_0x236108=Math['floor'](Math[_0x3e7212(0x2c5)](_0x46ed06-_0xeac285,0x0)/0x2);_0x102e6a[_0x3e7212(0x119)](_0x3026fe,_0x22d562,_0x10f2be,_0x150925,_0x39a9c6,_0x5742e1,_0x236108,_0xeac285,_0xeac285);},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x17d)]=function(_0x94c69f){const _0x295750=_0xfb7938,_0x23cb2d=this[_0x295750(0x2d2)](),_0x226a2b=this[_0x295750(0xa4)](),_0x52a285=Math[_0x295750(0x242)](_0x23cb2d,_0x226a2b);this['_graphicSprite'][_0x295750(0x208)]=new Bitmap(_0x23cb2d,_0x226a2b);const _0x294ac8=this['_graphicSprite'][_0x295750(0x208)],_0x31781a=0x9,_0xf82c8c=0x6,_0x53a3d8=_0x94c69f[_0x295750(0x15a)]/_0x31781a,_0x7a328c=_0x94c69f['height']/_0xf82c8c,_0x3e73e0=Math[_0x295750(0x242)](0x1,_0x52a285/_0x53a3d8,_0x52a285/_0x7a328c),_0x3f3443=_0x53a3d8*_0x3e73e0,_0x232558=_0x7a328c*_0x3e73e0,_0x2033ca=Math[_0x295750(0x141)]((_0x23cb2d-_0x3f3443)/0x2),_0x186632=Math[_0x295750(0x141)]((_0x226a2b-_0x232558)/0x2);_0x294ac8[_0x295750(0x119)](_0x94c69f,0x0,0x0,_0x53a3d8,_0x7a328c,_0x2033ca,_0x186632,_0x3f3443,_0x232558);},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]['changeEnemyGraphicBitmap']=function(_0x2cfa4e){const _0x5959c4=_0xfb7938,_0x3d9b87=Window_BTB_TurnOrder['Settings'],_0x516d9f=this[_0x5959c4(0x2d2)](),_0x16f350=this[_0x5959c4(0xa4)](),_0x3f21a5=Math[_0x5959c4(0x242)](_0x516d9f,_0x16f350);this[_0x5959c4(0x217)][_0x5959c4(0x208)]=new Bitmap(_0x516d9f,_0x16f350);const _0x1b2674=this[_0x5959c4(0x217)][_0x5959c4(0x208)],_0x14f22a=Math[_0x5959c4(0x242)](0x1,_0x3f21a5/_0x2cfa4e[_0x5959c4(0x15a)],_0x3f21a5/_0x2cfa4e[_0x5959c4(0x203)]),_0x210822=_0x2cfa4e[_0x5959c4(0x15a)]*_0x14f22a,_0x546db1=_0x2cfa4e['height']*_0x14f22a,_0xfc5d49=Math['round']((_0x516d9f-_0x210822)/0x2),_0x4e412e=Math[_0x5959c4(0x141)]((_0x16f350-_0x546db1)/0x2);_0x1b2674['blt'](_0x2cfa4e,0x0,0x0,_0x2cfa4e[_0x5959c4(0x15a)],_0x2cfa4e[_0x5959c4(0x203)],_0xfc5d49,_0x4e412e,_0x210822,_0x546db1);},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0x148)]=function(){const _0x1d8033=_0xfb7938,_0x28cdaf=this[_0x1d8033(0x13f)]();if(!_0x28cdaf)return;if(!_0x28cdaf['isEnemy']())return;if(this['_graphicHue']===_0x28cdaf['battlerHue']())return;this[_0x1d8033(0x28e)]=_0x28cdaf[_0x1d8033(0x24d)]();if(_0x28cdaf[_0x1d8033(0x197)]())this['_graphicHue']=0x0;this[_0x1d8033(0x217)][_0x1d8033(0x1f8)](this[_0x1d8033(0x28e)]);},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)]['updateLetter']=function(){const _0x48f819=_0xfb7938;if(!this['_letterSprite'])return;const _0x35ae72=this['battler']();if(!_0x35ae72)return;if(this[_0x48f819(0x28f)]===_0x35ae72[_0x48f819(0x28f)]&&this[_0x48f819(0xa0)]===_0x35ae72[_0x48f819(0xa0)])return;this['_letter']=_0x35ae72[_0x48f819(0x28f)],this[_0x48f819(0xa0)]=_0x35ae72[_0x48f819(0xa0)];const _0x50e6a7=Window_BTB_TurnOrder['Settings'],_0x345e1d=this[_0x48f819(0xff)](),_0x3f9d5d=this[_0x48f819(0x2d2)](),_0x492f1c=this[_0x48f819(0xa4)](),_0xc0032d=this[_0x48f819(0x1ee)][_0x48f819(0x208)];_0xc0032d[_0x48f819(0x150)]();if(!this['_plural'])return;_0xc0032d[_0x48f819(0x1c0)]=_0x50e6a7['EnemyBattlerFontFace']||$gameSystem[_0x48f819(0xf5)](),_0xc0032d[_0x48f819(0x299)]=_0x50e6a7['EnemyBattlerFontSize']||0x10,_0x345e1d?_0xc0032d[_0x48f819(0x271)](this[_0x48f819(0x28f)][_0x48f819(0x17f)](),0x0,_0x492f1c/0x2,_0x3f9d5d,_0x492f1c/0x2,_0x48f819(0xe1)):_0xc0032d[_0x48f819(0x271)](this['_letter']['trim'](),0x0,0x2,_0x3f9d5d-0x8,_0x492f1c-0x4,_0x48f819(0x1e7));},Sprite_BTB_TurnOrder_Battler[_0xfb7938(0xa8)][_0xfb7938(0xec)]=function(){const _0x53eef3=_0xfb7938,_0x295a20=this[_0x53eef3(0x13f)]();if(!_0x295a20)return;const _0x2fbba0=_0x295a20['battler']();if(!_0x2fbba0)return;const _0xd1fd31=_0x2fbba0['mainSprite']();if(!_0xd1fd31)return;this[_0x53eef3(0x2a3)](_0xd1fd31['_blendColor']);},Sprite_BTB_TurnOrder_Battler['prototype']['getStateTooltipBattler']=function(){return this['battler']();},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0xbf)]=Window_Base[_0xfb7938(0xa8)][_0xfb7938(0x296)],Window_Base[_0xfb7938(0xa8)][_0xfb7938(0x296)]=function(_0x535e88,_0x326f63,_0x5e34eb){const _0x31ce07=_0xfb7938;return _0x5e34eb=VisuMZ[_0x31ce07(0x2ce)][_0x31ce07(0xbf)][_0x31ce07(0x1f4)](this,_0x535e88,_0x326f63,_0x5e34eb),_0x5e34eb=this['makeAdditionalCostTextBTB'](_0x535e88,_0x326f63,_0x5e34eb),_0x5e34eb;},VisuMZ[_0xfb7938(0x2ce)]['Window_Base_drawItemNumber']=Window_Base[_0xfb7938(0xa8)][_0xfb7938(0x209)],Window_Base[_0xfb7938(0xa8)]['drawItemNumber']=function(_0x2eddd4,_0x576a97,_0x599888,_0x27a9ce){const _0x25fdf0=_0xfb7938;BattleManager[_0x25fdf0(0x1bf)]()&&this[_0x25fdf0(0x264)]===Window_BattleItem?this[_0x25fdf0(0x20a)](_0x2eddd4,_0x576a97,_0x599888,_0x27a9ce):VisuMZ[_0x25fdf0(0x2ce)][_0x25fdf0(0x2d6)][_0x25fdf0(0x1f4)](this,_0x2eddd4,_0x576a97,_0x599888,_0x27a9ce),this[_0x25fdf0(0xf4)]();},Window_Base[_0xfb7938(0xa8)]['drawItemNumberBTB']=function(_0x3c6074,_0xff0278,_0x4c93a2,_0x3a4993){const _0x7ce1d8=_0xfb7938,_0x585241=VisuMZ[_0x7ce1d8(0x2ce)][_0x7ce1d8(0xe9)][_0x7ce1d8(0x135)],_0x28550c=BattleManager[_0x7ce1d8(0x19f)]||$gameParty['members']()[0x0],_0x3aec87=this[_0x7ce1d8(0x2d1)](_0x28550c,_0x3c6074,''),_0x4a6d2e=this['textSizeEx'](_0x3aec87)[_0x7ce1d8(0x15a)],_0x752201=_0x585241[_0x7ce1d8(0xc1)];let _0x57fded=_0xff0278+_0x3a4993-_0x4a6d2e;if(_0x3aec87==='')VisuMZ[_0x7ce1d8(0x2ce)][_0x7ce1d8(0x2d6)][_0x7ce1d8(0x1f4)](this,_0x3c6074,_0xff0278,_0x4c93a2,_0x3a4993);else{if(this['isDrawItemNumber'](_0x3c6074)){this[_0x7ce1d8(0xf4)]();const _0x3accd6=VisuMZ[_0x7ce1d8(0x25b)]['Settings'][_0x7ce1d8(0x23c)];this[_0x7ce1d8(0xc3)][_0x7ce1d8(0x299)]=_0x3accd6[_0x7ce1d8(0x10e)];if(_0x752201){const _0x563404=_0x3accd6[_0x7ce1d8(0x1d3)],_0x192cef=_0x563404[_0x7ce1d8(0x20c)]($gameParty['numItems'](_0x3c6074)),_0xd97919=this[_0x7ce1d8(0x1fe)](_0x192cef+this[_0x7ce1d8(0x138)]());_0x57fded-=_0xd97919;}else _0x3a4993-=this[_0x7ce1d8(0x1fe)](this[_0x7ce1d8(0x138)]())+_0x4a6d2e;VisuMZ[_0x7ce1d8(0x2ce)][_0x7ce1d8(0x2d6)][_0x7ce1d8(0x1f4)](this,_0x3c6074,_0xff0278,_0x4c93a2,_0x3a4993);}}this[_0x7ce1d8(0x2e1)](_0x3aec87,_0x57fded,_0x4c93a2);},Window_Base[_0xfb7938(0xa8)]['makeAdditionalCostTextBTB']=function(_0x2fe724,_0x491b4d,_0x2dc89a){const _0x52175f=_0xfb7938;if(!BattleManager[_0x52175f(0x1bf)]())return _0x2dc89a;if(!_0x2fe724)return _0x2dc89a;if(!_0x491b4d)return _0x2dc89a;if(_0x491b4d['note'][_0x52175f(0x20e)](VisuMZ['BattleSystemBTB'][_0x52175f(0x169)][_0x52175f(0x21b)]))return _0x2dc89a;let _0x2ffba1=_0x2fe724[_0x52175f(0xbe)](_0x491b4d);const _0x4634cb=VisuMZ['BattleSystemBTB']['Settings'][_0x52175f(0x135)],_0x217360=_0x4634cb[_0x52175f(0xc1)],_0x42a5ed=_0x4634cb[_0x52175f(0xfd)],_0x3ab9bb=_0x4634cb[_0x52175f(0x2da)],_0x4201e5=_0x4634cb[_0x52175f(0x254)]||0x0,_0x139189=_0x4634cb[_0x52175f(0x10f)],_0x4324de=_0x4634cb[_0x52175f(0x2a4)];if(DataManager[_0x52175f(0x238)](_0x491b4d)&&this[_0x52175f(0x264)]===Window_ActorCommand){if(!_0x42a5ed&&_0x491b4d['id']===_0x2fe724[_0x52175f(0x291)]())return _0x2dc89a;if(!_0x3ab9bb&&_0x491b4d['id']===_0x2fe724[_0x52175f(0x281)]())return _0x2dc89a;}_0x2ffba1-=_0x4201e5;if(_0x2ffba1<0x0)return _0x2dc89a;if(!_0x139189&&_0x2ffba1===0x0)return _0x2dc89a;if(!_0x4324de&&_0x2ffba1===0x1)return _0x2dc89a;const _0x2c4d88='\x5cI[%1]'[_0x52175f(0x20c)](ImageManager['btbBravePointsIcon']),_0x5f3a09=TextManager[_0x52175f(0x2a9)];let _0x3c114a=TextManager['btbCostFormat']['format'](_0x2ffba1,_0x5f3a09,_0x2c4d88);if(_0x2dc89a==='')_0x2dc89a+=_0x3c114a;else _0x217360?_0x2dc89a=_0x3c114a+this['skillCostSeparator']()+_0x2dc89a:_0x2dc89a=_0x2dc89a+this['skillCostSeparator']()+_0x3c114a;return _0x2dc89a;},Window_Selectable[_0xfb7938(0xa8)][_0xfb7938(0x1f0)]=function(){return![];},VisuMZ['BattleSystemBTB'][_0xfb7938(0x276)]=Window_Selectable['prototype'][_0xfb7938(0x202)],Window_Selectable[_0xfb7938(0xa8)]['select']=function(_0x4b4ed2){const _0x3cff28=_0xfb7938;VisuMZ[_0x3cff28(0x2ce)][_0x3cff28(0x276)][_0x3cff28(0x1f4)](this,_0x4b4ed2),this['isBattleItemWindowBTB']()&&this[_0x3cff28(0x1ed)]&&this[_0x3cff28(0x21e)]();},Window_Selectable[_0xfb7938(0xa8)][_0xfb7938(0x21e)]=function(){const _0x238c78=_0xfb7938;BattleManager[_0x238c78(0xfb)]();},VisuMZ['BattleSystemBTB'][_0xfb7938(0xaa)]=Window_Help[_0xfb7938(0xa8)][_0xfb7938(0x2df)],Window_Help[_0xfb7938(0xa8)][_0xfb7938(0x2df)]=function(_0x13b1c6){const _0x333a16=_0xfb7938;BattleManager[_0x333a16(0x1bf)]()&&_0x13b1c6&&_0x13b1c6[_0x333a16(0x210)]&&_0x13b1c6[_0x333a16(0x210)]['match'](VisuMZ[_0x333a16(0x2ce)]['RegExp'][_0x333a16(0xb3)])?this[_0x333a16(0x13c)](String(RegExp['$1'])):VisuMZ[_0x333a16(0x2ce)]['Window_Help_setItem'][_0x333a16(0x1f4)](this,_0x13b1c6);},VisuMZ['BattleSystemBTB'][_0xfb7938(0x2d8)]=Window_BattleLog[_0xfb7938(0xa8)]['startAction'],Window_BattleLog[_0xfb7938(0xa8)][_0xfb7938(0x172)]=function(_0x4a6237,_0x373a7a,_0xaff400){const _0x46a1e8=_0xfb7938;this['showBraveAnimationBTB'](_0x4a6237)?this['queueBraveAnimationsBTB'](_0x4a6237,_0x373a7a,_0xaff400):VisuMZ['BattleSystemBTB'][_0x46a1e8(0x2d8)][_0x46a1e8(0x1f4)](this,_0x4a6237,_0x373a7a,_0xaff400);},Window_BattleLog[_0xfb7938(0xa8)][_0xfb7938(0x234)]=function(_0x13dc09,_0x106f32,_0x11e55b){const _0xfda87b=_0xfb7938;VisuMZ[_0xfda87b(0x2ce)][_0xfda87b(0x2d8)][_0xfda87b(0x1f4)](this,_0x13dc09,_0x106f32,_0x11e55b);},Window_BattleLog[_0xfb7938(0xa8)][_0xfb7938(0x143)]=function(_0x1df526){const _0x2724b0=_0xfb7938;if(!BattleManager[_0x2724b0(0x1bf)]())return![];if(!_0x1df526)return![];if(!_0x1df526[_0x2724b0(0xab)]())return![];if(_0x1df526['_braveStartupAnimation'])return![];const _0x41485b=VisuMZ[_0x2724b0(0x2ce)][_0x2724b0(0xe9)][_0x2724b0(0x230)];if(!_0x41485b[_0x2724b0(0x245)])return![];if(_0x41485b[_0x2724b0(0x161)]<=0x0)return![];return VisuMZ[_0x2724b0(0x2ce)][_0x2724b0(0xe9)][_0x2724b0(0x230)][_0x2724b0(0x245)];},Window_BattleLog[_0xfb7938(0xa8)][_0xfb7938(0x28c)]=function(_0x3a06a8,_0xe1a634,_0x28a80c){const _0x476891=_0xfb7938;_0x3a06a8['_braveStartupAnimation']=!![];let _0xaf6aca=_0x3a06a8[_0x476891(0x11a)]();const _0x38e8da=VisuMZ[_0x476891(0x2ce)][_0x476891(0xe9)][_0x476891(0x230)],_0x5c27f4=_0x38e8da['BraveAnimationID'],_0x351d7f=_0x38e8da['WaitFrames'];while(_0xaf6aca--){this[_0x476891(0xca)](_0x476891(0x14d),[_0x3a06a8],_0x5c27f4),_0xaf6aca>0x0?this[_0x476891(0xca)](_0x476891(0x183),_0x351d7f):this[_0x476891(0xca)](_0x476891(0x179));}this['push'](_0x476891(0x234),_0x3a06a8,_0xe1a634,_0x28a80c);},VisuMZ[_0xfb7938(0x2ce)]['Window_ActorCommand_addGuardCommand']=Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x288)],Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x288)]=function(){const _0x122f54=_0xfb7938;this['addBraveCommand'](),VisuMZ['BattleSystemBTB'][_0x122f54(0xaf)]['call'](this);},Window_ActorCommand['prototype'][_0xfb7938(0xa5)]=function(){const _0x5323a7=_0xfb7938;if(!this[_0x5323a7(0x26c)]())return;const _0x4e5621=this[_0x5323a7(0x1ae)](),_0x32c1ac=TextManager['btbBraveCommand'],_0x297658=ImageManager[_0x5323a7(0x1c1)],_0x37fc81=_0x4e5621===_0x5323a7(0xe4)?_0x32c1ac:_0x5323a7(0x10d)['format'](_0x297658,_0x32c1ac);this[_0x5323a7(0x139)](_0x37fc81,_0x5323a7(0xda),this[_0x5323a7(0x19f)][_0x5323a7(0x283)]()),BattleManager['refreshStatusBTB']();},Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x26c)]=function(){const _0x5175ed=_0xfb7938;if(!BattleManager[_0x5175ed(0x1bf)]())return![];if(!VisuMZ[_0x5175ed(0x2ce)][_0x5175ed(0xe9)]['Window'][_0x5175ed(0x131)])return![];if(this['_actor']&&this[_0x5175ed(0x19f)][_0x5175ed(0x1f6)]())return![];return!![];},VisuMZ[_0xfb7938(0x2ce)]['Window_Selectable_cursorPagedown']=Window_Selectable[_0xfb7938(0xa8)][_0xfb7938(0xa9)],Window_Selectable[_0xfb7938(0xa8)][_0xfb7938(0xa9)]=function(){const _0x19d6e4=_0xfb7938;this[_0x19d6e4(0x216)]()?this['_actor']&&!this['_actor'][_0x19d6e4(0x1f6)]()&&this[_0x19d6e4(0x19f)][_0x19d6e4(0x283)]()&&SceneManager['_scene'][_0x19d6e4(0x1c6)]():VisuMZ[_0x19d6e4(0x2ce)]['Window_Selectable_cursorPagedown'][_0x19d6e4(0x1f4)](this);},VisuMZ[_0xfb7938(0x2ce)]['Window_Selectable_cursorPageup']=Window_Selectable['prototype'][_0xfb7938(0x278)],Window_Selectable['prototype']['cursorPageup']=function(){const _0x3002ad=_0xfb7938;this[_0x3002ad(0x216)]()?this[_0x3002ad(0x19f)]&&!this[_0x3002ad(0x19f)][_0x3002ad(0x1f6)]()&&this[_0x3002ad(0x19f)]['numActions']()>0x1&&SceneManager[_0x3002ad(0x294)][_0x3002ad(0x11e)]():VisuMZ[_0x3002ad(0x2ce)]['Window_Selectable_cursorPageup'][_0x3002ad(0x1f4)](this);},Window_Selectable[_0xfb7938(0xa8)]['isUsePageUpDnShortcutBTB']=function(){const _0x17503a=_0xfb7938;if(this[_0x17503a(0x264)]!==Window_ActorCommand)return![];if(!SceneManager[_0x17503a(0x1d4)]())return![];if(!BattleManager[_0x17503a(0x1bf)]())return![];return VisuMZ[_0x17503a(0x2ce)][_0x17503a(0xe9)][_0x17503a(0x163)][_0x17503a(0x26e)];},VisuMZ[_0xfb7938(0x2ce)]['Window_ActorCommand_makeCommandList']=Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x9d)],Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x9d)]=function(){const _0x1b7c04=_0xfb7938;VisuMZ[_0x1b7c04(0x2ce)][_0x1b7c04(0x200)]['call'](this),this['createBTBActionCounters']();},VisuMZ['BattleSystemBTB'][_0xfb7938(0x14b)]=Window_Base[_0xfb7938(0xa8)][_0xfb7938(0x2d9)],Window_Base[_0xfb7938(0xa8)][_0xfb7938(0x2d9)]=function(){const _0x316505=_0xfb7938;VisuMZ[_0x316505(0x2ce)]['Window_Base_close']['call'](this),SceneManager['isSceneBattle']()&&this[_0x316505(0x108)]&&this[_0x316505(0x108)]();},Window_ActorCommand[_0xfb7938(0xa8)]['destroyBTBActionCounters']=function(){const _0x4f1d8a=_0xfb7938;if(!this['_btbActionSprite'])return;this['_btbActionSprite'][_0x4f1d8a(0x208)]&&this[_0x4f1d8a(0x2b3)][_0x4f1d8a(0x208)][_0x4f1d8a(0x171)](),this[_0x4f1d8a(0x2cc)](this[_0x4f1d8a(0x2b3)]),delete this[_0x4f1d8a(0x2b3)];},Window_ActorCommand[_0xfb7938(0xa8)]['createBTBActionCounters']=function(){const _0x290016=_0xfb7938;if(!BattleManager[_0x290016(0x1bf)]())return;if(!this[_0x290016(0x19f)])return;this[_0x290016(0x108)]();if(this['_actor']['hideBraveTrait']())return;this['_btbActionSprite']=new Sprite(),this[_0x290016(0x170)](this[_0x290016(0x2b3)]),this[_0x290016(0x1a7)]();},Window_ActorCommand[_0xfb7938(0xa8)]['modifyBTBActionCounterSprite']=function(){const _0x3ab858=_0xfb7938,_0x25642f=VisuMZ[_0x3ab858(0x2ce)]['Settings'][_0x3ab858(0x163)]['DrawActionCountersJS'];_0x25642f?_0x25642f[_0x3ab858(0x1f4)](this,this[_0x3ab858(0x2b3)],this,this['_actor']):this[_0x3ab858(0x206)][_0x3ab858(0x1f4)](this,this[_0x3ab858(0x2b3)],this,this[_0x3ab858(0x19f)]);},Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x206)]=function(){const _0x2e14b1=_0xfb7938,_0x127918=arguments[0x0],_0x4dc865=arguments[0x1],_0x19719b=arguments[0x2];_0x127918['x']=Math[_0x2e14b1(0x141)](_0x4dc865[_0x2e14b1(0x15a)]/0x2),_0x127918['y']=0x0,_0x127918[_0x2e14b1(0xb2)]['x']=0.5,_0x127918['anchor']['y']=0.5;const _0xec7978=TextManager[_0x2e14b1(0xb9)],_0x4ffcec=TextManager[_0x2e14b1(0x28d)];let _0x37095d=_0xec7978[_0x2e14b1(0x297)](_0x19719b[_0x2e14b1(0x123)]());const _0x514bc2=_0x19719b['_actionInputIndex'];_0x37095d=_0x37095d[_0x2e14b1(0x1a2)](0x0,_0x514bc2)+_0x4ffcec+_0x37095d[_0x2e14b1(0x1a2)](_0x514bc2+0x1);const _0x57baa5=new Bitmap(_0x4dc865['width'],_0x4dc865[_0x2e14b1(0x1b7)]());_0x57baa5['fontSize']=0x24,_0x57baa5[_0x2e14b1(0x271)](_0x37095d,0x0,0x0,_0x57baa5[_0x2e14b1(0x15a)],_0x57baa5[_0x2e14b1(0x203)],'center'),_0x127918['bitmap']=_0x57baa5;},Window_ActorCommand[_0xfb7938(0xa8)][_0xfb7938(0x1f0)]=function(){const _0x17af0a=_0xfb7938;return BattleManager[_0x17af0a(0x1bf)]();},Window_ActorCommand[_0xfb7938(0xa8)]['applyBattleItemWindowBTB']=function(){const _0x4bea78=_0xfb7938,_0x7854fc=BattleManager[_0x4bea78(0x18a)]();if(_0x7854fc){const _0x3665dd=this[_0x4bea78(0x212)]();switch(_0x3665dd){case _0x4bea78(0x1be):_0x7854fc[_0x4bea78(0xbc)]();break;case _0x4bea78(0xee):_0x7854fc[_0x4bea78(0x2ae)]();break;case _0x4bea78(0x250):_0x7854fc[_0x4bea78(0xbb)](this['currentExt']());break;default:_0x7854fc[_0x4bea78(0xbb)](null);break;}}Window_Command['prototype'][_0x4bea78(0x21e)][_0x4bea78(0x1f4)](this);},Window_Base[_0xfb7938(0xa8)][_0xfb7938(0x1e8)]=function(_0x1d5d51,_0x2946c7,_0x1abf4f,_0x260330,_0x51ee22){const _0x1d9271=_0xfb7938;if(!_0x1d5d51)return;if(!BattleManager[_0x1d9271(0x1bf)]())return;const _0xc93454=VisuMZ[_0x1d9271(0x2ce)]['Settings'][_0x1d9271(0x163)],_0x531378=BattleManager[_0x1d9271(0xd7)]()?_0xc93454[_0x1d9271(0x260)]:_0xc93454['StatusDisplayFmt'],_0x21e3ed=_0xc93454[_0x1d9271(0x185)],_0x54cd57=_0xc93454[_0x1d9271(0x259)],_0x577b83=_0xc93454['NegativeColor'];let _0x345e42=0x0,_0x2d961d=0x0;_0x2d961d=_0x1d5d51['bravePoints']();if(_0x2d961d>0x0)_0x345e42=_0x54cd57;if(_0x2d961d===0x0)_0x345e42=_0x21e3ed;if(_0x2d961d<0x0)_0x345e42=_0x577b83;const _0x5134f2=_0x1d9271(0x101)['format'](_0x345e42,_0x2d961d),_0x3b7343=_0x1d9271(0x12f)['format'](ImageManager[_0x1d9271(0x1c1)]);_0x2d961d=_0x1d5d51[_0x1d9271(0xf1)]();if(_0x2d961d>0x0)_0x345e42=_0x54cd57;if(_0x2d961d===0x0)_0x345e42=_0x21e3ed;_0x2d961d<0x0&&(_0x345e42=_0x577b83);const _0x5dde90=_0x1d9271(0x101)[_0x1d9271(0x20c)](_0x345e42,_0x2d961d);let _0x2625ad=_0x531378[_0x1d9271(0x20c)](_0x5134f2,TextManager['btbBravePointsAbbr'],_0x3b7343,_0x5dde90);const _0x508773=this[_0x1d9271(0x255)](_0x2625ad)[_0x1d9271(0x15a)];if(_0x51ee22==='center')_0x2946c7+=Math[_0x1d9271(0x141)]((_0x260330-_0x508773)/0x2);else _0x51ee22===_0x1d9271(0x1e7)&&(_0x2946c7+=Math['round'](_0x260330-_0x508773));this['drawTextEx'](_0x2625ad,_0x2946c7,_0x1abf4f,_0x260330);},Window_StatusBase['prototype'][_0xfb7938(0x237)]=function(_0x449b64){const _0x32763b=_0xfb7938;if(!_0x449b64)return![];if(!BattleManager['isBTB']())return![];if(!this[_0x32763b(0x18d)])return![];if(_0x449b64[_0x32763b(0x1f6)]())return![];const _0x480956=VisuMZ[_0x32763b(0x2ce)][_0x32763b(0xe9)][_0x32763b(0x163)],_0x4253c1=this[_0x32763b(0x18d)]();return _0x480956['%1_display'[_0x32763b(0x20c)](_0x4253c1)];},VisuMZ['BattleSystemBTB'][_0xfb7938(0xd2)]=Window_BattleStatus[_0xfb7938(0xa8)][_0xfb7938(0x274)],Window_BattleStatus[_0xfb7938(0xa8)][_0xfb7938(0x274)]=function(_0x4e25d8){const _0x41a4a2=_0xfb7938;VisuMZ[_0x41a4a2(0x2ce)][_0x41a4a2(0xd2)][_0x41a4a2(0x1f4)](this,_0x4e25d8);const _0x42b846=this[_0x41a4a2(0x24a)](_0x4e25d8);if(this[_0x41a4a2(0x237)](_0x42b846)){const _0x1343c2=this[_0x41a4a2(0x289)](_0x4e25d8),_0x5a9c2d=$dataSystem['optDisplayTp']?0x4:0x3,_0x9217de=_0x5a9c2d*0x80+(_0x5a9c2d-0x1)*0x8+0x4;let _0xd7e9be=_0x1343c2['x']+this['padding'];VisuMZ[_0x41a4a2(0x24e)][_0x41a4a2(0xe9)][_0x41a4a2(0x23a)]['ShowFacesListStyle']?_0xd7e9be=_0x1343c2['x']+ImageManager[_0x41a4a2(0x11b)]+0x8:_0xd7e9be+=ImageManager[_0x41a4a2(0x1c4)];const _0x55fe24=Math['round'](Math[_0x41a4a2(0x242)](_0x1343c2['x']+_0x1343c2[_0x41a4a2(0x15a)]-_0x9217de,_0xd7e9be));let _0x132b93=_0x55fe24+0x88,_0x4f9d99=_0x1343c2['y'];_0x132b93+=0x88*($dataSystem[_0x41a4a2(0x22e)]?0x3:0x2),_0x132b93+=this['getOffsetX_BTB'](),_0x4f9d99+=this[_0x41a4a2(0x218)]();const _0x29f13c=this['getAlignmentBTB']();if(_0x132b93>_0x1343c2['x']+_0x1343c2['width'])return;this['drawActorBravePoints'](_0x42b846,_0x132b93,_0x4f9d99,_0x1343c2[_0x41a4a2(0x15a)],_0x29f13c);}},VisuMZ[_0xfb7938(0x2ce)][_0xfb7938(0x11f)]=Window_BattleStatus[_0xfb7938(0xa8)][_0xfb7938(0x111)],Window_BattleStatus[_0xfb7938(0xa8)][_0xfb7938(0x111)]=function(_0x369105){const _0x1f223d=_0xfb7938;VisuMZ[_0x1f223d(0x2ce)]['Window_BattleStatus_drawItemStatusXPStyle'][_0x1f223d(0x1f4)](this,_0x369105);const _0x6204e5=this[_0x1f223d(0x24a)](_0x369105);if(this[_0x1f223d(0x237)](_0x6204e5)){const _0x5694ba=this['itemRectPortraitBTB'](_0x369105);let _0x447418=_0x5694ba['x'],_0x338f0f=_0x5694ba['y'];_0x447418+=this[_0x1f223d(0x164)](),_0x338f0f+=this[_0x1f223d(0x218)]();const _0x34e6a7=this[_0x1f223d(0x231)]();this['drawActorBravePoints'](_0x6204e5,_0x447418,_0x338f0f,_0x5694ba[_0x1f223d(0x15a)],_0x34e6a7);}},Window_BattleStatus['prototype'][_0xfb7938(0x18e)]=function(_0x5285e3){const _0x2684a6=_0xfb7938,_0x4e7582=this[_0x2684a6(0x205)](_0x5285e3);if(_0x4e7582['width']<ImageManager[_0x2684a6(0x11b)])return _0x4e7582;let _0x2afd30=Math[_0x2684a6(0x141)]((_0x4e7582[_0x2684a6(0x15a)]-ImageManager['faceWidth'])/0x2);return _0x4e7582[_0x2684a6(0x15a)]=ImageManager[_0x2684a6(0x11b)],_0x4e7582['x']+=_0x2afd30,_0x4e7582;},Window_BattleStatus['prototype'][_0xfb7938(0x231)]=function(){const _0x44e36b=_0xfb7938,_0x4f1ec2=VisuMZ[_0x44e36b(0x2ce)][_0x44e36b(0xe9)][_0x44e36b(0x163)],_0x3e0814=this[_0x44e36b(0x18d)]();return _0x4f1ec2['%1_align'['format'](_0x3e0814)]||0x0;},Window_BattleStatus[_0xfb7938(0xa8)]['getOffsetX_BTB']=function(){const _0x5d76eb=_0xfb7938,_0x289123=VisuMZ[_0x5d76eb(0x2ce)][_0x5d76eb(0xe9)][_0x5d76eb(0x163)],_0x107ef1=this[_0x5d76eb(0x18d)]();return _0x289123[_0x5d76eb(0x233)['format'](_0x107ef1)]||0x0;},Window_BattleStatus[_0xfb7938(0xa8)][_0xfb7938(0x218)]=function(){const _0x51a869=_0xfb7938,_0x207565=VisuMZ[_0x51a869(0x2ce)][_0x51a869(0xe9)]['Window'],_0x5605df=this[_0x51a869(0x18d)]();return _0x207565[_0x51a869(0x2de)[_0x51a869(0x20c)](_0x5605df)]||0x0;},Window_BattleSkill[_0xfb7938(0xa8)][_0xfb7938(0x1f0)]=function(){const _0x1f44fa=_0xfb7938;return BattleManager[_0x1f44fa(0x1bf)]();},Window_BattleSkill[_0xfb7938(0xa8)][_0xfb7938(0x21e)]=function(){const _0x334dec=_0xfb7938,_0x2d6577=this[_0x334dec(0xd6)](),_0x43d527=BattleManager[_0x334dec(0x18a)]();if(_0x43d527)_0x43d527['setSkill'](_0x2d6577?_0x2d6577['id']:null);Window_SkillList[_0x334dec(0xa8)][_0x334dec(0x21e)][_0x334dec(0x1f4)](this);},Window_BattleItem[_0xfb7938(0xa8)]['isBattleItemWindowBTB']=function(){const _0x134d9d=_0xfb7938;return BattleManager[_0x134d9d(0x1bf)]();},Window_BattleItem['prototype'][_0xfb7938(0x21e)]=function(){const _0x2eb3fa=_0xfb7938,_0x40695c=this[_0x2eb3fa(0xd6)](),_0x33c959=BattleManager['inputtingAction']();if(_0x33c959)_0x33c959[_0x2eb3fa(0x2df)](_0x40695c?_0x40695c['id']:null);Window_ItemList['prototype'][_0x2eb3fa(0x21e)]['call'](this);};function Window_BTB_TurnOrder(){const _0x38d9f8=_0xfb7938;this[_0x38d9f8(0x165)](...arguments);}Window_BTB_TurnOrder[_0xfb7938(0xa8)]=Object['create'](Window_Base[_0xfb7938(0xa8)]),Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x264)]=Window_BTB_TurnOrder,Window_BTB_TurnOrder[_0xfb7938(0xe9)]=VisuMZ[_0xfb7938(0x2ce)]['Settings'][_0xfb7938(0x191)],Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x165)]=function(){const _0xdd0d82=_0xfb7938,_0x347dde=this[_0xdd0d82(0x1f5)]();this[_0xdd0d82(0x29f)](_0x347dde),Window_Base[_0xdd0d82(0xa8)][_0xdd0d82(0x165)]['call'](this,_0x347dde),this[_0xdd0d82(0x2c4)](),this['updateVisibility'](),this[_0xdd0d82(0xe3)]=0x0;},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x1f5)]=function(){const _0x148f2c=_0xfb7938;return this[_0x148f2c(0x280)]($gameParty[_0x148f2c(0x262)](),0x9,!![]);},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x29f)]=function(_0x12d52a){const _0xd53c67=_0xfb7938;this[_0xd53c67(0x1b5)]=this[_0xd53c67(0x147)]=_0x12d52a['x'],this['_targetHomeY']=this[_0xd53c67(0xa6)]=_0x12d52a['y'],this['_fullWidth']=_0x12d52a[_0xd53c67(0x15a)],this[_0xd53c67(0x226)]=_0x12d52a[_0xd53c67(0x203)],this[_0xd53c67(0x25c)]=0x0;},Window_BTB_TurnOrder['prototype'][_0xfb7938(0x280)]=function(_0x359eee,_0xfcbfd4,_0x554843){const _0x4df438=_0xfb7938,_0x2511e6=Window_BTB_TurnOrder[_0x4df438(0xe9)],_0x4125b9=this[_0x4df438(0xff)]()?_0x2511e6[_0x4df438(0x2c7)]:_0x2511e6['MaxVertSprites'],_0x3e495b=Math['min'](_0x4125b9,_0x359eee+_0xfcbfd4),_0x38c625=SceneManager['_scene'][_0x4df438(0x142)]['height'],_0x216cb8=SceneManager[_0x4df438(0x294)][_0x4df438(0x19d)][_0x4df438(0x203)],_0x5de514=_0x2511e6['SubjectDistance'],_0x28463e=Graphics[_0x4df438(0x203)]-_0x38c625-_0x216cb8;let _0x12b071=0x0,_0x6af5d5=0x0,_0x4e3ae6=0x0,_0x9ed947=0x0;switch(_0x2511e6[_0x4df438(0x25a)]){case _0x4df438(0x26d):_0x12b071=_0x2511e6[_0x4df438(0xe0)]*_0x3e495b+_0x5de514,_0x6af5d5=_0x2511e6[_0x4df438(0x1e0)],_0x4e3ae6=Math[_0x4df438(0xce)]((Graphics[_0x4df438(0x15a)]-_0x12b071)/0x2),_0x9ed947=_0x2511e6[_0x4df438(0x235)];break;case _0x4df438(0x154):_0x12b071=_0x2511e6['SpriteThin']*_0x3e495b+_0x5de514,_0x6af5d5=_0x2511e6[_0x4df438(0x1e0)],_0x4e3ae6=Math['ceil']((Graphics[_0x4df438(0x15a)]-_0x12b071)/0x2),_0x9ed947=Graphics[_0x4df438(0x203)]-_0x38c625-_0x6af5d5-_0x2511e6[_0x4df438(0x235)];break;case _0x4df438(0x193):_0x12b071=_0x2511e6[_0x4df438(0x1e0)],_0x6af5d5=_0x2511e6[_0x4df438(0xe0)]*_0x3e495b+_0x5de514,_0x4e3ae6=_0x2511e6[_0x4df438(0x235)],_0x9ed947=Math[_0x4df438(0xce)]((_0x28463e-_0x6af5d5)/0x2),_0x9ed947+=_0x216cb8;break;case'right':_0x12b071=_0x2511e6[_0x4df438(0x1e0)],_0x6af5d5=_0x2511e6[_0x4df438(0xe0)]*_0x3e495b+_0x5de514,_0x4e3ae6=Graphics[_0x4df438(0x15a)]-_0x12b071-_0x2511e6['ScreenBuffer'],_0x9ed947=Math[_0x4df438(0xce)]((_0x28463e-_0x6af5d5)/0x2),_0x9ed947+=_0x216cb8;break;}if(!_0x554843){const _0x4063e0=Window_BTB_TurnOrder['Settings'][_0x4df438(0x261)];let _0x1bf273=Math[_0x4df438(0x242)](_0x4125b9,Math['min']($gameParty[_0x4df438(0x262)]()+0x8)-_0x3e495b);switch(_0x2511e6[_0x4df438(0x25a)]){case _0x4df438(0x26d):case _0x4df438(0x154):_0x4063e0&&(_0x4e3ae6-=_0x1bf273*_0x2511e6['SpriteThin']);break;}}return _0x4e3ae6+=_0x2511e6['DisplayOffsetX'],_0x9ed947+=_0x2511e6[_0x4df438(0x103)],new Rectangle(_0x4e3ae6,_0x9ed947,_0x12b071,_0x6af5d5);},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0xe6)]=function(){this['padding']=0x0;},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0xff)]=function(){const _0x3571c6=_0xfb7938,_0x4f329f=Window_BTB_TurnOrder['Settings'],_0x5cdf46=['top',_0x3571c6(0x154)][_0x3571c6(0x2ca)](_0x4f329f[_0x3571c6(0x25a)]);return _0x5cdf46;},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x2c4)]=function(){const _0x85fe66=_0xfb7938;this[_0x85fe66(0x1cd)]=new Sprite(),this['addInnerChild'](this['_turnOrderInnerSprite']),this[_0x85fe66(0x105)]=[];for(let _0xe4b4f5=0x0;_0xe4b4f5<$gameParty[_0x85fe66(0x262)]();_0xe4b4f5++){const _0x22d6e2=new Sprite_BTB_TurnOrder_Battler($gameParty,_0xe4b4f5);this[_0x85fe66(0x1cd)][_0x85fe66(0x170)](_0x22d6e2),this['_turnOrderContainer'][_0x85fe66(0xca)](_0x22d6e2);}for(let _0x480911=0x0;_0x480911<0x8;_0x480911++){const _0x2d1aeb=new Sprite_BTB_TurnOrder_Battler($gameTroop,_0x480911);this[_0x85fe66(0x1cd)]['addChild'](_0x2d1aeb),this[_0x85fe66(0x105)]['push'](_0x2d1aeb);}},Window_BTB_TurnOrder[_0xfb7938(0xa8)]['update']=function(){const _0x348f50=_0xfb7938;Window_Base[_0x348f50(0xa8)][_0x348f50(0x282)][_0x348f50(0x1f4)](this),this[_0x348f50(0xd1)](),this[_0x348f50(0xc8)](),this['updateSidePosition'](),this[_0x348f50(0x146)](),this['updateVisibility']();},Window_BTB_TurnOrder[_0xfb7938(0xa8)]['updateHomePosition']=function(){const _0x33c4ae=_0xfb7938;if(this[_0x33c4ae(0x25c)]>0x0){const _0x356197=this[_0x33c4ae(0x25c)];this['_homeX']=(this[_0x33c4ae(0x147)]*(_0x356197-0x1)+this['_targetHomeX'])/_0x356197,this['_homeY']=(this[_0x33c4ae(0xa6)]*(_0x356197-0x1)+this[_0x33c4ae(0x2b6)])/_0x356197,this[_0x33c4ae(0x25c)]--,this[_0x33c4ae(0x25c)]<=0x0&&(this[_0x33c4ae(0x147)]=this[_0x33c4ae(0x1b5)],this['_homeY']=this[_0x33c4ae(0x2b6)]);}},Window_BTB_TurnOrder['prototype']['updatePosition']=function(){const _0x519e74=_0xfb7938,_0x3e6edf=Window_BTB_TurnOrder[_0x519e74(0xe9)];if(_0x3e6edf[_0x519e74(0x25a)]!==_0x519e74(0x26d))return;if(!_0x3e6edf[_0x519e74(0x270)])return;const _0x2959d9=SceneManager[_0x519e74(0x294)][_0x519e74(0x19d)];if(!_0x2959d9)return;_0x2959d9[_0x519e74(0x275)]?(this['x']=this[_0x519e74(0x147)]+(_0x3e6edf[_0x519e74(0x112)]||0x0),this['y']=this['_homeY']+(_0x3e6edf['RepositionTopHelpY']||0x0)):(this['x']=this[_0x519e74(0x147)],this['y']=this[_0x519e74(0xa6)]);const _0x221c02=SceneManager[_0x519e74(0x294)][_0x519e74(0x266)];this['_ogWindowLayerX']===undefined&&(this['_ogWindowLayerX']=Math[_0x519e74(0x141)]((Graphics[_0x519e74(0x15a)]-Math[_0x519e74(0x242)](Graphics[_0x519e74(0x15d)],_0x221c02[_0x519e74(0x15a)]))/0x2),this[_0x519e74(0xea)]=Math['round']((Graphics['height']-Math['min'](Graphics[_0x519e74(0xae)],_0x221c02[_0x519e74(0x203)]))/0x2)),this['x']+=_0x221c02['x']-this[_0x519e74(0x2aa)],this['y']+=_0x221c02['y']-this[_0x519e74(0xea)];},Window_BTB_TurnOrder['prototype']['updateSidePosition']=function(){const _0x3d70cd=_0xfb7938,_0x1ed226=Window_BTB_TurnOrder[_0x3d70cd(0xe9)];if([_0x3d70cd(0x26d)][_0x3d70cd(0x2ca)](_0x1ed226[_0x3d70cd(0x25a)]))return;this['x']=this[_0x3d70cd(0x147)],this['y']=this[_0x3d70cd(0xa6)];const _0x30711d=SceneManager[_0x3d70cd(0x294)][_0x3d70cd(0x266)];this['x']+=_0x30711d['x'],this['y']+=_0x30711d['y'];},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x146)]=function(){const _0xcdbcbc=_0xfb7938;if(!this[_0xcdbcbc(0x1cd)])return;const _0x14e51b=this[_0xcdbcbc(0x1cd)]['children'];if(!_0x14e51b)return;_0x14e51b['sort'](this[_0xcdbcbc(0x1a8)][_0xcdbcbc(0x192)](this));},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x1a8)]=function(_0x907f27,_0x21cf55){const _0x4a08a2=_0xfb7938,_0x24d201=this['isHorz'](),_0x14c01d=Window_BTB_TurnOrder[_0x4a08a2(0xe9)]['OrderDirection'];if(_0x24d201&&!_0x14c01d)return _0x907f27['x']-_0x21cf55['x'];else{if(_0x24d201&&_0x14c01d)return _0x21cf55['x']-_0x907f27['x'];else{if(!_0x24d201&&_0x14c01d)return _0x907f27['y']-_0x21cf55['y'];else{if(!_0x24d201&&!_0x14c01d)return _0x21cf55['y']-_0x907f27['y'];}}}},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x9e)]=function(){this['visible']=$gameSystem['isBattleSystemBTBTurnOrderVisible']();},Window_BTB_TurnOrder['prototype'][_0xfb7938(0x16a)]=function(_0x595d0d){const _0x2e8e0a=_0xfb7938;this[_0x2e8e0a(0x105)][_0x2e8e0a(0x293)]((_0x24db69,_0x3c2120)=>{const _0x56a1f6=_0x2e8e0a;return _0x24db69['containerPosition']()-_0x3c2120[_0x56a1f6(0x229)]();}),this[_0x2e8e0a(0x122)]();if(!_0x595d0d)return;for(const _0x2dc537 of this[_0x2e8e0a(0x105)]){if(!_0x2dc537)continue;_0x2dc537[_0x2e8e0a(0x282)](),_0x2dc537[_0x2e8e0a(0x2b5)]=0x0;}},Window_BTB_TurnOrder[_0xfb7938(0xa8)][_0xfb7938(0x122)]=function(){const _0x5beabe=_0xfb7938;if(!this['isHorz']())return;const _0x1129de=VisuMZ[_0x5beabe(0x2ce)]['Settings'][_0x5beabe(0x191)];if(!_0x1129de[_0x5beabe(0x1ca)])return;const _0x32e50e=$gameParty[_0x5beabe(0xf2)]()[_0x5beabe(0x27c)](_0x3e32ac=>_0x3e32ac&&_0x3e32ac['isAlive']()&&_0x3e32ac['isAppeared']())[_0x5beabe(0x279)],_0x30bd12=$gameTroop[_0x5beabe(0xf2)]()[_0x5beabe(0x27c)](_0x300e73=>_0x300e73&&_0x300e73[_0x5beabe(0x1a3)]()&&_0x300e73[_0x5beabe(0x133)]())[_0x5beabe(0x279)],_0x4820e0=this['createBattlerRect'](_0x32e50e,_0x30bd12);this[_0x5beabe(0x1b5)]=_0x4820e0['x'],this[_0x5beabe(0x2b6)]=_0x4820e0['y'],(this[_0x5beabe(0x1b5)]!==this[_0x5beabe(0x147)]||this[_0x5beabe(0x2b6)]!==this[_0x5beabe(0xa6)])&&(this[_0x5beabe(0x25c)]=_0x1129de[_0x5beabe(0xc2)]);};