//=============================================================================
// VisuStella MZ - Aggro Control System
// VisuMZ_2_AggroControlSystem.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_2_AggroControlSystem = true;

var VisuMZ = VisuMZ || {};
VisuMZ.AggroControlSystem = VisuMZ.AggroControlSystem || {};
VisuMZ.AggroControlSystem.version = 1.07;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 2] [Version 1.07] [AggroControlSystem]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Aggro_Control_System_VisuStella_MZ
 * @orderAfter VisuMZ_1_BattleCore
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * A common mechanic found in many RPG's nowadays is the ability to steer the
 * way enemies target party members. This can be in the form of provocations, 
 * taunts, and aggro.
 *
 * Provocations come in the form of states, where when a unit applies a provoke
 * state on a target, the target must attack the provoker when using single
 * target skills. This plugin provides support for multiple provocations and
 * such provocations will be given focus based on the state's priority value.
 *
 * Taunts are a third way to steer an opponent to focus on a party member. The
 * taunt effects can be split up into global, physical, magical, or certain hit
 * only taunts and these can be applied to almost any trait object.
 *
 * Aggro is a numeric value that determines the likelihood and/or priority
 * level of how often a target party member is to be attacked by an enemy unit.
 * The higher the aggro value, the more likely the chances of being targeted.
 * A option can be turned on (or through notetags) to set enemies to always
 * target the party member with the highest aggro.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Three different ways to influencing which targets enemies should attack:
 *   Provoke, taunt, and aggro.
 * * Provoke and taunt effects work both ways for actors and enemies.
 * * Aggro effects accumulate through battle and can be manipulated through
 *   notetag values, Plugin Commands, and/or Plugin Parameters.
 * * Provoked battlers can have provoke lines displayed to indicate which
 *   unit has provoked them.
 * * Taunting units can have animations played on them repeatedly to quickly
 *   relay information to the player about their taunt properties.
 * * Gauges that can be displayed over the heads of actor sprites to display
 *   how much aggro that actor holds in comparison to the other actors.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 2 ------
 *
 * This plugin is a Tier 2 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Extra Features
 * ============================================================================
 *
 * There are some extra features found if other VisuStella MZ plugins are found
 * present in the Plugin Manager list.
 *
 * ---
 *
 * VisuMZ_0_CoreEngine
 * VisuMZ_1_BattleCore
 *
 * - Provoke Priority Lines and Taunt animations become available if these
 *   plugins are installed.
 *
 * ---
 *
 * ============================================================================
 * How Aggro, Provoke, and Taunts Work
 * ============================================================================
 *
 * This section will explain how aggro, provoke, and taunts work.
 *
 * ---
 *
 * Provoke
 *
 * - Provocations come in the form of states, where when a unit applies a
 * provoke state on a target, the target must attack the provoker when using
 * single target skills. This plugin provides support for multiple provocations
 * and such provocations will be given focus based on the state's database
 * priority value.
 *
 * - The provoke will last only as long as the duration of the state itself. If
 * the state's duration is refreshed by reapplying the Provoke state, then the
 * provoker of that state will then switch over to the one applying the newly
 * added state.
 *
 * - When an actor selects a target for an action and the actor is provoked by
 * an enemy on the other team, the player's choice selection becomes limited to
 * only the provoker.
 *
 * - Provoke can be bypassed through the <Bypass Provoke> notetag.
 *
 * ---
 *
 * Taunts
 *
 * - Taunts are a third way to steer an opponent to focus on a party member.
 * The taunt effects can be split up into global, physical, magical, or certain
 * hit only taunts and these can be applied to almost any trait object.
 *
 * - When an actor selects a target and the enemy team has a taunting unit,
 * the player's choice selection becomes limited to only the targets with the
 * associated taunt type.
 *
 * - Taunts can be bypassed through the <Bypass Taunt> notetag.
 *
 * ---
 *
 * Aggro
 *
 * - Aggro is a numeric value that determines the likelihood and/or priority
 * level of how often a target party member is to be attacked by an enemy unit.
 * The higher the aggro value, the more likely the chances of being targeted.
 * A option can be turned on (or through notetags) to set enemies to always
 * target the party member with the highest aggro.
 *
 * - Skills and items can raise its user's aggro value through notetags and/or
 * how much damage they've dealt or healed. Skills and items can also change a
 * target's aggro value through notetags, too.
 *
 * - Through the Plugin Parameters, you can set Aggro to automatically raised
 * based on how much damage or healing dealt by a user.
 *
 * - Some enemies can be bypass forced aggro target through the <Bypass Aggro>
 * notetag while other enemies can be forced to target the highest aggro target
 * through the <Target Highest Aggro> notetag;
 *
 * ---
 *
 * Priorities
 *
 * - Priority will be given in the order of provokes, taunts, and then aggro.
 * This means if an enemy is provoked, the opposing side has a taunt, and there
 * is a member with high aggro, then the enemy will always attack the provoker
 * first before targeting a taunting unit before targeting the unit with high
 * aggro values.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * === Provoke-Related Notetags ===
 *
 * The following notetags enable you to utilize the Provoke effects added by
 * this plugin. Provoked targets can only attack the provoking unit for single
 * target actions.
 *
 * ---
 *
 * <Provoke>
 *
 * - Used for: State Notetags
 * - Causes the state affected unit to be able to only attack the caster of the
 *   provoke state for single target actions.
 * - If multiple provoke states are applied, then the provoker is the one who
 *   applied the highest priority provoke state.
 *
 * ---
 *
 * <Bypass Provoke>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Makes the affected unit to ignore any and all provoke effects applied by
 *   any provoke states, allowing them to target foes as if they are unaffected
 *   by provoke states altogether.
 *
 * ---
 * 
 * <Bypass Provoke>
 * - Used for: Skill and Item Notetags
 * - Makes the action bypass provoke effects applied by any provoke states,
 *   allowing this action to target foes as if the user is unaffected by any
 *   provoke effects altogether.
 * 
 * ---
 * 
 * <Provoke Height Origin: x%>
 * 
 * - Used for: Actor, Enemy Notetags
 * - Sets the provoke height origin point to x% of the sprite's height.
 * - This is the landing point for the provoke trails.
 * - Replace 'x' with a number presenting what rate of the sprite's height to
 *   set as the provoke height origin point.
 * 
 * ---
 *
 * === Taunt-Related Notetags ===
 *
 * ---
 *
 * <Taunt>
 * <All Taunt>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Causes the taunting unit to become the target of the opposing team's
 *   single target actions for physical, magical, and certain hit actions.
 * - If multiple taunters exist, then the opposing team can select between any
 *   of the taunters for targets.
 *
 * ---
 *
 * <Physical Taunt>
 * <Magical Taunt>
 * <Certain Taunt>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Causes the taunting unit to become the target of the opposing team's
 *   single target actions for physical, magical, and certain hit actions
 *   respectively.
 * - Add/remove any combination of the above to cause the affected unit to
 *   become the target of those types of actions.
 * - If multiple taunters exist, then the opposing team can select between any
 *   of the taunters for targets.
 *
 * ---
 *
 * <Bypass Taunt>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - The affected unit will ignore any and all taunt effects created by the
 *   opposing team, allowing them to use single target actions as if no
 *   taunters exist on the opposing team.
 *
 * ---
 * 
 * <Bypass Taunt>
 * - Used for: Skill and Item Notetags
 * - Makes the action bypass taunt effects created by the opposing team,
 *   allowing the user to use single target actions as if no taunters exist on
 *   the opposing team.
 * 
 * ---
 *
 * === Aggro-Related Notetags ===
 *
 * ---
 *
 * <User Aggro: +x>
 * <User Aggro: -x>
 *
 * - Used for: Skill, Item
 * - Upon using this action, raise the user's battle aggro value by 'x'.
 * - Replace 'x' with the amount of battle aggro to increase/decrease by.
 * - This effect will only apply once per usage regardless of the number of
 *   successful hits landed by the action.
 *
 * ---
 *
 * <Target Aggro: +x>
 * <Target Aggro: -x>
 *
 * - Used for: Skill, Item
 * - Upon using this action, raise the target's battle aggro value by 'x'.
 * - Replace 'x' with the amount of battle aggro to increase/decrease by.
 * - This effect will apply multiple times based on the number of successful
 *   hits landed by the action.
 *
 * ---
 *
 * <Aggro: +x>
 * <Aggro: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Causes the affected unit to passively have increased/decreased aggro
 *   values independent of the amount of aggro it earns in battle.
 * - Replace 'x' with the amount of aggro this object increases/decreases by.
 *
 * ---
 *
 * <Aggro Multiplier: x%>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Causes the affected unit to increase the amount of perceived aggro it has
 *   by the aggro multiplier.
 * - Replace 'x' with a number representing the percentage to increase/decrease
 *   the perceived aggro by.
 * - If multiple of these traits exist across different trait objects, the
 *   effects are increased multiplicatively.
 *
 * ---
 *
 * <Bypass Highest Aggro>
 *
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills or items, the action will decide targets by aggro weight
 *   instead of always picking the highest aggro unit(s).
 * - If used on trait objects, the affected unit will decide targets by aggro
 *   weight instead of always picking the highest aggro unit(s).
 * - This is used for enemy A.I. or Actor auto battle A.I.
 *
 * ---
 * 
 * <Bypass Highest Aggro>
 * - Used for: Skill and Item Notetags
 * - Makes the action bypass highest aggro effects and instead focuses on
 *   targets by aggro weight instead.
 * - This is used for enemy A.I. or Actor auto battle A.I.
 * 
 * ---
 *
 * <Target Highest Aggro>
 *
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills or items, the action will always decide its targets by
 *   the highest aggro value.
 * - If used on trait objects, the affected unit will always decide on targets
 *   by the highest aggro value.
 * - If the <Bypass Highest Aggro> notetag exists, this effect is ignored.
 * - This is used for enemy A.I. or Actor auto battle A.I.
 *
 * ---
 *
 * === JavaScript Notetags: Aggro-Related ===
 *
 * ---
 *
 * <JS User Aggro>
 *  code
 *  code
 *  value = code
 * </JS User Aggro>
 *
 * - Used for: Skill, Item
 * - Replace 'code' with JavaScript code to determine the final 'value' to
 *   change the user's battle aggro to upon using this skill.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 * - This effect will only apply once per usage regardless of the number of
 *   successful hits landed by the action.
 *
 * ---
 *
 * <JS Target Aggro>
 *  code
 *  code
 *  value = code
 * </JS Target Aggro>
 *
 * - Used for: Skill, Item
 * - Replace 'code' with JavaScript code to determine the final 'value' to
 *   change target's battle aggro to upon using this skill.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 * - This effect will apply multiple times based on the number of successful
 *   hits landed by the action.
 *
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Actor Plugin Commands ===
 * 
 * ---
 *
 * Actor: Change Aggro
 * - Changes target actor's aggro value.
 *
 *   Actor ID:
 *   - Select which Actor ID to affect.
 *
 *   Change Aggro By:
 *   - Change aggro by this amount.
 *   - Use negative numbers to reduce aggro.
 *
 * ---
 *
 * Actor: Set Aggro
 * - Set target actor's aggro value.
 *
 *   Actor ID:
 *   - Select which Actor ID to affect.
 *
 *   Set Aggro To:
 *   - Sets target's aggro to this amount.
 *   - Aggro must be at least a value of 1.
 *
 * ---
 * 
 * === Enemy Plugin Commands ===
 * 
 * ---
 *
 * Enemy: Change Aggro
 * - Changes target enemy's aggro value.
 *
 *   Enemy Index:
 *   - Select which Enemy Index to affect.
 *
 *   Change Aggro By:
 *   - Change aggro by this amount.
 *   - Use negative numbers to reduce aggro.
 *
 * ---
 *
 * Enemy: Set Aggro
 * - Set target enemy's aggro value.
 *
 *   Enemy Index:
 *   - Select which Enemy Index to affect.
 *
 *   Set Aggro To:
 *   - Sets target's aggro to this amount.
 *   - Aggro must be at least a value of 1.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Provoke Settings
 * ============================================================================
 *
 * The Provoke Settings Plugin Parameters adjust the visual aspects related to
 * the provoke effect. These settings will require VisuMZ_1_BattleCore to be
 * installed in order for them to work due to dependencies. 
 *
 * ---
 *
 * VisuMZ_1_BattleCore
 * 
 *   Show Priority Lines?:
 *   - Show priority target lines for this plugin?
 *   - Requires VisuMZ_1_BattleCore.
 *
 * ---
 *
 * Line Settings
 * 
 *   Arc Height:
 *   - How tall should the line arc in pixels?
 * 
 *   Blend Mode:
 *   - The blend mode used for the sprite.
 * 
 *   Height Origin:
 *   - The rate from the battler's sprite base to determine where the line
 *     starts from.
 * 
 *   Line Color:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Opacity:
 *   - The highest possible opacity for active provoke lines.
 * 
 *   Opacity Speed:
 *   - The speed at which opacity fluctuates for the line sprite.
 * 
 *   Parts:
 *   - The number of joint parts to split up the sprite as.
 * 
 *   Parts Size:
 *   - The number in pixels for the diameter of each part.
 *
 * ---
 * 
 * Options
 * 
 *   Add Provoke Option?
 *   - Add the 'Show Provoke Origin' option to the Options menu?
 * 
 *   Adjust Window Height
 *   - Automatically adjust the options window height?
 * 
 *   Option Name
 *   - Command name of the option.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Taunt Settings
 * ============================================================================
 *
 * Battlers with specific taunt types can have animations playing on them over
 * and over to relay information to the player. These settings require you to
 * have both VisuMZ_0_CoreEngine and VisuMZ_1_BattleCore installed in your
 * project's plugin list in order to use.
 *
 * ---
 *
 * VisuMZ_0_CoreEngine & VisuMZ_1_BattleCore
 * 
 *   Show Animations?:
 *   - Show animations for each of the taunt effects?
 *   - Requires VisuMZ_0_CoreEngine and VisuMZ_1_BattleCore.
 *
 * ---
 *
 * Animation ID's
 * 
 *   Physical Taunt:
 *   - The animation ID used for physical taunts.
 *   - Use 0 or 'None' to bypass this type.
 * 
 *   Magical Taunt:
 *   - The animation ID used for magical taunts.
 *   - Use 0 or 'None' to bypass this type.
 * 
 *   Certain Hit Taunt:
 *   - The animation ID used for certain hit taunts.
 *   - Use 0 or 'None' to bypass this type.
 *
 * ---
 *
 * Animation Settings
 * 
 *   Cycle Time:
 *   - The amount of frames to wait before each animation cycle.
 *   - WARNING: Lower numbers can jeopardize game performance.
 * 
 *   Mirror Actor Ani?:
 *   - Mirror animations played on actors?
 * 
 *   Mute Animation SFX?:
 *   - Mute sounds played by animations?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Aggro Settings
 * ============================================================================
 *
 * This lets you adjust the settings for this plugin's Aggro mechanics. Most of
 * these settings focus on the visual gauge display of the Aggro gauge, but you
 * can also change up the settings for how aggro is utilized.
 *
 * ---
 *
 * General
 * 
 *   Priority: Highest TGR:
 *   - When enemies target actors for an single target attack, always target
 *     the highest members or make it weighted?
 *
 *   Aggro Per Damage:
 *   - The amount of aggro generated per point of HP damage dealt to an enemy.
 *
 *   Aggro Per Heal:
 *   - The amount of aggro generated per point of HP recovered to an ally.
 *
 * ---
 *
 * Gauge
 * 
 *   Visible Battler Gauge:
 *   - Display an aggro gauge over an SV actor's head to show current aggro
 *     level compared to other party members.
 * 
 *   Visible Status Gauge:
 *   - Display an aggro gauge in the Battle Status Window to show the current
 *     aggro level compared to others.
 * 
 *   Gauge Color 1:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Gauge Color 2:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Gauge Width:
 *   - Width in pixels you want the gauge to be.
 * 
 *   Anchor X:
 *   Anchor Y:
 *   - Where do you want the Aggro Gauge sprite's anchor X/Y to be?
 *   - Use values between 0 and 1 to be safe.
 * 
 *   Scale:
 *   - How large/small do you want the Aggro Gauge to be scaled?
 * 
 *   Battler Gauge
 * 
 *     Offset X:
 *     Offset Y:
 *     - How many pixels to offset the Aggro Gauge's X/Y by?
 * 
 *   Battle Status Gauge
 * 
 *     Offset X:
 *     Offset Y:
 *     - How many pixels to offset the Aggro Gauge's X/Y by?
 *
 * ---
 * 
 * Options
 * 
 *   Add Provoke Option?
 *   - Add the 'Show Aggro Gauge' option to the Options menu?
 * 
 *   Adjust Window Height
 *   - Automatically adjust the options window height?
 * 
 *   Option Name
 *   - Command name of the option.
 * 
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 * 
 * 7. If this VisuStella MZ plugin is a paid product, all project team members
 * must purchase their own individual copies of the paid product if they are to
 * use it. Usage includes working on related game mechanics, managing related
 * code, and/or using related Plugin Commands and features. Redistribution of
 * the plugin and/or its code to other members of the team is NOT allowed
 * unless they own the plugin itself as that conflicts with Article 4.
 * 
 * 8. Any extensions and/or addendums made to this plugin's Terms of Use can be
 * found on VisuStella.com and must be followed.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.07: April 9, 2021
 * * Bug Fixes!
 * ** Provoke effect now takes into consideration when Provoke is applied by
 *    a weapon effect that comes off a counter attack from an actor. Fix made
 *    by Olivia.
 * 
 * Version 1.06: March 12, 2021
 * * Bug Fixes!
 * ** Subsequent battles or changing scenes should no longer clear the custom
 *    rendered bitmap used for the provoke trail. Fix made by Irina.
 * * Documentation Update!
 * ** Added documentation for the Skill and Item versions of the following
 *    notetags into the help file and wiki:
 * *** <Bypass Provoke>
 * *** <Bypass Taunt>
 * *** <Bypass Highest Aggro>
 * 
 * Version 1.05: March 5, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features!
 * ** New Plugin Parameters added by Irina:
 * *** Plugin Parameters > Aggro Settings > Battle Status Gauge
 * **** These settings allow you to offset the Aggro Gauge in the Battle Status
 *      Window from its original position.
 * 
 * Version 1.04: February 26, 2021
 * * Bug Fixes!
 * ** Fixed positioning of gauge for List Style battle layouts without faces.
 *    Fix made by Olivia.
 * 
 * Version 1.03: February 5, 2021
 * * Feature Update!
 * ** Aggro is now cleared at the end of each battle in addition to the start
 *    of each battle. Update made by Olivia.
 *
 * Version 1.02: November 1, 2020
 * * Compatibility Update!
 * ** Plugin is made more compatible with other plugins.
 * 
 * Version 1.01: October 4, 2020
 * * Bug Fixes!
 * ** Provoke lines should now be placed correctly if the UI area is smaller
 *    than the resolution area.
 * ** The Plugin Commands should no longer cause crashes. Fix made by Irina.
 *
 * Version 1.00: September 28, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActorChangeAggro
 * @text Actor: Change Aggro
 * @desc Changes target actor's aggro value.
 *
 * @arg ActorID:num
 * @text Actor ID
 * @type actor
 * @desc Select which Actor ID to affect.
 * @default 1
 *
 * @arg Aggro:num
 * @text Change Aggro By
 * @desc Change aggro by this amount.
 * Use negative numbers to reduce aggro.
 * @default +1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActorSetAggro
 * @text Actor: Set Aggro
 * @desc Set target actor's aggro value.
 *
 * @arg ActorID:num
 * @text Actor ID
 * @type actor
 * @desc Select which Actor ID to affect.
 * @default 1
 *
 * @arg Aggro:num
 * @text Set Aggro To
 * @desc Sets target's aggro to this amount.
 * Aggro must be at least a value of 1.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EnemyChangeAggro
 * @text Enemy: Change Aggro
 * @desc Changes target enemy's aggro value.
 *
 * @arg EnemyIndex:num
 * @text Enemy Index
 * @type actor
 * @desc Select which Enemy Index to affect.
 * @default 0
 *
 * @arg Aggro:num
 * @text Change Aggro By
 * @desc Change aggro by this amount.
 * Use negative numbers to reduce aggro.
 * @default +1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EnemySetAggro
 * @text Enemy: Set Aggro
 * @desc Set target enemy's aggro value.
 *
 * @arg EnemyIndex:num
 * @text Enemy Index
 * @type actor
 * @desc Select which Enemy Index to affect.
 * @default 0
 *
 * @arg Aggro:num
 * @text Set Aggro To
 * @desc Sets target's aggro to this amount.
 * Aggro must be at least a value of 1.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param AggroControl
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Provoke:struct
 * @text Provoke Settings
 * @type struct<Provoke>
 * @desc Settings related to the Provoke mechanic.
 * These settings require VisuMZ_1_BattleCore.
 * @default {"VisuMZ_1_BattleCore":"","ShowLines:eval":"true","LineSettings":"","ArcHeight:num":"125","BlendMode:num":"1","HeightOrigin:num":"0.8","LineColor:str":"#ff0000","Opacity:num":"255","OpacitySpeed:num":"4","Parts:num":"256","PartsSize:num":"5","Options":"","AddOption:eval":"true","AdjustOptionsRect:eval":"true","OptionName:str":"Show Provoke Origin"}
 *
 * @param Taunt:struct
 * @text Taunt Settings
 * @type struct<Taunt>
 * @desc Settings related to the Taunt mechanic.
 * @default {"Dependency":"VisuMZ_1_BattleCore","ShowAnimation:eval":"true","AnimationID":"","AniPhysical:num":"1","AniMagical:num":"2","AniCertain:num":"3","AnimationSettings":"","CycleTime:num":"60","MirrorActorAni:eval":"true","MuteAnimations:eval":"true"}
 *
 * @param Aggro:struct
 * @text Aggro Settings
 * @type struct<Aggro>
 * @desc Settings related to the Aggro mechanic.
 * @default {"General":"","PriorityHighest:eval":"true","AggroPerDmg:num":"0.1","AggroPerHeal:num":"0.5","Gauge":"","VisibleGauge:eval":"false","StatusGauge:eval":"true","GaugeColor1:str":"#959595","GaugeColor2:str":"#ffffff","AnchorX:num":"0.5","AnchorY:num":"1.0","Scale:num":"0.5","OffsetX:num":"+0","OffsetY:num":"+2","Options":"","AddOption:eval":"true","AdjustOptionsRect:eval":"true","OptionName:str":"Show Aggro Gauge"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Provoke Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Provoke:
 *
 * @param VisuMZ_1_BattleCore
 *
 * @param ShowLines:eval
 * @text Show Priority Lines?
 * @parent VisuMZ_1_BattleCore
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show priority target lines for this plugin?
 * Requires VisuMZ_1_BattleCore.
 * @default true
 *
 * @param LineSettings
 * @text Line Settings
 *
 * @param ArcHeight:num
 * @text Arc Height
 * @parent LineSettings
 * @type number
 * @desc How tall should the line arc in pixels?
 * @default 125
 *
 * @param BlendMode:num
 * @text Blend Mode
 * @parent LineSettings
 * @type select
 * @option Normal
 * @value 0
 * @option Additive
 * @value 1
 * @option Multiply
 * @value 2
 * @option Screen
 * @value 3
 * @desc The blend mode used for the sprite.
 * @default 1
 *
 * @param HeightOrigin:num
 * @text Height Origin
 * @parent LineSettings
 * @desc The rate from the battler's sprite base to determine where the line starts from.
 * @default 0.8
 *
 * @param LineColor:str
 * @text Line Color
 * @parent LineSettings
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default #ff0000
 *
 * @param Opacity:num
 * @text Opacity
 * @parent LineSettings
 * @type number
 * @min 1
 * @max 255
 * @desc The highest possible opacity for active provoke lines.
 * @default 255
 *
 * @param OpacitySpeed:num
 * @text Opacity Speed
 * @parent Opacity:num
 * @type number
 * @min 1
 * @desc The speed at which opacity fluctuates for the line sprite.
 * @default 4
 *
 * @param Parts:num
 * @text Parts
 * @parent LineSettings
 * @type number
 * @min 1
 * @desc The number of joint parts to split up the sprite as.
 * @default 256
 *
 * @param PartsSize:num
 * @text Parts Size
 * @parent Parts:num
 * @type number
 * @min 1
 * @desc The number in pixels for the diameter of each part.
 * @default 5
 *
 * @param Options
 * @text Options
 *
 * @param AddOption:eval
 * @text Add Provoke Option?
 * @parent Options
 * @type boolean
 * @on Add
 * @off Don't Add
 * @desc Add the 'Show Provoke Origin' option to the Options menu?
 * @default true
 *
 * @param AdjustOptionsRect:eval
 * @text Adjust Window Height
 * @parent Options
 * @type boolean
 * @on Adjust
 * @off Don't
 * @desc Automatically adjust the options window height?
 * @default true
 *
 * @param OptionName:str
 * @text Option Name
 * @parent Options
 * @desc Command name of the option.
 * @default Show Provoke Origin
 *
 */
/* ----------------------------------------------------------------------------
 * Taunt Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Taunt:
 *
 * @param Dependency
 * @text VisuMZ_0_CoreEngine
 * @default VisuMZ_1_BattleCore
 *
 * @param ShowAnimation:eval
 * @text Show Animations?
 * @parent Dependency
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show animations for each of the taunt effects?
 * Requires VisuMZ_0_CoreEngine and VisuMZ_1_BattleCore.
 * @default true
 *
 * @param AnimationID
 * @text Animation ID's
 *
 * @param AniPhysical:num
 * @text Physical Taunt
 * @parent AnimationID
 * @type animation
 * @desc The animation ID used for physical taunts.
 * Use 0 or 'None' to bypass this type.
 * @default 13
 *
 * @param AniMagical:num
 * @text Magical Taunt
 * @parent AnimationID
 * @type animation
 * @desc The animation ID used for magical taunts.
 * Use 0 or 'None' to bypass this type.
 * @default 14
 *
 * @param AniCertain:num
 * @text Certain Hit Taunt
 * @parent AnimationID
 * @type animation
 * @desc The animation ID used for certain hit taunts.
 * Use 0 or 'None' to bypass this type.
 * @default 15
 *
 * @param AnimationSettings
 * @text Animation Settings
 *
 * @param CycleTime:num
 * @text Cycle Time
 * @parent AnimationSettings
 * @type number
 * @min 1
 * @desc The amount of frames to wait before each animation cycle.
 * WARNING: Lower numbers can jeopardize game performance.
 * @default 60
 *
 * @param MirrorActorAni:eval
 * @text Mirror Actor Ani?
 * @parent AnimationSettings
 * @type boolean
 * @on Mirror
 * @off Don't
 * @desc Mirror animations played on actors?
 * @default true
 *
 * @param MuteAnimations:eval
 * @text Mute Animation SFX?
 * @parent AnimationSettings
 * @type boolean
 * @on Mute
 * @off Don't
 * @desc Mute sounds played by animations?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Aggro Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Aggro:
 *
 * @param General
 *
 * @param PriorityHighest:eval
 * @text Priority: Highest TGR
 * @parent General
 * @type boolean
 * @on Always
 * @off Weighted
 * @desc When enemies target actors for an single target attack,
 * always target the highest members or make it weighted?
 * @default true
 *
 * @param AggroPerDmg:num
 * @text Aggro Per Damage
 * @parent General
 * @desc The amount of aggro generated per point of HP damage dealt to an enemy.
 * @default 0.1
 *
 * @param AggroPerHeal:num
 * @text Aggro Per Heal
 * @parent General
 * @desc The amount of aggro generated per point of HP recovered to an ally.
 * @default 0.5
 *
 * @param Gauge
 *
 * @param VisibleGauge:eval
 * @text Visible Battler Gauge
 * @parent Gauge
 * @type boolean
 * @on Visible
 * @off None
 * @desc Display an aggro gauge over an SV actor's head to show
 * current aggro level compared to other party members.
 * @default false
 *
 * @param StatusGauge:eval
 * @text Visible Status Gauge
 * @parent Gauge
 * @type boolean
 * @on Visible
 * @off None
 * @desc Display an aggro gauge in the Battle Status Window
 * to show the current aggro level compared to others.
 * @default true
 *
 * @param GaugeColor1:str
 * @text Gauge Color 1
 * @parent Gauge
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default #959595
 *
 * @param GaugeColor2:str
 * @text Gauge Color 2
 * @parent Gauge
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default #ffffff
 *
 * @param AnchorX:num
 * @text Anchor X
 * @parent Gauge
 * @desc Where do you want the Aggro Gauge sprite's anchor X to be?
 * Use values between 0 and 1 to be safe.
 * @default 0.5
 *
 * @param AnchorY:num
 * @text Anchor Y
 * @parent Gauge
 * @desc Where do you want the Aggro Gauge sprite's anchor Y to be?
 * Use values between 0 and 1 to be safe.
 * @default 1.0
 *
 * @param Scale:num
 * @text Scale
 * @parent Gauge
 * @desc How large/small do you want the Aggro Gauge to be scaled?
 * @default 0.5
 * 
 * @param BattlerGauge
 * @text Battler Gauge
 * @parent Gauge
 *
 * @param OffsetX:num
 * @text Offset X
 * @parent BattlerGauge
 * @desc How many pixels to offset the Aggro Gauge's X by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param OffsetY:num
 * @text Offset Y
 * @parent BattlerGauge
 * @desc How many pixels to offset the Aggro Gauge's Y by?
 * Negative goes up. Positive goes down.
 * @default +2
 * 
 * @param BattleStatus
 * @text Battle Status Gauge
 * @parent Gauge
 *
 * @param BattleStatusOffsetX:num
 * @text Offset X
 * @parent BattleStatus
 * @desc How many pixels to offset the Aggro Gauge's X by?
 * Negative goes left. Positive goes right.
 * @default +0
 *
 * @param BattleStatusOffsetY:num
 * @text Offset Y
 * @parent BattleStatus
 * @desc How many pixels to offset the Aggro Gauge's Y by?
 * Negative goes up. Positive goes down.
 * @default +0
 *
 * @param Options
 * @text Options
 *
 * @param AddOption:eval
 * @text Add Option?
 * @parent Options
 * @type boolean
 * @on Add
 * @off Don't Add
 * @desc Add the 'Show Aggro Gauge' option to the Options menu?
 * @default true
 *
 * @param AdjustOptionsRect:eval
 * @text Adjust Window Height
 * @parent Options
 * @type boolean
 * @on Adjust
 * @off Don't
 * @desc Automatically adjust the options window height?
 * @default true
 *
 * @param OptionName:str
 * @text Option Name
 * @parent Options
 * @desc Command name of the option.
 * @default Show Aggro Gauge
 *
 */
//=============================================================================

const _0x41c4=['ActorChangeAggro','makeProvokeTarget','certainHitTaunt','GaugeColor1','matchTauntType','BattleStatusOffsetX','BlendMode','loseAggro','_statusType','gaugeColor2','LineColor','abs','gaugeRate','currentMaxValue','ConvertParams','HITTYPE_PHYSICAL','_provokeBitmap','HeightOrigin','provokeBitmap','placeActorName','250257SndZqd','_aggroGaugeSprite','visible','bind','magicalTauntMembers','includes','placeAggroGauge','opponentsUnit','Opacity','filter','ConfigManager_makeData','getNextTauntAnimation','addState','Sprite_Battler_initialize','actor','ActorSetAggro','provoker','bitmap','findTgrMember','15680QYOQuk','status','tauntTargetsForAlive','Aggro','isSceneBattle','parentContainer','addAggroControlSystemProvokeCommand','83JxygVA','Spriteset_Battle_createBattleField','_counterAttackingTarget','showVisualAtbGauge','addAggroControlSystemCommands','#%1','_mirrorActorTauntAnimations','list','BattleManager_invokeMagicReflection','blendMode','isBypassTaunt','updateOpacity','initAggroControl','updateOpacityAggroControl','certainHitTauntMembers','randomTarget','provokeOrigin','gaugeHeight','addAggroControlSystemAggroCommand','ARRAYSTR','Sprite_Gauge_gaugeColor2','gainAggro','Game_BattlerBase_initMembers','clearProvokers','_magicalTauntAnimation','_homeX','_battler','isShowPriorityLines','description','min','isMagical','1599179OXeSJj','format','_spriteset','Game_Action_applyItemUserEffect','provokeLineColor','Sprite_Battler_update','physical','certainHit','baseAggro','constructor','applyData','initMembers','aggroMultiplier','bypassHighestAggro','EVAL','Game_Battler_addState','battleLayoutStyle','_mainSprite','addChildAt','isActor','sparam','Game_Battler_onBattleStart','ShowFacesListStyle','AddOption','convertStringToBattleTarget','bitmapHeight','tgr','Game_BattlerBase_refresh','itemRect','_animationCycleTime','physicalTaunt','stateHasProvoke','69602HRDbCa','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','isAggroGaugeShown','Sprite_Gauge_drawValue','isBypassHighestAggro','currentValueAggroControl','_aggro','iconWidth','faceWidth','setup','_targetX','createInnerSprite','drawValue','_tauntAnimationCycle','_cache','trim','applyItemUserEffectAggroControl','log','isProvokeAffected','lowestTgrMember','return\x200','battleUIOffsetY','isTauntAffected','name','AnchorY','anchor','subject','hitType','Parts','applyProvokeFilters','69837zflijM','isAggroType','getColorDataFromPluginParameters','isStateAffected','clamp','PartsSize','setFrame','note','ARRAYNUM','BattleLayout','executeHpDamage','AniPhysical','_targetIndex','time','1889829SsJcud','_menuAggroType','randomTauntTarget','Sprite_Gauge_update','isDead','isCertainHit','refresh','setBattler','tgrMax','Sprite_Gauge_currentMaxValue','AggroPerDmg','createStateSprite','parameters','createChildSprites','prototype','opacity','Sprite_Battler_initMembers','VisibleGauge','itemRectWithPadding','textColor','parse','isAggroGaugeVisible','Sprite_Actor_update','EnemyIndex','%1Taunt','ShowAnimation','_damageContainer','physicalTauntMembers','_customModified','Window_BattleEnemy_refresh','_sprites','BattleCore','_provoker','aggroGaugeX','nameX','pow','height','addChild','map','OffsetY','battler','updateAggroControl','provokeHeightOrigin','_homeY','createAggroGauge','isAtbGaugeVisible','battleAggro','VisuMZ_1_BattleCore','length','invokeMagicReflection','OptionName','magicalTaunt','Spriteset_Battle_update','AdjustOptionsRect','maxSprites','gaugeColor1','tgrSumFromGroup','Sprite_Gauge_gaugeColor1','AggroPerHeal','round','_%1TauntAnimation','_battleField','onBattleStart','_scene','aggroGaugeY','HITTYPE_MAGICAL','highestTgrMember','ConfigManager_applyData','BattleManager_invokeCounterAttack','getColor','Taunt','VisuMZ_0_CoreEngine','CycleTime','isSideView','match','scale','_physicalTauntAnimation','maxOpacity','_provokeSprite','tgrMin','19ZcFvep','smoothTarget','Sprite_Battler_setBattler','startNewTauntAnimation','EnemyChangeAggro','bypassTaunt','currentMaxValueAggroControl','aggroGauge','arcHeight','ARRAYEVAL','_provokeContainer','convertBattleTargetToString','reduce','ShowLines','addGeneralOptions','setAggro','Settings','AggroControlSystem','onBattleEnd','createProvokeHeightOrigin','HITTYPE_CERTAIN','OpacitySpeed','maxCommands','Window_StatusBase_placeActorName','_muteTauntAnimations','padding','members','item','_certainHitTauntAnimation','_colorCache','sortEnemies','drawAggroGauge','PriorityHighest','applyProvokeEffect','exit','FUNC','makeData','executeHpDamageAggroControl','updateAggroGaugeSprite','aggroGaugeColor2','Game_Action_executeHpDamage','Sprite_Actor_createStateSprite','Sprite_Gauge_gaugeRate','aggroGaugeColor1','Provoke','addCommand','traitObjects','randomInt','MuteAnimations','boxHeight','call','friendsUnit','createBattleField','isTargetHighestTGR','_tauntAnimationTimer','index','Game_Action_targetsForAlive','inBattle','Scale','isEnemy','_enemies','shift','targetsForAlive','alwaysTargetHighestAggro','_targetY','Sprite_Gauge_currentValue','requestFauxAnimation','Window_Options_addGeneralOptions','832524VKJmyP','applySubjectAggro','Battle\x20Actor\x20%1','_opacitySpeed','bypassProvoke','isAggroAffected','isAlive','updateChildrenOpacity','placeGauge','width','leftwardAnimation','Game_BattlerBase_sparam','registerCommand','invokeCounterAttack','indexOf','createProvokeSprite','ArcHeight','scope','VisuMZ_2_BattleSystemATB','updateBattlerPositions','updateSubPositions','applyTauntFilters','AniMagical','createBattleFieldAggroControl','update','initialize','applyGlobal','NUM','currentValue','ActorID','version','taunting','inputtingAction','max','isBypassProvoke','initTauntAnimations','aggro-gauge-color-1','actorId','Sprite_Gauge_gaugeX','partsSize','aliveMembers','some','aggro','Game_Action_applyGlobal','clearAggro','battleUIOffsetX','Scene_Options_maxCommands'];const _0x543b=function(_0x139688,_0x4e6f42){_0x139688=_0x139688-0x145;let _0x41c48e=_0x41c4[_0x139688];return _0x41c48e;};const _0x1298eb=_0x543b;(function(_0x3d60c9,_0x4d9d33){const _0x3a6595=_0x543b;while(!![]){try{const _0x4f1830=-parseInt(_0x3a6595(0x299))*parseInt(_0x3a6595(0x1b9))+-parseInt(_0x3a6595(0x253))*-parseInt(_0x3a6595(0x25a))+-parseInt(_0x3a6595(0x279))+parseInt(_0x3a6595(0x15b))+parseInt(_0x3a6595(0x1fd))+-parseInt(_0x3a6595(0x240))+parseInt(_0x3a6595(0x169));if(_0x4f1830===_0x4d9d33)break;else _0x3d60c9['push'](_0x3d60c9['shift']());}catch(_0x237ccf){_0x3d60c9['push'](_0x3d60c9['shift']());}}}(_0x41c4,0xe109c));var label=_0x1298eb(0x1ca),tier=tier||0x0,dependencies=[],pluginData=$plugins['filter'](function(_0x126667){const _0x5f3030=_0x1298eb;return _0x126667[_0x5f3030(0x254)]&&_0x126667[_0x5f3030(0x276)][_0x5f3030(0x245)]('['+label+']');})[0x0];VisuMZ[label][_0x1298eb(0x1c9)]=VisuMZ[label][_0x1298eb(0x1c9)]||{},VisuMZ[_0x1298eb(0x23a)]=function(_0x41ff11,_0x418b9){const _0x3f49bd=_0x1298eb;for(const _0x4eade5 in _0x418b9){if(_0x4eade5['match'](/(.*):(.*)/i)){const _0x468277=String(RegExp['$1']),_0x5ca3af=String(RegExp['$2'])['toUpperCase']()[_0x3f49bd(0x14c)]();let _0x2dfa27,_0x1a7c22,_0x1cc1f1;switch(_0x5ca3af){case _0x3f49bd(0x218):_0x2dfa27=_0x418b9[_0x4eade5]!==''?Number(_0x418b9[_0x4eade5]):0x0;break;case _0x3f49bd(0x163):_0x1a7c22=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):[],_0x2dfa27=_0x1a7c22[_0x3f49bd(0x18f)](_0x2aa6c7=>Number(_0x2aa6c7));break;case _0x3f49bd(0x287):_0x2dfa27=_0x418b9[_0x4eade5]!==''?eval(_0x418b9[_0x4eade5]):null;break;case _0x3f49bd(0x1c2):_0x1a7c22=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):[],_0x2dfa27=_0x1a7c22[_0x3f49bd(0x18f)](_0x3f08ed=>eval(_0x3f08ed));break;case'JSON':_0x2dfa27=_0x418b9[_0x4eade5]!==''?JSON['parse'](_0x418b9[_0x4eade5]):'';break;case'ARRAYJSON':_0x1a7c22=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):[],_0x2dfa27=_0x1a7c22['map'](_0x1b5d61=>JSON[_0x3f49bd(0x17d)](_0x1b5d61));break;case _0x3f49bd(0x1dc):_0x2dfa27=_0x418b9[_0x4eade5]!==''?new Function(JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5])):new Function(_0x3f49bd(0x151));break;case'ARRAYFUNC':_0x1a7c22=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):[],_0x2dfa27=_0x1a7c22[_0x3f49bd(0x18f)](_0x3ef93a=>new Function(JSON[_0x3f49bd(0x17d)](_0x3ef93a)));break;case'STR':_0x2dfa27=_0x418b9[_0x4eade5]!==''?String(_0x418b9[_0x4eade5]):'';break;case _0x3f49bd(0x26d):_0x1a7c22=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):[],_0x2dfa27=_0x1a7c22[_0x3f49bd(0x18f)](_0xab9921=>String(_0xab9921));break;case'STRUCT':_0x1cc1f1=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):{},_0x2dfa27=VisuMZ[_0x3f49bd(0x23a)]({},_0x1cc1f1);break;case'ARRAYSTRUCT':_0x1a7c22=_0x418b9[_0x4eade5]!==''?JSON[_0x3f49bd(0x17d)](_0x418b9[_0x4eade5]):[],_0x2dfa27=_0x1a7c22[_0x3f49bd(0x18f)](_0x29dc61=>VisuMZ['ConvertParams']({},JSON[_0x3f49bd(0x17d)](_0x29dc61)));break;default:continue;}_0x41ff11[_0x468277]=_0x2dfa27;}}return _0x41ff11;},(_0x4a8385=>{const _0x3dc1d9=_0x1298eb,_0x439f94=_0x4a8385[_0x3dc1d9(0x154)];for(const _0xcd90e5 of dependencies){if(!Imported[_0xcd90e5]){alert('%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.'[_0x3dc1d9(0x27a)](_0x439f94,_0xcd90e5)),SceneManager[_0x3dc1d9(0x1db)]();break;}}const _0x433b6c=_0x4a8385[_0x3dc1d9(0x276)];if(_0x433b6c['match'](/\[Version[ ](.*?)\]/i)){const _0xd3790c=Number(RegExp['$1']);_0xd3790c!==VisuMZ[label][_0x3dc1d9(0x21b)]&&(alert('%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.'[_0x3dc1d9(0x27a)](_0x439f94,_0xd3790c)),SceneManager[_0x3dc1d9(0x1db)]());}if(_0x433b6c[_0x3dc1d9(0x1b3)](/\[Tier[ ](\d+)\]/i)){const _0x1ae846=Number(RegExp['$1']);_0x1ae846<tier?(alert(_0x3dc1d9(0x29a)['format'](_0x439f94,_0x1ae846,tier)),SceneManager[_0x3dc1d9(0x1db)]()):tier=Math[_0x3dc1d9(0x21e)](_0x1ae846,tier);}VisuMZ['ConvertParams'](VisuMZ[label][_0x3dc1d9(0x1c9)],_0x4a8385[_0x3dc1d9(0x175)]);})(pluginData),PluginManager[_0x1298eb(0x209)](pluginData['name'],_0x1298eb(0x22c),_0x9dfbb1=>{const _0x557243=_0x1298eb;if(!$gameParty[_0x557243(0x1f2)]())return;VisuMZ[_0x557243(0x23a)](_0x9dfbb1,_0x9dfbb1);const _0x4f4a9b=$gameActors[_0x557243(0x24e)](_0x9dfbb1[_0x557243(0x21a)]),_0x1eaa46=_0x9dfbb1[_0x557243(0x256)];if(_0x4f4a9b)_0x4f4a9b['gainAggro'](_0x1eaa46);}),PluginManager[_0x1298eb(0x209)](pluginData[_0x1298eb(0x154)],_0x1298eb(0x24f),_0x2837a5=>{const _0x276b37=_0x1298eb;if(!$gameParty[_0x276b37(0x1f2)]())return;VisuMZ[_0x276b37(0x23a)](_0x2837a5,_0x2837a5);const _0x4742fe=$gameActors[_0x276b37(0x24e)](_0x2837a5[_0x276b37(0x21a)]),_0x10d2fb=_0x2837a5[_0x276b37(0x256)];if(_0x4742fe)_0x4742fe[_0x276b37(0x1c8)](_0x10d2fb);}),PluginManager[_0x1298eb(0x209)](pluginData[_0x1298eb(0x154)],_0x1298eb(0x1bd),_0x2e3f02=>{const _0x38cfa0=_0x1298eb;if(!$gameParty[_0x38cfa0(0x1f2)]())return;VisuMZ[_0x38cfa0(0x23a)](_0x2e3f02,_0x2e3f02);const _0x3ea399=$gameTroop['members']()[_0x2e3f02[_0x38cfa0(0x180)]],_0x56cd7c=_0x2e3f02[_0x38cfa0(0x256)];if(_0x3ea399)_0x3ea399[_0x38cfa0(0x26f)](_0x56cd7c);}),PluginManager[_0x1298eb(0x209)](pluginData[_0x1298eb(0x154)],'EnemySetAggro',_0x43dbf9=>{const _0x3a9214=_0x1298eb;if(!$gameParty[_0x3a9214(0x1f2)]())return;VisuMZ[_0x3a9214(0x23a)](_0x43dbf9,_0x43dbf9);const _0x4445ee=$gameTroop[_0x3a9214(0x1d3)]()[_0x43dbf9[_0x3a9214(0x180)]],_0x267bc7=_0x43dbf9[_0x3a9214(0x256)];if(_0x4445ee)_0x4445ee['setAggro'](_0x267bc7);}),DataManager['stateHasProvoke']=function(_0x61ecb0){const _0x430a88=_0x1298eb;if(!_0x61ecb0)return![];return _0x61ecb0[_0x430a88(0x162)][_0x430a88(0x1b3)](/<PROVOKE>/i);},DataManager[_0x1298eb(0x21f)]=function(_0x4da563){const _0x24ce6f=_0x1298eb;if(!_0x4da563)return![];return _0x4da563[_0x24ce6f(0x162)]['match'](/<BYPASS PROVOKE>/i);},DataManager[_0x1298eb(0x264)]=function(_0x39c1a2){const _0x5008f0=_0x1298eb;if(!_0x39c1a2)return![];return _0x39c1a2[_0x5008f0(0x162)][_0x5008f0(0x1b3)](/<BYPASS TAUNT>/i);},DataManager[_0x1298eb(0x29d)]=function(_0xf09c2c){const _0x1f8096=_0x1298eb;if(!_0xf09c2c)return![];return _0xf09c2c[_0x1f8096(0x162)][_0x1f8096(0x1b3)](/<BYPASS HIGHEST (?:AGGRO|ENMITY|THREAT)>/i);},DataManager['alwaysTargetHighestAggro']=function(_0x825731){const _0xf0b17f=_0x1298eb;if(!_0x825731)return![];return _0x825731[_0xf0b17f(0x162)][_0xf0b17f(0x1b3)](/<TARGET HIGHEST (?:AGGRO|ENMITY|THREAT)>/i);},ImageManager[_0x1298eb(0x23e)]=function(){const _0xe7cfbc=_0x1298eb;if(this[_0xe7cfbc(0x23c)])return this[_0xe7cfbc(0x23c)];return this['_provokeBitmap']=new Bitmap(0x64,0x64),this[_0xe7cfbc(0x23c)]['drawCircle'](0x32,0x32,0x32,ColorManager[_0xe7cfbc(0x27d)]()),this['_provokeBitmap'][_0xe7cfbc(0x185)]=![],this['_provokeBitmap'];},ConfigManager['aggroGauge']=!![],ConfigManager[_0x1298eb(0x26a)]=!![],VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x24a)]=ConfigManager[_0x1298eb(0x1dd)],ConfigManager['makeData']=function(){const _0x4d115d=_0x1298eb,_0x405c40=VisuMZ[_0x4d115d(0x1ca)][_0x4d115d(0x24a)]['call'](this);return _0x405c40[_0x4d115d(0x1c0)]=this[_0x4d115d(0x1c0)],_0x405c40['provokeOrigin']=this[_0x4d115d(0x26a)],_0x405c40;},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1ac)]=ConfigManager[_0x1298eb(0x283)],ConfigManager[_0x1298eb(0x283)]=function(_0x6a541e){const _0x56b3d6=_0x1298eb;VisuMZ['AggroControlSystem']['ConfigManager_applyData'][_0x56b3d6(0x1eb)](this,_0x6a541e),_0x56b3d6(0x1c0)in _0x6a541e?this['aggroGauge']=_0x6a541e[_0x56b3d6(0x1c0)]:this[_0x56b3d6(0x1c0)]=!![],'provokeOrigin'in _0x6a541e?this['provokeOrigin']=_0x6a541e[_0x56b3d6(0x26a)]:this['provokeOrigin']=!![];},TextManager[_0x1298eb(0x1c0)]=VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1c9)][_0x1298eb(0x256)][_0x1298eb(0x19b)],TextManager['provokeOrigin']=VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1c9)][_0x1298eb(0x1e5)]['OptionName'],ColorManager[_0x1298eb(0x15d)]=function(_0x2f4d5b,_0x2fe3d8){const _0x557f76=_0x1298eb;return _0x2fe3d8=String(_0x2fe3d8),this['_colorCache']=this[_0x557f76(0x1d6)]||{},_0x2fe3d8[_0x557f76(0x1b3)](/#(.*)/i)?this[_0x557f76(0x1d6)][_0x2f4d5b]=_0x557f76(0x25f)[_0x557f76(0x27a)](String(RegExp['$1'])):this[_0x557f76(0x1d6)][_0x2f4d5b]=this[_0x557f76(0x17c)](Number(_0x2fe3d8)),this[_0x557f76(0x1d6)][_0x2f4d5b];},ColorManager[_0x1298eb(0x1ae)]=function(_0x41a61d){const _0x23d967=_0x1298eb;return _0x41a61d=String(_0x41a61d),_0x41a61d[_0x23d967(0x1b3)](/#(.*)/i)?_0x23d967(0x25f)[_0x23d967(0x27a)](String(RegExp['$1'])):this['textColor'](Number(_0x41a61d));},ColorManager['provokeLineColor']=function(){const _0x4b1704=_0x1298eb,_0x5d9e5f='provoke-line-color';this[_0x4b1704(0x1d6)]=this[_0x4b1704(0x1d6)]||{};if(this[_0x4b1704(0x1d6)][_0x5d9e5f])return this[_0x4b1704(0x1d6)][_0x5d9e5f];const _0x31d750=VisuMZ[_0x4b1704(0x1ca)][_0x4b1704(0x1c9)][_0x4b1704(0x1e5)][_0x4b1704(0x236)];return this[_0x4b1704(0x15d)](_0x5d9e5f,_0x31d750);},ColorManager[_0x1298eb(0x1e4)]=function(){const _0x16a061=_0x1298eb,_0x3b9ab3=_0x16a061(0x221);this['_colorCache']=this[_0x16a061(0x1d6)]||{};if(this['_colorCache'][_0x3b9ab3])return this[_0x16a061(0x1d6)][_0x3b9ab3];const _0x479f30=VisuMZ[_0x16a061(0x1ca)][_0x16a061(0x1c9)]['Aggro'][_0x16a061(0x22f)];return this[_0x16a061(0x15d)](_0x3b9ab3,_0x479f30);},ColorManager[_0x1298eb(0x1e0)]=function(){const _0x49f57c=_0x1298eb,_0x219724='aggro-gauge-color-2';this[_0x49f57c(0x1d6)]=this[_0x49f57c(0x1d6)]||{};if(this[_0x49f57c(0x1d6)][_0x219724])return this[_0x49f57c(0x1d6)][_0x219724];const _0x9380cd=VisuMZ[_0x49f57c(0x1ca)][_0x49f57c(0x1c9)]['Aggro']['GaugeColor2'];return this[_0x49f57c(0x15d)](_0x219724,_0x9380cd);},SceneManager['isSceneBattle']=function(){const _0xed7e6=_0x1298eb;return this[_0xed7e6(0x1a8)]&&this[_0xed7e6(0x1a8)][_0xed7e6(0x282)]===Scene_Battle;},BattleManager['convertBattleTargetToString']=function(_0x5f18a7){const _0x412325=_0x1298eb;let _0x3c3564=this['_subject'];this[_0x412325(0x25c)]&&(_0x3c3564=this[_0x412325(0x25c)]);if(!_0x3c3564)return null;if(_0x3c3564[_0x412325(0x28c)]()&&_0x5f18a7[_0x412325(0x1f4)]())return _0x412325(0x1ff)[_0x412325(0x27a)](_0x3c3564[_0x412325(0x222)]());else{if(_0x3c3564[_0x412325(0x1f4)]()&&_0x5f18a7[_0x412325(0x28c)]())return'Battle\x20Enemy\x20%1'[_0x412325(0x27a)](_0x3c3564['index']());}return null;},BattleManager[_0x1298eb(0x291)]=function(_0x56fd3a){const _0x423689=_0x1298eb;if(!_0x56fd3a)return null;if(_0x56fd3a[_0x423689(0x1b3)](/BATTLE ACTOR (\d+)/i))return $gameActors[_0x423689(0x24e)](Number(RegExp['$1']));else{if(_0x56fd3a[_0x423689(0x1b3)](/BATTLE ENEMY (\d+)/i))return $gameTroop['members']()[Number(RegExp['$1'])];}return null;},BattleManager[_0x1298eb(0x1ee)]=function(){const _0x2de23e=_0x1298eb;return VisuMZ['AggroControlSystem'][_0x2de23e(0x1c9)][_0x2de23e(0x256)][_0x2de23e(0x1d9)];},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1f1)]=Game_Action[_0x1298eb(0x177)][_0x1298eb(0x1f7)],Game_Action[_0x1298eb(0x177)]['targetsForAlive']=function(_0x23954c){const _0x4372bb=_0x1298eb;if(this[_0x4372bb(0x14f)]())return this[_0x4372bb(0x22d)]();else{if(this['isTauntAffected']())return this[_0x4372bb(0x255)](_0x23954c);else return this[_0x4372bb(0x202)]()?[_0x23954c[_0x4372bb(0x1ab)]()]:VisuMZ['AggroControlSystem'][_0x4372bb(0x1f1)][_0x4372bb(0x1eb)](this,_0x23954c);}},Game_Action[_0x1298eb(0x177)]['isProvokeAffected']=function(){const _0x380b43=_0x1298eb;if(this[_0x380b43(0x1d4)]()[_0x380b43(0x20e)]!==0x1)return![];if(DataManager[_0x380b43(0x21f)](this[_0x380b43(0x1d4)]()))return![];if(this[_0x380b43(0x157)]()[_0x380b43(0x201)]())return![];return this['subject']()[_0x380b43(0x14f)]();},Game_Action['prototype'][_0x1298eb(0x22d)]=function(){const _0x136e98=_0x1298eb;return[this[_0x136e98(0x157)]()[_0x136e98(0x250)]()];},Game_Action[_0x1298eb(0x177)][_0x1298eb(0x153)]=function(){const _0x22417a=_0x1298eb;if(this[_0x22417a(0x1d4)]()['scope']!==0x1)return![];if(DataManager[_0x22417a(0x264)](this[_0x22417a(0x1d4)]()))return![];if(this[_0x22417a(0x157)]()[_0x22417a(0x1be)]())return![];const _0x4fa388=this[_0x22417a(0x247)]();if(this['isPhysical']()&&_0x4fa388[_0x22417a(0x184)]()['length']>0x0)return!![];if(this[_0x22417a(0x278)]()&&_0x4fa388['magicalTauntMembers']()['length']>0x0)return!![];if(this['isCertainHit']()&&_0x4fa388[_0x22417a(0x268)]()[_0x22417a(0x199)]>0x0)return!![];return![];},Game_Action[_0x1298eb(0x177)][_0x1298eb(0x255)]=function(_0x2e75f2){const _0x4a0514=_0x1298eb;if(this[_0x4a0514(0x167)]<0x0)return[_0x2e75f2[_0x4a0514(0x16b)](this[_0x4a0514(0x1d4)]()[_0x4a0514(0x158)])];else{const _0x2e41a6=_0x2e75f2[_0x4a0514(0x1ba)](this[_0x4a0514(0x167)]);return _0x2e41a6['matchTauntType'](this[_0x4a0514(0x1d4)]()['hitType'])?[_0x2e41a6]:[_0x2e75f2[_0x4a0514(0x16b)]()];}},Game_Action[_0x1298eb(0x177)][_0x1298eb(0x202)]=function(){const _0x2641a7=_0x1298eb;if(this[_0x2641a7(0x1d4)]()[_0x2641a7(0x20e)]!==0x1)return![];if(this[_0x2641a7(0x167)]>=0x0)return![];if(DataManager[_0x2641a7(0x29d)](this['item']()))return![];if(this[_0x2641a7(0x157)]()[_0x2641a7(0x286)]())return![];if(DataManager[_0x2641a7(0x1f8)](this[_0x2641a7(0x1d4)]()))return!![];if(this['subject']()[_0x2641a7(0x1f8)]())return!![];return BattleManager[_0x2641a7(0x1ee)]();},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x228)]=Game_Action[_0x1298eb(0x177)][_0x1298eb(0x217)],Game_Action[_0x1298eb(0x177)][_0x1298eb(0x217)]=function(){const _0x20375b=_0x1298eb;VisuMZ[_0x20375b(0x1ca)][_0x20375b(0x228)]['call'](this),this[_0x20375b(0x1fe)]();},Game_Action[_0x1298eb(0x177)][_0x1298eb(0x1fe)]=function(){const _0x1ea133=_0x1298eb,_0x13bc24=this[_0x1ea133(0x1d4)]()[_0x1ea133(0x162)];if(_0x13bc24[_0x1ea133(0x1b3)](/<(?:USER AGGRO|AGGRO|USER ENMITY|ENMITY|USER THREAT|THREAT): ([\+\-]\d+)>/i)){const _0x461b12=Number(RegExp['$1']);this[_0x1ea133(0x157)]()[_0x1ea133(0x26f)](_0x461b12);}if(_0x13bc24[_0x1ea133(0x1b3)](/<JS (?:USER AGGRO|AGGRO|USER ENMITY|ENMITY|USER THREAT|THREAT)>\s*([\s\S]*)\s*<\/JS (?:USER AGGRO|AGGRO|USER ENMITY|ENMITY|USER THREAT|THREAT)>/i)){const _0x2e0834=String(RegExp['$1']),_0x4fe3e8=this[_0x1ea133(0x157)](),_0x1e6bb4=this['item'](),_0x494ca2=this[_0x1ea133(0x157)](),_0x536ec5=_0x494ca2;let _0x5bfc34=_0x4fe3e8[_0x1ea133(0x197)]();try{eval(_0x2e0834);}catch(_0x164fd7){if($gameTemp['isPlaytest']())console[_0x1ea133(0x14e)](_0x164fd7);}_0x4fe3e8[_0x1ea133(0x1c8)](_0x5bfc34);}},VisuMZ[_0x1298eb(0x1ca)]['Game_Action_applyItemUserEffect']=Game_Action[_0x1298eb(0x177)]['applyItemUserEffect'],Game_Action[_0x1298eb(0x177)]['applyItemUserEffect']=function(_0xa89724){const _0x414e2c=_0x1298eb;VisuMZ['AggroControlSystem'][_0x414e2c(0x27c)][_0x414e2c(0x1eb)](this,_0xa89724),this[_0x414e2c(0x14d)](_0xa89724);},Game_Action[_0x1298eb(0x177)][_0x1298eb(0x14d)]=function(_0x29a525){const _0x3ab674=_0x1298eb;if(!this[_0x3ab674(0x1d4)]())return;if(!SceneManager['isSceneBattle']())return;const _0x12fd9d=this[_0x3ab674(0x1d4)]()['note'];if(_0x12fd9d[_0x3ab674(0x1b3)](/<TARGET (?:AGGRO|ENMITY|THREAT): ([\+\-]\d+)>/i)){const _0x10f66d=Number(RegExp['$1']);_0x29a525['gainAggro'](_0x10f66d);}if(_0x12fd9d[_0x3ab674(0x1b3)](/<JS TARGET (?:AGGRO|ENMITY|THREAT)>\s*([\s\S]*)\s*<\/JS TARGET (?:AGGRO|ENMITY|THREAT)>/i)){const _0x26be16=String(RegExp['$1']),_0x2db359=this[_0x3ab674(0x157)](),_0x4f715b=this[_0x3ab674(0x1d4)](),_0x4bf888=this[_0x3ab674(0x157)](),_0x1824af=_0x29a525;let _0x418bf0=_0x29a525[_0x3ab674(0x197)]();try{eval(_0x26be16);}catch(_0x939c91){if($gameTemp['isPlaytest']())console[_0x3ab674(0x14e)](_0x939c91);}_0x29a525[_0x3ab674(0x1c8)](_0x418bf0);}},VisuMZ[_0x1298eb(0x1ca)]['Game_Action_executeHpDamage']=Game_Action[_0x1298eb(0x177)]['executeHpDamage'],Game_Action[_0x1298eb(0x177)][_0x1298eb(0x165)]=function(_0x4c3ab0,_0x2436ae){const _0x29d7a7=_0x1298eb;VisuMZ[_0x29d7a7(0x1ca)][_0x29d7a7(0x1e1)][_0x29d7a7(0x1eb)](this,_0x4c3ab0,_0x2436ae),this[_0x29d7a7(0x1de)](_0x4c3ab0,_0x2436ae);},Game_Action[_0x1298eb(0x177)][_0x1298eb(0x1de)]=function(_0x5e37cf,_0x4d0453){const _0x48f64c=_0x1298eb,_0x5e868a=VisuMZ[_0x48f64c(0x1ca)]['Settings'][_0x48f64c(0x256)];if(_0x4d0453>0x0&&_0x5e37cf['isActor']()!==this[_0x48f64c(0x157)]()['isActor']()){const _0x594358=_0x5e868a[_0x48f64c(0x173)];this['subject']()['gainAggro'](_0x594358*_0x4d0453);}if(_0x4d0453<0x0&&_0x5e37cf[_0x48f64c(0x28c)]()===this[_0x48f64c(0x157)]()[_0x48f64c(0x28c)]()){const _0x1d71a8=_0x5e868a[_0x48f64c(0x1a3)];this[_0x48f64c(0x157)]()[_0x48f64c(0x26f)](_0x1d71a8*Math[_0x48f64c(0x237)](_0x4d0453));}},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x270)]=Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x284)],Game_BattlerBase[_0x1298eb(0x177)]['initMembers']=function(){const _0x3388af=_0x1298eb;this['_cache']={},VisuMZ['AggroControlSystem'][_0x3388af(0x270)][_0x3388af(0x1eb)](this),this[_0x3388af(0x266)]();},Game_BattlerBase['prototype']['initAggroControl']=function(){const _0x3188c8=_0x1298eb;this[_0x3188c8(0x271)](),this[_0x3188c8(0x229)]();},Game_BattlerBase['prototype'][_0x1298eb(0x271)]=function(){const _0xbe8270=_0x1298eb;this[_0xbe8270(0x189)]={};},VisuMZ['AggroControlSystem']['Game_BattlerBase_refresh']=Game_BattlerBase['prototype'][_0x1298eb(0x16f)],Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x16f)]=function(){const _0x54e36c=_0x1298eb;this[_0x54e36c(0x14b)]={},VisuMZ['AggroControlSystem'][_0x54e36c(0x294)][_0x54e36c(0x1eb)](this);},Game_BattlerBase['prototype']['checkCacheKey']=function(_0xc2686d){const _0xa2f0dc=_0x1298eb;return this[_0xa2f0dc(0x14b)]=this[_0xa2f0dc(0x14b)]||{},this[_0xa2f0dc(0x14b)][_0xc2686d]!==undefined;},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x250)]=function(){const _0x1c34ba=_0x1298eb;for(const _0x5040b5 of this['states']()){if(DataManager[_0x1c34ba(0x298)](_0x5040b5)){if(this[_0x1c34ba(0x189)]===undefined)this[_0x1c34ba(0x271)]();const _0x450405=this[_0x1c34ba(0x189)][_0x5040b5['id']],_0x1cc651=BattleManager[_0x1c34ba(0x291)](_0x450405);if(_0x1cc651&&_0x1cc651['isAlive']())return _0x1cc651;}}return null;},Game_BattlerBase[_0x1298eb(0x177)]['isProvokeAffected']=function(){const _0x12bd1d=_0x1298eb;return!!this[_0x12bd1d(0x250)]();},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x201)]=function(){const _0x53b746=_0x1298eb;return this[_0x53b746(0x1e7)]()['some'](_0x53069b=>_0x53069b&&_0x53069b[_0x53b746(0x162)][_0x53b746(0x1b3)](/<BYPASS PROVOKE>/i));},Game_BattlerBase[_0x1298eb(0x177)]['provokeHeightOrigin']=function(){const _0x31baa5=_0x1298eb;let _0x51cc22=_0x31baa5(0x193);if(this['checkCacheKey'](_0x51cc22))return this[_0x31baa5(0x14b)][_0x51cc22];return this[_0x31baa5(0x14b)][_0x51cc22]=this[_0x31baa5(0x1cc)](),this[_0x31baa5(0x14b)][_0x51cc22];},Game_BattlerBase['prototype']['createProvokeHeightOrigin']=function(){const _0x578df4=_0x1298eb,_0x2d63af=this[_0x578df4(0x28c)]()?this[_0x578df4(0x24e)]()[_0x578df4(0x162)]:this[_0x578df4(0x1f4)]()?this['enemy']()[_0x578df4(0x162)]:'';if(_0x2d63af[_0x578df4(0x1b3)](/<PROVOKE HEIGHT ORIGIN: (\d+)([%％])>/i))return Number(RegExp['$1'])*0.01;return VisuMZ[_0x578df4(0x1ca)]['Settings'][_0x578df4(0x1e5)][_0x578df4(0x23d)];},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x230)]=function(_0x446de9){const _0x136470=_0x1298eb;switch(_0x446de9){case Game_Action[_0x136470(0x23b)]:return this[_0x136470(0x297)]();break;case Game_Action[_0x136470(0x1aa)]:return this[_0x136470(0x19c)]();break;case Game_Action[_0x136470(0x1cd)]:return this[_0x136470(0x22e)]();break;}},Game_BattlerBase['prototype'][_0x1298eb(0x21c)]=function(){const _0x78dff6=_0x1298eb;return this[_0x78dff6(0x297)]()||this[_0x78dff6(0x19c)]()||this[_0x78dff6(0x22e)]();},Game_BattlerBase['prototype'][_0x1298eb(0x297)]=function(){const _0x2f21ce=_0x1298eb;return this[_0x2f21ce(0x1e7)]()[_0x2f21ce(0x226)](_0x1a3244=>_0x1a3244&&_0x1a3244[_0x2f21ce(0x162)][_0x2f21ce(0x1b3)](/<(?:TAUNT|PHYSICAL TAUNT|ALL TAUNT)>/i));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x19c)]=function(){const _0x42df30=_0x1298eb;return this[_0x42df30(0x1e7)]()[_0x42df30(0x226)](_0x29a35f=>_0x29a35f&&_0x29a35f[_0x42df30(0x162)]['match'](/<(?:TAUNT|MAGICAL TAUNT|ALL TAUNT)>/i));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x22e)]=function(){const _0x49eb95=_0x1298eb;return this[_0x49eb95(0x1e7)]()[_0x49eb95(0x226)](_0x5363db=>_0x5363db&&_0x5363db['note']['match'](/<(?:TAUNT|CERTAIN TAUNT|CERTAIN HIT TAUNT|ALL TAUNT)>/i));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x1be)]=function(){const _0x23d682=_0x1298eb;return this[_0x23d682(0x1e7)]()[_0x23d682(0x226)](_0x3375e8=>_0x3375e8&&_0x3375e8[_0x23d682(0x162)][_0x23d682(0x1b3)](/<BYPASS TAUNT>/i));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x229)]=function(){const _0x1fcdc0=_0x1298eb;this[_0x1fcdc0(0x29f)]=0x1;},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x208)]=Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x28d)],Game_BattlerBase['prototype'][_0x1298eb(0x28d)]=function(_0xa9f491){const _0x5509a9=_0x1298eb;let _0x30e9e9=VisuMZ[_0x5509a9(0x1ca)][_0x5509a9(0x208)]['call'](this,_0xa9f491);if(_0xa9f491===0x0){if(this[_0x5509a9(0x29f)]===undefined)this[_0x5509a9(0x229)]();_0x30e9e9*=this[_0x5509a9(0x227)]();}return _0x30e9e9;},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x1c8)]=function(_0x1b6165){const _0x32e3ea=_0x1298eb;if(this[_0x32e3ea(0x29f)]===undefined)this[_0x32e3ea(0x229)]();this[_0x32e3ea(0x29f)]=Math[_0x32e3ea(0x21e)](0x1,Math['round'](this[_0x32e3ea(0x29f)]));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x26f)]=function(_0x49d313){const _0x21d542=_0x1298eb;if(this[_0x21d542(0x29f)]===undefined)this[_0x21d542(0x229)]();this[_0x21d542(0x29f)]=Math[_0x21d542(0x21e)](0x1,this[_0x21d542(0x29f)]+Math[_0x21d542(0x1a4)](_0x49d313));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x233)]=function(_0x370ec3){const _0x45fec8=_0x1298eb;this[_0x45fec8(0x26f)](-_0x370ec3);},Game_BattlerBase[_0x1298eb(0x177)]['aggro']=function(){const _0x36e11b=_0x1298eb;if(this['isDead']())return 0x0;return this[_0x36e11b(0x281)]()*this[_0x36e11b(0x285)]();},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x197)]=function(){const _0x33176a=_0x1298eb;return this[_0x33176a(0x29f)]===undefined&&this[_0x33176a(0x229)](),this[_0x33176a(0x29f)];},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x281)]=function(){const _0x4a27d0=_0x1298eb;return this[_0x4a27d0(0x1e7)]()['reduce']((_0x46b943,_0x2a7380)=>{const _0x221c61=_0x4a27d0;return _0x2a7380&&_0x2a7380[_0x221c61(0x162)]['match'](/<(?:AGGRO|ENMITY|THREAT): ([\+\-]\d+)>/i)?_0x46b943+Number(RegExp['$1'])/0x64:_0x46b943;},this[_0x4a27d0(0x197)]());},Game_BattlerBase['prototype'][_0x1298eb(0x285)]=function(){const _0x56cfd5=_0x1298eb;return this[_0x56cfd5(0x1e7)]()[_0x56cfd5(0x1c5)]((_0x4f3441,_0x92ee5f)=>{const _0x36424b=_0x56cfd5;return _0x92ee5f&&_0x92ee5f[_0x36424b(0x162)][_0x36424b(0x1b3)](/<(?:AGGRO|ENMITY|THREAT) MULTIPLIER: (\d+)%>/i)?_0x4f3441+Number(RegExp['$1'])/0x64:_0x4f3441;},0x1);},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x286)]=function(){const _0x12c5f2=_0x1298eb;return this['traitObjects']()['some'](_0x47751d=>_0x47751d&&_0x47751d[_0x12c5f2(0x162)][_0x12c5f2(0x1b3)](/<BYPASS HIGHEST (?:AGGRO|ENMITY|THREAT)>/i));},Game_BattlerBase[_0x1298eb(0x177)][_0x1298eb(0x1f8)]=function(){const _0x29a987=_0x1298eb;return this[_0x29a987(0x1e7)]()[_0x29a987(0x226)](_0x40dfc7=>_0x40dfc7&&_0x40dfc7[_0x29a987(0x162)]['match'](/<TARGET HIGHEST (?:AGGRO|ENMITY|THREAT)>/i));},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x28e)]=Game_Battler[_0x1298eb(0x177)][_0x1298eb(0x1a7)],Game_Battler['prototype']['onBattleStart']=function(_0x54e17f){const _0x453e33=_0x1298eb;VisuMZ[_0x453e33(0x1ca)][_0x453e33(0x28e)][_0x453e33(0x1eb)](this,_0x54e17f),this[_0x453e33(0x229)]();},VisuMZ[_0x1298eb(0x1ca)]['Game_Battler_onBattleEnd']=Game_Battler[_0x1298eb(0x177)][_0x1298eb(0x1cb)],Game_Battler[_0x1298eb(0x177)][_0x1298eb(0x1cb)]=function(){const _0x33dce5=_0x1298eb;VisuMZ[_0x33dce5(0x1ca)]['Game_Battler_onBattleEnd'][_0x33dce5(0x1eb)](this),this[_0x33dce5(0x229)]();},VisuMZ['AggroControlSystem'][_0x1298eb(0x288)]=Game_Battler[_0x1298eb(0x177)]['addState'],Game_Battler[_0x1298eb(0x177)][_0x1298eb(0x24c)]=function(_0x398826){const _0x51ca02=_0x1298eb;VisuMZ[_0x51ca02(0x1ca)][_0x51ca02(0x288)][_0x51ca02(0x1eb)](this,_0x398826),this['applyProvokeEffect'](_0x398826);},Game_Battler['prototype'][_0x1298eb(0x1da)]=function(_0x13df2c){const _0x201565=_0x1298eb;if(this[_0x201565(0x15e)](_0x13df2c)){if(this['_provoker']===undefined)this['clearProvokers']();const _0x45be43=BattleManager[_0x201565(0x1c4)](this);this[_0x201565(0x189)][_0x13df2c]=_0x45be43,!this[_0x201565(0x189)][_0x13df2c]&&delete this[_0x201565(0x189)][_0x13df2c];}},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1ad)]=BattleManager[_0x1298eb(0x20a)],BattleManager[_0x1298eb(0x20a)]=function(_0x25bc9b,_0x343298){const _0x1576c0=_0x1298eb;this[_0x1576c0(0x25c)]=_0x343298,VisuMZ[_0x1576c0(0x1ca)]['BattleManager_invokeCounterAttack']['call'](this,_0x25bc9b,_0x343298),this[_0x1576c0(0x25c)]=undefined;},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x262)]=BattleManager[_0x1298eb(0x19a)],BattleManager['invokeMagicReflection']=function(_0x193fa3,_0x1cac6f){const _0x1fef32=_0x1298eb;this['_counterAttackingTarget']=_0x1cac6f,VisuMZ['AggroControlSystem'][_0x1fef32(0x262)][_0x1fef32(0x1eb)](this,_0x193fa3,_0x1cac6f),this[_0x1fef32(0x25c)]=undefined;},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x184)]=function(){const _0x203ab8=_0x1298eb;return this[_0x203ab8(0x225)]()['filter'](_0x4ff06c=>_0x4ff06c&&_0x4ff06c['physicalTaunt']());},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x244)]=function(){const _0x48a6d3=_0x1298eb;return this[_0x48a6d3(0x225)]()[_0x48a6d3(0x249)](_0x5c6480=>_0x5c6480&&_0x5c6480[_0x48a6d3(0x19c)]());},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x268)]=function(){const _0x56c6c3=_0x1298eb;return this[_0x56c6c3(0x225)]()[_0x56c6c3(0x249)](_0x446c4d=>_0x446c4d&&_0x446c4d[_0x56c6c3(0x22e)]());},Game_Unit[_0x1298eb(0x177)]['randomTauntTarget']=function(_0x4e8707){const _0x5f4617=_0x1298eb;let _0x34d9e6=[];switch(_0x4e8707){case Game_Action[_0x5f4617(0x23b)]:_0x34d9e6=this[_0x5f4617(0x184)]();break;case Game_Action['HITTYPE_MAGICAL']:_0x34d9e6=this[_0x5f4617(0x244)]();break;case Game_Action[_0x5f4617(0x1cd)]:_0x34d9e6=this['certainHitTauntMembers']();break;}let _0x38e844=Math['random']()*this[_0x5f4617(0x1a1)](_0x34d9e6),_0xfa54c4=null;if(BattleManager['isTargetHighestTGR']()){const _0x1f804d=!![];return this['findTgrMember'](_0x34d9e6,_0x1f804d);}else{for(const _0x2e2884 of _0x34d9e6){_0x38e844-=_0x2e2884[_0x5f4617(0x293)],_0x38e844<=0x0&&!_0xfa54c4&&(_0xfa54c4=_0x2e2884);}return _0xfa54c4||this[_0x5f4617(0x269)]();}},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x1a1)]=function(_0x1ca9d8){const _0x16215d=_0x1298eb;return _0x1ca9d8[_0x16215d(0x1c5)]((_0x41cc58,_0x1b4eec)=>_0x41cc58+_0x1b4eec[_0x16215d(0x293)],0x0);},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x171)]=function(){const _0x41ad6f=_0x1298eb,_0x42b4a9=this[_0x41ad6f(0x225)]()['map'](_0xb18eb4=>_0xb18eb4['tgr']);return Math[_0x41ad6f(0x21e)](..._0x42b4a9);},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x1b8)]=function(){const _0x598d3c=_0x1298eb,_0x3e6e77=this[_0x598d3c(0x225)]()[_0x598d3c(0x18f)](_0x5b44db=>_0x5b44db[_0x598d3c(0x293)]);return Math[_0x598d3c(0x277)](..._0x3e6e77);},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x1ab)]=function(){const _0xb1300c=_0x1298eb,_0x43eea0=this[_0xb1300c(0x171)](),_0x2c3ea0=this['aliveMembers']()['filter'](_0x378dd0=>_0x378dd0[_0xb1300c(0x293)]===_0x43eea0);return _0x2c3ea0[Math[_0xb1300c(0x1e8)](_0x2c3ea0[_0xb1300c(0x199)])]||this[_0xb1300c(0x269)]();},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x150)]=function(){const _0x2117e5=_0x1298eb,_0x563030=this['tgrMin'](),_0x4354ad=this['aliveMembers']()['filter'](_0xc01e6f=>_0xc01e6f[_0x2117e5(0x293)]===_0x563030);return _0x4354ad[Math[_0x2117e5(0x1e8)](_0x4354ad[_0x2117e5(0x199)])]||this[_0x2117e5(0x269)]();},Game_Unit[_0x1298eb(0x177)][_0x1298eb(0x252)]=function(_0x12f580,_0x24f3ba){const _0x2950f5=_0x1298eb,_0x361c89=_0x12f580[_0x2950f5(0x18f)](_0x576b68=>_0x576b68[_0x2950f5(0x293)]),_0x4bc9b5=_0x24f3ba?Math[_0x2950f5(0x21e)](..._0x361c89):Math[_0x2950f5(0x277)](..._0x361c89),_0x5986d7=_0x12f580[_0x2950f5(0x249)](_0x31e1bf=>_0x31e1bf[_0x2950f5(0x293)]===_0x4bc9b5);return _0x5986d7[Math[_0x2950f5(0x1e8)](_0x5986d7[_0x2950f5(0x199)])]||this[_0x2950f5(0x269)]();},VisuMZ['AggroControlSystem'][_0x1298eb(0x22b)]=Scene_Options[_0x1298eb(0x177)][_0x1298eb(0x1cf)],Scene_Options[_0x1298eb(0x177)]['maxCommands']=function(){const _0x15eea9=_0x1298eb;let _0x31df2f=VisuMZ[_0x15eea9(0x1ca)][_0x15eea9(0x22b)][_0x15eea9(0x1eb)](this);const _0x12ec18=VisuMZ[_0x15eea9(0x1ca)][_0x15eea9(0x1c9)];if(_0x12ec18[_0x15eea9(0x1e5)][_0x15eea9(0x290)]&&_0x12ec18[_0x15eea9(0x1e5)][_0x15eea9(0x19e)])_0x31df2f++;if(_0x12ec18[_0x15eea9(0x256)][_0x15eea9(0x290)]&&_0x12ec18['Aggro']['AdjustOptionsRect'])_0x31df2f++;return _0x31df2f;},Sprite_Battler[_0x1298eb(0x296)]=VisuMZ[_0x1298eb(0x1ca)]['Settings'][_0x1298eb(0x1af)][_0x1298eb(0x1b1)],Sprite_Battler[_0x1298eb(0x1b5)]=VisuMZ['AggroControlSystem'][_0x1298eb(0x1c9)][_0x1298eb(0x1af)][_0x1298eb(0x166)],Sprite_Battler[_0x1298eb(0x272)]=VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1c9)][_0x1298eb(0x1af)][_0x1298eb(0x213)],Sprite_Battler[_0x1298eb(0x1d5)]=VisuMZ['AggroControlSystem'][_0x1298eb(0x1c9)][_0x1298eb(0x1af)]['AniCertain'],Sprite_Battler[_0x1298eb(0x260)]=VisuMZ['AggroControlSystem'][_0x1298eb(0x1c9)][_0x1298eb(0x1af)]['MirrorActorAni'],Sprite_Battler[_0x1298eb(0x1d1)]=VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1c9)]['Taunt'][_0x1298eb(0x1e9)],VisuMZ['AggroControlSystem'][_0x1298eb(0x24d)]=Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x216)],Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x216)]=function(_0x52b9cd){const _0x3c2ebd=_0x1298eb;VisuMZ[_0x3c2ebd(0x1ca)][_0x3c2ebd(0x24d)][_0x3c2ebd(0x1eb)](this,_0x52b9cd),this[_0x3c2ebd(0x275)]()&&setTimeout(this[_0x3c2ebd(0x20c)][_0x3c2ebd(0x243)](this),0x3e8);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x179)]=Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x284)],Sprite_Battler['prototype'][_0x1298eb(0x284)]=function(){const _0x30248a=_0x1298eb;VisuMZ[_0x30248a(0x1ca)]['Sprite_Battler_initMembers'][_0x30248a(0x1eb)](this),this[_0x30248a(0x220)]();},Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x220)]=function(){const _0x4b9d40=_0x1298eb;this[_0x4b9d40(0x1ef)]=VisuMZ[_0x4b9d40(0x1ca)][_0x4b9d40(0x1c9)][_0x4b9d40(0x1af)]['CycleTime'],this[_0x4b9d40(0x14a)]=[_0x4b9d40(0x27f),'magical',_0x4b9d40(0x280)];},Sprite_Battler['prototype'][_0x1298eb(0x275)]=function(){const _0xe52597=_0x1298eb;if(!Imported[_0xe52597(0x198)])return![];if(![Sprite_Actor,Sprite_Enemy][_0xe52597(0x245)](this[_0xe52597(0x282)]))return![];return ConfigManager['provokeOrigin']&&VisuMZ[_0xe52597(0x1ca)][_0xe52597(0x1c9)]['Provoke'][_0xe52597(0x1c6)];},Sprite_Battler['prototype'][_0x1298eb(0x20c)]=function(){const _0x26cf76=_0x1298eb;if(!SceneManager[_0x26cf76(0x257)]())return;this[_0x26cf76(0x1b7)]=new Sprite_ProvokeTrail(this),this[_0x26cf76(0x1b7)]['parentContainer']()[_0x26cf76(0x18e)](this[_0x26cf76(0x1b7)]);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1bb)]=Sprite_Battler['prototype']['setBattler'],Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x170)]=function(_0x467254){const _0x300c23=_0x1298eb;VisuMZ['AggroControlSystem'][_0x300c23(0x1bb)][_0x300c23(0x1eb)](this,_0x467254);if(this['_aggroGaugeSprite'])this[_0x300c23(0x241)][_0x300c23(0x274)]=_0x467254;},VisuMZ['AggroControlSystem'][_0x1298eb(0x27e)]=Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x215)],Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x215)]=function(){const _0x9fffa=_0x1298eb;VisuMZ[_0x9fffa(0x1ca)][_0x9fffa(0x27e)]['call'](this),this['updateTauntAnimations']();},Sprite_Battler['prototype']['updateTauntAnimations']=function(){const _0x485d87=_0x1298eb;if(!Imported[_0x485d87(0x1b0)])return;if(!Imported[_0x485d87(0x198)])return;if(!VisuMZ[_0x485d87(0x1ca)][_0x485d87(0x1c9)]['Taunt'][_0x485d87(0x182)])return;if(!this[_0x485d87(0x274)])return;this[_0x485d87(0x1ef)]--,this['_tauntAnimationTimer']<=0x0&&this[_0x485d87(0x1bc)]();},Sprite_Battler[_0x1298eb(0x177)][_0x1298eb(0x1bc)]=function(){const _0xe317fa=_0x1298eb;this[_0xe317fa(0x1ef)]=Sprite_Battler[_0xe317fa(0x296)];if(!this[_0xe317fa(0x274)])return;if(!this[_0xe317fa(0x274)][_0xe317fa(0x21c)]())return;const _0x1e5c01=[this['_battler']],_0x3edac3=this[_0xe317fa(0x24b)](),_0x50d0e4=this[_0xe317fa(0x274)][_0xe317fa(0x28c)]()&&Sprite_Battler[_0xe317fa(0x260)],_0x431897=Sprite_Battler[_0xe317fa(0x1d1)];$gameTemp[_0xe317fa(0x1fb)](_0x1e5c01,_0x3edac3,_0x50d0e4,_0x431897);},Sprite_Battler['prototype'][_0x1298eb(0x24b)]=function(){const _0x2c9c0f=_0x1298eb;let _0x471881=this[_0x2c9c0f(0x14a)][_0x2c9c0f(0x199)];while(_0x471881){const _0x2e3afd=this[_0x2c9c0f(0x14a)][_0x2c9c0f(0x1f6)]();this[_0x2c9c0f(0x14a)]['push'](_0x2e3afd);const _0x3bcd37=_0x2c9c0f(0x181)[_0x2c9c0f(0x27a)](_0x2e3afd);if(this[_0x2c9c0f(0x274)][_0x3bcd37]()){const _0x5ad496=_0x2c9c0f(0x1a5)['format'](_0x2e3afd),_0x29afbb=Sprite_Battler[_0x5ad496];if(_0x29afbb)return _0x29afbb;}_0x471881--;}return Sprite_Battler[_0x2c9c0f(0x1d5)];},VisuMZ[_0x1298eb(0x1ca)]['Sprite_Actor_createStateSprite']=Sprite_Actor[_0x1298eb(0x177)][_0x1298eb(0x174)],Sprite_Actor[_0x1298eb(0x177)][_0x1298eb(0x174)]=function(){const _0x17499f=_0x1298eb;VisuMZ[_0x17499f(0x1ca)][_0x17499f(0x1e2)]['call'](this),this[_0x17499f(0x195)]();},Sprite_Actor[_0x1298eb(0x177)]['createAggroGauge']=function(){const _0x2acdfe=_0x1298eb;if(this[_0x2acdfe(0x282)]!==Sprite_Actor)return;if(!this['isAggroGaugeVisible']())return;if(!SceneManager['isSceneBattle']())return;const _0x41abaa=VisuMZ[_0x2acdfe(0x1ca)][_0x2acdfe(0x1c9)][_0x2acdfe(0x256)],_0x583f1f=new Sprite_Gauge();_0x583f1f['anchor']['x']=_0x41abaa['AnchorX'],_0x583f1f[_0x2acdfe(0x156)]['y']=_0x41abaa[_0x2acdfe(0x155)];const _0x4c53e9=Sprite_Gauge[_0x2acdfe(0x177)]['bitmapWidth']();_0x583f1f['scale']['x']=_0x583f1f[_0x2acdfe(0x1b4)]['y']=_0x41abaa[_0x2acdfe(0x1f3)],this[_0x2acdfe(0x241)]=_0x583f1f,this[_0x2acdfe(0x18e)](_0x583f1f);},Sprite_Actor[_0x1298eb(0x177)][_0x1298eb(0x17e)]=function(){const _0x1bd0a8=_0x1298eb;if(Imported[_0x1bd0a8(0x198)]&&this[_0x1bd0a8(0x282)]===Sprite_SvEnemy)return![];return ConfigManager['aggroGauge']&&VisuMZ['AggroControlSystem'][_0x1bd0a8(0x1c9)][_0x1bd0a8(0x256)][_0x1bd0a8(0x17a)];},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x17f)]=Sprite_Actor[_0x1298eb(0x177)]['update'],Sprite_Actor[_0x1298eb(0x177)][_0x1298eb(0x215)]=function(){const _0x54ab88=_0x1298eb;VisuMZ[_0x54ab88(0x1ca)][_0x54ab88(0x17f)][_0x54ab88(0x1eb)](this),this[_0x54ab88(0x1df)]();},Sprite_Actor[_0x1298eb(0x177)]['updateAggroGaugeSprite']=function(){const _0x131551=_0x1298eb;if(!this[_0x131551(0x274)])return;if(!this['_aggroGaugeSprite'])return;const _0x3f2b15=VisuMZ[_0x131551(0x1ca)][_0x131551(0x1c9)]['Aggro'],_0x11b273=this[_0x131551(0x241)];let _0x316a78=_0x3f2b15['OffsetX'];this['_battler'][_0x131551(0x22a)]&&(_0x316a78+=this[_0x131551(0x274)][_0x131551(0x22a)]());let _0x54ed94=_0x3f2b15[_0x131551(0x190)];this[_0x131551(0x274)][_0x131551(0x152)]&&(_0x54ed94+=this[_0x131551(0x274)]['battleUIOffsetY']()),_0x11b273['x']=_0x316a78,_0x11b273['y']=-this[_0x131551(0x18d)]+_0x54ed94,this[_0x131551(0x274)]&&_0x11b273[_0x131551(0x234)]!==_0x131551(0x227)&&(_0x11b273[_0x131551(0x242)]=!![],_0x11b273[_0x131551(0x146)](this[_0x131551(0x274)],'aggro')),this[_0x131551(0x1b4)]['x']<0x0&&(_0x11b273['scale']['x']=-Math[_0x131551(0x237)](_0x11b273[_0x131551(0x1b4)]['x']));},Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x15c)]=function(){const _0xc37114=_0x1298eb;return this[_0xc37114(0x274)]&&this['_statusType']===_0xc37114(0x227);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x223)]=Sprite_Gauge[_0x1298eb(0x177)]['gaugeX'],Sprite_Gauge[_0x1298eb(0x177)]['gaugeX']=function(){const _0x1b338a=_0x1298eb;return this['isAggroType']()?0x0:VisuMZ[_0x1b338a(0x1ca)][_0x1b338a(0x223)][_0x1b338a(0x1eb)](this);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1e3)]=Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x238)],Sprite_Gauge[_0x1298eb(0x177)]['gaugeRate']=function(){const _0x54dab2=_0x1298eb;let _0x30541b=VisuMZ[_0x54dab2(0x1ca)][_0x54dab2(0x1e3)][_0x54dab2(0x1eb)](this);if(this['isAggroType']()&&this[_0x54dab2(0x274)]){if(this[_0x54dab2(0x274)][_0x54dab2(0x16d)]())return 0x0;if(this[_0x54dab2(0x274)][_0x54dab2(0x203)]()&&this['_battler'][_0x54dab2(0x1ec)]()['aliveMembers']()[_0x54dab2(0x199)]===0x1)return 0x1;}return _0x30541b[_0x54dab2(0x15f)](0x0,0x1);},VisuMZ[_0x1298eb(0x1ca)]['Sprite_Gauge_currentValue']=Sprite_Gauge[_0x1298eb(0x177)]['currentValue'],Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x219)]=function(){const _0x45897b=_0x1298eb;return this['isAggroType']()?this[_0x45897b(0x29e)]():VisuMZ[_0x45897b(0x1ca)][_0x45897b(0x1fa)][_0x45897b(0x1eb)](this);},Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x29e)]=function(){const _0x5a96bc=_0x1298eb,_0x483d88=this['_battler']['friendsUnit'](),_0x3160e6=this[_0x5a96bc(0x274)][_0x5a96bc(0x293)]-_0x483d88[_0x5a96bc(0x1b8)](),_0x3a0bbc=_0x483d88[_0x5a96bc(0x171)]()-_0x483d88[_0x5a96bc(0x1b8)]();if(_0x3160e6>=_0x3a0bbc)return 0x64;return _0x3160e6/Math[_0x5a96bc(0x21e)](_0x3a0bbc,0x1)*0x64;},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x172)]=Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x239)],Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x239)]=function(){const _0x3b58d5=_0x1298eb;return this[_0x3b58d5(0x15c)]()?this[_0x3b58d5(0x1bf)]():VisuMZ[_0x3b58d5(0x1ca)][_0x3b58d5(0x172)][_0x3b58d5(0x1eb)](this);},Sprite_Gauge['prototype'][_0x1298eb(0x1bf)]=function(){return 0x64;},VisuMZ['AggroControlSystem']['Sprite_Gauge_gaugeColor1']=Sprite_Gauge[_0x1298eb(0x177)]['gaugeColor1'],Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x1a0)]=function(){const _0x36892c=_0x1298eb;return this[_0x36892c(0x15c)]()?ColorManager[_0x36892c(0x1e4)]():VisuMZ[_0x36892c(0x1ca)][_0x36892c(0x1a2)][_0x36892c(0x1eb)](this);},VisuMZ['AggroControlSystem'][_0x1298eb(0x26e)]=Sprite_Gauge['prototype'][_0x1298eb(0x235)],Sprite_Gauge['prototype'][_0x1298eb(0x235)]=function(){const _0x2edde6=_0x1298eb;return this[_0x2edde6(0x15c)]()?ColorManager['aggroGaugeColor2']():VisuMZ[_0x2edde6(0x1ca)][_0x2edde6(0x26e)][_0x2edde6(0x1eb)](this);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x16c)]=Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x215)],Sprite_Gauge[_0x1298eb(0x177)]['update']=function(){const _0x47272a=_0x1298eb;VisuMZ['AggroControlSystem'][_0x47272a(0x16c)]['call'](this),this['updateOpacityAggroControl']();},Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x267)]=function(){const _0x4f6d34=_0x1298eb;if(!this['isAggroType']())return;if(!Imported[_0x4f6d34(0x198)])return;const _0x2538e9=this[_0x4f6d34(0x274)][_0x4f6d34(0x191)]();if(this[_0x4f6d34(0x16a)])this[_0x4f6d34(0x178)]=0xff;else _0x2538e9&&_0x2538e9[_0x4f6d34(0x178)]>0x0?this['opacity']=0xff:this[_0x4f6d34(0x178)]=0x0;},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x29c)]=Sprite_Gauge['prototype'][_0x1298eb(0x149)],Sprite_Gauge[_0x1298eb(0x177)][_0x1298eb(0x149)]=function(){const _0x4a9667=_0x1298eb;if(this[_0x4a9667(0x15c)]())return;VisuMZ['AggroControlSystem'][_0x4a9667(0x29c)]['call'](this);};function Sprite_ProvokeTrail(){const _0x2efffc=_0x1298eb;this[_0x2efffc(0x216)](...arguments);}Sprite_ProvokeTrail[_0x1298eb(0x177)]=Object['create'](Sprite['prototype']),Sprite_ProvokeTrail['prototype'][_0x1298eb(0x282)]=Sprite_ProvokeTrail,Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x216)]=function(_0x234d18){const _0x191910=_0x1298eb;this['_mainSprite']=_0x234d18,Sprite[_0x191910(0x177)][_0x191910(0x216)][_0x191910(0x1eb)](this),this[_0x191910(0x284)](),this[_0x191910(0x176)]();},Sprite_ProvokeTrail[_0x1298eb(0x177)]['initMembers']=function(){const _0xdb3848=_0x1298eb,_0x4df9ec=VisuMZ[_0xdb3848(0x1ca)]['Settings'][_0xdb3848(0x1e5)];this['anchor']['x']=0.5,this[_0xdb3848(0x156)]['y']=0.5,this[_0xdb3848(0x273)]=0x0,this[_0xdb3848(0x194)]=0x0,this[_0xdb3848(0x147)]=0x0,this['_targetY']=0x0,this[_0xdb3848(0x178)]=0x0,this[_0xdb3848(0x200)]=_0x4df9ec[_0xdb3848(0x1ce)],this[_0xdb3848(0x263)]=_0x4df9ec[_0xdb3848(0x232)];},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x19f)]=function(){const _0x4231fc=_0x1298eb;return VisuMZ['AggroControlSystem'][_0x4231fc(0x1c9)][_0x4231fc(0x1e5)][_0x4231fc(0x159)];},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x224)]=function(){const _0x14928a=_0x1298eb;return VisuMZ['AggroControlSystem'][_0x14928a(0x1c9)][_0x14928a(0x1e5)][_0x14928a(0x160)]/0x64;},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x176)]=function(){const _0x2c0efe=_0x1298eb;this[_0x2c0efe(0x187)]=[];let _0xf01cc9=0x0;for(let _0x6dd41b=0x0;_0x6dd41b<=this['maxSprites']();_0x6dd41b++){const _0x5ab03a=new Sprite();_0x5ab03a[_0x2c0efe(0x251)]=ImageManager[_0x2c0efe(0x23e)](),_0x5ab03a[_0x2c0efe(0x156)]['x']=0.5,_0x5ab03a['anchor']['y']=0.5,_0x5ab03a[_0x2c0efe(0x1b4)]['x']=_0x5ab03a[_0x2c0efe(0x1b4)]['y']=this[_0x2c0efe(0x224)](),_0x5ab03a[_0x2c0efe(0x178)]=_0xf01cc9,_0x5ab03a[_0x2c0efe(0x263)]=this[_0x2c0efe(0x263)],this[_0x2c0efe(0x18e)](_0x5ab03a),this[_0x2c0efe(0x187)]['push'](_0x5ab03a),_0xf01cc9+=this['_opacitySpeed'];if(_0xf01cc9>=0xff)_0xf01cc9=0x0;}},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x207)]=function(){const _0xfa75e6=_0x1298eb;return this[_0xfa75e6(0x28a)][_0xfa75e6(0x282)]===Sprite_Actor;},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x258)]=function(){const _0x5ca3a7=_0x1298eb;return SceneManager[_0x5ca3a7(0x1a8)][_0x5ca3a7(0x27b)]['_provokeContainer'];},Sprite_ProvokeTrail['prototype'][_0x1298eb(0x215)]=function(){const _0x765f97=_0x1298eb;Sprite[_0x765f97(0x177)][_0x765f97(0x215)]['call'](this),this[_0x765f97(0x210)](),this[_0x765f97(0x211)](),this[_0x765f97(0x265)](),this[_0x765f97(0x204)]();},Sprite_ProvokeTrail[_0x1298eb(0x177)]['heightOrigin']=function(){const _0x4efe9d=_0x1298eb;return VisuMZ[_0x4efe9d(0x1ca)][_0x4efe9d(0x1c9)][_0x4efe9d(0x1e5)][_0x4efe9d(0x23d)];},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x210)]=function(){const _0x447eb9=_0x1298eb;if(!this[_0x447eb9(0x28a)][_0x447eb9(0x274)])return;if(!this[_0x447eb9(0x28a)][_0x447eb9(0x274)][_0x447eb9(0x250)]())return;const _0x1f560e=this['_mainSprite'][_0x447eb9(0x274)][_0x447eb9(0x250)]()[_0x447eb9(0x191)]();if(!_0x1f560e)return;const _0x28311a=this[_0x447eb9(0x28a)]['_battler'][_0x447eb9(0x193)](),_0x74ea0c=this['_mainSprite'][_0x447eb9(0x274)]['provoker']()[_0x447eb9(0x193)]();this['_homeX']=this[_0x447eb9(0x28a)]['x'],this[_0x447eb9(0x194)]=this[_0x447eb9(0x28a)]['y']-this['_mainSprite'][_0x447eb9(0x18d)]*_0x28311a,this[_0x447eb9(0x147)]=_0x1f560e['x'],this['_targetY']=_0x1f560e['y']-_0x1f560e[_0x447eb9(0x18d)]*_0x74ea0c,this['_homeX']+=Math[_0x447eb9(0x1a4)]((Graphics['width']-Graphics['boxWidth'])/0x2),this['_homeY']+=Math[_0x447eb9(0x1a4)]((Graphics['height']-Graphics[_0x447eb9(0x1ea)])/0x2),this['_targetX']+=Math[_0x447eb9(0x1a4)]((Graphics[_0x447eb9(0x206)]-Graphics['boxWidth'])/0x2),this[_0x447eb9(0x1f9)]+=Math[_0x447eb9(0x1a4)]((Graphics['height']-Graphics[_0x447eb9(0x1ea)])/0x2);if(!$gameSystem[_0x447eb9(0x1b2)]()){if(_0x1f560e[_0x447eb9(0x274)][_0x447eb9(0x28c)]())visible=!![],this[_0x447eb9(0x147)]+=SceneManager[_0x447eb9(0x1a8)]['_statusWindow']['x'],this[_0x447eb9(0x1f9)]+=SceneManager[_0x447eb9(0x1a8)]['_statusWindow']['y'];else _0x1f560e[_0x447eb9(0x274)][_0x447eb9(0x1f4)]()&&(visible=!![],this[_0x447eb9(0x273)]+=SceneManager[_0x447eb9(0x1a8)]['_statusWindow']['x'],this[_0x447eb9(0x194)]+=SceneManager[_0x447eb9(0x1a8)]['_statusWindow']['y']);}},Sprite_ProvokeTrail['prototype'][_0x1298eb(0x1c1)]=function(){const _0x2a24fe=_0x1298eb;return VisuMZ['AggroControlSystem'][_0x2a24fe(0x1c9)]['Provoke'][_0x2a24fe(0x20d)];},Sprite_ProvokeTrail['prototype'][_0x1298eb(0x211)]=function(){const _0x521801=_0x1298eb;if(!this[_0x521801(0x28a)][_0x521801(0x274)])return;if(!this['_mainSprite'][_0x521801(0x274)][_0x521801(0x250)]())return;if(!this[_0x521801(0x187)])return;if(this[_0x521801(0x187)][_0x521801(0x199)]<=0x0)return;const _0x4b8752=(this['_targetX']-this[_0x521801(0x273)])/this[_0x521801(0x19f)](),_0xe4bcbf=(this[_0x521801(0x1f9)]-this[_0x521801(0x194)])/this[_0x521801(0x19f)]();for(let _0x531a28=0x0;_0x531a28<=this['maxSprites']();_0x531a28++){const _0xa1be27=this[_0x521801(0x187)][_0x531a28];if(!_0xa1be27)continue;_0xa1be27['x']=this[_0x521801(0x273)]+_0x4b8752*_0x531a28;const _0x54db84=this['maxSprites']()-_0x531a28,_0x2c247=this[_0x521801(0x19f)]()/0x2,_0x2578ee=this[_0x521801(0x1c1)](),_0x1b5ce1=-_0x2578ee/Math['pow'](_0x2c247,0x2),_0x4a11fb=_0x1b5ce1*Math[_0x521801(0x18c)](_0x54db84-_0x2c247,0x2)+_0x2578ee;_0xa1be27['y']=this['_homeY']+_0xe4bcbf*_0x531a28-_0x4a11fb;}},Sprite_ProvokeTrail[_0x1298eb(0x177)][_0x1298eb(0x1b6)]=function(){const _0x195b2b=_0x1298eb;return VisuMZ['AggroControlSystem'][_0x195b2b(0x1c9)][_0x195b2b(0x1e5)][_0x195b2b(0x248)];},Sprite_ProvokeTrail['prototype'][_0x1298eb(0x265)]=function(){const _0xb586b5=_0x1298eb,_0x1aca70=this[_0xb586b5(0x28a)][_0xb586b5(0x274)];if(!_0x1aca70)this['opacity']=0x0;else _0x1aca70['isAlive']()&&_0x1aca70[_0xb586b5(0x250)]()?this['opacity']=0xff:this[_0xb586b5(0x178)]=0x0;},Sprite_ProvokeTrail['prototype'][_0x1298eb(0x204)]=function(){const _0x461ed8=_0x1298eb;if(!this[_0x461ed8(0x28a)][_0x461ed8(0x274)])return;if(!this['_mainSprite'][_0x461ed8(0x274)][_0x461ed8(0x250)]())return;if(!this[_0x461ed8(0x187)])return;if(this[_0x461ed8(0x187)][_0x461ed8(0x199)]<=0x0)return;for(let _0x3289a1=0x0;_0x3289a1<=this['maxSprites']();_0x3289a1++){const _0xa39672=this[_0x461ed8(0x187)][this[_0x461ed8(0x207)]()?this[_0x461ed8(0x19f)]()-_0x3289a1:_0x3289a1];if(!_0xa39672)continue;_0xa39672[_0x461ed8(0x178)]-=this[_0x461ed8(0x200)];if(_0xa39672[_0x461ed8(0x178)]<=0x0)_0xa39672[_0x461ed8(0x178)]=0xff;}},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x25b)]=Spriteset_Battle[_0x1298eb(0x177)][_0x1298eb(0x1ed)],Spriteset_Battle['prototype']['createBattleField']=function(){const _0x3fe008=_0x1298eb;VisuMZ[_0x3fe008(0x1ca)][_0x3fe008(0x25b)][_0x3fe008(0x1eb)](this),this[_0x3fe008(0x214)]();},Spriteset_Battle[_0x1298eb(0x177)]['createBattleFieldAggroControl']=function(){const _0x1ac321=_0x1298eb;if(!Imported[_0x1ac321(0x198)])return;const _0x3c7fe3=this[_0x1ac321(0x1a6)]['x'],_0x23ff97=this['_battleField']['y'],_0x97e9b6=this[_0x1ac321(0x1a6)][_0x1ac321(0x206)],_0x21aaeb=this[_0x1ac321(0x1a6)][_0x1ac321(0x18d)];this[_0x1ac321(0x1c3)]=new Sprite(),this[_0x1ac321(0x1c3)][_0x1ac321(0x161)](0x0,0x0,_0x97e9b6,_0x21aaeb),this['_provokeContainer']['x']=_0x3c7fe3,this[_0x1ac321(0x1c3)]['y']=_0x23ff97;if(Imported[_0x1ac321(0x198)]){const _0x4ed249=this['children'][_0x1ac321(0x20b)](this[_0x1ac321(0x183)]);this[_0x1ac321(0x28b)](this['_provokeContainer'],_0x4ed249);}else this[_0x1ac321(0x18e)](this[_0x1ac321(0x1c3)]);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x19d)]=Spriteset_Battle[_0x1298eb(0x177)]['update'],Spriteset_Battle['prototype']['update']=function(){const _0x47d0cd=_0x1298eb;VisuMZ['AggroControlSystem']['Spriteset_Battle_update'][_0x47d0cd(0x1eb)](this),this[_0x47d0cd(0x192)]();},Spriteset_Battle['prototype'][_0x1298eb(0x192)]=function(){const _0x1124c4=_0x1298eb;if(!this[_0x1124c4(0x1c3)])return;if(!this[_0x1124c4(0x183)])return;this[_0x1124c4(0x1c3)]['x']=this[_0x1124c4(0x183)]['x'],this[_0x1124c4(0x1c3)]['y']=this[_0x1124c4(0x183)]['y'];},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x186)]=Window_BattleEnemy[_0x1298eb(0x177)]['refresh'],Window_BattleEnemy[_0x1298eb(0x177)][_0x1298eb(0x16f)]=function(){const _0x4797c1=_0x1298eb;if(this[_0x4797c1(0x15a)]())Imported[_0x4797c1(0x198)]&&this['sortEnemies'](),Window_Selectable[_0x4797c1(0x177)][_0x4797c1(0x16f)][_0x4797c1(0x1eb)](this);else this[_0x4797c1(0x212)]()?(Imported[_0x4797c1(0x198)]&&this[_0x4797c1(0x1d7)](),Window_Selectable[_0x4797c1(0x177)]['refresh'][_0x4797c1(0x1eb)](this)):VisuMZ[_0x4797c1(0x1ca)]['Window_BattleEnemy_refresh'][_0x4797c1(0x1eb)](this);},Window_BattleEnemy[_0x1298eb(0x177)][_0x1298eb(0x15a)]=function(){const _0x205283=_0x1298eb,_0x5489bd=BattleManager[_0x205283(0x21d)](),_0x26c09d=BattleManager[_0x205283(0x24e)]();if(!_0x5489bd)return![];if(!_0x26c09d)return![];if(DataManager[_0x205283(0x21f)](_0x5489bd[_0x205283(0x1d4)]()))return![];if(_0x26c09d[_0x205283(0x201)]())return![];return _0x26c09d[_0x205283(0x14f)]()?(this[_0x205283(0x1f5)]=[_0x26c09d[_0x205283(0x250)]()],!![]):![];},Window_BattleEnemy[_0x1298eb(0x177)]['applyTauntFilters']=function(){const _0x557c52=_0x1298eb,_0xdfed06=BattleManager[_0x557c52(0x21d)](),_0x516824=BattleManager[_0x557c52(0x24e)](),_0x3f2bd9=$gameTroop;if(!_0xdfed06)return![];if(!_0x516824)return![];if(!_0xdfed06['item']())return![];if(DataManager[_0x557c52(0x264)](_0xdfed06[_0x557c52(0x1d4)]()))return![];if(_0x516824[_0x557c52(0x1be)]())return![];if(_0xdfed06['isPhysical']()&&_0x3f2bd9[_0x557c52(0x184)]()[_0x557c52(0x199)]>0x0)this[_0x557c52(0x1f5)]=_0x3f2bd9[_0x557c52(0x184)]();else{if(_0xdfed06[_0x557c52(0x278)]()&&_0x3f2bd9[_0x557c52(0x244)]()['length']>0x0)this[_0x557c52(0x1f5)]=_0x3f2bd9[_0x557c52(0x244)]();else{if(_0xdfed06[_0x557c52(0x16e)]()&&_0x3f2bd9[_0x557c52(0x268)]()[_0x557c52(0x199)]>0x0)this[_0x557c52(0x1f5)]=_0x3f2bd9[_0x557c52(0x268)]();else return![];}}return!![];},VisuMZ['AggroControlSystem'][_0x1298eb(0x1fc)]=Window_Options[_0x1298eb(0x177)]['addGeneralOptions'],Window_Options[_0x1298eb(0x177)][_0x1298eb(0x1c7)]=function(){const _0xbc1415=_0x1298eb;VisuMZ[_0xbc1415(0x1ca)]['Window_Options_addGeneralOptions'][_0xbc1415(0x1eb)](this),this[_0xbc1415(0x25e)]();},Window_Options['prototype'][_0x1298eb(0x25e)]=function(){const _0x45f51e=_0x1298eb;VisuMZ['AggroControlSystem']['Settings']['Provoke'][_0x45f51e(0x290)]&&this[_0x45f51e(0x259)](),VisuMZ[_0x45f51e(0x1ca)][_0x45f51e(0x1c9)]['Aggro'][_0x45f51e(0x290)]&&this[_0x45f51e(0x26c)]();},Window_Options[_0x1298eb(0x177)][_0x1298eb(0x259)]=function(){const _0x1610fd=_0x1298eb,_0x368657=TextManager[_0x1610fd(0x26a)],_0x549841='provokeOrigin';this[_0x1610fd(0x1e6)](_0x368657,_0x549841);},Window_Options[_0x1298eb(0x177)]['addAggroControlSystemAggroCommand']=function(){const _0x199214=_0x1298eb,_0x39d56c=TextManager[_0x199214(0x1c0)],_0xf157ea=_0x199214(0x1c0);this['addCommand'](_0x39d56c,_0xf157ea);},VisuMZ[_0x1298eb(0x1ca)][_0x1298eb(0x1d0)]=Window_StatusBase[_0x1298eb(0x177)][_0x1298eb(0x23f)],Window_StatusBase[_0x1298eb(0x177)][_0x1298eb(0x23f)]=function(_0x39af45,_0x38860a,_0x343089){const _0x1f9ca1=_0x1298eb;if(this[_0x1f9ca1(0x29b)]())this[_0x1f9ca1(0x1d8)](_0x39af45[_0x1f9ca1(0x1f0)]());VisuMZ[_0x1f9ca1(0x1ca)]['Window_StatusBase_placeActorName']['call'](this,_0x39af45,_0x38860a,_0x343089);},Window_StatusBase['prototype']['isAggroGaugeShown']=function(){const _0x23b902=_0x1298eb;if(![Window_BattleActor,Window_BattleStatus][_0x23b902(0x245)](this[_0x23b902(0x282)]))return![];if(!SceneManager[_0x23b902(0x257)]())return![];return ConfigManager['aggroGauge']&&VisuMZ['AggroControlSystem'][_0x23b902(0x1c9)][_0x23b902(0x256)]['StatusGauge'];},Window_StatusBase[_0x1298eb(0x177)][_0x1298eb(0x246)]=function(_0x4079d0,_0x2b6e34,_0x32d32f){const _0xb25458=_0x1298eb;this[_0xb25458(0x205)](_0x4079d0,_0xb25458(0x227),_0x2b6e34,_0x32d32f);},Window_BattleStatus['prototype'][_0x1298eb(0x1d8)]=function(_0x553de6){const _0x6076d7=_0x1298eb,_0x592444=this[_0x6076d7(0x24e)](_0x553de6),_0x21cc0d=this[_0x6076d7(0x18a)](_0x553de6),_0x1c8721=this[_0x6076d7(0x1a9)](_0x553de6),_0x1a8cb1='actor%1-gauge-aggro'[_0x6076d7(0x27a)](_0x592444[_0x6076d7(0x222)]()),_0x54b6a6=this[_0x6076d7(0x148)](_0x1a8cb1,Sprite_Gauge),_0x8ff74d=VisuMZ[_0x6076d7(0x1ca)][_0x6076d7(0x1c9)][_0x6076d7(0x256)];_0x54b6a6['x']=_0x21cc0d+(_0x8ff74d[_0x6076d7(0x231)]||0x0),_0x54b6a6['y']=_0x1c8721+(_0x8ff74d['BattleStatusOffsetY']||0x0),_0x54b6a6[_0x6076d7(0x16a)]=!![],_0x54b6a6[_0x6076d7(0x146)](_0x592444,_0x6076d7(0x227)),_0x54b6a6[_0x6076d7(0x242)]=!![];},Window_BattleStatus[_0x1298eb(0x177)][_0x1298eb(0x18a)]=function(_0x345b8f){const _0x4984c5=_0x1298eb;let _0x2d5dd1=this[_0x4984c5(0x17b)](_0x345b8f),_0x3f8b67=this[_0x4984c5(0x18b)](_0x2d5dd1);if(Imported[_0x4984c5(0x198)]){let _0x335fe8=this[_0x4984c5(0x295)](_0x345b8f);if(this['battleLayoutStyle']()===_0x4984c5(0x261)){const _0x52f631=$dataSystem['optDisplayTp']?0x4:0x3,_0x5d23d4=_0x52f631*0x80+(_0x52f631-0x1)*0x8+0x4,_0x1ec6f3=this[_0x4984c5(0x24e)](_0x345b8f);let _0x3c0c43=_0x335fe8['x']+this[_0x4984c5(0x1d2)];VisuMZ[_0x4984c5(0x188)][_0x4984c5(0x1c9)][_0x4984c5(0x164)][_0x4984c5(0x28f)]?_0x3c0c43=_0x335fe8['x']+ImageManager[_0x4984c5(0x145)]+0x8:_0x3c0c43+=ImageManager[_0x4984c5(0x2a0)],_0x3f8b67=Math['round'](Math[_0x4984c5(0x277)](_0x335fe8['x']+_0x335fe8[_0x4984c5(0x206)]-_0x5d23d4,_0x3c0c43)),_0x3f8b67-=0x4;}else _0x3f8b67=Math[_0x4984c5(0x1a4)](_0x335fe8['x']+(_0x335fe8[_0x4984c5(0x206)]-0x80)/0x2);}return _0x3f8b67;},Window_BattleStatus['prototype'][_0x1298eb(0x1a9)]=function(_0x365b6c){const _0x524963=_0x1298eb,_0x5f4c18=this[_0x524963(0x295)](_0x365b6c);let _0x5142e5=this['nameY'](_0x5f4c18);if(Imported[_0x524963(0x198)]){if(this[_0x524963(0x289)]()===_0x524963(0x261)){let _0x518aa0=this[_0x524963(0x295)](_0x365b6c);_0x5142e5=Math['round'](_0x518aa0['y']+(_0x518aa0['height']-Sprite_Name[_0x524963(0x177)][_0x524963(0x292)]())/0x2);}}if(this[_0x524963(0x196)]())_0x5142e5-=Sprite_Gauge[_0x524963(0x177)][_0x524963(0x26b)]()-0x1;return _0x5142e5;},Window_BattleStatus[_0x1298eb(0x177)][_0x1298eb(0x196)]=function(){const _0x92adcf=_0x1298eb;if(!BattleManager['isTpb']())return![];if(Imported[_0x92adcf(0x20f)])return this[_0x92adcf(0x25d)](_0x92adcf(0x168));return!![];};